/*************************************************************************
 *  Copyright (C) 2020 SecureFlag Limited
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains the property 
 * of SecureFlag Limited. The intellectual and technical concepts contained 
 * herein are proprietary to SecureFlag Limited and may be covered by EU
 * and Foreign Patents, patents in process, and are protected by trade secret 
 * or copyright law. Dissemination of this information or reproduction of this 
 * material is strictly forbidden unless prior written permission is obtained
 * from SecureFlag Limited.
 */
package com.secureflag.portal.gateway;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.model.ECSContainerTask;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.GatewayTempUser;
import com.secureflag.portal.model.User;

public class GuacamoleHelper {

	private static Logger logger = LoggerFactory.getLogger(GatewayHelper.class);

	public boolean isGuacOnline(Gateway gw){
		String cookie;
		try {
			cookie = doLogin(gw.getFqdn(),SFConfig.getGuacAdminUsername(),SFConfig.getGuacAdminPassword());
			JsonObject cookieObj = (JsonObject) new JsonParser().parse(cookie);
			String adminToken = null;
			if(null!=cookieObj.get("authToken")) {
				adminToken = (String) cookieObj.get("authToken").getAsString();
			}
			return null!=adminToken && !adminToken.equals("");
		} catch (Exception e) {
			logger.debug("Could not parse response from Gateway "+gw.getFqdn()+" due to:\n"+e.getMessage());
			return false;
		}	
	}

	public Integer getUserExerciseDuration(GatewayTempUser guacUser){
		try {
			String[] cookies = doALBLogin(guacUser.getGateway().getFqdn(),SFConfig.getGuacAdminUsername(),SFConfig.getGuacAdminPassword());
			JsonObject cookieObj = (JsonObject) new JsonParser().parse(cookies[0]);
			String adminToken = (String) cookieObj.get("authToken").getAsString();
			String encodedCookie = "GUAC_AUTH="+URLEncoder.encode(cookies[0], "UTF-8")+";"+cookies[1];
			long duration = 0;

			String history = sendGet(guacUser.getGateway().getFqdn(), "/sf/api/session/data/mysql/connections/"+guacUser.getConnectionId()+"/history?token="+adminToken, encodedCookie);

			JsonArray historyObj = new JsonParser().parse(history).getAsJsonArray();
			for (JsonElement h : historyObj) {
				JsonObject hObj = h.getAsJsonObject();
				long start = hObj.get("startDate").getAsLong();
				long end = 0;
				if(hObj.get("endDate").isJsonNull()){
					end = System.currentTimeMillis();
				}
				else{
					end = hObj.get("endDate").getAsLong();
				}
				duration += (end - start);
			}
			return (int) TimeUnit.MILLISECONDS.toMinutes(duration);
		}catch(Exception e) {
			logger.warn("Could not get duration for exercise "+guacUser.getUsername());
			return -1;
		}
	}

	public GatewayTempUser setupUser(User user, ECSContainerTask instance, Gateway gw, String password) throws Exception{
		try {
			Integer userId = user.getIdUser();
			String instanceHostname = instance.getIpAddress();
			Integer rdpPort = instance.getRdpPort();

			String [] cookies =  doALBLogin(gw.getFqdn(),SFConfig.getGuacAdminUsername(),SFConfig.getGuacAdminPassword());
			JsonObject cookieObj = (JsonObject) new JsonParser().parse(cookies[0]);
			String adminToken = (String) cookieObj.get("authToken").getAsString();

			String jsonContentType = "application/json";
			String encodedCookie = "GUAC_AUTH="+URLEncoder.encode(cookies[0], "UTF-8");
			encodedCookie += ";"+cookies[1];
			String instanceName = userId+"-E-"+System.currentTimeMillis();

			String newInstance = "{\"parentIdentifier\":\"ROOT\",\"name\":\""+instanceName+"\",\"protocol\":\"rdp\",\"parameters\":{\"port\":\""+rdpPort+"\",\"read-only\":\"\",\"swap-red-blue\":\"\",\"cursor\":\"\",\"color-depth\":\"\",\"recording-path\":\"\",\"recording-name\":\"\",\"create-recording-path\":\"\",\"resize-method\":\"reconnect\",\"enable-audio-input\":\"\",\"clipboard-encoding\":\"\",\"disable-copy\":\"true\",\"disable-paste\":\"\",\"dest-port\":\"\",\"recording-exclude-output\":\"\",\"recording-exclude-mouse\":\"\",\"recording-include-keys\": \"\",\"enable-sftp\":\"\",\"sftp-port\":\"\", \"sftp-server-alive-interval\":\"\",\"sftp-disable-download\":\"\",\"sftp-disable-upload\":\"\",\"enable-audio\":\"\",\"wol-send-packet\":\"\",\"wol-wait-time\":\"\",\"security\":\"any\",\"disable-auth\":\"\",\"ignore-cert\":\"true\",\"server-layout\":\"\",\"gateway-port\":\"\",\"server-layout\":\"\",\"timezone\":null,\"console\":\"\",\"width\":\"\",\"height\":\"\",\"dpi\":\"\",\"console-audio\":\"\",\"disable-audio\":\"true\",\"enable-printing\":\"\",\"enable-drive\":\"\",\"disable-download\":\"true\",\"disable-upload\": \"true\",\"create-drive-path\":\"\",\"enable-wallpaper\":\"\",\"enable-theming\":\"\",\"enable-font-smoothing\":\"\",\"enable-full-window-drag\":\"\",\"enable-desktop-composition\":\"\",\"enable-menu-animations\":\"\",\"disable-bitmap-caching\":\"\",\"disable-offscreen-caching\": \"\",\"disable-glyph-caching\":\"\",\"preconnection-id\":\"\",\"hostname\":\""+instanceHostname+"\",\"username\":\"sf\",\"password\":\""+password+"\"},\"attributes\":{\"max-connections\":\"5\",\"max-connections-per-user\":\"5\",\"weight\": \"\",\"failover-only\": \"\",\"guacd-port\": \"\",\"guacd-encryption\": \"\"}}";

			String newUsername = instanceName;

			String createdConnection = sendPost(gw.getFqdn(), "/sf/api/session/data/mysql/connections?token="+adminToken, newInstance, encodedCookie, jsonContentType);
			JsonObject connectionObj = (JsonObject) new JsonParser().parse(createdConnection);
			String connectionId = (String) connectionObj.get("identifier").getAsString();

			String newUser = "{\"username\":\""+newUsername+"\",\"password\":\""+password+"\",\"attributes\":{\"disabled\":\"\",\"expired\":\"\",\"access-window-start\":\"\",\"access-window-end\":\"\",\"valid-from\":\"\",\"valid-until\":\"\"}}";

			sendPost(gw.getFqdn(), "/sf/api/session/data/mysql/users?token="+adminToken, newUser, encodedCookie, jsonContentType);

			String user2connection = "[{\"op\":\"add\",\"path\":\"/connectionPermissions/"+connectionId+"\",\"value\":\"READ\"}]";
			String AWSCookie = cookies[1];
			sendPatch(gw.getFqdn() + "/sf/api/session/data/mysql/users/"+newUsername+"/permissions?token="+adminToken, user2connection, AWSCookie); 
			String[] userCookie = doALBLogin(gw.getFqdn(),newUsername,password);
			JsonObject userCookieObj = (JsonObject) new JsonParser().parse(userCookie[0]);
			String userToken = (String) userCookieObj.get("authToken").getAsString();
			GatewayTempUser guacTemp = new GatewayTempUser();
			guacTemp.setGateway(gw);
			guacTemp.setUser(user);
			guacTemp.setNode(userCookie[1]);
			guacTemp.setUsername(newUsername);	
			guacTemp.setLastValidToken(userToken);
			guacTemp.setPassword(password);	
			guacTemp.setConnectionId(connectionId);
			return guacTemp;
		}catch(Exception e) {
			logger.error("Could not setup user "+user.getIdUser()+" for connection on gw "+gw.getRegion()+" due to: "+e.getMessage());
			return null;
		}


	}

	public String[] getFreshToken(Gateway gw, String guacTempUsername, String guacTempPpassword){
		try {
			String[] userCookie = doALBLogin(gw.getFqdn(),guacTempUsername,guacTempPpassword);
			JsonObject userCookieObj = (JsonObject) new JsonParser().parse(userCookie[0]);
			String userToken = (String) userCookieObj.get("authToken").getAsString();
			String [] arr = {userToken,userCookie[1]};
			return arr;
		} catch (Exception e) {
			logger.error("Could not refresh tmp Guac user "+guacTempUsername+"'s token in region "+gw.getRegion());
			return null;
		}
	}

	private String[] doALBLogin(String hostname, String username, String password) throws Exception{
		DataOutputStream wr = null;
		BufferedReader in = null;
		StringBuffer response = new StringBuffer();
		try {
			String payload = "username="+username+"&password="+password;		
			URL obj = new URL("https://"+hostname+"/sf/api/tokens");
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setConnectTimeout(4000);

			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(payload);
			wr.flush();
			wr.close();
			con.getResponseCode();
			InputStream stream = con.getErrorStream();
			if (stream == null) {
				stream = con.getInputStream();
			}
			in = new BufferedReader(new InputStreamReader(stream));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			String guacCookie = response.toString();
			String otherCookies = getCookies(con);
			String [] arr = {guacCookie, otherCookies};
			return arr;
		}catch(Exception e ) {
			logger.warn(e.getMessage());
			String [] arr = {response.toString(), ""};
			return arr;
		}finally {
			if(null!=in)
				in.close();
			if(null!=wr)
				wr.close();
		}
	}

	private String doLogin(String hostname, String username, String password) throws Exception{
		DataOutputStream wr = null;
		BufferedReader in = null;
		StringBuffer response = new StringBuffer();
		try {
			String payload = "username="+username+"&password="+password;		
			URL obj = new URL("https://"+hostname+"/sf/api/tokens");
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setConnectTimeout(4000);

			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(payload);
			wr.flush();
			wr.close();
			con.getResponseCode();
			InputStream stream = con.getErrorStream();
			if (stream == null) {
				stream = con.getInputStream();
			}
			in = new BufferedReader(new InputStreamReader(stream));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		}catch(Exception e ) {
			logger.warn(e.getMessage());
			return response.toString();
		}finally {
			if(null!=in)
				in.close();
			if(null!=wr)
				wr.close();
		}
	}

	private String getCookies(HttpsURLConnection urlConnection) throws Exception{
		String headerName = null, returnCookie = "";
		try {
			// checking for each headers
			for (int i=1; (headerName = urlConnection.getHeaderFieldKey(i))!=null; i++) {
				// if its set-cookie, then take it
				if (headerName.equalsIgnoreCase("Set-Cookie")) {                  
					String cookie = urlConnection.getHeaderField(i);
					returnCookie += cookie.substring(0,cookie.indexOf(";")) + ";";
				}
			}
			return returnCookie;
		}catch(Exception e) {
			logger.error("Could not extract cookies due to:"+e.getMessage());
			return "";
		}
	}

	private String sendPost(String baseHost, String url, String body, String cookie, String contentType) throws Exception {
		DataOutputStream wr = null;
		BufferedReader in = null;
		StringBuffer response = new StringBuffer();
		try {
			URL obj = new URL("https://"+baseHost+url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Cookie", cookie);
			con.setRequestProperty("Content-Type",contentType); 
			con.setDoOutput(true);
			con.setConnectTimeout(8000);
			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(body);
			wr.flush();
			wr.close();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		}catch(Exception e ) {
			logger.warn(e.getMessage());
			return response.toString();
		}finally {
			if(null!=in)
				in.close();
			if(null!=wr)
				wr.close();
		}
	}

	private String sendGet(String baseHost, String url, String cookie) throws Exception {
		BufferedReader in = null;
		StringBuffer response = new StringBuffer();

		try{
			URL obj = new URL("https://"+baseHost+url);

			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Cookie", cookie);
			con.setDoOutput(true);
			con.setConnectTimeout(8000);
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		}catch(Exception e ) {
			logger.warn(e.getMessage());
			return response.toString();
		}finally {
			if(null!=in)
				in.close();
		}
	}

	private String sendPatch(String url, String body, String cookie) throws Exception, IOException {
		try {
			HttpResponse response = null;
			HttpClient httpclient = HttpClients.createDefault();
			HttpPatch httpPatch = new HttpPatch("https://"+url);

			int CONNECTION_TIMEOUT_MS = 8000; // Timeout in millis.
			RequestConfig requestConfig = RequestConfig.custom()
					.setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
					.setConnectTimeout(CONNECTION_TIMEOUT_MS)
					.setSocketTimeout(CONNECTION_TIMEOUT_MS)
					.build();

			StringEntity params = new StringEntity(body, ContentType.APPLICATION_JSON);
			httpPatch.setHeader("Cookie", cookie);
			httpPatch.setEntity(params);
			httpPatch.setConfig(requestConfig);
			response = httpclient.execute(httpPatch);
			return response.getStatusLine().getStatusCode()+"";
		}catch(Exception e) {
			logger.warn("Failed PATCH request to guacamole due to :"+e.getMessage());
			return "";
		}
	}
}
