/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.gateway;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceLaunchType;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultFile;
import com.secureflag.portal.model.dto.ExerciseInstanceAppStatus;
import com.secureflag.portal.model.dto.ExerciseInstanceSelfCheckResult;
import com.secureflag.portal.model.dto.SelfCheckResult;
import com.secureflag.portal.utils.ExerciseUtils;

public class GatewayHelper {

	private static Logger logger = LoggerFactory.getLogger(GatewayHelper.class);

	public ExerciseInstanceResultFile getResultFile(ExerciseInstance instance){
		String ip = "";
		Integer httpPort = -1;
		if(null==instance || null==instance.getLaunchType()) {
			return null;
		}
		if(instance.getLaunchType().equals(ExerciseInstanceLaunchType.ECS)) {
			ip = instance.getEcsInstance().getIpAddress();
			httpPort = instance.getEcsInstance().getHttpPort();
		}
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_ACTION, "getResultArchive");
		msg.addProperty("ip", ip+":"+httpPort);

		logger.debug("Sending getResultFile request to "+ip+":"+httpPort);

		byte[] resultFile = sendFilePost(instance.getGuac().getGateway().getFqdn(),msg.toString());

		ExerciseInstanceResultFile file = new ExerciseInstanceResultFile();

		if(null==resultFile || resultFile.length==0) {
			logger.warn("ExerciseResultFile from exerciseInstance "+instance.getIdExerciseInstance()+" is null");
			return null;
		}
		logger.debug("Returning result file for "+ip+":"+httpPort+" length:"+resultFile.length);

		file.setFile(resultFile);
		if(instance.getLaunchType().equals(ExerciseInstanceLaunchType.ECS))
			file.setFilename(instance.getEcsInstance().getName());

		return file;	
	}

	public ExerciseInstanceSelfCheckResult getSelfcheckResult(ExerciseInstance instance, String check){
		String ip = "";
		Integer httpPort = -1;
		ExerciseInstanceSelfCheckResult selfcheck = new ExerciseInstanceSelfCheckResult();

		if(null==instance || null==instance.getLaunchType()) {
			return selfcheck;
		}
		if(instance.getLaunchType().equals(ExerciseInstanceLaunchType.ECS)) {
			ip = instance.getEcsInstance().getIpAddress();
			httpPort = instance.getEcsInstance().getHttpPort();
		}
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_ACTION, "getResultStatus");
		msg.addProperty(Constants.IP_ADDRESS, ip+":"+httpPort);
		msg.addProperty(Constants.PARAM_NAME_CHECK, check);

		logger.debug("Sending getSelfcheckResult request to "+ip+":"+httpPort);

		List<SelfCheckResult> sList = new LinkedList<SelfCheckResult>();
		String jsonResponse = sendPost(instance.getGuac().getGateway().getFqdn(),msg.toString());

		if(null==jsonResponse || jsonResponse.equals("")){
			logger.warn("Received null results response from exerciseInstance "+instance.getIdExerciseInstance());
			return selfcheck;
		}
		JsonParser parser = new JsonParser();
		try {
			JsonObject obj = parser.parse(jsonResponse).getAsJsonObject();
			if(obj.get("results") == null || (obj.get("result") != null && obj.get("result").getAsString().equals("error"))) {
				return selfcheck;
			}

			Set<Map.Entry<String, JsonElement>> entries = obj.get("results").getAsJsonObject().entrySet();
			for (Map.Entry<String, JsonElement> entry: entries) {
				if(ExerciseUtils.isInScope(entry.getKey(), instance.getAvailableExercise().getQuestionsList())) {
					SelfCheckResult r = new SelfCheckResult();
					r.setName(entry.getKey());
					r.setStatus(entry.getValue().getAsString());
					sList.add(r);
				}
			}		
			selfcheck.setFlagList(sList);
			logger.debug("Returning result list for "+ip+":"+httpPort+" length:"+sList.size());
			return selfcheck;
		}catch(Exception e) {
			logger.warn("Received non-JSON results response from exerciseInstance "+instance.getIdExerciseInstance()+" due to:"+e.getMessage());
			return selfcheck;
		}
	}

	public ExerciseInstanceAppStatus getInstanceStatus(ExerciseInstance instance){
		String ip = "";
		Integer httpPort = -1;
		ExerciseInstanceAppStatus status = new ExerciseInstanceAppStatus();

		if(null==instance || null==instance.getLaunchType()) {
			return status;
		}
		if(instance.getLaunchType().equals(ExerciseInstanceLaunchType.ECS)) {
			ip = instance.getEcsInstance().getIpAddress();
			httpPort = instance.getEcsInstance().getHttpPort();
		}
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_ACTION, "getResultStatus");
		msg.addProperty(Constants.IP_ADDRESS, ip+":"+httpPort);
		msg.addProperty(Constants.PARAM_NAME_CHECK, "app-status");

		logger.debug("Sending getSelfcheckResult (for Instance Status) request to "+ip+":"+httpPort);

		String jsonResponse = sendPost(instance.getGuac().getGateway().getFqdn(),msg.toString());

		if(null==jsonResponse || jsonResponse.equals("")){
			logger.warn("Received null results response from exerciseInstance status for ex:"+instance.getIdExerciseInstance());
			return status;
		}
		JsonParser parser = new JsonParser();
		try {
			JsonObject obj = parser.parse(jsonResponse).getAsJsonObject();
			if(obj.get("results") == null || (obj.get("result") != null && obj.get("result").getAsString().equals("error"))) {
				return status;
			}

			Set<Map.Entry<String, JsonElement>> entries = obj.get("results").getAsJsonObject().entrySet();
			for (Map.Entry<String, JsonElement> entry: entries) {
				if(entry.getKey().equals("app-status")) {
					status.setStatus(entry.getValue().getAsString());
					return status;
				}
			}		
			logger.debug("Returning result (status) for "+ip+":"+httpPort);
			return status;
		}catch(Exception e) {
			logger.warn("Received non-JSON results response from exerciseInstance "+instance.getIdExerciseInstance()+" due to:"+e.getMessage());
			return status;
		}
	}



	public List<ExerciseInstanceResult> getResultStatus(ExerciseInstance instance, Challenge exerciseChallenge) {
		String ip = "";
		Integer httpPort = -1;
		List<ExerciseInstanceResult> sList = new LinkedList<ExerciseInstanceResult>();

		if(null==instance || null==instance.getLaunchType()) {
			return sList;
		}
		if(instance.getLaunchType().equals(ExerciseInstanceLaunchType.ECS)) {
			ip = instance.getEcsInstance().getIpAddress();
			httpPort = instance.getEcsInstance().getHttpPort();
		}

		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_ACTION, "getResultStatus");
		msg.addProperty("ip", ip+":"+httpPort);

		logger.debug("Sending getResultStatus request to "+ip+":"+httpPort);

		String jsonResponse = sendPost(instance.getGuac().getGateway().getFqdn(),msg.toString());

		if(null==jsonResponse || jsonResponse.equals("")){
			logger.warn("Received null results response from exerciseInstance "+instance.getIdExerciseInstance());
			return sList;
		}
		JsonParser parser = new JsonParser();
		try {
			JsonObject obj = parser.parse(jsonResponse).getAsJsonObject();
			if(obj.get("results") == null || (obj.get("result") != null && obj.get("result").getAsString().equals("error"))) {
				return sList;
			}

			Set<Map.Entry<String, JsonElement>> entries = obj.get("results").getAsJsonObject().entrySet();
			for (Map.Entry<String, JsonElement> entry: entries) {
				if(ExerciseUtils.isInScope(entry.getKey(), instance.getAvailableExercise().getQuestionsList())) {
					ExerciseInstanceResult r = ExerciseUtils.getExerciseResult(instance, exerciseChallenge, entry);
					if(null!=r)
						sList.add(r);
				}
			}
			logger.debug("Returning result list for "+ip+":"+httpPort+" length:"+sList.size());
			return sList;


		}catch(Exception e) {
			logger.warn("Received non-JSON results response from exerciseInstance "+instance.getIdExerciseInstance()+" due to :\n"+e.getMessage());
			return sList;
		}
	}




	private String getBasicAuthString() {
		//server-to-server interaction with the Gateway Agent
		//too expensive at this stage to setup Mutual TLS
		//password is configured from the cloudformation template
		String credsString = SFConfig.getAgentUser()+":"+SFConfig.getAgentPassword();
		String credentials = Base64.getEncoder().encodeToString(credsString.getBytes());
		return "Basic "+credentials;
	}

	private String sendPost(String gateway, String body) {
		URL obj;
		DataOutputStream wr = null;
		BufferedReader in = null;
		StringBuffer response = new StringBuffer();
		try {
			obj = new URL("https://"+gateway+"/helper/handler");
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type","application/json"); 
			con.setRequestProperty("Authorization", getBasicAuthString());
			con.setDoOutput(true);
			con.setConnectTimeout(18000);
			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(body);
			wr.flush();
			wr.close();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		} catch (Exception e) {
			logger.error("Failed sending POST request to gateway: "+gateway+" for body:\n"+body+"\n due to: "+e.getMessage());
			return null;
		}finally {
			try {
			if(null!=in)
				in.close();
			if(null!=wr)
				wr.close();
			}catch(Exception e2) {}
		}
		return response.toString();
	}
	private byte[] sendFilePost(String gateway, String body) {
		URL obj;
		DataOutputStream wr = null;
		InputStream is = null;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		try {
			obj = new URL("https://"+gateway+"/helper/handler");
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");			
			con.setRequestProperty("Content-Type","application/json"); 
			con.setRequestProperty("Authorization", getBasicAuthString());
			con.setDoOutput(true);
			con.setConnectTimeout(18000);
			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(body);
			wr.flush();
			wr.close();
			is = con.getInputStream();
			int nRead;
			byte[] data = new byte[16384];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();
			return buffer.toByteArray();
		} catch (Exception e) {
			logger.error("Failed sending File POST request to gateway: "+gateway+" for body:\n"+body+"\n due to: "+e.getMessage());
			return null;
		}finally {
			try {
			if(null!=is)
				is.close();
			if(null!=wr)
				wr.close();
			}catch(Exception e2) {}
		}
		
	}

	public Boolean pullImage(String gw, String instance, String image) {

		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_ACTION, "pullImage");
		msg.addProperty("ip", instance);
		msg.addProperty("url", image);

		try {
			String jsonResponse = sendPost(gw,msg.toString());
			if(null==jsonResponse || jsonResponse.equals("")){
				logger.warn("Received NULL response from pullImage of "+image+" from "+instance);
			}
			JsonParser parser = new JsonParser();

			JsonObject obj = parser.parse(jsonResponse).getAsJsonObject();
			if(obj.get("result") == null || (obj.get("result") != null && obj.get("result").getAsString().equals("error"))) {
				logger.warn("Received FAILURE response from pullImage of "+image+" from "+instance);
				return false;
			}
			return true;
		}catch(Exception e) {
			logger.warn("Received non-JSON response from pullImage of "+image+" from "+instance);
			return false;
		}
	}
	
	public Boolean removeImage(String gw, String instance, String image) {

		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_ACTION, "removeImage");
		msg.addProperty("ip", instance);
		msg.addProperty("url", image);

		try {
			String jsonResponse = sendPost(gw,msg.toString());
			if(null==jsonResponse || jsonResponse.equals("")){
				logger.warn("Received NULL response from removeImage of "+image+" from "+instance);
			}
			JsonParser parser = new JsonParser();

			JsonObject obj = parser.parse(jsonResponse).getAsJsonObject();
			if(obj.get("result") == null || (obj.get("result") != null && obj.get("result").getAsString().equals("error"))) {
				logger.warn("Received FAILURE response from removeImage of "+image+" from "+instance);
				return false;
			}
			return true;
		}catch(Exception e) {
			logger.warn("Received non-JSON response from removeImage of "+image+" from "+instance);
			return false;
		}
	}

}