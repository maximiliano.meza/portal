package com.secureflag.portal.hub;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.securitytoken.model.Credentials;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.model.HubAuthentication;
import com.secureflag.portal.model.dto.AuthRequest;
import com.secureflag.portal.model.dto.ResponseMessage;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class HubAuthenticationHelper {

	private AWSHelper aws = new AWSHelper();
	private static Logger logger = LoggerFactory.getLogger(HubAuthenticationHelper.class);

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private static String currentJwtAuth = "";	
	

	public String doHubAuthentication() {
		String jwt = getJWTString(aws.getHubAWSCredentials());
		if(null!=jwt) {
			currentJwtAuth = jwt;
			hpc.removeHubAuthentication();
			HubAuthentication hub = new HubAuthentication();
			hub.setJwt(jwt);
			hpc.addHubAuthentication(hub);
		}
		return jwt;
	}
	
	private String getJWTString(Credentials credentials) {

		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/api/login-deployment";

		AuthRequest request = new AuthRequest();
		request.setAccessKeyId(credentials.getAccessKeyId());
		request.setSecretAccessKey(credentials.getSecretAccessKey());
		request.setSessionToken(credentials.getSessionToken());

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		try {
			Response response = webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(Entity.entity(request, MediaType.APPLICATION_JSON));
			ResponseMessage m = response.readEntity(ResponseMessage.class);
			return m.getValue();
		}catch(Exception e ) {
			e.printStackTrace();
			logger.error("Could not exchange JWT String due to:\n"+e.getMessage());
			return null;
		}
	}

	public String getCurrentJwtAuth() {
		// get from memory
		if(null!=currentJwtAuth && !currentJwtAuth.equals(""))
			return currentJwtAuth;
		// get from db
		HubAuthentication authDb = hpc.getHubAuthentication();
		if(null!=authDb && null!=authDb.getJwt()) {
			currentJwtAuth =  authDb.getJwt();
			return currentJwtAuth;
		}
		// get new creds from hub
		return doHubAuthentication();
	}

}
