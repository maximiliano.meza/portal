package com.secureflag.portal.hub;

import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.securitytoken.model.Credentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.messages.json.TimestampTypeAdapter;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.AvailableExerciseQuality;
import com.secureflag.portal.model.AvailableExerciseStatus;
import com.secureflag.portal.model.DownloadQueueItemType;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.KBStatus;
import com.secureflag.portal.model.KBTechnologyItem;
import com.secureflag.portal.model.KBVulnerabilityItem;
import com.secureflag.portal.model.LearningPath;
import com.secureflag.portal.model.LearningPathStatus;
import com.secureflag.portal.model.dto.InstallationRequest;
import com.secureflag.portal.model.dto.LearningPathInstallationResponse;
import com.secureflag.portal.model.dto.UploadBucket;

public class HubRestFacade {

	private static Logger logger = LoggerFactory.getLogger(HubRestFacade.class);

	private HubAuthenticationHelper hubHelper = new HubAuthenticationHelper();

	public AvailableExercise installExercise(String uuid, DownloadQueueItemType type) {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/exercises/install";

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		InstallationRequest request = new InstallationRequest();
		request.setType(type);
		request.setUuid(uuid);
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).post(Entity.entity(request, MediaType.APPLICATION_JSON));
			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					installExercise(uuid,type);
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			String res = response.readEntity(String.class);
			
			Gson gson = new GsonBuilder()
					.registerTypeAdapter(AvailableExerciseQuality.class, GsonCustomDeserializerUtils.availableExerciseQuality)
					.registerTypeAdapter(AvailableExerciseStatus.class, GsonCustomDeserializerUtils.availableExerciseStatus)
					.registerTypeAdapter(ExerciseInstanceResultStatus.class, GsonCustomDeserializerUtils.exerciseResultStatus)
					.registerTypeAdapter(AvailableExerciseExerciseScoringMode.class, GsonCustomDeserializerUtils.exerciseScoringMode)
					.registerTypeAdapter(KBStatus.class, GsonCustomDeserializerUtils.kbStatus)
					.registerTypeAdapter(LearningPathStatus.class, GsonCustomDeserializerUtils.learningPathStatus).create();
			return gson.fromJson(res, AvailableExercise.class);		
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		} finally {
			if(null!=response)
				response.close();
		}
	}
	
	public LearningPathInstallationResponse installLearningPath(String uuid, DownloadQueueItemType type) {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/learning/install";

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		InstallationRequest request = new InstallationRequest();
		request.setType(type);
		request.setUuid(uuid);
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).post(Entity.entity(request, MediaType.APPLICATION_JSON));
			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					installLearningPath(uuid,type);
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			String res = response.readEntity(String.class);
			Gson gson = new GsonBuilder()
					.registerTypeAdapter(AvailableExerciseQuality.class, GsonCustomDeserializerUtils.availableExerciseQuality)
					.registerTypeAdapter(AvailableExerciseStatus.class, GsonCustomDeserializerUtils.availableExerciseStatus)
					.registerTypeAdapter(ExerciseInstanceResultStatus.class, GsonCustomDeserializerUtils.exerciseResultStatus)
					.registerTypeAdapter(AvailableExerciseExerciseScoringMode.class, GsonCustomDeserializerUtils.exerciseScoringMode)
					.registerTypeAdapter(KBStatus.class, GsonCustomDeserializerUtils.kbStatus)
					.registerTypeAdapter(LearningPathStatus.class, GsonCustomDeserializerUtils.learningPathStatus).create();
			return gson.fromJson(res, LearningPathInstallationResponse.class);		
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		} finally {
			if(null!=response)
				response.close();
		}
	}

	public List<AvailableExercise> getExercises() {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/exercises";

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);
			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getExercises();
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			List<AvailableExercise> exercises = response.readEntity(new GenericType<List<AvailableExercise>>() {});
			return exercises;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}

	public List<LearningPath> getLearningPaths() {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/learning";

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);
			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getLearningPaths();
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			List<LearningPath> exercises = response.readEntity(new GenericType<List<LearningPath>>() {});
			return exercises;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}

	public AvailableExercise getExerciseDetails(String uuid) {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/exercises/"+uuid;

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);

			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getExerciseDetails(uuid);
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			AvailableExercise exercise = null;
			String res = response.readEntity(String.class);
			try {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).create();
				exercise = gson.fromJson(res, AvailableExercise.class);
			}catch(Exception e) {
				Gson gson = new Gson();
				exercise = gson.fromJson(res, AvailableExercise.class);
			}
			return exercise;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}

	public KBVulnerabilityItem getVulnerabilityKBItem(String uuid) {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/vulnerabilities/"+uuid;

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);

			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getVulnerabilityKBItem(uuid);
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			KBVulnerabilityItem vulnerability = null;
			String res = response.readEntity(String.class);
			try {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).create();
				vulnerability = gson.fromJson(res, KBVulnerabilityItem.class);
			}catch(Exception e) {
				Gson gson = new Gson();
				vulnerability = gson.fromJson(res, KBVulnerabilityItem.class);
			}
			return vulnerability;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}

	public KBTechnologyItem getTechnologyKBItem(String uuid) {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/technologies/"+uuid;

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);

			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getTechnologyKBItem(uuid);
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			KBTechnologyItem stack = null;
			String res = response.readEntity(String.class);
			try {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).create();
				stack = gson.fromJson(res, KBTechnologyItem.class);
			}catch(Exception e) {
				Gson gson = new Gson();
				stack = gson.fromJson(res, KBTechnologyItem.class);
			}
			return stack;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}

	public List<KBVulnerabilityItem> getVulnerabilityKBs() {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/vulnerabilities";

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;

		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);

			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getVulnerabilityKBs();
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			List<KBVulnerabilityItem> list = response.readEntity(new GenericType<List<KBVulnerabilityItem>>() {});
			return list;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}

	public List<KBTechnologyItem> getTechnologyKBs() {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/technologies";

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);

			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getTechnologyKBs();
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			List<KBTechnologyItem> list = response.readEntity(new GenericType<List<KBTechnologyItem>>() {});
			return list;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}

	public Credentials getHubS3Credentials() {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/uploads/credentials";

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;

		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);

			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getHubS3Credentials();
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			Credentials credentials = null;
			String res = response.readEntity(String.class);
			try {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).create();
				credentials = gson.fromJson(res, Credentials.class);
			}catch(Exception e) {
				Gson gson = new Gson();
				credentials = gson.fromJson(res, Credentials.class);
			}
			return credentials;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}

	public UploadBucket getHubUploadBucket() {
		final String REST_URI = RestClient.getProtocol()+SFConfig.getHubAddress()+"/rest/deployment/uploads/bucket";

		WebTarget webTarget = RestClient.getClient().target(REST_URI);
		String bearer = "Bearer "+hubHelper.getCurrentJwtAuth();
		Response response = null;
		try {
			response = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, bearer).accept(MediaType.APPLICATION_JSON).get(Response.class);

			if(response.getStatus()==401) {
				// old bearer, re-authenticate, if successful re-run
				if(null!=hubHelper.doHubAuthentication()) {
					logger.info("JWT Token is not valid, a new token has been generated.");
					return getHubUploadBucket();
				}
				else {
					logger.warn("JWT Token is not valid, a new token could not be exchanged.");
				}		
			}
			UploadBucket uploadBucket = null;
			String res = response.readEntity(String.class);
			try {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).create();
				uploadBucket = gson.fromJson(res, UploadBucket.class);
			}catch(Exception e) {
				Gson gson = new Gson();
				uploadBucket = gson.fromJson(res, UploadBucket.class);
			}
			return uploadBucket;
		}catch(Exception e ) {
			logger.error(e.getMessage());
			return null;
		}finally {
			if(null!=response)
				response.close();
		}
	}
}