package com.secureflag.portal.hub;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.AvailableExerciseQuality;
import com.secureflag.portal.model.AvailableExerciseStatus;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.KBStatus;
import com.secureflag.portal.model.LearningPathStatus;

public class GsonCustomDeserializerUtils {
	
	public static JsonDeserializer<ExerciseInstanceResultStatus> exerciseResultStatus = new JsonDeserializer<ExerciseInstanceResultStatus>() {  
	   
		@Override
		public ExerciseInstanceResultStatus deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			String enumStr = json.getAsString();
			try {
				return ExerciseInstanceResultStatus.getStatusFromStatusString(enumStr);
			}catch(Exception e) {
				return ExerciseInstanceResultStatus.getStatusFromStatusCode(Integer.valueOf(enumStr));
			}
		}
	};
	
	public static JsonDeserializer<AvailableExerciseQuality> availableExerciseQuality = new JsonDeserializer<AvailableExerciseQuality>() {  
		   
		@Override
		public AvailableExerciseQuality deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			String enumStr = json.getAsString();
			try {
				return AvailableExerciseQuality.getStatusFromStatusString(enumStr);
			}catch(Exception e) {
				return AvailableExerciseQuality.getStatusFromStatusCode(Integer.valueOf(enumStr));
			}
		}
	};
	
	public static JsonDeserializer<AvailableExerciseExerciseScoringMode> exerciseScoringMode = new JsonDeserializer<AvailableExerciseExerciseScoringMode>() {  
		   
		@Override
		public AvailableExerciseExerciseScoringMode deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			String enumStr = json.getAsString();
			try {
				return AvailableExerciseExerciseScoringMode.getStatusFromStatusString(enumStr);
			}catch(Exception e) {
				return AvailableExerciseExerciseScoringMode.getStatusFromStatusCode(Integer.valueOf(enumStr));
			}
		}
	};
	
	public static JsonDeserializer<AvailableExerciseStatus> availableExerciseStatus = new JsonDeserializer<AvailableExerciseStatus>() {  
		   
		@Override
		public AvailableExerciseStatus deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			String enumStr = json.getAsString();
			try {
				return AvailableExerciseStatus.getStatusFromStatusString(enumStr);
			}catch(Exception e) {
				return AvailableExerciseStatus.getStatusFromStatusCode(Integer.valueOf(enumStr));
			}
		}
	};
	
	public static JsonDeserializer<KBStatus> kbStatus = new JsonDeserializer<KBStatus>() {  
		   
		@Override
		public KBStatus deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			String enumStr = json.getAsString();
			try {
				return KBStatus.getStatusFromStatusString(enumStr);
			}catch(Exception e) {
				return KBStatus.getStatusFromStatusCode(Integer.valueOf(enumStr));
			}
		}
	};
	
	public static JsonDeserializer<LearningPathStatus> learningPathStatus = new JsonDeserializer<LearningPathStatus>() {  
		   
		@Override
		public LearningPathStatus deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			String enumStr = json.getAsString();
			try {
				return LearningPathStatus.getStatusFromStatusString(enumStr);
			}catch(Exception e) {
				return LearningPathStatus.getStatusFromStatusCode(Integer.valueOf(enumStr));
			}
		}
	};

}
