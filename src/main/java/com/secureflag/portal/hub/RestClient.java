package com.secureflag.portal.hub;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.secureflag.portal.config.SFConfig;

public class RestClient {

	private static Client client;
	private static String protocol;

	public static void init() {
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(GsonProvider.class);
		client = ClientBuilder.newClient( clientConfig);
		if(SFConfig.getEnvironment().equals("DEV")) {
			protocol = "http://";
		}
		else {
			protocol = "https://";
		}
	}

	public static String getProtocol() {
		if(null==protocol)
			init();
		return protocol;
	}

	public static Client getClient() {
		if(null==client)
			init();
		return client;
	}


}
