package com.secureflag.portal.servlets;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class HServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		if(hpc.testDatabaseConnection()) {
			response.setStatus(200);
			out.print("OK");
		}
		else {
			response.setStatus(500);
			out.print("KO");
		}
		out.close();
	}
}