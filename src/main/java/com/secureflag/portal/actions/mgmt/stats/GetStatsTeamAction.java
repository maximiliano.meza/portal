/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.stats;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.Team;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.dto.StatsObject;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.StatsUtils;

public class GetStatsTeamAction extends SFAction {

	private  HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private static StatsUtils statsUtils = new StatsUtils();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer teamId= jsonElement.getAsInt();
		List<User> users = hpc.getUsersForTeamId(teamId,sessionUser.getManagedOrganizations());

		if(sessionUser.getRole().equals(Constants.ROLE_TEAM_MANAGER)){
			Team team = hpc.getTeam(teamId,sessionUser.getManagedOrganizations());
			boolean isManager = false;
			for(User u : team.getManagers()){
				if(u.getIdUser().equals(sessionUser.getIdUser())){
					isManager = true;
					break;
				}
			}
			if(!isManager){
				MessageGenerator.sendErrorMessage("NotFound", response);
				return;
			}
		}
		
		if(null==users || users.isEmpty()){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		
		List<ExerciseInstance> exerciseInstances = hpc.getReviewedExerciseInstancesWithResultsFlagsUserForStats(users);
	
		StatsObject stats = new StatsObject();
		statsUtils.setTimePerCategory(exerciseInstances,stats);
		stats.setIssuesRemediationRate(statsUtils.getRemediationRatePerIssue(exerciseInstances));
		stats.setCategoriesRemediationRate(statsUtils.getRemediationRatePerIssueCategory(exerciseInstances));

		MessageGenerator.sendStatsMessage(stats, response);
	}


}
