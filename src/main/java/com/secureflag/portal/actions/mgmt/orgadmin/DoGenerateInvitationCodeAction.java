/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.orgadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.InvitationCodeForOrganization;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.RandomGeneratorUtils;

public class DoGenerateInvitationCodeAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonElement orgIdElement = json.get(Constants.ACTION_PARAM_ORG_ID);
		Integer orgId = orgIdElement.getAsInt();
		JsonElement maxRedeemsElement = json.get(Constants.ACTION_PARAM_MAX_USERS);
		Integer maxRedeems = maxRedeemsElement.getAsInt();
		JsonElement challengeIdElement =  json.get(Constants.ACTION_PARAM_CHALLENGE_ID);
		
		String redeem = RandomGeneratorUtils.getNextString(10);
		
		
		Organization org = hpc.getOrganizationById(orgId);
		if(!isManagingOrg(sessionUser,org)){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		
		InvitationCodeForOrganization invitation = new InvitationCodeForOrganization();
		invitation.setCode(redeem);
		invitation.setLeftToRedeem(maxRedeems);
		invitation.setOrganization(org);
		
		if(null!=challengeIdElement) {
			Integer challengeId = challengeIdElement.getAsInt();
			invitation.setChallengeId(challengeId);
		}
		
		Boolean result = hpc.addInvitationCode(invitation);
		
		if(result)
			MessageGenerator.sendSuccessMessage(response);
		else
			MessageGenerator.sendErrorMessage("Failed", response);
	}
	
	private Boolean isManagingOrg(User user, Organization org) {
		for(Organization managed : user.getManagedOrganizations()) {
			if(managed.getId().equals(org.getId())) {
				return true;
			}
		}
		return false;
	}

}
