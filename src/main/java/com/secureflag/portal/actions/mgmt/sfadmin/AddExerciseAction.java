/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.AvailableExerciseInformation;
import com.secureflag.portal.model.AvailableExerciseQuality;
import com.secureflag.portal.model.AvailableExerciseSolution;
import com.secureflag.portal.model.AvailableExerciseStatus;
import com.secureflag.portal.model.AvailableExerciseType;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.FlagQuestionHint;
import com.secureflag.portal.model.FlagQuestionSelfcheck;
import com.secureflag.portal.model.KBStatus;
import com.secureflag.portal.model.KBTechnologyItem;
import com.secureflag.portal.model.KBVulnerabilityItem;
import com.secureflag.portal.model.MarkdownText;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class AddExerciseAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement titleElement = json.get(Constants.ACTION_PARAM_TITLE);
		JsonElement subtitleElement = json.get(Constants.ACTION_PARAM_SUBTITLE);
		JsonElement descriptionElement = json.get(Constants.ACTION_PARAM_DESCRIPTION);
		JsonElement difficultyElement = json.get(Constants.ACTION_PARAM_DIFFICULTY);
		JsonElement technologyElement = json.get(Constants.ACTION_PARAM_TECHNOLOGY);
		JsonElement durationElement = json.get(Constants.ACTION_PARAM_DURATION);
		JsonElement trophyNameElement = json.get(Constants.ACTION_PARAM_TROPHY_NAME);
		JsonElement statusElement = json.get(Constants.ACTION_PARAM_STATUS);
		JsonElement typeElement = json.get(Constants.ACTION_PARAM_EXERCISE_TYPE);
		JsonElement authorElement = json.get(Constants.ACTION_PARAM_AUTHOR);
		JsonElement ignoreDefaultStackElement =  json.get(Constants.ACTION_PARAM_IGNORE_DEFAULT_STACK);
		JsonElement qualityElement =  json.get(Constants.ACTION_PARAM_EXERCISE_QUALITY);
		JsonElement frameworkElement = json.get(Constants.ACTION_PARAM_FRAMEWORK);

		AvailableExercise exercise = new AvailableExercise();
		exercise.setDescription(descriptionElement.getAsString());
		exercise.setDifficulty(difficultyElement.getAsString());
		exercise.setDuration(durationElement.getAsInt());
		exercise.setTitle(titleElement.getAsString());
		exercise.setSubtitle(subtitleElement.getAsString());
		exercise.setTechnology(technologyElement.getAsString());
		if(null!=frameworkElement) {
			exercise.setFramework(frameworkElement.getAsString());
		}
		else {
			exercise.setFramework("Default");

		}
		exercise.setStatus(AvailableExerciseStatus.getStatusFromStatusCode(statusElement.getAsInt()));
		exercise.setAuthor(authorElement.getAsString());
		exercise.setIgnoreDefaultStack(ignoreDefaultStackElement.getAsBoolean());
		exercise.setQuality(AvailableExerciseQuality.getStatusFromStatusCode(qualityElement.getAsInt()));
		exercise.setFromHub(false);
		exercise.setUuid("local-"+UUID.randomUUID().toString());
		exercise.setVersion(0);
		exercise.setVotingScore(0);
		exercise.setLastUpdate(new Date());
		exercise.setVotingScoreNumbers(0);
		exercise.setInternetOutboundAllowed(false);
		exercise.setClipboardAllowed(true);

		exercise.setExerciseType(AvailableExerciseType.getStatusFromName(typeElement.getAsString()));
		exercise.setTrophyName(trophyNameElement.getAsString());

		Gson gson = new Gson();
		List<String> tags = gson.fromJson(json.get(Constants.ACTION_PARAM_TAGS), new TypeToken<List<String>>(){}.getType());
		exercise.setTags(tags);

		JsonElement flags = json.get(Constants.ACTION_PARAM_FLAGS_LIST);
		AvailableExerciseInformation infos =  gson.fromJson(json.get(Constants.INFORMATION).getAsJsonObject(),AvailableExerciseInformation.class);
		if(null!=infos)
			infos.setId(null);
		exercise.setInformation(infos);

		if(exercise.getIgnoreDefaultStack() && (null == exercise.getInformation() || null == exercise.getInformation().getText() || "".equals(exercise.getInformation().getText()))) {
			MessageGenerator.sendErrorMessage("infoEmpty", response);
			return;
		}

		AvailableExerciseSolution solution =  gson.fromJson(json.get(Constants.SOLUTION).getAsJsonObject(),AvailableExerciseSolution.class);
		if(null!=solution) {
			solution.setId(null);
			solution.setText(solution.getText().replaceAll("Â", "").replaceAll("Ã", ""));
		}
		exercise.setSolution(solution);

		if(null == exercise.getSolution() || null == exercise.getSolution().getText() || "".equals(exercise.getSolution().getText())) {
			MessageGenerator.sendErrorMessage("solutionEmpty", response);
			return;
		}
		KBTechnologyItem stack = gson.fromJson(json.get(Constants.STACK).getAsJsonObject(),KBTechnologyItem.class);
		if(null == stack || null == stack.getTechnology() || "".equals(stack.getTechnology())) {
			MessageGenerator.sendErrorMessage("stackEmpty", response);
			return;
		}
		KBTechnologyItem dbStack = null;
		if(null!=stack.getUuid()) {
			dbStack = hpc.getTechnologyStackByUUID(stack.getUuid());
			if(null==dbStack)
				dbStack = hpc.getTechnologyStack(stack.getTechnology(),stack.getVariant());
		}
		else { 
			dbStack = hpc.getTechnologyStack(stack.getTechnology(),stack.getVariant());
		}

		if(null==dbStack) {
			stack.setFromHub(false);
			stack.setLastUpdate(new Date());
			stack.setUuid("local-"+UUID.randomUUID().toString());
			stack.setStatus(KBStatus.AVAILABLE);
			stack.setId(null);
			exercise.setStack(stack);
		}else {
			exercise.setStack(dbStack);
		}

		Integer totalScore = 0;
		HashSet<Flag> flagList = new HashSet<Flag>();
		exercise.setSupportsAutomatedScoring(true);
		for(JsonElement flagElem : flags.getAsJsonArray()) {
			Flag flag = new Flag();

			JsonObject tmpFlag = flagElem.getAsJsonObject();
			JsonElement flagTitle = tmpFlag.get(Constants.ACTION_PARAM_TITLE);
			JsonElement flagCategory = tmpFlag.get(Constants.ACTION_PARAM_CATEGORY);
			JsonElement flagQuestions = tmpFlag.get(Constants.ACTION_PARAM_FLAG_QUESTIONS);

			KBVulnerabilityItem flagKBItem = gson.fromJson(tmpFlag.get(Constants.ACTION_PARAM_KB).getAsJsonObject(),KBVulnerabilityItem.class);
			KBVulnerabilityItem vulnerabilityDB = null;
			if(null!=flagKBItem.getUuid()) {
				vulnerabilityDB = hpc.getVulnerabilityKBItemByUUIDNoRecursive(flagKBItem.getUuid());
				if(null==vulnerabilityDB) {
					if(null!=flagKBItem.getIsAgnostic() && flagKBItem.getIsAgnostic()) {
						vulnerabilityDB = hpc.getAgnosticVulnerabilityKBItem(flagKBItem.getVulnerability());
					}else {
						vulnerabilityDB = hpc.getNonAgnosticVulnerabilityKBItem(flagKBItem.getVulnerability(),flagKBItem.getTechnology());
					}
				}
			}
			else {
				if(null!=flagKBItem.getIsAgnostic() && flagKBItem.getIsAgnostic()) {
					vulnerabilityDB = hpc.getAgnosticVulnerabilityKBItem(flagKBItem.getVulnerability());
				}else {
					vulnerabilityDB = hpc.getNonAgnosticVulnerabilityKBItem(flagKBItem.getVulnerability(),flagKBItem.getTechnology());
				}
			}
			if(null==vulnerabilityDB) {
				flagKBItem.setFromHub(false);
				flagKBItem.setLastUpdate(new Date());
				flagKBItem.setUuid("local-"+UUID.randomUUID().toString());
				flagKBItem.setStatus(KBStatus.AVAILABLE);
				flagKBItem.setId(null);
				flagKBItem.setKBDetailsMapping(new HashMap<String,KBVulnerabilityItem>());
				Integer idDb = hpc.addVulnerability(flagKBItem);
				if(null!=idDb) {
					flagKBItem.setId(idDb);
					flag.setKb(flagKBItem);
				}
			}
			else {
				flag.setKb(vulnerabilityDB);
			}
			flag.setCategory(flagCategory.getAsString());
			flag.setTitle(flagTitle.getAsString());

			HashSet<FlagQuestion> questionList = new HashSet<FlagQuestion>();

			for(JsonElement questionElem : flagQuestions.getAsJsonArray()) {
				FlagQuestion tmpQuestion = new FlagQuestion();
				JsonObject qEl = questionElem.getAsJsonObject();
				tmpQuestion.setType(qEl.get(Constants.ACTION_PARAM_TYPE).getAsString());
				tmpQuestion.setOptional(qEl.get(Constants.ACTION_PARAM_OPTIONAL).getAsBoolean());
				if(!tmpQuestion.getOptional()) {
					tmpQuestion.setMaxScore(qEl.get(Constants.ACTION_PARAM_MAX_SCORE).getAsInt());
					totalScore += tmpQuestion.getMaxScore();
				}
				else {
					tmpQuestion.setMaxScore(0);
				}
				tmpQuestion.setSelfCheckAvailable(qEl.get(Constants.ACTION_PARAM_SELF_CHECK_AVAILABLE).getAsBoolean());
				if(tmpQuestion.getSelfCheckAvailable()) {
					FlagQuestionSelfcheck flagSelfCheck = gson.fromJson(qEl.get(Constants.ACTION_PARAM_SELFCHECK).getAsJsonObject(),FlagQuestionSelfcheck.class);
					flagSelfCheck.setId(null);
					for(String status : flagSelfCheck.getMessageMappings().keySet()) {
						flagSelfCheck.getMessageMappings().put(status , flagSelfCheck.getMessageMappings().get(status).replaceAll("Â", "").replaceAll("Ã", ""));
					}		
					tmpQuestion.setSelfCheck(flagSelfCheck);
				}
				else {
					tmpQuestion.setSelfCheck(null);
				}
				MarkdownText md = gson.fromJson(qEl.get(Constants.ACTION_PARAM_MD).getAsJsonObject(),MarkdownText.class);
				md.setId(null);
				//WORKAROUND for TUI.Editor
				md.setText(md.getText().replaceAll("Â", "").replaceAll("Ã", ""));
				tmpQuestion.setMd(md);
				tmpQuestion.setHintAvailable(qEl.get(Constants.ACTION_PARAM_HINT_AVAILABLE).getAsBoolean());
				if(tmpQuestion.getHintAvailable()) {
					FlagQuestionHint newHint = gson.fromJson(qEl.get(Constants.ACTION_PARAM_HINT).getAsJsonObject(),FlagQuestionHint.class);
					newHint.setId(null);
					newHint.getMd().setId(null);
					newHint.getMd().setText(newHint.getMd().getText().replaceAll("Â", "").replaceAll("Ã", ""));
					tmpQuestion.setHint(newHint);
				}
				else {
					FlagQuestionHint emptyHint = new FlagQuestionHint();
					emptyHint.setMd(new MarkdownText());
					tmpQuestion.setHint(emptyHint);
				}
				if(!tmpQuestion.getSelfCheckAvailable() && !tmpQuestion.getOptional())
					exercise.setSupportsAutomatedScoring(false);
				questionList.add(tmpQuestion);
			}
			flag.setFlagQuestionList(questionList);
			flagList.add(flag);
		}

		exercise.setQuestionsList(flagList);
		if(flagList.isEmpty()) {
			MessageGenerator.sendErrorMessage("flagListEmpty", response);
			return;
		}

		if(exercise.getSupportsAutomatedScoring()) {
			exercise.setDefaultScoring(AvailableExerciseExerciseScoringMode.AUTOMATED_REVIEW);
		}
		else {
			exercise.setDefaultScoring(AvailableExerciseExerciseScoringMode.MANUAL_REVIEW);
		}

		exercise.setScore(totalScore);

		Integer id = hpc.addAvailableExercise(exercise);
		if(null!=id) {
			MessageGenerator.sendUUIDSuccess(exercise.getUuid(),response);
			return;
		}
		MessageGenerator.sendErrorMessage("Error", response);

	}
}