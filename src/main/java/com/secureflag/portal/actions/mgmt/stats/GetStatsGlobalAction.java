/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.stats;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.regions.Regions;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.Team;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.dto.StatsObject;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.StatsUtils;

public class GetStatsGlobalAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private StatsUtils statsUtils = new StatsUtils();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_FILTER);
		int [] filterOrgIds;
		try{
			Gson parser = new Gson();
			filterOrgIds = parser.fromJson(jsonElement, int[].class);
		} catch( Exception e ){
			filterOrgIds = new int[0];
		}
		Set<Organization> filteredOrg = new HashSet<Organization>();
		if(filterOrgIds.length==0){
			filteredOrg.addAll(user.getManagedOrganizations());
		}
		else{
			for(Integer id : filterOrgIds){
				for(Organization org : user.getManagedOrganizations()){
					if(id.equals(org.getId())){
						filteredOrg.add(org);
					}
				}
			}
		}
		if(user.getRole().equals(Constants.ROLE_TEAM_MANAGER)){
			MessageGenerator.sendErrorMessage("NotAvailable", response);
			return;
		}
		StatsObject stats = new StatsObject();
		List<ExerciseInstance> exerciseInstances = hpc.getReviewedExerciseInstancesWithResultsFlagsUserForStats(filteredOrg);
		
		List<ExerciseInstance> runningInstances = hpc.getAllRunningExerciseInstances(filteredOrg);
		List<ExerciseInstance> pendingInstances = hpc.getManagegementPendingExerciseInstances(filteredOrg);
		List<ExerciseInstance> cancelledInstances = hpc.getManagementCancelledExerciseInstances(filteredOrg);
		List<AvailableExercise> availableExercises = hpc.getAllAvailableExercisesWithFlags(filteredOrg);
		List<User> users = hpc.getManagementAllUsers(filteredOrg);

		stats.setTotalUsers(users.size());
		stats.setTotalExercisesRun(pendingInstances.size() + cancelledInstances.size() + exerciseInstances.size());
		stats.setExercisesRunning(runningInstances.size());
		stats.setAvailableExercises(availableExercises.size());
		Integer issuesNr = 0;
		Set<String> technologies = new HashSet<String>();
		Set<String> issueCategories = new HashSet<String>();
		List<Team> teams = hpc.getManagementAllTeams(filteredOrg);

		for(AvailableExercise exercise : availableExercises){
			issuesNr += exercise.getQuestionsList().size();
			technologies.add(exercise.getTechnology());
			for(Flag flag : exercise.getQuestionsList()){
				issueCategories.add(flag.getCategory());
			}
		}
		Integer feedbackNr = 0;
		Integer totalMinutes = 0;
		Integer awardedTrophies = 0;
		Integer issuesIntroduced = 0;

		for(ExerciseInstance instance : exerciseInstances){
			if(null!=instance.getDuration())
				totalMinutes += instance.getDuration();
			if(null!=instance.getFeedback() && instance.getFeedback()){
				feedbackNr++;
			}
			if(null!=instance.getTrophyAwarded() && instance.getTrophyAwarded()){
				awardedTrophies++;
			}
			if(null!=instance.getNewIssuesIntroduced() && instance.getNewIssuesIntroduced()){
				issuesIntroduced++;
			}	
		}

		stats.setIssues(issuesNr);
		stats.setTechnologies(technologies.size());
		stats.setTeams(teams.size());
		stats.setAwardedTrophies(awardedTrophies);
		stats.setIssueCategories(issueCategories.size());
		stats.setPendingReviews(pendingInstances.size());
		stats.setSubmittedReviews(exerciseInstances.size());
		stats.setCancelledReviews(cancelledInstances.size());
		stats.setTotalFeedback(feedbackNr);
		List<Gateway> activeGws = hpc.getAllActiveGateways();
		Set<Regions> regions = new HashSet<Regions>();
		for(Gateway gw : activeGws) {
			regions.add(gw.getRegion());
		}
		stats.setActiveRegions(regions.size());
		stats.setActiveGateways(activeGws.size());

		stats.setNewIssuesIntroduced(issuesIntroduced);
		stats.setMinutes(totalMinutes);
		if(totalMinutes == 0 || exerciseInstances.isEmpty())
			stats.setAvgExerciseDuration(0.0);
		else
			stats.setAvgExerciseDuration((double) (totalMinutes/(exerciseInstances.size())));

		statsUtils.setTimePerRegion(exerciseInstances,stats);
		statsUtils.setTimePerCategory(exerciseInstances,stats);
		statsUtils.setTimePerTeam(exerciseInstances,stats);

		stats.setIssuesRemediationRate(statsUtils.getRemediationRatePerIssue(exerciseInstances));
		stats.setCategoriesRemediationRate(statsUtils.getRemediationRatePerIssueCategory(exerciseInstances));
		stats.setRegionsRemediationRate(statsUtils.getRemediationRatePerRegion(exerciseInstances));
		stats.setTeamRemediationRate(statsUtils.getRemediationRatePerTeam(exerciseInstances));

		MessageGenerator.sendStatsMessage(stats, response);
	}
}