/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.ECSTaskDefinitionForExerciseInRegion;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class RemoveExerciseFromRegionAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	// a task definition can be removed only if there are no ECSTaskDefinitionForExerciseInRegion referencing it as active
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement idTaskDefElement = json.get(Constants.ACTION_PARAM_TASK_DEFINITION_ID);
		Integer idTaskDef = idTaskDefElement.getAsInt();
		JsonElement idExerciseElement = json.get(Constants.ACTION_PARAM_EXERCISE_ID);
		Integer idExercise = idExerciseElement.getAsInt();
		
		List<ECSTaskDefinitionForExerciseInRegion> list = hpc.getAllTaskDefinitionForExerciseInRegion(idTaskDef);
		for(ECSTaskDefinitionForExerciseInRegion taskDef : list) {
			if(taskDef.getActive()) {
				MessageGenerator.sendErrorMessage("TaskDefinitionActive", response);
				return;
			}
		}
		ECSTaskDefinitionForExerciseInRegion taskDefinitionForDeletion = hpc.getTaskDefinitionForExerciseInRegion(hpc.getExerciseUUIDFromID(idExercise), idTaskDef);
		AWSHelper awsHelper = new AWSHelper();
		Boolean awsResult = awsHelper.removeTaskDefinitionInRegion(taskDefinitionForDeletion.getTaskDefinition().getTaskDefinitionArn(), taskDefinitionForDeletion.getRegion());
		if(!awsResult) {
			logger.warn("Could not deregister task definition "+idTaskDef+" from AWS region "+taskDefinitionForDeletion.getRegion().getName());
			MessageGenerator.sendErrorMessage("TaskDefinitionDeregisterFailed", response);
			return;
		}
		Boolean result = hpc.removeTaskDefinitionInRegion(idTaskDef);
		if(!result) {
			logger.warn("Could not remove task definition "+idTaskDef);
			MessageGenerator.sendErrorMessage("TaskDefinitionRemoveFailed", response);
			return;
		}
		MessageGenerator.sendSuccessMessage(response);
		return;	
	}
}
