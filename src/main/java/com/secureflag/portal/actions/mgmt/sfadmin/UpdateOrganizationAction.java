/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.OrganizationStatus;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class UpdateOrganizationAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement idOrgElement = json.get(Constants.ACTION_PARAM_ID);

		JsonElement nameElement = json.get(Constants.ACTION_PARAM_NAME);
		JsonElement contactEmailElement = json.get(Constants.ACTION_PARAM_EMAIL);
		JsonElement maxUsersElement = json.get(Constants.ACTION_PARAM_MAX_USERS);
		JsonElement allowManualReviewElement = json.get(Constants.ACTION_PARAM_ALLOW_MANUAL_REVIEW);
		JsonElement maxCreditsElement = json.get(Constants.ACTION_PARAM_CREDITS);
		JsonElement statusElement = json.get(Constants.ACTION_PARAM_STATUS);

		Boolean allowManualReview = allowManualReviewElement.getAsBoolean();
		String name = nameElement.getAsString();
		String contactEmail = contactEmailElement.getAsString();
		Integer maxUsers = maxUsersElement.getAsInt();

		Organization o = new Organization();
		
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		o.setDateJoined(c.getTime());
		o.setName(name);
		o.setEmail(contactEmail);
		o.setMaxUsers(maxUsers);
		o.setAllowManualReview(allowManualReview);
		o.setDefaultCredits(maxCreditsElement.getAsInt());
		o.setStatus(OrganizationStatus.getStatusFromStatusCode(statusElement.getAsInt()));
				
		Boolean result = hpc.updateOrganization(idOrgElement.getAsInt(), o);
		if(result){
			MessageGenerator.sendSuccessMessage(response);
		}else{
			MessageGenerator.sendErrorMessage("Failed", response);
		}
	}

}
