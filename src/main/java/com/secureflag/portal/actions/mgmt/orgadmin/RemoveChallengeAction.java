/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.orgadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class RemoveChallengeAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonElement idChallengeEement = json.get(Constants.ACTION_PARAM_ID);
		Integer idChallenge = idChallengeEement.getAsInt();
		Challenge challenge = hpc.getChallengeWithDetails(idChallenge, sessionUser.getManagedOrganizations());
		if(null==challenge) {
			logger.warn("User "+sessionUser.getIdUser()+" not authorized to remove challenge: "+idChallenge);
			MessageGenerator.sendErrorMessage("NotAuthorized", response);
			return;
		}
		Boolean result = hpc.removeChallenge(challenge);
		if(!result) {
			logger.warn("Could not remove challenge "+idChallenge);
			MessageGenerator.sendErrorMessage("ChallengeRemoveFailed", response);
			return;
		}
		MessageGenerator.sendSuccessMessage(response);
		return;	
	}
}
