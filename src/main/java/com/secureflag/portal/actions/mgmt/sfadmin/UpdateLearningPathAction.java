/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.LearningPath;
import com.secureflag.portal.model.LearningPathStatus;
import com.secureflag.portal.model.MarkdownText;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class UpdateLearningPathAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Type listIntegerType = new TypeToken<List<Integer>>() {}.getType();
		Type listStringType = new TypeToken<List<String>>() {}.getType();
		Gson gson = new Gson();

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement idElement = json.get(Constants.ACTION_PARAM_ID);
		JsonElement nameElement = json.get(Constants.ACTION_PARAM_NAME);
		JsonElement detailsElement = json.get(Constants.ACTION_PARAM_DETAILS);
		JsonElement technologyElement = json.get(Constants.ACTION_PARAM_TECHNOLOGY);
		JsonElement difficultyElement = json.get(Constants.ACTION_PARAM_DIFFICULTY);
		JsonElement expirationElement = json.get(Constants.ACTION_PARAM_EXPIRATION);
		JsonElement renewalElement = json.get(Constants.ACTION_PARAM_RENEWAL);
		JsonElement refresherElement = json.get(Constants.ACTION_PARAM_REFRESHER);
		JsonElement statusElement = json.get(Constants.ACTION_PARAM_STATUS);	
		List<String> tags = gson.fromJson(json.get(Constants.ACTION_PARAM_TAGS), new TypeToken<List<String>>(){}.getType());

		Set<Organization> pathOrganizations = new HashSet<Organization>();
		List<String> pathExercises = new LinkedList<String>();

		LearningPath learningPath = hpc.getLearningPath(idElement.getAsInt());

		if(learningPath.getFromHub()) {
			List<Integer> orgsInt = gson.fromJson(json.get(Constants.ACTION_PARAM_ORGS_LIST), listIntegerType);
			for(Integer orgId : orgsInt) {
				Organization organization = hpc.getOrganizationById(orgId);
				if(null!=organization)
					pathOrganizations.add(organization);
			}
			learningPath.setOrganizations(pathOrganizations);

			List<String> exercises = learningPath.getExercises();
			for(String uuid : exercises) {
				AvailableExercise tmpExercise = hpc.getAvailableExercise(uuid);
				if(null!=tmpExercise) {
					pathExercises.add(tmpExercise.getUuid());
					for(Organization o : pathOrganizations) {
						if(hpc.addAvailableExerciseForOrganization(o, tmpExercise)) {
							logger.info("Enabling exercise "+tmpExercise.getId()+" for organization "+o.getId()+" due to being added to training plan "
									+ technologyElement.getAsString()+" "+nameElement.getAsString()+" "+difficultyElement.getAsString());
						}
					}
				}
			}
			Integer id = hpc.updateLearningPath(learningPath);
			if(null!=id) {
				MessageGenerator.sendUUIDSuccess(learningPath.getUuid(),response);
			}
			else {
				MessageGenerator.sendErrorMessage("PathNotSaved", response);
			}
			return;
		}
		
		if(!learningPath.getName().equals(nameElement.getAsString())) {
			LearningPath existingPath = hpc.getLearningPath(nameElement.getAsString(),difficultyElement.getAsString(),technologyElement.getAsString());
			if (existingPath != null) {
				MessageGenerator.sendErrorMessage("NameNotAvailable", response);
				return;
			} 
		}
		try {
			List<Integer> orgsInt = gson.fromJson(json.get(Constants.ACTION_PARAM_ORGS_LIST), listIntegerType);
			for(Integer orgId : orgsInt) {
				Organization organization = hpc.getOrganizationById(orgId);
				if(null!=organization)
					pathOrganizations.add(organization);
			}
			List<String> exercises = gson.fromJson(json.get(Constants.ACTION_PARAM_EXERCISE_LIST), listStringType);
			for(String uuid : exercises) {
				AvailableExercise tmpExercise = hpc.getAvailableExercise(uuid);
				if(null!=tmpExercise) {
					pathExercises.add(tmpExercise.getUuid());
					for(Organization o : pathOrganizations) {
						if(hpc.addAvailableExerciseForOrganization(o, tmpExercise)) {
							logger.info("Enabling exercise "+tmpExercise.getId()+" for organization "+o.getId()+" due to being added to training plan "
									+ technologyElement.getAsString()+" "+nameElement.getAsString()+" "+difficultyElement.getAsString());
						}
					}
				}
			}
		}catch(Exception e) {
			MessageGenerator.sendErrorMessage("ListsParseError", response);
			return;
		}
		MarkdownText pathDescription = gson.fromJson(json.get(Constants.ACTION_PARAM_DESCRIPTION).getAsJsonObject(),MarkdownText.class);
		if(null==pathDescription) {
			MessageGenerator.sendErrorMessage("DescriptionEmpty", response);
			return;
		}
		learningPath.setDescription(pathDescription);
		learningPath.setDetails(detailsElement.getAsString());
		learningPath.setExercises(pathExercises);
		learningPath.setLastUpdate(new Date());
		learningPath.setTags(tags);
		learningPath.setFromHub(false);
		learningPath.setName(nameElement.getAsString());
		learningPath.setTechnology(technologyElement.getAsString());
		learningPath.setDifficulty(difficultyElement.getAsString());
		learningPath.setAllowRenewal(renewalElement.getAsBoolean());
		if(learningPath.getAllowRenewal())
			learningPath.setRefresherPercentage(refresherElement.getAsInt());
		else
			learningPath.setRefresherPercentage(null);
		learningPath.setMonthsExpiration(expirationElement.getAsInt());
		learningPath.setOrganizations(pathOrganizations);
		learningPath.setStatus(LearningPathStatus.getStatusFromStatusCode(statusElement.getAsInt()));

		Integer id = hpc.updateLearningPath(learningPath);
		if(null!=id) {
			MessageGenerator.sendUUIDSuccess(learningPath.getUuid(),response);
		}
		else {
			MessageGenerator.sendErrorMessage("PathNotSaved", response);
		}

	}
}