/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.services.securitytoken.model.Credentials;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.hub.HubRestFacade;
import com.secureflag.portal.messages.json.MessageGenerator;

public class GetHubS3CredentialsAction extends SFAction {

	private HubRestFacade hubRestClient = new HubRestFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Credentials credentials = hubRestClient.getHubS3Credentials();
		if(null!=credentials && null!=credentials.getAccessKeyId())
			MessageGenerator.sendAWSTempCredentials(credentials,response);
		else
			MessageGenerator.sendErrorMessage("Error", response);

	}

}
