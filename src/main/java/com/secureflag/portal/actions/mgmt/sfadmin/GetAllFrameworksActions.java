package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Framework;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetAllFrameworksActions extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		List<Framework> frameworks = hpc.getAllFrameworks();
		MessageGenerator.sendFrameworkListMessage(frameworks, response);
		
	}

}
