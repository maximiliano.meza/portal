/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.regions.Regions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.gateway.GatewayHelper;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class RemoveImageFromContainerInstanceAction extends SFAction {

	private GatewayHelper gwHelper = new GatewayHelper();
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private AWSHelper awsHelper = new AWSHelper();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		if(SFConfig.isImageSyncModuleAvailable()) {

			JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
			JsonElement imageElement = json.get(Constants.ACTION_PARAM_IMAGE);
			String image = imageElement.getAsString();

			JsonElement instanceElement = json.get(Constants.ACTION_PARAM_CONTAINER_INSTANCE);
			String instance = instanceElement.getAsString();

			JsonElement regionElement = json.get(Constants.ACTION_PARAM_REGION);
			String region = regionElement.getAsString();
			Regions awsRegion = Regions.fromName(region.toLowerCase().replaceAll("_", "-"));
			Gateway gateway = hpc.getGatewayForRegion(awsRegion);

			List<String> agents = awsHelper.listInstanceAgents(awsRegion);

			for(String agent : agents) {
				if(agent.indexOf(instance)>-1) {
					instance = agent;
				}
			}

			if(gwHelper.removeImage(gateway.getFqdn(), instance, image)) {
				logger.debug("Sent request to GW "+gateway.getFqdn()+" to remove image "+image+" from container instance "+instance);
				MessageGenerator.sendSuccessMessage(response);
			}
			else {
				logger.error("Failed to request to GW "+gateway.getFqdn()+" to remove image "+image+" from container instance "+instance);
				MessageGenerator.sendErrorMessage("CouldNotRemove", response);
			}
			return;
		}
		MessageGenerator.sendErrorMessage("NotAvailable", response);
	}
}
