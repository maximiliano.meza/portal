/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.orgadmin;

import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Team;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class AddUsersToTeamAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer teamId = jsonElement.getAsInt();
		Team team = hpc.getTeam(teamId,user.getManagedOrganizations());
		if(null==team){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		
		Type listType = new TypeToken<List<String>>() {}.getType();
		Gson gson = new Gson();
		List<String> usernames = gson.fromJson(json.get(Constants.ACTION_PARAM_USERNAME_LIST), listType);
		
		if(user.getRole().equals(Constants.ROLE_TEAM_MANAGER)){
			boolean isManager = false;
			for(User u : team.getManagers()){
				if(u.getIdUser().equals(user.getIdUser())){
					isManager = true;
					break;
				}
			}
			if(!isManager){
				MessageGenerator.sendErrorMessage("NotFound", response);
				return;
			}
		}
		Integer nrAdded = 0;
		for(String u : usernames){
			User usr = hpc.getUserFromUsername(u,user.getManagedOrganizations());
			if(usr.getTeam()==null){
				hpc.addUserToTeam(usr,team);
				nrAdded++;
			}
		}
		MessageGenerator.sendNumberUpdatedMessage(nrAdded, response);
	}
}