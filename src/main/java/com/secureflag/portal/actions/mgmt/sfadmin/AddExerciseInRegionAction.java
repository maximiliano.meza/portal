/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.regions.Regions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.ECSTaskDefinition;
import com.secureflag.portal.model.ECSTaskDefinitionForExerciseInRegion;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class AddExerciseInRegionAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	

		JsonElement idExerciseElement = json.get(Constants.ACTION_PARAM_EXERCISE_ID);

		JsonElement taskDefinitionNameElement = json.get(Constants.ACTION_PARAM_TASK_DEFINITION_NAME);
		JsonElement containerNameElement = json.get(Constants.ACTION_PARAM_CONTAINER_NAME);
		JsonElement repositoryImageUrlElement = json.get(Constants.ACTION_PARAM_REPO_URL);
		JsonElement softMemoryLimitElement= json.get(Constants.ACTION_PARAM_SOFT_MEMORY);
		JsonElement regionElement = json.get(Constants.ACTION_PARAM_REGION);
		JsonElement hardMemoryLimitElement = json.get(Constants.ACTION_PARAM_HARD_MEMORY);
		JsonElement statusElement = json.get(Constants.ACTION_PARAM_STATUS);

		String taskDefinitionName = taskDefinitionNameElement.getAsString();
		String containerName = containerNameElement.getAsString();
		
		String repositoryImageUrl = repositoryImageUrlElement.getAsString();
		String region = regionElement.getAsString();
		Integer softMemoryLimit = softMemoryLimitElement.getAsInt(); 
		Integer hardMemoryLimit = hardMemoryLimitElement.getAsInt();

		Boolean active = statusElement.getAsBoolean();
		Integer idExercise = idExerciseElement.getAsInt();

		AvailableExercise exercise = hpc.getAvailableExercise(hpc.getExerciseUUIDFromID(idExercise));
		if(null==exercise) {
			MessageGenerator.sendErrorMessage("ExerciseNotFound", response);
			logger.error("Exercise "+idExercise+" not found for user "+sessionUser.getIdUser());
			return;	
		}

		Regions awsRegion = null;
		try{
			awsRegion = Regions.valueOf(region);
		} catch(Exception e){
			MessageGenerator.sendErrorMessage("RegionNotFound", response);
			logger.error("Region "+region+" not found for user "+sessionUser.getIdUser());
			return;	
		}

		ECSTaskDefinition ecsTask = new ECSTaskDefinition();

		ecsTask.setRegion(awsRegion);
		ecsTask.setContainerName(containerName);
		ecsTask.setTaskDefinitionName(taskDefinitionName);
		ecsTask.setHardMemoryLimit(hardMemoryLimit);
		ecsTask.setSoftMemoryLimit(softMemoryLimit);
		ecsTask.setRepositoryImageUrl(repositoryImageUrl);
		ecsTask.setUpdateDate(new Date());

		AWSHelper awsHelper = new AWSHelper();
		String arn = awsHelper.registerECSTaskDefinition(ecsTask, sessionUser);
		if(null!=arn) {
			String fargateArn = awsHelper.registerFargateTaskDefinition(ecsTask, sessionUser);
			if(null==fargateArn) {
				ecsTask.setFargateDefinitionArn(null);
				ecsTask.setFargateDefinitionName(null);
				logger.error("Failed to create Fargate TaskDefinition for user "+sessionUser.getIdUser());
			}
			else {
				ecsTask.setFargateDefinitionArn(fargateArn);
				ecsTask.setFargateDefinitionName(taskDefinitionName+"-fargate");
			}
				
			ecsTask.setTaskDefinitionArn(arn);
			String nameWithRevision = arn.split("/")[(arn.split("/").length)-1];
			if(null!=nameWithRevision && !nameWithRevision.equals(""))
				ecsTask.setTaskDefinitionName(nameWithRevision);
			ECSTaskDefinitionForExerciseInRegion ecsTaskForExercise = new ECSTaskDefinitionForExerciseInRegion();
			ecsTaskForExercise.setExercise(exercise);
			ecsTaskForExercise.setRegion(awsRegion);
			ecsTaskForExercise.setTaskDefinition(ecsTask);
			ecsTaskForExercise.setActive(active);
			Integer id = hpc.addECSTaskDefinitionForExerciseInRegion(ecsTaskForExercise);
			if(null==id) {
				MessageGenerator.sendErrorMessage("Error", response);
				logger.error("New ECSTaskDefinitionForExerciseInRegion could not be saved for user "+sessionUser.getIdUser());
				return;	
			}
			MessageGenerator.sendSuccessMessage(response);
			return;
		}
		MessageGenerator.sendErrorMessage("Error", response);
		logger.error("New ECSTaskDefinitionForExerciseInRegion could not be saved for user "+sessionUser.getIdUser());
	}
}