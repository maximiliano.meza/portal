/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.KBStatus;
import com.secureflag.portal.model.KBTechnologyItem;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class UpdateTechnologyAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		Gson gson = new Gson();  
		KBTechnologyItem stack = gson.fromJson(json.get(Constants.JSON_ATTRIBUTE_OBJ).getAsJsonObject(),KBTechnologyItem.class);
		
		
		if(stack.getUuid()==null) {
			logger.warn("Could not update Technology as uuid was not provided");
			MessageGenerator.sendErrorMessage("UuidNotProvided", response);
			return;
		}
		KBTechnologyItem dbStack = hpc.getTechnologyStackByUUID(stack.getUuid());
		if(null==dbStack) {
			logger.warn("Could not update Technology "+stack.getUuid()+", not found");
			MessageGenerator.sendErrorMessage("VulnerabilityUpdateFailed", response);
			return;
		}
		if(dbStack.getFromHub()) {
			logger.warn("Could not update Technology "+stack.getTechnology()+" as it comes from the Hub");
			MessageGenerator.sendErrorMessage("VulnerabilityUpdateFailed", response);
			return;
		}
		if(stack.getImageUrl() != null && stack.getTechnology() != null && stack.getMd() != null) {
			
			if(stack.getVariant()==null || stack.getVariant().equals(""))
				stack.setVariant("Default");
			//WORKAROUND for TUI.Editor
			stack.getMd().setText(stack.getMd().getText().replaceAll("Â", "").replaceAll("Ã", ""));
			stack.getMd().setId(null);
			stack.setStatus(KBStatus.AVAILABLE);
			stack.setLastUpdate(new Date());
			stack.setFromHub(false);
			stack.setId(dbStack.getId());
			
			Boolean result = hpc.updateTechnology(stack);
			if(!result) {
				logger.warn("Could not update Technology "+stack.getTechnology());
				MessageGenerator.sendErrorMessage("TechnologyUpdateFailed", response);
				return;
			}
			MessageGenerator.sendUUIDSuccess(stack.getUuid(),response);
			return;	
		}
		
		MessageGenerator.sendErrorMessage("TechnologyUpdateFailed", response);
	}

}
