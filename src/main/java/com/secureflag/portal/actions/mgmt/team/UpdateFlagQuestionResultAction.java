/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.team;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserResultComment;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class UpdateFlagQuestionResultAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private NotificationsHelper notificationsHelper = new NotificationsHelper();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		Integer exerciseInstanceId = json.get(Constants.ACTION_PARAM_ID).getAsInt();
		Integer updatedScore = json.get(Constants.ACTION_PARAM_SCORE).getAsInt();
		String selfCheckName = json.get(Constants.ACTION_PARAM_NAME).getAsString();
		String updatedComment = json.get(Constants.ACTION_PARAM_COMMENT).getAsString();
		Integer updatedStatus = json.get(Constants.ACTION_PARAM_STATUS).getAsInt();

		ExerciseInstance instance = hpc.getManagementExerciseInstance(exerciseInstanceId,user.getManagedOrganizations());
		if(null==instance){
			MessageGenerator.sendErrorMessage("NOT_FOUND", response);
			return;
		}
		if(user.getRole().equals(Constants.ROLE_TEAM_MANAGER)){
			List<User> users = hpc.getUsersInTeamManagedBy(user);
			if(!users.contains(instance.getUser())){
				MessageGenerator.sendErrorMessage("NotFound", response);
				return;
			}
		}
		Integer previousScore = instance.getScore().getResult();
		Integer updatedTotalScore = 0;
		for(ExerciseInstanceResult res : instance.getResults()) {
			if(res.getName().equals(selfCheckName)) {
				res.setLastChange(new Date());
				res.setVerified(true);
				res.setStatus(ExerciseInstanceResultStatus.getStatusFromStatusCode(updatedStatus));
				res.setScore(updatedScore);
				UserResultComment e = new UserResultComment();
				e.setDate(new Date());
				e.setFromAdmin(true);
				e.setFromUserId(user.getIdUser());
				e.setText(updatedComment);
				res.getUserReportedComplaints().add(e);
			}
			updatedTotalScore += res.getScore();
		}
		instance.getScore().setResult(updatedTotalScore);
		if(!updatedTotalScore.equals(previousScore)) {
			instance.getUser().setScore(instance.getUser().getScore() - previousScore + instance.getScore().getTotal());
		}
		notificationsHelper.addUpdateReviewNotification(instance.getUser(), instance);

		instance.setStatus(ExerciseInstanceStatus.REVIEWED_MODIFIED);
		hpc.updateExerciseInstance(instance);
		MessageGenerator.sendSuccessMessage(response);
		return;
	}
}