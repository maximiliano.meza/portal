/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.team;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.AchievementType;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.ExerciseInstanceType;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.Trophy;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAchievement;
import com.secureflag.portal.model.dto.ManualReviewObject;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoPostReviewAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private NotificationsHelper notificationsHelper = new NotificationsHelper();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_ATTRIBUTE_JSON);
		JsonObject jsonObj = json.getAsJsonObject(Constants.JSON_ATTRIBUTE_OBJ);

		Gson gson = new Gson();  
		ManualReviewObject review = gson.fromJson(jsonObj, ManualReviewObject.class);  

		if(review.getAwardTrophy()==null || review.getId()==null || review.getNewIssuesIntroduced()==null || review.getTotalScore()==null || review.getReview()==null || review.getReview().isEmpty()){
			logger.error("Invalid data supplied for review from user "+user.getIdUser());
			MessageGenerator.sendErrorMessage("InvalidData", response);
			return;
		}	

		Integer exerciseInstanceId = review.getId();
		Boolean awardTrophy = review.getAwardTrophy();
		Boolean newIssuesIntroduced = review.getNewIssuesIntroduced();
		String newIssuesIntroducedText = review.getNewIssuesIntroducedText();

		ExerciseInstance reviewedInstance = hpc.getManagementCompletedExerciseInstance(exerciseInstanceId,user.getManagedOrganizations());
		if(null==reviewedInstance){
			logger.error("Exercise "+exerciseInstanceId+" not found for user "+user.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		if(user.getRole().equals(Constants.ROLE_TEAM_MANAGER)){
			List<User> users = hpc.getUsersInTeamManagedBy(user);
			if(!users.contains(reviewedInstance.getUser())){
				MessageGenerator.sendErrorMessage("NotFound", response);
				return;
			}
		}
		if(reviewedInstance.getStatus().equals(ExerciseInstanceStatus.REVIEWED) || reviewedInstance.getStatus().equals(ExerciseInstanceStatus.REVIEWED_MODIFIED) || reviewedInstance.getStatus().equals(ExerciseInstanceStatus.CANCELLED)){
			logger.error("Exercise "+exerciseInstanceId+" already reviewer for user "+user.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		if(reviewedInstance.getResults().isEmpty()){
			for(ExerciseInstanceResult rev : review.getReview()){
				ExerciseInstanceResult res = new ExerciseInstanceResult();
				res.setName(rev.getName());
				res.setComment(rev.getComment());
				res.setVerified(true);
				res.setStatus(rev.getStatus());
				res.setCheckerStatus(rev.getCheckerStatus());
				res.setLastChange(new Date());
				res.setScore(rev.getScore());
				res.setAutomated(false);
				Flag f = getFlagForResult(reviewedInstance,rev.getName());
				res.setCategory(f.getCategory());
				res.setFlagTitle(f.getTitle());
				reviewedInstance.getResults().add(res);
			}
		}
		else {
			for(ExerciseInstanceResult orig : reviewedInstance.getResults()){
				for(ExerciseInstanceResult rev : review.getReview()){
					if(rev.getName().equals(orig.getName())){
						orig.setComment(rev.getComment());
						orig.setVerified(true);
						orig.setLastChange(new Date());
						orig.setStatus(rev.getStatus());
						orig.setScore(rev.getScore());
						orig.setAutomated(false);
					}
				}
			}
		}
		reviewedInstance.setResultsAvailable(true);
		reviewedInstance.setStatus(ExerciseInstanceStatus.REVIEWED);
		reviewedInstance.setTrophyAwarded(review.getAwardTrophy());
		reviewedInstance.setNewIssuesIntroduced(newIssuesIntroduced);
		if(newIssuesIntroduced){
			reviewedInstance.setNewIssuesIntroducedText(newIssuesIntroducedText);
		}
		reviewedInstance.setReviewer(user.getIdUser());
		reviewedInstance.setReviewedDate(new Date());
		if(reviewedInstance.getType().equals(ExerciseInstanceType.CHALLENGE)) {
			Challenge c = hpc.getChallengeFromIdLight(reviewedInstance.getChallengeId());
			c.setLastActivity(new Date());
			hpc.updateChallenge(c);
		}
		notificationsHelper.addCompletedReviewNotification(reviewedInstance.getUser(), reviewedInstance);

		List<ExerciseInstance> userRunExercises = hpc.getCompletedExerciseInstancesWithResultsForUser(reviewedInstance.getUser().getIdUser(),reviewedInstance.getExercise().getUuid());
		boolean alreadyRun = false;
		boolean alreadySolved = false;
		List<ExerciseInstanceResult> solvedResults = new LinkedList<ExerciseInstanceResult>();
		reviewedInstance.setSolved(isSolved(reviewedInstance.getResults()));

		for(ExerciseInstance runEx : userRunExercises) {
			if(!runEx.getIdExerciseInstance().equals(reviewedInstance.getIdExerciseInstance()) && (runEx.getStatus().equals(ExerciseInstanceStatus.REVIEWED) || runEx.getStatus().equals(ExerciseInstanceStatus.REVIEWED_MODIFIED) || runEx.getStatus().equals(ExerciseInstanceStatus.AUTOREVIEWED)) ) {
				alreadyRun = true;
				if(runEx.isSolved()) {
					alreadySolved = true;
				}
				for(ExerciseInstanceResult result : runEx.getResults()) {
					if(result.getStatus()!=null && (result.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || !result.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
						solvedResults.add(result);
					}
				}
			}

		}
		User dbUser = hpc.getUserFromUserId(reviewedInstance.getUser().getIdUser());

		if(alreadyRun && !alreadySolved){
			List<ExerciseInstanceResult> uniqueSolved = new LinkedList<ExerciseInstanceResult>();
			for(ExerciseInstanceResult instanceRes: reviewedInstance.getResults()) {
				Boolean previouslySolved = false;
				for(ExerciseInstanceResult res : solvedResults) {
					if(res.getFlagTitle().equals(instanceRes.getFlagTitle())) {
						if(!isAlreadyPresent(uniqueSolved,res)){
							uniqueSolved.add(res);
						}
						previouslySolved = true;
						break;
					}
				}
				if(!previouslySolved && (instanceRes.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || instanceRes.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
					uniqueSolved.add(instanceRes);
				}
				// no points if the flag was previously solved.
				else if(previouslySolved && reviewedInstance.getChallengeId()==null && (instanceRes.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || instanceRes.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
					instanceRes.setScore(0);
					instanceRes.setPreviouslySolved(true);
				}
			}
			// aggregate previous and current NOT_VULNERABLE/EXPLOITED results to determine exercise solved
			if(reviewedInstance.getResults().size()==uniqueSolved.size()) {
				reviewedInstance.setSolved(true);
			}
		}
		Integer totalScore = 0;
		for(ExerciseInstanceResult r : reviewedInstance.getResults()){
			totalScore += r.getScore();
		}
		reviewedInstance.getScore().setResult(totalScore);
		dbUser.setScore(dbUser.getScore() + reviewedInstance.getScore().getResult());
		
		checkAndAttributeTrophy(reviewedInstance,awardTrophy);

		hpc.updateUserInfo(dbUser);
		hpc.updateExerciseInstance(reviewedInstance);
		MessageGenerator.sendSuccessMessage(response);

		logger.debug("Review submitted for exercise "+exerciseInstanceId+" from reviewer "+user.getIdUser());

	}

	private Flag getFlagForResult(ExerciseInstance instance, String name) {
		for(Flag flag : instance.getAvailableExercise().getQuestionsList()){
			for(FlagQuestion fq : flag.getFlagQuestionList()) {
				if(fq.getSelfCheckAvailable() && fq.getSelfCheck().getName().equals(name)) {
					return flag;
				}
			}
		}
		return null;
	}

	private Boolean isAlreadyPresent(List<ExerciseInstanceResult> uniqueSolved, ExerciseInstanceResult r) {
		for(ExerciseInstanceResult ur : uniqueSolved) {
			if(ur.getFlagTitle().equals(r.getFlagTitle())) {
				return true;
			}
		}
		return false;
	}

	private Boolean isSolved(List<ExerciseInstanceResult> results) {
		if(null == results || results.isEmpty())
			return false;
		for(ExerciseInstanceResult result : results) {
			if(!result.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !result.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED)) {
				return false;
			}
		}
		return true;
	}
	private void checkAndAttributeTrophy(ExerciseInstance instance, Boolean awardTrophy) {
		if(instance.getScore().getResult() >= instance.getScore().getTotal() || awardTrophy) {
			List<UserAchievement> userAchievedTrophy = hpc.getAllAchievementsForUser(instance.getUser().getIdUser());
			boolean alreadyAchieved = false;
			for(UserAchievement trophy : userAchievedTrophy){
				if(trophy.getAchievement().getName().equals(instance.getAvailableExercise().getTrophyName())){
					alreadyAchieved = true;
					break;
				}
			}
			if(!alreadyAchieved){
				UserAchievement t = new UserAchievement();
				t.setDate(new Date());
				t.setUser(instance.getUser());
				Trophy achievement = new Trophy();
				achievement.setName(instance.getAvailableExercise().getTrophyName());
				achievement.setType(AchievementType.TROPHY);
				achievement.setTechnology(instance.getAvailableExercise().getTechnology());
				t.setAchievement(achievement);
				hpc.managementAddAchievedTrophy(t);
				notificationsHelper.addTrophyNotification(instance.getUser(),achievement);
				instance.setTrophyAwarded(true);
			}
		}
	}

}