/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin.validators;

import org.owasp.esapi.ESAPI;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.validators.IClassValidator;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.model.dto.ValidatedData;

public class AddExerciseInRegionValidator implements IClassValidator {

	@Override
	public ValidatedData doValidation(JsonObject json, ValidatedData validatedData) {

		JsonElement taskDefinitionNameElement = json.get(Constants.ACTION_PARAM_TASK_DEFINITION_NAME);
		JsonElement containerNameElement = json.get(Constants.ACTION_PARAM_CONTAINER_NAME);
		JsonElement softMemoryLimitElement= json.get(Constants.ACTION_PARAM_SOFT_MEMORY);
		JsonElement hardMemoryLimitElement = json.get(Constants.ACTION_PARAM_HARD_MEMORY);

		String taskDefinitionName = taskDefinitionNameElement.getAsString();
		try {
			if(!ESAPI.validator().isValidInput("AddExerciseInRegionValidator", taskDefinitionName, "ECSSafe", 255, false)){
				validatedData.setWithErrors(true);
				validatedData.addError("taskDefinitionName" + ":" + taskDefinitionName + " NOT pass AddExerciseInRegionValidator");
				return validatedData;
			}
			String containerName = containerNameElement.getAsString();
			if(!ESAPI.validator().isValidInput("AddExerciseInRegionValidator", containerName, "ECSSafe", 255, false)){
				validatedData.setWithErrors(true);
				validatedData.addError("containerName" + ":" + containerName + " NOT pass AddExerciseInRegionValidator");
				return validatedData;
			}
			if(!ESAPI.validator().isValidInteger("AddExerciseInRegionValidator", softMemoryLimitElement.getAsString(), Constants.MIN_SOFT_MEMORY_LIMIT, Constants.MAX_SOFT_MEMORY_LIMIT, false)){
				validatedData.setWithErrors(true);
				validatedData.addError("softMemoryLimit" + ":" + softMemoryLimitElement.getAsString() + " NOT pass AddExerciseInRegionValidator");
				return validatedData;
			}
			if(!ESAPI.validator().isValidInteger("AddExerciseInRegionValidator", hardMemoryLimitElement.getAsString(), Constants.MIN_HARD_MEMORY_LIMIT, Constants.MAX_HARD_MEMORY_LIMIT, false)){
				validatedData.setWithErrors(true);
				validatedData.addError("hardMemoryLimit" + ":" + hardMemoryLimitElement.getAsString() + " NOT pass AddExerciseInRegionValidator");
				return validatedData;
			}
			return validatedData;
		}catch(Exception e) {
			return validatedData;
		}
	}
}