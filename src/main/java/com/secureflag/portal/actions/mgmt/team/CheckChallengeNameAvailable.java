/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.team;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class CheckChallengeNameAvailable extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
 
		JsonElement nameElement = json.get(Constants.ACTION_PARAM_NAME);
		JsonElement orgElement = json.get(Constants.ACTION_PARAM_ORG_ID);

		String name = nameElement.getAsString();
		Integer org = orgElement.getAsInt();

		Challenge existingChallenge = hpc.getChallengeFromName(name,org);
		if (existingChallenge != null) {
			MessageGenerator.sendAvailable(false, response);
		} else {
			MessageGenerator.sendAvailable(true, response);
		}
	}

}
