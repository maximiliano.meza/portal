/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.team;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetAvailableExerciseDetailsAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_UUID);
		String uuidExercise = jsonElement.getAsString();
		AvailableExercise exercise;
		if(user.getRole().equals(Constants.ROLE_SF_ADMIN)) {
			exercise =  hpc.getAvailableExerciseDetailsMgmt(uuidExercise);
		}
		else {
			exercise =  hpc.getAvailableExerciseDetailsMgmt(uuidExercise,user.getManagedOrganizations());
		}
		if(null!=exercise) {
			for(Flag f : exercise.getQuestionsList()) {
				f.setKb(hpc.getVulnerabilityKBItemByUUID(f.getKb().getUuid(),exercise.getTechnology()));
			}
			MessageGenerator.sendExerciseInfoMessageWithHints(exercise,response);
		}
		else{
			MessageGenerator.sendErrorMessage("NotFound", response);
			logger.error("User "+user.getIdUser()+" requested AvailableExercise "+uuidExercise+" details, exercise is inactive/not found");
		}
	}
}
