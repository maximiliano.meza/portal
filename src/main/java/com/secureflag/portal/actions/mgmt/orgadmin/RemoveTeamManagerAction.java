/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.orgadmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Team;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class RemoveTeamManagerAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonTeamId = json.get(Constants.ACTION_PARAM_TEAM_ID);
		Integer teamId = jsonTeamId.getAsInt();
		JsonElement jsonUser = json.get(Constants.ACTION_PARAM_USERNAME);
		String username = jsonUser.getAsString();

		Team team = hpc.getTeam(teamId,sessionUser.getManagedOrganizations());
		if(null==team){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		User user = hpc.getUserFromUsername(username,sessionUser.getManagedOrganizations());
		if(null!=user){
			hpc.removeFromTeamManager(team.getIdTeam(),user);
			List<Team> teams = hpc.getTeamsManagedBy(user);
			if(teams.isEmpty() && user.getRole().equals(Constants.ROLE_TEAM_MANAGER)) {
				user.setRole(Constants.ROLE_USER);
				hpc.updateUserInfo(user);
			}
			MessageGenerator.sendSuccessMessage(response);
		}
		else{
			MessageGenerator.sendErrorMessage("NotFound", response);
		}
	}
}
