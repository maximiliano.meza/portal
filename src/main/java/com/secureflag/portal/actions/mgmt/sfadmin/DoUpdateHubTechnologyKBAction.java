/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.hub.HubRestFacade;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.KBStatus;
import com.secureflag.portal.model.KBTechnologyItem;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoUpdateHubTechnologyKBAction extends SFAction {
	
	private HubRestFacade hubRestClient = new HubRestFacade();
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		
		JsonElement idElement = json.get(Constants.ACTION_PARAM_UUID);
		String uuid = idElement.getAsString();
		KBTechnologyItem hubStack  = hubRestClient.getTechnologyKBItem(uuid);
		KBTechnologyItem dbStack = hpc.getTechnologyStackByUUID(uuid);
		
		if(null!=dbStack && null!=dbStack.getLastUpdate() && null!=hubStack && null!=hubStack.getLastUpdate()) {
			if(DateUtils.truncate(hubStack.getLastUpdate(), Calendar.SECOND).after(DateUtils.truncate(dbStack.getLastUpdate(), Calendar.SECOND))) {
				hubStack.setFromHub(true);
				hubStack.setStatus(KBStatus.AVAILABLE);
				hpc.updateTechnology(hubStack.getUuid(),hubStack);
				MessageGenerator.sendSuccessMessage(response);
			}
			else {
				MessageGenerator.sendErrorMessage("AlreadyUpdated", response);
			}
		}
		else {
			MessageGenerator.sendErrorMessage("NotFound", response);
		}
	}
}
