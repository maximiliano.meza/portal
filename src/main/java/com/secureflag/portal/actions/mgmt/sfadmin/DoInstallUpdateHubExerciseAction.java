/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.hub.HubRestFacade;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.AvailableExerciseQuality;
import com.secureflag.portal.model.AvailableExerciseStatus;
import com.secureflag.portal.model.DownloadQueueItem;
import com.secureflag.portal.model.DownloadQueueItemStatus;
import com.secureflag.portal.model.DownloadQueueItemType;
import com.secureflag.portal.model.ExerciseInstanceVotingScore;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.Framework;
import com.secureflag.portal.model.KBStatus;
import com.secureflag.portal.model.KBTechnologyItem;
import com.secureflag.portal.model.KBVulnerabilityItem;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoInstallUpdateHubExerciseAction extends SFAction {

	private HubRestFacade hubRestClient = new HubRestFacade();
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement uuidElement = json.get(Constants.ACTION_PARAM_UUID);

		AvailableExercise oldExercise = hpc.getAvailableExerciseDetailsMgmt(uuidElement.getAsString());
		AvailableExercise exercise = null;
		if(null!=oldExercise)
			exercise = hubRestClient.installExercise(uuidElement.getAsString(),DownloadQueueItemType.UPDATE);
		else
			exercise = hubRestClient.installExercise(uuidElement.getAsString(),DownloadQueueItemType.INSTALLATION);

		if(null==exercise || null==exercise.getUuid()) {
			MessageGenerator.sendErrorMessage("error", response);
			return;
		}

		if(null!=oldExercise && null!=oldExercise.getLastUpdate() && null!=oldExercise.getImage().getUpdateDate()) {
			if(! DateUtils.truncate(exercise.getLastUpdate(), Calendar.SECOND).after(DateUtils.truncate(oldExercise.getLastUpdate(), Calendar.SECOND)) && !DateUtils.truncate(exercise.getImage().getUpdateDate(), Calendar.SECOND).after(DateUtils.truncate(oldExercise.getImage().getUpdateDate(), Calendar.SECOND))) {
				logger.debug("Exercise "+exercise.getUuid()+" is already installed and up to date, skipping...");
				MessageGenerator.sendExerciseInfoMessage(exercise, response);
				return;
			}
		}

		if(null==exercise.getFramework() || "".equals(exercise.getFramework()))
			exercise.setFramework("Default");

		exercise.setStatus(AvailableExerciseStatus.DOWNLOAD_QUEUE);
		exercise.setFromHub(true);
		exercise.setQuality(AvailableExerciseQuality.HUB_DEFAULT);


		KBTechnologyItem dbStack = hpc.getTechnologyStackByUUID(exercise.getStack().getUuid());
		if(null!=dbStack && null!=dbStack.getLastUpdate()) {
			if(DateUtils.truncate(exercise.getStack().getLastUpdate(), Calendar.SECOND).after(DateUtils.truncate(dbStack.getLastUpdate(), Calendar.SECOND))) {
				exercise.getStack().setFromHub(true);
				exercise.getStack().setStatus(KBStatus.AVAILABLE);
				exercise.setStack(hpc.updateTechnology(exercise.getStack().getUuid(),exercise.getStack()));
			}
			else {
				exercise.setStack(dbStack);
			}
		}
		else {
			exercise.getStack().setStatus(KBStatus.AVAILABLE);
			exercise.getStack().setFromHub(true);
			exercise.getStack().getMd().setId(null);
			exercise.getStack().setId(null);
		}

		exercise.getInformation().setId(null);
		exercise.getSolution().setId(null);
		exercise.getImage().setId(null);

		if(null==oldExercise) {
			exercise.setVotingScore(0);
			exercise.setVotingScoreList(new LinkedList<ExerciseInstanceVotingScore>());
			exercise.setVotingScoreNumbers(0);
			exercise.setInternetOutboundAllowed(false);
			exercise.setClipboardAllowed(true);
		}
		else {
			exercise.setVotingScore(oldExercise.getVotingScore());
			exercise.setVotingScoreList(oldExercise.getVotingScoreList());
			exercise.setVotingScoreNumbers(oldExercise.getVotingScoreNumbers());
			exercise.setInternetOutboundAllowed(oldExercise.getInternetOutboundAllowed());
			exercise.setClipboardAllowed(oldExercise.getClipboardAllowed());
		}
		for(Flag f : exercise.getQuestionsList()) {
			f.setId(null);
			KBVulnerabilityItem dbVulnerability = hpc.getVulnerabilityKBItemByUUIDNoRecursive(f.getKb().getUuid());
			if(null!=dbVulnerability && null!=dbVulnerability.getLastUpdate()) {
				if(DateUtils.truncate(f.getKb().getLastUpdate(), Calendar.SECOND).after(DateUtils.truncate(dbVulnerability.getLastUpdate(), Calendar.SECOND))) {
					f.getKb().setFromHub(true);
					f.getKb().setStatus(KBStatus.AVAILABLE);
					fetchInnerKbs(f);
					f.setKb(hpc.updateVulnerability(f.getKb().getUuid(),f.getKb()));
				}
				else {
					f.setKb(dbVulnerability);
				}
			}
			else {
				f.getKb().setStatus(KBStatus.AVAILABLE);
				f.getKb().setFromHub(true);
				f.getKb().setId(null);		
				f.getKb().getMd().setId(null);
				fetchInnerKbs(f);
				// we need to save it right now or it could be added twice (remediation/exploitation).
				f.setKb(hpc.addVulnerabilityKB(f.getKb()));
			}
			for(FlagQuestion fq : f.getFlagQuestionList()) {
				fq.setId(null);
				fq.getMd().setId(null);
				fq.getHint().setId(null);
				fq.getHint().getMd().setId(null);
				if(null!=fq.getSelfCheck())
					fq.getSelfCheck().setId(null);
			}
		}

		Gson gson = new Gson();
		Set<Integer> orgsInt = gson.fromJson(json.get(Constants.ACTION_PARAM_ORGS_LIST), new TypeToken<Set<Integer>>(){}.getType());
		Set<String> regions = gson.fromJson(json.get(Constants.ACTION_PARAM_REGION_LIST), new TypeToken<Set<String>>(){}.getType());

		if(null==orgsInt)
			orgsInt = new HashSet<Integer>();
		if(null==regions)
			regions = new HashSet<String>();

		if(exercise.getSupportsAutomatedScoring()) {
			exercise.setDefaultScoring(AvailableExerciseExerciseScoringMode.AUTOMATED_REVIEW);
		}
		else {
			exercise.setDefaultScoring(AvailableExerciseExerciseScoringMode.MANUAL_REVIEW);
		}


		DownloadQueueItem item = new DownloadQueueItem();
		item.setDate(new Date());
		item.setStatus(DownloadQueueItemStatus.RECEIVED);
		item.setOrganizations(orgsInt);
		item.setRegions(regions);
		if(null==oldExercise) {
			item.setType(DownloadQueueItemType.INSTALLATION);
		}
		else {
			item.setType(DownloadQueueItemType.UPDATE);
		}

		item.setLastUpdate(new Date());		

		Integer id = hpc.addAvailableExercise(exercise);
		
		exercise = hpc.getAvailableExerciseDetailsMgmt(id);
		item.setExercise(exercise);

		if(null!=id && null!=oldExercise) {
			oldExercise.setStatus(AvailableExerciseStatus.SUPERSEDED);
			logger.debug("Exercise  "+oldExercise.getUuid()+"/"+oldExercise.getId()+" is being updated, setting old exercise status to "+oldExercise.getStatus());	
			hpc.updateAvailableExercise(oldExercise);
		}else {
			logger.debug("Exercise  "+exercise.getUuid()+" is new");
		}

		hpc.addDownalodQueueItem(item);
		MessageGenerator.sendExerciseInfoMessage(exercise, response);
		
		if(!hpc.doesFrameworkExist(exercise.getFramework())){
			Framework fram = new Framework();
			fram.setName(exercise.getFramework());
			hpc.addFramework(fram);
		}
	}

	private void fetchInnerKbs(Flag f) {
		if(null!=f && null!=f.getKb().getIsAgnostic() && f.getKb().getIsAgnostic() && null!=f.getKb().getKBDetailsMapping()) {
			f.getKb().setTechnology("Agnostic");
			for(String tech : f.getKb().getKBDetailsMapping().keySet()) {
				KBVulnerabilityItem newInnerKB = f.getKb().getKBDetailsMapping().get(tech);
				newInnerKB.setId(null);
				newInnerKB.getMd().setId(null);
				if(null!=newInnerKB && null!=newInnerKB.getUuid() && null != newInnerKB.getMd()) {
					newInnerKB.setStatus(KBStatus.AVAILABLE);
					KBVulnerabilityItem dbTmpInnerKB = hpc.getVulnerabilityKBItemByUUIDNoRecursive(newInnerKB.getUuid());
					
					if(dbTmpInnerKB!=null) {
						// if inner exists
						if(null!=dbTmpInnerKB.getLastUpdate() && null!=newInnerKB.getLastUpdate()) {
							// update if hub is newer
							if(DateUtils.truncate(newInnerKB.getLastUpdate(), Calendar.SECOND).after(DateUtils.truncate(dbTmpInnerKB.getLastUpdate(), Calendar.SECOND))){
								KBVulnerabilityItem dbUpdatedKBItem = hpc.updateVulnerability(newInnerKB.getUuid(), newInnerKB);
								f.getKb().getKBDetailsMapping().put(tech, dbUpdatedKBItem);
							}
							// or use db one
							else {
								f.getKb().getKBDetailsMapping().put(tech, dbTmpInnerKB);
							}
						}
						else { // kb timestamp on db doesn't exist, update with hub kb
							KBVulnerabilityItem dbUpdatedKBItem = hpc.updateVulnerability(newInnerKB.getUuid(), newInnerKB);
							f.getKb().getKBDetailsMapping().put(tech, dbUpdatedKBItem);
						}
					}
					else { // inner kb item is not present on deployment, add and link
						Integer id = hpc.addVulnerability(newInnerKB);
						if(null!=id) {
							KBVulnerabilityItem dbNewKBItem = hpc.getVulnerabilityKBItem(id);
							f.getKb().getKBDetailsMapping().put(tech, dbNewKBItem);
						}
					}
				}
			}
		}
	}
}