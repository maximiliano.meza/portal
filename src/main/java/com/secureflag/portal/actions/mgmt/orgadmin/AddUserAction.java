/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.orgadmin;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.Country;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserStatus;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.PasswordComplexityUtils;
import com.secureflag.portal.utils.RandomGeneratorUtils;

public class AddUserAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@SuppressWarnings("serial")
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JsonObject json = (JsonObject)request.getAttribute("json");

		User sessionUser = (User)request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonElement usernameElement = json.get(Constants.ACTION_PARAM_USERNAME);
		JsonElement firstNameElement = json.get(Constants.ACTION_PARAM_FIRST_NAME);
		JsonElement lastNameElement = json.get(Constants.ACTION_PARAM_LAST_NAME);
		JsonElement emailElement = json.get(Constants.ACTION_PARAM_EMAIL);
		JsonElement countryElement = json.get(Constants.ACTION_PARAM_COUNTRY);
		JsonElement passwordElement = json.get(Constants.ACTION_PARAM_PASSWORD);
		JsonElement orgElement = json.get(Constants.ACTION_PARAM_ORG_ID);

		JsonElement roleElement = json.get(Constants.ACTION_PARAM_ROLE_ID);
		JsonElement concurrentExerciseLimitElement = json.get(Constants.ACTION_PARAM_CONCURRENT_EXERCISE_LIMIT);
		JsonElement creditsElement = json.get(Constants.ACTION_PARAM_CREDITS);
		JsonElement passwordChangeElement = json.get(Constants.ACTION_PARAM_FORCE_CHANGE_PASSWORD);
		JsonElement emailVerifiedElement = json.get(Constants.ACTION_PARAM_EMAIL_VERIFIED);


		String username = usernameElement.getAsString();
		String firstName = firstNameElement.getAsString();
		String lastName = lastNameElement.getAsString();
		String email = emailElement.getAsString();
		String country = countryElement.getAsString();
		String password = passwordElement.getAsString();
		Integer orgId = orgElement.getAsInt();
		Integer credits = creditsElement.getAsInt();


		Integer roleId = roleElement.getAsInt();
		Integer concurrentExercisesLimit = concurrentExerciseLimitElement.getAsInt();
		Boolean emailVerified = true;
		if(emailVerifiedElement!=null)
			emailVerified = emailVerifiedElement.getAsBoolean();

		Boolean forcePasswordChange = false;
		if(passwordChangeElement!=null)
			forcePasswordChange = passwordChangeElement.getAsBoolean();

		Integer usrRole = -2;
		switch (roleId){
		case -1: 
			usrRole = Constants.ROLE_SF_ADMIN;
			break;
		case 0: 
			usrRole = Constants.ROLE_ADMIN;
			break;
		case 3: 
			usrRole = Constants.ROLE_TEAM_MANAGER;
			break;
		case 4: 
			usrRole = Constants.ROLE_STATS;
			break;
		case 7: 
			usrRole = Constants.ROLE_USER;
			break;

		default: {
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		}
		if (usrRole.intValue() < sessionUser.getRole().intValue()){
			MessageGenerator.sendErrorMessage("NotAuthorized", response);
			return;
		}
		Organization o = hpc.getOrganizationById(orgId);
		if(null==o){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		boolean isManager = false;
		for(Organization organization : sessionUser.getManagedOrganizations()){
			if(o.getId().equals(organization.getId())){
				isManager = true;
				break;
			}
		}
		if(!isManager){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		List<User> organizationUsers = hpc.getManagementAllUsers(new HashSet<Organization>(){

			{add(o);}});
		if (organizationUsers.size()>=o.getMaxUsers()) {
			MessageGenerator.sendErrorMessage("MaxUserLimit", response);
			return;
		}
		if(!PasswordComplexityUtils.isPasswordComplex(password)){
			MessageGenerator.sendErrorMessage("WeakPassword", response);
			return;
		}
		User existingUser = hpc.getUserFromUsername(username);
		if (existingUser != null) {
			MessageGenerator.sendErrorMessage("UserExists", response);
			return;
		}
		Country c = hpc.getCountryFromCode(country);
		if(null==c){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		User user = new User();
		user.setEmail(email);
		user.setLastName(lastName);
		user.setUsername(username);
		user.setFirstName(firstName);
		user.setRole(usrRole);
		String salt = RandomGeneratorUtils.getNextSalt();
		String pwd = DigestUtils.sha512Hex(password.concat(salt));
		user.setSalt(salt);
		user.setPassword(pwd);
		user.setStatus(UserStatus.ACTIVE);
		user.setCountry(c);
		user.setScore(0);
		user.setExercisesRun(0);
		user.setEmailVerified(emailVerified);
		user.setForceChangePassword(forcePasswordChange);
		user.setInstanceLimit(concurrentExercisesLimit);
		user.setJoinedDateTime(new Date());
		user.setTeam(null);
		user.setCredits(credits);
		user.setCreatedByUser(sessionUser.getIdUser());
		user.setDefaultOrganization(o);

		if(sessionUser.getRole().equals(Constants.ROLE_SF_ADMIN) && !user.getRole().equals(Constants.ROLE_USER)) {
			JsonElement managedOrgs = json.get(Constants.ACTION_PARAM_ORGS_LIST);
			if(managedOrgs!=null) {
				Type listOrgsType = new TypeToken<Set<Organization>>() {}.getType();
				Gson gson = new Gson();
				Set<Organization> orgs = gson.fromJson(json.get(Constants.ACTION_PARAM_ORGS_LIST), listOrgsType);
				if(orgs!=null && !orgs.isEmpty())
					user.setManagedOrganizations(orgs);
			}
			if(user.getManagedOrganizations().isEmpty() && user.getRole().intValue() < Constants.ROLE_USER)
				user.setManagedOrganizations(new HashSet<Organization>() {{ add(o); }});
		}
		else {
			if (user.getRole().intValue() < Constants.ROLE_USER) {
				user.setManagedOrganizations(new HashSet<Organization>() {{ add(o); }});
			} else {
				user.setManagedOrganizations(null);
			}
		}
		Integer id = hpc.addUser(user);
		if(null!=id && id>0){
			NotificationsHelper helper = new NotificationsHelper();
			helper.addNewUserAdded(user);
			helper.addWelcomeToSecureFlagNotification(user);
			MessageGenerator.sendSuccessMessage(response);

			User dbUsr = hpc.getUserFromUserId(id);
			dbUsr.setEmailVerified(true);
			hpc.updateUserInfo(dbUsr);
		}
		else{
			logger.error("Signup failed at DB-end for email: "+email);
			MessageGenerator.sendErrorMessage("SignupFailed", response);
		}
	}

}
