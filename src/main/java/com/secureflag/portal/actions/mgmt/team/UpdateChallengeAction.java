/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.team;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ChallengeStatus;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class UpdateChallengeAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement idElement = json.get(Constants.ACTION_PARAM_ID);
		JsonElement nameElement = json.get(Constants.ACTION_PARAM_NAME);
		JsonElement detailsElement = json.get(Constants.ACTION_PARAM_DETAILS);
		JsonElement startElement = json.get(Constants.ACTION_PARAM_START_DATE);
		JsonElement endElement = json.get(Constants.ACTION_PARAM_END_DATE);
		JsonElement scoringElement = json.get(Constants.ACTION_PARAM_SCORING_MODE);
		JsonElement firstPlaceElement = json.get(Constants.ACTION_PARAM_SCORING_FIRST_PLACE);
		JsonElement secondPlaceElement = json.get(Constants.ACTION_PARAM_SCORING_SECOND_PLACE);
		JsonElement thirdPlaceElement = json.get(Constants.ACTION_PARAM_SCORING_THIRD_PLACE);

		Challenge c = hpc.getChallengeWithDetails(idElement.getAsInt(), user.getManagedOrganizations());
		if(null==c) {
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		if(!nameElement.getAsString().equals(c.getName())) {
			Challenge existingChallenge = hpc.getChallengeFromName(nameElement.getAsString(),c.getOrganization().getId());
			if (existingChallenge != null) {
				MessageGenerator.sendErrorMessage("NameNotAvailable", response);
				return;
			} 
		}
		Type listStringType = new TypeToken<List<String>>() {}.getType();

		Gson gson = new Gson();
		Set<User> challengeUsers = new HashSet<User>();

		try {
			List<String> users = gson.fromJson(json.get(Constants.ACTION_PARAM_USERID_LIST), listStringType);
			for(String username : users) {
				User tmpUser = hpc.getUserFromUsername(username);
				if(null!=tmpUser && tmpUser.getDefaultOrganization().equals(c.getOrganization()))
					challengeUsers.add(tmpUser);
			}
		}catch(Exception e) {
			MessageGenerator.sendErrorMessage("ListsParseError", response);
			return;
		}

		c.setDetails(detailsElement.getAsString());

		SimpleDateFormat parser=new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss zzz");
		c.setEndDate(parser.parse(endElement.getAsString()));
		c.setStartDate(parser.parse(startElement.getAsString()));


		c.setFirstInFlag(firstPlaceElement.getAsInt());
		c.setSecondInFlag(secondPlaceElement.getAsInt());
		c.setThirdInFlag(thirdPlaceElement.getAsInt());

		if(c.getEndDate().before(c.getStartDate())){
			MessageGenerator.sendErrorMessage("DatesError", response);
			return;
		}

		c.setLastActivity(new Date());
		c.setName(nameElement.getAsString());
		try {
			AvailableExerciseExerciseScoringMode scoring = AvailableExerciseExerciseScoringMode.getStatusFromStatusCode(scoringElement.getAsInt());
			c.setScoring(scoring);
		}catch(Exception e) {
			c.setScoring(AvailableExerciseExerciseScoringMode.MANUAL_REVIEW);
		}
		c.setUsers(challengeUsers);

		if(c.getStartDate().before(new Date())) 
			c.setStatus(ChallengeStatus.IN_PROGRESS);
		else
			c.setStatus(ChallengeStatus.NOT_STARTED);

		Double completion = 0.0;

		Set<String> challengeExerciseUUIDs = c.getScopeExercises();
		Set<AvailableExercise> challengeExercises = new HashSet<AvailableExercise>();
		challengeExercises.addAll(hpc.getChallengeExercises(challengeExerciseUUIDs));
		c.setExerciseData(challengeExercises);
		Integer nrExercises = c.getScopeExercises().size();
		Integer runExercises = c.getRunExercises().size();
		try {
			if(runExercises != 0 && null!=c.getUsers() && !c.getUsers().isEmpty() && !c.getRunExercises().isEmpty()) {
				completion = (double) ( (double) runExercises / ((double) nrExercises * (double) c.getUsers().size())) * 100.0;
			}
		}catch(Exception e) {
			logger.warn(e.getMessage());
		}
		c.setCompletion(completion);
		
		Boolean result = hpc.updateChallenge(c);
		if(result) {
			MessageGenerator.sendSuccessMessage(response);
		}
		else {
			MessageGenerator.sendErrorMessage("ChallengeNotUpdate", response);
		}

	}

}
