package com.secureflag.portal.actions.mgmt.sfadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.messages.json.MessageGenerator;

public class CreateRepoForSDKDeployAction extends SFAction {

	private AWSHelper aws = new AWSHelper();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement nameElement = json.get(Constants.ACTION_PARAM_NAME);
		String name	 = nameElement.getAsString();

		String url = aws.createECRRepository(SFConfig.getRegion(), "secureflag-exercises/"+name);
		
		if(url!=null)
			MessageGenerator.sendECRUrlMessage(url, response);
		else
			MessageGenerator.sendErrorMessage("Error", response);
	}

}
