/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.team;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoMarkExerciseAsCancelledAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer idExercise = jsonElement.getAsInt();

		ExerciseInstance reviewedInstance = hpc.getManagementCompletedExerciseInstance(idExercise,sessionUser.getManagedOrganizations());
		if(null==reviewedInstance){
			logger.error("ExerciseInstance "+idExercise+" not found from reviewer "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		if(sessionUser.getRole().equals(Constants.ROLE_TEAM_MANAGER)){
			List<User> users = hpc.getUsersInTeamManagedBy(sessionUser);
			if(!users.contains(reviewedInstance.getUser())){
				MessageGenerator.sendErrorMessage("NotFound", response);
				return;
			}
		}
		Integer idFile = null;
		if(null!=reviewedInstance.getResultFile()){
			idFile = reviewedInstance.getResultFile().getId();
			reviewedInstance.setResultFile(null);
		}
		Integer idScore = reviewedInstance.getScore().getIdExerciseScore();
		List<Integer> idResults = new LinkedList<Integer>();
		for(ExerciseInstanceResult er : reviewedInstance.getResults()){
			idResults.add(er.getIdResult());
		}

		reviewedInstance.setStatus(ExerciseInstanceStatus.CANCELLED);
		reviewedInstance.setResultsAvailable(false);
		reviewedInstance.getResults().clear();
		reviewedInstance.setScore(null);
		reviewedInstance.setReviewer(sessionUser.getIdUser());
		reviewedInstance.setReviewedDate(new Date());

		hpc.updateExerciseInstance(reviewedInstance);
		hpc.cleanDataForCancelledInstance(idFile,idScore,idResults);

		User u = reviewedInstance.getUser();

		List<ExerciseInstance> userRunInstances = hpc.getReviewedExerciseInstancesForUser(u.getIdUser());

		u.setExercisesRun(userRunInstances.size());
		Integer newScore = 0;
		for(ExerciseInstance userRunInst : userRunInstances) {
			newScore += userRunInst.getScore().getResult();
		}
		u.setScore(newScore);
		hpc.updateUserInfo(u);

		if(null!=reviewedInstance.getChallengeId()) {
			Challenge c = hpc.getChallengeCompleteManagement(reviewedInstance.getChallengeId());
			if(null!=c) {
				Double completion = 0.0;
				Integer nrExercises = c.getScopeExercises().size();
				Integer runExercises = c.getRunExercises().size();
				try {
					if(runExercises != 0 && null!=c.getUsers() && !c.getUsers().isEmpty() && !c.getRunExercises().isEmpty()) {
						completion = (double) ( (double) runExercises / ((double) nrExercises * (double) c.getUsers().size())) * 100.0;
					}
				}catch(Exception e) {
					logger.warn(e.getMessage());
				}
				c.setCompletion(completion);
				c.setLastActivity(new Date());
				hpc.updateChallenge(c);
			}
		}

		logger.debug("Exercise "+idExercise+" marked as cancelled from reviewer "+sessionUser.getIdUser());
		MessageGenerator.sendSuccessMessage(response);
	}

}
