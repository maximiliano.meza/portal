/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.orgadmin;

import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.Team;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class AddTeamAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@SuppressWarnings("serial")
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement newTeamName = json.get(Constants.ACTION_PARAM_TEAM_NAME);
		String teamName = newTeamName.getAsString();

		JsonElement newTeamOrganization = json.get(Constants.ACTION_PARAM_ORG_NAME);
		Integer teamOrg = newTeamOrganization.getAsInt();

		//check if user can manage specified org
		Organization org = hpc.getOrganizationById(teamOrg);
		boolean isManager = false;
		for(Organization mOrg : sessionUser.getManagedOrganizations()){
			if(org.getId().equals(mOrg.getId())){
				isManager = true;
				break;
			}
		}
		if(null==org || !isManager){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		//check if team already exists for specified org
		Team t = hpc.getTeamFromName(teamName,org);
		if(null!=t){
			MessageGenerator.sendErrorMessage("TeamExists", response);
			return;
		}
		
		Team team = new Team();
		team.setManagers(new HashSet<User>(){{add(sessionUser);}});
		team.setName(teamName);
		team.setCreatedByUser(sessionUser.getIdUser());
		team.setOrganization(org);
		if(null!=hpc.addTeam(team))
			MessageGenerator.sendSuccessMessage(response);
		else
			MessageGenerator.sendErrorMessage("Error", response);

	}
}
