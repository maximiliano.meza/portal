/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.mgmt.sfadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.ECSTaskPlacementStrategy;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.GatewayStatus;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class UpdateSatelliteGatewayAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement idElement = json.get(Constants.ACTION_PARAM_ID);
		JsonElement nameElement = json.get(Constants.ACTION_PARAM_NAME);
		JsonElement fqdnElement = json.get(Constants.ACTION_PARAM_FQDN);
		JsonElement statusElement = json.get(Constants.ACTION_PARAM_STATUS);
		JsonElement imageSyncElement = json.get(Constants.ACTION_PARAM_IMAGE_SYNC);
		JsonElement instanceScaleInElement = json.get(Constants.ACTION_PARAM_INSTANCE_SCALE_IN);
		JsonElement placementStrategyElement = json.get(Constants.ACTION_PARAM_PLACEMENT_STRATEGY);

		Integer id = idElement.getAsInt();
		String name = nameElement.getAsString();
		String fqdn = fqdnElement.getAsString();
		Boolean status = statusElement.getAsBoolean();
		Boolean imageSync = imageSyncElement.getAsBoolean();
		Boolean instanceScaleIn = instanceScaleInElement.getAsBoolean();
		ECSTaskPlacementStrategy strategy = ECSTaskPlacementStrategy.getStrategyFromCode(placementStrategyElement.getAsInt());
		
		Gateway g = new Gateway();
		g.setName(name);
		g.setFqdn(fqdn);
		g.setActive(status);
		g.setEnableScaleIn(instanceScaleIn);
		g.setEnableImageSync(imageSync);
		g.setPlacementStrategy(strategy);
		g.setStatus(GatewayStatus.AVAILABLE);

	
		if(hpc.updateGateway(id,g))
			MessageGenerator.sendSuccessMessage(response);
		else
			MessageGenerator.sendErrorMessage("Failed", response);
	}
}
