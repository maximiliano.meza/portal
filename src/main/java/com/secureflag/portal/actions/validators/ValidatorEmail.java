/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.validators;

import com.google.gson.JsonObject;
import org.apache.commons.validator.routines.EmailValidator;

public class ValidatorEmail  implements IFieldValidator{

	@Override
	public boolean isValid(JsonObject json, String attribute) {
		return null != json.get(attribute) && EmailValidator.getInstance().isValid(json.get(attribute).getAsString());
	}

}
