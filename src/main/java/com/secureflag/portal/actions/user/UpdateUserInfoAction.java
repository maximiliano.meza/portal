/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.secureflag.portal.actions.user;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Country;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class UpdateUserInfoAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement firstNameElement = json.get(Constants.ACTION_PARAM_FIRST_NAME);
		String firstName = firstNameElement.getAsString();

		JsonElement lastNameElement = json.get(Constants.ACTION_PARAM_LAST_NAME);
		String lastName = lastNameElement.getAsString();

		JsonElement emailElement = json.get(Constants.ACTION_PARAM_EMAIL);
		String email = emailElement.getAsString();

		JsonElement countryElement = json.get(Constants.ACTION_PARAM_COUNTRY);
		String country = countryElement.getAsString();
		Country c = hpc.getCountryFromCode(country);

		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		Boolean emailChanged = false;
		if(!sessionUser.getEmail().equals(email)) {
			emailChanged = true;
		}

		if(null!=firstName && null!=lastName && null!=email && null!=c){
			sessionUser.setCountry(c);
			sessionUser.setEmail(email);
			sessionUser.setPersonalDataUpdateDateTime(new Date());
			sessionUser.setFirstName(firstName);
			sessionUser.setLastName(lastName);
			if(emailChanged && SFConfig.getEmailModule())
				sessionUser.setEmailVerified(false);
			hpc.updateUserInfo(sessionUser);
			MessageGenerator.sendSuccessMessage(response);
		}
		else{
			logger.error("Could not update profile details for user "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("InvalidData", response);
		}
	}
}