/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceFeedback;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoLeaveFeedbackAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private NotificationsHelper notificationsHelper = new NotificationsHelper();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer idExercise = jsonElement.getAsInt();
		JsonElement feedbackElement = json.get(Constants.ACTION_PARAM_FEEDBACK);
		String feedbackMessage = feedbackElement.getAsString();
		ExerciseInstance instance =  hpc.getCompletedExerciseInstanceForUser(sessionUser.getIdUser(), idExercise);	
		if(null!=instance && instance.getFeedback()==false){
			ExerciseInstanceFeedback feedback = new ExerciseInstanceFeedback();
			feedback.setFeedback(feedbackMessage);
			feedback.setInstance(instance);
			feedback.setUser(sessionUser);
			feedback.setDate(new Date());
			hpc.addUserFeedback(feedback);
			MessageGenerator.sendSuccessMessage(response);
			instance.setFeedback(true);
			hpc.updateExerciseInstance(instance);
			notificationsHelper.newFeedbackReceived(sessionUser, instance);
		}
		else{
			logger.error("ExerciseInstance: "+idExercise+" not found for user "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
		}
	}
}
