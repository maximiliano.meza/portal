/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAuthenticationEvent;
import com.secureflag.portal.model.UserStatus;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class RemoveUserSelfAction extends SFAction {

	private  HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		long time = System.currentTimeMillis();
		Date date = new Date();
		user.setEmail("removed"+time+"@secureflag.com");
		user.setEmailVerified(false);
		user.setForceChangePassword(true);
		user.setFirstName("Removed");
		user.setLastName("User");
		user.setInstanceLimit(0);
		user.setTeam(null);
		user.setCredits(0);
		user.setDefaultOrganization(null);
		user.setManagedOrganizations(null);
		user.setPersonalDataAnonymisedDateTime(date);
		user.setUsername("removed"+time);
		user.setStatus(UserStatus.REMOVED);

		Boolean result = hpc.updateUserInfo(user);
		if(!result) {
			logger.error("Could not update info for removal of user: "+user.getIdUser() );
			MessageGenerator.sendErrorMessage("UpdateFailed", response);
			return;
		}

		UserAuthenticationEvent attempt = new UserAuthenticationEvent();
		attempt.setLogoutDate(new Date());
		attempt.setUsername(user.getUsername());
		attempt.setSessionIdHash(DigestUtils.sha256Hex(request.getSession().getId()));
		hpc.addLogoutEvent(attempt);

		request.getSession().removeAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		request.getSession().removeAttribute(Constants.ATTRIBUTE_SECURITY_ROLE);	
		request.getSession().invalidate();
		request.getSession(true);

		logger.debug("Logout successful for : "+user.getIdUser());
		MessageGenerator.sendRedirectMessage(Constants.INDEX_PAGE, response);
	}
}