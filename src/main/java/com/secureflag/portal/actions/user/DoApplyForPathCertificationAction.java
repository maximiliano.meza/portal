/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.AchievementType;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.LearningPath;
import com.secureflag.portal.model.LearningPathCertification;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAchievement;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoApplyForPathCertificationAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);

		JsonElement idElement = json.get(Constants.ACTION_PARAM_ID);

		LearningPath paths = hpc.getLearningPath(idElement.getAsInt(),user.getDefaultOrganization());
		List<ExerciseInstance> instances = hpc.getReviewedExerciseInstancesForUser(user.getIdUser());
		for(String uuid : paths.getExercises()) {
			Boolean found = false;
			Boolean solved = false;
			for(ExerciseInstance i : instances) {
				if(uuid.equals(i.getAvailableExercise().getUuid())) {
					found = true;
					solved = isSolved(i.getResults());
					if(solved)
						break;
				}
			}
			if(!found || !solved) {
				MessageGenerator.sendErrorMessage("NotQualified", response);
				logger.warn("User "+user.getIdUser()+" applied for certification for plan "+paths.getIdPath()+" without being qualified, exercise "+uuid+" found: "+found+" solved: "+solved);
				return;
			}
		}
		List<UserAchievement> achievements = hpc.getAllAchievementsForUser(user.getIdUser(),AchievementType.CERTIFICATION);

		Boolean alreadyCertified = false;
		for(UserAchievement a : achievements) {
			LearningPathCertification certification = (LearningPathCertification) a.getAchievement();
			if(certification.getPathId().equals(paths.getIdPath()) && !isExpired(certification.getExpiration())) {
				alreadyCertified = true;
				break;
			}
		}
		if(alreadyCertified) {
			MessageGenerator.sendErrorMessage("AlreadyCertified", response);
			logger.warn("User "+user.getIdUser()+" applied for certification for plan "+paths.getIdPath()+" but he is already certified.");
			return;
		}

		UserAchievement a = new UserAchievement();
		LearningPathCertification certification = new LearningPathCertification();
		certification.setAllowRenewal(paths.getAllowRenewal());
		certification.setDifficulty(paths.getDifficulty());

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, paths.getMonthsExpiration());

		certification.setExpiration(cal.getTime());
		certification.setPathId(paths.getIdPath());
		certification.setRecertificationProgress(0.0);
		certification.setRefresherPercentage(paths.getRefresherPercentage());
		certification.setTechnology(paths.getTechnology());
		certification.setName(paths.getName());
		certification.setType(AchievementType.CERTIFICATION);
		a.setAchievement(certification);
		a.setDate(new Date());
		a.setUser(user);
		hpc.managementAddAchievedTrophy(a);
		NotificationsHelper notificationsHelper = new NotificationsHelper();
		notificationsHelper.addCertificationNotification(user,certification);	
		MessageGenerator.sendSuccessMessage(response);

	}
	
	private Boolean isExpired(Date expiration) {
		Date now = new Date();
		return expiration.before(now);
	}

	private Boolean isSolved(List<ExerciseInstanceResult> results) {
		if(null == results || results.isEmpty())
			return false;
		for(ExerciseInstanceResult result : results) {
			if(!result.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !result.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED)) {
				return false;
			}
		}
		return true;
	}
	
}
