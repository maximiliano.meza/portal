package com.secureflag.portal.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.gateway.GatewayHelper;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.dto.ExerciseInstanceAppStatus;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetExerciseInstanceStatusAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer idExercise = jsonElement.getAsInt();

		ExerciseInstance instance = hpc.getActiveExerciseInstanceForUserWithECSGuacExerciseAndResult(sessionUser.getIdUser(),idExercise);
		if(null!=instance){			
			GatewayHelper gw = new GatewayHelper();
			ExerciseInstanceAppStatus res = gw.getInstanceStatus(instance);
			MessageGenerator.sendExerciseAppStatus(res,response);
		}
		else{
			logger.error("Exercise "+idExercise+" not found for user "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
		}

	}

}
