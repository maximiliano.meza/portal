/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.cloud.AWSECSLaunchStrategy;
import com.secureflag.portal.cloud.AWSFargateLaunchStrategy;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.cloud.LaunchStrategy;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.gateway.GuacamoleHelper;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.AvailableExerciseRegion;
import com.secureflag.portal.model.AvailableExerciseStatus;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.EC2Instance;
import com.secureflag.portal.model.ECSTaskDefinition;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceReservation;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoLaunchExerciseInstanceAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_ATTRIBUTE_JSON);

		if(user.getCredits()==0) {
			MessageGenerator.sendErrorMessage("CreditsLimit", response);
			logger.warn("No credits left for user "+user.getIdUser());
			return;	
		}
		List<ExerciseInstance> activeInstances = hpc.getRunningExerciseInstanceForUser(user.getIdUser());
		List<ExerciseInstanceReservation> activeReservations = hpc.getUnfulfilledReservationsForUser(user);
 		if((activeInstances.size() + activeReservations.size()) >= user.getInstanceLimit()){
			MessageGenerator.sendErrorMessage("InstanceLimit", response);
			logger.warn("Instance limit reached for user "+user.getIdUser()+" current: "+activeInstances.size()+" limit:"+user.getInstanceLimit());
			return;	
		}

		Integer crashedExerciseId = null;
		if(json.get(Constants.ACTION_PARAM_CRASHED_EXERCISE_ID)!=null)
			crashedExerciseId = json.get(Constants.ACTION_PARAM_CRASHED_EXERCISE_ID).getAsInt();

		String uuid = json.get(Constants.ACTION_PARAM_UUID).getAsString();
		JsonElement challengeElement = json.get(Constants.ACTION_PARAM_CHALLENGE_ID);
		Integer challengeElementId = getValidInteger(challengeElement);
		
		AvailableExercise exercise = hpc.getAvailableExerciseDetails(uuid,user.getDefaultOrganization());
		if(null==exercise || exercise.getStatus().equals(AvailableExerciseStatus.INACTIVE) || exercise.getStatus().equals(AvailableExerciseStatus.COMING_SOON)){
			MessageGenerator.sendErrorMessage("ExerciseUnavailable", response);
			logger.error("User "+user.getIdUser()+" requested an unavailable exercise: "+uuid);
			return;
		}

		Integer validatedChallengeId = null;

		List<Challenge> userChallengesWithExercise = hpc.getChallengesForUserExercise(exercise.getUuid(), user.getIdUser());
		if(!userChallengesWithExercise.isEmpty()) {
			Boolean inChallenge = false;
			if(null!=challengeElementId && challengeElementId!=-1) {
				Integer challengeId = challengeElementId;
				Challenge c = hpc.getChallengeWithResultsForUser(challengeId,user.getIdUser());
						
				if(null!=c) {
					for(String avE : c.getScopeExercises()) {
						if(avE.equals(exercise.getUuid())){
							validatedChallengeId = c.getIdChallenge();
							inChallenge = true;
							break;
						}
					}
				}
				if(!inChallenge) {
					logger.warn("Exercise "+uuid+" is NOT in Challenge: "+challengeId+", but a challenge id was provided by user: "+user.getIdUser());
					validatedChallengeId = null;
				}
				for(ExerciseInstance e : c.getRunExercises()) {
					if(e.getUser().getIdUser().equals(user.getIdUser()) && e.getAvailableExercise().getUuid().equals(exercise.getUuid())) {
						if(!e.getStatus().equals(ExerciseInstanceStatus.CANCELLED) && !e.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
							logger.warn("Exercise "+uuid+" is in Challenge: "+challengeId+", but user: "+user.getIdUser()+" already run it");
							validatedChallengeId = null;
							break;
						}
					}
				}
			}
			else {
				logger.warn("Exercise "+uuid+" is in Challenge, but no challenge id was provided by user: "+user.getIdUser());
				Boolean run = false;
				for(Challenge dbChallenges :userChallengesWithExercise) {
					for(ExerciseInstance dbChallengeExercise : dbChallenges.getRunExercises()) {
						if(dbChallengeExercise.getUser().getIdUser().equals(user.getIdUser()) && dbChallengeExercise.getAvailableExercise().getUuid().equals(exercise.getUuid())) {
							if(!dbChallengeExercise.getStatus().equals(ExerciseInstanceStatus.CANCELLED) && !dbChallengeExercise.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
								run = true;
								break;
							}
						}
					}
					if(!run) {
						validatedChallengeId = userChallengesWithExercise.get(0).getIdChallenge();
						break;
					}
				}
			}	
		}

		JsonElement exerciseRegionsElement = json.getAsJsonArray(Constants.ACTION_PARAM_REGION);
		Type listType = new TypeToken<ArrayList<AvailableExerciseRegion>>(){}.getType();
		List<AvailableExerciseRegion> regList  = null;
		try {
			regList = new Gson().fromJson(exerciseRegionsElement, listType);

		}catch(Exception e) {
			MessageGenerator.sendErrorMessage("NoRegionsPing", response);
			logger.error("User "+user.getIdUser()+" requested an exercise: "+uuid+" without supplying regions pings");
			return;
		}
		Collections.sort(regList, new Comparator<AvailableExerciseRegion>() {
			public int compare(AvailableExerciseRegion p1, AvailableExerciseRegion p2) {
				return Integer.valueOf(p1.getPing()).compareTo(p2.getPing());
			}
		});
		AWSHelper awsHelper = new AWSHelper();
		ECSTaskDefinition taskDefinition = null;
		Regions awsRegion = null;
		LaunchStrategy strategy = null;
		String otp = UUID.randomUUID().toString().replaceAll("-", "");

		Boolean satisfied = false;
		EC2Instance androidInstance = null;
		for(AvailableExerciseRegion r : regList) {
			logger.info("User "+user.getIdUser()+" attempting launch of exercise: "+uuid+" in region "+r.getName()+" with provided ping time: "+r.getPing());
			if(satisfied)
				break;
			try{
				awsRegion = Regions.valueOf(r.getName());
			} catch(Exception e){
				logger.warn("Region "+r.getName()+" not found for user "+user.getIdUser()+" for launching exercise: "+uuid);
				continue;	
			}

			Gateway gw = hpc.getGatewayForRegion(awsRegion);
			if(null==gw || !gw.isActive()){
				logger.warn("User "+user.getIdUser()+" requested an unavailable gateway for region: "+awsRegion+" for launching exercise: "+uuid);
				continue;
			}
			GuacamoleHelper guacHelper = new GuacamoleHelper();
			if(!guacHelper.isGuacOnline(gw)){
				logger.warn("User "+user.getIdUser()+" could not launch instance as Guac is not available in region: "+awsRegion+" for launching exercise: "+uuid);
				continue;
			}
			taskDefinition = hpc.getTaskDefinitionFromUUID(exercise.getUuid(),awsRegion);
			if(null==taskDefinition) {
				logger.warn("No task definition available for user "+user.getIdUser()+" attempting launch of exercise: "+uuid+" in region "+awsRegion+", trying in a different region..");
				continue;
			}

			if(!resourcesAvailable(Region.getRegion(awsRegion))) {
				logger.warn("Resources NOT available for user "+user.getIdUser()+" attempting launch of exercise: "+uuid+" in region "+awsRegion+" with FARGATE launch type");
				if(null!=taskDefinition.getFargateDefinitionArn()) {
					strategy = new AWSFargateLaunchStrategy(user, exercise.getDuration(), SFConfig.getExercisesCluster(), otp, taskDefinition, androidInstance, exercise, validatedChallengeId, crashedExerciseId);
				}
				else {
					logger.warn("Resources NOT available for user "+user.getIdUser()+" FARGATE launch not defined for exercise: "+uuid+" in region "+awsRegion);
					continue;
				}
			}
			else {
				logger.debug("Resources available for user "+user.getIdUser()+" launching exercise: "+uuid+" in region "+awsRegion+" - selecting ECS launch type");

				strategy = new AWSECSLaunchStrategy(user, exercise.getDuration(), SFConfig.getExercisesCluster(), otp, taskDefinition, androidInstance, exercise, validatedChallengeId, gw.getPlacementStrategy(), crashedExerciseId);
			}
			satisfied = true;
		}

		if(!satisfied) {
			MessageGenerator.sendErrorMessage("Unavailable", response);
			logger.error("User "+user.getIdUser()+" could not launch instance as there is no available region/gateway/guac/task def for exercise id "+uuid);
			return;
		}


		ExerciseInstanceReservation reservation = awsHelper.createExerciseInstance(strategy);
		if(!reservation.getError()) {
			logger.info("Reservation "+reservation.getId()+" for user "+user.getIdUser()+"  launching exercise: "+uuid+" in region "+awsRegion);
			if(user.getCredits()!=-1) {
				user.setCredits(user.getCredits()-1);
				hpc.updateUserInfo(user);
				request.getSession().setAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT, user);
				logger.info("Reducing 1 credit for user "+user.getIdUser()+" to: "+user.getCredits());
			}
			MessageGenerator.sendReservationMessage(reservation, response);
			return;
		}
		else {
			logger.warn("Reservation returned error for launch exercise "+uuid+ " for user "+user.getIdUser()+", attempting with FARGATE Strategy...");

			AWSFargateLaunchStrategy strategyFallback = new AWSFargateLaunchStrategy(user, exercise.getDuration(), SFConfig.getExercisesCluster(), otp, taskDefinition, androidInstance, exercise, validatedChallengeId, crashedExerciseId);
			ExerciseInstanceReservation reservationFallback = awsHelper.createExerciseInstance(strategyFallback);

			if(!reservationFallback.getError()) {
				logger.info("Reservation "+reservation.getId()+" for user "+user.getIdUser()+"  launching exercise: "+uuid+" in region "+awsRegion+" using Fargate");
				if(user.getCredits()!=-1) {
					user.setCredits(user.getCredits()-1);
					hpc.updateUserInfo(user);
					request.getSession().setAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT, user);
					logger.info("Reducing 1 credit for user "+user.getIdUser()+" to: "+user.getCredits());
				}
				MessageGenerator.sendReservationMessage(reservationFallback, response);
				return;
			}
			else {
				logger.warn("Reservation "+reservation.getId()+" returned error for launch exercise "+reservation.getExercise().getId()+ " for user "+user.getIdUser()+" with launch type: "+reservation.getType().toString());
				MessageGenerator.sendErrorMessage("InstanceUnavailable", response);
			}
		}
	}
	private boolean resourcesAvailable(Region region) {
		AWSHelper aws = new AWSHelper();
		if(aws.getNumberClusterContainerInstances(region)>0) {
			if(aws.getClusterMemoryReservation(region)<90) {
				return true;
			}
			return false;
		}
		return false;
	}
	private Integer getValidInteger(JsonElement element) {
		try {
			return Integer.parseInt(element.getAsString());
		} catch (Exception e) {
			return null;
		}
	}
}
