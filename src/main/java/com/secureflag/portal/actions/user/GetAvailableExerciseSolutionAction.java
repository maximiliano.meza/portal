/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.secureflag.portal.actions.user;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExerciseSolution;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ChallengeStatus;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetAvailableExerciseSolutionAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer idExercise = jsonElement.getAsInt();

		ExerciseInstance instance =  hpc.getCompletedExerciseInstanceWithSolution(sessionUser.getIdUser(),idExercise);
		if(null==instance) {
			logger.error("Solution file not found for exerciseInstance "+idExercise+" for user "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}

		if(null==instance.getAvailableExercise() || null==instance.getAvailableExercise().getSolution()) {
			logger.error("Solution file not PRESENT for exerciseInstance "+idExercise+" for user "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		AvailableExerciseSolution solution = instance.getAvailableExercise().getSolution();

		if(instance.getChallengeId()!=null) {
			Challenge c = hpc.getChallengeFromId(instance.getChallengeId());
			if(!c.getStatus().equals(ChallengeStatus.FINISHED)) {
				solution = new AvailableExerciseSolution();
				solution.setText("This exercise is part of a tournament, solutions will be available once the tournament has finished.");
			}

		}

		MessageGenerator.sendExerciseSolutionMessage(solution,response);

	}

}
