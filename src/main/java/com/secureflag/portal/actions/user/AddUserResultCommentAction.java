/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserResultComment;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class AddUserResultCommentAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private NotificationsHelper notificationsHelper = new NotificationsHelper();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		Integer exerciseInstanceId = json.get(Constants.ACTION_PARAM_ID).getAsInt();
		String selfCheckName = json.get(Constants.ACTION_PARAM_NAME).getAsString();
		String text = json.get(Constants.ACTION_PARAM_TEXT).getAsString();
		
		
		ExerciseInstance instance = hpc.getCompletedExerciseInstanceForUser(user.getIdUser(),exerciseInstanceId);
		if(null!=instance){
			for(ExerciseInstanceResult res : instance.getResults()) {
				if(res.getName().equals(selfCheckName)) {
					UserResultComment e = new UserResultComment();
					e.setDate(new Date());
					e.setFromAdmin(false);
					e.setFromUserId(user.getIdUser());
					e.setText(text);
					res.getUserReportedComplaints().add(e);
					res.setUserReportedIssueAddressed(false);
					instance.setIssuesReported(true);
					instance.setIssuesAddressed(false);
					break;
				}
			}
			hpc.updateExerciseInstance(instance);
			MessageGenerator.sendSuccessMessage(response);
			notificationsHelper.newScoringComment(user, instance);
			return;
		}
		MessageGenerator.sendErrorMessage("NOT_FOUND", response);
	}
}