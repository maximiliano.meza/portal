package com.secureflag.portal.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.KBTechnologyItem;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetTechnologyKBItemAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_UUID);
		String uuidItem= jsonElement.getAsString();
		
		KBTechnologyItem item =  hpc.getTechnologyStackByUUID(uuidItem);	
		if(null!=item){
			MessageGenerator.sendStackKBMessage(item,response);
		}else{
			logger.error("KB item not found for id "+uuidItem+" for user "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
		}
		
		
	}

}
