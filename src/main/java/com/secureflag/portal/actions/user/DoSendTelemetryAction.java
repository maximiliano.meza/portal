/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceTelemetry;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoSendTelemetryAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		Integer exerciseId = json.get(Constants.ACTION_PARAM_ID).getAsInt();
		Gson gson = new Gson();
		ExerciseInstanceTelemetry telemetry = gson.fromJson(json.get(Constants.TELEMETRY).getAsJsonObject(),ExerciseInstanceTelemetry.class);
		if(null!=telemetry) {
			ExerciseInstance instance = hpc.getRunningExerciseInstance(exerciseId);
			if(instance.getUser().getIdUser().equals(sessionUser.getIdUser())) {
				if(null==instance.getTelemetry()) {
					instance.setTelemetry(telemetry);
				}
				else if(null==instance.getTelemetry().getPageLoadTime()) {
					instance.getTelemetry().setConnectionTime(telemetry.getConnectionTime());
					instance.getTelemetry().setDnsLookupTime(telemetry.getDnsLookupTime());
					instance.getTelemetry().setDownloadTime(telemetry.getDownloadTime());
					instance.getTelemetry().setPageLoadTime(telemetry.getPageLoadTime());
					instance.getTelemetry().setRenderTime(telemetry.getRenderTime());
					instance.getTelemetry().setTlsTime(telemetry.getTlsTime());
					instance.getTelemetry().setTotalTime(telemetry.getTotalTime());
					instance.getTelemetry().setTtfb(telemetry.getTtfb());
					hpc.updateExerciseInstance(instance);
					logger.info("User "+sessionUser.getIdUser()+" submitted telemetry for exercise instance "+exerciseId);
				}
			}
			else {
				logger.warn("User "+sessionUser.getIdUser()+" is trying to submit telemetry for exercise instance "+exerciseId+" w/o being authorized.");
			}
		}
		MessageGenerator.sendSuccessMessage(response);
	}
}
