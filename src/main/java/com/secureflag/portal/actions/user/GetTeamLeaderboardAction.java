/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.secureflag.portal.actions.user;

import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.dto.TeamLeaderboard;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetTeamLeaderboardAction extends SFAction {
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	
		if(null==sessionUser.getTeam()){
			logger.debug("User "+sessionUser.getIdUser()+" is not enrolled in any team");
			MessageGenerator.sendErrorMessage("NoTeam", response);
			return;
		}
		HashSet<Organization> orgs = new HashSet<Organization>();
		orgs.add(sessionUser.getDefaultOrganization());
		List<User> users = hpc.getUsersForTeamId(sessionUser.getTeam().getIdTeam(),orgs);

		if(null!=users){
			TeamLeaderboard leaderboard = new TeamLeaderboard();
			leaderboard.setUsers(users);
			MessageGenerator.sendUserTeamLeaderboard(leaderboard,response);
		}
		else{
			logger.debug("User "+sessionUser.getIdUser()+" is not enrolled in any team");
			MessageGenerator.sendErrorMessage("NoTeam", response);
		}
	}
}