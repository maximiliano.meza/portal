/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.gateway.GatewayHelper;
import com.secureflag.portal.gateway.GuacamoleHelper;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.AchievementType;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultFile;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.Trophy;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAchievement;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoStopExerciseInstanceAction extends SFAction {
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private NotificationsHelper notificationsHelper = new NotificationsHelper();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_ATTRIBUTE_JSON);
		Integer exerciseInstanceId = json.get(Constants.ACTION_PARAM_ID).getAsInt();

		ExerciseInstance instance = hpc.getActiveExerciseInstanceForUserWithAWSInstanceAndGW(user.getIdUser(),exerciseInstanceId);
		if(null!=instance){	

			Thread stopExerciseThread = new Thread(new Runnable() {

				@Override
				public void run() {
					// get results
					GatewayHelper gwHelper = new GatewayHelper();

					ExerciseInstanceResultFile fr = gwHelper.getResultFile(instance);
					instance.setResultFile(fr);

					Challenge exerciseChallenge = null;
					if(null!=instance.getChallengeId()) {
						exerciseChallenge = hpc.getChallengeCompleteUser(instance.getChallengeId(), user.getIdUser());
						exerciseChallenge.setLastActivity(new Date());
						hpc.updateChallenge(exerciseChallenge);
					}

					List<ExerciseInstanceResult> results = gwHelper.getResultStatus(instance,exerciseChallenge);
					if(!instance.getResults().isEmpty()) {
						for(ExerciseInstanceResult origRes : instance.getResults()){
							for(ExerciseInstanceResult newRes : results){
								if(newRes.getName().equals(origRes.getName())){
									if(!origRes.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !origRes.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED) && !newRes.getStatus().equals(origRes.getStatus())) {
										origRes.setStatus(newRes.getStatus());
										origRes.setCheckerStatus(newRes.getCheckerStatus());
										origRes.setScore(newRes.getScore());
										origRes.setLastChange(new Date());
										origRes.setFirstForFlag(newRes.getFirstForFlag());
										origRes.setSecondForFlag(newRes.getSecondForFlag());
										origRes.setThirdForFlag(newRes.getThirdForFlag());
									}
									break;
								}
							}
						}
						hpc.updateExerciseInstance(instance);
					}
					else {
						instance.setResults(results);
					}
					if(instance.getResults().isEmpty()) {
						for(Flag flag : instance.getAvailableExercise().getQuestionsList()){
							for(FlagQuestion fq : flag.getFlagQuestionList()) {
								if(!fq.getOptional()) {
									ExerciseInstanceResult result = new ExerciseInstanceResult();
									result.setAutomated(true);
									result.setCategory(flag.getCategory());
									result.setScore(0);
									result.setStatus(ExerciseInstanceResultStatus.NOT_AVAILABLE);
									result.setVerified(false);
									result.setLastChange(new Date());
									result.setFlagTitle(flag.getTitle());
									instance.getResults().add(result);
								}
							}
						}
					}
					Calendar cal = Calendar.getInstance();
					instance.setEndTime(cal.getTime());

					GuacamoleHelper guacHelper = new GuacamoleHelper();
					try {
						Integer duration = guacHelper.getUserExerciseDuration(instance.getGuac());
						instance.setDuration(duration);
					}catch(Exception e) {
						logger.error("Could not get duration for instance "+instance.getIdExerciseInstance()+e.getMessage());
						long diffInMillis = instance.getEndTime().getTime() - instance.getStartTime().getTime();
						Integer diff = (int) TimeUnit.MINUTES.convert(diffInMillis, TimeUnit.MILLISECONDS);
						instance.setDuration(diff);
					}
					// stop instance
					AWSHelper awsHelper = new AWSHelper();
					if(null!=instance.getEcsInstance()){
						awsHelper.terminateTask(instance.getEcsInstance().getTaskArn());
						instance.getEcsInstance().setStatus(Constants.STATUS_STOPPED);
					}
					else {
						MessageGenerator.sendErrorMessage("Error", response);
					}
					if(null!=instance.getEc2Instance()) {
						if(awsHelper.terminateEC2Instance(instance.getRegion(), instance.getEc2Instance().getInstanceId())) {
							instance.getEc2Instance().setStatus(Constants.STATUS_STOPPED);
							instance.getEc2Instance().setEndTime(new Date());
							awsHelper.removeKeypair(instance.getEc2Instance().getKeyPairName(), instance.getRegion());
						}
						else {
							logger.error("Could not terminate ec2 instance "+instance.getEc2Instance().getInstanceId()+" and its corresponding keypair");
						}
					}
					User dbUser = hpc.getUserFromUserId(user.getIdUser());

					if(instance.getScoring().equals(AvailableExerciseExerciseScoringMode.MANUAL_REVIEW)) {
						instance.setStatus(ExerciseInstanceStatus.STOPPED);
						dbUser.setExercisesRun(dbUser.getExercisesRun() + 1);
						hpc.updateExerciseInstance(instance);
						hpc.updateUserInfo(dbUser);
					}
					else {
						instance.setStatus(ExerciseInstanceStatus.AUTOREVIEWED);
						instance.setResultsAvailable(true);
						List<ExerciseInstance> userRunExercises = hpc.getCompletedExerciseInstancesWithResultsForUser(user.getIdUser(), instance.getExercise().getUuid());

						boolean alreadyRun = false;
						boolean alreadySolved = false;
						List<ExerciseInstanceResult> solvedResults = new LinkedList<ExerciseInstanceResult>();
						instance.setSolved(isSolved(instance.getResults()));

						for(ExerciseInstance runEx : userRunExercises) {
							if(!runEx.getIdExerciseInstance().equals(instance.getIdExerciseInstance()) && (runEx.getStatus().equals(ExerciseInstanceStatus.REVIEWED) || runEx.getStatus().equals(ExerciseInstanceStatus.REVIEWED_MODIFIED) || runEx.getStatus().equals(ExerciseInstanceStatus.AUTOREVIEWED)) ) {
								alreadyRun = true;
								if(runEx.isSolved()) {
									alreadySolved = true;
								}
								for(ExerciseInstanceResult result : runEx.getResults()) {
									if(null!=result.getStatus() && (result.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || result.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
										solvedResults.add(result);
									}
								}
							}

						}
						dbUser.setExercisesRun(dbUser.getExercisesRun() + 1);

						if(alreadyRun && !alreadySolved){
							List<ExerciseInstanceResult> uniqueSolved = new LinkedList<ExerciseInstanceResult>();
							for(ExerciseInstanceResult instanceRes: instance.getResults()) {
								Boolean previouslySolved = false;
								for(ExerciseInstanceResult res : solvedResults) {
									if(res.getFlagTitle().equals(instanceRes.getFlagTitle())) {
										if(!isAlreadyPresent(uniqueSolved,res)){
											uniqueSolved.add(res);
										}
										previouslySolved = true;
										break;
									}
								}
								if(!previouslySolved && (instanceRes.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || instanceRes.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
									uniqueSolved.add(instanceRes);
								}
								// no points if the flag was previously solved.
								else if(previouslySolved && instance.getChallengeId()==null && (instanceRes.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || instanceRes.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
									instanceRes.setScore(0);
									instanceRes.setPreviouslySolved(true);
								}
							}
							// aggregate previous and current NOT_VULNERABLE/EXPLOITED results to determine exercise solved
							if(instance.getResults().size()==uniqueSolved.size()) {
								instance.setSolved(true);
							}
						}
						Integer totalScore = 0;
						for(ExerciseInstanceResult r : instance.getResults()){
							totalScore += r.getScore();
						}
						instance.getScore().setResult(totalScore);
						dbUser.setScore(dbUser.getScore() + instance.getScore().getResult());
						checkAndAttributeTrophy(instance,dbUser);
						hpc.updateExerciseInstance(instance);
						hpc.updateUserInfo(dbUser);
					}
				}
			});
			stopExerciseThread.start();
			instance.setStatus(ExerciseInstanceStatus.STOPPING);
			hpc.updateExerciseInstance(instance);
			MessageGenerator.sendSuccessMessage(response);
			return;
		}
		logger.error("Could not stop ExerciseInstance: "+exerciseInstanceId+" for user: "+user.getIdUser());
		MessageGenerator.sendErrorMessage("NotFound", response);
	}

	private Boolean isAlreadyPresent(List<ExerciseInstanceResult> uniqueSolved, ExerciseInstanceResult r) {
		for(ExerciseInstanceResult ur : uniqueSolved) {
			if(ur.getFlagTitle().equals(r.getFlagTitle())) {
				return true;
			}
		}
		return false;
	}

	private Boolean isSolved(List<ExerciseInstanceResult> results) {
		if(null == results || results.isEmpty())
			return false;
		for(ExerciseInstanceResult result : results) {
			if(!result.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !result.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED)) {
				return false;
			}
		}
		return true;
	}
	private void checkAndAttributeTrophy(ExerciseInstance instance, User user) {
		if(instance.getScore().getResult() >= instance.getScore().getTotal()) {
			List<UserAchievement> userAchievedTrophy = hpc.getAllAchievementsForUser(instance.getUser().getIdUser());
			boolean alreadyAchieved = false;
			for(UserAchievement trophy : userAchievedTrophy){
				if(trophy.getAchievement().getName().equals(instance.getAvailableExercise().getTrophyName())){
					alreadyAchieved = true;
					break;
				}
			}
			if(!alreadyAchieved){
				UserAchievement t = new UserAchievement();
				t.setDate(new Date());
				t.setUser(instance.getUser());
				Trophy achievement = new Trophy();
				achievement.setName(instance.getAvailableExercise().getTrophyName());
				achievement.setType(AchievementType.TROPHY);
				achievement.setTechnology(instance.getAvailableExercise().getTechnology());
				t.setAchievement(achievement);
				hpc.managementAddAchievedTrophy(t);
				notificationsHelper.addTrophyNotification(user,achievement);
				instance.setTrophyAwarded(true);
			}
		}
	}
}