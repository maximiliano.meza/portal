/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class CancelCrashedExerciseAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer idExercise = jsonElement.getAsInt();
		
		ExerciseInstance reviewedInstance = hpc.getCompletedExerciseInstanceForUser(sessionUser.getIdUser(), idExercise);
		if(null==reviewedInstance || !reviewedInstance.getStatus().equals(ExerciseInstanceStatus.CRASHED) ){
			logger.error("ExerciseInstance "+idExercise+" not found from reviewer "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
	
		reviewedInstance.setStatus(ExerciseInstanceStatus.CANCELLED);
		reviewedInstance.setHasCrashed(true);
		hpc.updateExerciseInstance(reviewedInstance);
		
		User dbUser = hpc.getUserFromUserId(sessionUser.getIdUser());

		List<ExerciseInstance> userRunInstances = hpc.getReviewedExerciseInstancesForUser(sessionUser.getIdUser());
		
		dbUser.setExercisesRun(userRunInstances.size());
		Integer newScore = 0;
		for(ExerciseInstance userRunInst : userRunInstances) {
			newScore += userRunInst.getScore().getResult();
		}
		dbUser.setScore(newScore);
		hpc.updateUserInfo(dbUser);
		
		if(null!=reviewedInstance.getChallengeId()) {
			Challenge c = hpc.getChallengeCompleteForUser(reviewedInstance.getChallengeId(), dbUser.getIdUser());

			Double completion = 0.0;
			Integer nrExercises = c.getScopeExercises().size();
			Integer runExercises = c.getRunExercises().size();	
			try {
				if(runExercises != 0 && null!=c.getUsers() && !c.getUsers().isEmpty() && !c.getRunExercises().isEmpty()) {
					completion = (double) ( (double) runExercises / ((double) nrExercises * (double) c.getUsers().size())) * 100.0;
				}
			}catch(Exception e) {
				logger.warn(e.getMessage());
			}
			c.setCompletion(completion);
		
		}
		
		logger.debug("Exercise "+idExercise+" (that crashed) was marked as cancelled from user "+sessionUser.getIdUser());
		MessageGenerator.sendSuccessMessage(response);
		

	}

}
