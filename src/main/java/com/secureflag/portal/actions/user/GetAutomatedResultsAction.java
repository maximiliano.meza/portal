/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.gateway.GatewayHelper;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.dto.ExerciseInstanceSelfCheckResult;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.ExerciseUtils;

public class GetAutomatedResultsAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		Integer exerciseInstanceId = json.get(Constants.ACTION_PARAM_ID).getAsInt();
		String selfcheckName = null;
		if(null!=json.get(Constants.PARAM_NAME_CHECK))
			selfcheckName = json.get(Constants.PARAM_NAME_CHECK).getAsString();
		if(null==selfcheckName || selfcheckName.isEmpty())
			selfcheckName = Constants.ALL_CHECKS;
		
		ExerciseInstance instance = hpc.getActiveExerciseInstanceForUserWithECSGuacExerciseAndResult(user.getIdUser(),exerciseInstanceId);
		if(null!=instance){			
			GatewayHelper gw = new GatewayHelper();
			ExerciseInstanceSelfCheckResult res = gw.getSelfcheckResult(instance,selfcheckName);
			MessageGenerator.sendExerciseSelfCheckStatus(res,response);

			Challenge exerciseChallenge = null;
			if(null!=instance.getChallengeId()) 
				exerciseChallenge = hpc.getChallengeCompleteUser(instance.getChallengeId(), user.getIdUser());

			Integer totalScore = 0;
			if(instance.getResults().isEmpty()){
				for(Flag flag : instance.getAvailableExercise().getQuestionsList()){
					for(FlagQuestion fq : flag.getFlagQuestionList()) {
						if(fq.getSelfCheckAvailable()) {
							ExerciseInstanceResult result = ExerciseUtils.getExerciseResult(instance,flag,fq,res.getFlagList(),exerciseChallenge);
							if(null!=result) {
								totalScore+=result.getScore();
							}
							else {
								result = new ExerciseInstanceResult();
								result.setAutomated(true);
								result.setCheckerStatus("not-available");
								result.setComment(null);
								result.setCategory(flag.getCategory());
								result.setHint(false);
								result.setFlagTitle(flag.getTitle());
								result.setFirstForFlag(false);
								result.setSecondForFlag(false);
								result.setThirdForFlag(false);
								result.setLastChange(new Date());
								result.setName(fq.getSelfCheck().getName());
								result.setScore(0);
								result.setStatus(ExerciseInstanceResultStatus.NOT_AVAILABLE);
								result.setVerified(false);
							}
							instance.getResults().add(result);
						}
					}
				}
			}
			else {	
				totalScore = instance.getScore().getResult() + ExerciseUtils.updateExerciseResults(instance,res.getFlagList(),exerciseChallenge);
			}
			instance.getScore().setResult(totalScore);

			if(res.getFlagList().size()>0) {
				if(null==instance.getCountSelfCheckByUser())
					instance.setCountSelfCheckByUser(1);
				else
					instance.setCountSelfCheckByUser(instance.getCountSelfCheckByUser()+1);
			}
			hpc.updateExerciseInstance(instance);
			return;
		}

		logger.error("Could not retrieve selfcheck results for exerciseInstance: "+exerciseInstanceId+" for user: "+user.getIdUser());
		MessageGenerator.sendErrorMessage("NotFound", response);
	}
}