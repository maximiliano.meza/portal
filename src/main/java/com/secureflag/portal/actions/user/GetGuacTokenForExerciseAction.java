/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.secureflag.portal.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.gateway.GuacamoleHelper;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetGuacTokenForExerciseAction extends SFAction{
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private GuacamoleHelper guacHelper = new GuacamoleHelper();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer idExercise = jsonElement.getAsInt();

		ExerciseInstance exercise =  hpc.getActiveExerciseInstanceForUserWithGuac(sessionUser.getIdUser(),idExercise);	
				
		String[] cookies = null;
		if(null!=exercise && exercise.getGuac()!=null){
			cookies = guacHelper.getFreshToken( exercise.getGuac().getGateway(), exercise.getGuac().getUsername(), exercise.getGuac().getPassword());
		}
		else{
			logger.error("Could not find active ExerciseInstance "+idExercise+" for user: "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		
		if(null!=cookies){
			if(null!=exercise.getEc2Instance()) {
				MessageGenerator.sendTokenMessage(exercise.getIdExerciseInstance(),exercise.getGuac().getGateway().getFqdn(), exercise.getGuac().getUsername(), cookies[0], cookies[1], exercise.getGuac().getConnectionId(), 0, exercise.getEc2Instance().getInstanceId(), exercise.getEc2Instance().getPrivateIpAddress(), 0, response);
			}
			else {
				MessageGenerator.sendTokenMessage(exercise.getIdExerciseInstance(),exercise.getGuac().getGateway().getFqdn(), exercise.getGuac().getUsername(), cookies[0], cookies[1], exercise.getGuac().getConnectionId(), 0, null, null, null, response);
			}
		}
		else{
			logger.error("Could not get GuacToken for ExerciseInstance "+idExercise+" for user: "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("CouldNotRetrieveToken", response);
		}	
	}
}