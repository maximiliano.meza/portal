/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAuthenticationEvent;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoLogoutAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
 
		if(null!=user) {
			UserAuthenticationEvent attempt = new UserAuthenticationEvent();
			attempt.setLogoutDate(new Date());
			attempt.setUsername(user.getUsername());
			attempt.setSessionIdHash(DigestUtils.sha256Hex(request.getSession().getId()));
			hpc.addLogoutEvent(attempt);
			logger.debug("Logout successful for : "+user.getIdUser());
		}
		request.getSession().removeAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		request.getSession().removeAttribute(Constants.ATTRIBUTE_SECURITY_ROLE);	
		request.getSession().invalidate();
		request.getSession(true);

		try {
			response.sendRedirect(Constants.INDEX_PAGE);
		} catch (IOException e) {
			logger.error("Failed Logout Redirect: "+e.getMessage());
		}
	}
}
