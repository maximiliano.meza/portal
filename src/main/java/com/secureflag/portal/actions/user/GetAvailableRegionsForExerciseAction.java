/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExerciseRegion;
import com.secureflag.portal.model.ECSTaskDefinition;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetAvailableRegionsForExerciseAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_UUID);
		List<Gateway> regions = hpc.getAllActiveGateways();
		List<AvailableExerciseRegion> availableRegions = new LinkedList<AvailableExerciseRegion>();
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	
		String uuid = jsonElement.getAsString();

		for(Gateway gw : regions){
			if(null==gw.getRegion())
				continue;
			
			ECSTaskDefinition task = hpc.getTaskDefinitionFromUUID(uuid,gw.getRegion(),sessionUser.getDefaultOrganization());
			if(null!=task){
				AvailableExerciseRegion r = new AvailableExerciseRegion();
				r.setFqdn(gw.getFqdn());
				r.setName(gw.getRegion().toString());
				r.setPing(-1);	
				availableRegions.add(r);
			}
			else{
				logger.warn("Exercise "+uuid+" doesn't have a TaskDefinition in region "+gw.getRegion().getName());
			}
		}
		MessageGenerator.sendExerciseRegionsMessage(availableRegions,response);
	}
}