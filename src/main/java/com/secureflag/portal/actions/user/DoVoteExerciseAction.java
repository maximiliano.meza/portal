/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceVotingScore;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class DoVoteExerciseAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement jsonElement = json.get(Constants.ACTION_PARAM_ID);
		Integer idExercise = jsonElement.getAsInt();
		JsonElement voteElement = json.get(Constants.ACTION_PARAM_VOTE);
		Integer voteInt = voteElement.getAsInt();
		ExerciseInstance instance =  hpc.getCompletedExerciseInstanceForUser(sessionUser.getIdUser(), idExercise);	
		if(null!=instance && (null==instance.getVote() || instance.getVote() == false) && voteInt > 0){
			ExerciseInstanceVotingScore score = new ExerciseInstanceVotingScore();
			score.setDate(new Date());
			score.setScore(voteInt);
			score.setHasCompletedExercise(true);
			score.setUser(sessionUser);
			score.setInstance(instance);
			hpc.addExerciseVotingScore(score);
			MessageGenerator.sendSuccessMessage(response);
			instance.setVote(true);
			hpc.updateExerciseInstance(instance);
			AvailableExercise e = hpc.getAvailableExerciseDetails(instance.getExercise().getUuid());
			e.getVotingScoreList().add(score);
			int totalScore = 0;
			for(ExerciseInstanceVotingScore s : e.getVotingScoreList()) {
				totalScore += s.getScore();
			}
			e.setVotingScoreNumbers(e.getVotingScoreList().size());
			e.setVotingScore(Math.round(totalScore/e.getVotingScoreNumbers()));
			hpc.updateAvailableExercise(e);
		}
		else{
			logger.error("ExerciseInstance: "+idExercise+" not found for user "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("NotFound", response);
		}
	}
}
