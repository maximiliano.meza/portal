/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.user;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.gateway.GuacamoleHelper;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceLaunchType;
import com.secureflag.portal.model.ExerciseInstanceReservation;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceScore;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.ExerciseInstanceTelemetry;
import com.secureflag.portal.model.ExerciseInstanceType;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.GatewayTempUser;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.dto.TokenConnectionMessage;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.ChallengeUtils;

public class DoPollReservationUpdateAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_ATTRIBUTE_JSON);
		Integer reservationId = json.get(Constants.ACTION_PARAM_ID).getAsInt();

		ExerciseInstanceReservation reservation = hpc.getReservation(reservationId);

		if(null==reservation || reservation.getError() || !reservation.getUser().getIdUser().equals(user.getIdUser())){
			MessageGenerator.sendErrorMessage("NotFound", response);
			return;
		}
		if(reservation.getFulfilled()){
			MessageGenerator.sendReservationMessage(reservation, response);
			return;
		}
		if(null!=reservation.getEcs()) {
			AWSHelper helper = new AWSHelper();
			ExerciseInstanceReservation updatedReservation = helper.pollReservation(reservation);
			if(updatedReservation.getError()) {
				updatedReservation.setTimesPolled(updatedReservation.getTimesPolled()+1);
				hpc.updateReservation(updatedReservation);
				MessageGenerator.sendErrorMessage("Expired", response);
				return;
			}
			if(!updatedReservation.getFulfilled()) {
				updatedReservation.setTimesPolled(updatedReservation.getTimesPolled()+1);
				hpc.updateReservation(updatedReservation);
				MessageGenerator.sendReservationMessage(updatedReservation, response);
				return;
			}
			else {
				updatedReservation.setTimesPolled(updatedReservation.getTimesPolled()+1);
				updatedReservation.setDateFulfilled(new Date());
				hpc.updateReservation(updatedReservation);
				Gateway gw = hpc.getGatewayForRegion(updatedReservation.getEcs().getRegion());
				if(null==gw || !gw.isActive()){
					MessageGenerator.sendErrorMessage("GWUnavailable", response);
					logger.error("User "+user.getIdUser()+" requested an unavailable gateway for region: "+updatedReservation.getEcs().getRegion().toString());
					return;
				}
				GuacamoleHelper guacHelper = new GuacamoleHelper();
				GatewayTempUser guacUser = guacHelper.setupUser(user,updatedReservation.getEcs(),gw,updatedReservation.getTmpPassword());
				if(null==guacUser || null==guacUser.getLastValidToken()){
					MessageGenerator.sendErrorMessage("GuacUnavailable", response);
					logger.error("User "+user.getIdUser()+" could not contact guacamole for task: "+updatedReservation.getEcs().getTaskArn()+" in gateway: "+gw.getName());
					return;
				}
				hpc.addGuacTempUser(guacUser);

				ExerciseInstance ei = new ExerciseInstance();
				updatedReservation.getEcs().setStatus(Constants.STATUS_RUNNING);
				Challenge c = null;
				if(null!=updatedReservation.getChallengeId()) {
					c = hpc.getChallengeWithExercisesAndResultsForUser(updatedReservation.getChallengeId(),user.getIdUser());
					ei.setType(ExerciseInstanceType.CHALLENGE);
					ei.setChallengeId(updatedReservation.getChallengeId());
					ei.setScoring(c.getScoring());
				}
				else {
					ei.setType(ExerciseInstanceType.TRAINING);
					ei.setChallengeId(null);
					if(updatedReservation.getExercise().getSupportsAutomatedScoring() && updatedReservation.getExercise().getDefaultScoring().equals(AvailableExerciseExerciseScoringMode.AUTOMATED_REVIEW))
						ei.setScoring(AvailableExerciseExerciseScoringMode.AUTOMATED_REVIEW);
					else
						ei.setScoring(AvailableExerciseExerciseScoringMode.MANUAL_REVIEW);
				}				
				ei.setEcsInstance(updatedReservation.getEcs());
				if(null!=updatedReservation.getEc2())
					ei.setEc2Instance(updatedReservation.getEc2());
				ei.setRegion(updatedReservation.getEcs().getRegion());
				ei.setStartTime(new Date());
				ei.setCountResultsReviewedByUser(0);
				ei.setAvailableExercise(updatedReservation.getExercise());
				Calendar cal = Calendar.getInstance();
				cal.setTime(ei.getStartTime());
				cal.add(Calendar.MINUTE, ei.getAvailableExercise().getDuration());
				ei.setEndTime(cal.getTime());
				ei.getEcsInstance().setShutdownTime(cal.getTime());
				ei.setResultsAvailable(false);
				ei.setLaunchType(ExerciseInstanceLaunchType.ECS);
				ei.setOrganization(user.getDefaultOrganization());

				if(updatedReservation.getCrashedExerciseId()!=null) {
					ExerciseInstance reviewedInstance = hpc.getCompletedExerciseInstanceForUser(user.getIdUser(), updatedReservation.getCrashedExerciseId());
					if(null!=reviewedInstance && reviewedInstance.getStatus().equals(ExerciseInstanceStatus.CRASHED) ){
						List<ExerciseInstanceResult> results = new LinkedList<ExerciseInstanceResult>();
						for(ExerciseInstanceResult previousResult : reviewedInstance.getResults()) {
							previousResult.setIdResult(null);
							results.add(previousResult);
						}
						ei.setResults(results);
						ei.setUsedHints(reviewedInstance.getUsedHints());
						ExerciseInstanceScore score = new ExerciseInstanceScore();
						score.setTotal(updatedReservation.getExercise().getScore());
						score.setResult(reviewedInstance.getScore().getResult());
						ei.setScore(score);
						ei.setCrashedInstance(updatedReservation.getCrashedExerciseId());
						reviewedInstance.setStatus(ExerciseInstanceStatus.CANCELLED);
						hpc.updateExerciseInstance(reviewedInstance);
					}
				}
				else {
					ei.setResults(new LinkedList<ExerciseInstanceResult>());
					ExerciseInstanceScore score = new ExerciseInstanceScore();
					score.setTotal(updatedReservation.getExercise().getScore());
					score.setResult(-1);
					ei.setScore(score);
				}

				ei.setStatus(ExerciseInstanceStatus.RUNNING);
				ei.setTechnology(updatedReservation.getExercise().getTechnology());
				ei.setTitle(updatedReservation.getExercise().getTitle());
				ei.setUser(user);
				ei.setGuac(guacUser);

				ExerciseInstanceTelemetry telemetry = new ExerciseInstanceTelemetry();
				telemetry.setTimesPolled(updatedReservation.getTimesPolled());
				telemetry.setDateFulfilled(updatedReservation.getDateFulfilled());
				telemetry.setDateRequested(updatedReservation.getDateRequested());
				telemetry.setDateRunning(updatedReservation.getDateRunning());

				Integer idTelemetry = hpc.addTelemetry(telemetry);
				telemetry.setId(idTelemetry);
				ei.setTelemetry(telemetry);
				ei.setFeedback(false);
				ei.setVote(false);

				Integer idExercise = hpc.addExerciseInstance(ei);
				if(null==idExercise) {
					logger.error("User "+user.getIdUser()+" could not create exercise instance for : "+updatedReservation.getEcs().getTaskArn()+" in gateway: "+gw.getName());
					MessageGenerator.sendErrorMessage("ExerciseFailed", response);
					return;
				}


				Integer countdown = 0;
				TokenConnectionMessage token;
				if(null!=updatedReservation.getEc2() && null!= updatedReservation.getEc2().getInstanceId()) {
					token = new TokenConnectionMessage(ei.getIdExerciseInstance(), guacUser.getGateway().getFqdn(),guacUser.getUsername(), guacUser.getLastValidToken(), guacUser.getNode(), guacUser.getConnectionId(), countdown, false, updatedReservation.getEc2().getPrivateIpAddress(),updatedReservation.getEc2().getInstanceId(),100);
				}
				else {
					token = new TokenConnectionMessage(ei.getIdExerciseInstance(), guacUser.getGateway().getFqdn(),guacUser.getUsername(), guacUser.getLastValidToken(), guacUser.getNode(), guacUser.getConnectionId(), countdown, false, null, null, null);
				}
				if(updatedReservation.getType().equals(ExerciseInstanceLaunchType.FARGATE)) {
					if(!helper.updateFargateNoOutboundSG(ei.getEcsInstance().getSecurityGroup(), ei.getRegion())) {
						logger.error("Could not update Fargate to deny outbound traffic, refusing to provide token to container "+updatedReservation.getEcs().getTaskArn());
						updatedReservation.setError(true);
						MessageGenerator.sendReservationMessage(updatedReservation, response);
						return;
					}
				}
				updatedReservation.setToken(token);
				MessageGenerator.sendReservationMessage(updatedReservation, response);

				if(ei.getType().equals(ExerciseInstanceType.CHALLENGE)) {
					ChallengeUtils.addExerciseUpdateChallenge(c, ei);
				}
			}
		}		
	}
}