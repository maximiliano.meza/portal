/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.secureflag.portal.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.PasswordComplexityUtils;

public class UpdateUserPasswordAction extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement pwdElement = json.get(Constants.ACTION_PARAM_OLDPASSWORD);
		String oldPwd = pwdElement.getAsString();
		
		JsonElement newPwdElement = json.get(Constants.ACTION_PARAM_NEWPASSWORD);
		String newPwd = newPwdElement.getAsString();
	
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		
		User user = hpc.getUser(sessionUser.getUsername(), oldPwd);
		if(null!=user && sessionUser.getIdUser().equals(user.getIdUser())){
			if(PasswordComplexityUtils.isPasswordComplex(newPwd)){
				hpc.updateUserPassword(user.getIdUser(),newPwd);
				sessionUser.setForceChangePassword(false);
				request.getSession().setAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT, sessionUser);
				user.setForceChangePassword(false);
				hpc.updateUserInfo(user);
				MessageGenerator.sendSuccessMessage(response);
			}
			else{
				logger.error("Password complexity not met for user: "+sessionUser.getIdUser());
				MessageGenerator.sendErrorMessage("PasswordComplexity", response);
			}
		}
		else{
			logger.error("Invalid old password for user: "+sessionUser.getIdUser());
			MessageGenerator.sendErrorMessage("InvalidUserPassword", response);
		}
	}
}
