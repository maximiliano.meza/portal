/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.secureflag.portal.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.FlagQuestionHint;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class GetHintForFlagQuestionAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement idQuestionElement = json.get(Constants.ACTION_PARAM_ID);
		Integer idQuestion = idQuestionElement.getAsInt();
		JsonElement idExerciseInstanceElement = json.get(Constants.ACTION_PARAM_ID_EXERCISE_INSTANCE);
		Integer idExerciseInstance = idExerciseInstanceElement.getAsInt();
		Integer idFlag = hpc.getFlagIdFromQuestionId(idQuestion);
		if(null==idFlag){
			User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
			logger.error("Flag not found for questionId "+idQuestion+" for user "+user.getIdUser());
			MessageGenerator.sendErrorMessage("HintUnavailable", response);
			return;
		}
		Flag f =  hpc.getFlagWithHints(idFlag);	

		FlagQuestionHint hint = null;
		for(FlagQuestion fq : f.getFlagQuestionList()){
			if(fq.getId().equals(idQuestion)){
				hint = fq.getHint();
				break;
			}
		}
		if(null!=hint){
			ExerciseInstance ei = hpc.getExerciseInstanceWithHints(idExerciseInstance);
			if(null!=ei && null!=ei.getStatus() && !ei.getStatus().equals(ExerciseInstanceStatus.RUNNING)){
				User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
				logger.error("Refused to provide hint "+hint.getId()+" for ExerciseInstance "+ei.getIdExerciseInstance()+" with status "+ei.getStatus().toString()+" for user "+user.getIdUser());
				MessageGenerator.sendErrorMessage("HintUnavailable", response);
				return;
			}
			boolean alreadyIn = false;
			for(FlagQuestionHint fqh : ei.getUsedHints()){
				if(fqh.getId().equals(hint.getId())){
					alreadyIn = true;
					break;
				}
			}
			if(!alreadyIn){
				ei.getUsedHints().add(hint);
				hpc.updateExerciseInstanceUsedHints(ei,hint);
			}
			MessageGenerator.sendHintMessage(hint,response);
		}
		else{
			User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
			logger.error("Hint not found for questionId "+idQuestion+" for user "+user.getIdUser());
			MessageGenerator.sendErrorMessage("HintUnavailable", response);
		}
	}
}