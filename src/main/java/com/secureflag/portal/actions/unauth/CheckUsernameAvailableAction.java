/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.unauth;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckUsernameAvailableAction extends SFAction {
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		
		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	
		if(user==null || null==user.getIdUser()){
			JsonElement orgCodeElement = json.get(Constants.ACTION_PARAM_ORG_CODE);
			String orgInviteCode = orgCodeElement.getAsString();
			Organization org = hpc.getOrganizationFromInvitationCode(orgInviteCode);
			if (org == null) {
				MessageGenerator.sendErrorMessage("NotAuthorized", response);
				return;
			} 
		}
		
		JsonElement usernameElement = json.get(Constants.ACTION_PARAM_USERNAME);
		String username = usernameElement.getAsString();

		User existingUser = hpc.getUserFromUsername(username);
		if (existingUser != null) {
			MessageGenerator.sendAvailable(false, response);
		} else {
			MessageGenerator.sendAvailable(true, response);
		}
	}
}