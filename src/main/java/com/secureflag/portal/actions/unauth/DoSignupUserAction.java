/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.unauth;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.Country;
import com.secureflag.portal.model.InvitationCodeForOrganization;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserStatus;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.PasswordComplexityUtils;
import com.secureflag.portal.utils.RandomGeneratorUtils;

public class DoSignupUserAction  extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement usernameElement = json.get(Constants.ACTION_PARAM_USERNAME);
		String username	 = usernameElement.getAsString();

		JsonElement firstNameElement = json.get(Constants.ACTION_PARAM_FIRST_NAME);
		JsonElement lastNameElement = json.get(Constants.ACTION_PARAM_LAST_NAME);
		JsonElement emailElement = json.get(Constants.ACTION_PARAM_EMAIL);
		JsonElement countryElement = json.get(Constants.ACTION_PARAM_COUNTRY);
		JsonElement passwordElement = json.get(Constants.ACTION_PARAM_PASSWORD);
		JsonElement orgInvitationCodeElement = json.get(Constants.ACTION_PARAM_ORG_CODE);

		String firstName = firstNameElement.getAsString();
		String lastName = lastNameElement.getAsString();
		String email = emailElement.getAsString();
		String country = countryElement.getAsString();
		String password = passwordElement.getAsString();
		String orgInvitationCode = orgInvitationCodeElement.getAsString();

		InvitationCodeForOrganization invitation = hpc.getInvitationFromCode(orgInvitationCode);
		if(null==invitation){
			MessageGenerator.sendErrorMessage("InvalidData", response);
			return;
		}

		Organization o = invitation.getOrganization(); 
		if(null==o){
			MessageGenerator.sendErrorMessage("OrgCodeWrong", response);
			return;
		}
		@SuppressWarnings("serial")
		List<User> organizationUsers = hpc.getManagementAllUsers(new HashSet<Organization>(){{add(o);}});
		if (organizationUsers.size()>=o.getMaxUsers()) {
			MessageGenerator.sendErrorMessage("MaxUserLimit", response);
			return;
		}
		User existingUser = hpc.getUserFromUsername(username);
		if (existingUser != null) {
			MessageGenerator.sendErrorMessage("UserExists", response);
			return;
		}
		if(!PasswordComplexityUtils.isPasswordComplex(password)){
			MessageGenerator.sendErrorMessage("WeakPassword", response);
			return;
		}
		Country c = hpc.getCountryFromCode(country);
		if(null==c){
			logger.error("Invalid data entered for signup for email: "+email);
			MessageGenerator.sendErrorMessage("InvalidData", response);
			return;
		}
		User user = new User();
		user.setEmail(email);
		user.setInvitationCodeRedeemed(orgInvitationCode);
		user.setLastName(lastName);
		user.setUsername(username);
		user.setFirstName(firstName);
		user.setRole(Constants.ROLE_USER);		
		String salt = RandomGeneratorUtils.getNextSalt();
		String pwd = DigestUtils.sha512Hex(password.concat(salt)); 
		user.setSalt(salt);
		user.setPassword(pwd);
		user.setCountry(c);
		user.setScore(0);	
		user.setExercisesRun(0);
		if(SFConfig.getEmailModule()) {
			user.setEmailVerified(false);
			user.setStatus(UserStatus.ACTIVE);
		}
		else {
			user.setEmailVerified(true);
			user.setStatus(UserStatus.INACTIVE);
		}

		user.setForceChangePassword(false);
		user.setInstanceLimit(1);
		Date today =new Date();
		user.setJoinedDateTime(today);
		user.setPersonalDataUpdateDateTime(today);
		user.setCredits(o.getDefaultCredits());
		user.setCreatedByUser(null);
		user.setTeam(null);
		user.setDefaultOrganization(o);
		Integer id = hpc.addUser(user);
		user.setIdUser(id);
		if(null!=id && id>0){
			hpc.decreseOrganizationCodeRedeem(o,orgInvitationCode);
			NotificationsHelper helper = new NotificationsHelper();
			helper.addNewUserAdded(user);
			helper.addWelcomeToSecureFlagNotification(user);


			MessageGenerator.sendRedirectMessage(Constants.POST_SIGNUP_ADMIN_APPROVAL, response);


			if(null!=invitation.getChallengeId()) {
				Challenge challenge = hpc.getChallengeCompleteManagement(invitation.getChallengeId());
				if(null!=challenge) {
					challenge.getUsers().add(user);
					Boolean challengeUpdated = hpc.updateChallenge(challenge);
					if(challengeUpdated)
						logger.debug("Challenge "+challenge.getIdChallenge()+" updated, user "+user.getIdUser()+" added");
					else
						logger.warn("Challenge "+challenge.getIdChallenge()+" COULD NOT be updated, user "+user.getIdUser()+" COULD NOT beadded");

				}
			}
		}
		else{
			logger.error("Signup failed at DB-end for email: "+email);
			MessageGenerator.sendErrorMessage("SignupFailed", response);
		}
	}
}