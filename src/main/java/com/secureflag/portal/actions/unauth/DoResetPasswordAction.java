/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.unauth;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.ValidationServiceType;
import com.secureflag.portal.model.ValidationToken;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.PasswordComplexityUtils;

public class DoResetPasswordAction extends SFAction {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement tokenElement = json.get(Constants.ACTION_PARAM_TOKEN);

		ValidationToken v = hpc.getValidationToken(tokenElement.getAsString());

		if( null!=v && v.getExpiration().getTime() >= new Date().getTime() && v.getService().equals(ValidationServiceType.PASSWORD_RESET)){

			JsonElement newPwdElement = json.get(Constants.ACTION_PARAM_NEWPASSWORD);
			String newPwd = newPwdElement.getAsString();

			User user = hpc.getUserFromUserId(v.getUser().getIdUser());
			if(null!=user){
				if(PasswordComplexityUtils.isPasswordComplex(newPwd)){
					hpc.updateUserPassword(user.getIdUser(),newPwd);
					user.setForceChangePassword(false);
					hpc.updateUserInfo(user);
					MessageGenerator.sendSuccessMessage(response);
					hpc.removeValidationToken(v);
				}
				else{
					logger.error("Password complexity not met for user: "+user.getIdUser());
					MessageGenerator.sendErrorMessage("PasswordComplexity", response);
				}
			}
			else{
				logger.error("Could not retrieve database user for: "+v.getUser().getIdUser());
				MessageGenerator.sendErrorMessage("Error", response);
			}
		}
		else {
			logger.error("Invalid token in password: "+tokenElement.getAsString());
			MessageGenerator.sendErrorMessage("InvalidToken", response);
		}
	}
}