/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.actions.unauth;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAuthenticationEvent;
import com.secureflag.portal.model.UserStatus;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.secureflag.portal.utils.CSRFTokenUtils;

public class DoLoginAction  extends SFAction{

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement usernameElement = json.get(Constants.ACTION_PARAM_USERNAME);
		String username = usernameElement.getAsString();
		
		JsonElement passwordElement = json.get(Constants.ACTION_PARAM_PASSWORD);
		String password = passwordElement.getAsString();
	
		if(null==username || username.equals("") || null==password || password.equals("")) {
			MessageGenerator.sendRedirectMessage(Constants.INDEX_PAGE, response);
			return;
		}
		Integer failedAttempts = 0; //hpc.getFailedLoginAttemptsForUser(username);
		if(failedAttempts>=Constants.FAILED_ATTEMPTS_LOCKOUT){
			logger.warn("Username "+username+" is locked out, login refused");
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACCOUNT_LOCKOUT, response);
			User lockedUser = hpc.getUserFromUsername(username);
			if(null!=lockedUser){
				lockedUser.setStatus(UserStatus.LOCKED);
				hpc.updateUserInfo(lockedUser);
			}
			MessageGenerator.sendRedirectMessage(Constants.INDEX_PAGE, response);
			return;
		}
		
		User user = hpc.getUser(username, password);
		
		UserAuthenticationEvent attempt = new UserAuthenticationEvent();
		attempt.setUsername(username);

		if(null != user && user.getUsername()!=null) {
			if(!user.getStatus().equals(UserStatus.ACTIVE)){
				logger.warn("Login UNSUCCESSFUL for USER: "+username+" - USER IS: "+user.getStatus());
				MessageGenerator.sendRedirectMessage(Constants.INDEX_PAGE, response);
				return;
			}
			request.getSession().invalidate();
			String sessionId = request.getSession(true).getId();
			request.getSession().setAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT, user);
			request.getSession().setAttribute(Constants.ATTRIBUTE_SECURITY_ROLE, user.getRole());
			CSRFTokenUtils.setToken(request.getSession());
			
			if(user.getForceChangePassword()) {
				logger.debug("Login successful for "+user.getUsername()+" redirecting to password change...");
				MessageGenerator.sendRedirectMessage(Constants.PASSWORD_CHANGE, response);
			}
			else if(!user.getEmailVerified()) {
				logger.debug("Login successful for "+user.getUsername()+" redirecting to email validation...");
				MessageGenerator.sendRedirectMessage(Constants.EMAIL_VALIDATION, response);
			}
			else if(user.getRole()<=Constants.ROLE_STATS){
				logger.debug("Login successful for ADMIN: "+user.getUsername());
				MessageGenerator.sendRedirectMessage(Constants.MGMT_HOME, response);
			}
			else{
				MessageGenerator.sendRedirectMessage(Constants.USER_HOME, response);		
				logger.debug("Login successful for USER: "+user.getUsername());
			}
			user.setLastLogin(new Date());
			hpc.updateUserInfo(user);
			attempt.setSessionIdHash(DigestUtils.sha256Hex(sessionId));
			attempt.setLoginSuccessful(true);
			attempt.setLoginDate(new Date());
			hpc.addLoginEvent(attempt);
			//hpc.setFailedLoginAttemptsForUser(username,0);
			return;
		} else {
			MessageGenerator.sendRedirectMessage(Constants.INDEX_PAGE, response);
			failedAttempts++;
			attempt.setLoginSuccessful(false);
			attempt.setLoginDate(new Date());
			hpc.addLoginEvent(attempt);
			//hpc.setFailedLoginAttemptsForUser(username,failedAttempts);
			logger.warn("Login UNSUCCESSFUL for USER: "+username);
			return;
		}	
	}
}