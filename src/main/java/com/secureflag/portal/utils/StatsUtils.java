/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.dto.StatsObject;

public class StatsUtils {

	private static long getDateDiff(Date date1, Date date2) {

		long diffInMillies = date2.getTime() - date1.getTime();
		return TimeUnit.MILLISECONDS.toMinutes(diffInMillies);


	}
	public void setTimePerCategory(List<ExerciseInstance> exerciseInstances, StatsObject stats){

		Map<String, Long> totalTimeEach = new HashMap<String, Long>();
		Map<String, Integer> totalInstancesEach = new HashMap<String, Integer>();
		Map<String, Integer> map = new HashMap<String, Integer>();

		for(ExerciseInstance ei : exerciseInstances){
			String category = ei.getExercise().getQuestionsList().iterator().next().getCategory();
			Integer dur = ei.getDuration();
			if(null==dur){
				dur = (int) getDateDiff(ei.getStartTime(),ei.getEndTime());
			}
			if(!totalTimeEach.containsKey(category)){
				totalTimeEach.put(category, (long) 0);
			}
			if(!totalInstancesEach.containsKey(category)){
				totalInstancesEach.put(category, 0);
			}
			totalTimeEach.put(category, (totalTimeEach.get(category)+dur));
			totalInstancesEach.put(category, (totalInstancesEach.get(category)+1));
		}
		for(String key : totalTimeEach.keySet()){
			map.put(key,(int) Math.floor(totalTimeEach.get(key)/totalInstancesEach.get(key)));
		}
		stats.setAverageMinutesPerIssueCategory(map);
		stats.setTotalMinutesPerIssueCategory(totalTimeEach);
	}
	public void setTimePerRegion(List<ExerciseInstance> exerciseInstances, StatsObject stats){
		Map<String, Long> totalTimeEach = new HashMap<String, Long>();
		Map<String, Integer> totalInstancesEach = new HashMap<String, Integer>();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for(ExerciseInstance ei : exerciseInstances){
			String region = ei.getRegion().toString();
			Integer dur = ei.getDuration();
			if(null==dur){
				dur = (int) getDateDiff(ei.getStartTime(),ei.getEndTime());
			}
			if(!totalTimeEach.containsKey(region)){
				totalTimeEach.put(region, (long) 0);
			}
			if(!totalInstancesEach.containsKey(region)){
				totalInstancesEach.put(region, 0);
			}
			totalTimeEach.put(region, (totalTimeEach.get(region)+dur));
			totalInstancesEach.put(region, (totalInstancesEach.get(region)+1));
		}
		for(String key : totalTimeEach.keySet()){

			map.put(key,(int) Math.floor(totalTimeEach.get(key)/totalInstancesEach.get(key)));
		}
		stats.setAverageMinutesPerRegion(map);
		stats.setTotalMinutesPerRegion(totalTimeEach);
	}
	public void setTimePerTeam(List<ExerciseInstance> exerciseInstances, StatsObject stats){
		Map<String, Long> totalTimeEach = new HashMap<String, Long>();
		Map<String, Integer> totalInstancesEach = new HashMap<String, Integer>();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for(ExerciseInstance ei : exerciseInstances){
			if(null==ei.getUser() || null==ei.getUser().getTeam())
				continue;
			String team = ei.getUser().getTeam().getName();
			Integer dur = ei.getDuration();
			if(null==dur){
				dur = (int) getDateDiff(ei.getStartTime(),ei.getEndTime());
			}
			if(!totalTimeEach.containsKey(team)){
				totalTimeEach.put(team, (long) 0);
			}
			if(!totalInstancesEach.containsKey(team)){
				totalInstancesEach.put(team, 0);
			}
			totalTimeEach.put(team, (totalTimeEach.get(team)+dur));
			totalInstancesEach.put(team, (totalInstancesEach.get(team)+1));
		}
		for(String key : totalTimeEach.keySet()){
			map.put(key,(int) Math.floor(totalTimeEach.get(key)/totalInstancesEach.get(key)));
		}
		stats.setAverageMinutesPerTeam(map);
		stats.setTotalMinutesPerTeam(totalTimeEach);
	}

	public Map<String,Map<String,Integer>> getRemediationRatePerRegion(List<ExerciseInstance> exerciseInstances){ 

		Map<String,Map<String,Integer>> irr = new HashMap<String,Map<String,Integer>>();

		for(ExerciseInstance ei : exerciseInstances){
			if(!irr.containsKey(ei.getRegion().toString())){
				Map<String,Integer> tmpMap = new HashMap<String,Integer>();
				tmpMap.put(ExerciseInstanceResultStatus.NOT_VULNERABLE.toString(),0);
				tmpMap.put(ExerciseInstanceResultStatus.PARTIALLY_REMEDIATED.toString(),0);
				tmpMap.put(ExerciseInstanceResultStatus.VULNERABLE.toString(),0);
				tmpMap.put(ExerciseInstanceResultStatus.BROKEN_FUNCTIONALITY.toString(),0);
				tmpMap.put(ExerciseInstanceResultStatus.NOT_ADDRESSED.toString(),0);

				irr.put(ei.getRegion().toString(), tmpMap);
			}
			Map<String,Integer> tmpStatus = irr.get(ei.getRegion().toString());
			for(ExerciseInstanceResult er : ei.getResults()){
				try {
					if(null!=tmpStatus.get(er.getStatus().toString()))
						tmpStatus.put(er.getStatus().toString(), (tmpStatus.get(er.getStatus().toString())+1));
				}catch(Exception e) {
					continue;
				}
			}
		}
		return irr;
	}
	public Map<String,Map<String,Integer>> getRemediationRatePerTeam(List<ExerciseInstance> exerciseInstances){ 

		Map<String,Map<String,Integer>> irr = new HashMap<String,Map<String,Integer>>();

		for(ExerciseInstance ei : exerciseInstances){
			if(null==ei.getUser().getTeam())
				continue;
			if(!irr.containsKey(ei.getUser().getTeam().getName())){
				Map<String,Integer> tmpMap = new HashMap<String,Integer>();
				tmpMap.put(ExerciseInstanceResultStatus.NOT_VULNERABLE.toString(),0);
				tmpMap.put(ExerciseInstanceResultStatus.VULNERABLE.toString(),0);
				tmpMap.put(ExerciseInstanceResultStatus.PARTIALLY_REMEDIATED.toString(),0);
				tmpMap.put(ExerciseInstanceResultStatus.BROKEN_FUNCTIONALITY.toString(),0);
				tmpMap.put(ExerciseInstanceResultStatus.NOT_ADDRESSED.toString(),0);
				irr.put(ei.getUser().getTeam().getName(), tmpMap);
			}
			Map<String,Integer> tmpStatus = irr.get(ei.getUser().getTeam().getName());
			for(ExerciseInstanceResult er : ei.getResults()){
				try {
					tmpStatus.put(er.getStatus().toString(), (tmpStatus.get(er.getStatus().toString())+1));
				}catch(Exception e) {
					continue;
				}
			}
		}
		return irr;
	}
	public Map<String,Map<String,Integer>> getRemediationRatePerIssueCategory(List<ExerciseInstance> exerciseInstances){ 

		Map<String,Map<String,Integer>> irr = new HashMap<String,Map<String,Integer>>();

		for(ExerciseInstance ei : exerciseInstances){
			for(ExerciseInstanceResult er : ei.getResults()){
				if(!irr.containsKey(er.getCategory())){
					Map<String,Integer> tmpMap = new HashMap<String,Integer>();
					tmpMap.put(ExerciseInstanceResultStatus.NOT_VULNERABLE.toString(),0);
					tmpMap.put(ExerciseInstanceResultStatus.PARTIALLY_REMEDIATED.toString(),0);
					tmpMap.put(ExerciseInstanceResultStatus.VULNERABLE.toString(),0);
					tmpMap.put(ExerciseInstanceResultStatus.BROKEN_FUNCTIONALITY.toString(),0);
					tmpMap.put(ExerciseInstanceResultStatus.NOT_ADDRESSED.toString(),0);
					irr.put(er.getCategory(), tmpMap);
				}
				Map<String,Integer> tmpStatus = irr.get(er.getCategory());
				try {

					tmpStatus.put(er.getStatus().toString(), (tmpStatus.get(er.getStatus().toString())+1));
				}catch(Exception e) {
					continue;
				}
			}
		}
		return irr;
	}
	public Map<String,Map<String,Integer>> getRemediationRatePerIssue(List<ExerciseInstance> exerciseInstances){ 

		Map<String,Map<String,Integer>> irr = new HashMap<String,Map<String,Integer>>();

		for(ExerciseInstance ei : exerciseInstances){
			for(ExerciseInstanceResult er : ei.getResults()){
				String title = er.getFlagTitle() + " ("+ei.getTechnology()+")";
				if(!irr.containsKey(title)){
					Map<String,Integer> tmpMap = new HashMap<String,Integer>();
					tmpMap.put(ExerciseInstanceResultStatus.NOT_VULNERABLE.toString(),0);
					tmpMap.put(ExerciseInstanceResultStatus.PARTIALLY_REMEDIATED.toString(),0);
					tmpMap.put(ExerciseInstanceResultStatus.VULNERABLE.toString(),0);
					tmpMap.put(ExerciseInstanceResultStatus.BROKEN_FUNCTIONALITY.toString(),0);
					tmpMap.put(ExerciseInstanceResultStatus.NOT_ADDRESSED.toString(),0);
					irr.put(title, tmpMap);
				}
				Map<String,Integer> tmpStatus = irr.get(title);
				try {
					tmpStatus.put(er.getStatus().toString(), (tmpStatus.get(er.getStatus().toString())+1));
				}catch(Exception e) {
					continue;
				}
			}
		}
		return irr;
	}	
}