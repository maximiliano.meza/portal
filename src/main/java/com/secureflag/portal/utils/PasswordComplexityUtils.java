/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.utils;

public class PasswordComplexityUtils {

	public static boolean isPasswordComplex(String password) {
		boolean isAtLeast8   = password.length() >= 8;//Checks for at least 8 characters
		boolean hasSpecial   = !password.matches("[A-Za-z0-9 ]*");//Checks at least one char is not alpha numeric
		boolean hasUppercase = !password.equals(password.toLowerCase());
		boolean hasLowercase = !password.equals(password.toUpperCase());
		boolean hasNumber  = password.matches(".*\\d.*");  // "a digit with anything before or after"

		boolean isComplex = false;
		if(isAtLeast8){
			Integer count = 0;
			if(hasSpecial)
				count++;
			if(hasUppercase)
				count++;
			if(hasLowercase)
				count++;
			if(hasNumber)
				count++;
			if(count>=3)
				isComplex = true;
		}
		return isComplex;
	}
}
