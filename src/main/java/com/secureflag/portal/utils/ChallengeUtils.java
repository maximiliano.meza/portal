package com.secureflag.portal.utils;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceType;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;

public class ChallengeUtils { // NO_UCD 

	private static Logger logger = LoggerFactory.getLogger(ChallengeUtils.class);

	public static void addExerciseUpdateChallenge(Challenge c, ExerciseInstance ei) {
		if(ei.getType().equals(ExerciseInstanceType.CHALLENGE)) {
			c.getRunExercises().add(ei);
			Double completion = 0.0;
			Integer nrExercises = c.getScopeExercises().size();
			Integer runExercises = c.getRunExercises().size();
			try {
				if(runExercises != 0 && null!=c.getUsers() && !c.getUsers().isEmpty() && !c.getRunExercises().isEmpty()) {
					completion = (double) ( (double) runExercises / ((double) nrExercises * (double) c.getUsers().size())) * 100.0;
				}
			}catch(Exception e) {
				logger.warn(e.getMessage());
			}
			c.setCompletion(completion);
			c.setLastActivity(new Date());
		}
	}

	public static void updateChallenge(Challenge c) {
		Double completion = 0.0;
		Integer nrExercises = c.getScopeExercises().size();
		Integer runExercises = c.getRunExercises().size();
		try {
			if(runExercises != 0 && null!=c.getUsers() && !c.getUsers().isEmpty() && !c.getRunExercises().isEmpty()) {
				completion = (double) ( (double) runExercises / ((double) nrExercises * (double) c.getUsers().size())) * 100.0;
			}
		}catch(Exception e) {
			logger.warn(e.getMessage());
		}
		c.setCompletion(completion);
		c.setLastActivity(new Date());
	}

	public static Integer getFlagPositionInChallenge(Challenge challenge, FlagQuestion fq) {
		Integer instances = -1;
		for(ExerciseInstance i : challenge.getRunExercises()) {
			if(hasRun(i, fq)) {
				instances++;
			}
		}
		return instances;
	}

	public static Integer getIncreaseResultFromPosition(Challenge challenge, Integer position) {
		Integer result = null;
		switch(position) {
		case 0:
			result = challenge.getFirstInFlag();
			break;
		case 1:
			result = challenge.getSecondInFlag();
			break;
		case 2:
			result = challenge.getThirdInFlag();
			break;
		}	
		if(null== result || result==0)
			return null;
		else
			return result;
	}

	private static Boolean hasRun(ExerciseInstance i, FlagQuestion fq) {
		for(Flag f : i.getAvailableExercise().getQuestionsList()) {
			for(FlagQuestion flq : f.getFlagQuestionList()) {
				if(flq.getSelfCheckAvailable() && flq.getSelfCheck().getName().equals(fq.getSelfCheck().getName())) {
					return true;
				}
			}
		}
		return false;
	}


}
