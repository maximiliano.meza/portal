package com.secureflag.portal.utils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonElement;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.FlagQuestionHint;
import com.secureflag.portal.model.dto.SelfCheckResult;

public class ExerciseUtils {

	private static Integer getHintPercentageReduction(FlagQuestionHint hint, ExerciseInstance exerciseInstance) {
		for(FlagQuestionHint usedHint : exerciseInstance.getUsedHints()) {
			if(usedHint.getId().equals(hint.getId())) {
				return hint.getScoreReducePercentage();
			}
		}
		return null;
	}

	public static Boolean isInScope(String name, Set<Flag> flags) {
		for(Flag f :flags) {
			for(FlagQuestion fq : f.getFlagQuestionList()) {
				if(fq.getSelfCheckAvailable() && fq.getSelfCheck().getName().equals(name))
					return true;
			}
		}
		return false;
	}
	
	
	public static Integer updateExerciseResults(ExerciseInstance instance, List<SelfCheckResult> results, Challenge exerciseChallenge) {
		Integer totalScore = 0;
		for(ExerciseInstanceResult orig : instance.getResults()){
			for(SelfCheckResult rev : results){
				if(null!= rev && null!=rev.getName() && null!=rev.getStatus() && null!= orig && null!=orig.getStatus() && null!=orig.getName() && rev.getName().equals(orig.getName()) && !orig.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !orig.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED) && !rev.getStatus().equals(orig.getCheckerStatus())) {
					FlagQuestion fq = getFlagQuestionFromSelfcheckName(instance,rev.getName());					
					if(fq!=null) {
						orig.setStatus(fq.getSelfCheck().getStatusMapping().get(rev.getStatus()));
						if(null!=orig.getStatus() && (orig.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || orig.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
							Integer hintPercentageReduction = ExerciseUtils.getHintPercentageReduction(fq.getHint(),instance);
							if(null==hintPercentageReduction) {
								orig.setScore(fq.getMaxScore());
							}
							else {
								orig.setHint(true);
								orig.setScore(fq.getMaxScore() - (int)Math.round((fq.getMaxScore() * hintPercentageReduction) / 100.0));
							}
							if(null!=exerciseChallenge) {
								Integer challengePosition = ChallengeUtils.getFlagPositionInChallenge(exerciseChallenge, fq);
								Integer challengePositionIncrease = ChallengeUtils.getIncreaseResultFromPosition(exerciseChallenge, challengePosition);
								if(null!=challengePositionIncrease) {
									switch(challengePosition) {
									case 0:
										orig.setFirstForFlag(true);
										break;
									case 1:
										orig.setSecondForFlag(true);
										break;
									case 2:
										orig.setThirdForFlag(true);
										break;
									}	
									orig.setScore(orig.getScore() + (int)Math.ceil((fq.getMaxScore() * challengePositionIncrease) / 100.0));
								}
							}
						}
						orig.setLastChange(new Date());
						totalScore+=orig.getScore();
					}
					break;
				}
			}
		}
		return totalScore;
	}
	private static FlagQuestion getFlagQuestionFromSelfcheckName(ExerciseInstance instance, String name) {
		for(Flag f : instance.getAvailableExercise().getQuestionsList()) {
			for(FlagQuestion fq : f.getFlagQuestionList()) {
				if(fq.getSelfCheckAvailable() && fq.getSelfCheck().getName().equals(name)){
					return fq;
				}
			}
		}
		return null;
	}

	public static ExerciseInstanceResult getExerciseResult(ExerciseInstance instance, Flag flag, FlagQuestion fq, List<SelfCheckResult> results, Challenge exerciseChallenge ) {
		for(SelfCheckResult rev : results){
			if(fq.getSelfCheckAvailable() && fq.getSelfCheck().getName().equals(rev.getName())){
				ExerciseInstanceResult result = new ExerciseInstanceResult();

				result.setStatus(fq.getSelfCheck().getStatusMapping().get(rev.getStatus()));
				result.setCheckerStatus(rev.getStatus());
				
				if(null==result.getStatus())
					result.setStatus(ExerciseInstanceResultStatus.NOT_AVAILABLE);

				if(!result.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !result.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED)) {
					result.setScore(0);
				}
				else {
					Integer hintPercentageReduction = ExerciseUtils.getHintPercentageReduction(fq.getHint(),instance);
					if(null==hintPercentageReduction) {
						result.setScore(fq.getMaxScore());
					}
					else {
						result.setHint(true);
						result.setScore(fq.getMaxScore() - (int)Math.ceil((fq.getMaxScore() * hintPercentageReduction) / 100.0));
					}
					if(null!=exerciseChallenge) {
						Integer challengePosition = ChallengeUtils.getFlagPositionInChallenge(exerciseChallenge, fq);
						Integer challengePositionIncrease = ChallengeUtils.getIncreaseResultFromPosition(exerciseChallenge, challengePosition);
						if(null!=challengePositionIncrease) {
							switch(challengePosition) {
							case 0:
								result.setFirstForFlag(true);
								break;
							case 1:
								result.setSecondForFlag(true);
								break;
							case 2:
								result.setThirdForFlag(true);
								break;
							}	
							result.setScore(result.getScore() + (int)Math.ceil((fq.getMaxScore() * challengePositionIncrease) / 100.0));
						}

					}

				}
				result.setComment(null);
				result.setLastChange(new Date());
				result.setVerified(false);
				if(instance.getScoring().equals(AvailableExerciseExerciseScoringMode.MANUAL_REVIEW)) {
					result.setAutomated(false);
				}
				else {
					result.setAutomated(true);
				}
				result.setName(rev.getName());
				result.setFlagTitle(flag.getTitle());
				result.setCategory(flag.getCategory());
				result.setType(fq.getType());
				return result;
			}
		}
		return null;
	}

	public static ExerciseInstanceResult getExerciseResult(ExerciseInstance instance,Challenge exerciseChallenge, Map.Entry<String, JsonElement> entry) {
		for(Flag flag : instance.getAvailableExercise().getQuestionsList()){
			for(FlagQuestion fq : flag.getFlagQuestionList()){
				if(fq.getSelfCheckAvailable() && fq.getSelfCheck().getName().equals(entry.getKey())){
					ExerciseInstanceResult r = new ExerciseInstanceResult();
					r.setName(entry.getKey());
					r.setCategory(flag.getCategory());
					r.setType(fq.getType());
					r.setVerified(false);
					r.setFlagTitle(flag.getTitle());
					r.setLastChange(new Date());
					if(instance.getScoring().equals(AvailableExerciseExerciseScoringMode.MANUAL_REVIEW))
						r.setAutomated(false);
					else
						r.setAutomated(true);

					String tmpStatus = entry.getValue().getAsString();
					r.setStatus(fq.getSelfCheck().getStatusMapping().get(tmpStatus));
					r.setCheckerStatus(tmpStatus);

					if(!r.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !r.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED)) {
						r.setScore(0);
					}
					else{
						Integer hintPercentageReduction = ExerciseUtils.getHintPercentageReduction(fq.getHint(),instance);
						if(null==hintPercentageReduction) {
							r.setScore(fq.getMaxScore());
						}
						else {
							r.setHint(true);
							r.setScore(fq.getMaxScore() - (int)Math.ceil((fq.getMaxScore() * hintPercentageReduction) / 100.0));
						}
						if(null!=exerciseChallenge) 	{
							Integer challengePosition = ChallengeUtils.getFlagPositionInChallenge(exerciseChallenge, fq);
							Integer challengePositionIncrease = ChallengeUtils.getIncreaseResultFromPosition(exerciseChallenge, challengePosition);
							if(null!=challengePositionIncrease) {
								switch(challengePosition) {
								case 0:
									r.setFirstForFlag(true);
									break;
								case 1:
									r.setSecondForFlag(true);
									break;
								case 2:
									r.setThirdForFlag(true);
									break;
								}	
								r.setScore(r.getScore() + (int)Math.ceil((fq.getMaxScore() * challengePositionIncrease) / 100.0));
							}
						}
					}
					return r;
				}
			}
		}
		return null;
	}
}