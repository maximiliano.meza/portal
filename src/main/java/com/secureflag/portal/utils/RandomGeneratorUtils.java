/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.utils;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Random;

public class RandomGeneratorUtils {

	private static final Random RANDOM = new SecureRandom();
	private static final String symbols = "ABCDEFGJKLMNPRSTUVWXYZ0123456789"; 
	
	public static String getNextSalt() {
		byte[] salt = new byte[16];
		RANDOM.nextBytes(salt);
		return Base64.getEncoder().encodeToString(salt);
	}

	public static String getNextString(Integer length){
		char[] buf = new char[length];
		for (int idx = 0; idx < buf.length; ++idx) 
			buf[idx] = symbols.charAt(RANDOM.nextInt(symbols.length()));
		return new String(buf);
	}

}
