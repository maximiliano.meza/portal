/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.tasks;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.AchievementType;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ChallengePlacement;
import com.secureflag.portal.model.ChallengeStatus;
import com.secureflag.portal.model.ChallengeWinner;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAchievement;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class ChallengesStatusUpdateTask implements Runnable {

	private static Logger logger = LoggerFactory.getLogger(ChallengesStatusUpdateTask.class);
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private NotificationsHelper notificationsHelper = new NotificationsHelper();

	@Override
	public void run() {

		if(!SFConfig.isTaskCoordinationModuleAvailable()) {
			logger.debug("Skipping sync task as member is not master");
			return;
		}
		logger.debug("Running Challenge Status Update Task");

		List<Challenge> expired = hpc.getAllExpiredChallenges();
		logger.debug("Returned "+expired.size()+" expired Challenges");
		for(Challenge expiredChallenge : expired) {
			expiredChallenge.setStatus(ChallengeStatus.FINISHED);
			expiredChallenge.setLastActivity(new Date());
			Double completion = 0.0;
			Integer nrExercises = expiredChallenge.getScopeExercises().size();
			Integer runExercises = expiredChallenge.getRunExercises().size();
			try {
				if(runExercises != 0 && null!=expiredChallenge.getUsers() && !expiredChallenge.getUsers().isEmpty() && !expiredChallenge.getRunExercises().isEmpty()) {
					completion = (double) ( (double) runExercises / ((double) nrExercises * (double) expiredChallenge.getUsers().size())) * 100.0;
				}
			}catch(Exception e) {
				logger.error("Could not update completion for challenge "+expiredChallenge.getIdChallenge()+" gue to "+e.getMessage());
			}
			if(completion!=0.0)
				expiredChallenge.setCompletion(completion);
			Map<User, Integer> scoreMap = new HashMap<User,Integer>();
			Map<User, Integer> sorted = new HashMap<User,Integer>();
			try {
				for(ExerciseInstance i : expiredChallenge.getRunExercises()) {
					if(i.getStatus().equals(ExerciseInstanceStatus.AUTOREVIEWED) || i.getStatus().equals(ExerciseInstanceStatus.REVIEWED) || i.getStatus().equals(ExerciseInstanceStatus.REVIEWED_MODIFIED)){
						if(scoreMap.containsKey(i.getUser())) 
							scoreMap.put(i.getUser(), scoreMap.get(i.getUser())+ i.getScore().getResult());
						else 
							scoreMap.put(i.getUser(), i.getScore().getResult());
					}
				}
				sorted = scoreMap.entrySet()
						.stream()
						.sorted((Map.Entry.<User, Integer>comparingByValue().reversed()))
						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
			}catch(Exception e) {
				logger.error("Could not create score map and winner for challenge "+expiredChallenge.getIdChallenge()+" gue to "+e.getMessage());
			}
			Integer index = 0;
			Integer oldScore = -1;
			List<ChallengeWinner> winners = new LinkedList<ChallengeWinner>();
			for(User u : sorted.keySet()) {
				if(!sorted.get(u).equals(oldScore))
					index++;
				oldScore = sorted.get(u);
				if(index<=3) {
					ChallengeWinner tmpWinner = new ChallengeWinner(u.getIdUser(),oldScore);
					winners.add(tmpWinner);
				}
				Boolean found = false;
				List<UserAchievement> achievements = hpc.getAllAchievementsForUser(u.getIdUser(),AchievementType.CHALLENGE);
				for(UserAchievement a : achievements) {
					ChallengePlacement placement = (ChallengePlacement) a.getAchievement();
					if(placement.getChallengeId().equals(expiredChallenge.getIdChallenge())) {
						placement.setPlacement(index);
						a.setAchievement(placement);
						hpc.upateUserAchievement(a);
						found = true;
						break;
					}
				}
				if(!found) {
					UserAchievement a = new UserAchievement();
					ChallengePlacement placement = new ChallengePlacement();
					placement.setChallengeId(expiredChallenge.getIdChallenge());
					placement.setName(expiredChallenge.getName());
					placement.setPlacement(index);
					placement.setType(AchievementType.CHALLENGE);
					a.setAchievement(placement);
					a.setDate(new Date());
					a.setUser(u);
					hpc.managementAddAchievedTrophy(a);
					notificationsHelper.addPlacementNotification(u,placement);

				}
			}
			expiredChallenge.setWinners(winners);
			hpc.updateChallenge(expiredChallenge);
			logger.debug("Updating status for challenge "+expiredChallenge.getName()+" to COMPLETED");
		}	
		List<Challenge> notStarted = hpc.getAllNotStartedChallenges();
		logger.debug("Returned "+notStarted.size()+" not started Challenges");

		for(Challenge notStartedChallenge : notStarted) {
			notStartedChallenge.setStatus(ChallengeStatus.IN_PROGRESS);
			notStartedChallenge.setLastActivity(new Date());
			hpc.updateChallenge(notStartedChallenge);
			logger.debug("Updating status for challenge "+notStartedChallenge.getName()+" to IN PROGRESS");
		}
		logger.debug("Completed Challenge Status Update Task");
	}
}