package com.secureflag.portal.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.secureflag.portal.hub.HubAuthenticationHelper;

public class RetrieveHubTokenTask implements Runnable {
	
	private HubAuthenticationHelper hubHelper = new HubAuthenticationHelper();
	private static Logger logger = LoggerFactory.getLogger(RetrieveHubTokenTask.class);

	@Override
	public void run() {
		String jwt = hubHelper.getCurrentJwtAuth();
		if(null!=jwt) {
			logger.debug("Fetched JWT token to authenticate to HUB");
		}
		else {
			logger.warn("Could not authenticate to HUB");
		}
	}
}