package com.secureflag.portal.tasks;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class ContainersEventsSQSTask implements Runnable {

	private static Logger logger = LoggerFactory.getLogger(ContainersEventsSQSTask.class);
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private SQSConnection connection;
	private Session session;

	private static class ReceiverCallback implements MessageListener {
		@Override
		public void onMessage(Message message) {
			try {
				handleMessage(message);
			} catch (JMSException e) {
				logger.error( "Error processing message: " + e.getMessage() );
			}
		}
	}	
	private static void handleMessage(Message message) throws JMSException {
		if( message instanceof TextMessage ) {
			TextMessage txtMessage = ( TextMessage ) message;
			try {
				JSONObject obj = (JSONObject) new JSONParser().parse(txtMessage.getText().toString());
				JSONObject detail = (JSONObject) obj.get(Constants.AWS_CLOUDWATCH_DETAIL);
				String group = (String) detail.get(Constants.AWS_CLOUDWATCH_GROUP);
				if(group != null && SFConfig.getSysGroups().contains(group)) {
					return;
				}
				String lastStatus = (String) detail.get(Constants.AWS_CLOUDWATCH_LASTSTATUS);
				if(null != lastStatus && lastStatus.equalsIgnoreCase(Constants.AWS_ECS_STATUS_STOPPED)) {
					String stoppedReason = (String) detail.get(Constants.AWS_CLOUDWATCH_STOPPEDREASON);
					logger.debug("Container stopped reason is: "+stoppedReason);
					if(null!= stoppedReason && !stoppedReason.equalsIgnoreCase(Constants.AWS_ECS_STOPPED_NORMAL)) {
						String taskArn = (String) detail.get("taskArn");
						logger.debug("Container task arn: "+taskArn+" crashed..");
						HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
						ExerciseInstance ei = hpc.getExerciseInstanceByArn(taskArn);
						if(null!=ei && (ei.getCrashReason() == null || ei.getCrashReason().equals(""))) {
							logger.debug("Updating status for ExerciseInstance: "+ei.getIdExerciseInstance()+" to crashed..");
							ei.setStatus(ExerciseInstanceStatus.CRASHED);
							ei.setCrashReason(stoppedReason);
							ei.setHasCrashed(true);
							hpc.updateExerciseInstance(ei);
						}
					}
				}
			} catch (ParseException e) {
				logger.warn("Could not parse message "+ message.getJMSMessageID()+" from SQS");
			} 
		} 
	}
	

	
	@Override
	public void run() {
		
		if(!SFConfig.isTaskCoordinationModuleAvailable()) {
			logger.debug("Skipping ContainersEventsSQS task as member is not master");
			return;
		}
		
		logger.debug("ContainersEventsSQS task running...");
		try {
			session.close();
			connection.stop();
		}catch(Exception e) {
		}
		List<Gateway> gateways = hpc.getAllActiveGateways();
		for(Gateway gateway : gateways) {
			try {
				SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
						new ProviderConfiguration(),
						AmazonSQSClientBuilder.standard().withRegion(gateway.getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain())
						);
				connection = connectionFactory.createConnection();
				session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				MessageConsumer consumer = session.createConsumer( session.createQueue( Constants.EXERCISE_EVENTS_QUEUE_NAME ) );

				ReceiverCallback callback = new ReceiverCallback();
				consumer.setMessageListener( callback );
				connection.start();
				logger.debug(" Connected to SQS Queue "+Constants.EXERCISE_EVENTS_QUEUE_NAME+" in region "+gateway.getRegion().toString());
			} catch (JMSException jmse) {
				logger.error(" ASyncMessageReceiver: Can't create connection for SQS Queue in region "+gateway.getRegion().toString()+" due to:\n" + jmse.toString());
				continue;
			}
		}
	}
}