/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.tasks;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.regions.Region;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.gateway.GatewayHelper;
import com.secureflag.portal.gateway.GuacamoleHelper;
import com.secureflag.portal.messages.notifications.NotificationsHelper;
import com.secureflag.portal.model.AchievementType;
import com.secureflag.portal.model.AvailableExerciseExerciseScoringMode;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultFile;
import com.secureflag.portal.model.ExerciseInstanceResultStatus;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.Trophy;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAchievement;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class ExercisesAutoShutdownTask implements Runnable {

	private static Logger logger = LoggerFactory.getLogger(ExercisesAutoShutdownTask.class);
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private GatewayHelper gwHelper = new GatewayHelper();
	private AWSHelper awsHelper = new AWSHelper();
	private NotificationsHelper notificationsHelper = new NotificationsHelper();

	@Override
	public void run() {

		if(!SFConfig.isTaskCoordinationModuleAvailable()) {
			logger.debug("Skipping Exercises Auto Shutdown Task as member is not master");
			return;
		}
		logger.debug("Exercises Auto Shutdown Task");

		//get running exercise instances from db, returns ExerciseInstance
		List<ExerciseInstance> exerciseInstances = hpc.getAllRunningExerciseInstancesWithAwsGWFlag();
		logger.debug("Returned "+exerciseInstances.size()+" running exercise instances from db");

		// get active regions
		List<Region> activeRegions = new LinkedList<Region>();
		for(Gateway gw : hpc.getAllActiveGateways()){
			activeRegions.add(Region.getRegion(gw.getRegion()));
		}
		logger.debug("Returned "+activeRegions.size()+" active regions from db");

		// get running ecs instances from AWS for active regions
		List<String> runningTasks = awsHelper.getRunningECSTasks(activeRegions);
		logger.debug("Returned "+runningTasks.size()+" running tasks for aws active regions");		

		Date now = new Date();
		for(String taskArn : runningTasks){
			boolean found = false;
			for(ExerciseInstance instance : exerciseInstances){
				if(instance.getEcsInstance().getTaskArn().equals(taskArn)){
					found = true;
					if(instance.getEndTime().getTime() < now.getTime()) {
						logger.debug("Exercise "+instance.getIdExerciseInstance()+" needs to be shutdown");
						Thread stopExerciseThread = new Thread(new Runnable() {
							@Override
							public void run() {
								if(!instance.getStatus().equals(ExerciseInstanceStatus.REVIEWED) && !instance.getStatus().equals(ExerciseInstanceStatus.AUTOREVIEWED) && !instance.getStatus().equals(ExerciseInstanceStatus.REVIEWED_MODIFIED)){
									logger.debug("Pulling exercise result file for exercise instance "+instance.getIdExerciseInstance());
									// get results
									ExerciseInstanceResultFile fr = gwHelper.getResultFile(instance);
									instance.setResultFile(fr);

									Challenge exerciseChallenge = null;
									if(null!=instance.getChallengeId()) {
										exerciseChallenge = hpc.getChallengeCompleteManagement(instance.getChallengeId());
										exerciseChallenge.setLastActivity(new Date());
										hpc.updateChallenge(exerciseChallenge);
									}

									List<ExerciseInstanceResult> results = gwHelper.getResultStatus(instance,exerciseChallenge);
									if(!instance.getResults().isEmpty()) {
										for(ExerciseInstanceResult origRes : instance.getResults()){
											for(ExerciseInstanceResult newRes : results){
												if(newRes.getName().equals(origRes.getName())){
													if(!origRes.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !origRes.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED) && !newRes.getStatus().equals(origRes.getStatus())) {
														origRes.setStatus(newRes.getStatus());
														origRes.setCheckerStatus(newRes.getCheckerStatus());
														origRes.setScore(newRes.getScore());
														origRes.setLastChange(new Date());
														origRes.setFirstForFlag(newRes.getFirstForFlag());
														origRes.setSecondForFlag(newRes.getSecondForFlag());
														origRes.setThirdForFlag(newRes.getThirdForFlag());
													}
													break;
												}
											}
										}
										hpc.updateExerciseInstance(instance);

									}
									else {
										instance.setResults(results);
									}
									if(instance.getResults().isEmpty()) {
										for(Flag flag : instance.getAvailableExercise().getQuestionsList()){
											for(FlagQuestion fq : flag.getFlagQuestionList()) {
												if(!fq.getOptional()) {
													ExerciseInstanceResult result = new ExerciseInstanceResult();
													result.setAutomated(true);
													result.setCategory(flag.getCategory());
													result.setScore(0);
													result.setStatus(ExerciseInstanceResultStatus.NOT_AVAILABLE);
													result.setVerified(false);
													result.setLastChange(new Date());
													result.setFlagTitle(flag.getTitle());
													instance.getResults().add(result);
												}
											}
										}
									}
									Calendar cal = Calendar.getInstance();
									instance.setEndTime(cal.getTime());

									GuacamoleHelper guacHelper = new GuacamoleHelper();
									try {
										Integer duration = guacHelper.getUserExerciseDuration(instance.getGuac());
										instance.setDuration(duration);
									}catch(Exception e) {
										logger.error("Could not get duration for instance "+instance.getIdExerciseInstance()+e.getMessage());
										long diffInMillis = instance.getEndTime().getTime() - instance.getStartTime().getTime();
										Integer diff = (int) TimeUnit.MINUTES.convert(diffInMillis, TimeUnit.MILLISECONDS);
										instance.setDuration(diff);
									}
									// stop instance
									AWSHelper awsHelper = new AWSHelper();
									if(null!=instance.getEcsInstance()){
										awsHelper.terminateTask(instance.getEcsInstance().getTaskArn());
										instance.getEcsInstance().setStatus(Constants.STATUS_STOPPED);
									}
									if(null!=instance.getEc2Instance()) {
										if(awsHelper.terminateEC2Instance(instance.getRegion(), instance.getEc2Instance().getInstanceId())) {
											instance.getEc2Instance().setStatus(Constants.STATUS_STOPPED);
											instance.getEc2Instance().setEndTime(new Date());
											awsHelper.removeKeypair(instance.getEc2Instance().getKeyPairName(), instance.getRegion());
										}
										else {
											logger.error("Could not terminate ec2 instance "+instance.getEc2Instance().getInstanceId()+" and its corresponding keypair");
										}
									}

									User dbUser = hpc.getUserFromUserId(instance.getUser().getIdUser());

									if(instance.getScoring().equals(AvailableExerciseExerciseScoringMode.MANUAL_REVIEW)) {
										instance.setStatus(ExerciseInstanceStatus.STOPPED);
										dbUser.setExercisesRun(dbUser.getExercisesRun() + 1);
										hpc.updateExerciseInstance(instance);
										hpc.updateUserInfo(dbUser);
									}
									else {
										instance.setStatus(ExerciseInstanceStatus.AUTOREVIEWED);
										instance.setResultsAvailable(true);
										List<ExerciseInstance> userRunExercises = hpc.getCompletedExerciseInstancesWithResultsForUser(dbUser.getIdUser(), instance.getExercise().getUuid());

										boolean alreadyRun = false;
										boolean alreadySolved = false;
										List<ExerciseInstanceResult> solvedResults = new LinkedList<ExerciseInstanceResult>();
										instance.setSolved(isSolved(instance.getResults()));

										for(ExerciseInstance runEx : userRunExercises) {
											if(!runEx.getIdExerciseInstance().equals(instance.getIdExerciseInstance()) && (runEx.getStatus().equals(ExerciseInstanceStatus.REVIEWED) || runEx.getStatus().equals(ExerciseInstanceStatus.REVIEWED_MODIFIED) || runEx.getStatus().equals(ExerciseInstanceStatus.AUTOREVIEWED)) ) {
												alreadyRun = true;
												if(runEx.isSolved()) {
													alreadySolved = true;
												}
												for(ExerciseInstanceResult result : runEx.getResults()) {
													if(null!=result.getStatus() && (result.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || result.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
														solvedResults.add(result);
													}
												}
											}

										}
										dbUser.setExercisesRun(dbUser.getExercisesRun() + 1);

										if(alreadyRun && !alreadySolved){
											List<ExerciseInstanceResult> uniqueSolved = new LinkedList<ExerciseInstanceResult>();
											for(ExerciseInstanceResult instanceRes: instance.getResults()) {
												Boolean previouslySolved = false;
												for(ExerciseInstanceResult res : solvedResults) {
													if(res.getFlagTitle().equals(instanceRes.getFlagTitle())) {
														if(!isAlreadyPresent(uniqueSolved,res)){
															uniqueSolved.add(res);
														}
														previouslySolved = true;
														break;
													}
												}
												if(!previouslySolved && (instanceRes.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || instanceRes.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
													uniqueSolved.add(instanceRes);
												}
												// no points if the flag was previously solved.
												else if(previouslySolved && instance.getChallengeId()==null && (instanceRes.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) || instanceRes.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED))) {
													instanceRes.setScore(0);
													instanceRes.setPreviouslySolved(true);
												}
											}
											// aggregate previous and current NOT_VULNERABLE/EXPLOITED results to determine exercise solved
											if(instance.getResults().size()==uniqueSolved.size()) {
												instance.setSolved(true);
											}
										}
										Integer totalScore = 0;
										for(ExerciseInstanceResult r : instance.getResults()){
											totalScore += r.getScore();
										}
										instance.getScore().setResult(totalScore);
										dbUser.setScore(dbUser.getScore() + instance.getScore().getResult());

										checkAndAttributeTrophy(instance,dbUser);

										hpc.updateExerciseInstance(instance);
										hpc.updateUserInfo(dbUser);
									
									}
								}
							}
						});
						stopExerciseThread.start();
						instance.setStatus(ExerciseInstanceStatus.STOPPING);						
						hpc.updateExerciseInstance(instance);
						
					}
				}
			}

			if(!found && SFConfig.getEnvironment().equals(Constants.PROD_ENVIRONMENT)){ //orphans

				Date createdTime = awsHelper.getRunningECSTaskStartTime(taskArn);
				long time = System.currentTimeMillis();
				if(time-createdTime.getTime() >= 1200000) {
					logger.warn("Exercise "+taskArn+" is orphan and needs to be shutdown");
					awsHelper.terminateTask(taskArn);		
				}
				else {
					logger.debug("Fullfilled Reservation "+(time-createdTime.getTime())+" ms old");
				}
			}
		}
		logger.debug("End of Exercises Auto Shutdown Task");
	}

	private Boolean isAlreadyPresent(List<ExerciseInstanceResult> uniqueSolved, ExerciseInstanceResult r) {
		for(ExerciseInstanceResult ur : uniqueSolved) {
			if(ur.getFlagTitle().equals(r.getFlagTitle())) {
				return true;
			}
		}
		return false;
	}

	private Boolean isSolved(List<ExerciseInstanceResult> results) {
		if(null == results || results.isEmpty())
			return false;
		for(ExerciseInstanceResult result : results) {
			if(!result.getStatus().equals(ExerciseInstanceResultStatus.NOT_VULNERABLE) && !result.getStatus().equals(ExerciseInstanceResultStatus.EXPLOITED)) {
				return false;
			}
		}
		return true;
	}
	private void checkAndAttributeTrophy(ExerciseInstance instance, User user) {
		if(instance.getScore().getResult() >= instance.getScore().getTotal()) {
			List<UserAchievement> userAchievedTrophy = hpc.getAllAchievementsForUser(instance.getUser().getIdUser());
			boolean alreadyAchieved = false;
			for(UserAchievement trophy : userAchievedTrophy){
				if(trophy.getAchievement().getName().equals(instance.getAvailableExercise().getTrophyName())){
					alreadyAchieved = true;
					break;
				}
			}
			if(!alreadyAchieved){
				UserAchievement t = new UserAchievement();
				t.setDate(new Date());
				t.setUser(instance.getUser());
				Trophy achievement = new Trophy();
				achievement.setName(instance.getAvailableExercise().getTrophyName());
				achievement.setType(AchievementType.TROPHY);
				achievement.setTechnology(instance.getAvailableExercise().getTechnology());
				t.setAchievement(achievement);
				hpc.managementAddAchievedTrophy(t);
				notificationsHelper.addTrophyNotification(user,achievement);
				instance.setTrophyAwarded(true);
			}
		}
	}

}