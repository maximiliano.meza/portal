/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.tasks;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.model.FargateSGForDeletion;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class FargateSGCleanupTask implements Runnable {

	private static Logger logger = LoggerFactory.getLogger(FargateSGCleanupTask.class);
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private AWSHelper awsHelper = new AWSHelper();

	@Override
	public void run() {

		if(!SFConfig.isFargateModuleAvailable()) {
			logger.debug("Skipping Fargate SG Deletion Task as member is not master");
			return;
		}
		logger.debug("Running Fargate SG Deletion Task");
		try {
			List<FargateSGForDeletion> list = hpc.getFargateSGForDeletion();
			logger.debug("FargateSGForDeletion contains "+list.size()+" items");

			for(FargateSGForDeletion f : list) {
				if(awsHelper.deleteSecurityGroup(f.getName(), f.getRegion())) {
					logger.debug("Removed sg "+f.getName()+" from AWS");
					if(hpc.deleteFargateSGForDeletion(f.getName())) {
						logger.debug("Removed sg "+f.getName()+" from HB");
					}
					else {
						logger.error("Could not remove sg "+f.getName()+" from HB");
					}
				}
				else {
					logger.warn("Could not remove sg "+f.getName()+" from AWS");
				}
			}
			logger.debug("End of Fargate SG Deletion Task");
		}catch(Exception e) {
			logger.error("Exception during Fargate SG Deletion Task due to:\n"+e.getMessage());
		}
	}
}