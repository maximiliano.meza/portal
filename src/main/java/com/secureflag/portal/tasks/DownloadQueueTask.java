package com.secureflag.portal.tasks;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.regions.Regions;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.AvailableExerciseStatus;
import com.secureflag.portal.model.DownloadQueueItem;
import com.secureflag.portal.model.DownloadQueueItemStatus;
import com.secureflag.portal.model.DownloadQueueItemType;
import com.secureflag.portal.model.ECSTaskDefinition;
import com.secureflag.portal.model.ECSTaskDefinitionForExerciseInRegion;
import com.secureflag.portal.model.LearningPath;
import com.secureflag.portal.model.LearningPathStatus;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;

public class DownloadQueueTask implements Runnable {

	private static Logger logger = LoggerFactory.getLogger(DownloadQueueTask.class);
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private AWSHelper aws = new AWSHelper();
	private DockerClient docker;

	@Override
	public void run() {

		while(true) {
			logger.info("Running DownloadQueue Task");
			try {
				logger.info("Looking at pending items in DownloadQueue...");

				if(!SFConfig.isTaskCoordinationModuleAvailable()) {
					logger.debug("Skipping DownloadQueue Task as member is not master");
					return;
				}

				List<DownloadQueueItem> items = hpc.getPendingDownloadQueueItems();

				if(items.isEmpty()) {
					Thread.sleep(30000);
					continue;
				}


				DownloadQueueItem queueItem = items.get(0);

				logger.info("Extracted item from DownloadQueue...");

				queueItem.setStatus(DownloadQueueItemStatus.STARTED);
				queueItem.setLastUpdate(new Date());
				hpc.updateDownalodQueueItemStatus(queueItem);

				String imageUrl = queueItem.getExercise().getImage().getImageUrl();
				String [] arr = imageUrl.split("\\.");
				String repo = arr[5].replace("com/","");
				if(repo.indexOf(":")>=0)
					repo = repo.substring(0,repo.indexOf(":"));
				
				String repoUri = aws.createECRRepository(SFConfig.getRegion(), repo);
				queueItem.setStatus(DownloadQueueItemStatus.REPO_CREATED);
				queueItem.setLastUpdate(new Date());
				hpc.updateDownalodQueueItemStatus(queueItem);

				Date currentImageDate = aws.getLastUpdateDate(SFConfig.getRegion(),repo);
				if(null==currentImageDate || queueItem.getExercise().getImage().getUpdateDate().after(currentImageDate)) {

					docker = DefaultDockerClient.fromEnv().build();
					queueItem.setStatus(DownloadQueueItemStatus.PULL_STARTED);
					queueItem.setLastUpdate(new Date());
					hpc.updateDownalodQueueItemStatus(queueItem);

					if(imageUrl.indexOf(":")>=0) {
						docker.pull(imageUrl);
					}
					else {
						docker.pull(imageUrl+":latest");
					}
					queueItem.setStatus(DownloadQueueItemStatus.PULL_COMPLETE);
					queueItem.setLastUpdate(new Date());
					hpc.updateDownalodQueueItemStatus(queueItem);

					docker.tag(imageUrl, repoUri);
					queueItem.setStatus(DownloadQueueItemStatus.IMAGE_TAGGED);
					queueItem.setLastUpdate(new Date());
					hpc.updateDownalodQueueItemStatus(queueItem);

					docker.push(repoUri);
					queueItem.setStatus(DownloadQueueItemStatus.PUSH_COMPLETE);
					queueItem.setLastUpdate(new Date());
					hpc.updateDownalodQueueItemStatus(queueItem);

					docker.removeImage(imageUrl, true, false);
					docker.removeImage(repoUri, true, false);
					queueItem.setStatus(DownloadQueueItemStatus.IMAGE_REMOVED);
					queueItem.setLastUpdate(new Date());
					hpc.updateDownalodQueueItemStatus(queueItem);

				}
				if(queueItem.getType().equals(DownloadQueueItemType.INSTALLATION)) {
					for(String reg : queueItem.getRegions()) {
						Regions awsRegion = null;
						try{
							awsRegion = Regions.valueOf(reg);

							ECSTaskDefinition ecsTask = new ECSTaskDefinition();

							ecsTask.setRegion(awsRegion);
							ecsTask.setContainerName(queueItem.getExercise().getImage().getContainerName());
							ecsTask.setTaskDefinitionName(queueItem.getExercise().getImage().getTaskDefinitionName());
							ecsTask.setHardMemoryLimit(queueItem.getExercise().getImage().getHardMemoryLimit());
							ecsTask.setSoftMemoryLimit(queueItem.getExercise().getImage().getSoftMemory());
							ecsTask.setRepositoryImageUrl(repoUri);
							ecsTask.setUpdateDate(new Date());

							User user = new User();
							user.setIdUser(-1);

							AWSHelper awsHelper = new AWSHelper();
							String arn = awsHelper.registerECSTaskDefinition(ecsTask, user);
							if(null!=arn) {
								String fargateArn = awsHelper.registerFargateTaskDefinition(ecsTask, user);
								if(null==fargateArn) {
									ecsTask.setFargateDefinitionArn(null);
									ecsTask.setFargateDefinitionName(null);
									logger.error("Failed to create Fargate TaskDefinition");
								}
								else {
									ecsTask.setFargateDefinitionArn(fargateArn);
									ecsTask.setFargateDefinitionName(queueItem.getExercise().getImage().getTaskDefinitionName()+"-fargate");
								}

								ecsTask.setTaskDefinitionArn(arn);
								String nameWithRevision = arn.split("/")[(arn.split("/").length)-1];
								if(null!=nameWithRevision && !nameWithRevision.equals(""))
									ecsTask.setTaskDefinitionName(nameWithRevision);

								ECSTaskDefinitionForExerciseInRegion ecsTaskForExercise = new ECSTaskDefinitionForExerciseInRegion();
								ecsTaskForExercise.setExercise(queueItem.getExercise());
								ecsTaskForExercise.setRegion(awsRegion);
								ecsTaskForExercise.setTaskDefinition(ecsTask);
								ecsTaskForExercise.setActive(true);
								Integer id = hpc.addECSTaskDefinitionForExerciseInRegion(ecsTaskForExercise);
								if(null==id) {
									logger.error("New ECSTaskDefinitionForExerciseInRegion could not be saved");
								}
							}
						} catch(Exception e){
							logger.warn(e.getMessage());
							continue;
						}
					}

					queueItem.setStatus(DownloadQueueItemStatus.TASK_DEFINITION_COMPLETE);
					queueItem.setLastUpdate(new Date());
					hpc.updateDownalodQueueItemStatus(queueItem);

					for(Integer idOrganization : queueItem.getOrganizations()) {
						try {
							Organization org = hpc.getOrganizationById(idOrganization);
							hpc.addAvailableExerciseForOrganization(org,queueItem.getExercise());
						}catch(Exception e) {
							logger.warn(e.getMessage());
							continue;
						}
					}

					queueItem.setStatus(DownloadQueueItemStatus.ORGANIZATIONS_COMPLETE);
					queueItem.setLastUpdate(new Date());
					hpc.updateDownalodQueueItemStatus(queueItem);
				}
				else if(queueItem.getType().equals(DownloadQueueItemType.UPDATE)) {

					List<Organization> organizations = hpc.getAllOrganizations();
					AvailableExercise newExercise = hpc.getAvailableExercise(queueItem.getExercise().getId());
					for(Organization org : organizations) {
						if(hpc.isExerciseEnabledForOrganization(org.getId(), newExercise.getUuid())) {
							hpc.updateExerciseEnabledForOrganization(org.getId(), newExercise.getUuid(), newExercise);
						}
					}						
					queueItem.setStatus(DownloadQueueItemStatus.ORGANIZATIONS_COMPLETE);
					queueItem.setLastUpdate(new Date());
					hpc.updateDownalodQueueItemStatus(queueItem);
				}

				AvailableExercise queueExercise = queueItem.getExercise();
				queueExercise.setStatus(AvailableExerciseStatus.AVAILABLE);
				hpc.updateAvailableExercise(queueExercise);
				queueItem.setStatus(DownloadQueueItemStatus.COMPLETE);
				queueItem.setLastUpdate(new Date());
				hpc.updateDownalodQueueItemStatus(queueItem);
				logger.info("Completed DownloadQueue Task");

				//check if part of learning path installation
				if(null!=queueItem.getPathUuid() && !queueItem.getPathUuid().equals("")) {
					Boolean ready = true;
					List<DownloadQueueItem> queueItems = hpc.getDownalodQueueItems();
					for(DownloadQueueItem item : queueItems) {
						if(item.getPathUuid().equals(queueItem.getPathUuid())) {
							logger.debug(item.getExercise().getTitle()+" is still in queue");
							ready = false;
							break;
						}
					}
					if(ready) {
						logger.debug("There are no items left in the DownloadQueue for Learning Path "+queueItem.getPathUuid()+" updating status to Available");
						LearningPath path = hpc.getLearningPath(queueItem.getPathUuid());
						if(null!=path) {
							path.setStatus(LearningPathStatus.AVAILABLE);
							hpc.updateLearningPath(path);
						}
					}
					else {
						logger.debug("There are still items left in the DownloadQueue for Learning Path "+queueItem.getPathUuid()+"...");
					}
				}

			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				logger.error(errors.toString());
				continue;
			}
		}
	}
}