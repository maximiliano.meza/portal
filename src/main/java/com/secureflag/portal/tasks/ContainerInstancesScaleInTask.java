/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.tasks;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.ecs.model.ContainerInstance;
import com.secureflag.portal.cloud.AWSHelper;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class ContainerInstancesScaleInTask implements Runnable {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private AWSHelper awsHelper = new AWSHelper();
	private static Logger logger = LoggerFactory.getLogger(ContainerInstancesScaleInTask.class);

	@Override
	public void run() {

		if(!SFConfig.isScaleInModuleAvailable()) {
			logger.debug("Skipping Container Scale In Task as member is not master");
			return;
		}
		List<Gateway> activeGws = hpc.getAllActiveGateways();

		logger.info("Running Container Scale In Task, Active Gateways/Regions: "+activeGws.size());		

		for(Gateway gw : activeGws){
			if(!gw.getEnableScaleIn())
				continue;

			logger.info("Looking at Container Instances in Region: "+gw.getRegion().toString());
			ContainerInstance toRemove = awsHelper.getClusterContainerInstanceToRemove(gw.getRegion());

			if(null!=toRemove) {
				logger.info("Removing Container Instance "+toRemove.getContainerInstanceArn()+" in Region: "+gw.getRegion().toString());
				awsHelper.detachAndRemoveInstance(gw.getRegion(),toRemove);
			}
			else {
				logger.debug("No container instances selected for Scale In in region "+gw.getRegion());
			}
		}
		logger.debug("End of Container Scale In Task");
	}
}