/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.messages.notifications;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.secureflag.portal.config.Constants;
import com.secureflag.portal.model.ChallengePlacement;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.LearningPathCertification;
import com.secureflag.portal.model.Notification;
import com.secureflag.portal.model.Trophy;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class NotificationsHelper {
	
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	public boolean addCompletedReviewNotification(User user, ExerciseInstance e){
		Notification n = new Notification();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		n.setDateStart(c.getTime());
		c.add(Calendar.DATE, 60); 
		n.setDateEnd(c.getTime());
		n.setIdUser(user.getIdUser());
		String link = Constants.NOTIFICATION_LINK_USER_EXERCISE_INSTANCE;
		link = link.replace("{EXERCISE}", String.valueOf(e.getIdExerciseInstance()));
		n.setLink(link);
		String text = Constants.NOTIFICATION_TEXT_COMPLETED_REVIEW;
		text = text.replace("{EXERCISE}", e.getTitle());
		n.setText(text);
		n.setUserRead(false);
		return hpc.addNotification(n);
	}
	public boolean addNewUserAdded(User user){
		List<User> admins = hpc.getManagementUsersForOrganization(user.getDefaultOrganization());
		List<Notification> notifications = new LinkedList<Notification>();
		for(User u : admins){
			if(u.getIdUser().equals(user.getIdUser()))
				continue;
			Notification n = new Notification();
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			n.setDateStart(c.getTime());
			c.add(Calendar.DATE, 60); 
			n.setDateEnd(c.getTime());
			n.setIdUser(u.getIdUser());
			String link = Constants.NOTIFICATION_LINK_NEW_USER_ADDED.replace("{USERNAME}",user.getUsername());
			n.setLink(link);
			String text = Constants.NOTIFICATION_TEXT_NEW_USER_ADDED.replace("{USERNAME}", user.getUsername());
			n.setText(text);
			n.setUserRead(false);
			notifications.add(n);
		}
		return hpc.addNotificationList(notifications);
	}
	public boolean newFeedbackReceived(User user, ExerciseInstance instance){
		List<User> admins = hpc.getManagementUsersForOrganization(user.getDefaultOrganization());
		List<Notification> notifications = new LinkedList<Notification>();
		for(User u : admins){
			if(u.getIdUser().equals(user.getIdUser()))
				continue;
			Notification n = new Notification();
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			n.setDateStart(c.getTime());
			c.add(Calendar.DATE, 60); 
			n.setDateEnd(c.getTime());
			n.setIdUser(u.getIdUser());
			String link = Constants.NOTIFICATION_LINK_MGMT_EXERCISE_INSTANCE.replace("{EXERCISE}",String.valueOf(instance.getIdExerciseInstance()));
			n.setLink(link);
			String text = Constants.NOTIFICATION_TEXT_FEEDBACK_ADDED.replace("{USERNAME}", user.getUsername()).replace("{EXERCISE}", String.valueOf(instance.getIdExerciseInstance()));
			n.setText(text);
			n.setUserRead(false);
			notifications.add(n);
		}
		return hpc.addNotificationList(notifications);
	}
	public boolean newScoringComment(User user, ExerciseInstance instance){
		List<User> admins = hpc.getManagementUsersForOrganization(user.getDefaultOrganization());
		List<Notification> notifications = new LinkedList<Notification>();
		for(User u : admins){
			if(u.getIdUser().equals(user.getIdUser()))
				continue;
			Notification n = new Notification();
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			n.setDateStart(c.getTime());
			c.add(Calendar.DATE, 60); 
			n.setDateEnd(c.getTime());
			n.setIdUser(u.getIdUser());
			String link = Constants.NOTIFICATION_LINK_MGMT_EXERCISE_INSTANCE.replace("{EXERCISE}",String.valueOf(instance.getIdExerciseInstance()));
			n.setLink(link);
			String text = Constants.NOTIFICATION_TEXT_SCORING_COMMENT_ADDED.replace("{USERNAME}", user.getUsername()).replace("{EXERCISE}", String.valueOf(instance.getIdExerciseInstance()));
			n.setText(text);
			n.setUserRead(false);
			notifications.add(n);
		}
		return hpc.addNotificationList(notifications);
	}
	
	
	
	public boolean addWelcomeToSecureFlagNotification(User user){
		Notification n = new Notification();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		n.setDateStart(c.getTime());
		c.add(Calendar.DATE, 60); 
		n.setDateEnd(c.getTime());
		n.setIdUser(user.getIdUser());
		n.setLink(Constants.NOTIFICATION_LINK_WELCOME_TO_SF);
		n.setText(Constants.NOTIFICATION_TEXT_WELCOME_TO_SF);
		n.setUserRead(false);
		return hpc.addNotification(n);
	}
	public boolean addNewNotification(User user, String text) {
		Notification n = new Notification();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		n.setDateStart(c.getTime());
		c.add(Calendar.DATE, 60); 
		n.setDateEnd(c.getTime());
		n.setIdUser(user.getIdUser());
		n.setLink(null);
		n.setText(text);
		n.setUserRead(false);
		return hpc.addNotification(n);
	}
	
	public boolean addScoringCommentNotification(User user, ExerciseInstance instance) {
		Notification n = new Notification();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		n.setDateStart(c.getTime());
		c.add(Calendar.DATE, 60); 
		n.setDateEnd(c.getTime());
		n.setIdUser(user.getIdUser());
		String link = Constants.NOTIFICATION_LINK_USER_EXERCISE_INSTANCE.replace("{EXERCISE}",String.valueOf(instance.getIdExerciseInstance()));
		n.setLink(link);
		String text = Constants.NOTIFICATION_TEXT_SCORING_COMMENT.replace("{EXERCISE}", String.valueOf(instance.getTitle()));
		n.setText(text);
		n.setUserRead(false);
		return hpc.addNotification(n);
	}
	
	public boolean addUpdateReviewNotification(User user, ExerciseInstance instance) {
		Notification n = new Notification();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		n.setDateStart(c.getTime());
		c.add(Calendar.DATE, 60); 
		n.setDateEnd(c.getTime());
		n.setIdUser(user.getIdUser());
		String link = Constants.NOTIFICATION_LINK_USER_EXERCISE_INSTANCE;
		link = link.replace("{EXERCISE}", String.valueOf(instance.getIdExerciseInstance()));
		n.setLink(link);
		String text = Constants.NOTIFICATION_TEXT_UPDATED_REVIEW;
		text = text.replace("{EXERCISE}", instance.getTitle());
		n.setText(text);
		n.setUserRead(false);
		return hpc.addNotification(n);
	}
	public boolean addTrophyNotification(User user, Trophy trophy) {
		Notification n = new Notification();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		n.setDateStart(c.getTime());
		c.add(Calendar.DATE, 60); 
		n.setDateEnd(c.getTime());
		n.setIdUser(user.getIdUser());
		String link = Constants.NOTIFICATION_LINK_ACHIEVEMENTS;
		n.setLink(link);
		String text = Constants.NOTIFICATION_TEXT_NEW_TROPHY;
		text = text.replace("{TROPHY}", trophy.getName());
		text = text.replace("{TECHNOLOGY}", trophy.getTechnology());
		n.setText(text);
		n.setUserRead(false);
		return hpc.addNotification(n);
	}
	public boolean addCertificationNotification(User user, LearningPathCertification certification) {
		Notification n = new Notification();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		n.setDateStart(c.getTime());
		c.add(Calendar.DATE, 60); 
		n.setDateEnd(c.getTime());
		n.setIdUser(user.getIdUser());
		String link = Constants.NOTIFICATION_LINK_ACHIEVEMENTS;
		n.setLink(link);
		String text = Constants.NOTIFICATION_TEXT_NEW_CERTIFICATION;
		text = text.replace("{TECHNOLOGY}", certification.getTechnology());
		text = text.replace("{CERTIFICATION}", certification.getName());
		text = text.replace("{DIFFICULTY}", certification.getDifficulty());
		n.setText(text);
		n.setUserRead(false);
		return hpc.addNotification(n);
	}
	public boolean addPlacementNotification(User user, ChallengePlacement placement) {
		Notification n = new Notification();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		n.setDateStart(c.getTime());
		c.add(Calendar.DATE, 60); 
		n.setDateEnd(c.getTime());
		n.setIdUser(user.getIdUser());
		String link = Constants.NOTIFICATION_LINK_ACHIEVEMENTS;
		n.setLink(link);
		String text = Constants.NOTIFICATION_TEXT_NEW_PLACEMENT;
		text = text.replace("{CHALLENGE}", placement.getName());
		text = text.replace("{PLACEMENT}", String.valueOf(placement.getPlacement()));
		n.setText(text);
		n.setUserRead(false);
		return hpc.addNotification(n);
	}
}