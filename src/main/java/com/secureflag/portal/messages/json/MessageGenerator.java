/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.messages.json;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.securitytoken.model.Credentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.model.AWSSupportedRegion;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.AvailableExerciseRegion;
import com.secureflag.portal.model.AvailableExerciseSolution;
import com.secureflag.portal.model.AvailableExercisesForOrganization;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.Country;
import com.secureflag.portal.model.DownloadQueueItem;
import com.secureflag.portal.model.ECSTaskDefinitionForExerciseInRegion;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceFeedback;
import com.secureflag.portal.model.ExerciseInstanceReservation;
import com.secureflag.portal.model.ExerciseInstanceResultFile;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.FlagQuestionHint;
import com.secureflag.portal.model.Framework;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.InvitationCodeForOrganization;
import com.secureflag.portal.model.KBTechnologyItem;
import com.secureflag.portal.model.KBVulnerabilityItem;
import com.secureflag.portal.model.LearningPath;
import com.secureflag.portal.model.Notification;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.Team;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.dto.CompletedReview;
import com.secureflag.portal.model.dto.ExerciseInChallengeStatus;
import com.secureflag.portal.model.dto.ExerciseInstanceAppStatus;
import com.secureflag.portal.model.dto.ExerciseInstanceSelfCheckResult;
import com.secureflag.portal.model.dto.PendingReview;
import com.secureflag.portal.model.dto.PlatformFeatures;
import com.secureflag.portal.model.dto.StatsObject;
import com.secureflag.portal.model.dto.TeamLeaderboard;
import com.secureflag.portal.model.dto.UploadBucket;
import com.secureflag.portal.model.dto.UserAchievements;

public class MessageGenerator {

	private static Logger logger = LoggerFactory.getLogger(MessageGenerator.class);

	public static void sendErrorMessage(String error, HttpServletResponse response) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, Constants.JSON_VALUE_ERROR);
		msg.addProperty(Constants.JSON_ATTRIBUTE_ERROR_MSG, error);
		send(msg.toString(),response);
	}

	public static void sendSuccessMessage(HttpServletResponse response) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, Constants.JSON_VALUE_SUCCESS);
		send(msg.toString(),response);
	}

	public static void sendRedirectMessage(String destination, HttpServletResponse response) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, Constants.JSON_VALUE_REDIRECT);
		msg.addProperty(Constants.JSON_ATTRIBUTE_LOCATION, destination);
		send(msg.toString(),response);
	}

	private static void send(String msg, HttpServletResponse response){
		try {
			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.print(msg.toString());
			out.close();
		} catch (IOException e) {
			logger.error("HTTP Response. Exception: " + e.getMessage());
		}
	}

	public static void sendAllExercisesForOrgMessage(Set<AvailableExercise> exercises, List<AvailableExercisesForOrganization> exercisesForOrg,
			HttpServletResponse response) {
		Gson exercisesGson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String exercisesJson = exercisesGson.toJson(exercises);
		Gson orgsGson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.briefDetails).create();
		String orgsJson = orgsGson.toJson(exercisesForOrg);
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("exercises", parser.parse(exercisesJson));
		jsonObject.add("orgs", parser.parse(orgsJson));
		send(jsonObject.toString(),response);
	}

	public static void sendTokenMessage(Integer eiId, String fqdn, String guacUser, String lastValidToken, String node, String connectionId, Integer countdown, String ec2Instance, String ec2Ip, Integer ec2Preload, HttpServletResponse response) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("exInstanceId", eiId);
		jsonObject.addProperty("fqdn", fqdn);
		jsonObject.addProperty("token", lastValidToken);
		jsonObject.addProperty("node", node);
		jsonObject.addProperty("user", guacUser);
		jsonObject.addProperty("connectionId", connectionId);
		jsonObject.addProperty("countdown", countdown);
		jsonObject.addProperty("ec2Instance", ec2Instance);
		jsonObject.addProperty("ec2Ip", ec2Ip);
		jsonObject.addProperty("ec2Preload", ec2Preload);
		send(jsonObject.toString(),response);

	}
	public static void sendReservationMessage(ExerciseInstanceReservation reservation, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedNonUserAndLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(reservation);
		send(json,response);
	}


	public static void sendUserInfoMessage(User user, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.userDetails).create();
		String json = gson.toJson(user);
		send(json,response);
	}

	public static void sendUserAchievementsMessage(UserAchievements achievements, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(achievements);
		send(json,response);
	}


	public static void sendAllChallengesMessage(List<Challenge> challenges, HttpServletResponse response) {
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(challenges);
		send(json,response);
	}

	public static void sendAllHubExercisesMessage(List<AvailableExercise> exercises, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(exercises);
		send(json,response);
	}

	public static void sendAllExercisesMessage(List<AvailableExercise> exercises, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedNonUserAndLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(exercises);
		send(json,response);
	}

	public static void sendCSRFTokenMessage(String token, HttpServletResponse response) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("ctoken", token);
		send(jsonObject.toString(),response);
	}

	public static void sendUserHistoryMessage(List<ExerciseInstance> history, HttpServletResponse response) {
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyHeavyExerciseData).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(history);
		send(json,response);
	}
	public static String getExerciseLightInfoMessage(AvailableExercise exercise) {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.addSerializationExclusionStrategy(ExclusionStrategies.excludedHeavyExerciseData).excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(exercise);
	}
	public static void sendExerciseLightInfoMessage(AvailableExercise exercise, HttpServletResponse response) {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.addSerializationExclusionStrategy(ExclusionStrategies.excludedHeavyExerciseData).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(exercise);
		send(json,response);
	}
	public static void sendExerciseInfoMessage(AvailableExercise exercise, HttpServletResponse response) {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.addSerializationExclusionStrategy(ExclusionStrategies.excludedSolutions).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(exercise);
		send(json,response);
	}
	public static void sendExerciseInfoMessageWithHints(AvailableExercise exercise, HttpServletResponse response) {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(exercise);
		send(json,response);
	}

	public static void sendAllCountriesMessage(List<Country> countries, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(countries);
		send(json,response);
	}

	public static void sendUserHistoryDetailMessage(ExerciseInstance instance, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedSolutions).addSerializationExclusionStrategy(ExclusionStrategies.historyDetails).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(instance);
		send(json,response);
	}

	public static void sendFileMessage(ExerciseInstance instance, HttpServletResponse response) {
		ExerciseInstanceResultFile resultFile = instance.getResultFile();
		if(null==resultFile || null==resultFile.getFile()){
			MessageGenerator.sendErrorMessage("CouldNotRetrieve", response);
			return;
		}
		byte[] file = resultFile.getFile();
		response.setContentType("application/octet-stream");
		response.setContentLength((int) file.length);
		response.setHeader( "Content-Disposition", String.format("attachment; filename=\"%s\"", resultFile.getFilename()+".zip"));
		try {
			response.getOutputStream().write(file);
		} catch (IOException e) {
			logger.error("Failed to send zip file: "+e.getMessage());
		}
	}

	public static void sendExerciseRegionsMessage(List<AvailableExerciseRegion> regions,HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(regions);
		send(json,response);

	}

	public static void sendHintMessage(FlagQuestionHint hint, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(hint);
		send(json,response);

	}


	public static void sendUserReservationsMessage(List<ExerciseInstanceReservation> reservations, HttpServletResponse response) {

		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(reservations);
		send(json,response);

	}
	public static void sendUserRunningExercisesMessage(List<ExerciseInstance> exercises, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedHeavyExerciseData).addSerializationExclusionStrategy(ExclusionStrategies.runningExercises).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(exercises);
		send(json,response);
	}

	public static void sendExerciseSelfCheckStatus(ExerciseInstanceSelfCheckResult res, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(res);
		send(json,response);
	}

	public static void sendUserTeamLeaderboard(TeamLeaderboard leaderboard, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.leadearboardUsers).create();
		String json = gson.toJson(leaderboard);
		send(json,response);
	}

	public static void sendUsersListAsMembers(List<User> users, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.memberUsers).create();
		String json = gson.toJson(users);
		send(json,response);
	}
	public static void sendUsersListMessage(List<User> users, HttpServletResponse response) {
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).addSerializationExclusionStrategy(ExclusionStrategies.usersList).create();
		String json = gson.toJson(users);
		send(json,response);
	}
	public static void sendPendingExerciseInstances(List<ExerciseInstance> pending, HttpServletResponse response) {

		List<PendingReview> reviews = new LinkedList<PendingReview>();
		for(ExerciseInstance e : pending){
			PendingReview r = new PendingReview();
			r.setEndTime(e.getEndTime());
			r.setExerciseName(e.getTitle());
			r.setChallengeId(e.getChallengeId());
			r.setExerciseTopic(e.getAvailableExercise().getSubtitle());
			r.setId(e.getIdExerciseInstance());
			r.setStartTime(e.getStartTime());
			r.setOrganization(e.getOrganization());
			r.setTechnology(e.getTechnology());
			r.setDuration(e.getDuration());
			r.setUser(e.getUser().getUsername());
			r.setNrSelfRefresh(e.getCountSelfCheckByUser());
			reviews.add(r);
		}
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(reviews);
		send(json,response);
	}

	public static void sendExerciseDetails(ExerciseInstance pending, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(pending);
		send(json,response);
	}

	public static void sendStatsMessage(StatsObject stats, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(stats);
		send(json,response);
	}

	public static void sendTeamListMessage(List<Team> teams, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(teams);
		send(json,response);

	}


	public static void sendReviewedExercises(List<ExerciseInstance> exercises, HttpServletResponse response) {
		List<CompletedReview> reviews = new LinkedList<CompletedReview>();
		for(ExerciseInstance e : exercises){
			CompletedReview r = new CompletedReview();
			r.setEndTime(e.getEndTime());
			r.setExerciseName(e.getTitle());
			r.setChallengeId(e.getChallengeId());
			r.setStatus(e.getStatus());
			if(null!=e.getAvailableExercise())
				r.setExerciseTopic(e.getAvailableExercise().getSubtitle());
			r.setId(e.getIdExerciseInstance());
			r.setDuration(e.getDuration());
			r.setOrganization(e.getOrganization());
			r.setStartTime(e.getStartTime());
			r.setTechnology(e.getTechnology());
			r.setHasCrashed(e.hasCrashed());
			r.setIssuesReportedAddressed(e.isIssuesAddressed());
			r.setReportedScoringIssues(e.isIssuesReported());
			if(null!=e.getUser())
				r.setUser(e.getUser().getUsername());
			r.setFeedback(e.getFeedback());
			r.setNrSelfRefresh(e.getCountSelfCheckByUser());
			r.setNrResRefresh(e.getCountResultsReviewedByUser());
			if(e.getStatus().equals(ExerciseInstanceStatus.REVIEWED) || e.getStatus().equals(ExerciseInstanceStatus.AUTOREVIEWED) || e.getStatus().equals(ExerciseInstanceStatus.REVIEWED_MODIFIED) || e.getStatus().equals(ExerciseInstanceStatus.CRASHED)){
				if(null!=e.getScore())
					r.setScore(e.getScore().getResult());
				r.setTrophyAwarded(e.getTrophyAwarded());
				r.setNewIssueIntroduced(e.getNewIssuesIntroduced());
				r.setDate(e.getReviewedDate());
			}
			else{
				r.setScore(0);
				r.setTrophyAwarded(false);
				r.setNewIssueIntroduced(false);
				r.setDate(null);
			}
			reviews.add(r);
		}
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(reviews);
		send(json,response);	
	}
	public static void sendUserFeedbackMessage(ExerciseInstanceFeedback feedback, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(feedback);
		send(json,response);
	}
	public static void sendOrganizationsMessage(List<Organization> organizations, HttpServletResponse response) {
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).create();
		String json = gson.toJson(organizations);
		send(json,response);
	}
	public static void sendGatewaysMessage(List<Gateway> gateways, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).create();
		String json = gson.toJson(gateways);
		send(json,response);
	}
	public static void sendNotificationsMessage(List<Notification> notifications, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(notifications);
		send(json,response);
	}
	public static void sendUsersListAddTeam(List<User> users, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.leadearboardUsers).create();
		String json = gson.toJson(users);
		send(json,response);
	}
	public static void sendNumberUpdatedMessage(Integer nrAdded, HttpServletResponse response) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, nrAdded);
		send(msg.toString(),response);
	}
	public static void sendTeamDetailsMessage(Team team, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.teamManagers).create();
		String json = gson.toJson(team);
		send(json,response);
	}
	public static void sendAvailable(boolean b, HttpServletResponse response){
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, b);
		send(msg.toString(), response);
	}
	public static void sendCodeValid(boolean b, HttpServletResponse response) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, b);
		send(msg.toString(), response);
	}
	public static void sendIsExerciseInChallengeMessage(ExerciseInChallengeStatus isInChallenge, HttpServletResponse response) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(isInChallenge);
		send(json,response);

	}
	public static void sendChallengeResultsMessage(Challenge challenge, HttpServletResponse response) {

		for(ExerciseInstance e : challenge.getRunExercises()) {
			AvailableExercise tmp = new AvailableExercise();
			tmp.setUuid(e.getAvailableExercise().getUuid());
			e.setAvailableExercise(tmp);;
		}
		challenge.setExerciseData(null);
		Gson gson = new GsonBuilder().setDateFormat("EEE, d MMM yyyy HH:mm:ss zzz").addSerializationExclusionStrategy(ExclusionStrategies.challengeDetails).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(challenge);
		send(json,response);
	}

	public static void sendChallengeDetailsMessage(Challenge challenge, HttpServletResponse response) {

		for(ExerciseInstance e : challenge.getRunExercises()) {
			AvailableExercise tmp = new AvailableExercise();
			tmp.setUuid(e.getAvailableExercise().getUuid());
			e.setAvailableExercise(tmp);;
		}

		Gson gson = new GsonBuilder().setDateFormat("EEE, d MMM yyyy HH:mm:ss zzz").addSerializationExclusionStrategy(ExclusionStrategies.challengeDetails).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(challenge);
		send(json,response);
	}
	public static void sendAllUserChallengesMessage(List<Challenge> challenges,
			HttpServletResponse response) {
		if(null==challenges)
			challenges = new LinkedList<Challenge>();
		for(Challenge c : challenges) {
			if(c==null)
				continue;
			for(ExerciseInstance e : c.getRunExercises()) {
				AvailableExercise tmp = new AvailableExercise();
				tmp.setUuid(e.getAvailableExercise().getUuid());
				e.setAvailableExercise(tmp);;
			}
			c.setExerciseData(null);
		}

		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.challengeList).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(challenges);
		send(json,response);
	}
	public static void sendAWSRegionsMessage(List<AWSSupportedRegion> regions, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(regions);
		send(json,response);
	}
	public static void sendExerciseTaskDefinitionsMessage(List<ECSTaskDefinitionForExerciseInRegion> tasks, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(tasks);
		send(json,response);
	}

	public static void sendRunningExercisesMessage(List<ExerciseInstance> runningExercises, HttpServletResponse response) {
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new TimestampTypeAdapter()).addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(runningExercises);
		send(json,response);
	}

	public static void sendInvitationCodesMessage(List<InvitationCodeForOrganization> codes,
			HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(codes);
		send(json,response);
	}

	public static void sendImageList(HashMap<String, Map<String, JsonArray>> images, HttpServletResponse response) {
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();
		String json = gson.toJson(images);
		send(json,response);
	}

	public static void sendStacksMessage(List<KBTechnologyItem> s, HttpServletResponse response ) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(s);
		send(json,response);
	}

	public static void sendVulnerabilityKBMessage(KBVulnerabilityItem item, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(item);
		send(json,response);
	}

	public static void sendVulnerabilityKBListMessage(List<KBVulnerabilityItem> item, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(item);
		send(json,response);
	}

	public static void sendExerciseSolutionMessage(AvailableExerciseSolution solution, HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(solution);
		send(json,response);
	}

	public static void sendExerciseSuccess(Integer id, HttpServletResponse response) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, Constants.JSON_VALUE_SUCCESS);
		msg.addProperty(Constants.ACTION_PARAM_ID, id);
		send(msg.toString(),response);

	}


	public static void sendUUIDSuccess(String uuid, HttpServletResponse response) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, Constants.JSON_VALUE_SUCCESS);
		msg.addProperty(Constants.ACTION_PARAM_UUID, uuid);
		send(msg.toString(),response);

	}

	public static void sendStackKBMessage(KBTechnologyItem item, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedForUsers).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(item);
		send(json,response);
	}

	public static void sendLearningPathMessage(List<LearningPath> paths, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(paths);
		send(json,response);

	}

	public static void sendLearningPathMessageUsers(List<LearningPath> paths, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedNonUserAndLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(paths);
		send(json,response);

	}

	public static void sendLearningPathMessage(LearningPath path, HttpServletResponse response) {
		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(path);
		send(json,response);
	}

	public static void sendDownloadQueueMessage(List<DownloadQueueItem> downalodQueueItems,
			HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().addSerializationExclusionStrategy(ExclusionStrategies.excludedLazyObjects).create();
		String json = gson.toJson(downalodQueueItems);
		send(json,response);

	}

	public static void sendPlatformFeatures(PlatformFeatures features,HttpServletResponse response) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(features);
		send(json,response);

	}

	public static void sendAWSTempCredentials(Credentials credentials, HttpServletResponse response) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(credentials);
		send(json,response);

	}

	public static void sendUploadBucket(UploadBucket bucket, HttpServletResponse response) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(bucket);
		send(json,response);

	}

	public static void sendFrameworkListMessage(List<Framework> frameworks, HttpServletResponse response) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(frameworks);
		send(json,response);
		
	}

	public static void sendExerciseAppStatus(ExerciseInstanceAppStatus res, HttpServletResponse response) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(res);
		send(json,response);
	}

	public static void sendTechnologyKBItemMessage(KBTechnologyItem e, HttpServletResponse response) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(e);
		send(json,response);
	}

	
	public static void sendECRUrlMessage(String url, HttpServletResponse response) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_RESULT, Constants.JSON_VALUE_SUCCESS);
		msg.addProperty(Constants.ACTION_PARAM_URL, url);
		send(msg.toString(),response);
	}

		public static String getExerciseMessage(AvailableExercise exercise) {
		JsonObject msg = new JsonObject();
		msg.addProperty(Constants.JSON_ATTRIBUTE_TITLE, exercise.getTitle());
		msg.addProperty(Constants.JSON_ATTRIBUTE_TECHNOLOGY, exercise.getTechnology());
		msg.addProperty(Constants.JSON_ATTRIBUTE_FRAMEWORK, exercise.getFramework());
		msg.addProperty(Constants.JSON_ATTRIBUTE_UUID, exercise.getUuid());
		msg.add(Constants.JSON_ATTRIBUTE_FLAGS, getExerciseFlagsInfoMessage(exercise));

		return msg.toString();
	} 
	
	public static JsonElement getExerciseFlagsInfoMessage(AvailableExercise exercise) {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.addSerializationExclusionStrategy(ExclusionStrategies.lightweightEnvDataExclusions).excludeFieldsWithoutExposeAnnotation().create();
		JsonElement jsonElement = gson.toJsonTree(exercise.getQuestionsList());
		return jsonElement;
	}





}