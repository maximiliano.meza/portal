package com.secureflag.portal.messages.json;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.annotations.Expose;
import com.secureflag.portal.messages.json.annotations.BriefDetails;
import com.secureflag.portal.messages.json.annotations.ChallengeDetails;
import com.secureflag.portal.messages.json.annotations.ChallengeExcludedData;
import com.secureflag.portal.messages.json.annotations.ChallengeListExcludedData;
import com.secureflag.portal.messages.json.annotations.ExcludeFromHistoryList;
import com.secureflag.portal.messages.json.annotations.ExcludedForUsers;
import com.secureflag.portal.messages.json.annotations.HistoryDetails;
import com.secureflag.portal.messages.json.annotations.LazilyHint;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;
import com.secureflag.portal.messages.json.annotations.LazilySolution;
import com.secureflag.portal.messages.json.annotations.LeaderboardUser;
import com.secureflag.portal.messages.json.annotations.MemberUser;
import com.secureflag.portal.messages.json.annotations.NoChallengeList;
import com.secureflag.portal.messages.json.annotations.NoLightDetails;
import com.secureflag.portal.messages.json.annotations.RunningExercises;
import com.secureflag.portal.messages.json.annotations.TeamManager;
import com.secureflag.portal.messages.json.annotations.UserDetails;
import com.secureflag.portal.messages.json.annotations.UserStatusList;
import com.secureflag.portal.model.FlagQuestionHint;
import com.secureflag.portal.model.KBVulnerabilityItem;
import com.secureflag.portal.model.MarkdownText;
import com.secureflag.portal.model.User;

public class ExclusionStrategies {

	public static final ExclusionStrategy runningExercises = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(LazilySerialized.class) != null && f.getAnnotation(RunningExercises.class) == null;
		}
	};
	public static ExclusionStrategy lightweightEnvDataExclusions = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return clazz.equals(MarkdownText.class) || clazz.equals(KBVulnerabilityItem.class) || clazz.equals(FlagQuestionHint.class);
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getName().equals("statusMapping") || f.getName().equals("messageMappings");
		}
	};
	public static ExclusionStrategy excludedLazyObjects = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(LazilySerialized.class) != null;
		}
	};
	public static ExclusionStrategy excludedLazyHeavyExerciseData = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return  f.getAnnotation(ExcludeFromHistoryList.class) != null || f.getAnnotation(LazilySerialized.class) != null || f.getAnnotation(NoLightDetails.class) != null;
		}
	};
	public static ExclusionStrategy excludedHeavyExerciseData = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(NoLightDetails.class) != null;
		}
	};
	public static ExclusionStrategy excludedForUsers = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(ExcludedForUsers.class) != null;
		}
	};
	public static ExclusionStrategy excludedSolutions = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(LazilySolution.class) != null || f.getAnnotation(ExcludedForUsers.class) != null;
		}
	};
	public static ExclusionStrategy excludedHints = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(LazilyHint.class) != null;
		}
	};
	public static ExclusionStrategy briefDetails = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(BriefDetails.class) == null;
		}
	};
	public static ExclusionStrategy teamManagers = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(TeamManager.class) == null;
		}
	};
	public static ExclusionStrategy historyDetails = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(LazilySerialized.class) != null && f.getAnnotation(HistoryDetails.class) == null;
		}
	};
	public static ExclusionStrategy challengeList = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return clazz.equals(FlagQuestionHint.class);
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			if(f.getDeclaringClass().equals(User.class) && f.getAnnotation(LeaderboardUser.class) == null)
				return true;
	
			return ( (f.getAnnotation(LazilySerialized.class) != null && (f.getAnnotation(ChallengeDetails.class) == null || f.getAnnotation(NoChallengeList.class) != null)) ||  f.getAnnotation(ChallengeExcludedData.class) != null || f.getAnnotation(ChallengeListExcludedData.class)!=null);
		}
	};
	public static ExclusionStrategy challengeDetails = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return clazz.equals(FlagQuestionHint.class);
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			if(f.getDeclaringClass().equals(User.class) && f.getAnnotation(LeaderboardUser.class) == null && f.getAnnotation(MemberUser.class) == null)
				return true;
			return (f.getAnnotation(LazilySerialized.class) != null && f.getAnnotation(ChallengeDetails.class) == null) ||  f.getAnnotation(ChallengeExcludedData.class) != null ;
		}
	};
	public static ExclusionStrategy leadearboardUsers = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(LeaderboardUser.class) == null;
		}
	};
	public static ExclusionStrategy memberUsers = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(MemberUser.class) == null && f.getAnnotation(LeaderboardUser.class) == null;
		}
	};
	public static ExclusionStrategy userDetails = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(Expose.class) == null && f.getAnnotation(UserDetails.class) == null;
		}
	};
	public static ExclusionStrategy usersList = new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(LeaderboardUser.class) == null && f.getAnnotation(UserStatusList.class) == null;
		}
	};
	public static ExclusionStrategy excludedNonUserObjects= new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(ExcludedForUsers.class) != null;
		}
	};
	
	public static ExclusionStrategy excludedNonUserAndLazyObjects= new ExclusionStrategy() {
		@Override
		public boolean shouldSkipClass(Class<?> clazz) {
			return false;
		}
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getAnnotation(ExcludedForUsers.class) != null || f.getAnnotation(LazilySerialized.class) != null;
		}
	};
	public static ExclusionStrategy challengeExercises;
	
}
