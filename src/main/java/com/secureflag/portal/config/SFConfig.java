/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SFConfig {

	private static Properties config = new Properties();
	private static String guacAdminUser = "";
	private static String guacAdminPassword = "";
	private static String exercisesCluster = "";
	private static String agentUser = "";
	private static String agentPassword = "";
	private static String environment = "";
	private static String deploymentRegion = "";
	private static Boolean fargateModule = false;
	private static Boolean scaleInModule = false;
	private static Boolean imageSyncModule = false;
	private static Boolean androidModule = false;
	private static Boolean devopsModule = false;
	private static Boolean taskCoordinationModule = false;
	private static Boolean emailModule = false;
	private static Boolean cacheModule = true;
	private static String hubAddress = "";
	private static String hubAuthRole = "";
	private static String ecrPushRole = "";
	private static String website = "";

	private static HashSet<String> sfSysImages =new HashSet<>(Arrays.asList("amazon/amazon-ecs-agent","amazon/amazon-ecs-pause")); 
	private static HashSet<String> sfSysGroups = new HashSet<>(Arrays.asList("service:mysql","service:exercise-gateway","service:portal","service:hazelcast","service:analytics", "service:hub")); 

	private static Logger logger = LoggerFactory.getLogger(SFConfig.class);

	static{
		try {
			config.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(Constants.CONFIG_FILE));
			
		
			SFConfig.website = config.getProperty("website");
			SFConfig.guacAdminUser = config.getProperty("guacAdminUser");
			SFConfig.guacAdminPassword = config.getProperty("guacAdminPassword");
			SFConfig.exercisesCluster = config.getProperty("exercisesCluster");
			SFConfig.agentUser = config.getProperty("agentUser");
			SFConfig.agentPassword = config.getProperty("agentPassword");
			SFConfig.environment = config.getProperty("environment");
			SFConfig.deploymentRegion = config.getProperty("deploymentRegion");
			SFConfig.hubAddress = config.getProperty("hubAddress");
			SFConfig.hubAuthRole = config.getProperty("hubAuthRole");
			SFConfig.ecrPushRole  = config.getProperty("ecrPushRole");
			SFConfig.scaleInModule = Boolean.valueOf(config.getProperty("scaleInModule"));
			SFConfig.fargateModule = Boolean.valueOf(config.getProperty("fargateModule"));
			SFConfig.imageSyncModule = Boolean.valueOf(config.getProperty("imageSyncModule"));
			SFConfig.taskCoordinationModule = Boolean.valueOf(config.getProperty("taskCoordinationModule"));
			SFConfig.emailModule = Boolean.valueOf(config.getProperty("emailModule"));
			SFConfig.cacheModule = Boolean.valueOf(config.getProperty("cacheModule"));
			SFConfig.devopsModule = Boolean.valueOf(config.getProperty("devopsModule"));
			SFConfig.androidModule = Boolean.valueOf(config.getProperty("androidModule"));
			//set timezone to UTC irrespective of where the portal is deployed
			TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
			
			//set the DNS cache to 0
			java.security.Security.setProperty("networkaddress.cache.ttl" , "30");

			logger.info("SF Portal Configuration parsed");
		} catch (IOException e) {
			logger.error(e.toString());
		}			
	}

	public static String getGuacAdminUsername() {
		return guacAdminUser;
	}
	public static String getGuacAdminPassword() {
		return guacAdminPassword;
	}
	public static String getExercisesCluster() {
		return exercisesCluster;
	}
	public static String getAgentUser() {
		return agentUser;
	}
	public static String getAgentPassword() {
		return agentPassword;
	}
	public static String getEnvironment() {
		return environment;
	}
	public static String getRegion() {
		return deploymentRegion;
	}

	public static Boolean isFargateModuleAvailable() {
		return fargateModule;
	}
	public static Boolean isScaleInModuleAvailable() {
		return scaleInModule;
	}
	public static Boolean isImageSyncModuleAvailable() {
		return imageSyncModule;
	}
	public static Boolean isTaskCoordinationModuleAvailable() {
		return taskCoordinationModule;
	}
	public static HashSet<String> getSysImages() {
		return sfSysImages;
	}
	public static HashSet<String> getSysGroups() {
		return sfSysGroups;
	}
	public static Boolean getEmailModule() {
		return emailModule;
	}
	public static String getWebsite() {
		return website;
	}
	public static String getHubAddress() {
		return hubAddress;
	}
	public static String getHubAuthRole() {
		return hubAuthRole;
	}
	public static String getEcrPushRole() {
		return ecrPushRole;
	}
	public static Boolean getCacheModule() {
		return cacheModule;
	}
	public static Boolean getAndroidModule() {
		return androidModule;
	}
	public static Boolean getDevopsModule() {
		return devopsModule;
	}
}
