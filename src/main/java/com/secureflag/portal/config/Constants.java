/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.config;

public class Constants {

	public static final Integer ROLE_USER = 7;
	public static final Integer ROLE_STATS = 4;
	public static final Integer ROLE_TEAM_MANAGER = 3;
	public static final Integer ROLE_REVIEWER = 1;
	public static final Integer ROLE_ADMIN = 0;
	public static final Integer ROLE_SF_ADMIN = -1;

	public static final Integer STATUS_RUNNING = 0;
	public static final Integer STATUS_STOPPED = 2;
	public static final Integer STATUS_PENDING = 3;

	public static final String INDEX_PAGE = "/index.html";
	public static final String USER_HOME = "/user/index.html";
	public static final String MGMT_HOME = "/management/index.html";
	public static final String POST_SIGNUP_ADMIN_APPROVAL = "/welcome.html";
	public static final String EMAIL_VALIDATION = "/validateEmail.html";
	public static final String PASSWORD_CHANGE = "/changePassword.html";

	public static final String ATTRIBUTE_SECURITY_CSRF_TOKEN = "token";
	public static final String ATTRIBUTE_SECURITY_CONTEXT = "SECURITY_CONTEXT";
	public static final String ATTRIBUTE_SECURITY_ROLE = "SECURITY_ROLE";

	public static final String JSON_VALUE_ERROR = "error";
	public static final String JSON_VALUE_SUCCESS = "success";
	public static final String JSON_VALUE_REDIRECT = "redirect";
	public static final String JSON_ATTRIBUTE_RESULT = "result";
	public static final String JSON_ATTRIBUTE_LOCATION = "location";
	public static final String JSON_ATTRIBUTE_ERROR_MSG = "errorMsg";
	public static final String JSON_ATTRIBUTE_ACTION = "action";
	public static final String REQUEST_ATTRIBUTE_JSON = "json";

	public static final String JSON_VALUE_ERROR_ACTION_NOT_VALIDATED = "Action message not valid";

	public static final String JSON_VALUE_ERROR_ACTION_EXCEPTION = "Action exception";
	public static final String JSON_VALUE_ERROR_ACTION_INVALID_CSRF = "Invalid token";

	public static final String JSON_VALUE_ERROR_ACCOUNT_LOCKOUT = "Account lockout, please contact support.";

	public static final String CONFIG_FILE = "config.properties";

	public static final String REQUEST_JSON = "json";
	public static final String ENV_USR_PWD = "USR_PWD";

	public static final String WITH_INTERNET_OUTBOUND = "ALLOW_INTERNET";


	public static final String ACTION_PARAM_INTERNET_OUTBOUND_ALLOWED = "internetOutbound";
	public static final String ACTION_PARAM_IMAGE_SYNC = "enableImageSync";
	public static final String ACTION_PARAM_INSTANCE_SCALE_IN = "enableScaleIn";
	public static final String ACTION_PARAM_ALLOW_MANUAL_REVIEW = "allowManualReview";
	public static final String ACTION_PARAM_ALLOW_INTERNET_OUTBOUND = "allowInternetOutbound";


	public static final String ACTION_PARAM_ORG_CODE = "orgCode";
	public static final String ACTION_PARAM_ORG_ID = "orgId";

	public static final String ACTION_PARAM_ID = "id";
	public static final String ACTION_PARAM_REGION = "region";
	public static final String ACTION_PARAM_CSRF_TOKEN = "ctoken";
	public static final String ACTION_PARAM_USERNAME = "username";
	public static final String ACTION_PARAM_PASSWORD = "password";
	public static final String ACTION_PARAM_OLDPASSWORD = "oldPwd";
	public static final String ACTION_PARAM_NEWPASSWORD = "newPwd";

	public static final String ACTION_PARAM_FIRST_NAME = "firstName";
	public static final String ACTION_PARAM_LAST_NAME = "lastName";
	public static final String ACTION_PARAM_EMAIL = "email";
	public static final String ACTION_PARAM_EMAIL_VERIFIED = "emailVerified";
	public static final String ACTION_PARAM_COUNTRY = "country";

	public static final Integer FAILED_ATTEMPTS_LOCKOUT = 5;
	public static final String JSON_CONTENT_TYPE = "application/json";
	public static final String ACTION_PARAM_ID_EXERCISE_INSTANCE = "exId";
	public static final String ACTION_PARAM_FEEDBACK = "feedback";
	public static final String JSON_ATTRIBUTE_OBJ = "obj";
	public static final String ACTION_PARAM_NAME = "name";
	//add team
	public static final String ACTION_PARAM_ORG_NAME = "orgName";
	public static final String ACTION_PARAM_TEAM_NAME = "teamName";
	public static final String ACTION_PARAM_TEAM_ID = "teamId";
	public static final String ACTION_PARAM_USERNAME_LIST = "users";
	public static final String ACTION_PARAM_ORGS_LIST = "orgs";


	public static final String ACTION_PARAM_NOTIFICATION_TEXT = "text";
	public static final String ACTION_PARAM_FILTER = "filter";
	public static final String ACTION_PARAM_MAX_USERS = "maxUsers";
	public static final String ACTION_PARAM_EXERCISE = "exercise";
	public static final String ACTION_PARAM_FQDN = "fqdn";
	public static final String ACTION_PARAM_STATUS = "status";
	public static final String ACTION_PARAM_EXERCISE_ID = "exerciseId";
	public static final String ACTION_PARAM_TASK_DEFINITION_ID = "taskDefId";
	public static final String ACTION_PARAM_TASK_DEFINITION_NAME = "taskDefinitionName";
	public static final String ACTION_PARAM_CONTAINER_NAME = "containerName";
	public static final String ACTION_PARAM_REPO_URL = "imageUrl";
	public static final String ACTION_PARAM_SOFT_MEMORY = "softMemory";
	public static final String ACTION_PARAM_HARD_MEMORY = "hardMemory";
	public static final String ACTION_PARAM_ROLE_ID = "roleId";
	public static final String ACTION_PARAM_FORCE_CHANGE_PASSWORD = "forceChangePassword"; 
	public static final String ACTION_PARAM_CONCURRENT_EXERCISE_LIMIT = "concurrentExercisesLimit";

	public static final Integer MIN_HARD_MEMORY_LIMIT = 300;
	public static final Integer MAX_HARD_MEMORY_LIMIT = 5000;
	public static final Integer MIN_SOFT_MEMORY_LIMIT = 50;
	public static final Integer MAX_SOFT_MEMORY_LIMIT = 3000;
	
	public static final String AWS_ECS_STATUS_STOPPED = "stopped";
	public static final String AWS_ECS_STATUS_RUNNING = "running";
	public static final String AWS_ECS_STOPPED_NORMAL = "Task stopped by user";
	public static final String AWS_CLOUDWATCH_LASTSTATUS = "lastStatus";
	public static final String AWS_CLOUDWATCH_DETAIL = "detail";
	public static final String AWS_CLOUDWATCH_STOPPEDREASON = "stoppedReason";

	public static final String EXERCISE_EVENTS_QUEUE_NAME = "ExerciseEvents-SQS";
	public static final String EXERCISE_CLUSTER_INSTANCE_AGENT = "service:instanceagent";

	//notifications
	public static final String NOTIFICATION_TEXT_UPDATED_REVIEW = "The review for your exercise '{EXERCISE}' was updated by an Admin, check your results and leave a feedback.";
	public static final String NOTIFICATION_TEXT_SCORING_COMMENT = "An Admin added a comment for your exercise '{EXERCISE}'.";

	public static final String NOTIFICATION_TEXT_COMPLETED_REVIEW = "Exercise '{EXERCISE}' has been reviewed, check your results and leave a feedback.";
	public static final String NOTIFICATION_LINK_WELCOME_TO_SF = "#/exercises/vulnerability";
	public static final String NOTIFICATION_TEXT_WELCOME_TO_SF = "Welcome to SecureFlag, browse available exercises and start hacking!";
	public static final String NOTIFICATION_LINK_NEW_USER_ADDED = "#/users/details/{USERNAME}";
	public static final String NOTIFICATION_TEXT_NEW_USER_ADDED = "User {USERNAME} is now enrolled on SecureFlag!";
	
	public static final String NOTIFICATION_LINK_ACHIEVEMENTS = "#/achievements";
	public static final String NOTIFICATION_TEXT_NEW_TROPHY = "You have been awarded the {TROPHY} for {TECHNOLOGY}.";
	public static final String NOTIFICATION_TEXT_NEW_CERTIFICATION = "You have completed a Learning Path! You have been awarded the {TECHNOLOGY} {CERTIFICATION} ({DIFFICULTY}) certification.";
	public static final String NOTIFICATION_TEXT_NEW_PLACEMENT = "You have completed a Tournament! Your final placement in '{CHALLENGE}' is #{PLACEMENT}";


	public static final String NOTIFICATION_LINK_MGMT_EXERCISE_INSTANCE = "#/exercises/details/{EXERCISE}";
	public static final String NOTIFICATION_TEXT_SCORING_COMMENT_ADDED = "User {USERNAME} commented score for exercise '{EXERCISE}'.";
	public static final String NOTIFICATION_LINK_USER_EXERCISE_INSTANCE = "#/history/details/{EXERCISE}";
	public static final String NOTIFICATION_TEXT_FEEDBACK_ADDED = "User {USERNAME} left a feedback for exercise '{EXERCISE}'.";

	public static final String ACTION_PARAM_SUBTITLE = "subtitle";
	public static final String ACTION_PARAM_DIFFICULTY= "difficulty";
	public static final String ACTION_PARAM_TITLE= "title";
	public static final String ACTION_PARAM_TYPE= "type";

	public static final String ACTION_PARAM_DESCRIPTION= "description";
	public static final String ACTION_PARAM_TECHNOLOGY= "technology";
	public static final String ACTION_PARAM_DURATION= "duration";
	public static final String ACTION_PARAM_SCORE= "score";
	public static final String ACTION_PARAM_TROPHY_NAME= "trophyName";
	public static final String ACTION_PARAM_TROPHY_DESCRIPTION= "trophyDescription";
	public static final String ACTION_PARAM_FLAGS_LIST = "flags";
	public static final String ACTION_PARAM_CATEGORY = "category";
	public static final String ACTION_PARAM_FLAG_QUESTIONS = "flagList";

	public static final String ACTION_PARAM_EXERCISE_TYPE = "exerciseType";
	public static final String ACTION_PARAM_SELF_CHECK_AVAILABLE = "selfCheckAvailable";
	public static final String ACTION_PARAM_SELFCHECK = "selfCheck";

	public static final String ACTION_PARAM_SELF_CHECK = "selfCheckName";
	public static final String ACTION_PARAM_MD = "md";
	public static final String ACTION_PARAM_HINT_AVAILABLE = "hintAvailable";
	public static final String ACTION_PARAM_HINT = "hint";
	public static final String ACTION_PARAM_IMAGE = "image";


	public static final String ACTION_PARAM_AUTHOR = "author";
	public static final String ACTION_PARAM_CREDITS = "credits";
	public static final String SF_EXERCISE_LOG_GROUP = "Exercise-Instances";
	public static final String ACTION_PARAM_USERID_LIST = "users";
	public static final String ACTION_PARAM_EXERCISE_LIST = "exercises";

	public static final String ACTION_PARAM_DETAILS = "details";
	public static final String ACTION_PARAM_START_DATE = "startDate";
	public static final String ACTION_PARAM_END_DATE = "endDate";
	public static final String ACTION_PARAM_ID_ORG = "idOrg";
	public static final String ACTION_PARAM_SCORING_MODE = "scoringMode";
	public static final String ACTION_PARAM_CHALLENGE_ID = "challengeId";
	public static final String PROD_ENVIRONMENT = "PROD";
	public static final String ACTION_PARAM_TEXT = "text";
	public static final String ACTION_PARAM_OPTIONAL = "optional";
	public static final String ACTION_PARAM_MAX_SCORE = "maxScore";
	public static final String ACTION_PARAM_SCORING_FIRST_PLACE = "firstPlace";
	public static final String ACTION_PARAM_SCORING_SECOND_PLACE = "secondPlace";
	public static final String ACTION_PARAM_SCORING_THIRD_PLACE = "thirdPlace";
	public static final String ACTION_PARAM_COMMENT = "comment";
	public static final String ACTION_PARAM_EXPIRATION = "monthsExpiration";
	public static final String ACTION_PARAM_RENEWAL = "allowRenewal";
	public static final String ACTION_PARAM_REFRESHER = "refresherPercentage";

	public static final Integer THRESHOLD_SCALE_IN = 50;
	public static final String ACTION_PARAM_CONTAINER_INSTANCE = "containerInstance";
	public static final String MASTER_TASK_LOCK = "masterTaskLock";
	public static final String ACTION_PARAM_DEFAULT_SCORING = "defaultScoring";
	public static final String ACTION_PARAM_PLACEMENT_STRATEGY = "placementStrategy";
	public static final Object AWS_CLOUDWATCH_GROUP = "group";
	public static final String ALL_CHECKS = "all";
	public static final String PARAM_NAME_CHECK = "check";
	public static final String IP_ADDRESS = "ip";
	public static final String INFORMATION = "information";
	public static final String SOLUTION = "solution";
	public static final String STACK = "stack";
	public static final String ACTION_PARAM_IGNORE_DEFAULT_STACK = "ignoreDefaultStack";
	public static final String ACTION_PARAM_KB = "kb";
	public static final String ACTION_PARAM_TAGS = "tags";
	
	//aws
	public static final String SF_SERVICE_INSTANCEAGENT = "instanceagent";
	public static final String SF_AUTOSCALINGGROUP_EXERCISES = "ExercisesAutoScalingGroup";
	public static final String ACTION_PARAM_EXERCISE_QUALITY = "quality";
	public static final String AGNOSTIC = "Agnostic";
	public static final String ACTION_PARAM_UUID = "uuid";
	public static final String ACTION_PARAM_TOKEN = "token";
	public static final String ACTION_PARAM_REGION_LIST = "regions";
	public static final String ACTION_PARAM_CLIPBOARD_ACCESS_ALLOWED = "clipboardAllowed";
	public static final String CLIPBOARD_ALLOWED = "CLIPBOARD_ALLOWED";
	public static final String ACTION_PARAM_CRASHED_EXERCISE_ID = "crashedExerciseId";
	public static final String TELEMETRY = "telemetry";
	public static final String EXERCISE = "EXERCISE";
	public static final String ACTION_PARAM_FRAMEWORK = "framework";
	public static final String ANDROID_IP_ADDRESS = "ANDROID_IP_ADDRESS";
	public static final String ANDROID_SSH_KEY = "ANDROID_SSH_KEY";
	public static final String ACTION_PARAM_VOTE = "vote";
	public static final String ACTION_PARAM_PANEL = "panel";
	public static final String ACTION_PARAM_URL = "url";
	public static final String JSON_ATTRIBUTE_FRAMEWORK = "framework";
	public static final String JSON_ATTRIBUTE_TECHNOLOGY = "technology";
	public static final String JSON_ATTRIBUTE_TITLE = "title";
	public static final String JSON_ATTRIBUTE_UUID = "uuid";
	public static final String JSON_ATTRIBUTE_FLAGS = "flags";
	



}
