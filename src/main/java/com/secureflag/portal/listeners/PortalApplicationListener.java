/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.listeners;

import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.secureflag.portal.tasks.ChallengesStatusUpdateTask;
import com.secureflag.portal.tasks.ContainerInstancesScaleInTask;
import com.secureflag.portal.tasks.ContainersEventsSQSTask;
import com.secureflag.portal.tasks.DownloadQueueTask;
import com.secureflag.portal.tasks.ExercisesAutoShutdownTask;
import com.secureflag.portal.tasks.FargateSGCleanupTask;
import com.secureflag.portal.tasks.RetrieveHubTokenTask;

public class PortalApplicationListener implements ServletContextListener {

	private ScheduledExecutorService scheduler;
	private static Logger logger = LoggerFactory.getLogger(PortalApplicationListener.class);


	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		scheduler = Executors.newScheduledThreadPool(5);
		logger.info("starting task scheduler");
		scheduler.scheduleAtFixedRate(new ExercisesAutoShutdownTask(), 0, 4, TimeUnit.MINUTES);
		scheduler.scheduleAtFixedRate(new FargateSGCleanupTask(), 0, 9, TimeUnit.MINUTES);
		scheduler.scheduleAtFixedRate(new ChallengesStatusUpdateTask(),0, 2, TimeUnit.MINUTES);
		scheduler.scheduleAtFixedRate(new ContainerInstancesScaleInTask(),0, 5, TimeUnit.MINUTES);
		scheduler.schedule(new ContainersEventsSQSTask(), 75, TimeUnit.SECONDS);
		scheduler.schedule(new DownloadQueueTask(), 60, TimeUnit.SECONDS);
		scheduler.schedule(new RetrieveHubTokenTask(), 45, TimeUnit.SECONDS);		
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		scheduler.shutdownNow();
	}
} 
