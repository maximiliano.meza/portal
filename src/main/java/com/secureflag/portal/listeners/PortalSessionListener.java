/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.listeners;

import java.util.Date;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.secureflag.portal.config.Constants;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAuthenticationEvent;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class PortalSessionListener implements HttpSessionListener {

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private static Logger logger = LoggerFactory.getLogger(PortalSessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		User user = (User) sessionEvent.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);
		if(null!=user){
			logger.debug("Session Listener: session destroyed for user: "+user.getUsername());
			UserAuthenticationEvent attempt = new UserAuthenticationEvent();
			attempt.setLogoutDate(new Date());
			attempt.setUsername(user.getUsername());
			attempt.setSessionIdHash(DigestUtils.sha256Hex(sessionEvent.getSession().getId()));
			hpc.addLogoutEvent(attempt);
		}      
	}
}
