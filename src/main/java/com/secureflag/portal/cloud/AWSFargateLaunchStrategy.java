/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.cloud;

import java.util.Date;

import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.EC2Instance;
import com.secureflag.portal.model.ECSContainerTask;
import com.secureflag.portal.model.ECSTaskDefinition;
import com.secureflag.portal.model.ExerciseInstanceLaunchType;
import com.secureflag.portal.model.ExerciseInstanceReservation;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class AWSFargateLaunchStrategy implements LaunchStrategy {

	private User user;
	private Integer durationMinutes;
	private String instanceName;
	private String clusterName;
	private AvailableExercise exercise;
	private ECSTaskDefinition taskDefinition;
	private String tmpPassword;
	private Integer challengeId;
	private Integer crashedExerciseId; 
	private EC2Instance androidInstance;

	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();
	private AWSHelper helper = new AWSHelper();

	public AWSFargateLaunchStrategy(User user, Integer durationMinutes, String clusterName, String password, ECSTaskDefinition taskDefinition, EC2Instance androidInstance, AvailableExercise exercise, Integer challengeId, Integer crashedExerciseId) {
		this.user = user;
		this.clusterName = clusterName;
		this.tmpPassword = password;
		this.taskDefinition = taskDefinition;
		this.exercise = exercise;
		this.durationMinutes = durationMinutes;
		this.instanceName  = "SF-"+this.user.getIdUser()+"-"+System.currentTimeMillis();
		this.challengeId = challengeId;
		this.androidInstance = androidInstance;
		this.crashedExerciseId = crashedExerciseId;
	}

	@Override
	public ExerciseInstanceReservation launch() {
		
		ECSContainerTask taskInstance = helper.runFargateTask(clusterName, instanceName, tmpPassword, taskDefinition, durationMinutes, user, exercise,androidInstance);
		ExerciseInstanceReservation reservation = new ExerciseInstanceReservation();
		
		reservation.setTmpPassword(tmpPassword);
		reservation.setChallengeId(challengeId);
		reservation.setCrashedExerciseId(crashedExerciseId);
		reservation.setUser(user);
		reservation.setEc2(androidInstance);
		reservation.setOrganization(user.getDefaultOrganization());
		reservation.setType(ExerciseInstanceLaunchType.ECS);
		reservation.setExercise(exercise);
		reservation.setFulfilled(false);
		reservation.setTimesPolled(0);
		reservation.setDateRequested(new Date());
		
		if(null!=taskInstance){
			reservation.setError(false);
			reservation.setWaitSeconds(140);
			reservation.setEcs(taskInstance);
		}
		else {
			reservation.setError(true);
			reservation.setWaitSeconds(0);
			reservation.setEcs(null);
		}
		Integer id = hpc.addReservation(reservation);
		reservation.setId(id);
		return reservation;
	}
}