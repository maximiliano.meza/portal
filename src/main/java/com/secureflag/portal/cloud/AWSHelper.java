/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.cloud;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.autoscaling.model.AutoScalingGroup;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.autoscaling.model.DetachInstancesRequest;
import com.amazonaws.services.autoscaling.model.DetachInstancesResult;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupEgressRequest;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupEgressResult;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.DeleteKeyPairRequest;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeNetworkInterfacesRequest;
import com.amazonaws.services.ec2.model.DescribeNetworkInterfacesResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.IpRange;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RevokeSecurityGroupEgressRequest;
import com.amazonaws.services.ec2.model.RevokeSecurityGroupEgressResult;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesResult;
import com.amazonaws.services.ecr.AmazonECR;
import com.amazonaws.services.ecr.AmazonECRClientBuilder;
import com.amazonaws.services.ecr.model.CreateRepositoryRequest;
import com.amazonaws.services.ecr.model.CreateRepositoryResult;
import com.amazonaws.services.ecr.model.DescribeImagesRequest;
import com.amazonaws.services.ecr.model.DescribeImagesResult;
import com.amazonaws.services.ecr.model.DescribeRepositoriesRequest;
import com.amazonaws.services.ecr.model.DescribeRepositoriesResult;
import com.amazonaws.services.ecr.model.ImageDetail;
import com.amazonaws.services.ecr.model.PutLifecyclePolicyRequest;
import com.amazonaws.services.ecr.model.Repository;
import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.AmazonECSClientBuilder;
import com.amazonaws.services.ecs.model.Attachment;
import com.amazonaws.services.ecs.model.AwsVpcConfiguration;
import com.amazonaws.services.ecs.model.ContainerDefinition;
import com.amazonaws.services.ecs.model.ContainerInstance;
import com.amazonaws.services.ecs.model.ContainerOverride;
import com.amazonaws.services.ecs.model.DeregisterTaskDefinitionRequest;
import com.amazonaws.services.ecs.model.DeregisterTaskDefinitionResult;
import com.amazonaws.services.ecs.model.DescribeClustersRequest;
import com.amazonaws.services.ecs.model.DescribeClustersResult;
import com.amazonaws.services.ecs.model.DescribeContainerInstancesRequest;
import com.amazonaws.services.ecs.model.DescribeContainerInstancesResult;
import com.amazonaws.services.ecs.model.DescribeTasksRequest;
import com.amazonaws.services.ecs.model.DescribeTasksResult;
import com.amazonaws.services.ecs.model.Failure;
import com.amazonaws.services.ecs.model.KernelCapabilities;
import com.amazonaws.services.ecs.model.KeyValuePair;
import com.amazonaws.services.ecs.model.LinuxParameters;
import com.amazonaws.services.ecs.model.ListContainerInstancesRequest;
import com.amazonaws.services.ecs.model.ListContainerInstancesResult;
import com.amazonaws.services.ecs.model.ListTasksRequest;
import com.amazonaws.services.ecs.model.ListTasksResult;
import com.amazonaws.services.ecs.model.LogConfiguration;
import com.amazonaws.services.ecs.model.NetworkBinding;
import com.amazonaws.services.ecs.model.NetworkConfiguration;
import com.amazonaws.services.ecs.model.NetworkMode;
import com.amazonaws.services.ecs.model.PlacementStrategy;
import com.amazonaws.services.ecs.model.PortMapping;
import com.amazonaws.services.ecs.model.RegisterTaskDefinitionRequest;
import com.amazonaws.services.ecs.model.RegisterTaskDefinitionResult;
import com.amazonaws.services.ecs.model.Resource;
import com.amazonaws.services.ecs.model.RunTaskRequest;
import com.amazonaws.services.ecs.model.RunTaskResult;
import com.amazonaws.services.ecs.model.StopTaskRequest;
import com.amazonaws.services.ecs.model.Task;
import com.amazonaws.services.ecs.model.TaskOverride;
import com.amazonaws.services.ecs.model.UpdateContainerInstancesStateRequest;
import com.amazonaws.services.ecs.model.UpdateContainerInstancesStateResult;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.GetRoleRequest;
import com.amazonaws.services.identitymanagement.model.GetRoleResult;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.amazonaws.services.securitytoken.model.Credentials;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.config.SFConfig;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.EC2Instance;
import com.secureflag.portal.model.ECSContainerTask;
import com.secureflag.portal.model.ECSTaskDefinition;
import com.secureflag.portal.model.ECSTaskPlacementStrategy;
import com.secureflag.portal.model.ExerciseInstanceReservation;
import com.secureflag.portal.model.FargateSGForDeletion;
import com.secureflag.portal.model.User;
import com.secureflag.portal.persistence.HibernatePersistenceFacade;

public class AWSHelper {

	private static Logger logger = LoggerFactory.getLogger(AWSHelper.class);
	private HibernatePersistenceFacade hpc = new HibernatePersistenceFacade();

	public ExerciseInstanceReservation createExerciseInstance(LaunchStrategy strategy){
		return strategy.launch();
	}

	public Credentials getECRPushCredentials(String username) {
		try {

			AWSSecurityTokenService stsClient = AWSSecurityTokenServiceClientBuilder.standard().withRegion(SFConfig.getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			AssumeRoleRequest assumeRoleRequest = new AssumeRoleRequest()
					.withRoleArn(SFConfig.getEcrPushRole())
					.withDurationSeconds(3600)
					.withRoleSessionName(username);
			AssumeRoleResult result = stsClient.assumeRole(assumeRoleRequest);
			return result.getCredentials();
		}catch(Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	public Credentials getHubAWSCredentials() {
		try {
			AWSSecurityTokenService stsClient = AWSSecurityTokenServiceClientBuilder.standard().withRegion(SFConfig.getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			AssumeRoleRequest assumeRoleRequest = new AssumeRoleRequest()
					.withRoleArn(SFConfig.getHubAuthRole())
					.withDurationSeconds(3600)
					.withRoleSessionName("SF-DEPLOYMENT");;
					AssumeRoleResult result = stsClient.assumeRole(assumeRoleRequest);
					return result.getCredentials();
		}catch(Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}


	public List<String> listInstanceAgents(Regions region) {
		try {
			AmazonECS client = AmazonECSClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

			ListContainerInstancesResult desConLisRes = client.listContainerInstances(new ListContainerInstancesRequest().withCluster(SFConfig.getExercisesCluster()));
			DescribeContainerInstancesResult desConRes = client.describeContainerInstances(new DescribeContainerInstancesRequest().withContainerInstances(desConLisRes.getContainerInstanceArns()).withCluster(SFConfig.getExercisesCluster()));
			List<String> instanceIds = new LinkedList<String>();
			for(ContainerInstance c : desConRes.getContainerInstances()) {
				instanceIds.add(c.getEc2InstanceId());
			}
			AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			DescribeInstancesResult instanceResults = ec2.describeInstances(new DescribeInstancesRequest().withInstanceIds(instanceIds));
			List<String> ec2InstancesIPs = new LinkedList<String>();
			for(Reservation reservation : instanceResults.getReservations()) {
				for(Instance instance : reservation.getInstances()) {
					if(null==instance.getPrivateIpAddress()) {
						logger.error("EC2 Instance "+instance.getInstanceId()+" does not have a private IP address assigned");
						continue;
					}
					String ip = instance.getPrivateIpAddress();
					ec2InstancesIPs.add(ip+":9000");
				}
				
			}
			return ec2InstancesIPs;
		} catch(Exception e){
			logger.error("Error listing container instances "+e.getMessage());
			return new LinkedList<String>();
		}
	}

	private void checkDeleteFargateSG(String taskArn) {
		String regionFromArn = taskArn.split(":")[3];
		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(regionFromArn).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		try {
			DescribeTasksRequest describeTaskRequest = new DescribeTasksRequest();
			describeTaskRequest.withCluster(SFConfig.getExercisesCluster()).withTasks(taskArn);
			DescribeTasksResult result = client.describeTasks(describeTaskRequest);
			if( result.getTasks().get(0).getLaunchType().equals("FARGATE")) {
				for(Attachment attachment : result.getTasks().get(0).getAttachments()) {
					for(KeyValuePair pair : attachment.getDetails()) {
						if(pair.getName().equals("networkInterfaceId")) {
							AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(Regions.fromName(regionFromArn)).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

							DescribeNetworkInterfacesRequest nrequest = new DescribeNetworkInterfacesRequest()
									.withNetworkInterfaceIds(pair.getValue());
							DescribeNetworkInterfacesResult nresult = ec2.describeNetworkInterfaces(nrequest);
							String outBoundSg = getFargateSecurityGroupWithOutbound(Regions.fromName(regionFromArn));
							String currentSg = nresult.getNetworkInterfaces().get(0).getGroups().get(0).getGroupId();
							if(!currentSg.equals(outBoundSg)) {
								FargateSGForDeletion sgDeletion = new FargateSGForDeletion();
								sgDeletion.setName(currentSg);
								sgDeletion.setDate(new Date());
								sgDeletion.setRegion(Regions.fromName(regionFromArn));
								if(hpc.addFargateSGForDeletion(sgDeletion)) {
									logger.debug("Successfully marked  SG "+currentSg+" for task "+taskArn+"for deletion");
								}
								else {
									logger.error("Error marking SG "+currentSg+" for deletion for task "+taskArn);
								}
							}
							return;
						}
					}
				}
			}

		} catch(Exception e){
			logger.error("Error deleting SG for task "+taskArn+" due to:\n"+e.getMessage());
		}
	}

	public Boolean deleteSecurityGroup(String sg, Regions region) {
		try{
			AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

			DeleteSecurityGroupRequest delete_request = new DeleteSecurityGroupRequest()
					.withGroupId(sg);
			ec2.deleteSecurityGroup(delete_request);
			logger.debug("Successfully removed sg "+sg+" from stopped Fargate container in region "+region.getName());
			return true;
		}catch(Exception e) {
			logger.error("Error deleting SG "+sg+" due to:\n"+e.getMessage());
			return false;
		}
	}

	public void terminateTask(String taskArn) {
		String regionFromArn = taskArn.split(":")[3];
		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(regionFromArn).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		try {
			StopTaskRequest stopTaskRequest = new StopTaskRequest();
			stopTaskRequest.withCluster(SFConfig.getExercisesCluster()).withTask(taskArn);
			client.stopTask(stopTaskRequest);
			checkDeleteFargateSG(taskArn);
		} catch(Exception e){
			logger.error("Error terminating task "+e.getMessage());
		}
	}

	public Boolean terminateEC2Instance(Regions region, String instanceId) {
		try {
			AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest().withInstanceIds(instanceId);
			TerminateInstancesResult result = ec2.terminateInstances(terminateRequest);
			logger.debug("EC2 Instance "+result.getTerminatingInstances().get(0).toString()+" in region "+region.getName()+" terminated");
			return true;
		} catch(Exception e){
			logger.error("Error terminating instance "+instanceId+" in region "+region.getName()+" due to:\n"+e.getMessage());
			return false;
		}
	}

	public Boolean removeKeypair(String name, Regions region) {
		try {
			AmazonEC2 amazonEC2Client = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			amazonEC2Client.deleteKeyPair(new DeleteKeyPairRequest().withKeyName(name));
			return true;
		}catch(Exception e) {
			logger.error("Could not remove keypair "+name+" in region "+region.getName()+" due to:\n"+e.getMessage());
			return false;
		}
	}

	private Integer getClusterLeftMemoryCapacity(List<ContainerInstance> instances) {
		Integer totalMemory = 0;
		Integer availableMemory = 0;
		for(ContainerInstance c : instances) {
			for(Resource reg : c.getRegisteredResources()) {
				if(reg.getName().equals("MEMORY")) {
					totalMemory += reg.getIntegerValue();
					break;
				}
			}
			for(Resource av : c.getRemainingResources()) {
				if(av.getName().equals("MEMORY")) {
					availableMemory += av.getIntegerValue();
					break;
				}
			}
		}
		if(availableMemory == 0 || totalMemory == 0)
			return 0;
		else {
			logger.debug("Capacity left is "+(int) Math.floor(availableMemory * 100) / totalMemory);
			return (int) Math.floor(availableMemory * 100) / totalMemory;
		}
	}

	public void detachAndRemoveInstance(Regions region, ContainerInstance toRemove) {
		try {

			AmazonAutoScaling client = AmazonAutoScalingClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

			DescribeAutoScalingGroupsRequest describeRequest = new DescribeAutoScalingGroupsRequest();
			DescribeAutoScalingGroupsResult describeResponse = client.describeAutoScalingGroups(describeRequest);
			for(AutoScalingGroup group : describeResponse.getAutoScalingGroups()) {
				if(group.getAutoScalingGroupName().indexOf(Constants.SF_AUTOSCALINGGROUP_EXERCISES)>0) {
					DetachInstancesRequest detachRequest = new DetachInstancesRequest()
							.withInstanceIds(toRemove.getEc2InstanceId())
							.withAutoScalingGroupName(group.getAutoScalingGroupName())
							.withShouldDecrementDesiredCapacity(true);
					DetachInstancesResult detachResponse = client.detachInstances(detachRequest);
					logger.debug(detachResponse.getActivities().get(0).toString());
					AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
					TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest().withInstanceIds(toRemove.getEc2InstanceId());
					TerminateInstancesResult result = ec2.terminateInstances(terminateRequest);
					logger.debug(result.getTerminatingInstances().get(0).toString());
				}
			}			
		}catch(Exception e) {
			logger.error(e.getMessage());
		}
	}

	public ContainerInstance getClusterContainerInstanceToRemove(Regions region) {
		try {
			AmazonECS client = AmazonECSClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

			ListContainerInstancesRequest listReq = new ListContainerInstancesRequest();
			listReq.withCluster(SFConfig.getExercisesCluster());
			ListContainerInstancesResult listRes = client.listContainerInstances(listReq);

			if(listRes.getContainerInstanceArns().isEmpty())
				return null;

			DescribeContainerInstancesRequest desReq = new DescribeContainerInstancesRequest();
			desReq.withCluster(SFConfig.getExercisesCluster()).withContainerInstances(listRes.getContainerInstanceArns());
			DescribeContainerInstancesResult desRes = client.describeContainerInstances(desReq);
			List<ContainerInstance> leftRunning = desRes.getContainerInstances();

			if(desRes.getContainerInstances().size()<=1)
				return null;

			for(ContainerInstance c : desRes.getContainerInstances()) {
				if(c.getRunningTasksCount() == 0 && c.getPendingTasksCount() == 0 && System.currentTimeMillis()-c.getRegisteredAt().getTime() >= 600000) {

					leftRunning.remove(c);
					if(getClusterLeftMemoryCapacity(leftRunning)>Constants.THRESHOLD_SCALE_IN) {
						logger.debug("Draining container instance "+c.getContainerInstanceArn());
						UpdateContainerInstancesStateRequest drainRequest = new UpdateContainerInstancesStateRequest()
								.withCluster(SFConfig.getExercisesCluster()).withContainerInstances(c.getContainerInstanceArn()).withStatus("DRAINING");
						UpdateContainerInstancesStateResult drainResult = client.updateContainerInstancesState(drainRequest);
						if(drainResult.getContainerInstances().get(0).getStatus().equals("DRAINING")) {
							return drainResult.getContainerInstances().get(0);
						}
					}
				}
			}
			return null;
		} catch(Exception e){
			logger.error("Error listing container instances "+e.getMessage());
			return null;
		}
	}

	public Integer getNumberClusterContainerInstances(Region region) {
		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(region.getName()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		DescribeClustersRequest request = new DescribeClustersRequest();
		request.withClusters(SFConfig.getExercisesCluster());
		logger.debug("Requesting number of cluster running instances for region "+region.getName()+" cluster "+SFConfig.getExercisesCluster());
		try {
			DescribeClustersResult response = client.describeClusters(request);

			return response.getClusters().get(0).getRegisteredContainerInstancesCount();
		}
		catch(Exception e){
			logger.error("Error getClusterContainerInstances for region "+region.getName()+" due to:\n"+e.getMessage());
			return 0;
		}
	}

	public Double getClusterMemoryReservation(Region region) {
		AmazonCloudWatch client = AmazonCloudWatchClientBuilder.standard().withRegion(region.getName()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

		Dimension dimension = new Dimension();
		dimension.setName("ClusterName");
		dimension.setValue(SFConfig.getExercisesCluster());
		Date date= new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, -5);
		GetMetricStatisticsRequest request= new GetMetricStatisticsRequest()
				.withMetricName("MemoryReservation")
				.withDimensions(dimension)
				.withPeriod(60)
				.withStartTime(cal.getTime())
				.withEndTime(date)
				.withStatistics("Average")
				.withNamespace("AWS/ECS");
		try {
			logger.debug("Requesting memory reservation for region "+region.getName()+" cluster "+SFConfig.getExercisesCluster());

			GetMetricStatisticsResult response = client.getMetricStatistics(request);
			if(response.getDatapoints().isEmpty())
				return 0.0;
			return response.getDatapoints().get(0).getAverage();
		}catch(Exception e){
			logger.error("Error getClusterContainerInstances for memory reservation in region "+region.getName()+" due to:\n"+e.getMessage());
			return 0.0;
		}
	}

	public String registerFargateTaskDefinition( ECSTaskDefinition taskDef, User user){

		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(taskDef.getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

		RegisterTaskDefinitionRequest request = new RegisterTaskDefinitionRequest();

		ArrayList<PortMapping> portMappings = new ArrayList<PortMapping>();
		portMappings.add(new PortMapping().withContainerPort(8080)
				.withHostPort(8080)
				.withProtocol("tcp"));
		portMappings.add(new PortMapping().withContainerPort(3389)
				.withHostPort(3389)
				.withProtocol("tcp"));

		LogConfiguration logConfiguration = new LogConfiguration();
		logConfiguration.setLogDriver("awslogs");
		Map<String, String> options = new HashMap<String,String>();
		options.put("awslogs-stream-prefix", "Fargate");
		options.put("awslogs-group", Constants.SF_EXERCISE_LOG_GROUP);
		options.put("awslogs-region",taskDef.getRegion().getName().toLowerCase());
		logConfiguration.setOptions(options);

		final ContainerDefinition def = new ContainerDefinition()
				.withName(taskDef.getContainerName())
				.withImage(taskDef.getRepositoryImageUrl())
				.withMemoryReservation(taskDef.getSoftMemoryLimit())
				.withMemory(taskDef.getHardMemoryLimit())
				.withPortMappings(portMappings)
				.withLogConfiguration(logConfiguration )
				.withEssential(true);

		AmazonIdentityManagement iam = AmazonIdentityManagementClientBuilder.standard().build();
		GetRoleRequest roleReq = new GetRoleRequest().withRoleName("ecsTaskExecutionRole");
		GetRoleResult roleRes = iam.getRole(roleReq);
		if(null==roleRes.getRole() || null== roleRes.getRole().getArn()) {
			logger.error("# Fargate Task Definition "+taskDef.getTaskDefinitionName()+" for user "+user.getIdUser()+" in region "+taskDef.getRegion()+" failed to get ARN for execution role");
			return null;
		}
		request.setExecutionRoleArn(roleRes.getRole().getArn());
		request.setContainerDefinitions(Arrays.asList(def));
		request.setFamily(taskDef.getTaskDefinitionName()+"-fargate");
		request.setCpu("2 vcpu");
		Integer fMemory = (int) Math.ceil(taskDef.getSoftMemoryLimit()/1000.0);
		if(fMemory<4)
			fMemory = 4;
		String fargateMemory = fMemory + "GB";
		request.setMemory(fargateMemory);
		List<String> compatibilities = new LinkedList<String>();
		compatibilities.add("FARGATE");
		request.setRequiresCompatibilities(compatibilities);
		request.setNetworkMode(NetworkMode.Awsvpc);

		try {
			RegisterTaskDefinitionResult response = client.registerTaskDefinition(request);
			logger.debug("# Fargate Task Definition "+taskDef.getTaskDefinitionName()+" created for user "+user.getIdUser()+" in region "+taskDef.getRegion());
			return response.getTaskDefinition().getTaskDefinitionArn();
		}catch(Exception e) {
			logger.debug("# Fargate Task Definition "+taskDef.getTaskDefinitionName()+" COULD NOT BE created for user "+user.getIdUser()+" in region "+taskDef.getRegion()+"\n"+e.getMessage());
			return null;
		}
	}

	public String registerECSTaskDefinition( ECSTaskDefinition taskDef, User user){

		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(taskDef.getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

		RegisterTaskDefinitionRequest request = new RegisterTaskDefinitionRequest();

		ArrayList<PortMapping> portMappings = new ArrayList<PortMapping>();
		portMappings.add(new PortMapping().withContainerPort(8080)
				.withHostPort(0)
				.withProtocol("tcp"));
		portMappings.add(new PortMapping().withContainerPort(3389)
				.withHostPort(0)
				.withProtocol("tcp"));

		LinuxParameters linuxParameters = new LinuxParameters();
		KernelCapabilities capabilities = new KernelCapabilities();
		//capabilities.setAdd(Arrays.asList("NET_ADMIN"));
		capabilities.setDrop(Arrays.asList("NET_RAW"));
		linuxParameters.setCapabilities(capabilities);

		LogConfiguration logConfiguration = new LogConfiguration();
		logConfiguration.setLogDriver("awslogs");
		Map<String, String> options = new HashMap<String,String>();
		options.put("awslogs-group", Constants.SF_EXERCISE_LOG_GROUP);
		options.put("awslogs-region",taskDef.getRegion().getName().toLowerCase());
		logConfiguration.setOptions(options);

		final ContainerDefinition def = new ContainerDefinition()
				.withName(taskDef.getContainerName())
				.withImage(taskDef.getRepositoryImageUrl())
				.withMemoryReservation(taskDef.getSoftMemoryLimit())
				.withMemory(taskDef.getHardMemoryLimit())
				.withPortMappings(portMappings)
				.withLinuxParameters(linuxParameters)
				.withLogConfiguration(logConfiguration )
				.withEssential(true);

		request.setContainerDefinitions(Arrays.asList(def));
		request.setFamily(taskDef.getTaskDefinitionName());
		List<String> compatibilities = new LinkedList<String>();
		compatibilities.add("EC2");
		request.setRequiresCompatibilities(compatibilities);
		request.setNetworkMode(NetworkMode.Bridge);
		try {
			RegisterTaskDefinitionResult response = client.registerTaskDefinition(request);
			logger.debug("# ECS Task Definition "+taskDef.getTaskDefinitionName()+" created for user "+user.getIdUser()+" in region "+taskDef.getRegion());
			return response.getTaskDefinition().getTaskDefinitionArn();
		}catch(Exception e) {
			logger.debug("# ECS Task Definition "+taskDef.getTaskDefinitionName()+" COULD NOT BE created for user "+user.getIdUser()+" in region "+taskDef.getRegion()+"\n"+e.getMessage());
			return null;
		}
	}

	public Boolean removeTaskDefinitionInRegion(String taskDefinitionArn, Regions region) {
		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		try {
			DeregisterTaskDefinitionRequest request = new DeregisterTaskDefinitionRequest().withTaskDefinition(taskDefinitionArn);
			DeregisterTaskDefinitionResult result = client.deregisterTaskDefinition(request);
			return result.getSdkHttpMetadata().getHttpStatusCode() == 200;
		}catch(Exception e) {
			logger.warn("# ECS TaskDefinition "+taskDefinitionArn+" could not be deregistered "+e.getMessage());
			return false;
		}
	}



	

	public String getFargateSecurityGroupWithOutbound(Regions region) {
		try {
			AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			DescribeSecurityGroupsRequest request = new DescribeSecurityGroupsRequest().withFilters( new Filter("tag:Name", Arrays.asList("*FargateWithOutbound*")));
			DescribeSecurityGroupsResult result = ec2.describeSecurityGroups(request);
			if(!result.getSecurityGroups().isEmpty())
				return result.getSecurityGroups().get(0).getGroupId();
			return null;
		} catch(Exception e){
			logger.error("Error returning fargate security group for region: "+region+" with outbound due to:\n"+e.getMessage());
			return null;
		}
	}

	public String[] getFargateSubnet(Regions region) {

		AmazonAutoScaling client = AmazonAutoScalingClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		try {
			DescribeAutoScalingGroupsResult desRes = client.describeAutoScalingGroups();
			for(AutoScalingGroup a :desRes.getAutoScalingGroups()) {
				if(a.getAutoScalingGroupName().indexOf(Constants.SF_AUTOSCALINGGROUP_EXERCISES)>-1) {
					return a.getVPCZoneIdentifier().split(",");
				}
			}
			return null;
		} catch(Exception e){
			logger.error("Error returning fargate subnet for cluster:"+SFConfig.getExercisesCluster()+" in region "+region+" due to:\n"+e.getMessage());
			return null;
		}
	}



	public ECSContainerTask runFargateTask(String clusterName, String instanceName, String password, ECSTaskDefinition taskDef, Integer duration, User user, AvailableExercise exercise, EC2Instance androidInstance){
		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(taskDef.getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		TaskOverride overrides = new TaskOverride();
		List<ContainerOverride> containerOverrides = new LinkedList<ContainerOverride>();
		ContainerOverride co = new ContainerOverride();
		List<KeyValuePair> environment  = new LinkedList<KeyValuePair>();
		KeyValuePair kv = new KeyValuePair();
		kv.setName(Constants.ENV_USR_PWD);
		kv.setValue(password);
		environment.add(kv);
		KeyValuePair kv4 = new KeyValuePair();
		kv4.setName(Constants.EXERCISE);
		String exerciseLight = Base64.getEncoder().encodeToString(MessageGenerator.getExerciseLightInfoMessage(exercise).getBytes());
		kv4.setValue(exerciseLight);
		environment.add(kv4);
		if(null!=androidInstance && null!=androidInstance.getPrivateIpAddress()) {
			KeyValuePair kv5 = new KeyValuePair();
			kv5.setName(Constants.ANDROID_IP_ADDRESS);
			kv5.setValue(androidInstance.getPrivateIpAddress());
			environment.add(kv5);	
		}
		if(null!=androidInstance && null!=androidInstance.getKeyPairMaterial()) {
			KeyValuePair kv6 = new KeyValuePair();
			kv6.setName(Constants.ANDROID_SSH_KEY);
			kv6.setValue(androidInstance.getKeyPairMaterial());
			environment.add(kv6);
		}
		co.setEnvironment(environment);
		co.setName(taskDef.getContainerName());
		containerOverrides.add(co);
		overrides.setContainerOverrides(containerOverrides);	

		AwsVpcConfiguration awsVpcConfiguration = new AwsVpcConfiguration();
		awsVpcConfiguration.setAssignPublicIp("ENABLED");
		String sg = "";
		sg = createSecurityGroupForFargateContainer(taskDef.getRegion(),instanceName);
		if(null==sg) {
			logger.error("# Task  for user "+user.getIdUser()+" with task definition: "+taskDef.getFargateDefinitionArn()+" on cluster: "+clusterName+" on region "+taskDef.getRegion().getName()+" could not be launched because Fargate SG could not be created.");
			return null;
		}
		awsVpcConfiguration.setSecurityGroups(Arrays.asList(sg));
		awsVpcConfiguration.setSubnets(Arrays.asList(getFargateSubnet(taskDef.getRegion())));
		NetworkConfiguration networkConfiguration = new NetworkConfiguration();
		networkConfiguration.withAwsvpcConfiguration(awsVpcConfiguration);

		RunTaskRequest request = new RunTaskRequest().withNetworkConfiguration(networkConfiguration).withLaunchType("FARGATE").withCluster(clusterName).withTaskDefinition(taskDef.getFargateDefinitionArn()).withOverrides(overrides);
		logger.debug("# ECS Requesting Task "+instanceName+" for user "+user.getIdUser()+" with task definition: "+taskDef.getFargateDefinitionArn()+" on cluster: "+clusterName+" on region "+taskDef.getRegion().getName());
		try {
			RunTaskResult response = client.runTask(request);
			String failureReason = "";
			if(response.getTasks().isEmpty()) {
				for(Failure failure : response.getFailures()) {
					failureReason += "\n"+ failure.getReason();
				}
				logger.error("Fargate Task creation failed due to: "+failureReason);
				return null;
			}
			Task task = response.getTasks().get(0);
			ECSContainerTask taskInstance = new ECSContainerTask();
			taskInstance.setCluster(task.getClusterArn());
			taskInstance.setTaskArn(task.getTaskArn());
			taskInstance.setIdContainerInstance(task.getContainerInstanceArn());
			taskInstance.setName(instanceName);
			taskInstance.setRegion(taskDef.getRegion());
			taskInstance.setUser(user);
			taskInstance.setSecurityGroup(sg);
			taskInstance.setCreateTime(task.getCreatedAt());
			taskInstance.setStatus(Constants.STATUS_PENDING);
			logger.info("# ECS Task "+instanceName+" created for user "+user.getIdUser()+" with task definition: "+taskDef.getFargateDefinitionName()+" start: "+taskInstance.getCreateTime());
			return taskInstance;
		}catch(Exception e) {
			logger.warn("# ECS Task "+instanceName+" could not be created for user "+user.getIdUser()+" "+e.getMessage());
			return null;
		}
	}

	private String createSecurityGroupForFargateContainer(Regions region, String name) {

		try {
			AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

			DescribeSecurityGroupsRequest request = new DescribeSecurityGroupsRequest().withFilters( new Filter("tag:Name", Arrays.asList("*FargateWithOutbound*")));
			DescribeSecurityGroupsResult result = ec2.describeSecurityGroups(request);

			if(result.getSecurityGroups().isEmpty()) {
				logger.error("Could not create sg for fargate container "+name+" in region "+region.getName()+" as SG withOutbound could not be described");
				return null;
			}
			SecurityGroup withOutbound = result.getSecurityGroups().get(0);

			CreateSecurityGroupRequest create_request = new CreateSecurityGroupRequest()
					.withGroupName("SG"+name)
					.withDescription("Fargate SG for Task "+name)
					.withVpcId(withOutbound.getVpcId());
			CreateSecurityGroupResult create_response =  ec2.createSecurityGroup(create_request);

			if(null==create_response.getGroupId()) {
				logger.error("Could not create sg for fargate container "+name+" in region "+region.getName()+" as new SG could not be created");
				return null;
			}
			AuthorizeSecurityGroupIngressRequest auth_request = new AuthorizeSecurityGroupIngressRequest()
					.withGroupId(create_response.getGroupId())
					.withIpPermissions( withOutbound.getIpPermissions());

			ec2.authorizeSecurityGroupIngress(auth_request);
			return create_response.getGroupId();

		}catch(Exception e) {
			logger.error("Could not create sg for fargate container "+name+" in region "+region.getName()+" due to:\n"+e.getMessage());
			return null;
		}

	}

	protected ECSContainerTask runECSTask(String clusterName, String instanceName, String password, ECSTaskDefinition taskDef, Integer duration, User user, ECSTaskPlacementStrategy placementStrategy,AvailableExercise exercise, EC2Instance androidInstance){

		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(taskDef.getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		TaskOverride overrides = new TaskOverride();
		List<ContainerOverride> containerOverrides = new LinkedList<ContainerOverride>();
		ContainerOverride co = new ContainerOverride();
		List<KeyValuePair> environmentVariables  = new LinkedList<KeyValuePair>();
		KeyValuePair kv = new KeyValuePair();
		kv.setName(Constants.ENV_USR_PWD);
		kv.setValue(password);
		environmentVariables.add(kv);
		KeyValuePair kv4 = new KeyValuePair();
		kv4.setName(Constants.EXERCISE);
		String exerciseLight = Base64.getEncoder().encodeToString(MessageGenerator.getExerciseMessage(exercise).getBytes());
		kv4.setValue(exerciseLight);
		environmentVariables.add(kv4);
		co.setEnvironment(environmentVariables);
		co.setName(taskDef.getContainerName());
		co.setCpu(680);
		containerOverrides.add(co);
		overrides.setContainerOverrides(containerOverrides);	
		PlacementStrategy strategy = null;
		if(placementStrategy.equals(ECSTaskPlacementStrategy.SPREAD))
			strategy = new PlacementStrategy().withType("spread").withField("host");
		else
			strategy = new PlacementStrategy().withType("binpack").withField("memory");

		RunTaskRequest request = new RunTaskRequest().withLaunchType("EC2").withCluster(clusterName).withTaskDefinition(taskDef.getTaskDefinitionArn()).withOverrides(overrides).withPlacementStrategy(strategy);
		logger.debug("# ECS Requesting Task "+instanceName+" for user "+user.getIdUser()+" with task definition: "+taskDef.getTaskDefinitionArn()+" "
				+ "on cluster: "+clusterName+" on region "+taskDef.getRegion().getName()+" with placement strategy: "+strategy.getType()+"/"+strategy.getField());
		try {
			RunTaskResult response = client.runTask(request);
			String failureReason = "";
			if(response.getTasks().isEmpty()) {
				for(Failure failure : response.getFailures()) {
					failureReason += "\n"+ failure.getReason();
				}
				logger.warn("ECS Task creation failed due to: "+failureReason);
				return null;
			}
			Task task = response.getTasks().get(0);
			ECSContainerTask taskInstance = new ECSContainerTask();
			taskInstance.setCluster(task.getClusterArn());
			taskInstance.setTaskArn(task.getTaskArn());
			taskInstance.setIdContainerInstance(task.getContainerInstanceArn());
			taskInstance.setName(instanceName);
			taskInstance.setSecurityGroup("ecsDefault");
			taskInstance.setRegion(taskDef.getRegion());
			taskInstance.setUser(user);
			taskInstance.setCreateTime(task.getCreatedAt());
			taskInstance.setStatus(Constants.STATUS_PENDING);
			logger.info("# ECS Task "+instanceName+" created for user "+user.getIdUser()+" with task definition: "+taskDef.getTaskDefinitionName()+" start: "+taskInstance.getCreateTime());
			return taskInstance;
		}catch(Exception e) {
			logger.warn("# ECS Task "+instanceName+" could not be created for user "+user.getIdUser()+" "+e.getMessage());
			return null;
		}
	}

	public List<String> getRunningECSTasks(List<Region> activeRegions){
		LinkedList<String> list = new LinkedList<String>();
		for(Region region : activeRegions) {
			logger.debug("Enumerating running tasks on cluster "+SFConfig.getExercisesCluster()+" for region "+region.getName());
			AmazonECS client = AmazonECSClientBuilder.standard().withRegion(Regions.fromName(region.getName())).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			ListTasksRequest request = new ListTasksRequest().withCluster(SFConfig.getExercisesCluster());
			try {
				ListTasksResult response = client.listTasks(request);
				list.addAll(response.getTaskArns());
			}catch(Exception e) {
				logger.error("Error getRunningECSTasks for region "+region+" due to:\n"+e.getMessage());
			}
		}
		return list;
	}

	public String getRunningECSGroup(String taskArn){
		String regionFromArn = taskArn.split(":")[3];
		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(regionFromArn).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		DescribeTasksRequest request = new DescribeTasksRequest().withCluster(SFConfig.getExercisesCluster()).withTasks(taskArn);
		try {
			DescribeTasksResult response = client.describeTasks(request);
			return response.getTasks().get(0).getGroup();
		}catch(Exception e) {
			logger.error("Could not get group for task arn "+taskArn);
			return null;
		}
	}

	public Date getRunningECSTaskStartTime(String taskArn){
		String regionFromArn = taskArn.split(":")[3];
		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(regionFromArn).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
		DescribeTasksRequest request = new DescribeTasksRequest().withCluster(SFConfig.getExercisesCluster()).withTasks(taskArn);
		try {
			DescribeTasksResult response = client.describeTasks(request);
			return response.getTasks().get(0).getCreatedAt();
		}catch(Exception e) {
			logger.error("Could not get creation time for task arn "+taskArn);
			return null;
		}
	}

	public ExerciseInstanceReservation pollReservation(ExerciseInstanceReservation reservation) {

		if(null==reservation.getEcs()) {
			reservation.setError(true);
			reservation.setFulfilled(false);
			reservation.setWaitSeconds(0);
			return reservation;
		}
		AmazonECS client = AmazonECSClientBuilder.standard().withRegion(reservation.getEcs().getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

		DescribeTasksRequest request = new DescribeTasksRequest().withCluster(SFConfig.getExercisesCluster()).withTasks(reservation.getEcs().getTaskArn());
		try {
			DescribeTasksResult response = client.describeTasks(request);


			if(response.getTasks().isEmpty()) {
				reservation.setError(true);
				reservation.setFulfilled(true);
				reservation.setWaitSeconds(0);
				return reservation;
			}

			Integer rdpPort = -1;
			Integer httpPort = -1;

			Task task =  response.getTasks().get(0);


			if(task.getLastStatus().equalsIgnoreCase(Constants.AWS_ECS_STATUS_STOPPED)) {
				reservation.setError(true);
				reservation.setFulfilled(true);
				reservation.setWaitSeconds(0);
				return reservation;
			}
			if(!task.getLastStatus().equalsIgnoreCase(Constants.AWS_ECS_STATUS_RUNNING)) {
				
				reservation.setFulfilled(false);
				reservation.setError(false);
				reservation.setWaitSeconds(30);

				return reservation;
			}

			reservation.setDateRunning(task.getStartedAt());

			List<NetworkBinding> nb = task.getContainers().get(0).getNetworkBindings();
			if(nb.size()>2) {
				logger.warn("More than two port bindings, only RDP 3389 and HTTP 8080 will be mapped");
			}
			if(task.getLaunchType().equals("FARGATE")) {
				rdpPort = 3389;
				httpPort = 8080;
			}
			else {
				for(NetworkBinding b : nb) {
					if(b.getContainerPort().equals(3389)) {
						rdpPort = b.getHostPort();
					}
					else if(b.getContainerPort().equals(8080)) {
						httpPort = b.getHostPort();
					}
				}
			}
			if(rdpPort== -1 || httpPort == -1) {
				reservation.setFulfilled(false);
				reservation.setError(false);
				reservation.setWaitSeconds(10);
				return reservation;
			}

			reservation.getEcs().setHttpPort(httpPort);
			reservation.getEcs().setRdpPort(rdpPort);

			String ipAddress = null;
			if(task.getLaunchType().equals("EC2")) {
				String containerInstanceId = task.getContainerInstanceArn();
				DescribeContainerInstancesRequest containerRequest = new DescribeContainerInstancesRequest().withCluster(SFConfig.getExercisesCluster()).withContainerInstances(containerInstanceId);
				DescribeContainerInstancesResult containerResponse = client.describeContainerInstances(containerRequest);
				if(containerResponse.getContainerInstances().isEmpty()) {
					reservation.setFulfilled(false);
					reservation.setError(true);
					reservation.setWaitSeconds(0);
					return reservation;
				}
				String ec2InstanceId = containerResponse.getContainerInstances().get(0).getEc2InstanceId();
				AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(reservation.getEcs().getRegion()).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
				DescribeInstancesRequest instanceRequest = new DescribeInstancesRequest().withInstanceIds(ec2InstanceId);
				DescribeInstancesResult instanceInstances = ec2.describeInstances(instanceRequest);

				if(instanceInstances.getReservations().isEmpty() || instanceInstances.getReservations().get(0).getInstances().isEmpty()) {
					reservation.setFulfilled(false);
					reservation.setError(true);
					reservation.setWaitSeconds(0);
					return reservation;
				}

				ipAddress = instanceInstances.getReservations().get(0).getInstances().get(0).getPrivateIpAddress();
			}
			else {
				ipAddress = task.getContainers().get(0).getNetworkInterfaces().get(0).getPrivateIpv4Address();	
			}
			reservation.getEcs().setIpAddress(ipAddress);
			reservation.setFulfilled(true);
			reservation.setError(false);
			reservation.setWaitSeconds(0);		
			return reservation;

		}catch(Exception e) {
			logger.error("Error pollReservation "+e.getMessage());
			reservation.setError(true);
			reservation.setFulfilled(false);
			reservation.setWaitSeconds(0);
			return reservation;
		}
	}

	public Boolean updateFargateNoOutboundSG(String fargateSg, Regions region) {

		IpRange range10 = new IpRange().withCidrIp("10.0.0.0/8");
		IpRange range127 = new IpRange().withCidrIp("127.0.0.1/32");
		IpRange range172 = new IpRange().withCidrIp("172.16.0.0/12");
		IpRange range192 = new IpRange().withCidrIp("192.168.0.0/16");

		IpPermission egress_permissions = new IpPermission()
				.withIpProtocol("-1")
				.withToPort(65535)
				.withFromPort(0)
				.withIpv4Ranges(range10,range127,range172, range192);

		logger.debug("Updating FG Security group: "+fargateSg+" in region: "+region.getName()+" with egress filtering for no outbound");

		IpPermission revoke_permission = new IpPermission()
				.withIpProtocol("-1")
				.withToPort(65535)
				.withFromPort(0)
				.withIpv4Ranges(new IpRange().withCidrIp("0.0.0.0/0"));

		RevokeSecurityGroupEgressRequest revokeSecurityGroupEgressRequest = new RevokeSecurityGroupEgressRequest()
				.withGroupId(fargateSg)
				.withIpPermissions(revoke_permission);

		AuthorizeSecurityGroupEgressRequest authorizeSecurityGroupEgressRequest  = new AuthorizeSecurityGroupEgressRequest()
				.withGroupId(fargateSg)
				.withIpPermissions(egress_permissions);

		try {
			AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			RevokeSecurityGroupEgressResult revokeResult = ec2.revokeSecurityGroupEgress(revokeSecurityGroupEgressRequest);
			logger.debug("Successfully revoked ALLOW ALLO egress rule for sg "+fargateSg+" due to:\n"+revokeResult.getSdkResponseMetadata().toString());
			AuthorizeSecurityGroupEgressResult egressResponse = ec2.authorizeSecurityGroupEgress(authorizeSecurityGroupEgressRequest);
			logger.debug("Successfully added egress rule for egress no outbound for sg "+fargateSg+" due to:\n"+egressResponse.getSdkResponseMetadata().toString());
			return true;
		}catch(Exception e) {
			logger.warn("Could not update egrress rules for not outbound for sg "+fargateSg+" due to:\n"+e.getMessage());
			return false;

		}
	}
	private String getDefaultLifecyclePolicy() {
		return "{\"rules\": [{\"action\": {\"type\": \"expire\"},\"selection\": {\"countType\": \"imageCountMoreThan\",\"countNumber\": 1,\"tagStatus\": \"any\"},\"description\": \"keep 1\",\"rulePriority\": 1}]}";
	}


	public String createECRRepository(String region, String name) {
		AmazonECR ecr = AmazonECRClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

		DescribeRepositoriesResult result = ecr.describeRepositories(new DescribeRepositoriesRequest());
		for(Repository repo : result.getRepositories()) {
			if(repo.getRepositoryName().equals(name)) {
				logger.info("Repository for "+name+" in region "+region+" already exists...");
				return repo.getRepositoryUri();
			}
		}
		try {
			CreateRepositoryResult res = ecr.createRepository(new CreateRepositoryRequest().withRepositoryName(name));
			logger.info("Created repository for "+name+" in region "+region+"");

			ecr.putLifecyclePolicy(new PutLifecyclePolicyRequest().withLifecyclePolicyText(getDefaultLifecyclePolicy()).withRepositoryName(name));

			return res.getRepository().getRepositoryUri();

		}catch(Exception e) {
			logger.warn("Could not create repository for "+name+" in region "+region+", due to:\n"+e.getMessage());
			return null;
		}
	}

	public Date getLastUpdateDate(String region, String repo) {
		try {
			if(repo.indexOf(":")>=0)
				repo = repo.substring(0,repo.indexOf(":"));

			AmazonECR ecr = AmazonECRClientBuilder.standard().withRegion(region).withCredentials(new DefaultAWSCredentialsProviderChain()).build();
			DescribeImagesResult result = ecr.describeImages(new DescribeImagesRequest().withRepositoryName(repo));
			List<ImageDetail> imageDetails = result.getImageDetails();
			for(ImageDetail image : imageDetails) {
				if(null!=image.getImageTags() && image.getImageTags().contains("latest")) {
					return image.getImagePushedAt();
				}
			}
			return null;
		}catch(Exception e) {
			return null;
		}
	}

	

	

	
}