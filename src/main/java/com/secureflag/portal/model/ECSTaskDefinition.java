/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.amazonaws.regions.Regions;
import com.google.gson.annotations.Expose;

@Entity(name = "ECSTaskDefinition")
@Table( name = "ecsTaskDefinitions" )
public class ECSTaskDefinition {

	@Id
	@Column(name = "idImage")
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "taskDefinitionName")
	@Expose
	private String taskDefinitionName;
	
	@Column(name = "fargateDefinitionName")
	@Expose
	private String fargateDefinitionName;
	
	@Column(name = "taskDefinitionArn")
	private String taskDefinitionArn;
	
	@Column(name = "fargateDefinitionArn")
	private String fargateDefinitionArn;
	
	@Column(name = "containerName")
	@Expose
	private String containerName;
	
	@Column(name = "repositoryImageUrl")
	@Expose
	private String repositoryImageUrl;
	
	@Column(name = "softMemoryLimit")
	@Expose
	private Integer softMemoryLimit;
	
	@Column(name = "hardMemoryLimit")
	@Expose
	private Integer hardMemoryLimit;
	
    @Enumerated(EnumType.STRING)
	@Column(name = "region")
	private Regions region;

	@Column(name = "updateDate")
	@Expose
	private Date updateDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaskDefinitionName() {
		return taskDefinitionName;
	}

	public void setTaskDefinitionName(String name) {
		this.taskDefinitionName = name;
	}

	public Regions getRegion() {
		return region;
	}

	public void setRegion(Regions region) {
		this.region = region;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getRepositoryImageUrl() {
		return repositoryImageUrl;
	}

	public void setRepositoryImageUrl(String repositoryImageUrl) {
		this.repositoryImageUrl = repositoryImageUrl;
	}

	public Integer getSoftMemoryLimit() {
		return softMemoryLimit;
	}

	public void setSoftMemoryLimit(Integer softMemoryLimit) {
		this.softMemoryLimit = softMemoryLimit;
	}

	public Integer getHardMemoryLimit() {
		return hardMemoryLimit;
	}

	public void setHardMemoryLimit(Integer hardMemoryLimit) {
		this.hardMemoryLimit = hardMemoryLimit;
	}

	public String getTaskDefinitionArn() {
		return taskDefinitionArn;
	}

	public void setTaskDefinitionArn(String taskDefinitionArn) {
		this.taskDefinitionArn = taskDefinitionArn;
	}

	public String getFargateDefinitionName() {
		return fargateDefinitionName;
	}

	public void setFargateDefinitionName(String fargateDefinitionName) {
		this.fargateDefinitionName = fargateDefinitionName;
	}

	public String getFargateDefinitionArn() {
		return fargateDefinitionArn;
	}

	public void setFargateDefinitionArn(String fargateDefinitionArn) {
		this.fargateDefinitionArn = fargateDefinitionArn;
	}


	
	
}