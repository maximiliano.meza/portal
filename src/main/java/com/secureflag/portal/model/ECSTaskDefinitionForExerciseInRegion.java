/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.amazonaws.regions.Regions;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity( name = "ECSTaskDefinitionForExerciseInRegion" )
@Table( name = "taskDefinitionsForExerciseInRegion" )
public class ECSTaskDefinitionForExerciseInRegion {

	@Id
	@Column(name = "idTaskDefinitionForExerciseInRegion")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@SerializedName("taskDefinition")
	@Expose
	@Cascade({CascadeType.ALL})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "taskDefinition")
	private ECSTaskDefinition taskDefinition;

	@Enumerated(EnumType.STRING)
	@Column(name="region")
	@Expose
	private Regions region;

	@SerializedName("exercise")
	@Cascade({CascadeType.SAVE_UPDATE})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "exerciseId")
	private AvailableExercise exercise;

	@Column(name = "active")
	@Expose
	private Boolean active;

	public ECSTaskDefinition getTaskDefinition() {
		return taskDefinition;
	}
	public void setTaskDefinition(ECSTaskDefinition image) {
		this.taskDefinition = image;
	}
	public Regions getRegion() {
		return region;
	}
	public void setRegion(Regions region) {
		this.region = region;
	}
	public AvailableExercise getExercise() {
		return exercise;
	}
	public void setExercise(AvailableExercise exercise) {
		this.exercise = exercise;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
}