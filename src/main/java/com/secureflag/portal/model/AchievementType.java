package com.secureflag.portal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum AchievementType {

	TROPHY(0),
	CHALLENGE(1),
	CERTIFICATION(2),
	;

	public static final AchievementType DEFAULT_STATUS = TROPHY;

	private AchievementType(Integer statusCode){
		this.id = statusCode;
	}

	@SerializedName("id")
	@Expose
	private Integer id;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}
