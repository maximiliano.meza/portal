package com.secureflag.portal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table( name = "validationTokens" )
public class ValidationToken {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idToken")
	@SerializedName("id")
	@Expose
	private Integer idToken;
	
	@Expose
	@SerializedName("user")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	private User user;
	
	@Expose
	@SerializedName("expiration")
	@Column(name="expiration")
	private Date expiration;
	
	@Expose
	@SerializedName("token")
	@Column(name="token")
	private String token;
	
	@Expose
	@SerializedName("service")
	@Column(name="service")
	@Enumerated(EnumType.STRING)
	private ValidationServiceType service;
	
	
	public Integer getIdToken() {
		return idToken;
	}
	public void setIdToken(Integer idToken) {
		this.idToken = idToken;
	}
	public Date getExpiration() {
		return expiration;
	}
	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public ValidationServiceType getService() {
		return service;
	}
	public void setService(ValidationServiceType service) {
		this.service = service;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

}
