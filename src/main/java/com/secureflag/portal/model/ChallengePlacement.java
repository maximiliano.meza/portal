package com.secureflag.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class ChallengePlacement extends Achievement {
	
	@SerializedName("placement")
	@Expose
	@Column(name = "placement")
	private Integer placement;
	
	@SerializedName("challengeId")
	@Expose
	@Column(name = "challengeId")
	private Integer challengeId;

	public Integer getPlacement() {
		return placement;
	}

	public void setPlacement(Integer placement) {
		this.placement = placement;
	}

	public Integer getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(Integer challengeId) {
		this.challengeId = challengeId;
	}

}
