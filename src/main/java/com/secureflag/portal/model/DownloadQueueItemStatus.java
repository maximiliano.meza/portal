package com.secureflag.portal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum DownloadQueueItemStatus {

	@SerializedName("0")
	RECEIVED(0),
	@SerializedName("1")
	STARTED(1),
	@SerializedName("2")
	PULL_STARTED(2),
	@SerializedName("3")
	PULL_COMPLETE(3),
	@SerializedName("4")
	REPO_CREATED(4),
	@SerializedName("5")
	IMAGE_TAGGED(5),
	@SerializedName("6")
	PUSH_COMPLETE(6),
	@SerializedName("7")
	IMAGE_REMOVED(7),
	@SerializedName("8")
	TASK_DEFINITION_COMPLETE(8),
	@SerializedName("9")
	ORGANIZATIONS_COMPLETE(9),
	@SerializedName("10")
	COMPLETE(10);
	
	public static final DownloadQueueItemStatus DEFAULT_STATUS = RECEIVED;

	private DownloadQueueItemStatus(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("status")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

}
