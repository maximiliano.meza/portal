package com.secureflag.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Entity(name = "Achievement")
@Table(name = "achievements")
public abstract class Achievement { // NO_UCD

	@SerializedName("id")
	@Expose
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@SerializedName("type")
	@Expose
	@Column(name = "type")
	private AchievementType type;
	
	@SerializedName("name")
	@Expose
	@Column(name = "name")
	private String name;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AchievementType getType() {
		return type;
	}

	public void setType(AchievementType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
