package com.secureflag.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity( name = "MarkdownText")
@Table( name = "MarkdownTexts" )
public class MarkdownText implements Serializable{

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idMarkdown")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
	@Expose
	private Integer id;

	@Column(name = "text", columnDefinition = "LONGTEXT")	
	@SerializedName("text")
	@Expose
	private String text;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
