/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model.dto;

import java.util.List;

import com.google.gson.JsonObject;

public class ValidatedData {

	private JsonObject json;
	private boolean withErrors;
	private List<String> errors;

	public ValidatedData(JsonObject json, boolean withErrors, List<String> errors) {
		this.json = json;
		this.withErrors = withErrors;
		this.errors = errors;
	}
	public JsonObject getJson() {
		return json;
	}
	public void setJson(JsonObject json) {
		this.json = json;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	public void addError(String error) {
		this.errors.add(error);
	}
	public boolean isWithErrors() {
		return withErrors;
	}
	public void setWithErrors(boolean withErrors) {
		this.withErrors = withErrors;
	}
}
