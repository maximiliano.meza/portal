/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.model.UserAchievement;


public class UserAchievements {

    @SerializedName("score")
    @Expose
    private Integer score;
    
    @SerializedName("exercisesRun")
    @Expose
    private Integer exercisesRun;
    
    @SerializedName("achievements")
    @Expose
    private List<UserAchievement> achievements = new ArrayList<UserAchievement>();

    /**
     * 
     * @return
     *     The score
     */
    public Integer getScore() {
        return score;
    }

    /**
     * 
     * @param score
     *     The score
     */
    public void setScore(Integer score) {
        this.score = score;
    }

    /**
     * 
     * @return
     *     The achievements
     */
    public List<UserAchievement> getAchievements() {
        return achievements;
    }

    /**
     * 
     * @param achievements
     *     The achievements
     */
    public void setAchievements(List<UserAchievement> achievements) {
        this.achievements = achievements;
    }

	public Integer getExercisesRun() {
		return exercisesRun;
	}

	public void setExercisesRun(Integer exercisesRun) {
		this.exercisesRun = exercisesRun;
	}


}
