package com.secureflag.portal.model.dto;

import java.util.LinkedList;
import java.util.List;

import com.secureflag.portal.model.LearningPath;

public class LearningPathInstallationResponse {
	
	private LearningPath path;
	private List<String> exercises;
	
	public LearningPathInstallationResponse() {
		this.exercises = new LinkedList<String>();
	}
	
	public LearningPath getPath() {
		return path;
	}
	public void setPath(LearningPath path) {
		this.path = path;
	}
	public List<String> getExercises() {
		return exercises;
	}
	public void setExercises(List<String> exercises) {
		this.exercises = exercises;
	}

}
