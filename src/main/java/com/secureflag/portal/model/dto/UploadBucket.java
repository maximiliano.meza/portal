package com.secureflag.portal.model.dto;

public class UploadBucket {

	private String arn;
	
	public String getArn() {
		return arn;
	}

	public void setArn(String arn) {
		this.arn = arn;
	}
	
}
