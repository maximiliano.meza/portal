/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model.dto;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.model.ExerciseInstanceStatus;

public class CompletedReview extends PendingReview {

	@Expose
	@SerializedName("score")
	private Integer score;
	@Expose
	@SerializedName("trophyAwarded")
	private Boolean trophyAwarded;
	@Expose
	@SerializedName("newIssuesIntroduced")
	private Boolean newIssueIntroduced;
	@Expose
	@SerializedName("status")
	private ExerciseInstanceStatus status;
	@Expose
	@SerializedName("feedback")	
	private Boolean feedback;
	@Expose
	@SerializedName("hasCrashed")	
	private Boolean hasCrashed;
	@Expose
	@SerializedName("dateReviewed")	
	private Date date;
	@Expose
	@SerializedName("issuesReported")	
	private Boolean issuesReported;
	@Expose
	@SerializedName("issuesReportedAddressed")	
	private Boolean issuesReportedAddressed;

	@Expose
	@SerializedName("nrResRefresh")
	private Integer nrResRefresh;

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Boolean getTrophyAwarded() {
		return trophyAwarded;
	}

	public void setTrophyAwarded(Boolean trophyAwarded) {
		this.trophyAwarded = trophyAwarded;
	}

	public Boolean getNewIssueIntroduced() {
		return newIssueIntroduced;
	}

	public void setNewIssueIntroduced(Boolean newIssueIntroduce) {
		this.newIssueIntroduced = newIssueIntroduce;
	}

	public ExerciseInstanceStatus getStatus() {
		return this.status;
	}


	public void setStatus(ExerciseInstanceStatus status) {
		this.status = status;
	}

	public void setFeedback(Boolean feedback) {
		this.feedback = feedback;

	}
	public Boolean getFeedback() {
		return this.feedback;

	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getReportedScoringIssues() {
		return this.issuesReported;		
	}

	public void setReportedScoringIssues(boolean issuesReported) {
		this.issuesReported = issuesReported;

	}

	public Boolean getIssuesReported() {
		return issuesReported;
	}

	public void setIssuesReported(Boolean issuesReported) {
		this.issuesReported = issuesReported;
	}

	public Integer getNrResRefresh() {
		return nrResRefresh;
	}

	public void setNrResRefresh(Integer nrResRefresh) {
		this.nrResRefresh = nrResRefresh;
	}

	public Boolean getIssuesReportedAddressed() {
		return issuesReportedAddressed;
	}

	public void setIssuesReportedAddressed(Boolean issuesReportedAddressed) {
		this.issuesReportedAddressed = issuesReportedAddressed;
	}

	public Boolean getHasCrashed() {
		return hasCrashed;
	}

	public void setHasCrashed(Boolean hasCrashed) {
		this.hasCrashed = hasCrashed;
	}

}
