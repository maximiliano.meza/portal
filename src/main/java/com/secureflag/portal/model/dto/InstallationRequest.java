package com.secureflag.portal.model.dto;

import com.secureflag.portal.model.DownloadQueueItemType;

public class InstallationRequest {
	
	private String uuid;
	private DownloadQueueItemType type;
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public DownloadQueueItemType getType() {
		return type;
	}
	public void setType(DownloadQueueItemType type) {
		this.type = type;
	}

}
