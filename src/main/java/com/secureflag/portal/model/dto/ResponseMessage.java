package com.secureflag.portal.model.dto;

public class ResponseMessage {
	
	private String type;
	private String value;
	
	public ResponseMessage() {}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
