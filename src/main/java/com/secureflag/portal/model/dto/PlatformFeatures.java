package com.secureflag.portal.model.dto;

import com.google.gson.annotations.Expose;

public class PlatformFeatures{

	@Expose
	private Boolean emailModule;
	@Expose
	private Boolean imageModule;
	@Expose
	private Boolean androidModule;
	@Expose
	private Boolean authModule;
	@Expose
	private Boolean cacheModule;

	public Boolean getEmailModule() {
		return emailModule;
	}

	public void setEmailModule(Boolean emailModule) {
		this.emailModule = emailModule;
	}

	public Boolean getImageModule() {
		return imageModule;
	}

	public void setImageModule(Boolean imageModule) {
		this.imageModule = imageModule;
	}

	public Boolean getAndroidModule() {
		return androidModule;
	}

	public void setAndroidModule(Boolean androidModule) {
		this.androidModule = androidModule;
	}

	public Boolean getAuthModule() {
		return authModule;
	}

	public void setAuthModule(Boolean authModule) {
		this.authModule = authModule;
	}

	public Boolean getCacheModule() {
		return cacheModule;
	}

	public void setCacheModule(Boolean cacheModule) {
		this.cacheModule = cacheModule;
	}

}
