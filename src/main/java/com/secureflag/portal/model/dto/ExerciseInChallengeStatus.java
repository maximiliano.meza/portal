package com.secureflag.portal.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExerciseInChallengeStatus {
	
	@Expose
	@SerializedName("challengeId")
	private Integer challengeId;
	@Expose
	@SerializedName("exerciseUuid")
	private String exerciseUuid;
	@Expose
	@SerializedName("exerciseInstanceId")
	private Integer exerciseInstanceId;
	@Expose
	@SerializedName("exerciseInChallenge")
	private Boolean exerciseInChallenge;
	@Expose
	@SerializedName("exerciseAlreadyRun")
	private Boolean exerciseAlreadyRun;
	@Expose
	@SerializedName("exerciseAlreadySolved")
	private Boolean exerciseAlreadySolved;
	
	
	public Boolean getExerciseInChallenge() {
		return exerciseInChallenge;
	}
	public void setExerciseInChallenge(Boolean exerciseInChallenge) {
		this.exerciseInChallenge = exerciseInChallenge;
	}
	public Boolean getExerciseAlreadyRun() {
		return exerciseAlreadyRun;
	}
	public void setExerciseAlreadyRun(Boolean exerciseAlreadyRun) {
		this.exerciseAlreadyRun = exerciseAlreadyRun;
	}
	public Boolean getExerciseAlreadySolved() {
		return exerciseAlreadySolved;
	}
	public void setExerciseAlreadySolved(Boolean exerciseAlreadySolved) {
		this.exerciseAlreadySolved = exerciseAlreadySolved;
	}
	public Integer getChallengeId() {
		return challengeId;
	}
	public void setChallengeId(Integer challengeId) {
		this.challengeId = challengeId;
	}
	public String getExerciseUuid() {
		return exerciseUuid;
	}
	public void setExerciseUuid(String exerciseUuid) {
		this.exerciseUuid = exerciseUuid;
	}
	public Integer getExerciseInstanceId() {
		return exerciseInstanceId;
	}
	public void setExerciseInstanceId(Integer exerciseInstanceId) {
		this.exerciseInstanceId = exerciseInstanceId;
	}


}
