/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model.dto;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.model.Organization;

public class PendingReview {

	@Expose
    @SerializedName("id")
	private Integer id;
	@Expose
    @SerializedName("exerciseName")
	private String exerciseName;
	@Expose
    @SerializedName("exerciseTopic")
	private String exerciseTopic;
	@Expose
    @SerializedName("technology")
	private String technology;
	@Expose
    @SerializedName("startTime")
	private Date startTime;
	@Expose
    @SerializedName("endTime")
	private Date endTime;
	@Expose
    @SerializedName("user")
	private String user;
	@Expose
    @SerializedName("duration")
	private Integer duration;
	@Expose
    @SerializedName("organization")
	private Organization organization;
	@Expose
    @SerializedName("challengeId")
	private Integer challengeId;
	@Expose
	@SerializedName("nrSelfRefresh")
	private Integer nrSelfRefresh;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getExerciseName() {
		return exerciseName;
	}
	public void setExerciseName(String exerciseName) {
		this.exerciseName = exerciseName;
	}
	public String getExerciseTopic() {
		return exerciseTopic;
	}
	public void setExerciseTopic(String exerciseTopic) {
		this.exerciseTopic = exerciseTopic;
	}
	public String getTechnology() {
		return technology;
	}
	public void setTechnology(String technology) {
		this.technology = technology;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	public Integer getChallengeId() {
		return challengeId;
	}
	public void setChallengeId(Integer challengeId) {
		this.challengeId = challengeId;
	}
	public Integer getNrSelfRefresh() {
		return nrSelfRefresh;
	}
	public void setNrSelfRefresh(Integer nrSelfRefresh) {
		this.nrSelfRefresh = nrSelfRefresh;
	}
	
}
