/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model.dto;

import javax.persistence.Lob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenConnectionMessage {

	@SerializedName("exInstanceId")
	@Expose
	private Integer exInstanceId;
	@SerializedName("fqdn")
	@Expose
	private String fqdn;
	@SerializedName("token")
	@Expose
	private String token;
	@SerializedName("node")
	@Lob
	@Expose
	private String node;
	@SerializedName("user")
	@Expose
	private String user;
	@SerializedName("countdown")
	@Expose
	private Integer countdown;
	@SerializedName("connectionId")
	@Expose
	private String connectionId;
	@SerializedName("preload")
	@Expose
	private Boolean preload;
	@SerializedName("ec2Ip")
	@Expose
	private String ec2Ip;
	@SerializedName("ec2Instance")
	@Expose
	private String ec2Instance;
	@SerializedName("ec2Preload")
	@Expose
	private Integer ec2Preload;

	public TokenConnectionMessage(Integer idExerciseInstance, String fqdn, String username, String lastValidToken, String node,
			String connectionId, Integer countdown, Boolean preload, String ec2Ip, String ec2Instance, Integer ec2Preload) {
		this.exInstanceId=idExerciseInstance;
		this.fqdn=fqdn;
		this.user=username;
		this.token=lastValidToken;
		this.countdown=countdown;
		this.connectionId = connectionId;
		this.preload=preload;
		this.node = node;
		this.ec2Ip = ec2Ip;
		this.ec2Instance=ec2Instance;
		this.ec2Preload = ec2Preload;
	}
	public Integer getExInstanceId() {
		return exInstanceId;
	}
	public void setExInstanceId(Integer exInstanceId) {
		this.exInstanceId = exInstanceId;
	}
	public String getFqdn() {
		return fqdn;
	}
	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Integer getCountdown() {
		return countdown;
	}
	public void setCountdown(Integer countdown) {
		this.countdown = countdown;
	}
	public Boolean getPreload() {
		return preload;
	}
	public void setPreload(Boolean preload) {
		this.preload = preload;
	}
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}
	public String getEc2Ip() {
		return ec2Ip;
	}
	public void setEc2Ip(String ec2Ip) {
		this.ec2Ip = ec2Ip;
	}
	public String getEc2Instance() {
		return ec2Instance;
	}
	public void setEc2Instance(String ec2Instance) {
		this.ec2Instance = ec2Instance;
	}
	public Integer getEc2Preload() {
		return ec2Preload;
	}
	public void setEc2Preload(Integer ec2Preload) {
		this.ec2Preload = ec2Preload;
	}
	public String getConnectionId() {
		return connectionId;
	}
	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

}
