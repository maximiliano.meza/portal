/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExerciseInstanceSelfCheckResult {

	@SerializedName("selfcheck")
	@Expose
	private List<SelfCheckResult> flagsStatus = new ArrayList<SelfCheckResult>();

	/**
	 * 
	 * @return
	 *     The flagList
	 */
	public List<SelfCheckResult> getFlagList() {
		return flagsStatus;
	}

	/**
	 * 
	 * @param flagList
	 *     The flagList
	 */
	public void setFlagList(List<SelfCheckResult> flagList) {
		this.flagsStatus = flagList;
	}


}
