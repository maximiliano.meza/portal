/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model.dto;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.model.ExerciseInstanceResult;

public class ManualReviewObject {

	@SerializedName("review")
	@Expose
	private List<ExerciseInstanceResult> review;
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("totalScore")
	@Expose
	private Integer totalScore;
	@SerializedName("awardTrophy")
	@Expose
	private Boolean awardTrophy;
	@SerializedName("newIssuesIntroduced")
	@Expose
	private Boolean newIssuesIntroduced;
	@SerializedName("newIssuesIntroducedText")
	@Expose
	private String newIssuesIntroducedText;
	
	
	public List<ExerciseInstanceResult> getReview() {
		return review;
	}
	public void setReview(List<ExerciseInstanceResult> review) {
		this.review = review;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}
	public Boolean getAwardTrophy() {
		return awardTrophy;
	}
	public void setAwardTrophy(Boolean awardTrophy) {
		this.awardTrophy = awardTrophy;
	}
	public Boolean getNewIssuesIntroduced() {
		return newIssuesIntroduced;
	}
	public void setNewIssuesIntroduced(Boolean newIssuesIntroduced) {
		this.newIssuesIntroduced = newIssuesIntroduced;
	}
	public String getNewIssuesIntroducedText() {
		return newIssuesIntroducedText;
	}
	public void setNewIssuesIntroducedText(String newIssuesIntroducedText) {
		this.newIssuesIntroducedText = newIssuesIntroducedText;
	}

}
