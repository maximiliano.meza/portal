/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table( name="guacTempUsers")
@Entity( name="GuacTempUser" )
public class GatewayTempUser implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idGuacTempUser")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idGuacTempUser;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "gatewayId")
	private Gateway gateway;

	@Column(name="lastValidToken")
	private String lastValidToken;
	
	@Column(name = "node", columnDefinition = "LONGTEXT")	
	private String node;

	@Column(name="connectionId")
	private String connectionId;

	public Integer getIdGuacTempUser() {
		return idGuacTempUser;
	}
	public void setIdGuacTempUser(Integer idGuacTempUser) {
		this.idGuacTempUser = idGuacTempUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Gateway getGateway() {
		return gateway;
	}
	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}
	public String getLastValidToken() {
		return lastValidToken;
	}
	public void setLastValidToken(String lastValidToken) {
		this.lastValidToken = lastValidToken;
	}
	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
	public String getConnectionId() {
		return connectionId;
	}
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}

}