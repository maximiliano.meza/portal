/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum AvailableExerciseType implements Serializable {
	
	TRAINING(0),
	CHALLENGE(1),
	BOTH(2),
	;

	public static final AvailableExerciseType DEFAULT_STATUS = BOTH;

	private AvailableExerciseType(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("name")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public static AvailableExerciseType getStatusFromName(String name){

		for(AvailableExerciseType t : AvailableExerciseType.values()) {
			if(t.toString().equals(name)) {
				return t;
			}
		}
		return DEFAULT_STATUS;
	}

}
