package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity( name = "FlagQuestionSelfcheck")
@Table( name = "flagQuestionSelfchecks" )
public class FlagQuestionSelfcheck implements Serializable{

	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idFlagQuestionSelfcheck")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
    @Expose
    private Integer id;
   
	@Column(name = "name")
    @Expose
	private String name;
	
	@ElementCollection(fetch=FetchType.EAGER)
	@Cascade({CascadeType.ALL})
	@Expose
	private Map<String, ExerciseInstanceResultStatus> statusMapping = new HashMap<String, ExerciseInstanceResultStatus>();
	
	@ElementCollection(fetch=FetchType.EAGER)
	@Cascade({CascadeType.ALL})
    @Expose
    @Lob
	private Map<String, String> messageMappings = new HashMap<String, String>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, ExerciseInstanceResultStatus> getStatusMapping() {
		return statusMapping;
	}
	public void setStatusMapping(Map<String, ExerciseInstanceResultStatus> statusMapping) {
		this.statusMapping = statusMapping;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Map<String, String> getMessageMappings() {
		return messageMappings;
	}

	public void setMessageMappings(Map<String, String> messageMappings) {
		this.messageMappings = messageMappings;
	}

}
