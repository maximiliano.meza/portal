package com.secureflag.portal.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(name = "DownloadQueueItem")
public class DownloadQueueItem {
	
	@SerializedName("id")
	@Expose
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@SerializedName("exercise")
	@Expose
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade({CascadeType.SAVE_UPDATE})
	@JoinColumn(name = "exerciseId")
	private AvailableExercise exercise;
	
	@Column(name="pathUuid")
	@Expose
	private String pathUuid;
	
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<String> regions;
	
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Integer> organizations;
	
	@Column(name="date")
	@Expose
	private Date date;
	
	@Column(name="lastUpdate")
	@Expose
	private Date lastUpdate;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status")
	@Expose
	private DownloadQueueItemStatus status;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "type")
	@Expose
	private DownloadQueueItemType type;

	public AvailableExercise getExercise() {
		return exercise;
	}

	public void setExercise(AvailableExercise exercise) {
		this.exercise = exercise;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public DownloadQueueItemStatus getStatus() {
		return status;
	}

	public void setStatus(DownloadQueueItemStatus status) {
		this.status = status;
	}

	public Set<String> getRegions() {
		return regions;
	}

	public void setRegions(Set<String> regions) {
		this.regions = regions;
	}

	public Set<Integer> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(Set<Integer> organizations) {
		this.organizations = organizations;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public DownloadQueueItemType getType() {
		return type;
	}

	public void setType(DownloadQueueItemType type) {
		this.type = type;
	}

	public String getPathUuid() {
		return pathUuid;
	}

	public void setPathUuid(String pathUuid) {
		this.pathUuid = pathUuid;
	}
	
	

}
