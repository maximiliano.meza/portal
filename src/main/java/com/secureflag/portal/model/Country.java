/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.LeaderboardUser;

@Entity
@Table( name = "countries" )
public class Country implements Serializable {  

	private static final long serialVersionUID = 1L;

	@SerializedName("id")
    @Expose
    @LeaderboardUser
    @Id
    @Column(name = "idCountry")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    
    @SerializedName("name")
    @Expose
    @LeaderboardUser
    @Column(name = "name", unique = true)
    private String name;
    
    @SerializedName("short")
    @Expose
    @LeaderboardUser
    @Column(name = "short", unique = true)
    private String shortName;
   

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 
     * @return
     *     The name
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }


}
