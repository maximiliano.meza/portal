package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;

@Entity( name = "VotingScore" )
@Table( name = "votingScores" )
public class ExerciseInstanceVotingScore implements Serializable{

	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idVotingScore")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
	@Expose
	private Integer id;
	
	@Column(name = "score")
	@Expose
	@SerializedName("score")
	private Integer score;
	
	@Column(name = "date")
	@Expose
	@SerializedName("date")
	private Date date;
	
	@Expose
	@LazilySerialized
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "exerciseInstanceId")
	private ExerciseInstance instance;

	
	@Expose
	@SerializedName("user")
	@LazilySerialized
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;
	
	@Column(name = "hasCompletedExercise")
	@Expose
	@SerializedName("hasCompletedExercise")
	private Boolean hasCompletedExercise;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Boolean getHasCompletedExercise() {
		return hasCompletedExercise;
	}
	public void setHasCompletedExercise(Boolean hasCompletedExercise) {
		this.hasCompletedExercise = hasCompletedExercise;
	}
	public ExerciseInstance getInstance() {
		return instance;
	}
	public void setInstance(ExerciseInstance instance) {
		this.instance = instance;
	}

}
