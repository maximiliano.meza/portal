/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity( name = "ExerciseScore")
@Table( name = "exerciseScores" )
public class ExerciseInstanceScore implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idExerciseScore")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
	private Integer idExerciseScore;

	@SerializedName("total")
    @Expose
    @Column(name="total")
    private Integer total;

	@SerializedName("result")
    @Expose
    @Column(name="result")
    private Integer result;

	/**
     * 
     * @return
     *     The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 
     * @return
     *     The result
     */
    public Integer getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(Integer result) {
        this.result = result;
    }

	public Integer getIdExerciseScore() {
		return idExerciseScore;
	}

	public void setIdExerciseScore(Integer idExerciseScore) {
		this.idExerciseScore = idExerciseScore;
	}

}
