package com.secureflag.portal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum ECSTaskPlacementStrategy {

	@SerializedName("0")
	BINPACK(0),
	@SerializedName("1")
	SPREAD(1),
	;

	public static final ECSTaskPlacementStrategy DEFAULT_STRATEGY = BINPACK;

	private ECSTaskPlacementStrategy(Integer statusCode){
		this.strategyCode = statusCode;
	}

	@SerializedName("strategy")
	@Expose
	private Integer strategyCode;

	public Integer getName() {
		return strategyCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.strategyCode = statusCode;
	}

	public static ECSTaskPlacementStrategy getStrategyFromCode(Integer statusCode){
		for (ECSTaskPlacementStrategy status :ECSTaskPlacementStrategy.values()) {
			if (statusCode==status.ordinal()) {
				return status;
			}
		}
		return DEFAULT_STRATEGY;
	}

}
