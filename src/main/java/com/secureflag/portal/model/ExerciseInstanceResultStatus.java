/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum ExerciseInstanceResultStatus {

	@SerializedName("0")
	NOT_VULNERABLE(0),
	@SerializedName("1")
	VULNERABLE(1),
	@SerializedName("2")
	BROKEN_FUNCTIONALITY(2),
	@SerializedName("3")
	NOT_AVAILABLE(3),
	@SerializedName("4")
	NOT_ADDRESSED(4),
	@SerializedName("5")
	PARTIALLY_REMEDIATED(5),
	@SerializedName("6")
	EXPLOITED(6),
	@SerializedName("7")
	NOT_EXPLOITED(7);

	public static final ExerciseInstanceResultStatus DEFAULT_STATUS = NOT_AVAILABLE;

	private ExerciseInstanceResultStatus(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("status")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public static ExerciseInstanceResultStatus getStatusFromStatusString(String statusCode){
		for (ExerciseInstanceResultStatus status :ExerciseInstanceResultStatus.values()) {
			if (statusCode.equals(status.name().toString())) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}

	public static ExerciseInstanceResultStatus getStatusFromStatusCode(Integer statusCode){
		for (ExerciseInstanceResultStatus status :ExerciseInstanceResultStatus.values()) {
			if (statusCode==status.getName()) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}

}