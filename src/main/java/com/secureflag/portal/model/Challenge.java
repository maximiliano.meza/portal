/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.ChallengeDetails;
import com.secureflag.portal.messages.json.annotations.ChallengeListExcludedData;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;

@Table(name="challenges")
@Entity(name="Challenge")
public class Challenge implements Serializable {

	private static final long serialVersionUID = 1L;

	@SerializedName("id")
	@Id
	@Expose
	@Column(name = "idChallenge")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idChallenge;
	
	@SerializedName("name")
	@Expose
	@Column(name = "name")
	private String name;
	
	@SerializedName("createdBy")
	@LazilySerialized
	@Column(name = "createdBy")
	private Integer createdBy;
	
	@SerializedName("completion")
	@Expose
	@Column(name = "completion")
	private Double completion;
	
	@SerializedName("lastActivity")
	@Expose
	@Column(name = "lastActivity")
	private Date lastActivity;

	@Enumerated(EnumType.ORDINAL)
	@Expose
	@Column(name="status")
	private ChallengeStatus status;
	
	@SerializedName("users")
	@Expose
	@ChallengeDetails
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<User> users;
	
	@SerializedName("details")
	@Expose
	@Lob
	private String details;
	
	@SerializedName("scoring")
	@Expose
	@ChallengeListExcludedData
	@Enumerated(EnumType.ORDINAL)
	private AvailableExerciseExerciseScoringMode scoring;
	
	@SerializedName("scopeExercises")
	@Expose
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<String> scopeExercises;

	@SerializedName("runExercises")
	@Expose
	@ChallengeListExcludedData
	@ChallengeDetails
	@LazilySerialized
	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<ExerciseInstance> runExercises;

	@SerializedName("organization")
	@Expose
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	@JoinColumn(name = "organizationId" )
	private Organization organization;
	
	@Column(name="startDate")
	@Expose
	private Date startDate;

	@Column(name="endDate")
	@Expose
	private Date endDate;
	
	@Column(name="firstInFlag",columnDefinition="int default 0")
	@Expose
	@ChallengeListExcludedData
	private Integer firstInFlag;
	
	@Column(name="secondInFlag",columnDefinition="int default 0")
	@Expose
	@ChallengeListExcludedData
	private Integer secondInFlag;
	
	@Column(name="thirdInFlag",columnDefinition="int default 0")
	@Expose
	@ChallengeListExcludedData
	private Integer thirdInFlag;
	
	@Expose
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@SerializedName("winners")
	@ChallengeListExcludedData
	private List<ChallengeWinner> challengeWinners = new LinkedList<ChallengeWinner>();
	
	@Transient
	@Expose
	private Set<AvailableExercise> exerciseData;

	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public List<ExerciseInstance> getRunExercises() {
		return runExercises;
	}
	public void setRunExercises(List<ExerciseInstance> runExercises) {
		this.runExercises = runExercises;
	}

	public Integer getIdChallenge() {
		return idChallenge;
	}
	public void setIdChallenge(Integer idChallenge) {
		this.idChallenge = idChallenge;
	}

	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getCompletion() {
		return completion;
	}
	public void setCompletion(Double completion) {
		this.completion = completion;
	}
	public ChallengeStatus getStatus() {
		return status;
	}
	public void setStatus(ChallengeStatus status) {
		this.status = status;
	}
	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Date getLastActivity() {
		return lastActivity;
	}
	public void setLastActivity(Date lastActivity) {
		this.lastActivity = lastActivity;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public AvailableExerciseExerciseScoringMode getScoring() {
		return scoring;
	}
	public void setScoring(AvailableExerciseExerciseScoringMode scoring) {
		this.scoring = scoring;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idChallenge == null) ? 0 : idChallenge.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Challenge other = (Challenge) obj;
		if (idChallenge == null) {
			if (other.idChallenge != null)
				return false;
		} else if (!idChallenge.equals(other.idChallenge))
			return false;
		return true;
	}
	public Integer getFirstInFlag() {
		return firstInFlag;
	}
	public void setFirstInFlag(Integer firstInFlag) {
		this.firstInFlag = firstInFlag;
	}
	public Integer getSecondInFlag() {
		return secondInFlag;
	}
	public void setSecondInFlag(Integer secondInFlag) {
		this.secondInFlag = secondInFlag;
	}
	public Integer getThirdInFlag() {
		return thirdInFlag;
	}
	public void setThirdInFlag(Integer thirdInFlag) {
		this.thirdInFlag = thirdInFlag;
	}
	public Set<String> getScopeExercises() {
		return scopeExercises;
	}
	public void setScopeExercises(Set<String> scopeExercises) {
		this.scopeExercises = scopeExercises;
	}
	public Set<AvailableExercise> getExerciseData() {
		return exerciseData;
	}
	public void setExerciseData(Set<AvailableExercise> exerciseData) {
		this.exerciseData = exerciseData;
	}
	public List<ChallengeWinner> getWinners() {
		return challengeWinners;
	}
	public void setWinners(List<ChallengeWinner> winners) {
		this.challengeWinners = winners;
	}
}
