package com.secureflag.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class ChallengeWinner implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name = "score")
	@Expose
	@SerializedName("score")
	private Integer score;
	
	@Column(name = "idUser")
	@Expose
	@SerializedName("idUser")
	private Integer idUser;
	
	@Id
	@Column(name = "idWinner")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	public ChallengeWinner() {}
	
	public ChallengeWinner(Integer idUser,Integer score) {
		this.score= score;
		this.idUser=idUser;
	}
	
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

}
