/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.amazonaws.regions.Regions;

@Entity(name = "Gateway")
@Table( name = "Gateways" )
public class Gateway implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idGateway")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
    @Enumerated(EnumType.STRING)
	@Column(name = "region")
	private Regions region;
	
	@Column(name = "fqdn")
	private String fqdn;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "status")
    @Enumerated(EnumType.STRING)
	private GatewayStatus status;
	
	@Column(name = "enableImageSync",columnDefinition="BIT DEFAULT 1")
	private Boolean enableImageSync;

	@Column(name = "enableScaleIn",columnDefinition="BIT DEFAULT 1")
	private Boolean enableScaleIn;
	
	@Column(name = "placementStrategy",columnDefinition="INT DEFAULT 0")
	private ECSTaskPlacementStrategy placementStrategy;
	
	public Boolean isActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFqdn() {
		return fqdn;
	}
	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}
	public Regions getRegion() {
		return region;
	}
	public void setRegion(Regions region) {
		this.region = region;
	}

	public Boolean getActive() {
		return active;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gateway other = (Gateway) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Boolean getEnableImageSync() {
		return enableImageSync;
	}
	public void setEnableImageSync(Boolean enableImageSync) {
		this.enableImageSync = enableImageSync;
	}
	public Boolean getEnableScaleIn() {
		return enableScaleIn;
	}
	public void setEnableScaleIn(Boolean enableScaleIn) {
		this.enableScaleIn = enableScaleIn;
	}
	public ECSTaskPlacementStrategy getPlacementStrategy() {
		return placementStrategy;
	}
	public void setPlacementStrategy(ECSTaskPlacementStrategy placementStrategy) {
		this.placementStrategy = placementStrategy;
	}
	public GatewayStatus getStatus() {
		return status;
	}
	public void setStatus(GatewayStatus status) {
		this.status = status;
	}
}
