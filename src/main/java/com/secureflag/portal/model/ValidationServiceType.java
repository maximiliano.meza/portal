package com.secureflag.portal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum ValidationServiceType {

	@SerializedName("0")
	EMAIL(0),
	@SerializedName("1")
	PASSWORD_RESET(1),
	;

	public static final ValidationServiceType DEFAULT_SERVICE = EMAIL;

	private ValidationServiceType(Integer code){
		this.service = code;
	}

	@SerializedName("service")
	@Expose
	private Integer service;

	public Integer getService() {
		return service;
	}
	public void setService(Integer code) {
		this.service = code;
	}

}
