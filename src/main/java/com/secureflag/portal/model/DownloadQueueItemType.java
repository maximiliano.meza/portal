package com.secureflag.portal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum DownloadQueueItemType {

	@SerializedName("INSTALLATION")
	INSTALLATION(0),
	@SerializedName("UPDATE")
	UPDATE(1),
	@SerializedName("PATH_INSTALLATION")
	PATH_INSTALLATION(2),
	@SerializedName("PATH_UPDATE")
	PATH_UPDATE(3);
	
	public static final DownloadQueueItemType DEFAULT_STATUS = INSTALLATION;

	private DownloadQueueItemType(Integer type){
		this.type = type;
	}

	@SerializedName("type")
	@Expose
	private Integer type;

	public Integer getName() {
		return type;
	}
	public void setStatusCode(Integer statusCode) {
		this.type = statusCode;
	}

}
