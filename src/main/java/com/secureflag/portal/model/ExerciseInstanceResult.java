/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity( name = "ExerciseResult")
@Table( name = "exerciseResults" )
public class ExerciseInstanceResult implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idResult")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
	@Expose
	private Integer idResult;
	
	@SerializedName("flagTitle")
	@Expose
	@Column(name = "flagTitle")
	private String flagTitle;

	@SerializedName("name")
	@Expose
	@Column(name = "name")
	private String name;

	@SerializedName("status")
	@Expose
	@Enumerated(EnumType.ORDINAL)
	@Column(name="status")
	private ExerciseInstanceResultStatus status;
	
	@SerializedName("checkerStatus")
	@Expose
	@Column(name="checkerStatus")
	private String checkerStatus;

	@SerializedName("category")
	@Expose
	@Column(name="category")
	private String category;
	
	@SerializedName("type")
	@Expose
	@Column(name="type")
	private String type;

	@SerializedName("verified")
	@Expose
	@Column(name = "verified")
	private Boolean verified;
	
	@SerializedName("automated")
	@Expose
	@Column(name = "automated")
	private Boolean automated;
	
	@SerializedName("previouslySolved")
	@Expose
	@Column(name = "previouslySolved")
	private Boolean previouslySolved;
	
	@SerializedName("hint")
	@Expose
	@Column(name = "hint")
	private Boolean hint;

	@SerializedName("comment")
	@Expose
	@Column(name = "comment", columnDefinition = "LONGTEXT")
	private String comment;

	@SerializedName("score")
	@Expose
	@Column(name = "score")
	private Integer score;
	
	@SerializedName("lastChange")
	@Expose
	@Column(name = "lastChange")
	private Date lastChange;
	
	@SerializedName("userReportedIssues")
	@Expose
	@Column(name = "userReportedIssues")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<UserResultComment> userReportedComplaints;
	
	@SerializedName("userReportedIssueAddressed")
	@Expose
	@Column(name = "userReportedIssueAddressed",columnDefinition="BIT DEFAULT 0")
	private Boolean userReportedIssueAddressed;

	@SerializedName("firstForFlag")
	@Expose
	@Column(name = "firstForFlag",columnDefinition="BIT DEFAULT 0")
	private Boolean firstForFlag;

	@SerializedName("secondForFlag")
	@Expose
	@Column(name = "secondForFlag",columnDefinition="BIT DEFAULT 0")
	private Boolean secondForFlag;

	@SerializedName("thirdForFlag")
	@Expose
	@Column(name = "thirdForFlag",columnDefinition="BIT DEFAULT 0")
	private Boolean thirdForFlag;
	
	public ExerciseInstanceResult() {
		this.hint = false;
		this.firstForFlag = false;
		this.secondForFlag = false;
		this.thirdForFlag = false;
		this.verified = false;
		this.automated = false;
		this.previouslySolved = false;
		this.userReportedIssueAddressed = false;
	}

	public Integer getIdResult() {
		return idResult;
	}

	public void setIdResult(Integer idResult) {
		this.idResult = idResult;
	}


	/**
	 * 
	 * @return
	 *     The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *     The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 *     The status
	 */
	public ExerciseInstanceResultStatus getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 *     The status
	 */
	public void setStatus(ExerciseInstanceResultStatus status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 *     The comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * 
	 * @param comment
	 *     The comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Boolean getAutomated() {
		return automated;
	}

	public void setAutomated(Boolean automated) {
		this.automated = automated;
	}

	public Date getLastChange() {
		return lastChange;
	}

	public void setLastChange(Date lastChange) {
		this.lastChange = lastChange;
	}

	public Boolean getHint() {
		return hint;
	}

	public void setHint(Boolean hint) {
		this.hint = hint;
	}

	public List<UserResultComment> getUserReportedComplaints() {
		return userReportedComplaints;
	}

	public void setUserReportedComplaints(List<UserResultComment> userReportedComplaints) {
		this.userReportedComplaints = userReportedComplaints;
	}

	public Boolean getUserReportedIssueAddressed() {
		return userReportedIssueAddressed;
	}

	public void setUserReportedIssueAddressed(Boolean userReportedIssueAddressed) {
		this.userReportedIssueAddressed = userReportedIssueAddressed;
	}

	public Boolean getFirstForFlag() {
		return firstForFlag;
	}

	public void setFirstForFlag(Boolean firstForFlag) {
		this.firstForFlag = firstForFlag;
	}

	public Boolean getSecondForFlag() {
		return secondForFlag;
	}

	public void setSecondForFlag(Boolean secondForFlag) {
		this.secondForFlag = secondForFlag;
	}

	public Boolean getThirdForFlag() {
		return thirdForFlag;
	}

	public void setThirdForFlag(Boolean thirdForFlag) {
		this.thirdForFlag = thirdForFlag;
	}

	public String getFlagTitle() {
		return flagTitle;
	}

	public void setFlagTitle(String flagTitle) {
		this.flagTitle = flagTitle;
	}

	public String getCheckerStatus() {
		return checkerStatus;
	}

	public void setCheckerStatus(String checkerStatus) {
		this.checkerStatus = checkerStatus;
	}

	public Boolean getPreviouslySolved() {
		return previouslySolved;
	}

	public void setPreviouslySolved(Boolean previouslySolved) {
		this.previouslySolved = previouslySolved;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


}
