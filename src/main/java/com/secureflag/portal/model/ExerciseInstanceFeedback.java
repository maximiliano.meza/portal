/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;

@Entity( name = "Feedback" )
@Table( name = "feedback" )
public class ExerciseInstanceFeedback {

	@Id
	@Column(name = "idFeedback")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idFeedback;

	@Expose
	@LazilySerialized
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;

	@Expose
	@LazilySerialized
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "exerciseInstanceId")
	private ExerciseInstance instance;

	@SerializedName("id")
	@Expose
	@Column(name="feedback", columnDefinition = "LONGTEXT")
	private String feedback;
	
	@SerializedName("date")
	@Expose
	@Column(name="date")
	private Date date;

	public Integer getIdFeedback() {
		return idFeedback;
	}
	public void setIdFeedback(Integer idFeedback) {
		this.idFeedback = idFeedback;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public ExerciseInstance getInstance() {
		return instance;
	}
	public void setInstance(ExerciseInstance instance) {
		this.instance = instance;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public Date getDate() {
		return date;		
	}
	public void setDate(Date date) {
		this.date = date;

	}
}
