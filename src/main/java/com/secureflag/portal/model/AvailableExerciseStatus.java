/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum AvailableExerciseStatus implements Serializable{

	@SerializedName("0")
	AVAILABLE(0),
	@SerializedName("1")
	DOWNLOAD_QUEUE(1),
	@SerializedName("2")
	COMING_SOON(2),
	@SerializedName("3")
	INACTIVE(3),
	@SerializedName("4")
	SUPERSEDED(4),
	@SerializedName("5")
	REMOVED(5);
	
	public static final AvailableExerciseStatus DEFAULT_STATUS = COMING_SOON;

	private AvailableExerciseStatus(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("status")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public static AvailableExerciseStatus getStatusFromStatusString(String statusCode){
		for (AvailableExerciseStatus status :AvailableExerciseStatus.values()) {
			if (statusCode.equals(status.name().toString())) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}

	public static AvailableExerciseStatus getStatusFromStatusCode(Integer statusCode){
		for (AvailableExerciseStatus status :AvailableExerciseStatus.values()) {
			if (statusCode==status.getName()) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}
}
