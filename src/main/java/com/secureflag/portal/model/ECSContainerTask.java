/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.amazonaws.regions.Regions;
import com.google.gson.annotations.Expose;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;

@Entity(name = "ECSInstance")
@Table( name = "ecsInstances" )
public class ECSContainerTask implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idInstance")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "cluster")
	private String cluster;

	@Column(name = "idContainerInstance")
	private String idContainerInstance;

	@Column(name = "taskArn")
	private String taskArn;

	@Column(name = "name")
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "region")
	private Regions region;

	@Column(name = "ipAddress")
	private String ipAddress;

	@Column(name = "rdpPort")
	private Integer rdpPort;

	@Column(name = "httpPort")
	private Integer httpPort;

	@Column(name = "createTime")
	private Date createTime;
	
	@Column(name = "shutdownTime")
	private Date actualShutdownTime;

	@Column(name = "status")
	private Integer status;

	@Expose
	@LazilySerialized
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;
	
	@Column(name = "securityGroup")
	private String securityGroup;
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Regions getRegion() {
		return region;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Regions getRegionId() {
		return region;
	}
	public void setRegion(Regions region) {
		this.region = region;
	}

	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String privateIp) {
		this.ipAddress = privateIp;
	}
	public Date getShutdownTime() {
		return actualShutdownTime;
	}
	public void setShutdownTime(Date shutdownTime) {
		this.actualShutdownTime = shutdownTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public User getUser() {
		return user;

	}
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getRdpPort() {
		return rdpPort;
	}
	public void setRdpPort(Integer rdpPort) {
		this.rdpPort = rdpPort;
	}
	public Integer getHttpPort() {
		return httpPort;
	}
	public void setHttpPort(Integer httpPort) {
		this.httpPort = httpPort;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCluster() {
		return cluster;
	}
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	public String getIdContainerInstance() {
		return idContainerInstance;
	}
	public void setIdContainerInstance(String idContainerInstance) {
		this.idContainerInstance = idContainerInstance;
	}
	public String getTaskArn() {
		return taskArn;
	}
	public void setTaskArn(String taskArn) {
		this.taskArn = taskArn;
	}
	public Date getActualShutdownTime() {
		return actualShutdownTime;
	}
	public void setActualShutdownTime(Date actualShutdownTime) {
		this.actualShutdownTime = actualShutdownTime;
	}
	public String getSecurityGroup() {
		return securityGroup;
	}
	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}

}
