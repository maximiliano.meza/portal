package com.secureflag.portal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class LearningPathCertification extends Achievement {
	
	@SerializedName("pathId")
	@Expose
	@Column(name = "pathId")
	private Integer pathId;

	@Column(name = "difficulty")
	@SerializedName("difficulty")
	@Expose
	private String difficulty;

	@Column(name = "technology")
	@SerializedName("technology")
	@Expose
	private String technology;
	
	@Column(name = "allowRenewal")
	@SerializedName("allowRenewal")
	@Expose
	private Boolean allowRenewal;

	@Column(name = "expiration")
	@SerializedName("expiration")
	@Expose
	private Date expiration;

	@Column(name = "refresherPercentage")
	@SerializedName("refresher")
	@Expose
	private Integer refresherPercentage;

	@Column(name = "recertificationProgress")
	@SerializedName("recertificationProgress")
	@Expose
	private Double recertificationProgress;

	public Integer getPathId() {
		return pathId;
	}

	public void setPathId(Integer pathId) {
		this.pathId = pathId;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public Boolean getAllowRenewal() {
		return allowRenewal;
	}

	public void setAllowRenewal(Boolean allowRenewal) {
		this.allowRenewal = allowRenewal;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public Integer getRefresherPercentage() {
		return refresherPercentage;
	}

	public void setRefresherPercentage(Integer refresherPercentage) {
		this.refresherPercentage = refresherPercentage;
	}

	public Double getRecertificationProgress() {
		return recertificationProgress;
	}

	public void setRecertificationProgress(Double recertificationProgress) {
		this.recertificationProgress = recertificationProgress;
	}
	
	

}
