/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(name = "Notification")
@Table( name = "notifications" )
public class Notification {

	@Id
	@Column(name = "idNotification")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
	@Expose
	private Integer idNotification;

	@Column(name="text", columnDefinition = "LONGTEXT")
	@SerializedName("text")
	@Expose
	private String text;
	
	@Column(name="link", columnDefinition = "LONGTEXT")
	@SerializedName("link")
	@Expose
	private String link;
	
	@Column(name = "idUser")
	private Integer idUser;

	@Column(name = "userRead")
	private Boolean userRead;

	@SerializedName("date")
	@Expose
	@Column(name = "dateStart")
	private Date dateStart;

	@Column(name = "dateEnd")
	private Date dateEnd;
	
	@Column(name = "dateRead")
	private Date dateRead;

	public Integer getId() {
		return idNotification;
	}

	public void setId(Integer id) {
		this.idNotification = id;
	}

	public String getText() {
		return text;
	}
	
	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Boolean getUserRead() {
		return userRead;
	}

	public void setUserRead(Boolean userRead) {
		this.userRead = userRead;
	}

	public Date getDateRead() {
		return dateRead;
	}

	public void setDateRead(Date dateRead) {
		this.dateRead = dateRead;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setText(String text) {
		this.text = text;
	}
}
