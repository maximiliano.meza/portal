/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.HistoryDetails;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;
import com.secureflag.portal.messages.json.annotations.NoLightDetails;

@Table( name="flagQuestionHints")
@Entity( name = "FlagQuestionHint")
public class FlagQuestionHint implements Serializable{

	@Transient
	private static final long serialVersionUID = 1L;

    @Id
	@Column(name = "idFlagQuestionHint")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
    @Expose
    private Integer id;
	
	@SerializedName("md")
	@LazilySerialized
	@Expose
	@HistoryDetails
	@Cascade({CascadeType.SAVE_UPDATE})
	@ManyToOne(fetch = FetchType.LAZY)
	@NoLightDetails
    private MarkdownText md;
	
	@SerializedName("type")
	@Expose
	@Column(name="type")
	private String type;
	
	@Column(name = "scoreReducePercentage")
	@SerializedName("scoreReducePercentage")
    @Expose
    private Integer scoreReducePercentage;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getScoreReducePercentage() {
		return scoreReducePercentage;
	}

	public void setScoreReducePercentage(Integer scoreReducePercentage) {
		this.scoreReducePercentage = scoreReducePercentage;
	}

	public MarkdownText getMd() {
		return md;
	}

	public void setMd(MarkdownText md) {
		this.md = md;
	}

}
