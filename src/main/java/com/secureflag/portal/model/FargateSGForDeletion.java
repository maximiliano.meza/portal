package com.secureflag.portal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.amazonaws.regions.Regions;

@Entity( name = "FargateSGForDeletion")
@Table( name = "fargateSGForDeletion" )
public class FargateSGForDeletion {
		
		@Id
		@Column(name = "name")
		private String name;
		
		@Column(name="region")
		private Regions region;
		
		@Column(name = "date")
		private Date date;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		public Regions getRegion() {
			return region;
		}
		public void setRegion(Regions region) {
			this.region = region;
		}

	}
