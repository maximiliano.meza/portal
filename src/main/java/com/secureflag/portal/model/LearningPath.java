package com.secureflag.portal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.ExcludedForUsers;

@Table(name="learningPaths")
@Entity(name="LearningPath")
public class LearningPath {

	@SerializedName("id")
	@Id
	@Expose
	@Column(name = "idPath")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idPath;

	@SerializedName("name")
	@Expose
	@Column(name = "name")
	private String name;

	@SerializedName("createdBy")
	@Column(name = "createdBy")
	private Integer createdBy;

	@SerializedName("lastUpdate")
	@Expose
	@Column(name = "lastUpdate")
	private Date lastUpdate;
	
	@Expose
	@Column(name = "uuid")
	private String uuid;

	@Enumerated(EnumType.ORDINAL)
	@Expose
	@Column(name="status")
	private LearningPathStatus status;

	@SerializedName("organizations")
	@Expose
	@ExcludedForUsers
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Organization> organizations;

	@SerializedName("details")
	@Expose
	@Column(name = "details")
	@Lob
	private String details;
	

	@SerializedName("description")
	@Expose
	@Cascade({CascadeType.SAVE_UPDATE})
	@OneToOne(fetch = FetchType.EAGER)
    private MarkdownText description;
	
	@Column(name = "fromHub",columnDefinition="BIT DEFAULT 0")
	@Expose
	@SerializedName("fromHub")
	private Boolean fromHub;

	@Column(name = "difficulty")
	@SerializedName("difficulty")
	@Expose
	private String difficulty;

	@Column(name = "technology")
	@SerializedName("technology")
	@Expose
	private String technology;

	@Column(name = "allowRenewal")
	@SerializedName("allowRenewal")
	@Expose
	private Boolean allowRenewal;

	@Column(name = "monthsExpiration")
	@SerializedName("monthsExpiration")
	@Expose
	private Integer monthsExpiration;

	@Column(name = "refresherPercentage")
	@SerializedName("refresherPercentage")
	@Expose
	private Integer refresherPercentage;

	@SerializedName("exercises")
	@Expose
	@OrderColumn
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> exercises = new LinkedList<String>(); 

	@SerializedName("tags")
	@Expose
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> tags = new ArrayList<String>();

	public Integer getIdPath() {
		return idPath;
	}

	public void setIdPath(Integer idPath) {
		this.idPath = idPath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public LearningPathStatus getStatus() {
		return status;
	}

	public void setStatus(LearningPathStatus status) {
		this.status = status;
	}

	public Set<Organization> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(Set<Organization> organizations) {
		this.organizations = organizations;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public Boolean getAllowRenewal() {
		return allowRenewal;
	}

	public void setAllowRenewal(Boolean allowRenewal) {
		this.allowRenewal = allowRenewal;
	}

	public Integer getMonthsExpiration() {
		return monthsExpiration;
	}

	public void setMonthsExpiration(Integer monthsExpiration) {
		this.monthsExpiration = monthsExpiration;
	}

	public Integer getRefresherPercentage() {
		return refresherPercentage;
	}

	public void setRefresherPercentage(Integer refresherPercentage) {
		this.refresherPercentage = refresherPercentage;
	}

	public List<String> getExercises() {
		return exercises;
	}

	public void setExercises(List<String> exercises) {
		this.exercises = exercises;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Boolean getFromHub() {
		return fromHub;
	}

	public void setFromHub(Boolean fromHub) {
		this.fromHub = fromHub;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public MarkdownText getDescription() {
		return description;
	}

	public void setDescription(MarkdownText description) {
		this.description = description;
	}


}
