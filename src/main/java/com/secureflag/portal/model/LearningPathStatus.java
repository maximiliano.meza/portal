package com.secureflag.portal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum LearningPathStatus {

	@SerializedName("0")
	AVAILABLE(0),
	@SerializedName("1")
	DRAFT(1),
	@SerializedName("2")
	INACTIVE(3),
	@SerializedName("3")
	DEPRECATED(3),
	@SerializedName("4")
	COMING_SOON(4),
	@SerializedName("5")
	DOWNLOAD_QUEUE(5);
	
	public static final LearningPathStatus DEFAULT_STATUS = DRAFT;

	private LearningPathStatus(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("status")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public static LearningPathStatus getStatusFromStatusString(String statusCode){
		for (LearningPathStatus status :LearningPathStatus.values()) {
			if (statusCode.equals(status.name().toString())) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}

	public static LearningPathStatus getStatusFromStatusCode(Integer statusCode){
		for (LearningPathStatus status :LearningPathStatus.values()) {
			if (statusCode==status.getName()) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}
	
}
