/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ColumnDefault;

import com.amazonaws.regions.Regions;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.ChallengeDetails;
import com.secureflag.portal.messages.json.annotations.ChallengeExcludedData;
import com.secureflag.portal.messages.json.annotations.HistoryDetails;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;
import com.secureflag.portal.messages.json.annotations.RunningExercises;

@Entity( name = "ExerciseInstance")
@Table( name = "exerciseInstances" )
public class ExerciseInstance implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idExerciseInstance")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
	@Expose
	private Integer idExerciseInstance;

	@SerializedName("organization")
	@Expose
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "organizationId" )
	@ChallengeExcludedData
	private Organization organization;

	@SerializedName("title")
	@Expose
	@Column(name = "title")
	private String title;

	@SerializedName("technology")
	@Expose
	@Column(name = "technology")
	private String technology;

	@SerializedName("startTime")
	@Expose
	@Column(name = "startTime")
	@ChallengeExcludedData
	private Date startTime;

	@SerializedName("endTime")
	@Expose
	@Column(name = "endTime")
	@ChallengeExcludedData
	private Date endTime;

	@SerializedName("region")
	@Expose
	@Enumerated(EnumType.STRING)
	@Column(name = "region")
	@ChallengeExcludedData
	private Regions region;

	@SerializedName("resultsAvailable")
	@Expose
	@ChallengeExcludedData
	@Column(name = "resultsAvailable",columnDefinition="BIT DEFAULT 0")
	private boolean resultsAvailable;
	
	@SerializedName("hasCrashed")
	@Expose
	@Column(name = "hasCrashed",columnDefinition="BIT DEFAULT 0")
	@ChallengeExcludedData
	private boolean hasCrashed;
	
	@SerializedName("crashedInstance")
	@Expose
	@ChallengeExcludedData
	@Column(name = "crashedInstance")
	private Integer crashedInstance;
	
	@SerializedName("issuesReported")
	@Expose
	@ChallengeExcludedData
	@Column(name = "issuesReported",columnDefinition="BIT DEFAULT 0")
	private boolean issuesReported;
	
	@SerializedName("issuesAddressed")
	@Expose
	@ChallengeExcludedData
	@Column(name = "issuesAddressed",columnDefinition="BIT DEFAULT 0")
	private boolean issuesAddressed;
	
	@SerializedName("solved")
	@Expose
	@Column(name = "solved",columnDefinition="BIT DEFAULT 0")
	private boolean solved;

	@SerializedName("status")
	@Expose
	@Enumerated(EnumType.ORDINAL)
	@Column(name="status")
	private ExerciseInstanceStatus status;
	
	@SerializedName("crashReason")
	@Expose
	@ChallengeExcludedData
	@Column(name = "crashReason")
	private String crashReason;

	@SerializedName("type")
	@Expose
	@Enumerated(EnumType.ORDINAL)
	@Column(name="type")
	@ChallengeExcludedData
	private ExerciseInstanceType type;

	@Enumerated(EnumType.ORDINAL)
	@SerializedName("launchType")
	@Expose
	@Column(name="launchType")
	private ExerciseInstanceLaunchType launchType;

	@SerializedName("exerciseStartOpened")
	@Expose
	@Column(name = "exerciseStartOpened",columnDefinition="BIT DEFAULT 0")
	private Boolean exerciseStartOpened;
	
	@SerializedName("exerciseLearnOpened")
	@Expose
	@Column(name="exerciseLearnOpened",columnDefinition="BIT DEFAULT 0")
	private Boolean exerciseLearnOpened;
	
	@SerializedName("exercise")
	@Expose
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@JoinColumn(name = "exerciseId")
	private AvailableExercise exercise;
	
	@SerializedName("challengeId")
	@Expose
	@ChallengeExcludedData
	@Column(name="challengeId")
	private Integer challengeId;

	@SerializedName("scoring")
	@ChallengeExcludedData
	@Expose
	@Column(name="scoring")
	private AvailableExerciseExerciseScoringMode scoring;
	
	@SerializedName("score")
	@Expose
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private ExerciseInstanceScore score;

	@SerializedName("guac")
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private GatewayTempUser guac;

	@Column
	@Expose
	@ChallengeExcludedData
	@ColumnDefault("0")
	@SerializedName("duration")
	private Integer duration;

	@Expose
	@SerializedName("user")
	@LazilySerialized
	@ChallengeDetails
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;

	@SerializedName("ecsInstance")
	@Expose
	@LazilySerialized
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ecsInstanceId")
	private ECSContainerTask ecsInstance;
	
	@SerializedName("ec2Instance")
	@Expose
	@LazilySerialized
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ec2InstanceId")
	private EC2Instance ec2Instance;
	
	@Expose
	@ChallengeExcludedData
	@ColumnDefault("0")
	@SerializedName("countSelfCheckByUser")
	@Column(name = "countSelfCheckByUser")
	private Integer countSelfCheckByUser;

	@SerializedName("results")
	@Expose
	@LazilySerialized
	@RunningExercises
	@HistoryDetails
	@ChallengeDetails
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ExerciseInstanceResult> results = new ArrayList<ExerciseInstanceResult>();

	@SerializedName("resultsFile")
	@LazilySerialized
	@Expose
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@OneToOne(fetch = FetchType.LAZY)
	private ExerciseInstanceResultFile resultFile = new ExerciseInstanceResultFile();

	@SerializedName("usedHints")
	@Expose
	@HistoryDetails
	@RunningExercises
	@LazilySerialized
	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@ManyToMany(fetch = FetchType.LAZY)
	private Set<FlagQuestionHint> usedHints;

	@Expose
	@ChallengeExcludedData
	@SerializedName("trophyAwarded")
	@Column(name = "trophyAwarded",columnDefinition="BIT DEFAULT 0")
	private Boolean trophyAwarded;

	@Expose
	@ChallengeExcludedData
	@SerializedName("newIssuesIntroduced")
	@Column(name = "newIssuesIntroduced",columnDefinition="BIT DEFAULT 0")
	private Boolean newIssuesIntroduced;

	@Expose
	@ChallengeExcludedData
	@SerializedName("newIssuesIntroducedText")
	@Column(name = "newIssuesIntroducedText", columnDefinition = "LONGTEXT")
	private String newIssuesIntroducedText;

	@Expose
	@ChallengeExcludedData
	@SerializedName("reviewer")
	@Column(name = "reviewerId")
	private Integer reviewer;

	@Expose
	@ChallengeExcludedData
	@SerializedName("reviewedDate")
	@Column(name = "reviewedDate")
	private Date reviewedDate;

	@Expose
	@ChallengeExcludedData
	@SerializedName("feedback")
	@Column(name = "feedback",columnDefinition="BIT DEFAULT 0")
	private Boolean feedback;
	
	@Expose
	@ChallengeExcludedData
	@SerializedName("vote")
	@Column(name = "vote",columnDefinition="BIT DEFAULT 0")
	private Boolean vote;
	
	@Expose
	@ChallengeExcludedData
	@SerializedName("countResultsReviewedByUser")
	@Column(name = "countResultsReviewedByUser")
	@ColumnDefault("0")
	private Integer countResultsReviewedByUser;
	
	@Expose
	@ChallengeExcludedData
	@SerializedName("latestDateResultsLastReviewed")
	@Column(name = "latestDateResultsLastReviewed")
	private Date latestDateResultsLastReviewed;
	
	@Expose
	@SerializedName("telemetry")
	@LazilySerialized
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.ALL})
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "telemetryId")
	private ExerciseInstanceTelemetry telemetry;
	
	public ExerciseInstance() {
		this.feedback = false;
		this.newIssuesIntroduced = false;
		this.trophyAwarded = false;
		this.issuesAddressed = false;
		this.issuesReported = false;
		this.countResultsReviewedByUser = 0;
		this.countSelfCheckByUser = 0;
		this.resultsAvailable = false;
	}

	public Set<FlagQuestionHint> getUsedHints() {
		return usedHints;
	}

	public void setUsedHints(Set<FlagQuestionHint> usedHints) {
		this.usedHints = usedHints;
	}

	public ExerciseInstanceResultFile getResultFile() {
		return resultFile;
	}

	public void setResultFile(ExerciseInstanceResultFile resultFiles) {
		this.resultFile = resultFiles;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public ExerciseInstanceStatus getStatus() {
		return status;
	}

	public void setStatus(ExerciseInstanceStatus status) {
		this.status = status;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * 
	 * @return
	 *     The technology
	 */
	public String getTechnology() {
		return technology;
	}

	/**
	 * 
	 * @param technology
	 *     The technology
	 */
	public void setTechnology(String technology) {
		this.technology = technology;
	}

	/**
	 * 
	 * @return
	 *     The dateTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	/**
	 * 
	 * @param dateTime
	 *     The dateTime
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}


	/**
	 * 
	 * @param dateTime
	 *     The dateTime
	 */
	public void setEndTime(Date dateTime) {
		this.endTime = dateTime;
	}

	/**
	 * 
	 * @return
	 *     The gw
	 */
	public Regions getRegion() {
		return region;
	}

	/**
	 * 
	 * @param gw
	 *     The gw
	 */
	public void setRegion(Regions region) {
		this.region = region;
	}

	/**
	 * 
	 * @return
	 *     The score
	 */
	public ExerciseInstanceScore getScore() {
		return score;
	}

	/**
	 * 
	 * @param score
	 *     The score
	 */
	public void setScore(ExerciseInstanceScore score) {
		this.score = score;
	}

	/**
	 * 
	 * @return
	 *     The exercise
	 */
	public AvailableExercise getAvailableExercise() {
		return exercise;
	}

	/**
	 * 
	 * @param exercise
	 *     The exercise
	 */
	public void setAvailableExercise(AvailableExercise exercise) {
		this.exercise = exercise;
	}

	/**
	 * 
	 * @return
	 *     The results
	 */
	public List<ExerciseInstanceResult> getResults() {
		return results;
	}

	/**
	 * 
	 * @param results
	 *     The results
	 */
	public void setResults(List<ExerciseInstanceResult> results) {
		this.results = results;
	}
	public Integer getIdExerciseInstance() {
		return idExerciseInstance;
	}

	public void setIdExerciseInstance(Integer idExerciseInstance) {
		this.idExerciseInstance = idExerciseInstance;
	}

	public boolean isResultsAvailable() {
		return resultsAvailable;
	}

	public void setResultsAvailable(boolean resultsAvailable) {
		this.resultsAvailable = resultsAvailable;
	}

	public GatewayTempUser getGuac() {
		return guac;
	}

	public void setGuac(GatewayTempUser guac) {
		this.guac = guac;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idExerciseInstance == null) ? 0 : idExerciseInstance.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExerciseInstance other = (ExerciseInstance) obj;
		if (idExerciseInstance == null) {
			if (other.idExerciseInstance != null)
				return false;
		} else if (!idExerciseInstance.equals(other.idExerciseInstance))
			return false;
		return true;
	}

	public AvailableExercise getExercise() {
		return exercise;
	}

	public void setExercise(AvailableExercise exercise) {
		this.exercise = exercise;
	}

	public void setTrophyAwarded(Boolean awardTrophy) {
		trophyAwarded = awardTrophy;
	}
	public Boolean getTrophyAwarded() {
		return trophyAwarded;

	}

	public Boolean getNewIssuesIntroduced() {
		return newIssuesIntroduced;
	}

	public void setNewIssuesIntroduced(Boolean newIssuesIntroduced) {
		this.newIssuesIntroduced = newIssuesIntroduced;
	}

	public String getNewIssuesIntroducedText() {
		return newIssuesIntroducedText;
	}

	public void setNewIssuesIntroducedText(String newIssuesIntroducedText) {
		this.newIssuesIntroducedText = newIssuesIntroducedText;
	}

	public Integer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Integer reviewer) {
		this.reviewer = reviewer;
	}

	public Date getReviewedDate() {
		return reviewedDate;
	}

	public void setReviewedDate(Date reviewedDate) {
		this.reviewedDate = reviewedDate;
	}

	public Boolean getFeedback() {
		return feedback;
	}

	public void setFeedback(Boolean feedback) {
		this.feedback = feedback;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public ExerciseInstanceType getType() {
		return type;
	}

	public void setType(ExerciseInstanceType type) {
		this.type = type;
	}

	public ExerciseInstanceLaunchType getLaunchType() {
		return launchType;
	}

	public void setLaunchType(ExerciseInstanceLaunchType launchType) {
		this.launchType = launchType;
	}

	public ECSContainerTask getEcsInstance() {
		return ecsInstance;
	}

	public void setEcsInstance(ECSContainerTask ecsInstance) {
		this.ecsInstance = ecsInstance;
	}

	public Integer getCountResultsReviewedByUser() {
		return countResultsReviewedByUser;
	}

	public void setCountResultsReviewedByUser(Integer countResultsReviewedByUser) {
		this.countResultsReviewedByUser = countResultsReviewedByUser;
	}

	public Date getLatestDateResultsLastReviewed() {
		return latestDateResultsLastReviewed;
	}

	public void setLatestDateResultsLastReviewed(Date latestDateResultsLastReviewed) {
		this.latestDateResultsLastReviewed = latestDateResultsLastReviewed;
	}

	public Integer getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(Integer challengeId) {
		this.challengeId = challengeId;
	}

	public AvailableExerciseExerciseScoringMode getScoring() {
		return scoring;
	}

	public void setScoring(AvailableExerciseExerciseScoringMode scoring) {
		this.scoring = scoring;
	}

	public Integer getCountSelfCheckByUser() {
		return countSelfCheckByUser;
	}

	public void setCountSelfCheckByUser(Integer countSelfCheckByUser) {
		this.countSelfCheckByUser = countSelfCheckByUser;
	}

	public boolean isIssuesReported() {
		return issuesReported;
	}

	public void setIssuesReported(boolean issuesReported) {
		this.issuesReported = issuesReported;
	}

	public boolean isIssuesAddressed() {
		return issuesAddressed;
	}

	public void setIssuesAddressed(boolean issuesAddressed) {
		this.issuesAddressed = issuesAddressed;
	}

	public String getCrashReason() {
		return crashReason;
	}

	public void setCrashReason(String crashReason) {
		this.crashReason = crashReason;
	}

	public boolean isSolved() {
		return solved;
	}

	public void setSolved(boolean solved) {
		this.solved = solved;
	}

	public boolean hasCrashed() {
		return hasCrashed;
	}

	public void setHasCrashed(boolean hasCrashed) {
		this.hasCrashed = hasCrashed;
	}

	public Integer getCrashedInstance() {
		return crashedInstance;
	}

	public void setCrashedInstance(Integer crashedInstance) {
		this.crashedInstance = crashedInstance;
	}

	public ExerciseInstanceTelemetry getTelemetry() {
		return telemetry;
	}

	public void setTelemetry(ExerciseInstanceTelemetry telemetry) {
		this.telemetry = telemetry;
	}

	public boolean isHasCrashed() {
		return hasCrashed;
	}

	public EC2Instance getEc2Instance() {
		return ec2Instance;
	}

	public void setEc2Instance(EC2Instance ec2Instance) {
		this.ec2Instance = ec2Instance;
	}

	public Boolean getVote() {
		return vote;
	}

	public void setVote(Boolean vote) {
		this.vote = vote;
	}

	public Boolean getExerciseStartOpened() {
		return exerciseStartOpened;
	}

	public void setExerciseStartOpened(Boolean exerciseStartOpened) {
		this.exerciseStartOpened = exerciseStartOpened;
	}

	public Boolean getExerciseLearnOpened() {
		return exerciseLearnOpened;
	}

	public void setExerciseLearnOpened(Boolean exerciseLearnOpened) {
		this.exerciseLearnOpened = exerciseLearnOpened;
	}
}
