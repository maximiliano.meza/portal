/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "UserAuthenticationEvent")
@Table( name = "userAuthenticationEvents" )
public class UserAuthenticationEvent {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idUserAuthenticationEvent")
	private Integer idUserAuthenticationEvent;
	
	@Column(name="sessionIdHash")
	private String sessionIdHash;
	
	@Column(name = "username")
	private String username;
	
	@Column(name="loginDate")
	private Date loginDate;
	
	@Column(name="logoutDate")
	private Date logoutDate;
	
	@Column(name="loginSuccessful")
	private boolean loginSuccessful;
	
	@Column(name="sessionTimeMinutes")
	private Integer sessionTimeMinutes;

	public Integer getIdUserAuthenticationEvent() {
		return idUserAuthenticationEvent;
	}

	public void setIdUserAuthenticationEvent(Integer idUserAuthenticationAttempt) {
		this.idUserAuthenticationEvent = idUserAuthenticationAttempt;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public Date getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(Date logoutDate) {
		this.logoutDate = logoutDate;
	}

	public boolean isLoginSuccessful() {
		return loginSuccessful;
	}

	public void setLoginSuccessful(boolean loginSuccessful) {
		this.loginSuccessful = loginSuccessful;
	}

	public Integer getSessionTimeMinutes() {
		return sessionTimeMinutes;
	}

	public void setSessionTimeMinutes(Integer sessionTimeMinutes) {
		this.sessionTimeMinutes = sessionTimeMinutes;
	}

	public String getSessionIdHash() {
		return sessionIdHash;
	}

	public void setSessionIdHash(String sessionIdHash) {
		this.sessionIdHash = sessionIdHash;
	}
	
}
