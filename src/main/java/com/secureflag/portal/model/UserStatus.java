/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum UserStatus implements Serializable {

	ACTIVE(1),
	INACTIVE(0),
	INVITED(-1), 
	CONFIRM_EMAIL(-2),
	LOCKED(-3),
	REMOVED(-4)
	;

	public static final UserStatus DEFAULT_STATUS = ACTIVE;

	private UserStatus(Integer statusCode){
		this.code = statusCode;
	}

	@SerializedName("code")
	@Expose
	private Integer code;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}

	public static UserStatus getStatusFromName(String status){
		for(UserStatus u : UserStatus.values()) {
			if(u.toString().equals(status.toUpperCase())) {
				return u;
			}
		}
		return DEFAULT_STATUS;
	}
}
