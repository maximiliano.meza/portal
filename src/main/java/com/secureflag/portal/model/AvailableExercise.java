/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ColumnDefault;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.BriefDetails;
import com.secureflag.portal.messages.json.annotations.ChallengeDetails;
import com.secureflag.portal.messages.json.annotations.ChallengeExcludedData;
import com.secureflag.portal.messages.json.annotations.ExcludeFromHistoryList;
import com.secureflag.portal.messages.json.annotations.ExcludedForUsers;
import com.secureflag.portal.messages.json.annotations.HistoryDetails;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;
import com.secureflag.portal.messages.json.annotations.LazilySolution;
import com.secureflag.portal.messages.json.annotations.NoChallengeList;
import com.secureflag.portal.messages.json.annotations.NoLightDetails;

@Entity( name = "AvailableExercise" )
@Table( name = "availableExercises" )
public class AvailableExercise implements Serializable{

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idExercise")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
	@Expose
	@ExcludeFromHistoryList
	private Integer id;

	@Expose
	@Column(name = "uuid")
	@BriefDetails
	private String uuid;

	@Column(name = "version")
	@ColumnDefault("0")
	@ExcludeFromHistoryList
	private Integer version;

	@Column(name = "fromHub",columnDefinition="BIT DEFAULT 0")
	@Expose
	@SerializedName("fromHub")
	@ExcludeFromHistoryList
	private Boolean fromHub;

	@SerializedName("quality")
	@Expose
	@ExcludeFromHistoryList
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "quality")
	private AvailableExerciseQuality quality;

	@Column(name = "title")
	@SerializedName("title")
	@ExcludeFromHistoryList
	@Expose
	private String title;

	@Column(name = "subtitle")
	@ExcludeFromHistoryList
	@SerializedName("subtitle")
	@Expose
	private String subtitle;

	@Column(name = "author")
	@ExcludeFromHistoryList
	@SerializedName("author")
	@Expose
	private String author;

	@Column(name = "score")
	@SerializedName("score")
	@ExcludeFromHistoryList
	@Expose
	private Integer score;

	@Column(name = "technology")
	@SerializedName("technology")
	@ExcludeFromHistoryList
	@Expose
	private String technology;
	
	@Column(name = "framework")	
	@SerializedName("framework")
	@ExcludeFromHistoryList
	@Expose
	private String framework;

	@SerializedName("trophyName")
	@ExcludeFromHistoryList
	@Expose
	private String trophyName;

	@Column(name = "duration")
	@SerializedName("duration")
	@ExcludeFromHistoryList
	@Expose
	private Integer duration;

	@Column(name = "difficulty")
	@SerializedName("difficulty")
	@ExcludeFromHistoryList
	@Expose
	private String difficulty;

	@SerializedName("description")
	@ExcludeFromHistoryList
	@Expose
	@Lob
	@Column(name="description")
	private String description;

	@SerializedName("lastUpdate")
	@ExcludeFromHistoryList
	@Expose
	@Column(name="lastUpdate")
	private Date lastUpdate;

	@SerializedName("information")
	@ExcludeFromHistoryList
	@Expose
	@LazilySerialized
	@Cascade({CascadeType.ALL})
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	private AvailableExerciseInformation information;

	@SerializedName("flags")
	@LazilySerialized
	@HistoryDetails
	@ChallengeDetails
	@ExcludeFromHistoryList
	@NoChallengeList
	@Expose
	@Cascade({CascadeType.ALL})
	@OneToMany(fetch = FetchType.LAZY)
	private Set<Flag> questionsList = new HashSet<Flag>();

	@SerializedName("votingScore")
	@ExcludeFromHistoryList
	@Expose
	@Column(name = "votingScore",columnDefinition="INT DEFAULT 0")
	private Integer votingScore;

	@SerializedName("votingScoreNumbers")
	@ExcludeFromHistoryList
	@Expose
	@Column(name = "votingScoreNumbers",columnDefinition="INT DEFAULT 0")
	private Integer votingScoreNumbers;

	@SerializedName("votingScoreList")
	@ExcludeFromHistoryList
	@LazilySerialized
	@Cascade({CascadeType.ALL})
	@OneToMany(fetch = FetchType.LAZY)
	@NoLightDetails
	private List<ExerciseInstanceVotingScore> votingScoreList = new ArrayList<ExerciseInstanceVotingScore>();
	
	@SerializedName("feedbackList")
	@ExcludeFromHistoryList
	@LazilySerialized
	@Cascade({CascadeType.ALL})
	@OneToMany(fetch = FetchType.LAZY)
	@NoLightDetails
	private List<ExerciseInstanceFeedback> feedbackList = new ArrayList<ExerciseInstanceFeedback>();

	@SerializedName("exerciseType")
	@ExcludeFromHistoryList
	@Expose
	@Enumerated(EnumType.STRING)
	@Column(name = "exerciseType")
	private AvailableExerciseType exerciseType;

	@SerializedName("solution")
	@ExcludeFromHistoryList
	@LazilySolution
	@LazilySerialized
	@Expose
	@Cascade({CascadeType.ALL})
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@NoLightDetails
	private AvailableExerciseSolution solution;

	@SerializedName("stack")
	@ExcludeFromHistoryList
	@Expose
	@Cascade({CascadeType.SAVE_UPDATE})
	@ManyToOne(fetch = FetchType.EAGER)
	@NoLightDetails
	private KBTechnologyItem stack;

	@SerializedName("ignoreDefaultStack")
	@ExcludeFromHistoryList
	@Expose
	@Column(name="ignoreDefaultStack",columnDefinition="BIT DEFAULT 0")
	private Boolean ignoreDefaultStack;

	@SerializedName("status")
	@ExcludeFromHistoryList
	@Expose
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status")
	private AvailableExerciseStatus status;

	@SerializedName("tags")
	@ExcludeFromHistoryList
	@Expose
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> tags = new ArrayList<String>();

	@Column(name = "internetOutboundAllowed",columnDefinition="BIT DEFAULT 0")
	@ExcludeFromHistoryList
	@Expose
	private Boolean internetOutboundAllowed;
	
	@Column(name = "clipboardAllowed",columnDefinition="BIT DEFAULT 1")
	@ExcludeFromHistoryList
	@Expose
	private Boolean clipboardAllowed;

	@Column(name = "supportsAutomatedScoring",columnDefinition="BIT DEFAULT 1")
	@ExcludeFromHistoryList
	@Expose
	private Boolean supportsAutomatedScoring;

	@Column(name = "defaultScoring", columnDefinition="INT DEFAULT 2")
	@ExcludeFromHistoryList
	@Expose
	private AvailableExerciseExerciseScoringMode defaultScoring;

	@SerializedName("image")
	@Cascade({CascadeType.ALL})
	@Expose
	@NoLightDetails
	@ChallengeExcludedData
	@ExcludedForUsers
	@OneToOne(fetch = FetchType.EAGER)
	@ExcludeFromHistoryList
	private AvailableExerciseImage image;

	/**
	 * 
	 * @return
	 *     The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *     The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 *     The title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 *     The title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 *     The subtitle
	 */
	public String getSubtitle() {
		return subtitle;
	}

	/**
	 * 
	 * @param subtitle
	 *     The subtitle
	 */
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	/**
	 * 
	 * @return
	 *     The description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 *     The description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 *     The score
	 */
	public Integer getScore() {
		return score;
	}

	/**
	 * 
	 * @param score
	 *     The score
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/**
	 * 
	 * @return
	 *     The technology
	 */
	public String getTechnology() {
		return technology;
	}

	/**
	 * 
	 * @param technology
	 *     The technology
	 */
	public void setTechnology(String technology) {
		this.technology = technology;
	}

	/**
	 * 
	 * @return
	 *     The trophy
	 */
	public String getTrophyName() {
		return trophyName;
	}

	/**
	 * 
	 * @param trophy
	 *     The trophy
	 */
	public void setTrophyName(String trophyName) {
		this.trophyName = trophyName;
	}

	/**
	 * 
	 * @return
	 *     The duration
	 */
	public Integer getDuration() {
		return duration;
	}

	/**
	 * 
	 * @param duration
	 *     The duration
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Set<Flag> getQuestionsList() {
		return questionsList;
	}

	public void setQuestionsList(Set<Flag> questionsList) {
		this.questionsList = questionsList;
	}


	public AvailableExerciseSolution getSolution() {
		return solution;
	}

	public void setSolution(AvailableExerciseSolution solution) {
		this.solution = solution;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public AvailableExerciseType getExerciseType() {
		return exerciseType;
	}

	public void setExerciseType(AvailableExerciseType exerciseType) {
		this.exerciseType = exerciseType;
	}

	public AvailableExerciseStatus getStatus() {
		return status;
	}

	public void setStatus(AvailableExerciseStatus status) {
		this.status = status;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}


	@Override
	public String toString() {
		return "AvailableExercise [id=" + id + ", version=" + version + ", title=" + title + ", subtitle=" + subtitle
				+ ", author=" + author + ", description=" + description + ", score=" + score + ", technology="
				+ technology + ", duration=" + duration + ", difficulty=" + difficulty + ", exerciseType="
				+ exerciseType + ", status=" + status + "]";
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Boolean getInternetOutboundAllowed() {
		return internetOutboundAllowed;
	}

	public void setInternetOutboundAllowed(Boolean internetOutboundAllowed) {
		this.internetOutboundAllowed = internetOutboundAllowed;
	}

	public Boolean getSupportsAutomatedScoring() {
		return supportsAutomatedScoring;
	}

	public void setSupportsAutomatedScoring(Boolean supportsAutomatedScoring) {
		this.supportsAutomatedScoring = supportsAutomatedScoring;
	}

	public AvailableExerciseExerciseScoringMode getDefaultScoring() {
		return defaultScoring;
	}

	public void setDefaultScoring(AvailableExerciseExerciseScoringMode defaultScoring) {
		this.defaultScoring = defaultScoring;
	}

	public AvailableExerciseInformation getInformation() {
		return information;
	}

	public void setInformation(AvailableExerciseInformation information) {
		this.information = information;
	}

	public KBTechnologyItem getStack() {
		return stack;
	}

	public void setStack(KBTechnologyItem stack) {
		this.stack = stack;
	}

	public Boolean getIgnoreDefaultStack() {
		return ignoreDefaultStack;
	}

	public void setIgnoreDefaultStack(Boolean ignoreDefaultStack) {
		this.ignoreDefaultStack = ignoreDefaultStack;
	}

	public Integer getVotingScore() {
		return votingScore;
	}

	public void setVotingScore(Integer votingScore) {
		this.votingScore = votingScore;
	}

	public List<ExerciseInstanceVotingScore> getVotingScoreList() {
		return votingScoreList;
	}

	public void setVotingScoreList(List<ExerciseInstanceVotingScore> votingScoreList) {
		this.votingScoreList = votingScoreList;
	}

	public Integer getVotingScoreNumbers() {
		return votingScoreNumbers;
	}

	public void setVotingScoreNumbers(Integer votingScoreNumbers) {
		this.votingScoreNumbers = votingScoreNumbers;
	}

	public AvailableExerciseQuality getQuality() {
		return quality;
	}

	public void setQuality(AvailableExerciseQuality quality) {
		this.quality = quality;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Boolean getFromHub() {
		return fromHub;
	}

	public void setFromHub(Boolean fromHub) {
		this.fromHub = fromHub;
	}

	public AvailableExerciseImage getImage() {
		return image;
	}

	public void setImage(AvailableExerciseImage image) {
		this.image = image;
	}

	public Boolean getClipboardAllowed() {
		return clipboardAllowed;
	}

	public void setClipboardAllowed(Boolean clipboardAllowed) {
		this.clipboardAllowed = clipboardAllowed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvailableExercise other = (AvailableExercise) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		else if(uuid.equals(other.uuid) && status.equals(other.status))
			return true;
		return false;
	}

	public String getFramework() {
		return framework;
	}

	public void setFramework(String framework) {
		this.framework = framework;
	}

	public List<ExerciseInstanceFeedback> getFeedbackList() {
		return feedbackList;
	}

	public void setFeedbackList(List<ExerciseInstanceFeedback> feedbackList) {
		this.feedbackList = feedbackList;
	}


}
