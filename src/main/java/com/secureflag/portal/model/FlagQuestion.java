/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;


import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.HistoryDetails;
import com.secureflag.portal.messages.json.annotations.LazilyHint;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;
import com.secureflag.portal.messages.json.annotations.NoLightDetails;

@Entity( name = "flagQuestion")
@Table( name = "flagQuestions" )
public class FlagQuestion implements Serializable{

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idFlagQuestion")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
    @Expose
    private Integer id;

	@SerializedName("md")
	@LazilySerialized
	@Expose
	@Cascade({CascadeType.SAVE_UPDATE})
	@ManyToOne(fetch = FetchType.LAZY)
	@HistoryDetails
	@NoLightDetails
    private MarkdownText md;
    
    @SerializedName("hintAvailable")
    @Expose
    @Column(name = "hintAvailable")
    private Boolean hintAvailable;
    
    @SerializedName("selfCheckAvailable")
    @Expose
    @Column(name = "selfCheckAvailable")
    private Boolean selfCheckAvailable;
    
    @SerializedName("optional")
    @Expose
    @Column(name = "optional")
    private Boolean optional;
    
    @SerializedName("selfCheck")
    @Expose
    @OneToOne(fetch=FetchType.EAGER)
	@Cascade({CascadeType.ALL})
    private FlagQuestionSelfcheck selfCheck;
    
    @SerializedName("selfCheckName")
    @Expose
    @Column(name = "selfCheckName")
    private String selfCheckName;
    
    @SerializedName("type")
    @Expose
    @Column(name = "type")
    private String type;
    
    @SerializedName("maxScore")
    @Expose
    @Column(name = "maxScore")
    private Integer maxScore;
    
    @LazilyHint
    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @Expose    
	@Cascade({CascadeType.ALL})
    private FlagQuestionHint hint;

    public FlagQuestionHint getHint() {
		return hint;
	}

	public void setHint(FlagQuestionHint hint) {
		this.hint = hint;
	}

	/**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

 
    /**
     * 
     * @return
     *     The hintAvailable
     */
    public Boolean getHintAvailable() {
        return hintAvailable;
    }

    /**
     * 
     * @param hintAvailable
     *     The hintAvailable
     */
    public void setHintAvailable(Boolean hintAvailable) {
        this.hintAvailable = hintAvailable;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

	public FlagQuestionSelfcheck getSelfCheck() {
		return selfCheck;
	}

	public void setSelfCheck(FlagQuestionSelfcheck selfCheckName) {
		this.selfCheck = selfCheckName;
	}

	public Boolean getSelfCheckAvailable() {
		return selfCheckAvailable;
	}

	public void setSelfCheckAvailable(Boolean selfCheckAvailable) {
		this.selfCheckAvailable = selfCheckAvailable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlagQuestion other = (FlagQuestion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Integer getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(Integer maxScore) {
		this.maxScore = maxScore;
	}

	public Boolean getOptional() {
		return optional;
	}

	public void setOptional(Boolean optional) {
		this.optional = optional;
	}

	public String getSelfCheckName() {
		return selfCheckName;
	}

	public void setSelfCheckName(String selfCheckName) {
		this.selfCheckName = selfCheckName;
	}

	public MarkdownText getMd() {
		return md;
	}

	public void setMd(MarkdownText md) {
		this.md = md;
	}

}
