package com.secureflag.portal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum GatewayStatus {

	@SerializedName("0")
	AVAILABLE(0),
	@SerializedName("1")
	DEPRECATED(1);
	
	public static final GatewayStatus DEFAULT_STATUS = AVAILABLE;

	private GatewayStatus(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("status")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
}
