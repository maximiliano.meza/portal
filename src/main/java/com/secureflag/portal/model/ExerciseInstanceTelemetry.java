package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.Telemetry;

@Entity
public class ExerciseInstanceTelemetry implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@SerializedName("pageLoadTime")
	@Telemetry
	@Column(name = "pageLoadTime")
	private Integer pageLoadTime;
	
	@SerializedName("renderTime")
	@Telemetry
	@Column(name = "renderTime")
	private Integer renderTime;
	
	@SerializedName("dnsLookupTime")
	@Telemetry
	@Column(name = "dnsLookupTime")
	private Integer dnsLookupTime;
	
	@SerializedName("connectionTime")
	@Telemetry
	@Column(name = "connectionTime")
	private Integer connectionTime;
	
	@SerializedName("tlsTime")
	@Telemetry
	@Column(name = "tlsTime")
	private Integer tlsTime;
	
	@SerializedName("totalTime")
	@Telemetry
	@Column(name = "totalTime")
	private Integer totalTime;
	
	@SerializedName("downloadTime")
	@Telemetry
	@Column(name = "downloadTime")
	private Integer downloadTime;
	
	@SerializedName("ttfb")
	@Telemetry
	@Column(name = "ttfb")
	private Integer ttfb;
	
	@SerializedName("timesPolled")
	@Column(name="timesPolled")
	@Telemetry
	private Integer timesPolled;
	
	@SerializedName("dateFulfilled")
	@Column(name="dateFulfilled")
	@Telemetry
	private Date dateFulfilled;
	
	@SerializedName("dateRequested")
	@Column(name="dateRequested")
	@Telemetry
	private Date dateRequested;
	
	@SerializedName("dateRunning")
	@Column(name="dateRunning")
	@Telemetry
	private Date dateRunning;
	
	public Integer getPageLoadTime() {
		return pageLoadTime;
	}
	public void setPageLoadTime(Integer pageLoadTime) {
		this.pageLoadTime = pageLoadTime;
	}
	public Integer getRenderTime() {
		return renderTime;
	}
	public void setRenderTime(Integer renderTime) {
		this.renderTime = renderTime;
	}
	public Integer getDnsLookupTime() {
		return dnsLookupTime;
	}
	public void setDnsLookupTime(Integer dnsLookupTime) {
		this.dnsLookupTime = dnsLookupTime;
	}
	public Integer getConnectionTime() {
		return connectionTime;
	}
	public void setConnectionTime(Integer connectionTime) {
		this.connectionTime = connectionTime;
	}
	public Integer getTlsTime() {
		return tlsTime;
	}
	public void setTlsTime(Integer tlsTime) {
		this.tlsTime = tlsTime;
	}
	public Integer getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(Integer totalTime) {
		this.totalTime = totalTime;
	}
	public Integer getDownloadTime() {
		return downloadTime;
	}
	public void setDownloadTime(Integer downloadTime) {
		this.downloadTime = downloadTime;
	}
	public Integer getTtfb() {
		return ttfb;
	}
	public void setTtfb(Integer ttfb) {
		this.ttfb = ttfb;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTimesPolled() {
		return timesPolled;
	}
	public void setTimesPolled(Integer timesPolled) {
		this.timesPolled = timesPolled;
	}
	public Date getDateFulfilled() {
		return dateFulfilled;
	}
	public void setDateFulfilled(Date dateFulfilled) {
		this.dateFulfilled = dateFulfilled;
	}
	public Date getDateRequested() {
		return dateRequested;
	}
	public void setDateRequested(Date dateRequested) {
		this.dateRequested = dateRequested;
	}
	public Date getDateRunning() {
		return dateRunning;
	}
	public void setDateRunning(Date dateRunning) {
		this.dateRunning = dateRunning;
	}

}
