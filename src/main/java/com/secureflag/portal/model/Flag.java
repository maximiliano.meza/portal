/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.NoLightDetails;

@Entity
@Table( name = "flags" )
public class Flag implements Serializable{

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idFlag")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
    @Expose
    private Integer id;
	
    @SerializedName("title")
    @Expose
	@Column(name = "title")
    private String title;
    
    @SerializedName("category")
    @Expose
    private String category;
    
    @SerializedName("kb")
    @Expose
    @ManyToOne
    private KBVulnerabilityItem kb;

    @SerializedName("flagList")
    @Expose
    @Cascade({CascadeType.ALL})
    @NoLightDetails
    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<FlagQuestion> flagQuestionList = new HashSet<FlagQuestion>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The flagList
     */
    public Set<FlagQuestion> getFlagQuestionList() {
        return flagQuestionList;
    }

    /**
     * 
     * @param flagList
     *     The flagList
     */
    public void setFlagQuestionList(Set<FlagQuestion> flagQuestionList) {
        this.flagQuestionList = flagQuestionList;
    }

	public KBVulnerabilityItem getKb() {
		return kb;
	}

	public void setKb(KBVulnerabilityItem kb) {
		this.kb = kb;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
