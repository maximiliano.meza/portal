package com.secureflag.portal.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum KBStatus  implements Serializable{

	@SerializedName("0")
	AVAILABLE(0),
	@SerializedName("1")
	DEPRECATED(1);
	
	public static final KBStatus DEFAULT_STATUS = AVAILABLE;

	private KBStatus(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("status")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public static KBStatus getStatusFromStatusString(String statusCode){
		for (KBStatus status :KBStatus.values()) {
			if (statusCode.equals(status.name().toString())) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}

	public static KBStatus getStatusFromStatusCode(Integer statusCode){
		for (KBStatus status : KBStatus.values()) {
			if (statusCode==status.getName()) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}
	
}
