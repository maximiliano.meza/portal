/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.BriefDetails;
import com.secureflag.portal.messages.json.annotations.UserStatusList;

@Entity( name = "Organization" )
@Table( name = "organizations" )
public class Organization implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@Expose
	@UserStatusList
	@BriefDetails
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "name", unique = true)
	@SerializedName("name")
	@Expose
	@BriefDetails
	@UserStatusList
	private String name;
	
	@Column(name = "dateJoined")
	@SerializedName("dateJoined")
	private Date dateJoined;
	
	@Column(name = "status")
	@SerializedName("status")
	private OrganizationStatus status;
	
	@Column(name = "maxUsers")
	@SerializedName("maxUsers")
	private Integer maxUsers;
	
	@Column(name = "email")
	@SerializedName("email")
	private String email;
	
	@SerializedName("createdByUser")
	@Column(name="createdByUser")
	private Integer createdByUser;
	
	@SerializedName("allowManualReview")
	@Column(name="allowManualReview",columnDefinition="BIT DEFAULT 1")
	private Boolean allowManualReview;
	
	@Column(name = "defaultCredits",columnDefinition="INT DEFAULT 10")
	@SerializedName("defaultCredits")
	private Integer defaultCredits;
		

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDateJoined() {
		return dateJoined;
	}
	public void setDateJoined(Date dateJoined) {
		this.dateJoined = dateJoined;
	}
	public Integer getMaxUsers() {
		return maxUsers;
	}
	public void setMaxUsers(Integer maxUsers) {
		this.maxUsers = maxUsers;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public OrganizationStatus getStatus() {
		return status;
	}
	public void setStatus(OrganizationStatus status) {
		this.status = status;
	}
	public Integer getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(Integer createdByUser) {
		this.createdByUser = createdByUser;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Organization other = (Organization) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Boolean getAllowManualReview() {
		return allowManualReview;
	}
	public void setAllowManualReview(Boolean allowManualReview) {
		this.allowManualReview = allowManualReview;
	}
	public Integer getDefaultCredits() {
		return defaultCredits;
	}
	public void setDefaultCredits(Integer defaultCredits) {
		this.defaultCredits = defaultCredits;
	}
}
