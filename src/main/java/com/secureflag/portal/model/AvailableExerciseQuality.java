package com.secureflag.portal.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum AvailableExerciseQuality implements Serializable {
	
	@SerializedName("0")
	HUB_DEFAULT(0),
	@SerializedName("1")
	HUB_VETTED(1),
	@SerializedName("2")
	SF_DEFAULT(2),
	@SerializedName("3")
	SF_VETTED(3),
	@SerializedName("4")
	SF_CUSTOM(4);
	
	public static final AvailableExerciseQuality DEFAULT_STATUS = SF_DEFAULT;

	private AvailableExerciseQuality(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("name")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	public static AvailableExerciseQuality getStatusFromStatusString(String statusCode){
		for (AvailableExerciseQuality status :AvailableExerciseQuality.values()) {
			if (statusCode.equals(status.name().toString())) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}

	public static AvailableExerciseQuality getStatusFromStatusCode(Integer statusCode){
		for (AvailableExerciseQuality status :AvailableExerciseQuality.values()) {
			if (statusCode==status.getName()) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}
}
