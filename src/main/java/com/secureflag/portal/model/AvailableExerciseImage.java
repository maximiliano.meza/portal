package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;

@Entity
public class AvailableExerciseImage implements Serializable{ // NO_UCD

	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idImage")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "taskDefinitionName")
	@Expose
	private String taskDefinitionName;
	
	@Column(name = "containerName")
	@Expose
	private String containerName;
	
	@Column(name = "softMemory")
	@Expose
	private Integer softMemory;
	
	@Column(name = "hardMemory")
	@Expose
	private Integer hardMemory;
	
	@Column(name = "updateDate")
	@Expose
	private Date updateDate;
	
	@Column(name = "imageUrl")
	@Expose
	private String imageUrl;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaskDefinitionName() {
		return taskDefinitionName;
	}

	public void setTaskDefinitionName(String taskDefinitionName) {
		this.taskDefinitionName = taskDefinitionName;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public Integer getSoftMemory() {
		return softMemory;
	}

	public void setSoftMemory(Integer softMemory) {
		this.softMemory = softMemory;
	}

	public Integer getHardMemoryLimit() {
		return hardMemory;
	}

	public void setHardMemoryLimit(Integer hardMemoryLimit) {
		this.hardMemory = hardMemoryLimit;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}
