package com.secureflag.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.messages.json.annotations.ChallengeExcludedData;
import com.secureflag.portal.messages.json.annotations.ExcludedForUsers;
import com.secureflag.portal.messages.json.annotations.HistoryDetails;
import com.secureflag.portal.messages.json.annotations.LazilySerialized;
import com.secureflag.portal.messages.json.annotations.NoLightDetails;

@Entity( name = "TechnologyStack")
@Table( name = "TechnologyStacks" )
public class KBTechnologyItem implements Serializable{

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idStack")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@SerializedName("id")
	@Expose
	private Integer id;

	@Column(name = "technology")	
	@SerializedName("technology")
	@Expose
	private String technology;
	
	@Column(name = "framework")	
	@SerializedName("variant")
	@Expose
	private String framework;

	@Column(name = "imageUrl")	
	@SerializedName("imageUrl")
	@ChallengeExcludedData
	@Expose
	@ExcludedForUsers
	private String imageUrl;
	
	@Column(name = "fromHub",columnDefinition="BIT DEFAULT 0")
	@Expose
	@SerializedName("fromHub")
	private Boolean fromHub;
	
	@Expose
	@Column(name = "uuid")
	private String uuid;
	
	@Expose
	@Enumerated(EnumType.ORDINAL)
	@SerializedName("status")
	@Column(name="status",columnDefinition="INT DEFAULT 0")
	private KBStatus status;
	
	@SerializedName("lastUpdate")
	@Expose
	@Column(name="lastUpdate")
	private Date lastUpdate;

	@SerializedName("md")
	@LazilySerialized
	@Expose
	@Cascade({CascadeType.SAVE_UPDATE})
	@ManyToOne(fetch = FetchType.LAZY)
	@NoLightDetails
	@HistoryDetails
	private MarkdownText md;

	public MarkdownText getMd() {
		return md;
	}

	public void setMd(MarkdownText md) {
		this.md = md;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public KBStatus getStatus() {
		return status;
	}

	public void setStatus(KBStatus status) {
		this.status = status;
	}

	public Boolean getFromHub() {
		return fromHub;
	}

	public void setFromHub(Boolean fromHub) {
		this.fromHub = fromHub;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getVariant() {
		return this.framework;
	}

	public void setVariant(String variation) {
		this.framework = variation;
	}

}
