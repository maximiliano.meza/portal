/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.secureflag.portal.model.dto.TokenConnectionMessage;

@Entity(name = "ExerciseInstanceReservation")
@Table( name = "ExerciseInstanceReservations" )
public class ExerciseInstanceReservation {

	@Id
	@Expose
	@SerializedName("idReservation")
	@Column(name = "idReservation")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@SerializedName("fulfilled")
	@Expose
	@Column(name="fulfilled")
	private Boolean fulfilled;
	
	@SerializedName("timesPolled")
	@Column(name="timesPolled")
	private Integer timesPolled;
	
	@SerializedName("dateFulfilled")
	@Column(name="dateFulfilled")
	private Date dateFulfilled;
	
	@SerializedName("dateRunning")
	@Column(name="dateRunning")
	private Date dateRunning;
	
	@SerializedName("dateRequested")
	@Column(name="dateRequested")
	private Date dateRequested;

	@SerializedName("crashedExerciseId")
	@Column(name="crashedExerciseId")
	private Integer crashedExerciseId; 

	@SerializedName("error")
	@Expose
	@Column(name="error")
	private Boolean error;

	@SerializedName("waitSeconds")
	@Expose
	@Column(name="waitSeconds")
	private Integer waitSeconds;

	@Column(name="type")
	private ExerciseInstanceLaunchType type;

	@Column(name="tmpPassword")
	private String tmpPassword;

	@Transient
	@Expose
	@SerializedName("connection")
	private TokenConnectionMessage token;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	private User user;

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	@JoinColumn(name = "organizationId" )
	private Organization organization;

	@ManyToOne(fetch = FetchType.EAGER)
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Expose
	@JoinColumn(name = "exerciseId")
	private AvailableExercise exercise;
	
	@SerializedName("challengeId")
	@Expose
	@Column(name="challengeId")
	private Integer challengeId;

	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ecs")
	private ECSContainerTask ecs;
	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ec2")
	private EC2Instance ec2;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getFulfilled() {
		return fulfilled;
	}

	public void setFulfilled(Boolean fulfilled) {
		this.fulfilled = fulfilled;
	}

	public Boolean getError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public Integer getWaitSeconds() {
		return waitSeconds;
	}

	public void setWaitSeconds(Integer waitSeconds) {
		this.waitSeconds = waitSeconds;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public ECSContainerTask getEcs() {
		return ecs;
	}

	public void setEcs(ECSContainerTask ecs) {
		this.ecs = ecs;
	}

	public String getTmpPassword() {
		return tmpPassword;
	}

	public void setTmpPassword(String tmpPassword) {
		this.tmpPassword = tmpPassword;
	}

	public AvailableExercise getExercise() {
		return exercise;
	}

	public void setExercise(AvailableExercise exercise) {
		this.exercise = exercise;
	}

	public ExerciseInstanceLaunchType getType() {
		return type;
	}

	public void setType(ExerciseInstanceLaunchType type) {
		this.type = type;
	}

	public TokenConnectionMessage getToken() {
		return token;
	}

	public void setToken(TokenConnectionMessage token) {
		this.token = token;
	}

	public Integer getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(Integer challenge) {
		this.challengeId = challenge;
	}


	public Integer getCrashedExerciseId() {
		return crashedExerciseId;
	}

	public void setCrashedExerciseId(Integer crashedExerciseId) {
		this.crashedExerciseId = crashedExerciseId;
	}

	public Integer getTimesPolled() {
		return timesPolled;
	}

	public void setTimesPolled(Integer timesPolled) {
		this.timesPolled = timesPolled;
	}

	public Date getDateFulfilled() {
		return dateFulfilled;
	}

	public void setDateFulfilled(Date dateFulfilled) {
		this.dateFulfilled = dateFulfilled;
	}

	public Date getDateRequested() {
		return dateRequested;
	}

	public void setDateRequested(Date dateRequested) {
		this.dateRequested = dateRequested;
	}

	public Date getDateRunning() {
		return dateRunning;
	}

	public void setDateRunning(Date dateRunning) {
		this.dateRunning = dateRunning;
	}

	public EC2Instance getEc2() {
		return ec2;
	}

	public void setEc2(EC2Instance ec2) {
		this.ec2 = ec2;
	}
}
