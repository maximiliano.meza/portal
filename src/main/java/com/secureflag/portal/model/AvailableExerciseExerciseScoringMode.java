/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum AvailableExerciseExerciseScoringMode implements Serializable{

	@SerializedName("0")
	AUTOMATED_REVIEW(0),
	@SerializedName("1")
	NOT_USED(1),
	@SerializedName("2")
	MANUAL_REVIEW(2),
	;

	public static final AvailableExerciseExerciseScoringMode DEFAULT_STATUS = MANUAL_REVIEW;

	private AvailableExerciseExerciseScoringMode(Integer statusCode){
		this.statusCode = statusCode;
	}

	@SerializedName("name")
	@Expose
	private Integer statusCode;

	public Integer getName() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public static AvailableExerciseExerciseScoringMode getStatusFromStatusString(String statusCode){
		for (AvailableExerciseExerciseScoringMode status :AvailableExerciseExerciseScoringMode.values()) {
			if (statusCode.equals(status.name().toString())) {
				return status;
			}
		}
		return DEFAULT_STATUS;
	}
	
	public static AvailableExerciseExerciseScoringMode getStatusFromStatusCode(Integer statusCode){
		for (AvailableExerciseExerciseScoringMode status :AvailableExerciseExerciseScoringMode.values()) {
			if (statusCode==status.getName()) {
				return status;
			}
		}
		return null;
	}

}