/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.controllers.mgmt;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.config.Constants;
import com.secureflag.portal.controllers.ActionsController;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;

public class AdminController extends ActionsController {

	@SuppressWarnings({ "rawtypes", "serial" })
	public AdminController(){
		
		HashMap<String, Class[]> idNotNullInteger = new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}};
		
		type2action.put("checkTeamNameAvailable", com.secureflag.portal.actions.mgmt.orgadmin.CheckTeamNameAvailable.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.CheckTeamNameAvailable.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		
		type2action.put("unlockUserAccount", com.secureflag.portal.actions.mgmt.orgadmin.DoUnlockUserAccountAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.DoUnlockUserAccountAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
			}
					);
		}});
		type2action.put("updateUserStatus", com.secureflag.portal.actions.mgmt.orgadmin.UpdateUserStatusAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.UpdateUserStatusAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
			}
					);
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
			}
					);
		}});
		type2action.put("addUser", com.secureflag.portal.actions.mgmt.orgadmin.AddUserAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.AddUserAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_EMAIL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorEmail.class
			}
					);
			put(Constants.ACTION_PARAM_PASSWORD, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_CREDITS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_COUNTRY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_FIRST_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_LAST_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ROLE_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_CONCURRENT_EXERCISE_LIMIT, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_FORCE_CHANGE_PASSWORD, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorBoolean.class
			}
					);
		}});
		type2action.put("addTeam", com.secureflag.portal.actions.mgmt.orgadmin.AddTeamAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.AddTeamAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_TEAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
			}
					);
			put(Constants.ACTION_PARAM_ORG_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		
		type2action.put("getAvailableUsersForTeam", com.secureflag.portal.actions.mgmt.orgadmin.GetUsersWithoutTeamAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.GetUsersWithoutTeamAction.class, idNotNullInteger);
		type2action.put("addTeamManager", com.secureflag.portal.actions.mgmt.orgadmin.DoMakeUserTeamManagerAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.DoMakeUserTeamManagerAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_TEAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("removeFromTeam", com.secureflag.portal.actions.mgmt.orgadmin.RemoveUserFromTeamAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.RemoveUserFromTeamAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_TEAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("addUsersToTeam", com.secureflag.portal.actions.mgmt.orgadmin.AddUsersToTeamAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.AddUsersToTeamAction.class, idNotNullInteger);

		type2action.put("getInvitationCodes", com.secureflag.portal.actions.mgmt.orgadmin.GetInvitationCodesAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.GetInvitationCodesAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("generateInvitation", com.secureflag.portal.actions.mgmt.orgadmin.DoGenerateInvitationCodeAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.DoGenerateInvitationCodeAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_MAX_USERS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("removeChallenge", com.secureflag.portal.actions.mgmt.orgadmin.RemoveChallengeAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.RemoveChallengeAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("removeInvitation", com.secureflag.portal.actions.mgmt.orgadmin.RemoveInvitationCodeAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.RemoveInvitationCodeAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_ORG_CODE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("updateUser", com.secureflag.portal.actions.mgmt.orgadmin.UpdateUserAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.UpdateUserAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_EMAIL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorEmail.class
			}
					);
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_CREDITS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_COUNTRY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_FIRST_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_LAST_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ROLE_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_CONCURRENT_EXERCISE_LIMIT, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		
		
	
		type2action.put("removeUser", com.secureflag.portal.actions.mgmt.orgadmin.RemoveUserAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.RemoveUserAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		
		type2action.put("getAWSRegions", com.secureflag.portal.actions.mgmt.orgadmin.GetAWSRegionsAction.class);
		
		
		type2action.put("getOrganizations", com.secureflag.portal.actions.mgmt.orgadmin.GetOrganizationsAction.class);
	
		
		type2action.put("removeTeamManager", com.secureflag.portal.actions.mgmt.orgadmin.RemoveTeamManagerAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.RemoveTeamManagerAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_TEAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		
		type2action.put("resetEmailUserPassword", com.secureflag.portal.actions.mgmt.orgadmin.DoResetEmailUserPasswordAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.DoResetEmailUserPasswordAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		
		type2action.put("resetUserPassword", com.secureflag.portal.actions.mgmt.orgadmin.DoResetUserPasswordAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.DoResetUserPasswordAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_PASSWORD, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("deleteTeam", com.secureflag.portal.actions.mgmt.orgadmin.RemoveTeamAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.orgadmin.RemoveTeamAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_TEAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected boolean isGranted(HttpServletRequest request, HttpServletResponse response, Class actionClass) {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		if(actionClass == com.secureflag.portal.actions.user.UpdateUserPasswordAction.class)
			return true;
	
		if(null!=sessionUser.getEmailVerified() && !sessionUser.getEmailVerified()){
			MessageGenerator.sendErrorMessage("VerifyEmail", response);
			return false;
		}
		if(null!=sessionUser.getForceChangePassword() && sessionUser.getForceChangePassword()){
			MessageGenerator.sendErrorMessage("ChangePassword", response);
			return false;
		}
		return true;
	}
	
	
	
}
