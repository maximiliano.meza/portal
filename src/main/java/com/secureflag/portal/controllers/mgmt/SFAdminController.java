package com.secureflag.portal.controllers.mgmt;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.config.Constants;
import com.secureflag.portal.controllers.ActionsController;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;

public class SFAdminController extends ActionsController {

	@SuppressWarnings({ "rawtypes", "serial" })
	public SFAdminController(){
		
		type2action.put("getUserCToken", com.secureflag.portal.actions.user.GetCSRFTokenAction.class);
		type2action.put("getExerciseDetails", com.secureflag.portal.actions.mgmt.team.GetAvailableExerciseDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetAvailableExerciseDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getExercises", com.secureflag.portal.actions.mgmt.team.GetAllAvailableExercisesAction.class);
		type2action.put("createECRRepo", com.secureflag.portal.actions.mgmt.sfadmin.CreateRepoForSDKDeployAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.CreateRepoForSDKDeployAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		type2action.put("getFrameworks", com.secureflag.portal.actions.mgmt.sfadmin.GetAllFrameworksActions.class);
		type2action.put("addFramework", com.secureflag.portal.actions.mgmt.sfadmin.AddFrameworkAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.AddFrameworkAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		type2action.put("updateGateway", com.secureflag.portal.actions.mgmt.sfadmin.UpdateSatelliteGatewayAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.UpdateSatelliteGatewayAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_FQDN, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorBoolean.class
			}
					);
			put(Constants.ACTION_PARAM_INSTANCE_SCALE_IN, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorBoolean.class
			}
					);
			put(Constants.ACTION_PARAM_IMAGE_SYNC, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorBoolean.class
			}
					);
			put(Constants.ACTION_PARAM_PLACEMENT_STRATEGY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("removeFramework", com.secureflag.portal.actions.mgmt.sfadmin.RemoveFrameworkAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveFrameworkAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		
		type2action.put("getHubVulnerabilityKBList", com.secureflag.portal.actions.mgmt.sfadmin.GetHubVulnerabilityKBListAction.class);
		type2action.put("getHubTechnologyKBList", com.secureflag.portal.actions.mgmt.sfadmin.GetHubTechnologyKBListAction.class);

		
		type2action.put("updateHubTechnologyKB", com.secureflag.portal.actions.mgmt.sfadmin.DoUpdateHubTechnologyKBAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.DoUpdateHubTechnologyKBAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		type2action.put("updateHubVulnerabilityKB", com.secureflag.portal.actions.mgmt.sfadmin.DoUpdateHubVulnerabilityKBAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.DoUpdateHubVulnerabilityKBAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		
		type2action.put("getInstallationQueue", com.secureflag.portal.actions.mgmt.sfadmin.GetInstallationQueueAction.class);
		type2action.put("restartInstallationQueueItem", com.secureflag.portal.actions.mgmt.sfadmin.RestartInstallationQueueItemAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RestartInstallationQueueItemAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
		}});
		type2action.put("removeInstallationQueueItem", com.secureflag.portal.actions.mgmt.sfadmin.RemoveInstallationQueueItemAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveInstallationQueueItemAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
		}});
		type2action.put("installHubExercise", com.secureflag.portal.actions.mgmt.sfadmin.DoInstallUpdateHubExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.DoInstallUpdateHubExerciseAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		
		type2action.put("installHubPath", com.secureflag.portal.actions.mgmt.sfadmin.DoInstallUpdateHubPathAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.DoInstallUpdateHubPathAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		
		type2action.put("getHubKBItem", com.secureflag.portal.actions.mgmt.sfadmin.GetHubVulnerabilityKBItemAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.GetHubVulnerabilityKBItemAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		type2action.put("getHubTechnologyItem", com.secureflag.portal.actions.mgmt.sfadmin.GetHubTechnologyKBItemAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.GetHubTechnologyKBItemAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		type2action.put("getHubExercises", com.secureflag.portal.actions.mgmt.sfadmin.GetHubExercisesAction.class);
		type2action.put("getHubExerciseDetails", com.secureflag.portal.actions.mgmt.sfadmin.GetHubExerciseDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.GetHubExerciseDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		type2action.put("getHubPaths", com.secureflag.portal.actions.mgmt.sfadmin.GetHubPathsAction.class);
		type2action.put("getAllOrganizations", com.secureflag.portal.actions.mgmt.sfadmin.GetAllOrganizationsAction.class);

		type2action.put("checkPathNameAvailable", com.secureflag.portal.actions.mgmt.sfadmin.CheckPathNameAvailableAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.CheckPathNameAvailableAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_TECHNOLOGY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_DIFFICULTY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		type2action.put("addPath", com.secureflag.portal.actions.mgmt.sfadmin.AddLearningPathAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.AddLearningPathAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_TECHNOLOGY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_DETAILS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_DIFFICULTY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_EXPIRATION, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
			put(Constants.ACTION_PARAM_RENEWAL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorBoolean.class
			});
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
		}});
		type2action.put("removePath", com.secureflag.portal.actions.mgmt.sfadmin.RemoveLearningPathAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveLearningPathAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
		}});	
		type2action.put("updatePath", com.secureflag.portal.actions.mgmt.sfadmin.UpdateLearningPathAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.UpdateLearningPathAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_TECHNOLOGY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_DETAILS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_DIFFICULTY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
			put(Constants.ACTION_PARAM_EXPIRATION, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
			put(Constants.ACTION_PARAM_REFRESHER, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
		}});
		
		type2action.put("enableExerciseForOrg", com.secureflag.portal.actions.mgmt.sfadmin.EnableAvailableExerciseForOrganizationAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.EnableAvailableExerciseForOrganizationAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_EXERCISE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		type2action.put("disableExerciseForOrg", com.secureflag.portal.actions.mgmt.sfadmin.DisableAvailableExerciseForOrganizationAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.DisableAvailableExerciseForOrganizationAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_EXERCISE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			});
		}});
		
		type2action.put("checkOrganizationNameAvailable", com.secureflag.portal.actions.mgmt.sfadmin.CheckOrganizationNameAvailableAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.CheckOrganizationNameAvailableAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		

		type2action.put("getGateways", com.secureflag.portal.actions.mgmt.sfadmin.GetSatelliteGatewaysAction.class);
		type2action.put("addVulnerability", com.secureflag.portal.actions.mgmt.sfadmin.AddVulnerabilityAction.class);
		type2action.put("updateVulnerability", com.secureflag.portal.actions.mgmt.sfadmin.UpdateVulnerabilityAction.class);
		type2action.put("deleteVulnerability", com.secureflag.portal.actions.mgmt.sfadmin.RemoveVulnerabilityAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveVulnerabilityAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("addOrganization", com.secureflag.portal.actions.mgmt.sfadmin.AddOrganizationAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.AddOrganizationAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_MAX_USERS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_CREDITS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_EMAIL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ALLOW_MANUAL_REVIEW, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorBoolean.class
			}
					);


		}});
		type2action.put("removeOrganization", com.secureflag.portal.actions.mgmt.sfadmin.RemoveOrganizationAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveOrganizationAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("updateOrganization", com.secureflag.portal.actions.mgmt.sfadmin.UpdateOrganizationAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.UpdateOrganizationAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_MAX_USERS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_EMAIL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ALLOW_MANUAL_REVIEW, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorBoolean.class
			}
					);
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_CREDITS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("addTechnology", com.secureflag.portal.actions.mgmt.sfadmin.AddTechnologyAction.class);
		type2action.put("updateTechnology", com.secureflag.portal.actions.mgmt.sfadmin.UpdateTechnologyAction.class);
		type2action.put("deleteTechnology", com.secureflag.portal.actions.mgmt.sfadmin.RemoveTechnologyAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveTechnologyAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getOnlineGateways", com.secureflag.portal.actions.mgmt.sfadmin.GetOnlineGatewaysAction.class);
		type2action.put("listContainerInstances", com.secureflag.portal.actions.mgmt.sfadmin.GetContainerInstancesInfoAction.class);
		type2action.put("removeImageFromContainerInstance", com.secureflag.portal.actions.mgmt.sfadmin.RemoveImageFromContainerInstanceAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveImageFromContainerInstanceAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_IMAGE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_CONTAINER_INSTANCE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_REGION, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("updateExerciseDefaultScoring", com.secureflag.portal.actions.mgmt.sfadmin.UpdateExerciseDefaultScoringAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.UpdateExerciseDefaultScoringAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DEFAULT_SCORING, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("updateExercise", com.secureflag.portal.actions.mgmt.sfadmin.UpdateExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.UpdateExerciseAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_SUBTITLE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DIFFICULTY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_TITLE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_EXERCISE_TYPE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_AUTHOR, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DESCRIPTION, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_TECHNOLOGY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DURATION, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_TROPHY_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_EXERCISE_QUALITY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_IGNORE_DEFAULT_STACK, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("addExercise", com.secureflag.portal.actions.mgmt.sfadmin.AddExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.AddExerciseAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_SUBTITLE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DIFFICULTY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_TITLE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_EXERCISE_TYPE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_AUTHOR, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DESCRIPTION, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_TECHNOLOGY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DURATION, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_TROPHY_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_EXERCISE_QUALITY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_IGNORE_DEFAULT_STACK, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);

		}});
		type2action.put("removeExercise", com.secureflag.portal.actions.mgmt.sfadmin.RemoveExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveExerciseAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_EXERCISE_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("addTaskDefinition", com.secureflag.portal.actions.mgmt.sfadmin.AddExerciseInRegionAction.class);
		type2classValidator.put("addTaskDefinition", com.secureflag.portal.actions.mgmt.sfadmin.validators.AddExerciseInRegionValidator.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.AddExerciseInRegionAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_EXERCISE_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_REGION, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_TASK_DEFINITION_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_CONTAINER_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_SOFT_MEMORY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_HARD_MEMORY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_REPO_URL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorURL.class
			}
					);
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorBoolean.class
			}
					);
		}});
	
		type2action.put("removeExerciseInRegion", com.secureflag.portal.actions.mgmt.sfadmin.RemoveExerciseFromRegionAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.RemoveExerciseFromRegionAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_EXERCISE_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
			put(Constants.ACTION_PARAM_TASK_DEFINITION_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
		}});
		type2action.put("enableExerciseInRegion", com.secureflag.portal.actions.mgmt.sfadmin.EnableExerciseInRegionAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.EnableExerciseInRegionAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_TASK_DEFINITION_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
		}});
		type2action.put("disableExerciseInRegion", com.secureflag.portal.actions.mgmt.sfadmin.DisableExerciseInRegionAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.sfadmin.DisableExerciseInRegionAction.class, new HashMap<String, Class[]>() {{

			put(Constants.ACTION_PARAM_TASK_DEFINITION_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			});
		}});
		
	//sdk
		
		type2action.put("getECRCredentials", com.secureflag.portal.actions.mgmt.sfadmin.GetECRCredentialsAction.class);
		type2action.put("getS3Credentials", com.secureflag.portal.actions.mgmt.sfadmin.GetHubS3CredentialsAction.class);
		type2action.put("getUploadBucket", com.secureflag.portal.actions.mgmt.sfadmin.GetHubUploadBucketAction.class);

	
		csrfExclusion.add(com.secureflag.portal.actions.mgmt.sfadmin.GetECRCredentialsAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.mgmt.sfadmin.GetHubS3CredentialsAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.mgmt.sfadmin.GetHubUploadBucketAction.class);

		csrfExclusion.add(com.secureflag.portal.actions.user.GetCSRFTokenAction.class);

	}

	@SuppressWarnings("rawtypes")
	@Override
	protected boolean isGranted(HttpServletRequest request, HttpServletResponse response, Class actionClass) {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		if(actionClass == com.secureflag.portal.actions.user.UpdateUserPasswordAction.class)
			return true;
	
		if(null!=sessionUser.getEmailVerified() && !sessionUser.getEmailVerified()){
			MessageGenerator.sendErrorMessage("VerifyEmail", response);
			return false;
		}
		if(null!=sessionUser.getForceChangePassword() && sessionUser.getForceChangePassword()){
			MessageGenerator.sendErrorMessage("ChangePassword", response);
			return false;
		}
		return true;
	}


}