/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.controllers.mgmt;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.config.Constants;
import com.secureflag.portal.controllers.ActionsController;

public class TeamManagerController  extends ActionsController {

	@SuppressWarnings({ "serial", "rawtypes" })
	public TeamManagerController() {		

		HashMap<String, Class[]> idNotNullInteger = new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}};
		type2action.put("getPaths", com.secureflag.portal.actions.mgmt.team.GetLearningPathsAction.class);
		
		type2action.put("getAllKbs", com.secureflag.portal.actions.mgmt.team.GetVulnerabilityKBAction.class);
		type2action.put("getAllStacks", com.secureflag.portal.actions.mgmt.team.GetTechnologyStackKBAction.class);
		type2action.put("getStackItem", com.secureflag.portal.actions.mgmt.team.GetTechnologyStackKBItemAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetTechnologyStackKBItemAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getKBItem", com.secureflag.portal.actions.mgmt.team.GetVulnerabilityKBItemAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetVulnerabilityKBItemAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("updateExerciseResult", com.secureflag.portal.actions.mgmt.team.UpdateFlagQuestionResultAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.UpdateFlagQuestionResultAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_STATUS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_SCORE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_COMMENT, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		
		
		type2action.put("addResultComment", com.secureflag.portal.actions.mgmt.team.AddResultCommentAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.AddResultCommentAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_TEXT, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		
		type2action.put("updateChallenge", com.secureflag.portal.actions.mgmt.team.UpdateChallengeAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.UpdateChallengeAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DETAILS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_START_DATE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_END_DATE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_SCORING_MODE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_SCORING_FIRST_PLACE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_SCORING_SECOND_PLACE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_SCORING_THIRD_PLACE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("addChallenge", com.secureflag.portal.actions.mgmt.team.AddChallengeAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.AddChallengeAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_DETAILS, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_START_DATE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_END_DATE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ID_ORG, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_SCORING_MODE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("checkChallengeNameAvailable", com.secureflag.portal.actions.mgmt.team.CheckChallengeNameAvailable.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.CheckChallengeNameAvailable.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.CheckChallengeNameAvailable.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getAllRunningExercises", com.secureflag.portal.actions.mgmt.team.GetRunningExercisesAction.class);
		type2action.put("getUsersInOrg", com.secureflag.portal.actions.mgmt.team.GetUsersInOrganization.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetUsersInOrganization.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ORG_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});

		type2action.put("getExerciseDetails", com.secureflag.portal.actions.mgmt.team.GetAvailableExerciseDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetAvailableExerciseDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});

		
		type2action.put("getRegionsForExercise", com.secureflag.portal.actions.mgmt.team.GetAvailableRegionsForExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetAvailableRegionsForExerciseAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		
		type2action.put("getExercises", com.secureflag.portal.actions.mgmt.team.GetAllAvailableExercisesAction.class);
		type2action.put("getSolutionFile", com.secureflag.portal.actions.mgmt.team.GetAvailableExerciseSolutionFileAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetAvailableExerciseSolutionFileAction.class, idNotNullInteger);
	
		type2action.put("getPendingReviews", com.secureflag.portal.actions.mgmt.team.GetPendingReviewsAction.class);
		type2action.put("getCompletedReviews", com.secureflag.portal.actions.mgmt.team.GetCompletedExercisesAction.class);
		type2action.put("renameTeam", com.secureflag.portal.actions.mgmt.team.UpdateTeamNameAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.UpdateTeamNameAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_TEAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		
		type2action.put("getReviewDetails", com.secureflag.portal.actions.mgmt.team.GetReviewDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetReviewDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getUserFeedback", com.secureflag.portal.actions.mgmt.team.GetUserFeedbackForExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetUserFeedbackForExerciseAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getReviewFile", com.secureflag.portal.actions.mgmt.team.GetReviewFileAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.GetReviewFileAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("postReview", com.secureflag.portal.actions.mgmt.team.DoPostReviewAction.class);
		type2action.put("markAsCancelled", com.secureflag.portal.actions.mgmt.team.DoMarkExerciseAsCancelledAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.team.DoMarkExerciseAsCancelledAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected boolean isGranted(HttpServletRequest request, HttpServletResponse response, Class actionClass) {

		return true;
	}
}