/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.controllers.mgmt;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.config.Constants;
import com.secureflag.portal.controllers.ActionsController;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;

public class MonitorController  extends ActionsController {

	@SuppressWarnings({ "serial", "rawtypes" })
	public MonitorController(){
		
		type2action.put("getChallenges", com.secureflag.portal.actions.mgmt.stats.GetChallengesAction.class);
		type2action.put("getChallengeDetails", com.secureflag.portal.actions.mgmt.stats.GetChallengeDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetChallengeDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getChallengeResults", com.secureflag.portal.actions.mgmt.stats.GetChallengeResultsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetChallengeResultsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("sendNotification", com.secureflag.portal.actions.mgmt.stats.DoSendNotificationToUser.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.DoSendNotificationToUser.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_NOTIFICATION_TEXT, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getGlobalStats", com.secureflag.portal.actions.mgmt.stats.GetStatsGlobalAction.class);		
		type2action.put("getTeamDetails", com.secureflag.portal.actions.mgmt.stats.GetTeamDetails.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetTeamDetails.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getUserStats", com.secureflag.portal.actions.mgmt.stats.GetStatsUserAction.class);		
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetStatsUserAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getUserAchievements", com.secureflag.portal.actions.mgmt.stats.GetUserAchievementsAction.class);		
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetUserAchievementsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getUserExercises", com.secureflag.portal.actions.mgmt.stats.GetUserCompletedExercisesAction.class);		
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetUserCompletedExercisesAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getTeamMembers", com.secureflag.portal.actions.mgmt.stats.GetTeamMembersAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetTeamMembersAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getTeams", com.secureflag.portal.actions.mgmt.stats.GetTeamsAction.class);
		type2action.put("getTeamStats", com.secureflag.portal.actions.mgmt.stats.GetStatsTeamAction.class);		
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetStatsTeamAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getUserDetails", com.secureflag.portal.actions.mgmt.stats.GetUserDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.mgmt.stats.GetUserDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});	
		type2action.put("getUsers", com.secureflag.portal.actions.mgmt.stats.GetUsersAction.class);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected boolean isGranted(HttpServletRequest request, HttpServletResponse response, Class actionClass) {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		if(actionClass == com.secureflag.portal.actions.user.UpdateUserPasswordAction.class)
			return true;
	
		if(null!=sessionUser.getEmailVerified() && !sessionUser.getEmailVerified()){
			MessageGenerator.sendErrorMessage("VerifyEmail", response);
			return false;
		}
		if(null!=sessionUser.getForceChangePassword() && sessionUser.getForceChangePassword()){
			MessageGenerator.sendErrorMessage("ChangePassword", response);
			return false;
		}
		return true;
	}
}