/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.controllers.user;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.config.Constants;
import com.secureflag.portal.controllers.ActionsController;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;

public class UserController extends ActionsController{

	@SuppressWarnings({ "serial", "rawtypes" })
	public UserController() {		

		
		HashMap<String, Class[]> idNotNullInteger = new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}};
		type2action.put("getPaths", com.secureflag.portal.actions.user.GetLearningPathsAction.class);
		type2action.put("sendTelemetry", com.secureflag.portal.actions.user.DoSendTelemetryAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoSendTelemetryAction.class, idNotNullInteger);
		type2action.put("setPanelRead", com.secureflag.portal.actions.user.DoSetPanelOpenedAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoSetPanelOpenedAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_PANEL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("cancelCrashedExercise", com.secureflag.portal.actions.user.CancelCrashedExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.CancelCrashedExerciseAction.class, idNotNullInteger);
		type2action.put("cancelChallengeExercise", com.secureflag.portal.actions.user.CancelChallengeExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.CancelChallengeExerciseAction.class, idNotNullInteger);
	
		type2action.put("applyForPathCertification", com.secureflag.portal.actions.user.DoApplyForPathCertificationAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoApplyForPathCertificationAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		
		type2action.put("removeUser", com.secureflag.portal.actions.user.RemoveUserSelfAction.class);
		type2action.put("getStackItem", com.secureflag.portal.actions.user.GetTechnologyKBItemAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetTechnologyKBItemAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getKBItem", com.secureflag.portal.actions.user.GetVulnerabilityKBItemAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetVulnerabilityKBItemAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_TECHNOLOGY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("addScoringComplaint", com.secureflag.portal.actions.user.AddUserResultCommentAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.AddUserResultCommentAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_TEXT, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
	
		type2action.put("getSolutionFile", com.secureflag.portal.actions.user.GetAvailableExerciseSolutionAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetAvailableExerciseSolutionAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getUserReservations", com.secureflag.portal.actions.user.GetReservationsUserAction.class);

		type2action.put("isExerciseInChallenge", com.secureflag.portal.actions.user.CheckExerciseInChallengeAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.CheckExerciseInChallengeAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
				com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
		}
				);
		}});
		type2action.put("getReservationUpdate", com.secureflag.portal.actions.user.DoPollReservationUpdateAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoPollReservationUpdateAction.class, idNotNullInteger);
		type2action.put("getNotifications", com.secureflag.portal.actions.user.GetUreadNotificationsAction.class);
		type2action.put("markNotificationRead", com.secureflag.portal.actions.user.DoMarkNotificationReadAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoMarkNotificationReadAction.class, idNotNullInteger);
		type2action.put("getChallenges", com.secureflag.portal.actions.user.GetChallengesAction.class);
		type2action.put("getChallengeDetails", com.secureflag.portal.actions.user.GetChallengeDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetChallengeDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getChallengeResults", com.secureflag.portal.actions.user.GetChallengeResultsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetChallengeResultsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getLeaderboard", com.secureflag.portal.actions.user.GetTeamLeaderboardAction.class);
		type2action.put("getExercises", com.secureflag.portal.actions.user.GetAvailableExercisesAction.class);
		type2action.put("getExerciseLightDetails", com.secureflag.portal.actions.user.GetAvailableExerciseLightDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetAvailableExerciseLightDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getExerciseDetails", com.secureflag.portal.actions.user.GetAvailableExerciseDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetAvailableExerciseDetailsAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getRegionsForExercise", com.secureflag.portal.actions.user.GetAvailableRegionsForExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetAvailableRegionsForExerciseAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});		
		type2action.put("getHint", com.secureflag.portal.actions.user.GetHintForFlagQuestionAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetHintForFlagQuestionAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_ID_EXERCISE_INSTANCE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getUserCToken", com.secureflag.portal.actions.user.GetCSRFTokenAction.class);
		type2action.put("getUserAchievements", com.secureflag.portal.actions.user.GetAchievementsAction.class);
		type2action.put("getUserHistory", com.secureflag.portal.actions.user.GetExerciseHistoryAction.class);
		type2action.put("getUserHistoryDetails", com.secureflag.portal.actions.user.GetExerciseHistoryDetailsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetExerciseHistoryDetailsAction.class, idNotNullInteger);
		type2action.put("getUserHistoryDetailsFile", com.secureflag.portal.actions.user.GetExerciseHistoryZipFileAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetExerciseHistoryZipFileAction.class, idNotNullInteger);
		type2action.put("getUserInfo", com.secureflag.portal.actions.user.GetInfoUserAction.class);
		type2action.put("doLogout", com.secureflag.portal.actions.user.DoLogoutAction.class);
		type2action.put("getRunningExercises", com.secureflag.portal.actions.user.GetRunningExercisesAction.class);
		type2action.put("setUserInfo",com.secureflag.portal.actions.user.UpdateUserInfoAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.UpdateUserInfoAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_FIRST_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_LAST_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_EMAIL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorEmail.class
			}
					);
			put(Constants.ACTION_PARAM_COUNTRY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});	
		type2action.put("setUserPassword", com.secureflag.portal.actions.user.UpdateUserPasswordAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.UpdateUserPasswordAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_OLDPASSWORD, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_NEWPASSWORD, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("launchExerciseInstance", com.secureflag.portal.actions.user.DoLaunchExerciseInstanceAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoLaunchExerciseInstanceAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_UUID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getInstanceStatus", com.secureflag.portal.actions.user.GetExerciseInstanceStatusAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetExerciseInstanceStatusAction.class, idNotNullInteger);
		type2action.put("getResultStatus", com.secureflag.portal.actions.user.GetAutomatedResultsAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetAutomatedResultsAction.class, idNotNullInteger);
		type2action.put("refreshGuacToken", com.secureflag.portal.actions.user.GetGuacTokenForExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.GetGuacTokenForExerciseAction.class, idNotNullInteger);
		type2action.put("stopExerciseInstance", com.secureflag.portal.actions.user.DoStopExerciseInstanceAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoStopExerciseInstanceAction.class, idNotNullInteger);
		type2action.put("acceptCrashedInstanceResult", com.secureflag.portal.actions.user.AcceptCrashedExerciseResultAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.AcceptCrashedExerciseResultAction.class, idNotNullInteger);
	
		type2action.put("addFeedback", com.secureflag.portal.actions.user.DoLeaveFeedbackAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoLeaveFeedbackAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_FEEDBACK, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("voteForExercise", com.secureflag.portal.actions.user.DoVoteExerciseAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.user.DoVoteExerciseAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ID, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorNotNull.class,
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
			put(Constants.ACTION_PARAM_VOTE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorInteger.class
			}
					);
		}});
		type2action.put("getTeamStats", com.secureflag.portal.actions.user.GetStatsTeamAction.class);		
		type2action.put("getMyStats", com.secureflag.portal.actions.user.GetStatsUserAction.class);		
		csrfExclusion.add(com.secureflag.portal.actions.user.GetCSRFTokenAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.user.UpdateUserPasswordAction.class);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected boolean isGranted(HttpServletRequest request, HttpServletResponse response, Class actionClass) {
		User sessionUser = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);

		if(actionClass == com.secureflag.portal.actions.user.UpdateUserPasswordAction.class)
			return true;
	
		if(null!=sessionUser.getEmailVerified() && !sessionUser.getEmailVerified()){
			MessageGenerator.sendErrorMessage("VerifyEmail", response);
			return false;
		}
		if(null!=sessionUser.getForceChangePassword() && sessionUser.getForceChangePassword()){
			MessageGenerator.sendErrorMessage("ChangePassword", response);
			return false;
		}
		return true;
	}
}