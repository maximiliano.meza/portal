/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.controllers.unauth;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.secureflag.portal.config.Constants;
import com.secureflag.portal.controllers.ActionsController;

public class AnonymousController extends ActionsController {

	@SuppressWarnings({ "serial", "rawtypes" })
	public AnonymousController() {		
		
		type2action.put("getPlatformFeatures", com.secureflag.portal.actions.unauth.GetPlatformFeaturesAction.class);

	
		type2action.put("doResetPassword", com.secureflag.portal.actions.unauth.DoResetPasswordAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.unauth.DoResetPasswordAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_NEWPASSWORD, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
					
			});
			put(Constants.ACTION_PARAM_TOKEN, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
					
			});
		}});

		type2action.put("doLogin", com.secureflag.portal.actions.unauth.DoLoginAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.unauth.DoLoginAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
					
			}
					);
			put(Constants.ACTION_PARAM_PASSWORD, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("isInviteCodeValid", com.secureflag.portal.actions.unauth.CheckOrganizationInvitationCodeAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.unauth.CheckOrganizationInvitationCodeAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_ORG_CODE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
					
			}
					);
		}});
		type2action.put("isUsernameAvailable", com.secureflag.portal.actions.unauth.CheckUsernameAvailableAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.unauth.CheckUsernameAvailableAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class,
					
			}
					);
		}});

		type2action.put("doSignup", com.secureflag.portal.actions.unauth.DoSignupUserAction.class);
		type2fieldValidator.put(com.secureflag.portal.actions.unauth.DoSignupUserAction.class, new HashMap<String, Class[]>() {{
			put(Constants.ACTION_PARAM_EMAIL, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorEmail.class
			}
					);
			put(Constants.ACTION_PARAM_PASSWORD, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_ORG_CODE, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_COUNTRY, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_FIRST_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_LAST_NAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
			put(Constants.ACTION_PARAM_USERNAME, new Class[]{
					com.secureflag.portal.actions.validators.ValidatorStringNotEmpty.class
			}
					);
		}});
		type2action.put("getCountries", com.secureflag.portal.actions.unauth.GetAllCountriesAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.unauth.CheckUsernameAvailableAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.unauth.CheckOrganizationInvitationCodeAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.unauth.DoLoginAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.unauth.DoSignupUserAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.unauth.GetAllCountriesAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.unauth.DoResetPasswordAction.class);
		csrfExclusion.add(com.secureflag.portal.actions.unauth.GetPlatformFeaturesAction.class);

	}

	@SuppressWarnings("rawtypes")
	@Override
	protected boolean isGranted(HttpServletRequest request, HttpServletResponse response, Class actionClass) {
		return true;
	}
}
