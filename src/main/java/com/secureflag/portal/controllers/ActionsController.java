/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.secureflag.portal.actions.SFAction;
import com.secureflag.portal.actions.validators.IClassValidator;
import com.secureflag.portal.actions.validators.IFieldValidator;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.messages.json.MessageGenerator;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.dto.ValidatedData;
import com.secureflag.portal.utils.CSRFTokenUtils;

public abstract class ActionsController {

	@SuppressWarnings("rawtypes")
	protected Map<String, Class> type2action = new HashMap<>();
	@SuppressWarnings("rawtypes")
	protected Map<Class, Map<String, Class[]>> type2fieldValidator= new HashMap<>();
	@SuppressWarnings("rawtypes")
	protected Map<String, Class> type2classValidator= new HashMap<>();
	@SuppressWarnings("rawtypes")
	protected List<Class> csrfExclusion = new LinkedList<Class>();

	private static Logger logger = LoggerFactory.getLogger(ActionsController.class);

	/**
	 * Execute Action
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public void executeAction(HttpServletRequest request, HttpServletResponse response) {
		
		User user = (User) request.getSession().getAttribute(Constants.ATTRIBUTE_SECURITY_CONTEXT);	

		//supports logging messages for unauthenticated requests
		String usr = "";
		if(user!=null && null!=user.getIdUser()){
			usr = String.valueOf(user.getIdUser());
		}
		
		if(!isJSONContentType(request)){
			logger.error("INVALID content type received from user: "+usr);
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_NOT_VALIDATED, response);
			return;	
		}	
		
		JsonObject jsonObject;
		String actionType;
		try {
			StringBuffer allLines = new StringBuffer();
			String output;
			while ((output = request.getReader().readLine()) != null) {
				allLines.append(output);
			}  
			jsonObject = (JsonObject) new JsonParser().parse(allLines.toString());
			JsonElement jActionType = jsonObject.get(Constants.JSON_ATTRIBUTE_ACTION);
			if (jActionType==null) {
				logger.error("Missing value for '"+Constants.JSON_ATTRIBUTE_ACTION+ "' - JSON is " + jsonObject.toString() + " - from user: "+usr);
				MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_NOT_VALIDATED, response);
				return;
			}
			actionType = jActionType.getAsString();
		} catch (JsonSyntaxException | IOException e) {
			logger.error("Not valid Json format from user: "+usr+" - Exception: " + e.getMessage());
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_NOT_VALIDATED, response);
			return;
		}

		// Find Action
		Class actionClass = type2action.get(actionType);
		if (null == actionClass) {
			logger.error("Action NOT found for actionType: "+actionType+ " for user: "+usr);
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_NOT_VALIDATED, response);
			return;
		}

		// Authorize Request
		if (!isGranted(request, response, actionClass)) {
			logger.error("Action NOT authorized. actionClass: " + actionClass+ " for user: "+usr);
			return;
		}

		// CSRF 
		if(!csrfExclusion.contains(actionClass)){
			if(!CSRFTokenUtils.isValid(request.getSession(), jsonObject)){
				logger.error("Action csrf token INVALID. actionClass: " + actionClass+ " for user: "+usr);
				MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_INVALID_CSRF, response);
				return;
			}
		}

		// Validate Request
		Map<String, Class[]> validators = type2fieldValidator.get(actionClass);
		ValidatedData validatedData = new ValidatedData(jsonObject, false, new ArrayList<String>());
		if(null!=validators){
			validatedData = performValidation(jsonObject, validators, validatedData);
		}

		// Validate Request, class-wide
		Class validatorClass = type2classValidator.get(actionType);
		if (null!=validatorClass && !validatedData.isWithErrors()) {
			try {
				IClassValidator validator = ((IClassValidator) validatorClass.newInstance());
				logger.debug("Class Wide Validator - doValidation: " + validatorClass);
				validatedData = validator.doValidation(jsonObject, validatedData);
			} catch (Exception e) {
				logger.error("Class Wide Validator - doValidation Exception: " + e.getMessage() + " for user: "+usr);
				MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_NOT_VALIDATED, response);
			}
		}
		if (validatedData.isWithErrors()) {
			logger.error("Action NOT validated. actionClass: " + actionClass + " validation: " + new Gson().toJson(validatedData.getErrors())+ " for user: "+usr);
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_NOT_VALIDATED, response);
			return;
		}

		// Execute request
		try {
			SFAction action = ((SFAction) actionClass.newInstance());
			logger.debug("ExecuteAction: " + actionClass);
			request.setAttribute(Constants.REQUEST_ATTRIBUTE_JSON, jsonObject);
			action.doAction(request,response);	
		} catch ( Exception  e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error("Action Exception: "+actionClass.toString()+" for user: "+usr+"\n" + e.getMessage()+"\n"+errors.toString() );
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_EXCEPTION, response);
		}

	}

	private Boolean isJSONContentType(HttpServletRequest request) {
		String cType = request.getHeader("Content-Type");
		if(null==cType || cType.equals("")){
			return false;
		}
		return cType.indexOf(Constants.JSON_CONTENT_TYPE)==0;
	}

	/**
	 * Check if Request Message is valid
	 * @param json
	 * @param validators
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	private ValidatedData performValidation(JsonObject json, Map<String, Class[]> validators, ValidatedData validatedData) {
		for (String attribute : validators.keySet()) {
			for (Class validatorClass : validators.get(attribute)) {
				try {
					IFieldValidator validator = ((IFieldValidator) validatorClass.newInstance());
					if (!validator.isValid(json, attribute) ){
						validatedData.setWithErrors(true);
						validatedData.addError(attribute + ":" + json.get(attribute) + " NOT pass " + validatorClass);
					}
				} catch (InstantiationException | IllegalAccessException e) {
					logger.error("Validator Exception: " + e.getMessage());
					validatedData.setWithErrors(true);
				}
			}
		}	
		return validatedData;
	}

	@SuppressWarnings("rawtypes")
	protected abstract boolean isGranted(HttpServletRequest request, HttpServletResponse response, Class actionClass);

	@SuppressWarnings("rawtypes")
	public Map<String, Class> getType2action() {
		return type2action;
	}
}