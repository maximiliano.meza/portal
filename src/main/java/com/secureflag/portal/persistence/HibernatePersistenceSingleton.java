/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.persistence;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernatePersistenceSingleton {

	private static Logger logger = LoggerFactory.getLogger(HibernatePersistenceSingleton.class);
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory==null)
			init();
		return sessionFactory;
	}
	
	private static void init(){
		StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure()
				.build();
		try {
			sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
			logger.debug("Created Hibernate SessionFactory");
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			StandardServiceRegistryBuilder.destroy( registry );
			throw e;
		}
	}
} 