/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.portal.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.regions.Regions;
import com.secureflag.portal.config.Constants;
import com.secureflag.portal.model.AWSSupportedRegion;
import com.secureflag.portal.model.AchievementType;
import com.secureflag.portal.model.AvailableExercise;
import com.secureflag.portal.model.AvailableExerciseStatus;
import com.secureflag.portal.model.AvailableExerciseType;
import com.secureflag.portal.model.AvailableExercisesForOrganization;
import com.secureflag.portal.model.Challenge;
import com.secureflag.portal.model.ChallengeStatus;
import com.secureflag.portal.model.Country;
import com.secureflag.portal.model.DownloadQueueItem;
import com.secureflag.portal.model.DownloadQueueItemStatus;
import com.secureflag.portal.model.ECSContainerTask;
import com.secureflag.portal.model.ECSTaskDefinition;
import com.secureflag.portal.model.ECSTaskDefinitionForExerciseInRegion;
import com.secureflag.portal.model.ExerciseInstance;
import com.secureflag.portal.model.ExerciseInstanceFeedback;
import com.secureflag.portal.model.ExerciseInstanceReservation;
import com.secureflag.portal.model.ExerciseInstanceResult;
import com.secureflag.portal.model.ExerciseInstanceResultFile;
import com.secureflag.portal.model.ExerciseInstanceScore;
import com.secureflag.portal.model.ExerciseInstanceStatus;
import com.secureflag.portal.model.ExerciseInstanceTelemetry;
import com.secureflag.portal.model.ExerciseInstanceVotingScore;
import com.secureflag.portal.model.FargateSGForDeletion;
import com.secureflag.portal.model.Flag;
import com.secureflag.portal.model.FlagQuestion;
import com.secureflag.portal.model.FlagQuestionHint;
import com.secureflag.portal.model.Framework;
import com.secureflag.portal.model.Gateway;
import com.secureflag.portal.model.GatewayStatus;
import com.secureflag.portal.model.GatewayTempUser;
import com.secureflag.portal.model.HubAuthentication;
import com.secureflag.portal.model.InvitationCodeForOrganization;
import com.secureflag.portal.model.KBStatus;
import com.secureflag.portal.model.KBTechnologyItem;
import com.secureflag.portal.model.KBVulnerabilityItem;
import com.secureflag.portal.model.LearningPath;
import com.secureflag.portal.model.LearningPathStatus;
import com.secureflag.portal.model.Notification;
import com.secureflag.portal.model.Organization;
import com.secureflag.portal.model.Team;
import com.secureflag.portal.model.Trophy;
import com.secureflag.portal.model.User;
import com.secureflag.portal.model.UserAchievement;
import com.secureflag.portal.model.UserAuthenticationEvent;
import com.secureflag.portal.model.UserFailedLogins;
import com.secureflag.portal.model.UserStatus;
import com.secureflag.portal.model.ValidationToken;
import com.secureflag.portal.model.dto.ExerciseInChallengeStatus;
import com.secureflag.portal.utils.RandomGeneratorUtils;

public class HibernatePersistenceFacade {
	private static Logger logger = LoggerFactory.getLogger(HibernatePersistenceFacade.class);

	private synchronized static HibernateSessionTransactionWrapper openSessionTransaction() {
		HibernatePersistenceFacade pf = new HibernatePersistenceFacade();
		HibernateSessionTransactionWrapper hb = pf.new HibernateSessionTransactionWrapper();
		hb.localSession = getHibernateSession();
		hb.localTransaction = hb.localSession.getTransaction();
		hb.localTransaction.begin();
		return hb;
	}
	private static void closeCommitSessionTransaction(HibernateSessionTransactionWrapper hb) {
		if(null!=hb) {
			hb.localTransaction.commit();
			hb.localSession.close();	
		}
	}
	private static void rollbackSessionTransaction(HibernateSessionTransactionWrapper hb) {
		if(null!=hb) {
			hb.localTransaction.rollback();
			hb.localSession.close();
		}
	}
	public class HibernateSessionTransactionWrapper {
		Session localSession;
		Transaction localTransaction;
	}
	private synchronized static EntityManager getHibernateEntityManager() {
		return HibernatePersistenceSingleton.getSessionFactory().createEntityManager();
	}
	private static Session getHibernateSession() {
		Session localSession = HibernatePersistenceSingleton.getSessionFactory().openSession();
		return localSession;
	}


	public Integer addUser(User user){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( user );
			closeCommitSessionTransaction(hb);
			return id;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<User> getManagementAllUsers() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();	
		try {
			List<User> users = hb.localSession.createQuery("select distinct(u) from User u "
					+ "where u.status != :removed")
					.setParameter("removed", UserStatus.REMOVED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return users;
		}catch(Exception e){	
			logger.error(e.getMessage());
			rollbackSessionTransaction(hb);
			return new LinkedList<User>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> getManagementAllUsers(Set<Organization> organizations){
		List<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();	
		try {
			List<User> users = hb.localSession.createQuery("select distinct(u) from User u "
					+ "where u.defaultOrganization.id in (:names) and u.status != :removed")
					.setParameterList("names", ids)
					.setParameter("removed", UserStatus.REMOVED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return users;
		}catch(Exception e){	
			logger.error(e.getMessage());
			rollbackSessionTransaction(hb);
			return new LinkedList<User>();
		}
	}

	public void updateExerciseInstanceUsedHints(ExerciseInstance ei,FlagQuestionHint hint) {
		EntityManager em =  getHibernateEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		try{
			em.createNativeQuery(
					"INSERT INTO exerciseInstances_flagQuestionHints "+ 
							"(ExerciseInstance_idExerciseInstance,usedHints_idFlagQuestionHint) "+ 
					"VALUES (?,?)" )
			.setParameter(1,ei.getIdExerciseInstance())
			.setParameter(2, hint.getId())
			.executeUpdate();
			et.commit();
			em.close();
		}catch(Exception e){
			et.rollback();
			em.close();
			logger.error("Failed adding hint "+hint.getId()+" to exercise instance "+ei.getIdExerciseInstance()+" because of: \n"+e.getMessage());
		}

	}	



	public Boolean testDatabaseConnection() {
		try{
			HibernatePersistenceFacade pf = new HibernatePersistenceFacade();
			HibernateSessionTransactionWrapper hb = pf.new HibernateSessionTransactionWrapper();
			hb.localSession = HibernatePersistenceSingleton.getSessionFactory().openSession();
			hb.localTransaction = hb.localSession.getTransaction();
			hb.localTransaction.begin();	
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){
			logger.warn("Database not available due to: "+e.getMessage());
			return false;
		}
	}

	public User getUser(String username, String password){		
		EntityManager em =  getHibernateEntityManager();
		User user;
		try{
			user = (User) em.createNativeQuery(
					"SELECT * FROM users WHERE username = :usr AND password = SHA2(CONCAT(:pwd,(SELECT salt FROM users WHERE username = :usr)),512)", User.class )
					.setParameter("usr", username)
					.setParameter("pwd", password)
					.getSingleResult();
			em.close();
			return user;
		}catch(Exception e){
			em.close();
			logger.error("Login Failed for "+username+": "+e.getMessage());
			return null;
		}
	}

	public User getUserByEmail(String email) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			User user = (User) hb.localSession.createQuery( "from User u where u.email = :email")
					.setParameter( "email", email )
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return user;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	public User getUserFromUserId(Integer userId, Set<Organization> organizations){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			User user = (User) hb.localSession.createQuery( "from User u where u.idUser = :usrId and u.defaultOrganization in (:orgs)")
					.setParameter( "usrId", userId )
					.setParameterList("orgs", organizations)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return user;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public User getUserFromUserId(Integer userId){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			User user = (User) hb.localSession.createQuery( "from User where idUser = :usrId")
					.setParameter( "usrId", userId )
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return user;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public User getUserFromUsername(String username) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			User user = (User) hb.localSession.createQuery( "from User where username = :username")
					.setParameter( "username", username )
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return user;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.info("getUserFromUsername: "+e.getMessage());
			return null;
		}
	}


	public void updateUserManagedOrganization(User admin) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			User info = hb.localSession.get( User.class, admin.getIdUser() );
			info.setManagedOrganizations(admin.getManagedOrganizations());
			hb.localSession.update(info);
			closeCommitSessionTransaction(hb);
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}
	}

	public KBVulnerabilityItem updateVulnerability(String uuid, KBVulnerabilityItem item) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		KBVulnerabilityItem info = null;
		try {
			info = (KBVulnerabilityItem) hb.localSession.createQuery("select s "+
					"from VulnerabilityKBItem s "+
			"join fetch s.md m where s.uuid = :uuid and s.status != :deprecated")
					.setParameter("uuid", uuid)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			if(null==info)
				return null;
			info.setVulnerability(item.getVulnerability());
			info.setCategory(item.getCategory());
			info.setTechnology(item.getTechnology());
			info.setStatus(item.getStatus());
			info.setFromHub(item.getFromHub());
			info.setLastUpdate(item.getLastUpdate());
			info.setIsAgnostic(item.getIsAgnostic());
			info.setKBDetailsMapping(item.getKBDetailsMapping());
			info.getMd().setText(item.getMd().getText());
			hb.localSession.update(info);
			closeCommitSessionTransaction(hb);
			return info;
		}catch(Exception e){	
			try {
				rollbackSessionTransaction(hb);
				hb = openSessionTransaction();
				hb.localSession.merge(info);
				closeCommitSessionTransaction(hb);
				return info;
			}catch(Exception e2) {
				rollbackSessionTransaction(hb);
				logger.error(e.getMessage());
				return null;
			}
		}
	}

	public Boolean updateVulnerability(KBVulnerabilityItem item) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		KBVulnerabilityItem info = null;
		try {
			info = (KBVulnerabilityItem) hb.localSession.createQuery(
					"select s "+
							"from VulnerabilityKBItem s "+
					"join fetch s.md m where s.uuid = :uuid and s.status != :deprecated")
					.setParameter("uuid", item.getUuid())
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			if(null==info)
				return false;
			info.setVulnerability(item.getVulnerability());
			info.setCategory(item.getCategory());
			info.setTechnology(item.getTechnology());
			info.setStatus(item.getStatus());
			info.setFromHub(item.getFromHub());
			info.setIsAgnostic(item.getIsAgnostic());
			info.setKBDetailsMapping(item.getKBDetailsMapping());
			info.setLastUpdate(item.getLastUpdate());
			info.getMd().setText(item.getMd().getText());
			hb.localSession.update(info);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			try {
				rollbackSessionTransaction(hb);
				hb = openSessionTransaction();
				hb.localSession.merge(info);
				closeCommitSessionTransaction(hb);
				return true;
			}catch(Exception e2) {
				rollbackSessionTransaction(hb);
				logger.error(e.getMessage());
				return false;
			}
		}
	}
	public KBTechnologyItem updateTechnology(String uuid, KBTechnologyItem stack) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			KBTechnologyItem info = (KBTechnologyItem) hb.localSession.createQuery( "from TechnologyStack where uuid = :uuid and status != :deprecated ")
					.setParameter( "uuid", uuid )
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			info.setImageUrl(stack.getImageUrl());
			info.setFromHub(stack.getFromHub());
			info.setTechnology(stack.getTechnology());
			info.setLastUpdate(stack.getLastUpdate());
			info.setStatus(stack.getStatus());
			info.setUuid(stack.getUuid());
			info.setVariant(stack.getVariant());
			info.getMd().setText(stack.getMd().getText());

			hb.localSession.update(info);

			closeCommitSessionTransaction(hb);
			return info;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Boolean updateTechnology(KBTechnologyItem stack) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			KBTechnologyItem info = (KBTechnologyItem) hb.localSession.createQuery(
					"select s "+
							"from TechnologyStack s "+
					"join fetch s.md m where s.uuid = :uuid and s.status != :deprecated")
					.setParameter("uuid", stack.getUuid())
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			if(null==info)
				return false;
			info.setImageUrl(stack.getImageUrl());
			info.setFromHub(stack.getFromHub());
			info.setTechnology(stack.getTechnology());
			info.setLastUpdate(stack.getLastUpdate());
			info.setStatus(stack.getStatus());
			info.setUuid(stack.getUuid());
			info.setVariant(stack.getVariant());
			info.getMd().setText(stack.getMd().getText());

			hb.localSession.update(info);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public Integer addHubAuthentication(HubAuthentication s){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( s );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public void removeHubAuthentication() {

		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<HubAuthentication> auth = hb.localSession.createQuery( "from HubAuthentication").getResultList();
			for(HubAuthentication a : auth) {
				hb.localSession.delete(a);
			}
			closeCommitSessionTransaction(hb);
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}
	}
	public HubAuthentication getHubAuthentication() {

		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			HubAuthentication auth =  (HubAuthentication) hb.localSession.createQuery( "from HubAuthentication")
					.setMaxResults(1)
					.setFirstResult(0)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return auth;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}


	public Integer addTechnology(KBTechnologyItem s){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( s );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	public KBTechnologyItem getTechnologyStackById(Integer idItem) {
		EntityManager em = getHibernateEntityManager();
		try{			
			KBTechnologyItem stack = (KBTechnologyItem) em.createQuery(
					"select s "+
							"from TechnologyStack s "+
					"join fetch s.md m where s.id = :stackId and s.status != :deprecated")
					.setParameter("stackId", idItem)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			em.close();
			return stack;
		}catch(Exception e){	
			em.close();
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<KBTechnologyItem> getTechnologyStacks() {

		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<KBTechnologyItem> stack =  hb.localSession.createQuery( "from TechnologyStack s where s.status != :deprecated")
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return stack;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<KBTechnologyItem>();
		}
	}

	public KBTechnologyItem getTechnologyStack(String name) {
		EntityManager em = getHibernateEntityManager();
		try{			
			KBTechnologyItem stack = (KBTechnologyItem) em.createQuery(
					"select s "+
							"from TechnologyStack s "+
					"join fetch s.md m where s.technology = :technology and s.status != :deprecated")
					.setParameter("technology", name)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			em.close();
			return stack;
		}catch(Exception e){	
			em.close();
			logger.error(e.getMessage());
			return null;
		}
	}

	public KBTechnologyItem getTechnologyStackByUUID(String uuid) {
		EntityManager em = getHibernateEntityManager();
		try{			
			KBTechnologyItem stack = (KBTechnologyItem) em.createQuery(
					"select s "+
							"from TechnologyStack s "+
					"join fetch s.md m where s.uuid = :uuid and s.status != :deprecated")
					.setParameter("uuid", uuid)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			em.close();
			return stack;
		}catch(Exception e){	
			em.close();
			logger.warn("Could not find KB "+uuid+" due to : "+e.getMessage());
			return null;
		}
	}

	public User getUserFromUsername(String username, Set<Organization> organizations){
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			User user = (User) hb.localSession.createQuery( "select u from User u "
					+ "where u.username = :username "
					+ "and u.defaultOrganization.id in (:names)")
					.setParameter( "username", username)
					.setParameterList("names", ids)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return user;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	public boolean updateGateway(Integer id, Gateway g) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Gateway o = hb.localSession.get( Gateway.class, id );
			o.setActive(g.isActive());
			o.setFqdn(g.getFqdn());
			o.setName(g.getName());
			o.setStatus(g.getStatus());
			o.setEnableImageSync(g.getEnableImageSync());
			o.setEnableScaleIn(g.getEnableScaleIn());
			o.setPlacementStrategy(g.getPlacementStrategy());
			hb.localSession.update(o);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public boolean addManagedOrganization(User usr, Integer id) {
		User user = getUserFromUserId(usr.getIdUser());
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Organization o = hb.localSession.get( Organization.class, id );
			user.getManagedOrganizations().add(o);
			hb.localSession.update(user);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}


	public Boolean addUserToTeam(User usr, Team team) {
		User user = getUserFromUserId(usr.getIdUser());
		user.setTeam(team);
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.update(user);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	public Boolean renameTeam(Integer teamId, String name) {
		Team oldTeam = getTeam(teamId);
		oldTeam.setName(name);
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			hb.localSession.update(oldTeam);
			closeCommitSessionTransaction(hb);
			return true;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	public Boolean addToTeamManager(Integer teamId, User user) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Team team = hb.localSession.get( Team.class, teamId );
		User uDb = hb.localSession.get( User.class, user.getIdUser() );
		List<User> managers = new LinkedList<User>();
		managers.addAll(team.getManagers());
		managers.add(uDb);
		team.getManagers().clear();
		team.getManagers().addAll(managers);
		try {
			hb.localSession.update(team);
			closeCommitSessionTransaction(hb);
			return true;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	public Boolean removeFromTeamManager(Integer teamId, User user) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Team team = hb.localSession.get( Team.class, teamId );
		List<User> managers = new LinkedList<User>();
		for(User u : team.getManagers()){
			if(!u.getIdUser().equals(user.getIdUser())){
				managers.add(u);
			}
		}
		team.getManagers().clear();
		team.getManagers().addAll(managers);
		try {
			hb.localSession.update(team);
			closeCommitSessionTransaction(hb);
			return true;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	public Boolean updateUserInfo(User user){
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			User oldUser = getUserFromUserId(user.getIdUser());
			oldUser.setCountry(user.getCountry());
			oldUser.setEmail(user.getEmail());
			oldUser.setFirstName(user.getFirstName());
			oldUser.setLastName(user.getLastName());
			oldUser.setEmailVerified(user.getEmailVerified());
			oldUser.setInstanceLimit(user.getInstanceLimit());
			oldUser.setForceChangePassword(user.getForceChangePassword());
			oldUser.setScore(user.getScore());
			oldUser.setPersonalDataAnonymisedDateTime(user.getPersonalDataAnonymisedDateTime());
			oldUser.setPersonalDataUpdateDateTime(user.getPersonalDataUpdateDateTime());
			oldUser.setTeam(user.getTeam());
			oldUser.setCredits(user.getCredits());
			oldUser.setUsername(user.getUsername());
			oldUser.setStatus(user.getStatus());
			oldUser.setDefaultOrganization(user.getDefaultOrganization());
			oldUser.setExercisesRun(user.getExercisesRun());
			oldUser.setRole(user.getRole());
			oldUser.setLastLogin(user.getLastLogin());
			oldUser.setManagedOrganizations(user.getManagedOrganizations());
			try {
				hb = openSessionTransaction();
				hb.localSession.update(oldUser);
				closeCommitSessionTransaction(hb);
				return true;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))					
					return false;
				if (++count == maxTries) {
					logger.error("Max Retries for updateUserInfo exceeded due to\n:"+e.getMessage());
					return false;
				}
				logger.warn("Retrying updateUserInfo attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}

	private void sleep() {
		try {Thread.sleep(200);} catch (InterruptedException e1) {};
	}

	private boolean shouldJustFail(Exception e) {
		if(e.getCause() instanceof org.hibernate.exception.ConstraintViolationException || 
				e.getCause() instanceof org.hibernate.exception.DataException || 
				e.getCause() instanceof javax.persistence.NoResultException || 
				e.getClass().equals(java.lang.IndexOutOfBoundsException.class) || 
				e.getClass().equals(org.hibernate.exception.ConstraintViolationException.class) || 
				e.getClass().equals(org.hibernate.exception.DataException.class) || 
				e.getClass().equals(javax.persistence.NoResultException.class)) 
			return true;
		return false;
	}


	public Integer updateAvailableExercise(AvailableExercise ei) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.update( ei );
			closeCommitSessionTransaction(hb);
			return ei.getId();
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Boolean updateUserPassword(Integer idUser, String newPwd) {
		String salt = RandomGeneratorUtils.getNextSalt();
		String pwd = DigestUtils.sha512Hex(newPwd.concat(salt)); 
		User user = getUserFromUserId(idUser);
		user.setSalt(salt);
		user.setPassword(pwd);
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.update(user);
			closeCommitSessionTransaction(hb);
			return true;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	public Integer addAvailableExercise(AvailableExercise exercise) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( exercise );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}



	@SuppressWarnings("unchecked")
	public List<Integer> getAllChallengesIds() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Integer> exercises = hb.localSession.createQuery("select c.idChallenge from Challenge c")
					.getResultList();
			closeCommitSessionTransaction(hb);
			return exercises;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}


	@SuppressWarnings("unchecked")
	public List<String> getAllAvailableExerciseUUIDs() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<String> exercises = hb.localSession.createQuery("select a.uuid from AvailableExercise a where a.status != :superseded and  a.status != :removed")
					.setParameter("superseded", AvailableExerciseStatus.SUPERSEDED)
					.setParameter("removed", AvailableExerciseStatus.REMOVED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return exercises;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AvailableExercise> getAllAvailableExercises(){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<AvailableExercise> exercises = hb.localSession.createQuery("from AvailableExercise a where a.status != :superseded and  a.status != :removed")
					.setParameter("superseded", AvailableExerciseStatus.SUPERSEDED)
					.setParameter("removed", AvailableExerciseStatus.REMOVED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return exercises;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<AvailableExercisesForOrganization> getAvailableExercisesForOrganization(Set<Organization> organizations){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		LinkedList<Integer> orgList = new LinkedList<Integer>();
		for(Organization o : organizations){
			orgList.add(o.getId());
		}
		try {
			List<AvailableExercisesForOrganization> exercises = hb.localSession.createQuery("from AvailableExercisesForOrganization a "
					+ "where a.organization.id in (:org)")
					.setParameterList( "org", orgList )
					.getResultList();
			closeCommitSessionTransaction(hb);
			for(AvailableExercisesForOrganization e : exercises) {
				List<AvailableExercise> tmpEx = new LinkedList<AvailableExercise>();
				for(AvailableExercise ex : e.getExercises()){
					tmpEx.add(ex);
				}
				e.setExercises(tmpEx);
			}
			return exercises;
		}catch(Exception e ) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<AvailableExercisesForOrganization>();
		}
	}

	@SuppressWarnings("unchecked")
	public Boolean isExerciseNameAvailable(String name) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<AvailableExercise> exercises = hb.localSession.createQuery("from AvailableExercise where title = :title")
					.setParameter("title", name)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return exercises.isEmpty();
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AvailableExercise> getAllAvailableTrainingExercises(Organization organization){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		LinkedList<Integer> orgList = new LinkedList<Integer>();
		orgList.add(organization.getId());
		try {
			List<AvailableExercise> exercises =  hb.localSession.createQuery("select a.exercises from AvailableExercisesForOrganization a "
					+ " where a.organization.id in (:org)")
					.setParameterList( "org", orgList )
					.getResultList();
			closeCommitSessionTransaction(hb);
			Iterator<AvailableExercise> iter = exercises.iterator();
			while (iter.hasNext()) {
				AvailableExercise ex = iter.next();
				if((!ex.getStatus().equals(AvailableExerciseStatus.AVAILABLE) && !ex.getStatus().equals(AvailableExerciseStatus.COMING_SOON)) || ex.getExerciseType().equals(AvailableExerciseType.CHALLENGE)) {
					iter.remove();              
				}
			}
			return exercises;
		}catch(Exception e ) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<AvailableExercise>();
		}
	}



	public AvailableExercise getAvailableExercise(String uuid) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			AvailableExercise exercise = (AvailableExercise) hb.localSession.createQuery("from AvailableExercise where "
					+ "uuid = :uuid and status = :active order by version desc")
					.setParameter( "uuid", uuid)
					.setParameter("active", AvailableExerciseStatus.AVAILABLE)
					.getResultList().get(0);
			closeCommitSessionTransaction(hb);
			return exercise;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}

	public AvailableExercise getAvailableExercise(Integer exerciseId) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			AvailableExercise exercise = hb.localSession.get( AvailableExercise.class, exerciseId );
			closeCommitSessionTransaction(hb);
			return exercise;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	public AvailableExercise getAvailableExercise(String uuid, Organization org) {
		if(isExerciseEnabledForOrganization(org.getId(),uuid)) {
			return getAvailableExercise(uuid);
		}
		return null;
	}

	public AvailableExercise getAvailableExerciseWithSolution(String uuid, Set<Organization> managedOrganizations) {
		if(isExerciseEnabledForOrganizations(managedOrganizations, uuid)) {
			return getAvailableExerciseWithSolution(uuid);
		}
		else {
			return null;
		}
	}

	public AvailableExercise getAvailableExerciseWithSolution(String uuid) {

		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			AvailableExercise ei = (AvailableExercise) hb.localSession.createQuery("from AvailableExercise ei "
					+ "join fetch ei.solution s where "
					+ "ei.uuid = :uuid and ei.status != :superseded and ei.status != :removed order by ei.version desc")
					.setParameter("uuid", uuid)
					.setParameter("superseded", AvailableExerciseStatus.SUPERSEDED)
					.setParameter("removed", AvailableExerciseStatus.REMOVED)
					.getResultList().get(0);
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}

	}


	@SuppressWarnings("unchecked")
	public List<AvailableExercise> getAllAvailableExercisesWithFlags(Set<Organization> orgs) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<AvailableExercise> exercises = hb.localSession.createQuery("from AvailableExercise a where a.status != :superseded and a.status != :removed")
					.setParameter("superseded", AvailableExerciseStatus.SUPERSEDED)
					.setParameter("removed", AvailableExerciseStatus.REMOVED)
					.getResultList();
			for(AvailableExercise exercise : exercises){
				for(Flag f : exercise.getQuestionsList()){
					Hibernate.initialize(f);
				}	
			}
			closeCommitSessionTransaction(hb);
			return exercises;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return new LinkedList<AvailableExercise>();
		}
	}
	public AvailableExercise getAvailableExerciseDetails(String uuid, Organization org) {
		if(isExerciseEnabledForOrganization(org.getId(),uuid)) {
			return getAvailableExerciseDetails(uuid);
		}
		return null;
	}
	public AvailableExercise getAvailableExerciseLightDetails(String uuid, Organization org) {
		if(isExerciseEnabledForOrganization(org.getId(),uuid)) {
			return getAvailableExerciseLightDetails(uuid);
		}
		return null;
	}

	public AvailableExercise getAvailableExerciseDetailsMgmt(String uuid, Set<Organization> managedOrganizations) {
		if(isExerciseEnabledForOrganizations(managedOrganizations,uuid))
			return getAvailableExerciseDetailsMgmt(uuid);
		else
			return null;
	}

	public AvailableExercise getAvailableExerciseDetailsMgmt(String uuid) {
		EntityManager em = getHibernateEntityManager();
		try {
			AvailableExercise e = (AvailableExercise) em.createQuery(
					"select at "+
							"from AvailableExercise at "+
							"join fetch at.information i join fetch at.solution s join fetch at.stack st join fetch st.md m "
							+ "join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm "
							+ "join fetch q.kb kkb join fetch kkb.md join fetch fq.hint h join fetch h.md "
							+ "where at.uuid = :uuid and at.status != :superseded and at.status != :removed order by at.version desc")
					.setParameter("uuid", uuid)
					.setParameter("superseded", AvailableExerciseStatus.SUPERSEDED)
					.setParameter("removed", AvailableExerciseStatus.REMOVED)
					.getResultList().get(0);
			Hibernate.initialize(e.getVotingScoreList());
			em.close();
			return e;
		}catch(Exception e){	
			logger.warn("Failed getAvailableExerciseDetailsMgmt (will retry in catch) for exercise "+uuid+" due to: "+e.getMessage());
			try {
				em = getHibernateEntityManager();
				AvailableExercise exerciseNoHint = (AvailableExercise) em.createQuery(
						"select at "+
								"from AvailableExercise at "+
								"join fetch at.information i join fetch at.solution s join fetch at.stack st join fetch st.md m "
								+ "join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm  "
								+ "where at.uuid = :uuid and at.status != :superseded and at.status != :removed order by at.version desc")
						.setParameter("uuid", uuid)
						.setParameter("superseded", AvailableExerciseStatus.SUPERSEDED)
						.setParameter("removed", AvailableExerciseStatus.REMOVED)
						.getResultList().get(0);
				Hibernate.initialize(exerciseNoHint.getVotingScoreList());
				em.close();
				return exerciseNoHint;
			}catch(Exception e2) {
				logger.warn("failed getAvailableExerciseDetailsMgmt for exercise "+uuid+" due to "+e2.getMessage());
				em.close();
				return null;
			}
		}
	}

	public AvailableExercise getAvailableExerciseDetailsMgmt(Integer id) {
		EntityManager em = getHibernateEntityManager();
		try {
			AvailableExercise e = (AvailableExercise) em.createQuery(
					"select at "+
							"from AvailableExercise at "+
							"join fetch at.information i join fetch at.solution s join fetch at.stack st join fetch st.md m "
							+ "join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm "
							+ "join fetch q.kb kkb join fetch kkb.md join fetch fq.hint h join fetch h.md "
							+ "where at.id = :id and at.status != :superseded and at.status != :removed order by at.version desc")
					.setParameter("id", id)
					.setParameter("superseded", AvailableExerciseStatus.SUPERSEDED)
					.setParameter("removed", AvailableExerciseStatus.REMOVED)
					.getResultList().get(0);
			Hibernate.initialize(e.getVotingScoreList());
			em.close();
			return e;
		}catch(Exception e){	
			logger.warn(e.getMessage());
			try {
				em = getHibernateEntityManager();
				AvailableExercise exerciseNoHint = (AvailableExercise) em.createQuery(
						"select at "+
								"from AvailableExercise at "+
								"join fetch at.information i join fetch at.solution s join fetch at.stack st join fetch st.md m "
								+ "join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm  "
								+ "where at.id = :id and at.status != :superseded and at.status != :removed order by at.version desc")
						.setParameter("id", id)
						.setParameter("superseded", AvailableExerciseStatus.SUPERSEDED)
						.setParameter("removed", AvailableExerciseStatus.REMOVED)
						.getResultList().get(0);
				Hibernate.initialize(exerciseNoHint.getVotingScoreList());
				em.close();
				return exerciseNoHint;
			}catch(Exception e2) {
				logger.error(e2.getMessage());
				em.close();
				return null;
			}
		}
	}


	public AvailableExercise getAvailableExerciseDetails(String uuid) {
		EntityManager em = getHibernateEntityManager();
		try {
			AvailableExercise e = (AvailableExercise) em.createQuery(
					"select at from AvailableExercise at "+
							"join fetch at.information i "
							+ "join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm "
							+ "where at.uuid = :uuid and at.status = :available order by at.version desc")
					.setParameter("uuid", uuid)
					.setParameter("available", AvailableExerciseStatus.AVAILABLE)
					.getResultList().get(0);
			if(null==e){
				return null;
			}
			Hibernate.initialize(e.getVotingScoreList());
			em.close();
			return e;
		}catch(Exception e){	
			em.close();
			logger.warn("getAvailableExerciseDetails exception due to: "+e.getMessage());		
			return null;
		}
	}

	public AvailableExercise getAvailableExerciseLightDetails(String uuid) {
		EntityManager em = getHibernateEntityManager();
		try {
			AvailableExercise e = (AvailableExercise) em.createQuery(
					"select at from AvailableExercise at "+
					"join fetch at.information i where at.uuid = :uuid and at.status = :available order by at.version desc")
					.setParameter("uuid", uuid)
					.setParameter("available", AvailableExerciseStatus.AVAILABLE)
					.getResultList().get(0);
			if(null==e){
				return null;
			}
			for(Flag f : e.getQuestionsList()){
				Hibernate.initialize(f);
			}	
			em.close();
			return e;
		}catch(Exception e){	
			em.close();
			logger.error(e.getMessage());		
			return null;
		}
	}



	@SuppressWarnings("unchecked")
	public List<Country> getAllCountries(){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Country> countries = hb.localSession.createQuery("from Country").getResultList();
			closeCommitSessionTransaction(hb);
			return countries;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	public Country getCountryFromCode(String code) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Country country =  (Country) hb.localSession.createQuery("from Country where shortName = :code")
					.setParameter("code", code)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return country;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<AWSSupportedRegion> getAllSupportedAWSRegions(){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<AWSSupportedRegion> gws = hb.localSession.createQuery("from SupportedAWSRegion").getResultList();
			closeCommitSessionTransaction(hb);
			return gws;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<Gateway> getAllGateways(){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<Gateway> gws = hb.localSession.createQuery("from Gateway g where g.status != :removed")
					.setParameter("removed", GatewayStatus.DEPRECATED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return gws;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<Gateway> getAllActiveGateways(){
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				List<Gateway> gws = hb.localSession.createQuery("from Gateway g where g.status != :removed and g.active is true")
						.setParameter("removed", GatewayStatus.DEPRECATED)
						.getResultList();
				closeCommitSessionTransaction(hb);
				return gws;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))					
					return new LinkedList<Gateway>();
				if (++count == maxTries) {
					logger.error("Max Retries for getAllActiveGateways exceeded due to\n:"+e.getMessage());
					return new LinkedList<Gateway>();
				}
				logger.warn("Retrying getAllActiveGateways attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}

	}

	public Integer addGateway(Gateway gw){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( gw );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	public Gateway getGatewayForRegion(Regions region) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				Gateway gw = (Gateway) hb.localSession.createQuery(
						"from Gateway g " +
						"where g.region = :reg and g.status != :removed and g.active is true")
						.setParameter( "reg", region )
						.setParameter("removed", GatewayStatus.DEPRECATED)
						.getSingleResult();
				closeCommitSessionTransaction(hb);
				return gw;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))					
					return null;
				if (++count == maxTries) {
					logger.error("Max Retries for getGatewayForRegion exceeded due to\n:"+e.getMessage());
					return null;
				}
				logger.warn("Retrying getGatewayForRegion attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}
	public Integer managementAddAchievedTrophy(UserAchievement achievedTrophy){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( achievedTrophy );
			closeCommitSessionTransaction(hb);
			return id;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<UserAchievement> getAllAchievedTropies(){
		EntityManager em = getHibernateEntityManager();
		try {
			List<UserAchievement> achievedTrophies = em.createQuery(
					"select at "+
							"from AchievedTrophy at "+
					"left join fetch at.user")
					.getResultList();
			em.close();
			return achievedTrophies;
		}catch(Exception e){	
			em.close();
			logger.error(e.getMessage());		
			return new LinkedList<UserAchievement>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<UserAchievement> getAllAchievementsForUser(Integer idUser, AchievementType type){
		EntityManager em = getHibernateEntityManager();
		try {
			List<UserAchievement> achievedTrophies = em.createQuery(
					"select at "+
							"from AchievedTrophy at "+
							"left join fetch at.user " +
					"where at.user.idUser = :idUsr and at.achievement.type= :type")
					.setParameter( "idUsr", idUser )
					.setParameter( "type", type )
					.getResultList();
			em.close();
			return achievedTrophies;
		}catch(Exception e){	
			em.close();
			logger.error(e.getMessage());		
			return new LinkedList<UserAchievement>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<UserAchievement> getAllAchievementsForUser(Integer idUser){
		EntityManager em = getHibernateEntityManager();
		try {
			List<UserAchievement> achievedTrophies = em.createQuery(
					"select at "+
							"from AchievedTrophy at "+
							"left join fetch at.user " +
					"where at.user.idUser = :idUsr")
					.setParameter( "idUsr", idUser )
					.getResultList();
			em.close();
			return achievedTrophies;
		}catch(Exception e){	
			em.close();
			logger.error(e.getMessage());		
			return new LinkedList<UserAchievement>();
		}
	}



	@SuppressWarnings("unchecked")
	public List<Trophy> getAllTrophies(){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Trophy> trophies = hb.localSession.createQuery("from Trophy" ).getResultList();
			closeCommitSessionTransaction(hb);
			return trophies;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return new LinkedList<Trophy>();
		}
	}

	public Flag getFlagWithHints(Integer idFlag) {
		EntityManager em = getHibernateEntityManager();
		try {
			Flag f = (Flag) em.createQuery(
					"select f from Flag f join fetch f.flagQuestionList fq "
							+ "join fetch fq.hint h join fetch h.md "
							+ "where f.id = :fId")
					.setParameter("fId", idFlag)
					.getSingleResult();
			em.close();
			return f;
		}catch(Exception e){	
			em.close();
			logger.error(e.getMessage());		
			return null;
		}
	}
	public Boolean deleteGateway(Integer id) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Gateway gw = (Gateway) hb.localSession.load(Gateway.class,id);
			gw.setStatus(GatewayStatus.DEPRECATED);
			gw.setActive(false);
			hb.localSession.update(gw);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return false;
		}
	}


	@SuppressWarnings("unchecked")
	public Boolean removeAvailableExercise(Integer idExercise) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			AvailableExercise ex = (AvailableExercise) hb.localSession.load(AvailableExercise.class,idExercise);
			String uuid = ex.getUuid();
			ex.setStatus(AvailableExerciseStatus.REMOVED);
			hb.localSession.update(ex);
			List<ECSTaskDefinitionForExerciseInRegion> list = (List<ECSTaskDefinitionForExerciseInRegion>) hb.localSession.createQuery(
					"select t from ECSTaskDefinitionForExerciseInRegion t join t.exercise ex where ex.uuid = :uuid")
					.setParameter( "uuid", ex.getUuid() )
					.getResultList();
			for(ECSTaskDefinitionForExerciseInRegion ecsTask: list) {
				hb.localSession.delete(ecsTask);
			}
			removeAvailableExerciseForAllOrganizations(uuid);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return false;
		}
	}
	@SuppressWarnings("unchecked")
	public Boolean deleteOrganization(Integer idOrg) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Organization o = (Organization) hb.localSession.load(Organization.class,idOrg);
			hb.localSession.delete(o);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error("Trying to remove org from users managed organizations due to:\n" +e.getMessage());		
			List<User> users = new LinkedList<User>();
			hb = openSessionTransaction();
			try {
				Organization o = (Organization) hb.localSession.load(Organization.class,idOrg);
				users = hb.localSession.createQuery("select u from User u inner join u.managedOrganizations managedOrgs "
						+ "where managedOrgs.id = :orgId ")
						.setParameter( "orgId", o.getId() )
						.getResultList();
				for(User u : users) {
					for(Organization org : u.getManagedOrganizations()) {
						if(org.getId().equals(idOrg)) {
							u.getManagedOrganizations().remove(org);
							break;
						}
					}
					hb.localSession.update(u);
				}
				logger.error("Managing users removed, trying to remove org again...");		
				hb.localSession.delete(o);
				closeCommitSessionTransaction(hb);
				logger.error("Removal was successful...");		
				return true;
			}catch(Exception innerException){	
				logger.error("Removal unsuccessful, rolling back users managed organizations due to:\n" +innerException.getMessage());		
				rollbackSessionTransaction(hb);
				hb = openSessionTransaction();
				try {
					Organization dbOrg = (Organization) hb.localSession.load(Organization.class,idOrg);
					for(User u : users) {
						u.getManagedOrganizations().add(dbOrg);
						hb.localSession.update(u);
					}			
					closeCommitSessionTransaction(hb);
					return false;
				}catch(Exception rollExp) {
					rollbackSessionTransaction(hb);
					return false;
				}
			}
		}
	}

	public Boolean deleteTeam(Team team) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Team myObject = (Team) hb.localSession.load(Team.class,team.getIdTeam());
		myObject.setManagers(null);
		try {
			hb.localSession.update(myObject);
			Team updatedTeam = (Team) hb.localSession.load(Team.class,team.getIdTeam());
			hb.localSession.delete(updatedTeam);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return false;
		}
	}
	@SuppressWarnings("unchecked")
	public List<Challenge> getAllNotStartedChallenges() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Challenge> ei = hb.localSession.createQuery("from Challenge where status = :notStarted and startDate < :now")
					.setParameter("notStarted", ChallengeStatus.NOT_STARTED)
					.setParameter("now", new Date())
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}
		catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Challenge>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<Challenge> getAllExpiredChallenges() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Challenge> ei = hb.localSession.createQuery("from Challenge c where c.status != :expired and c.endDate < :now")
					.setParameter("expired", ChallengeStatus.FINISHED)
					.setParameter("now", new Date())
					.getResultList();
			for(Challenge c : ei) {
				for(ExerciseInstance i : c.getRunExercises()) {
					Hibernate.initialize(i.getUser());
				}
				Hibernate.initialize(c.getUsers());
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}
		catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Challenge>();
		}
	}
	//TODO
	@SuppressWarnings("unchecked")
	public List<Challenge> getChallengesForUserExercise(String uuidExercise, Integer idUser) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				List<Challenge> cList = hb.localSession.createQuery("select c from Challenge as c inner join c.users as usrs inner join c.scopeExercises as ex "
						+ "where usrs.idUser = :usrId "
						+ "and ex = :uuidExercise "
						+ "and c.status != :status")
						.setParameter( "usrId", idUser )
						.setParameter( "uuidExercise", uuidExercise )
						.setParameter("status", ChallengeStatus.FINISHED)
						.getResultList();
				for(Challenge c : cList) {
					for(ExerciseInstance i : c.getRunExercises()) {
						Hibernate.initialize(i.getUser());
					}
				}
				closeCommitSessionTransaction(hb);
				return cList;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))					
					return new LinkedList<Challenge>();
				if (++count == maxTries) {
					logger.error("Max Retries for getChallengesForUserExercise exceeded due to\n:"+e.getMessage());
					return new LinkedList<Challenge>();
				}
				logger.warn("Retrying getChallengesForUserExercise attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}


	//TODO
	@SuppressWarnings("unchecked")
	public ExerciseInChallengeStatus isExerciseInChallengeForUser(String uuid, Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		List<Challenge> cList;
		boolean inChallenge = false;
		boolean alreadyRun = false;
		boolean alreadySolved = false;

		ExerciseInChallengeStatus challengeStatus = new ExerciseInChallengeStatus();

		try{
			cList = hb.localSession.createQuery("select c from Challenge c inner join c.users usrs join fetch c.runExercises where usrs.idUser = :usrId "
					+ "and c.status != :status")
					.setParameter( "usrId", idUser )
					.setParameter("status", ChallengeStatus.FINISHED)
					.getResultList();
			closeCommitSessionTransaction(hb);

			for(Challenge ei : cList){
				if(ei.getScopeExercises().contains(uuid)) {
					challengeStatus.setChallengeId(ei.getIdChallenge());
					inChallenge = true;
					for(ExerciseInstance i : ei.getRunExercises()) {
						if(i.getUser().getIdUser().equals(idUser) && i.getAvailableExercise().getUuid().equals(uuid) && !i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) && !i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
							alreadyRun = true;
							challengeStatus.setExerciseInstanceId(i.getIdExerciseInstance());
							if(i.isSolved())
								alreadySolved = true;
							break;
						}
					}
					break;
				}
			}
			challengeStatus.setExerciseAlreadySolved(alreadySolved);
			challengeStatus.setExerciseAlreadyRun(alreadyRun);
			challengeStatus.setExerciseInChallenge(inChallenge);
			challengeStatus.setExerciseUuid(uuid);

			return challengeStatus;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<Challenge> getAllChallenges(Set<Organization> organizations) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Challenge> ei = hb.localSession.createQuery("from Challenge where organization in (:names)")
					.setParameterList("names", organizations)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Challenge>();
		}
	}
	public Challenge getChallengeFromIdLight(Integer challengeId) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Challenge ei;
		try{
			ei = (Challenge) hb.localSession.createQuery("from Challenge where idChallenge = :id ")
					.setParameter( "id", challengeId )
					.getSingleResult();
			Iterator<ExerciseInstance> e = ei.getRunExercises().iterator();
			while(e.hasNext()){
				ExerciseInstance i = e.next();
				if(i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) || i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
					e.remove();
				}
				else {
					Hibernate.initialize(i.getResults());
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}

	}

	public Challenge getChallengeFromId(Integer challengeId) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Challenge ei;
		try{
			ei = (Challenge) hb.localSession.createQuery("from Challenge where idChallenge = :id ")
					.setParameter( "id", challengeId )
					.getSingleResult();
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.warn("Challenge "+challengeId+" could not be retreived due to:\n"+e.getMessage());
			return null;
		}
		closeCommitSessionTransaction(hb);
		return ei;
	}

	public Challenge getChallengeFromName(String name, Integer org) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Challenge ei;
		try{
			ei = (Challenge) hb.localSession.createQuery("from Challenge where name = :name and organization.id = :orgId ")
					.setParameter( "name", name )
					.setParameter("orgId", org)
					.getSingleResult();
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			return null;
		}
		closeCommitSessionTransaction(hb);
		return ei;
	}

	public Challenge getChallengeWithExercisesForUser(Integer idChallenge, Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Challenge ei;
		try{
			ei = (Challenge) hb.localSession.createQuery("select c from Challenge c inner join c.users usrs where usrs.idUser = :usrId and c.idChallenge = :idChallenge ")
					.setParameter( "usrId", idUser )
					.setParameter("idChallenge", idChallenge)
					.getSingleResult();
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.warn("Challenge "+idChallenge+" could not be retreived due to:\n"+e.getMessage());
			return null;
		}
		closeCommitSessionTransaction(hb);
		return ei;
	}

	public Challenge getChallengeCompleteManagement(Integer key) {
		Challenge challenge = getChallengeManagement(key);
		if(null!=challenge) {
			List<AvailableExercise> exercises = getChallengeExercises(challenge.getScopeExercises());
			HashSet<AvailableExercise> exerciseSet = new HashSet<AvailableExercise>();
			exerciseSet.addAll(exercises);
			challenge.setExerciseData(exerciseSet);	
		}
		return challenge;
	}
	public Challenge getChallengeCompleteUser(Integer key, Integer idUser) {
		Challenge challenge = getChallengeCompleteForUser(key,idUser);
		if(null!=challenge) {
			List<AvailableExercise> exercises = getChallengeExercises(challenge.getScopeExercises());
			HashSet<AvailableExercise> exerciseSet = new HashSet<AvailableExercise>();
			exerciseSet.addAll(exercises);
			challenge.setExerciseData(exerciseSet);	
		}
		return challenge;
	}

	public Challenge getChallengeCompleteForUser(Integer challengeId, Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Challenge ei;
		try{
			ei = (Challenge) hb.localSession.createQuery("select c from Challenge c inner join c.users usrs where c.idChallenge = :idChallenge and usrs.idUser = :userId")
					.setParameter("idChallenge", challengeId)
					.setParameter("userId", idUser)
					.getSingleResult();
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
		Iterator<ExerciseInstance> e = ei.getRunExercises().iterator();
		while(e.hasNext()){
			ExerciseInstance i = e.next();
			if(i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) || i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
				e.remove();
			}
			else {				
				Hibernate.initialize(i.getAvailableExercise().getQuestionsList());
				Hibernate.initialize(i.getUser());
				Hibernate.initialize(i.getResults());
			}
		}
		closeCommitSessionTransaction(hb);
		return ei;
	}

	public Challenge getChallengeManagement(Integer idChallenge) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Challenge ei;
		try{
			ei = (Challenge) hb.localSession.createQuery("from Challenge where idChallenge = :idChallenge ")
					.setParameter("idChallenge", idChallenge)
					.getSingleResult();
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.warn("Challenge "+idChallenge+" could not be retreived due to:\n"+e.getMessage());
			return null;
		}
		Iterator<ExerciseInstance> e = ei.getRunExercises().iterator();
		while(e.hasNext()){
			ExerciseInstance i = e.next();
			if(i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) || i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
				e.remove();
			}
			else {				
				Hibernate.initialize(i.getAvailableExercise().getQuestionsList());
				Hibernate.initialize(i.getUser());
				Hibernate.initialize(i.getResults());
			}
		}
		closeCommitSessionTransaction(hb);
		return ei;
	}
	public Challenge getChallengeWithExercisesAndResultsForUser(Integer idChallenge, Integer idUser) {
		Challenge ei;
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				ei = (Challenge) hb.localSession.createQuery("select c from Challenge c inner join c.users usrs where usrs.idUser = :usrId and c.idChallenge = :idChallenge ")
						.setParameter( "usrId", idUser )
						.setParameter("idChallenge", idChallenge)
						.getSingleResult();
				Iterator<ExerciseInstance> e = ei.getRunExercises().iterator();
				while(e.hasNext()){
					ExerciseInstance i = e.next();
					if(i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) || i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
						e.remove();
					}
					else {
						Hibernate.initialize(i.getResults());
					}
				}
				closeCommitSessionTransaction(hb);
				return ei;
			}catch(Exception e){	
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))					
					return null;
				if (++count == maxTries) {
					logger.error("Max Retries for getChallengeWithExercisesAndResultsForUser exceeded due to\n:"+e.getMessage());
					return null;
				}
				logger.warn("Retrying getChallengeWithExercisesAndResultsForUser attempt "+count+"/"+maxTries+" due to:"+e.getMessage());
				sleep();
			}
		}
	}
	public Challenge getChallengeWithResultsForUser(Integer idChallenge, Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Challenge ei;
		try{
			ei = (Challenge) hb.localSession.createQuery("select c from Challenge c inner join c.users usrs where usrs.idUser = :usrId and c.idChallenge = :idChallenge ")
					.setParameter( "usrId", idUser )
					.setParameter("idChallenge", idChallenge)
					.getSingleResult();
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
		Iterator<ExerciseInstance> e = ei.getRunExercises().iterator();
		while(e.hasNext()){
			ExerciseInstance i = e.next();
			if(i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) || i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
				e.remove();
			}
			else {
				Hibernate.initialize(i.getResults());
			}
		}
		closeCommitSessionTransaction(hb);
		return ei;
	}
	public Challenge getChallengeWithDetails(Integer idChallenge, Set<Organization> org) {
		LinkedList<Integer> orgList = new LinkedList<Integer>();
		for(Organization o : org){
			orgList.add(o.getId());
		}
		Challenge ei;
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ei = (Challenge) hb.localSession.createQuery("select c from Challenge c where c.idChallenge =:id and c.organization.id in :org ")
					.setParameter( "id", idChallenge )
					.setParameterList( "org", orgList )
					.getSingleResult();
			Iterator<ExerciseInstance> e = ei.getRunExercises().iterator();
			while(e.hasNext()){
				ExerciseInstance i = e.next();
				if(i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) || i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
					e.remove();
				}
				else {
					Hibernate.initialize(i.getResults());
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e1){	
			rollbackSessionTransaction(hb);
			logger.error(e1.getMessage()+" attempting to recover...");
			hb = openSessionTransaction();
			try{
				ei = (Challenge) hb.localSession.createQuery("select c from Challenge c where c.idChallenge =:id and c.organization.id in :org ")
						.setParameter( "id", idChallenge )
						.setParameterList( "org", orgList )
						.getSingleResult();
				Iterator<ExerciseInstance> e = ei.getRunExercises().iterator();
				while(e.hasNext()){
					ExerciseInstance i = e.next();
					if(i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) || i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
						e.remove();
					}
					else {
						Hibernate.initialize(i.getResults());
					}
				}
				closeCommitSessionTransaction(hb);
				return ei;
			}catch(Exception e2){	
				rollbackSessionTransaction(hb);
				logger.error("Cound not recover: "+e2.getMessage());
			}
			return null;
		}

	}
	@SuppressWarnings("unchecked")
	public List<Challenge> getAllChallengesForUser(Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		List<Challenge> cList;
		try{
			cList = hb.localSession.createQuery("select c from Challenge c inner join c.users usrs where usrs.idUser = :usrId ")
					.setParameter( "usrId", idUser )
					.getResultList();
			for(Challenge ei : cList){
				Iterator<ExerciseInstance> e = ei.getRunExercises().iterator();
				while(e.hasNext()){
					ExerciseInstance i = e.next();
					if(i.getStatus().equals(ExerciseInstanceStatus.CANCELLED) || i.getStatus().equals(ExerciseInstanceStatus.CRASHED)) {
						e.remove();
					}
					else {
						Hibernate.initialize(i.getResults());
					}
				}
			}
			closeCommitSessionTransaction(hb);
			return cList;

		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			cList = new LinkedList<Challenge>();
			logger.error(e.getMessage());
			return new LinkedList<Challenge>();
		}
	}
	
	public Boolean updateChallenge(Challenge challenge) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				hb.localSession.update( challenge );
				closeCommitSessionTransaction(hb);
				return true;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))					
					return false;
				logger.warn("Retrying updateChallenge attempt "+count+"/"+maxTries+" due to:"+e.getMessage());
				if (++count == maxTries) {
					logger.error("Max Retries for updateChallenge exceeded due to\n:"+e.getMessage());
					return false;
				}
				sleep();
			}
		}


	}
	public Boolean removeChallenge(Challenge challenge) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.delete( challenge );
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}


	public Integer addTeam(Team team) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Integer id = (Integer) hb.localSession.save( team );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<Team> getManagementAllTeams(Set<Organization> organizations) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Team> teams = hb.localSession.createQuery("from Team where organization in (:names)")
					.setParameterList("names", organizations)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return teams;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Team>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<Team> getManagementAllTeams(Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<Team> teams = hb.localSession.createQuery("select t from Team t inner join t.managers man where man.idUser =:idUsr")
					.setParameter("idUsr", idUser)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return teams;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Team>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<User> getUsersInTeamManagedBy(User sessionUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{	
			List<User> ei = hb.localSession.createQuery("select distinct(u) from User u inner join u.team.managers man where man.idUser =:idUsr")
					.setParameter("idUsr", sessionUser.getIdUser())
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<User>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<Team> getTeamsManagedBy(User sessionUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{	
			List<Team> ei = hb.localSession.createQuery("select distinct(t) from Team t inner join t.managers man where man.idUser =:idUsr")
					.setParameter("idUsr", sessionUser.getIdUser())
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Team>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsersForTeamId(Integer team, Set<Organization> organizations){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		List<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		try{	
			List<User> ei = hb.localSession.createQuery("select distinct(u) from User u "
					+ "where u.team.id = :teamName and u.defaultOrganization.id in (:names)")
					.setParameter( "teamName", team )
					.setParameterList("names", ids)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<User>();
		}
	}

	public Team getTeam(Integer idTeam) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Team aei = hb.localSession.get( Team.class, idTeam );
			closeCommitSessionTransaction(hb);
			return aei;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Team getTeam(Integer idTeam, Set<Organization> organizations){
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Team team = (Team) hb.localSession.createQuery("from Team as t where t.idTeam = :idTeam and t.organization.id in (:idOrg)")
					.setParameter( "idTeam", idTeam )
					.setParameterList( "idOrg", ids )
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return team;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	public Team getTeamFromName(String teamName, Organization organization) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Team team = (Team) hb.localSession.createQuery("from Team as t where t.name = :teamName and t.organization.id = :idOrg")

					.setParameter( "teamName", teamName )
					.setParameter( "idOrg", organization.getId() )
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return team;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.debug("Team Name Exist Check:"+e.getMessage());
			return null;
		}
	}

	public Integer addExerciseInstance(ExerciseInstance ei) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				Integer id = (Integer) hb.localSession.save( ei );
				closeCommitSessionTransaction(hb);
				if(count>0)
					logger.debug("Completed addExerciseInstance, retry worked!");
				return id;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e)) {			
					try {
						logger.warn("Retrying addExerciseInstance, rollbacked, now attempt update...");
						hb = openSessionTransaction();
						hb.localSession.update( ei );
						closeCommitSessionTransaction(hb);
						logger.debug("Completed addExerciseInstance, update worked!");
						return ei.getIdExerciseInstance();
					}catch(Exception e2) {
						rollbackSessionTransaction(hb);
						logger.debug("Retrying addExerciseInstance, save -> update DID NOT work, stopped Retries for addExerciseInstance exceeded due to it should just fail:\n:"+e2.getMessage());
						return null;
					}
				}
				logger.warn("Retrying addExerciseInstance attempt "+count+"/"+maxTries+" due to:"+e.getMessage());
				if (++count == maxTries) {
					logger.error("Max Retries for addExerciseInstance exceeded due to\n:"+e.getMessage());
					return null;
				}
				ei.setIdExerciseInstance(null);
				ei.getScore().setIdExerciseScore(null);
				ei.getResultFile().setId(null);
				sleep();
			}
		}
	}
	public Integer updateExerciseInstance(ExerciseInstance ei) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				hb.localSession.update( ei );
				closeCommitSessionTransaction(hb);
				return ei.getIdExerciseInstance();
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return null;
				logger.warn("Retrying updateExerciseInstance attempt "+count+"/"+maxTries+" due to:"+e.getMessage());
				if (++count == maxTries) {
					logger.error("Max Retries for updateExerciseInstance exceeded due to\n:"+e.getMessage());
					return null;
				}
				sleep();
			}
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getAllExercisesInstances(){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> teams = hb.localSession.createQuery("from ExerciseInstance").getResultList();
			closeCommitSessionTransaction(hb);
			return teams;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getRunningExerciseInstanceForUser(Integer id){
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
						+ "where "
						+ "ei.user.idUser = :userId "
						+ "and ei.status <= :running ")
						.setParameter( "userId",id )
						.setParameter("running", ExerciseInstanceStatus.RUNNING)
						.getResultList();				closeCommitSessionTransaction(hb);
						return ei;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return new LinkedList<ExerciseInstance>();
				if (++count == maxTries) {
					logger.error("Max Retries for getRunningExerciseInstanceForUser exceeded due to\n:"+e.getMessage());
					return new LinkedList<ExerciseInstance>();
				}
				logger.warn("Retrying getRunningExerciseInstanceForUser attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}

	public ExerciseInstance getRunningExerciseInstance(Integer idInstance){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.idExerciseInstance = :id "
					+ "and ei.status <= :running ")
					.setParameter( "id", idInstance )
					.setParameter("running", ExerciseInstanceStatus.RUNNING)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getActiveExerciseInstanceForUser(Integer id){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.user.idUser = :userId "
					+ "and "
					+ "(ei.status <= :running or ei.status = :crashed)")

					.setParameter( "userId",id )
					.setParameter("running", ExerciseInstanceStatus.RUNNING)
					.setParameter("crashed", ExerciseInstanceStatus.CRASHED)
					.getResultList();
			for(ExerciseInstance i : ei) {
				Hibernate.initialize(i.getResults());
				Hibernate.initialize(i.getUsedHints());
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	public ExerciseInstance getActiveExerciseInstanceForUserWithECSGuacExerciseAndResult(Integer userId,Integer exerciseId){
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				ExerciseInstance instance = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
						+ "where "
						+ "ei.user.idUser = :userId "
						+ "and ei.idExerciseInstance = :exerciseId "
						+ "and ei.status < 3")
						.setParameter( "userId",userId )
						.setParameter("exerciseId", exerciseId)
						.getSingleResult();
				Hibernate.initialize(instance.getAvailableExercise().getQuestionsList());
				Hibernate.initialize(instance.getGuac().getGateway());
				Hibernate.initialize(instance.getEcsInstance());
				Hibernate.initialize(instance.getResults());
				Hibernate.initialize(instance.getUser());
				Hibernate.initialize(instance.getUsedHints());
				closeCommitSessionTransaction(hb);
				return instance;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return null;
				if (++count == maxTries) {
					logger.error("Max Retries for getActiveExerciseInstanceForUserWithECSGuacExerciseAndResult exceeded due to\n:"+e.getMessage());
					return null;
				}
				logger.warn("Retrying getActiveExerciseInstanceForUserWithECSGuacExerciseAndResult attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}

	public ExerciseInstance getActiveExerciseInstanceForUserWithGuac(Integer userId, Integer exerciseId){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance instance = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.user.idUser = :userId "
					+ "and ei.idExerciseInstance = :exerciseId "
					+ "and ei.status < 3")
					.setParameter( "userId",userId )
					.setParameter("exerciseId", exerciseId)
					.getSingleResult();
			Hibernate.initialize(instance.getGuac().getGateway());
			closeCommitSessionTransaction(hb);
			return instance;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	public ExerciseInstance getActiveExerciseInstanceForUserWithAWSInstanceAndGW(Integer userId, Integer exerciseId){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance instance = (ExerciseInstance) hb.localSession.createQuery("select ei from ExerciseInstance ei "
					+ "join fetch ei.exercise at join fetch at.questionsList q join fetch q.flagQuestionList fq "
					+ "where "
					+ "ei.user.idUser = :userId "
					+ "and ei.idExerciseInstance = :exerciseId "
					+ "and ei.status < 3")
					.setParameter( "userId",userId )
					.setParameter("exerciseId", exerciseId)
					.getSingleResult();
			Hibernate.initialize(instance.getUser());
			Hibernate.initialize(instance.getResults());
			Hibernate.initialize(instance.getAvailableExercise().getQuestionsList());
			Hibernate.initialize(instance.getGuac().getGateway());
			Hibernate.initialize(instance.getEcsInstance());
			Hibernate.initialize(instance.getUsedHints());
			closeCommitSessionTransaction(hb);
			return instance;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ECSContainerTask> getRunningECSInstances() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<ECSContainerTask> ei = hb.localSession.createQuery("from ECSInstance ei "
					+ "where ei.status = :running or ei.status = :pending")
					.setParameter("running", Constants.STATUS_RUNNING)
					.setParameter("pending", Constants.STATUS_PENDING)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ECSContainerTask>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getAllRunningExerciseInstances(Set<Organization> filteredOrg) {
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : filteredOrg){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance instance "
					+ "where (instance.status <= :running or instance.status = :crashed) "
					+ "and instance.organization.id in (:names)")
					.setParameter("running", ExerciseInstanceStatus.RUNNING)
					.setParameter("crashed", ExerciseInstanceStatus.CRASHED)
					.setParameterList("names", ids)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getUser());
				if(null==ex.getUser()) {
					ex.setStatus(ExerciseInstanceStatus.CANCELLED);
					hb.localSession.update(ex);
					iteratorExercise.remove();
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getAllRunningExerciseInstancesWithAwsGWFlag() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where ei.status < 3")
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getGuac().getGateway());
				Hibernate.initialize(ex.getEcsInstance());
				Hibernate.initialize(ex.getUsedHints());
				Hibernate.initialize(ex.getResults());
				Hibernate.initialize(ex.getUser());
				for(Flag f : ex.getAvailableExercise().getQuestionsList()){
					Hibernate.initialize(f);
				}
				if(null==ex.getUser()) {
					ex.setStatus(ExerciseInstanceStatus.CANCELLED);
					hb.localSession.update(ex);
					iteratorExercise.remove();
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getManagegementPendingExerciseInstances(Set<Organization> organizations){
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance instance "
					+ "where instance.organization.id in (:names) and (instance.status = :pending or instance.status = :stopping or instance.status = :crashed)")
					.setParameter("pending", ExerciseInstanceStatus.STOPPED)
					.setParameter("stopping", ExerciseInstanceStatus.STOPPING)
					.setParameter("crashed", ExerciseInstanceStatus.CRASHED)
					.setParameterList("names", ids)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getUser());
				if(null==ex.getUser()) {
					ex.setStatus(ExerciseInstanceStatus.CANCELLED);
					hb.localSession.update(ex);
					iteratorExercise.remove();
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getManagementCancelledExerciseInstances(Set<Organization> organizations){
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance instance "
					+ "where instance.status = :cancelled "
					+ "and instance.organization.id in (:names)")
					.setParameter("cancelled",ExerciseInstanceStatus.CANCELLED)
					.setParameterList("names", ids)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getUser());
				if(null==ex.getUser()) {
					iteratorExercise.remove();
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getManagegementPendingExerciseInstances(List<User> users){
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(User u : users){
			ids.add(u.getIdUser());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance instance "
					+ "where (instance.status = :pending or instance.status = :stopping or instance.status = :crashed) "
					+ "and instance.user.idUser in (:names)")
					.setParameter("pending", ExerciseInstanceStatus.STOPPED)
					.setParameter("stopping", ExerciseInstanceStatus.STOPPING)
					.setParameter("crashed", ExerciseInstanceStatus.CRASHED)
					.setParameterList("names", ids)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getUser());
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	public ExerciseInstance getManagementExerciseInstance(Integer idExerciseInstance, Set<Organization> organizations) {
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance)  hb.localSession.createQuery("select ei from ExerciseInstance ei "
					+ "join fetch ei.exercise at join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm "
					+ "join fetch ei.usedHints uh join fetch uh.md umd "
					+ "where ei.idExerciseInstance = :instanceId "
					+ "and ei.organization.id in (:names)")
					.setParameter( "instanceId", idExerciseInstance)
					.setParameterList("names", ids)
					.getSingleResult();

			Hibernate.initialize(ei.getUser());
			for(ExerciseInstanceResult er : ei.getResults()){
				Hibernate.initialize(er);
			}
			closeCommitSessionTransaction(hb);
			return ei;

		}catch(Exception e){
			rollbackSessionTransaction(hb);
			try{
				hb = openSessionTransaction();
				ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("select ei from ExerciseInstance ei "
						+ "join fetch ei.exercise at join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm "
						+ "where ei.idExerciseInstance = :instanceId "
						+ "and ei.organization.id in (:names)")
						.setParameter( "instanceId", idExerciseInstance)
						.setParameterList("names", ids)
						.getSingleResult();

				Hibernate.initialize(ei.getUser());
				Hibernate.initialize(ei.getUsedHints());
				for(ExerciseInstanceResult er : ei.getResults()){
					Hibernate.initialize(er);
				}
				closeCommitSessionTransaction(hb);
				return ei;

			}catch(Exception e2){
				rollbackSessionTransaction(hb);
				logger.error(e2.getMessage());
				return null;
			}
		}
	}
	public ExerciseInstance getManagementExerciseInstanceWithFile(Integer idExerciseInstance, Set<Organization> organizations) {
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}		
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.idExerciseInstance = :instanceId "
					+ "and ei.organization.id in (:ids)")
					.setParameter( "instanceId", idExerciseInstance)
					.setParameterList("ids", ids)
					.getSingleResult();
			Hibernate.initialize(ei.getResultFile());
			Hibernate.initialize(ei.getUser());
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public ExerciseInstance getCompletedExerciseInstanceForUser(Integer idUser, Integer idExerciseInstance) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
						+ "join fetch ei.exercise at join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm "
						+ "join fetch ei.usedHints uh join fetch uh.md umd "
						+ "where ei.user.idUser = :userId "
						+ "and ei.idExerciseInstance = :instanceId "
						+ "and ei.status >= 3")
						.setParameter( "userId", idUser)
						.setParameter( "instanceId", idExerciseInstance)
						.getSingleResult();
				Hibernate.initialize(ei.getUsedHints());
				for(ExerciseInstanceResult er : ei.getResults())
					Hibernate.initialize(er);
				closeCommitSessionTransaction(hb);
				return ei;
			}catch(Exception e){
				rollbackSessionTransaction(hb);
				try {
					hb = openSessionTransaction();
					ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
							+ "join fetch ei.exercise at join fetch at.questionsList q join fetch q.flagQuestionList fq join fetch fq.md fm "
							+ "where ei.user.idUser = :userId "
							+ "and ei.idExerciseInstance = :instanceId "
							+ "and ei.status >= 3")
							.setParameter( "userId", idUser)
							.setParameter( "instanceId", idExerciseInstance)
							.getSingleResult();
					Hibernate.initialize(ei.getUsedHints());
					for(ExerciseInstanceResult er : ei.getResults())
						Hibernate.initialize(er);
					closeCommitSessionTransaction(hb);
					return ei;
				}catch(Exception e2) {
					rollbackSessionTransaction(hb);
					logger.error("Could not retrieve completed exercise instances for user "+idUser+"due to:\n"+e2.getMessage()+"\nClass: "+e.getClass());
					if(shouldJustFail(e))
						return null;
					if (++count == maxTries) {
						logger.error("Max Retries for getCompletedExerciseInstanceForUser exceeded due to\n:"+e.getMessage());
						return null;
					}
					logger.warn("Retrying getCompletedExerciseInstanceForUser attempt "+count+"/"+maxTries+" due to:"+e.getMessage());
				}
			}
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getManagementReviewedExerciseInstances(List<User> users) {
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(User u : users){
			ids.add(u.getIdUser());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where ei.user.idUser in (:names) and (ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified)")
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					//		.setParameter( "cancelled", ExerciseStatus.CANCELLED)
					.setParameterList("names", ids)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getManagementReviewedExerciseInstances(Set<Organization> organizations) {
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where (ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified) and ei.organization.id in (:names)")
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					//					.setParameter( "cancelled", ExerciseStatus.CANCELLED)
					.setParameterList("names", ids)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getUser());
				if(null==ex.getUser()) {
					ex.setStatus(ExerciseInstanceStatus.CANCELLED);
					hb.localSession.update(ex);
					iteratorExercise.remove();
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getReviewedExerciseInstancesWithResultsFlagsUserForStats(Set<Organization> organizations) {
		LinkedList<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where (ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified) and organization.id in (:names)")
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					.setParameterList("names", ids)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getResults());
				Hibernate.initialize(ex.getExercise().getQuestionsList().iterator().next().getCategory());
				Hibernate.initialize(ex.getUser());
				if(null==ex.getUser()) {
					ex.setStatus(ExerciseInstanceStatus.CANCELLED);
					hb.localSession.update(ex);
					iteratorExercise.remove();
				}
			}

			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getReviewedExerciseInstancesWithResultsFlagsUserForStats(List<User> users) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		List<Integer> userIds = new LinkedList<Integer>();
		for(User u : users){
			userIds.add(u.getIdUser());
		}
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where ei.user.idUser in (:names) and (ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified)")
					.setParameterList("names", userIds)
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getResults());
				Hibernate.initialize(ex.getExercise().getQuestionsList().iterator().next().getCategory());
				Hibernate.initialize(ex.getUser());
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getReviewedExerciseInstancesWithUsersForStats() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where (ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified)")
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getUser());
				if(null==ex.getUser()) {
					ex.setStatus(ExerciseInstanceStatus.CANCELLED);
					hb.localSession.update(ex);
					iteratorExercise.remove();
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getReviewedExerciseInstancesForUser(Integer userId) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where ei.user.idUser = :userId and (ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified) ")
					.setParameter( "userId", userId)
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					.getResultList();
			for(ExerciseInstance ex : ei){
				Hibernate.initialize(ex.getExercise().getQuestionsList().iterator().next().getCategory());
				for(ExerciseInstanceResult er : ex.getResults()){
					Hibernate.initialize(er);
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getReviewedExerciseInstancesWithFlagsForStats() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where (ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified) ")
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					.getResultList();
			Iterator<ExerciseInstance> iteratorExercise = ei.iterator();
			while(iteratorExercise.hasNext()) {
				ExerciseInstance ex = iteratorExercise.next();
				Hibernate.initialize(ex.getExercise().getQuestionsList().iterator().next().getCategory());
				Hibernate.initialize(ex.getUser());
				if(null==ex.getUser()) {
					ex.setStatus(ExerciseInstanceStatus.CANCELLED);
					hb.localSession.update(ex);
					iteratorExercise.remove();
				}
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}

	public ExerciseInstance getManagementCompletedExerciseInstance(Integer idExerciseInstance) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.idExerciseInstance = :instanceId "
					+ "and ei.status >= 3")
					.setParameter( "instanceId", idExerciseInstance)
					.getSingleResult();
			Hibernate.initialize(ei.getResultFile());
			Hibernate.initialize(ei.getUser());
			Hibernate.initialize(ei.getExercise().getQuestionsList());
			for(ExerciseInstanceResult er : ei.getResults()){
				Hibernate.initialize(er);
			}
			Hibernate.initialize(ei.getUsedHints());
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}


	public ExerciseInstance getManagementCompletedExerciseInstance(Integer idExerciseInstance, Set<Organization> organizations) {
		List<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.idExerciseInstance = :instanceId "
					+ "and ei.organization.id in (:ids) "
					+ "and ei.status >= 3")
					.setParameter( "instanceId", idExerciseInstance)
					.setParameterList("ids", ids)
					.getSingleResult();
			Hibernate.initialize(ei.getResultFile());
			Hibernate.initialize(ei.getUser());
			Hibernate.initialize(ei.getExercise().getQuestionsList());
			for(ExerciseInstanceResult er : ei.getResults()){
				Hibernate.initialize(er);
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getCompletedExerciseInstancesWithResultsForUser(Integer idUser,String uuid) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.exercise.uuid = :uuid and ei.user.idUser = :userId "
					+ "and "
					+ "ei.status >= 3")
					.setParameter( "uuid", uuid )
					.setParameter( "userId", idUser )
					.getResultList();
			for(ExerciseInstance e : ei) {
				Hibernate.initialize(e.getResults());
			}
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getCompletedExerciseInstancesForUser(Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.user.idUser = :userId "
					+ "and "
					+ "ei.status >= 3")
					.setParameter( "userId", idUser )
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getExerciseInstancesHistoryForUser(Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.user.idUser = :userId "
					+ "and "
					+ "(ei.status =:pending or ei.status =:stopping or ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified)")
					.setParameter( "userId", idUser )
					.setParameter( "pending", ExerciseInstanceStatus.STOPPED)
					.setParameter( "stopping", ExerciseInstanceStatus.STOPPING)
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ExerciseInstance> getCompletedExerciseInstancesPerUserForStats(Integer idUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<ExerciseInstance> ei = hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.user.idUser = :userId "
					+ "and "
					+ "(ei.status =:reviewed or ei.status =:autoreviewed or ei.status =:autoreviewed_modified) ")
					.setParameter( "userId", idUser )
					.setParameter( "reviewed", ExerciseInstanceStatus.REVIEWED)
					.setParameter( "autoreviewed", ExerciseInstanceStatus.AUTOREVIEWED)
					.setParameter( "autoreviewed_modified", ExerciseInstanceStatus.REVIEWED_MODIFIED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstance>();
		}
	}
	public ExerciseInstance getCompletedExerciseInstanceWithFileForUser(Integer idUser, Integer idExerciseInstance) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.user.idUser = :userId "
					+ "and ei.idExerciseInstance = :instanceId "
					+ "and "
					+ "ei.status >= 3")
					.setParameter( "userId", idUser)
					.setParameter( "instanceId", idExerciseInstance)
					.getSingleResult();
			Hibernate.initialize(ei.getResultFile());
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public ExerciseInstance getCompletedExerciseInstanceWithSolution(Integer idUser, Integer idExerciseInstance) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "join fetch ei.exercise ae join fetch ae.solution s where "
					+ "ei.user.idUser = :userId "
					+ "and ei.idExerciseInstance = :instanceId "
					+ "and "
					+ "ei.status >= 3")
					.setParameter( "userId", idUser)
					.setParameter( "instanceId", idExerciseInstance)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public ExerciseInstance getExerciseInstanceWithHints(Integer idExerciseInstance) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where "
					+ "ei.idExerciseInstance = :instanceId ")
					.setParameter( "instanceId", idExerciseInstance)
					.getSingleResult();
			Hibernate.initialize(ei.getUsedHints());
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Integer getFailedLoginAttemptsForUser(String username) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			UserFailedLogins ei = (UserFailedLogins) hb.localSession.createQuery("from UserFailedLogins ei "
					+ "where "
					+ "ei.username = :username ")
					.setParameter( "username", username )
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei.getCounter();
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			try {
				hb = openSessionTransaction();
				UserFailedLogins failedLogins = new UserFailedLogins();
				failedLogins.setUser(username);
				failedLogins.setCounter(0);
				failedLogins.setDate(new Date());
				hb.localSession.save(failedLogins);
				return 0;
			}catch(Exception e2) {
				rollbackSessionTransaction(hb);
				logger.warn(e.getMessage());
				return 0;
			}
		}
	}

	public Boolean setFailedLoginAttemptsForUser(String username, Integer counter) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				hb.localSession.createQuery("update UserFailedLogins ei set ei.counter = :counter " +
						" where ei.username = :username")
				.setParameter("counter", counter)
				.setParameter("username", username)
				.executeUpdate();
				closeCommitSessionTransaction(hb);
				return true;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return false;
				if (++count == maxTries) {
					logger.error("Max Retries for setFailedLoginAttemptsForUser exceeded due to\n:"+e.getMessage());
					return false;
				}
				logger.warn("Retrying setFailedLoginAttemptsForUser attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}
	public Integer addLoginEvent(UserAuthenticationEvent attempt){

		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				Integer id = (Integer) hb.localSession.save( attempt );
				closeCommitSessionTransaction(hb);
				return id;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return null;
				if (++count == maxTries) {
					logger.error("Max Retries for addLoginEvent exceeded due to\n:"+e.getMessage());
					return null;
				}
				logger.warn("Retrying addLoginEvent attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
				attempt.setIdUserAuthenticationEvent(null);
			}
		}
	}
	private UserAuthenticationEvent getAuthenticationEvent(String sessionHash){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			UserAuthenticationEvent ei = (UserAuthenticationEvent) hb.localSession.createQuery("from UserAuthenticationEvent ei "
					+ "where ei.sessionIdHash = :sessionHash ")
					.setParameter( "sessionHash", sessionHash)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	public Boolean addLogoutEvent(UserAuthenticationEvent logoutAttempt) {
		UserAuthenticationEvent loginAttempt = getAuthenticationEvent(logoutAttempt.getSessionIdHash());
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			if(null == loginAttempt){
				hb.localSession.save(logoutAttempt);
				closeCommitSessionTransaction(hb);
				return false;
			}
			Integer minutes = getElapsedMinutes(logoutAttempt.getLogoutDate(),loginAttempt.getLoginDate());
			loginAttempt.setLogoutDate(logoutAttempt.getLogoutDate());
			loginAttempt.setSessionTimeMinutes(minutes);
			hb.localSession.update(loginAttempt);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	private static Integer getElapsedMinutes(Date date1, Date date2) {
		long diffInMillies = date1.getTime() - date2.getTime();
		long diffMinutes = diffInMillies / (60 * 1000) % 60;
		return (int) diffMinutes;
	}

	@SuppressWarnings("unchecked")
	public List<ECSTaskDefinitionForExerciseInRegion> getAllTaskDefinitionsForExercise(String uuid) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ECSTaskDefinitionForExerciseInRegion> list = (List<ECSTaskDefinitionForExerciseInRegion>) hb.localSession.createQuery(
					"select t from ECSTaskDefinitionForExerciseInRegion t join t.exercise ex where ex.uuid = :uuid")
					.setParameter( "uuid", uuid )
					.getResultList();
			closeCommitSessionTransaction(hb);
			return list;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ECSTaskDefinitionForExerciseInRegion>();
		}
	}

	public ECSTaskDefinition getTaskDefinitionFromUUID(String uuid, Regions region, Organization org) {
		if(isExerciseEnabledForOrganization(org.getId(),uuid)) {
			return getTaskDefinitionFromUUID(uuid, region);
		}
		return null;
	}

	
	public ECSTaskDefinition getTaskDefinitionFromUUID(String uuid, Regions region) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				ECSTaskDefinitionForExerciseInRegion td = (ECSTaskDefinitionForExerciseInRegion) hb.localSession.createQuery("select t from ECSTaskDefinitionForExerciseInRegion t join t.exercise ex where "
						+ " t.region = :regId and ex.uuid = :uuid and t.active is true")
						.setParameter( "regId", region  )
						.setParameter( "uuid", uuid )
						.getResultList().get(0);
				closeCommitSessionTransaction(hb);
				return td.getTaskDefinition();
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return null;
				if (++count == maxTries) {
					logger.error("Max Retries for getTaskDefinitionFromUUID exceeded due to\n:"+e.getMessage());
					return null;
				}
				logger.warn("Retrying getTaskDefinitionFromUUID attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public List<ECSTaskDefinition> getTaskDefinitionsInRegion(Regions region) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ECSTaskDefinition> td = hb.localSession.createQuery("select taskDefinition from ECSTaskDefinitionForExerciseInRegion e where "
					+ " e.region = :regId and e.active is true")
					.setParameter( "regId", region )
					.getResultList();
			closeCommitSessionTransaction(hb);
			return td;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.warn("No task definitions in region "+region.getName()+" : "+e.getMessage());
			return null;
		}
	}

	public ECSTaskDefinition getTaskDefinitionForExerciseInRegion(String uuid, Regions region) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ECSTaskDefinitionForExerciseInRegion td = (ECSTaskDefinitionForExerciseInRegion) hb.localSession.createQuery("from ECSTaskDefinitionForExerciseInRegion t where "
					+ " t.region = :regId and t.exercise.uuid = :uuid and active is true")
					.setParameter( "regId", region  )
					.setParameter( "uuid", uuid )
					.getResultList().get(0);
			closeCommitSessionTransaction(hb);
			return td.getTaskDefinition();
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.warn("Exercise uuid "+uuid+" does not have task definition in region "+region.getName()+" : "+e.getMessage());
			return null;
		}
	}

	public Integer getFlagIdFromQuestionId(Integer idQuestion) {
		EntityManager em =  getHibernateEntityManager();
		Integer id = null;
		try{
			id = (Integer) em.createNativeQuery(
					"SELECT Flag_idFlag FROM flags_flagQuestions WHERE flagQuestionList_idFlagQuestion = :qst")
					.setParameter("qst", idQuestion)
					.getSingleResult();
			em.close();
			return id;
		}catch(Exception e){
			em.close();
			logger.error(e.getMessage());
			return id;
		}
	}
	public Integer addGuacTempUser(GatewayTempUser guacUser){
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				Integer id = (Integer) hb.localSession.save( guacUser );
				closeCommitSessionTransaction(hb);
				return id;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return null;
				if (++count == maxTries) {
					logger.error("Max Retries for addGuacTempUser exceeded due to\n:"+e.getMessage());
					return null;
				}
				logger.warn("Retrying addGuacTempUser attempt "+count+"/"+maxTries+" due to:"+e.getMessage());
			}
		}
	}
	public ExerciseInstanceFeedback getUserFeedbackForExercise(Integer id, Set<Organization> organizations) {
		List<Integer> ids = new LinkedList<Integer>();
		for(Organization o : organizations){
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			ExerciseInstanceFeedback fb = (ExerciseInstanceFeedback) hb.localSession.createQuery("from Feedback f "
					+ "where f.instance.idExerciseInstance = :exId and f.instance.organization.id in (:names) ")
					.setParameter( "exId", id)
					.setParameterList("names", ids)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return fb;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			return null;
		}
	}
	public Integer addUserFeedback(ExerciseInstanceFeedback feedback) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Integer id = (Integer) hb.localSession.save( feedback );
			closeCommitSessionTransaction(hb);
			return id;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Boolean cleanDataForCancelledInstance(Integer idFile, Integer idScore, List<Integer> idResults) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			ExerciseInstanceResultFile file = (ExerciseInstanceResultFile) hb.localSession.load( ExerciseInstanceResultFile.class, idFile );
			hb.localSession.delete(file);
			ExerciseInstanceScore score  = (ExerciseInstanceScore) hb.localSession.load( ExerciseInstanceScore.class, idScore );
			hb.localSession.delete(score);
			for(Integer idRes : idResults){
				ExerciseInstanceResult result =   (ExerciseInstanceResult) hb.localSession.load( ExerciseInstanceResult.class, idRes );
				hb.localSession.delete(result);
			}
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	@SuppressWarnings({ "unchecked"})
	public List<ExerciseInstanceResult> getAllVerifiedExerciseResultsForStats() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ExerciseInstanceResult> results = hb.localSession.createQuery("from ExerciseResult r where r.verified is true").getResultList();
			closeCommitSessionTransaction(hb);
			return results;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstanceResult>();
		}
	}


	@SuppressWarnings("unchecked")
	public List<Organization> getOrganizations(User user) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<Organization> results = hb.localSession.createQuery("select managedOrganizations from User u where u.idUser = :usr")
					.setParameter( "usr", user.getIdUser())
					.getResultList();
			closeCommitSessionTransaction(hb);
			return results;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Organization>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<Organization> getAllOrganizations() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<Organization> result = (List<Organization>) hb.localSession.createQuery("from Organization o")
					.getResultList();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Organization getOrganizationByName(String name) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Organization result = (Organization) hb.localSession.createQuery("from Organization o "
					+ "where o.name = :name")
					.setParameter( "name", name)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Organization getOrganizationById(Integer teamOrg) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Organization result = (Organization) hb.localSession.createQuery("from Organization o "
					+ "where o.id = :id")
					.setParameter( "id", teamOrg)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<Notification> getUnreadNotificationsForUser(User user) {
		Date date = new java.util.Date();
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<Notification> results = hb.localSession.createQuery("from Notification n "
					+ "where n.idUser = :usr "
					+ "and n.userRead is false "
					+ "and n.dateStart <= :start and dateEnd >= :end")
					.setParameter( "usr", user.getIdUser())
					.setParameter("start", date)
					.setParameter("end",date)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return results;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Notification>();
		}
	}

	@SuppressWarnings("unchecked")
	public Boolean markAllNotificationsAsRead(User user) {
		Date date = new java.util.Date();
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<Notification> results = hb.localSession.createQuery("from Notification n "
					+ "where n.idUser = :usr "
					+ "and n.userRead is false "
					+ "and n.dateStart <= :start and dateEnd >= :end")
					.setParameter( "usr", user.getIdUser())
					.setParameter("start", date)
					.setParameter("end",date)
					.getResultList();
			for(Notification n : results) {
				n.setUserRead(true);
				n.setDateRead(new Date());
				hb.localSession.update(n);
			}
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public boolean markNotificationAsRead(Integer idNotification, User user) {
		Notification n = getNotificationFromId(idNotification);
		if(null!=n && user.getIdUser().equals(n.getIdUser())){
			n.setUserRead(true);
			n.setDateRead(new Date());
			HibernateSessionTransactionWrapper hb = openSessionTransaction();
			try {
				hb.localSession.update(n);
				closeCommitSessionTransaction(hb);
				return true;
			}catch(Exception e){
				rollbackSessionTransaction(hb);
				logger.error(e.getMessage());
				return false;
			}
		}
		return false;
	}
	private Notification getNotificationFromId(Integer idNotification){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Notification result = (Notification) hb.localSession.createQuery("from Notification n "
					+ "where n.idNotification = :id ")
					.setParameter( "id", idNotification)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<User> getManagementUsersForOrganization(Organization defaultOrganization) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<User> result =  hb.localSession.createQuery("from User u "
					+ "where u.defaultOrganization.id =:id and u.role <= 2 and u.status = :active")
					.setParameter( "id", defaultOrganization.getId())
					.setParameter("active", UserStatus.ACTIVE)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<User>();
		}
	}
	@SuppressWarnings("unchecked")
	public List<User> getUsersWithoutTeam(Organization organization) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<User> result =  hb.localSession.createQuery("from User u "
					+ "where u.defaultOrganization.id =:id and u.team is null and u.status = :active")
					.setParameter( "id", organization.getId())
					.setParameter("active", UserStatus.ACTIVE)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<User>();
		}
	}
	public boolean addNotification(Notification n) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Integer id = (Integer) hb.localSession.save( n );
			closeCommitSessionTransaction(hb);
			if(null!=id)
				return true;
			return false;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	public boolean addNotificationList(List<Notification> notifications) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		boolean successful = true;
		for(Notification n : notifications){
			try{
				Integer id = (Integer) hb.localSession.save( n );
				if(null!=id)
					successful = false;
			} catch(Exception e){
				rollbackSessionTransaction(hb);
				logger.error(e.getMessage());
				successful = false;
				continue;
			}
		}
		closeCommitSessionTransaction(hb);
		return successful;
	}

	public Integer addChallenge(Challenge c) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Integer id;
		try{
			id = (Integer) hb.localSession.save( c );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Integer addOrganization(Organization o) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Integer id;
		try{
			id = (Integer) hb.localSession.save( o );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Boolean updateOrganization(Integer idOrg, Organization o) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Organization oldOrg = hb.localSession.get( Organization.class, idOrg );
			oldOrg.setName(o.getName());
			oldOrg.setEmail(o.getEmail());
			oldOrg.setMaxUsers(o.getMaxUsers());
			oldOrg.setStatus(o.getStatus());
			oldOrg.setDefaultCredits(o.getDefaultCredits());
			oldOrg.setAllowManualReview(o.getAllowManualReview());
			hb.localSession.update(oldOrg);
			closeCommitSessionTransaction(hb);
			return true;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public Integer addReservation(ExerciseInstanceReservation reservation) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				Integer id = (Integer) hb.localSession.save( reservation );
				closeCommitSessionTransaction(hb);
				if(count>0)
					logger.debug("Completed addReservation, retry worked!");
				return id;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e)) {		
					try {
						logger.warn("Retrying addReservation, rollbacked, now attempting update...");
						hb = openSessionTransaction();
						hb.localSession.update( reservation );
						closeCommitSessionTransaction(hb);
						logger.debug("Retrying COMPLETED for addReservation, update worked!");
						return reservation.getId();
					}catch(Exception e2) {
						rollbackSessionTransaction(hb);
						logger.debug("Retrying addReservation, save -> update DID NOT work, Stopped Retries for addReservation exceeded due to it should just fail:\n:"+e2.getMessage());
						return null;
					}
				}
				if (++count == maxTries) {
					logger.error("Max Retries for addReservation exceeded due to\n:"+e.getMessage());
					return null;
				}
				reservation.setId(null);
				reservation.getEcs().setId(null);
				logger.warn("Retrying addReservation attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}


	public ExerciseInstanceReservation getReservation(Integer reservationId) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				ExerciseInstanceReservation aei = hb.localSession.get( ExerciseInstanceReservation.class, reservationId );
				closeCommitSessionTransaction(hb);
				return aei;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return null;
				if (++count == maxTries) {
					logger.error("Max Retries for getReservation exceeded due to\n:"+e.getMessage());
					return null;
				}
				logger.warn("Retrying getReservation attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}
	public Boolean updateReservation(ExerciseInstanceReservation updatedReservation) {
		int count = 0;
		int maxTries = 3;
		HibernateSessionTransactionWrapper hb = null;
		while(true) {
			try {
				hb = openSessionTransaction();
				hb.localSession.update( updatedReservation );
				closeCommitSessionTransaction(hb);
				return true;
			} catch (Exception e) {
				rollbackSessionTransaction(hb);
				if(shouldJustFail(e))
					return null;
				if (++count == maxTries) {
					logger.error("Max Retries for updateReservation exceeded due to\n:"+e.getMessage());
					return false;
				}
				logger.warn("Retrying updateReservation attempt "+count+"/"+maxTries+" in 500ms. Exception: "+e.getClass().toString()+" cause "+e.getCause()+" due to:"+e.getMessage());
				sleep();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public List<ExerciseInstanceReservation> getUnfulfilledReservationsForUser(User sessionUser) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<ExerciseInstanceReservation> reservations = hb.localSession.createQuery("select r from ExerciseInstanceReservation r "
					+ "where r.user.idUser = :userId and r.fulfilled is false and r.error is false")
					.setParameter("userId", sessionUser.getIdUser())
					.getResultList();
			closeCommitSessionTransaction(hb);
			return reservations;
		}catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ExerciseInstanceReservation>();
		}
	}

	public String getExerciseUUIDFromID(Integer id) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			AvailableExercise exercise = (AvailableExercise) hb.localSession.createQuery("from AvailableExercise where "
					+ "id = :exId")
					.setParameter( "exId", id)
					.setMaxResults(1)
					.setFirstResult(0)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return exercise.getUuid();
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private boolean isExerciseEnabledForOrganizations(Set<Organization> orgs, String uuid) {
		Set<Integer> ids = new HashSet<Integer>();
		for(Organization o : orgs) {
			ids.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<AvailableExercise> exercises = hb.localSession.createQuery("select a.exercises from AvailableExercisesForOrganization a "
					+ "where a.organization.id in (:orgs)")
					.setParameterList("orgs", ids)
					.getResultList();
			closeCommitSessionTransaction(hb);
			for(AvailableExercise e : exercises) {
				if(e.getUuid().equals(uuid)) {
					return true;
				}
			}
			return false;
		}catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean isExerciseEnabledForOrganization(Integer orgId, String uuid) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<AvailableExercise> exercises = hb.localSession.createQuery("select a.exercises from AvailableExercisesForOrganization a "
					+ "where a.organization.id = :org")
					.setParameter("org", orgId)
					.getResultList();
			closeCommitSessionTransaction(hb);
			for(AvailableExercise e : exercises) {
				if(e.getUuid().equals(uuid)) {
					return true;
				}
			}
			return false;
		}catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<AvailableExercise> getAllAvailableExercisesForOrganization(Integer orgId) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<AvailableExercise> exercises = hb.localSession.createQuery("select a.exercises from AvailableExercisesForOrganization a "
					+ "where a.organization.id = :org")
					.setParameter("org", orgId)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return exercises;
		}catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<AvailableExercise>();
		}
	}

	public boolean updateExerciseEnabledForOrganization(Integer orgId, String uuid, AvailableExercise e) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			AvailableExercisesForOrganization exercises = (AvailableExercisesForOrganization) hb.localSession.createQuery("from AvailableExercisesForOrganization a "
					+ "where a.organization.id = :org")
					.setParameter("org", orgId)
					.getSingleResult();
			List<AvailableExercise> exerciseList = exercises.getExercises();
			ListIterator<AvailableExercise> iterator = exerciseList.listIterator(); 
			while (iterator.hasNext()) {
				AvailableExercise t = iterator.next();
				if(t.getUuid().equals(uuid)) {
					iterator.remove();
					iterator.add(e);
					hb.localSession.update(exercises);
					closeCommitSessionTransaction(hb);
					return true;
				}
			}
			closeCommitSessionTransaction(hb);
			return false;
		}catch(Exception x) {
			rollbackSessionTransaction(hb);
			logger.error(x.getMessage());
			return false;
		}
	}

	public Boolean addAvailableExerciseForOrganization(Organization org, AvailableExercise ex) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			AvailableExercisesForOrganization exercises = (AvailableExercisesForOrganization) hb.localSession.createQuery("select a from AvailableExercisesForOrganization a "
					+ "where a.organization.id = :org")
					.setParameter("org", org.getId())
					.getSingleResult();
			for(AvailableExercise avEx : exercises.getExercises()) {
				if(avEx.getUuid().equals(ex.getUuid())) {
					return false;
				}
			}
			exercises.getExercises().add(ex);
			hb.localSession.update(exercises);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e) {
			rollbackSessionTransaction(hb);
			AvailableExercisesForOrganization exercises = new AvailableExercisesForOrganization();
			exercises.setOrganization(org);
			List<AvailableExercise> list = new ArrayList<AvailableExercise>();
			list.add(ex);
			exercises.setExercises(list);
			try {
				hb = openSessionTransaction();
				hb.localSession.save(exercises);
				closeCommitSessionTransaction(hb);
			}catch(Exception e2) {
				rollbackSessionTransaction(hb);
				logger.error(e2.getMessage());
				return false;
			}
			logger.debug("AvailableExercisesForOrganization not present for organization "+org.getId()+" "+e.getMessage());
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	public Boolean removeAvailableExerciseForAllOrganizations(String uuid) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<AvailableExercisesForOrganization> exercises = (List<AvailableExercisesForOrganization>) hb.localSession.createQuery("select a from AvailableExercisesForOrganization a ")
					.getResultList();
			for(AvailableExercisesForOrganization avE : exercises) {
				try {
					Iterator<AvailableExercise> iter = avE.getExercises().iterator();
					while (iter.hasNext()) {
						AvailableExercise ex = iter.next();
						if(uuid.equals(ex.getUuid())) {
							iter.remove();              
						}
					}
					hb.localSession.update(avE);
				}catch(Exception e) {
					logger.warn(e.getMessage());
					continue;
				}
			}
			closeCommitSessionTransaction(hb);
			return true;
		} catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public Boolean removeAvailableExerciseForOrganization(Organization org, AvailableExercise ex) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			AvailableExercisesForOrganization exercises = (AvailableExercisesForOrganization) hb.localSession.createQuery("select a from AvailableExercisesForOrganization a "
					+ "where a.organization.id = :org")
					.setParameter("org", org.getId())
					.getSingleResult();
			if(null==exercises) {
				closeCommitSessionTransaction(hb);
				return true;
			}	
			Iterator<AvailableExercise> iterator = exercises.getExercises().iterator();
			while(iterator.hasNext()) {
				if(ex.getUuid().equals(iterator.next().getUuid())) {
					iterator.remove();
				}
			}
			hb.localSession.update(exercises);
			closeCommitSessionTransaction(hb);
			return true;
		} catch(Exception e) {
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Challenge> getAllChallengesForUsers(List<User> users) {
		HashSet<Integer> usrs = new HashSet<Integer>();
		for(User user : users) {
			usrs.add(user.getIdUser());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		List<Challenge> cList;
		try{
			cList = hb.localSession.createQuery("select distinct c from Challenge c inner join c.users usrs where usrs.idUser in (:usrIds) ")
					.setParameterList( "usrIds", usrs )
					.getResultList();
			for(Challenge ei : cList){
				for(ExerciseInstance i : ei.getRunExercises()){
					Hibernate.initialize(i.getResults());
				}
				Hibernate.initialize(ei.getUsers());
			}
			closeCommitSessionTransaction(hb);
			return cList;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Challenge>();
		}
	}
	public Integer addECSTaskDefinitionForExerciseInRegion(ECSTaskDefinitionForExerciseInRegion ecsTaskForExercise) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		Integer id;
		try{
			id = (Integer) hb.localSession.save( ecsTaskForExercise );
			closeCommitSessionTransaction(hb);
			if(ecsTaskForExercise.getActive()) {
				disableAllOtherTaskDefinitionsForExerciseInRegion(id);
			}
			return id;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked" )
	private Boolean disableAllOtherTaskDefinitionsForExerciseInRegion(Integer id) {

		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		ECSTaskDefinitionForExerciseInRegion tdEnabled;
		try {
			tdEnabled = hb.localSession.get( ECSTaskDefinitionForExerciseInRegion.class, id );
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
		try {
			List<ECSTaskDefinitionForExerciseInRegion> tds =  hb.localSession.createQuery("select td from ECSTaskDefinitionForExerciseInRegion td where "
					+ "td.active is true and td.exercise.uuid = :uuid and td.region = :tdRegion")
					.setParameter( "uuid", tdEnabled.getExercise().getUuid() )
					.setParameter( "tdRegion", tdEnabled.getRegion() )
					.getResultList();		
			for(ECSTaskDefinitionForExerciseInRegion dbTd : tds ) {
				if(!dbTd.getId().equals(id)) {
					dbTd.setActive(false);
					hb.localSession.update(dbTd);
				}
			}
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public Boolean removeTaskDefinitionInRegion(Integer idTaskDef) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			ECSTaskDefinitionForExerciseInRegion td = (ECSTaskDefinitionForExerciseInRegion) hb.localSession.createQuery("select td from ECSTaskDefinitionForExerciseInRegion td where "
					+ "td.taskDefinition.id = :tdId")
					.setParameter( "tdId", idTaskDef )
					.getSingleResult();
			if(null!=td) {
				hb.localSession.delete(td);
				closeCommitSessionTransaction(hb);
				return true;
			}
			closeCommitSessionTransaction(hb);
			return false;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public Boolean enableDisableTaskDefinitionInRegion(Integer taskDefId, Boolean active) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			ECSTaskDefinitionForExerciseInRegion td = (ECSTaskDefinitionForExerciseInRegion) hb.localSession.createQuery("select td from ECSTaskDefinitionForExerciseInRegion td where "
					+ "td.taskDefinition.id = :tdId")
					.setParameter( "tdId", taskDefId )
					.getSingleResult();
			if(null==td) {
				closeCommitSessionTransaction(hb);
				return false;
			}
			td.setActive(active);
			hb.localSession.update(td);
			closeCommitSessionTransaction(hb);
			if(active) {
				disableAllOtherTaskDefinitionsForExerciseInRegion(td.getId());
			}
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}


	@SuppressWarnings("unchecked")
	public List<ECSTaskDefinitionForExerciseInRegion> getAllTaskDefinitionForExerciseInRegion(Integer taskDefId) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<ECSTaskDefinitionForExerciseInRegion> taskDef = hb.localSession.createQuery("select t from ECSTaskDefinitionForExerciseInRegion t "
					+ "where t.taskDefinition.id = :taskId ")
					.setParameter( "taskId", taskDefId )
					.getResultList();
			closeCommitSessionTransaction(hb);
			return taskDef;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<ECSTaskDefinitionForExerciseInRegion>();
		}
	}
	public ECSTaskDefinitionForExerciseInRegion getTaskDefinitionForExerciseInRegion(String uuid,
			Integer idTaskDef) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ECSTaskDefinitionForExerciseInRegion taskDef = (ECSTaskDefinitionForExerciseInRegion) hb.localSession.createQuery("select t from ECSTaskDefinitionForExerciseInRegion t "
					+ "where t.exercise.uuid = :uuid and t.taskDefinition.id = :taskId ")
					.setParameter( "uuid", uuid )
					.setParameter( "taskId", idTaskDef )
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return taskDef;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Boolean addInvitationCode(InvitationCodeForOrganization invite) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.save( invite );
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}




	public Boolean removeInvitationCode(String code, Integer orgId) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			InvitationCodeForOrganization result = (InvitationCodeForOrganization) hb.localSession.createQuery("select u from InvitationCodeForOrganization u "
					+ "where u.code = :orgInvitationCode and u.organization.id = :orgId")
					.setParameter( "orgInvitationCode", code)
					.setParameter( "orgId", orgId)
					.getSingleResult();
			hb.localSession.delete(result);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public Boolean decreseOrganizationCodeRedeem(Organization o, String orgInvitationCode) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			InvitationCodeForOrganization result = (InvitationCodeForOrganization) hb.localSession.createQuery("select u from InvitationCodeForOrganization u "
					+ "where u.code = :orgInvitationCode")
					.setParameter( "orgInvitationCode", orgInvitationCode)
					.getSingleResult();
			result.setLeftToRedeem((result.getLeftToRedeem()-1));
			hb.localSession.update(result);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}

	public Organization getOrganizationFromInvitationCode(String orgInvitationCode) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			Organization result = (Organization) hb.localSession.createQuery("select u.organization from InvitationCodeForOrganization u "
					+ "where u.code =:code and u.leftToRedeem>0")
					.setParameter( "code", orgInvitationCode)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	public InvitationCodeForOrganization getInvitationFromCode(String orgInvitationCode) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			InvitationCodeForOrganization result = (InvitationCodeForOrganization) hb.localSession.createQuery("from InvitationCodeForOrganization u "
					+ "where u.code =:code and u.leftToRedeem>0")
					.setParameter( "code", orgInvitationCode)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<InvitationCodeForOrganization> getInvitationCodesForOrganization(Integer orgId) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<InvitationCodeForOrganization> result = hb.localSession.createQuery("select u from InvitationCodeForOrganization u "
					+ "where u.organization.id = :orgId")
					.setParameter( "orgId", orgId)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return result;
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<InvitationCodeForOrganization>();
		}
	}
	public Boolean addFargateSGForDeletion(FargateSGForDeletion sgDeletion) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.save( sgDeletion );
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}
	}
	@SuppressWarnings("unchecked")
	public List<FargateSGForDeletion> getFargateSGForDeletion() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<FargateSGForDeletion> list = hb.localSession.createQuery("from FargateSGForDeletion f").getResultList();
			closeCommitSessionTransaction(hb);
			return list;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Boolean deleteFargateSGForDeletion(String  name) {

		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			FargateSGForDeletion o = (FargateSGForDeletion) hb.localSession.load(FargateSGForDeletion.class,name);
			hb.localSession.delete(o);
			closeCommitSessionTransaction(hb);
			return true;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error("Failed to remove sg from fargateSGForDeleteion  due to:\n" +e.getMessage());	
			return false;
		}
	}
	public ExerciseInstance getExerciseInstanceByArn(String taskArn) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ExerciseInstance ei = (ExerciseInstance) hb.localSession.createQuery("from ExerciseInstance ei "
					+ "where ei.ecsInstance.taskArn = :arn")
					.setParameter( "arn",taskArn )
					.getSingleResult();
			Hibernate.initialize(ei.getUser().getIdUser());
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.warn("getExerciseInstanceByArn failed due to:\n"+e.getMessage());
			return null;
		}
	}
	public Integer addVulnerability(KBVulnerabilityItem s){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( s );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	public KBVulnerabilityItem addVulnerabilityKB(KBVulnerabilityItem s){
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( s );
			closeCommitSessionTransaction(hb);
			s.setId(id);
			return s;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	
	public KBVulnerabilityItem getNonAgnosticVulnerabilityKBItem(String vulnerability, String technology) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		KBVulnerabilityItem ei = null;
		try{
				ei = (KBVulnerabilityItem) hb.localSession.createQuery("from VulnerabilityKBItem ei "
						+ "where ei.vulnerability = :vulnerability and ei.technology = :technology and ei.isAgnostic is false and ei.status != :deprecated")
						.setParameter( "vulnerability",vulnerability )
						.setParameter( "technology",technology )
						.setParameter("deprecated", KBStatus.DEPRECATED)
						.setMaxResults(1)
						.setFirstResult(0)
						.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			closeCommitSessionTransaction(hb);
			logger.warn("Could not find vulnerability kb "+vulnerability+"/"+technology+" due to: "+e.getMessage());
			return ei;
		}
	}

	public KBVulnerabilityItem getAgnosticVulnerabilityKBItem(String vulnerability) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		KBVulnerabilityItem ei = null;
		try{
				ei = (KBVulnerabilityItem) hb.localSession.createQuery("from VulnerabilityKBItem ei "
						+ "where ei.vulnerability = :vulnerability and ei.isAgnostic is true and ei.status != :deprecated")
						.setParameter( "vulnerability",vulnerability )
						.setParameter("deprecated", KBStatus.DEPRECATED)
						.setMaxResults(1)
						.setFirstResult(0)
						.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.warn("Could not find vulnerability kb "+vulnerability+" due to: "+e.getMessage());
			return ei;
		}
	}
	public KBVulnerabilityItem getVulnerabilityKBItemByUUIDNoRecursive(String uuid) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			KBVulnerabilityItem vuln = (KBVulnerabilityItem) hb.localSession.createQuery(
					"select s "+
							"from VulnerabilityKBItem s "+
					"join fetch s.md m where s.uuid = :uuid and s.status != :deprecated")
					.setParameter("uuid", uuid)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return vuln;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.warn("Could not find vulnerability kb "+uuid+" due to: "+e.getMessage());
			return null;
		}
	}
	
	public KBVulnerabilityItem getVulnerabilityKBItemByUUID(String uuid) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			KBVulnerabilityItem vuln = (KBVulnerabilityItem) hb.localSession.createQuery(
					"select s "+
							"from VulnerabilityKBItem s "+
					"join fetch s.md m where s.uuid = :uuid and s.status != :deprecated")
					.setParameter("uuid", uuid)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			Iterator<String> it =  vuln.getKBDetailsMapping().keySet().iterator();
			while(it.hasNext()) {
				String tmpTech = it.next();
				String tmpUuid = vuln.getKBDetailsMapping().get(tmpTech).getUuid();
				if(null!=tmpUuid) {
					KBVulnerabilityItem tmpInner = getVulnerabilityKBItemByUUIDNoRecursive(tmpUuid);
					if(null!=tmpInner)
						vuln.getKBDetailsMapping().put(tmpTech, tmpInner);
				}
			}
			return vuln;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.warn("Could not find vulnerability kb "+uuid+" due to: "+e.getMessage());
			return null;
		}
	}
	public KBVulnerabilityItem getVulnerabilityKBItemByUUID(String uuid, String technology) {
		EntityManager em = getHibernateEntityManager();
		try{			
			KBVulnerabilityItem vuln = (KBVulnerabilityItem) em.createQuery(
					"select s "+
							"from VulnerabilityKBItem s "+
					"join fetch s.md m where s.uuid = :uuid and s.status != :deprecated")
					.setParameter("uuid", uuid)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			em.close();
			KBVulnerabilityItem proxyInner = vuln.getKBDetailsMapping().get(technology);
			if(null!=proxyInner && null!=proxyInner.getUuid()) {
				KBVulnerabilityItem tmpInner = getVulnerabilityKBItemByUUIDNoRecursive(proxyInner.getUuid());
				if(null!=tmpInner) {
					vuln.getKBDetailsMapping().clear();
					vuln.getKBDetailsMapping().put(technology, tmpInner);
				}
			}
			return vuln;
		}catch(Exception e){	
			em.close();
			logger.warn("Could not find vulnerability kb "+uuid+" due to: "+e.getMessage());
			return null;
		}
	}
	public KBVulnerabilityItem getVulnerabilityKBItem(Integer idItem) {
		EntityManager em = getHibernateEntityManager();
		try{			
			KBVulnerabilityItem vuln = (KBVulnerabilityItem) em.createQuery(
					"select s "+
							"from VulnerabilityKBItem s "+
					"join fetch s.md m where s.id = :vulnId and s.status != :deprecated")
					.setParameter("vulnId", idItem)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			em.close();
			return vuln;
		}catch(Exception e){	
			em.close();
			logger.warn("Could not find vulnerability kb "+idItem+" due to: "+e.getMessage());
			return null;
		}
	}
	public void updateFlagQuestion(FlagQuestion fq) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			FlagQuestion fqOld = hb.localSession.load( FlagQuestion.class, fq.getId() );
			fqOld.setSelfCheck(fq.getSelfCheck());
			hb.localSession.update(  fqOld );
			closeCommitSessionTransaction(hb);
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}
	}
	@SuppressWarnings("unchecked")
	public List<KBVulnerabilityItem> getAllVulnerabilityKbs() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<KBVulnerabilityItem> ei = hb.localSession.createQuery("from VulnerabilityKBItem s where s.status != :deprecated")
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<KBVulnerabilityItem>();
		}
	}

	public LearningPath getLearningPath(String uuid) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			LearningPath ei = (LearningPath) hb.localSession.createQuery("from LearningPath where status != :deprecated and uuid = :uuid ")
					.setParameter("deprecated", LearningPathStatus.DEPRECATED)
					.setParameter("uuid", uuid)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	public LearningPath getLearningPath(Integer id, Organization defaultOrganization) {

		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			LearningPath ei = (LearningPath) hb.localSession.createQuery("select DISTINCT l from LearningPath l join l.organizations orgs "
					+ "where l.idPath = :id and (l.status = :active or l.status = :soon) and orgs.id = :orgId ")
					.setParameter("id", id)
					.setParameter("active", LearningPathStatus.AVAILABLE)
					.setParameter("soon", LearningPathStatus.COMING_SOON)
					.setParameter("orgId", defaultOrganization.getId())
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<LearningPath> getLearningPaths(Set<Organization> organizations) {
		HashSet<Integer> idOrgs = new HashSet<Integer>();
		for(Organization o : organizations) {
			idOrgs.add(o.getId());
		}
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<LearningPath> ei = (ArrayList<LearningPath>) hb.localSession.createQuery("select DISTINCT l from LearningPath l join l.organizations orgs "
					+ "where l.status != :deprecated and orgs.id in (:idOrgs) ")
					.setParameter("deprecated", LearningPathStatus.DEPRECATED)
					.setParameterList("idOrgs", idOrgs)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new ArrayList<LearningPath>();
		}
	}
	public LearningPath getLearningPath(String name, String difficulty, String technology) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			LearningPath ei = (LearningPath) hb.localSession.createQuery("from LearningPath where name = :name and difficulty = :difficulty and technology = :technology and status != :deprecated ")
					.setParameter("name", name)
					.setParameter("difficulty", difficulty)
					.setParameter("technology", technology)
					.setParameter("deprecated", LearningPathStatus.DEPRECATED)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Integer addLearningPath(LearningPath learningPath) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( learningPath );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return null;
		}
	}
	public LearningPath getLearningPath(Integer id) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			LearningPath path = hb.localSession.get( LearningPath.class, id );
			closeCommitSessionTransaction(hb);
			return path;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	public Integer updateLearningPath(LearningPath learningPath) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.update( learningPath );
			closeCommitSessionTransaction(hb);
			return learningPath.getIdPath();
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<LearningPath> getUserLearningPaths(Organization defaultOrganization) {

		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<LearningPath> ei = (ArrayList<LearningPath>) hb.localSession.createQuery("select DISTINCT l from LearningPath l join l.organizations orgs "
					+ "where (l.status = :available or  l.status = :soon) and orgs.id = :orgid ")
					.setParameter("available", LearningPathStatus.AVAILABLE)
					.setParameter("soon", LearningPathStatus.COMING_SOON)
					.setParameter("orgid", defaultOrganization.getId())
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new ArrayList<LearningPath>();
		}
	}
	public void upateUserAchievement(UserAchievement a) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.update( a );
			closeCommitSessionTransaction(hb);
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}

	}
	public void addValidationToken(ValidationToken token) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.save( token );
			closeCommitSessionTransaction(hb);
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}

	}

	public ValidationToken getValidationToken(String token) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ValidationToken ei = (ValidationToken) hb.localSession.createQuery("from ValidationToken where token = :token")
					.setParameter("token", token)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.warn("No validation token found for "+token+" due to: "+e.getMessage());
			return null;
		}

	}
	public void removeValidationToken(ValidationToken v) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			ValidationToken result = hb.localSession.get( ValidationToken.class, v.getIdToken() );
			hb.localSession.delete(result);
			closeCommitSessionTransaction(hb);
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}

	}

	public void removeDownloadQueueItem(Integer id) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			DownloadQueueItem result = hb.localSession.get( DownloadQueueItem.class, id );
			hb.localSession.delete(result);
			closeCommitSessionTransaction(hb);
		}catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}

	}

	public void updateDownalodQueueItemStatus(DownloadQueueItem item) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {

			DownloadQueueItem queueItem = (DownloadQueueItem) hb.localSession.createQuery("from DownloadQueueItem i where i.id = :id")
					.setParameter("id", item.getId())
					.getSingleResult();
			queueItem.setStatus(item.getStatus());
			queueItem.setLastUpdate(item.getLastUpdate());
			hb.localSession.update( queueItem );
			closeCommitSessionTransaction(hb);
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}

	}

	public void addDownalodQueueItem(DownloadQueueItem item) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			hb.localSession.save( item );
			closeCommitSessionTransaction(hb);
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	public List<DownloadQueueItem> getPendingDownloadQueueItems() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<DownloadQueueItem> ei = (List<DownloadQueueItem>) hb.localSession.createQuery("from DownloadQueueItem i where i.status = :received")
					.setParameter("received", DownloadQueueItemStatus.RECEIVED)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<DownloadQueueItem>();
		}

	}

	@SuppressWarnings("unchecked")
	public List<DownloadQueueItem> getDownalodQueueItems() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<DownloadQueueItem> ei = (List<DownloadQueueItem>) hb.localSession.createQuery("from DownloadQueueItem i where i.status != :complete")
					.setParameter("complete", DownloadQueueItemStatus.COMPLETE)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<DownloadQueueItem>();
		}

	}

	public DownloadQueueItem getDownalodQueueItem(Integer id) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			DownloadQueueItem ei = (DownloadQueueItem) hb.localSession.createQuery("from DownloadQueueItem i where i.id = :id")
					.setParameter("id", id)
					.getSingleResult();
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}

	public Integer addTelemetry(ExerciseInstanceTelemetry telemetry) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( telemetry );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	public List<AvailableExercise> getChallengeExercises(Set<String> scopeExercises) {
		if(scopeExercises.isEmpty())
			return new LinkedList<AvailableExercise>();
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try{
			List<AvailableExercise> exercises =  hb.localSession.createQuery("select a from AvailableExercise a where "
					+ " a.status = :active and a.uuid in (:uuids)")
					.setParameter("active", AvailableExerciseStatus.AVAILABLE)
					.setParameterList( "uuids", scopeExercises)
					.getResultList();
			for(AvailableExercise e : exercises) {
				Hibernate.initialize(e.getQuestionsList());
			}
			closeCommitSessionTransaction(hb);
			return exercises;
		}catch(Exception e){	
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());		
			return new LinkedList<AvailableExercise>();
		}
	}
	public Integer addExerciseVotingScore(ExerciseInstanceVotingScore score) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( score );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean doesFrameworkExist(String framework) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Framework> f = hb.localSession.createQuery("from Framework where name = :name")
					.setParameter("name", framework)
					.getResultList();
			closeCommitSessionTransaction(hb);
			return f.size()>0;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			return false;
		}
	}
	
	public Integer addFramework(Framework f) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Integer id = (Integer) hb.localSession.save( f );
			closeCommitSessionTransaction(hb);
			return id;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			return null;
		}

	}
	public Boolean removeFramework(String name) {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			Framework ei = (Framework) hb.localSession.createQuery("from Framework where name = :name")
					.setParameter("name", name)
					.getSingleResult();
			hb.localSession.delete(ei);
			closeCommitSessionTransaction(hb);
			return true;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	public List<Framework> getAllFrameworks() {
		HibernateSessionTransactionWrapper hb = openSessionTransaction();
		try {
			List<Framework> ei = hb.localSession.createQuery("from Framework")
					.getResultList();
			closeCommitSessionTransaction(hb);
			return ei;
		} catch(Exception e){
			rollbackSessionTransaction(hb);
			logger.error(e.getMessage());
			return new LinkedList<Framework>();
		}
	}
	public KBTechnologyItem getTechnologyStack(String name,String variant) {
		EntityManager em = getHibernateEntityManager();
		try{			
			KBTechnologyItem stack = (KBTechnologyItem) em.createQuery(
					"select s from TechnologyStack s "+
					"join fetch s.md m where s.technology = :technology and s.framework = :variant"
					+ " and s.status != :deprecated")
					.setParameter("technology", name)
					.setParameter("variant", variant)
					.setParameter("deprecated", KBStatus.DEPRECATED)
					.getSingleResult();
			em.close();
			return stack;
		}catch(Exception e){	
			em.close();
			logger.error(e.getMessage());
			return null;
		}
	}


}