/*
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

!function(){var n=function(){return{restrict:"A",scope:{callback:"&ngOnload"},link:function(n,t,o){t.one("load",function(o){var c=t.length>0&&t[0].contentWindow?t[0].contentWindow.location:void 0;n.callback({contentLocation:c})})}}};n.$inject=[],n.directiveName="ngOnload",angular.module("ngOnload",[]).directive(n.directiveName,n)}();

const { Editor } = toastui;
var codeSyntaxHightlight = Editor.plugin.codeSyntaxHighlight;
var colorSyntax = Editor.plugin.colorSyntax;

var clipboardMenuOpen = false;
var gKeyboard = null;
var gMouse = null;
var gm = null;
var guac = null;
var is_chrome = navigator.vendor.toLowerCase().indexOf('google inc') != -1;
var is_newedge = navigator.userAgent.toLowerCase().indexOf('edg/') != -1;
function setupKeyboard(){
	gKeyboard.onkeydown = function (keysym) {
		if(null!=guac)
			guac.sendKeyEvent(1, keysym);
	};
	gKeyboard.onkeyup = function (keysym) {
		if(null!=guac)
			guac.sendKeyEvent(0, keysym);
	};
}
function tearDownKeyboard(){
	gKeyboard.onkeydown = null;
	gKeyboard.onkeyup = null; 
}

function randomInt(min, max){
	return Math.floor(Math.random() * (+max - +min)) + +min; 
}
String.prototype.equalsIgnoreCase = function(str)
{
	return (str != null 
			&& typeof str === 'string'
				&& this.toUpperCase() === str.toUpperCase());
}
var telemetry = {};
window.onload = function(){
	setTimeout(function(){
		if(window.performance){
			var perfData = window.performance.timing; 
			telemetry.pageLoadTime = perfData.loadEventEnd - perfData.navigationStart;
			telemetry.renderTime = perfData.domComplete - perfData.domLoading;
			telemetry.dnsLookupTime = perfData.domainLookupEnd - perfData.domainLookupStart;
			telemetry.connectionTime = perfData.connectEnd - perfData.connectStart;
			telemetry.tlsTime = 0;
			if (perfData.secureConnectionStart > 0) {
				telemetry.tlsTime = perfData.connectEnd - perfData.secureConnectionStart;
			}
			telemetry.totalTime = perfData.responseEnd - perfData.requestStart;
			telemetry.downloadTime = perfData.responseEnd - perfData.responseStart;
			telemetry.ttfb = perfData.responseStart - perfData.requestStart;
			angular.element(document.getElementById('exercise')).scope().sendTelemetry(telemetry);
		}
	},1000);
	

};
function receiveMessage(event){
	try{
		if(event.data == "ok"){
			return;
		}
		if(event.data == "close"){
			self.close();
			return;
		}
		if((typeof event.data === 'string' || event.data instanceof String) && event.data.indexOf('menuOpen')>=0){
			var val = event.data.split(":")[1];
			clipboardMenuOpen = (val == 'true');
			return;
		}
		if(event.data.type=="close"){
			if(angular.element(document.getElementById('exercise')).scope().isExpired()){
				$('#exerciseTimeoutModal').modal({
					backdrop: 'static',
					keyboard: false
				});
			}
			else{
				angular.element(document.getElementById('exercise')).scope().checkAndHandleCrashed();
			}
			return;
		}
	}catch(e){
		console.log(e)
	}
}
function isHidden(el) {
	return (el.offsetParent === null)
}
var rTimeout = null;
window.onresize = function(){
	if(null!=rTimeout)
		clearTimeout(rTimeout);
	rTimeout = setTimeout(function(){
		resizeExerciseCanvas();
	},150); 
}



function resizeExerciseCanvas(){
	if(isHidden(document.getElementById('mobileDisplay'))){
		
		var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		var pixelDensity = 2;

		width = Math.floor((width) * pixelDensity);
		height = Math.floor((height - 40) * pixelDensity);
		
		if (null!=guac && guac.getDisplay()) {
			guac.sendSize(width, height);	
		}
		
	}
	else{
		var containerElement = document.getElementById('mobileDisplay');
		var pixelDensity = 2;
		var width  = Math.floor(containerElement.clientWidth  * pixelDensity);
		var height = Math.floor(($(window).height() - 40) * pixelDensity);
		if (null!=guac && guac.getDisplay()) {
			guac.sendSize(width, height);
		}
	}
}

window.addEventListener("message", receiveMessage, false);
Number.isInteger = Number.isInteger || function(value) {
	return typeof value === 'number' && 
	isFinite(value) && 
	Math.floor(value) === value;
};
function setCookie(name,value,days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days*24*60*60*1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function cloneObj(obj){
	return JSON.parse(JSON.stringify(obj))
}
String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};
Array.prototype.remove = function(from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};
Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};
function autoplayMdVideo(selector){
	$(selector+' .mdvideo video').each(function(){
		$(this).attr('muted', true);
		$(this).attr('oncanplay',"this.muted=true");
		$(this).attr('playbackRate',"1.0");
		this.controls=true;
		this.autoplay=true;
		this.play();
	});
}
if (!Array.prototype.includes) {
	Object.defineProperty(Array.prototype, 'includes', {
		value: function(searchElement, fromIndex) {

			// 1. Let O be ? ToObject(this value).
			if (this == null) {
				throw new TypeError('"this" is null or not defined');
			}

			var o = Object(this);

			// 2. Let len be ? ToLength(? Get(O, "length")).
			var len = o.length >>> 0;

			// 3. If len is 0, return false.
			if (len === 0) {
				return false;
			}

			// 4. Let n be ? ToInteger(fromIndex).
			//    (If fromIndex is undefined, this step produces the value 0.)
			var n = fromIndex | 0;

			// 5. If n ≥ 0, then
			//  a. Let k be n.
			// 6. Else n < 0,
			//  a. Let k be len + n.
			//  b. If k < 0, let k be 0.
			var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

			function sameValueZero(x, y) {
				return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
			}

			// 7. Repeat, while k < len
			while (k < len) {
				// a. Let elementK be the result of ? Get(O, ! ToString(k)).
				// b. If SameValueZero(searchElement, elementK) is true, return true.
				// c. Increase k by 1. 
				if (sameValueZero(o[k], searchElement)) {
					return true;
				}
				k++;
			}

			// 8. Return false
			return false;
		}
	});
}
function deepCopy (arr) {
	var out = [];
	for (var i = 0, len = arr.length; i < len; i++) {
		var item = arr[i];
		var obj = {};
		for (var k in item) {
			obj[k] = item[k];
		}
		out.push(obj);
	}
	return out;
}

function replaceArrayContent(obj1, obj2){
	obj1.remove(0,(obj1.length-1));
	for(var i in obj2){
		obj1.push(obj2[i]);
	}
}
function replaceObjectContent(obj1, obj2){
	for (var key in obj1){
		if (obj1.hasOwnProperty(key)){
			delete obj1[key];
		}
	}
	for(var i in obj2){
		obj1[i] = obj2[i]
	}
}
jQuery.extend({
	deepclone: function(objThing) {

		if ( jQuery.isArray(objThing) ) {
			return jQuery.makeArray( jQuery.deepclone($(objThing)) );
		}
		return jQuery.extend(true, {}, objThing);
	},
});
function toTitleCase(str)
{
	return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
function splitValue(value, index) {
	return (value.substring(0, index) + "," + value.substring(index)).split(',');
}
_st = function(fRef, mDelay) {
	if(typeof fRef == "function") {
		var argu = Array.prototype.slice.call(arguments,2);
		var f = (function(){ fRef.apply(null, argu); });
		return setTimeout(f, mDelay);
	}
	try{
		return window.setTimeout(fRef, mDelay);
	}
	catch(err){

	}
}
var sf = angular.module('sfNg',['angular-table','ngRoute','ngAnimate','ngOnload','jlareau.pnotify']).filter('trustUrl', function ($sce) {
	return function(url) {
		return $sce.trustAsResourceUrl(url);
	};
})
var compareTo = function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {

			ngModel.$validators.compareTo = function(modelValue) {
				return modelValue == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};

};

var globalExerciseId = "";

$(document).ready(function(){

	$('li.dropdown a').on('click', function (event) {
		$(this).parent().toggleClass('open');
		if($(this).parent()[0].id == "tournamentDropdown"){
			angular.element(document.getElementById('exercise')).scope().closeSidebar();
			angular.element(document.getElementById('exercise')).scope().hideKBModal();
			angular.element(document.getElementById('exercise')).scope().checkTriggerUpdate();
			if($(this).parent().hasClass('open')){
				tearDownKeyboard();
			}
			else{
				setupKeyboard();
			}
				
		}
	});	
	
	$('body').keydown(function(e){
	    
	    if(e.which == 27){
	    	if($('#flagPanel').is(":visible") && !$('#getHintModal').is(":visible")){
	    		angular.element(document.getElementById('exercise')).scope().closeSidebar();
	    		setupKeyboard();
	    	}
	    	else if($('#kbModal').is(":visible") ||  $('#kbStackModal').is(":visible") ){
	    		angular.element(document.getElementById('exercise')).scope().hideKBModal();
	    		window.setTimeout(function(){$location.path("id="+globalExerciseId, false);},250);
	    	}
	    	else if($('#miniFlagPanel').is(":visible") ){
	    		angular.element(document.getElementById('exercise')).scope().closeMiniFlag();
	    	}
	    	else if($('#clipboardTextarea').is(":visible")){
	    		angular.element(document.getElementById('exercise')).scope().closeClipboardPanel();

	    	}
	    	
	    }
	});
	
	
	


	
	$("body").click(function(e) {
		
		if ($(e.target).closest("#mobileEmulator").length || $(e.target).closest(".fa-clipboard").length || $('#tournamentUL').is(":visible")) {
			tearDownKeyboard();
		} 
		else if($(e.target).closest('#miniFlagPanel').length>0 || $(e.target).closest('#kbModal').length>0 || $(e.target).closest('#flagPanel').length>0 || $(e.target).closest('#kbStackModal').length>0){
			tearDownKeyboard();
		}
		else if($('#clipboardTextarea').is(":visible")|| $('#exerciseStatusModal').is(":visible")){
			
		}
		else { 
			document.getElementById('synInp').focus();
			setupKeyboard();
		}
	});

})

sf.directive("compareTo", compareTo);
sf.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
	var original = $location.path;
	$location.path = function (path, reload) {
		if (reload === false) {
			var lastRoute = $route.current;
			var un = $rootScope.$on('$locationChangeSuccess', function () {
				$route.current = lastRoute;
				un();
			});
		}
		return original.apply($location, [path]);
	};
}])
sf.service('server',function($http,$rootScope,notificationService){

	var $this = this; 

	$rootScope.ctoken = "";

	this.voteExercise = function(exInstanceId,vote){
		var msg = {};
		msg.action = 'voteForExercise';   
		msg.id = exInstanceId;
		msg.vote = vote;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('voteForExercise:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	
	this.setPanelRead = function(exInstanceId,panel){
		var msg = {};
		msg.action = 'setPanelRead';   
		msg.id = exInstanceId;
		msg.panel = panel;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('setPanelRead:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	
	this.sendFeedback = function(exInstanceId,message){
		var msg = {};
		msg.action = 'addFeedback';   
		msg.id = exInstanceId;
		msg.feedback = message;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('sendFeedback:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}



	this.getCToken = function(){
		var msg = {};
		msg.action = 'getUserCToken';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.ctoken = response.data.ctoken;
			var hash = window.location.hash.replace("/","").replace("#","");
			var result = hash.split('&').reduce(function (result, item) {
				var parts = item.split('=');
				result[parts[0]] = parts[1];
				return result;
			}, {});
			if(parseInt(result["id"])){
				$rootScope.exInstanceId = result["id"];
				globalExerciseId = $rootScope.exInstanceId;
				$this.getUserProfile();
				$this.getRunningExercises();
				try{
					window.opener.close();
				}catch(e1){
					try{ 
						window.opener.postMessage("close",self.origin);
					}catch(e2){
						console.log("could not send close due to "+JSON.stringify(e2));
					}
				}

			}
			else{
				//TODO show error
			}

		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getCToken();

	this.sendTelemetryInfo = function(exerciseId,t){
		if(undefined==$rootScope.ctoken || $rootScope.ctoken == ""){
			var msg = {};
			msg.action = 'getUserCToken';

			var req = {
					method: 'POST',
					url: '/user/handler',
					data: msg,
			}
			$http(req).then(function successCallback(response) {
				$rootScope.ctoken = response.data.ctoken;
				var obj = {};
				obj.action = 'sendTelemetry';
				obj.id = exerciseId;
				obj.telemetry = t;
				var req2 = {
						method: 'POST',
						url: '/user/handler',
						data: obj,	
				}
				$http(req2).then(function successCallback(response) {}, function errorCallback(response) {
					console.log('ajax error');
				});
				return;
			});
		}
		else{
			var obj = {};
			obj.action = 'sendTelemetry';
			obj.id = exerciseId;
			obj.telemetry = t;
			var req = {
					method: 'POST',
					url: '/user/handler',
					data: obj,	
			}
			$http(req).then(function successCallback(response) {}, function errorCallback(response) {
				console.log('ajax error');
			});
		}


	}

	this.loadStackKB = function(uuid){
		$('.waitLoader').show();

		var obj = {};
		obj.action = 'getStackItem';
		obj.uuid = uuid;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('stackKBLoaded:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getRunningExercises = function(){
		var msg = {};
		msg.action = 'getRunningExercises';   

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(undefined!=response.data){
				$rootScope.$broadcast('runningExercises:updated',response.data);
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getRunningExercisesScore = function(){
		var msg = {};
		msg.action = 'getRunningExercises';   

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(undefined!=response.data){
				$rootScope.$broadcast('runningExercisesScore:updated',response.data);
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.loadVulnKB = function(uuid,technology){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'getKBItem';
		obj.uuid = uuid;
		obj.technology = technology;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('vulnKBLoaded:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getChallengeResults = function(id){

		var msg = {};
		msg.action = 'getChallengeResults';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			if(response.data.length==0)
				$rootScope.$broadcast('challengeResults:updated',null);
			else
				$rootScope.$broadcast('challengeResults:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.getChallengeDetails = function(id){

		var msg = {};
		msg.action = 'getChallengeDetails';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			if(response.data.length==0)
				$rootScope.$broadcast('challengeDetails:updated',null);
			else
				$rootScope.$broadcast('challengeDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.getExerciseDetails = function(uuid){

		var msg = {};
		msg.action = 'getExerciseDetails';
		msg.uuid = uuid;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('exerciseDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}



	this.getHintForQuestion = function(id,exId){

		var msg = {};
		msg.action = 'getHint';
		msg.id = id;
		msg.exId = exId;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hintReceived:updated',response.data);
			
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	
	this.getInstanceStatus = function(id){

		var msg = {};
		msg.action = 'getInstanceStatus';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('instanceStatusReceived:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	
	this.restoreCode = function(id){

		var msg = {};
		msg.action = 'resetInstanceCode';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			PNotify.removeAll();
			notificationService.success("Exercise code reset. Please restart the IDE.");
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getResultStatus = function(id,check){

		var msg = {};
		msg.action = 'getResultStatus';
		msg.id = id;
		if(undefined != check && null != check && "" != check)
			msg.check = check;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('resultStatusReceived:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.refreshGuacToken = function(id){

		var msg = {};
		msg.action = 'refreshGuacToken';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('tokenRefreshed:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.stopInstance = function(id){
		var msg = {};
		msg.action = 'stopExerciseInstance';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}

		$http(req).then(function successCallback(response) {
			NProgress.done();
			if(response.data.result == "success"){
				$rootScope.$broadcast('exerciseStopped:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.info("Could not stop the exercise, please try again or contact support.");
			}
		}, function errorCallback(response) {
			NProgress.done();
			console.log('ajax error');
		});

	}
	this.user = {};
	
	
	
	this.getUserProfile = function(){

		var msg = {};
		msg.action = 'getUserInfo';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceObjectContent($this.user,response.data);
			

			
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	
	

	this.checkAndHandleCrashed = function(){
		var msg = {};
		msg.action = 'getRunningExercises';   
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			var data = response.data;
			if(undefined!=data){
				for(var i in data){
					if(data.hasOwnProperty(i) && data[i].id == $rootScope.exInstanceId){
						if(data[i].status == "9" || data[i].status == "CRASHED"){
							try{
								window.opener.postMessage("backToSF",self.origin)
								setTimeout(function () {
									document.location = "/user/index.html#/running";
								},100);
								return;
							}
							catch(err){
								document.location = "/user/index.html#/running";
								return;
							}
						}
						return;
					}
				}
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}



});
sf.factory('xhrInterceptor', ['$q','$rootScope', function($q, $rootScope) {
	return {
		'request': function(config) {
			if(config.url == "/user/handler" && config.data.action !=undefined && config.data.action != "getUserCToken")
				config.data.ctoken = $rootScope.ctoken;
			return config;
		},
		'response': function(response) {
			return response;
		}
	};
}]);

sf.config(['$httpProvider', function($httpProvider) {  
	$httpProvider.interceptors.push('xhrInterceptor');
}]);

sf.controller('exercise',['$scope','server','$rootScope','$interval','$location','$filter',function($scope,server,$rootScope,$interval,$location,$filter){
	$scope.exerciseKbs = [];
	$rootScope.usedHints = [];
	$scope.exerciseHintsList = [];
	


	$scope.toggleTournamentUrl = function(){
		setTimeout(function(){
		  if($("#tournamentDropdown").hasClass('open')) {
				$location.path("id="+globalExerciseId, false);
		  }
		},650);
	}
	function getQueryVariable(query,variable) {
	    var vars = query.split('&');
	    for (var i = 0; i < vars.length; i++) {
	        var pair = vars[i].split('=');
	        if (decodeURIComponent(pair[0]) == variable) {
	            return decodeURIComponent(pair[1]);
	        }
	    }
	    console.log('Query variable %s not found', variable);
	}
	
	$rootScope.$on('$locationChangeSuccess', function(event, newUrl, oldUrl){
		
		if(undefined==$rootScope.exerciseDetails || $rootScope.exerciseDetails.flags == undefined){
			try{
			var hash = window.location.hash.replace("/","").replace("#","");
			var result = hash.split('&').reduce(function (result, item) {
				var parts = item.split('=');
				result[parts[0]] = parts[1];
				return result;
			}, {});
			if(parseInt(result["id"])){
				$location.path("id="+parseInt(result["id"]), false);
			}
			}catch(e){}
			return;
		}

		var target = newUrl.substr(newUrl.indexOf("#")).replace("#","").replace("/","");
		target = target.split("/")
		if(target[0]=="exerciseKB"){
			$location.path("id="+globalExerciseId+"&tab=start", false);
			$scope.openStackKB($rootScope.exerciseDetails, 'Start')
			return;
		}
		if(target[0]=="restore"){
			$location.path("id="+globalExerciseId, false);
			$scope.showRestoreCodeModal();
			return;
		}
		
		var tab = getQueryVariable(target[0],"tab");
		switch(tab){      
		case "flag" :
			var id = getQueryVariable(target[0],"item");
			for(var i=0;i<$rootScope.exerciseDetails.flags.length;i++){
				if($rootScope.exerciseDetails.flags[i].id == id){
					$scope.tmpPanelFlagOpen = false;
					$scope.tmpPanelFlag = {};
					$scope.openPanelForFlag($rootScope.exerciseDetails.flags[i]);
					return;
				}
			}
		case "learn" :
			var id = getQueryVariable(target[0],"item");
			
			for(var i=0;i<$scope.exerciseKbs.length;i++){
				if($scope.exerciseKbs[i].id == id){
					$scope.openKB($scope.exerciseKbs[i],$rootScope.exerciseDetails.technology,$scope.exerciseKbs[i].vulnerability);
					return;
				}
					
			}
		case "start":
			$scope.openStackKB($rootScope.exerciseDetails, 'Start')
			break;
		case "tournament":
			$scope.showTournament = true;
			server.getChallengeResults($rootScope.challengeDetails.id);
		default:
			break;
		}
	});
	$scope.showRestoreCodeModal = function(){
		$('#restoreCodeModal').modal('show');
	}
	$scope.restoreCode = function(){
		server.restoreCode(globalExerciseId);
		$('#restoreCodeModal').modal('hide');
	}
	
	$scope.navigateToLearn = function(){
		if($location.path() == "/id="+globalExerciseId+"&tab=learn&item="+$scope.exerciseKbs[0].id){
			$scope.closeKB()
		}
		else{
			$scope.showTournament = false;
			$location.path("id="+globalExerciseId+"&tab=learn&item="+$scope.exerciseKbs[0].id, false);
		}
	}
	$scope.navigateToStart = function(){
		if($location.path() == "/id="+globalExerciseId+"&tab=start")
			$scope.closeStackKB()
		else{
			$scope.showTournament = false;
			$location.path("id="+globalExerciseId+"&tab=start", false);
		}
	}
	$scope.navigateToFlag = function(item){
		
		if($location.path() == "/id="+globalExerciseId+"&tab=flag&item="+item.id){
			$scope.closePanelForFlag();
			return
		}
		if(item.flagList[0].type == "EXPLOITATION" && !$rootScope.hackUnlocked){
			return;
		}
		if(item.flagList[0].type == "REMEDIATION" && !$rootScope.fixUnlocked){
			return;
		}
		$scope.showTournament = false;

		$location.path("id="+globalExerciseId+"&tab=flag&item="+item.id, false);
	}
	$scope.navigateToTournament = function(){
		if($('#tournamentUL').is(":visible")){
			$scope.showTournament = false;
			$location.path("id="+globalExerciseId, true);
		}	
		else{
			$scope.tmpPanelFlagOpen = false;
			$scope.startOpen = false;
			$scope.learnOpen = false;
			$scope.tmpPanelFlag = {};
			window.setTimeout(function(){ window.location.hash = "id="+globalExerciseId+"&tab=tournament"; },250)
		}
	}
	
	$scope.$watch('tmpPanelFlagOpen', function() {
       if(!$scope.tmpPanelFlagOpen)
    	   $location.path("id="+globalExerciseId);
    });


	$('#kbModal').on('hidden.bs.modal', function () {
		$scope.learnOpen = false;
		$location.path("id="+globalExerciseId, false);
		setupKeyboard();
	})
	$('#kbStackModal').on('hidden.bs.modal', function () {
		$scope.startOpen = false;
		$location.path("id="+globalExerciseId, false);
		setupKeyboard();
	})
	
	$('#exerciseStatusModal').on('hidden.bs.modal', function () {
		setupKeyboard();
		$scope.doneOpen = false;
	})
	


	$rootScope.is_chrome = navigator.vendor.toLowerCase().indexOf('google inc') != -1;
	$rootScope.is_newedge = navigator.userAgent.toLowerCase().indexOf('edg/') != -1;


	$scope.checkAndHandleCrashed = function(){
		server.checkAndHandleCrashed();
	};

	$scope.sendTelemetry = function(t){
		server.sendTelemetryInfo($rootScope.exInstanceId,t)
	};

	$scope.isExpired = function(){
		if(undefined==$rootScope.exerciseEndTime || "" == $rootScope.exerciseEndTime )
			return true;
		var cSeconds = moment.utc($rootScope.exerciseEndTime).diff(moment.now(),'seconds');
		if(cSeconds<=0)
			return true;
		return false;
	}
	
	

	
	$scope.resetHashLink = function(){
		window.setTimeout(function(){$location.path("id="+globalExerciseId, false);},250);
	}

	$scope.exerciseName = "";
	$rootScope.challengeUserTableConfig = {
			itemsPerPage: 10,
			fillLastPage: false
	}

	$scope.challengeDetailsTableMasterList = [];
	$scope.challengeDetailsTableFilteredList = [];
	$scope.updateTableFilteredList = function() {
		$scope.challengeDetailsTableFilteredList = $filter("filter")($scope.challengeDetailsTableMasterList, $scope.tableQuery);
	};

	$scope.getMarkdownStatusClass = function(){
		
		
	};

	$rootScope.getDateInCurrentTimezone = function(date,format){
		return moment(date).local().format(format);
	}
	$scope.user = server.user;
	$scope.guacToken;
	$scope.resultStatusData = {};
	$scope.asNotStarted = false;
	$scope.flagIsVulnerable = false;
	$rootScope.exerciseDetails = {};	

	$rootScope.clipboardAllowed = true;
	$rootScope.isChallenge = false;

	var challengeUpdateTimer = null;
	$scope.challengeResults = {};
	$rootScope.challengeDetails = [];

	$scope.tmpPanelFlag = {}

	$scope.mdSelfCheckNotShown = function(){
		return document.querySelector('#selfCheckMarkdown-md').innerText.length == 0;
	}
	
	$scope.getIsLockedClass = function(type){
		if(type == "EXPLOITATION" && !$rootScope.hackUnlocked){
			return "lockedDetails"
		}
		if(type == "REMEDIATION" && !$rootScope.fixUnlocked){
			return "lockedDetails"
		}
	}

	$scope.getActivePanelClass = function(flag){
		var s = "";
		if(undefined == flag)
			return s;
		//if($scope.tmpPanelFlag.id == flag.id){
		//	s =  "activePanel";
		//}
		if(flag.flagList[0].type == "EXPLOITATION" && !$rootScope.hackUnlocked){
			s += " lockedPanel"
		}
		if(flag.flagList[0].type == "REMEDIATION" && !$rootScope.fixUnlocked){
			s += " lockedPanel"
		}
		return s;
	}
	$scope.flipCheck = function(selfCheckName){
		$scope.isFlipped = true;
		$scope.refreshResults(selfCheckName);
	}
	$scope.flipBack = function(){
		$scope.isFlipped = false;
	}

	$scope.unMinimizeMiniFlag = function(flag){
		$scope.openPanelForFlag(flag);
	}
	$scope.getOverflowClass = function(){
		if($scope.isMinimized)
			return 'overflowHidden';
		else
			return 'overflowScroll'
	}

	$scope.clipboardPanelOpen = false;
	
	$scope.openClipboardPanel = function(){
		angular.element(document.getElementById('exercise')).scope().closeSidebar();
		angular.element(document.getElementById('exercise')).scope().hideKBModal();
		angular.element(document.getElementById('exercise')).scope().closeMiniFlag();
		$scope.clipboardPanelOpen = true;
	}
	$scope.clipboardArea = "";
	$scope.closeClipboardPanel = function(){
		setupKeyboard();
		if($scope.clipboardArea!="" && null!=guac)
			guac.setClipboard($scope.clipboardArea);
		$scope.clipboardPanelOpen = false;
		$scope.clipboardArea = "";
	}
	$scope.closeClipboardPanelNoKeyboardSetup = function(){
		if($scope.clipboardArea!="" && null!=guac)
			guac.setClipboard($scope.clipboardArea);
		$scope.clipboardPanelOpen = false;
		$scope.clipboardArea = "";
	}
	
	$scope.toggleClipboardPanel = function(){
		if(!$scope.clipboardPanelOpen){
			$scope.openClipboardPanel();
		}
		else{ 
			$scope.closeClipboardPanel();
		}
	}
	
	$scope.isHintDisplayed = function(){
		return document.getElementById('hintMarkdown-md').innerHTML != "";
	}

	$scope.closeMiniFlag = function(){
		$scope.tmpPanelFlagOpen = false;
		$scope.miniFlagOpen = false;
		$scope.isFlipped = false;
		$scope.blurred = false;
		$scope.tmpPanelFlag = {};
		setupKeyboard();
	}
	$scope.openMiniFlag = function(flag){
		$scope.blurred = false;
		$scope.tmpPanelFlag = flag;
		loadMarkdown('instructionsMarkdownMini-md', $scope.tmpPanelFlag.flagList[0].md.text);
		$scope.tmpPanelFlagOpen = false;
		$scope.miniFlagOpen = true;
		document.querySelector('#selfCheckMarkdownMini-md').innerHTML = '';
		document.querySelector('#hintMarkdownMini-md').innerHTML = '';
		$scope.terminalMessage = "Ready...";
		$('#exerciseDropdown').removeClass('open');
		$('#tournamentDropdown').removeClass('open');
		$('#miniFlagPanel').draggable({ cancel: '.tui-editor-contents' });
		setupKeyboard();
	}
	
	$scope.getTerminalMessageClass = function(){
		if(undefined==$scope.tmpPanelFlag || undefined==$scope.tmpPanelFlag.flagList)
			return;
		if($scope.tmpPanelFlag.flagList[0].optional && $scope.tmpPanelFlag.flagList[0].skipPanelForFlag)
			return "green"
			
		for(var i in $rootScope.exerciseResults){
			if(undefined!=$scope.tmpPanelFlag.flagList[0].selfCheck && $scope.tmpPanelFlag.flagList[0].selfCheck.name == $rootScope.exerciseResults[i].name){
				return "red";
			}
		}
	}
	
	$scope.openPanelForFlag = function(flag){
		$scope.tmpPanelFlag = {};
		$scope.tmpPanelFlagOpen = false;
		$scope.miniFlagOpen = false;
		$scope.isFlipped = false;
		$scope.tmpPanelFlag = flag;
		$scope.blurred = true;
		$scope.hideKBModal();
		loadMarkdown('fq-'+$scope.tmpPanelFlag.flagList[0].id+'-md', $scope.tmpPanelFlag.flagList[0].md.text);
		$scope.tmpPanelFlag.flagList[0].panelOpened = true;
		document.querySelector('#selfCheckMarkdown-md').innerHTML = '';
		document.querySelector('#hintMarkdown-md').innerHTML = '';
		if(flag.flagList[0].type=="EXPLOITATION"){
			$scope.terminalMessage = "Follow the instructions to reproduce the attack and hack the application.";
		}
		else{
			$scope.terminalMessage = 'Follow the instructions to remediate, then click on "Test".';
		}
		$location.path("id="+globalExerciseId+"&tab=flag&item="+flag.id, false);
		$scope.tmpPanelFlagOpen = true;
		$scope.closeClipboardPanelNoKeyboardSetup();
		//tearDownKeyboard();
	}
	$rootScope.hackUnlocked = false;
	$rootScope.fixUnlocked = false;
	
	$scope.goToHack = function(){
		$rootScope.hackUnlocked = true;

		for(var i in $rootScope.exerciseDetails.flags){
			if($rootScope.exerciseDetails.flags[i].flagList[0].optional){
				$location.path("id="+globalExerciseId+"&tab=flag&item="+$rootScope.exerciseDetails.flags[i].id, false);
				return;
			}
		}
	}
	
	$scope.skipFinishAction = function(flag){
		if(flag.flagList[0].type == 'EXPLOITATION'){
			
			sessionStorage.setItem('exploit-'+globalExerciseId, true);
			$rootScope.fixUnlocked = true;
			for(var i in $rootScope.exerciseDetails.flags){
				if($rootScope.exerciseDetails.flags[i].id == flag.id){
					$rootScope.exerciseDetails.flags[i].flagList[0].panelOpened = true;
					$rootScope.exerciseDetails.flags[i].flagList[0].skipPanelForFlag = true;
				}
				if($rootScope.exerciseDetails.flags[i].flagList[0].type == "REMEDIATION")
					$location.path("id="+globalExerciseId+"&tab=flag&item="+$rootScope.exerciseDetails.flags[i].id, false);
			}
			
		}
		else{
			$scope.showExerciseStatusModal();
		}
	}
	
	
	
	
	
	$scope.openPanelForFlagWizard = function(flag){
		$scope.miniFlagOpen = false;
		$scope.isFlipped = false;
		$scope.tmpPanelFlag = flag;
		$scope.tmpPanelFlagOpen = true;
		loadMarkdownTs('fq-'+$scope.tmpPanelFlag.flagList[0].id+'-md', $scope.tmpPanelFlag.flagList[0].md.text,850);
		document.querySelector('#selfCheckMarkdown-md').innerHTML = '';
		document.querySelector('#hintMarkdown-md').innerHTML = '';
		$scope.terminalMessage = "Ready...";
	}
	$scope.blurred = false;
	var closePanelForFlagOnly = function(){
		$scope.tmpPanelFlagOpen = false;
		$scope.tmpPanelFlag = {};
		$location.path("id="+$rootScope.exInstanceId,false);
	}
	$scope.closePanelForFlag = function(){
		$scope.tmpPanelFlagOpen = false;
		$scope.tmpPanelFlag = {};
		$location.path("id="+$rootScope.exInstanceId,false);
		setupKeyboard();
	}

	$scope.closeSidebar = function(){
		$scope.tmpPanelFlagOpen = false;
		$scope.blurred = false;
		$scope.tmpPanelFlag = {};
		setupKeyboard();
	}
	
	$scope.getExerciseIdForFlag = function(usr,flag){
		var sf = getSelfCheckFromFlag(flag);
		if(sf=="")
			return "";
		var runExercises = $rootScope.challengeDetails.runExercises;
		for (var ex=0;ex<runExercises.length;ex++) {
			for(var res=0;res<runExercises[ex]["results"].length;res++){
				if (runExercises[ex]["results"][res].name==sf) {
					return runExercises[ex].id
				}
			}			
		}
	}
	function getSelfCheckFromFlag(flag){
		for(var f in $rootScope.challengeDetails.flags){
			if($rootScope.challengeDetails.flags.hasOwnProperty(f) && $rootScope.challengeDetails.flags[f].id==flag){
				for(var q in $rootScope.challengeDetails.flags[f].flagList){
					if($rootScope.challengeDetails.flags[f].flagList.hasOwnProperty(q) && $rootScope.challengeDetails.flags[f].flagList[q].selfCheckAvailable==true){
						return $rootScope.challengeDetails.flags[f].flagList[q].selfCheck.name;
					}
				}
			}
		}
		return "";
	}

	$scope.getClassPlacementInChallenge= function(usr,name){

		if($scope.challengeResults[usr]==undefined)
			return false;
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return false;
		if($scope.challengeResults[usr][sf]['firstForFlag']){
			return "goldPlacement";
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return "silverPlacement";
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return "bronzePlacement";
		}
		return "";
	}
	$scope.getPlacementInChallenge = function(usr,name){
		if($scope.challengeResults[usr]==undefined)
			return "";
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return "";

		if($scope.challengeResults[usr][sf]['firstForFlag']){
			return "1st";
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return "2nd";
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return "3rd";
		}
		return "";
	}

	$scope.isPlacedInChallenge = function(usr,name){

		if($scope.challengeResults[usr]==undefined)
			return "";
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return "";

		else if($scope.challengeResults[usr][sf]['firstForFlag']){
			return true;
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return true;
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return true;
		}
		return false;
	}


	$scope.getChallengeResultFor = function(usr,flag){
		var sf = getSelfCheckFromFlag(flag);

		if(sf=="")
			return "";
		var status = "-1";
		//loop1:
		try{
			var status = $scope.challengeResults[usr][sf].status
		}catch(e){
			status = "-1";
		}

		switch(status){
		case undefined:
			return "Not Started"
		case "-1":
			return "Not Started"
		case "1":
			return "Vulnerable"
		case "0":
			return "Not Vulnerable"
		case "3":
			return "Not Available"
		case "2":
			return "Broken Functionality"
		case "4":
			return "Not Addressed"
		case "5":
			return "Partially Remediated"
		default: return "N/A"
		};
	}
	$scope.getClassForChallengeResult = function(user,flag){
		var status = $scope.getChallengeResultFor(user,flag);

		switch(status){
		case "-1":
			return "table-light"
		case "Vulnerable":
			return "table-danger"
		case "Not Vulnerable":
			return "table-success"
		case "Broken Functionality":
			return "table-warning"
		case "Partially Remediated":
			return "table-warning"
		case "Not Addressed":
			return "table-info"
		default: return "table-light"
		};



	}


	$scope.challengeUsers  = [];

	$scope.$on('challengeResults:updated', function(event,data) {

		data.exerciseData = $rootScope.challengeDetails.exerciseData.slice(0);

		data.flags = [];
		data.theads = [];

		$scope.exercises = [];
		var tmpExercises = [];
		for(var i in data.exerciseData){
			if(data.exerciseData.hasOwnProperty(i) && data.exerciseData[i].id)
				tmpExercises.push(data.exerciseData[i])
		}
		var size = 3;
		while (tmpExercises.length > 0){
			$scope.exercises.push(tmpExercises.splice(0, size));
		}

		for (var property in data.exerciseData) {
			if (data.exerciseData.hasOwnProperty(property)) {
				for(var f in data.exerciseData[property].flags){
					if (data.exerciseData[property].flags.hasOwnProperty(f) && !data.exerciseData[property].flags[f].flagList[0].optional ) {
						var tmpObj = {};
						tmpObj.id = data.exerciseData[property].flags[f].id
						tmpObj.name = data.exerciseData[property].flags[f].title
						tmpObj.ename = data.exerciseData[property].title;
						data.theads.push(tmpObj)
						data.flags.push(data.exerciseData[property].flags[f]);
					}
				}
			}
		}

		data.userRunFlags = 0;
		var userRemediated = 0;
		data.userRunExercises = 0;
		var tmpScore = 0;
		for(var i=0;i<data.runExercises.length;i++){
			for(var j=0;j<data.runExercises[i].results.length;j++){
				if(data.runExercises[i].id==$rootScope.exInstanceId){
					tmpScore += data.runExercises[i].results[j].score;
				}
				if(data.runExercises[i].user.user==$scope.user.user && data.runExercises[i].results.length>0){
					data.userRunExercises++
				}
				if(data.runExercises[i].user.user==$scope.user.user){
					data.userRunFlags++
				}
				if(data.runExercises[i].results[j].status == "0"){
					if(data.runExercises[i].user.user==$scope.user.user){
						userRemediated++;
					}
				}
			}
		}
		if(userRemediated==0 || data.userRunFlags == 0)
			data.userRemediation = 0;
		else
			data.userRemediation = (userRemediated/data.userRunFlags) * 100;

		if(data.exerciseData.length!=0 && data.userRunExercises !=0)
			data.userCompletion = (data.userRunExercises /  data.exerciseData.length) * 100;
		else
			data.userCompletion = 0;

		$rootScope.challengeDetails = data;
		$rootScope.challengeDetails.teams = [];

		for(var u in $rootScope.challengeDetails.users){
			if ($rootScope.challengeDetails.users.hasOwnProperty(u)){
				if(undefined!=$rootScope.challengeDetails.users[u].team && $rootScope.challengeDetails.teams.indexOf($rootScope.challengeDetails.users[u].team.name)<0){
					$rootScope.challengeDetails.teams.push($rootScope.challengeDetails.users[u].team.name);
				}
				$scope.challengeResults[$rootScope.challengeDetails.users[u].user] = {}
				$rootScope.challengeDetails.users[u].challengeRunFlags = 0;
				$rootScope.challengeDetails.users[u].challengeRunExercises = 0;
				$rootScope.challengeDetails.users[u].challengeScore = 0;
				$rootScope.challengeDetails.users[u].challengeRemediatedFlags = 0;
				for(var e in $rootScope.challengeDetails.runExercises){

					if($rootScope.challengeDetails.runExercises.hasOwnProperty(e) && $rootScope.challengeDetails.runExercises[e].user.user==$rootScope.challengeDetails.users[u].user){
						$rootScope.challengeDetails.users[u].challengeRunExercises++;
						for(var r in $rootScope.challengeDetails.runExercises[e].results){
							if($rootScope.challengeDetails.runExercises[e].results.hasOwnProperty(r)){
								$scope.challengeResults[$rootScope.challengeDetails.users[u].user][$rootScope.challengeDetails.runExercises[e].results[r].name] = $rootScope.challengeDetails.runExercises[e].results[r];
								if($rootScope.challengeDetails.runExercises[e].results[r].status=="0" && Number.isInteger($rootScope.challengeDetails.runExercises[e].results[r].score)){
									$rootScope.challengeDetails.users[u].challengeScore += $rootScope.challengeDetails.runExercises[e].results[r].score;
									$rootScope.challengeDetails.users[u].challengeRemediatedFlags++;
								}

								$rootScope.challengeDetails.users[u].challengeRunFlags++;
							} 
						}
					}
				}
			}
		}
		$scope.challengeUsers = $rootScope.challengeDetails.users;
		$scope.challengeDetailsTableMasterList = $rootScope.challengeDetails.users;
		if($scope.tableQuery=="")
			$scope.challengeDetailsTableFilteredList =  $rootScope.challengeDetails.users;
		else
			$scope.challengeDetailsTableFilteredList =  $filter("filter")($scope.challengeDetailsTableMasterList, $scope.tableQuery);

		$rootScope.challengeDetails.lastRefreshed = new Date();
		$scope.refreshing = false;

		if(challengeUpdateTimer==null){
			var intervalTime = 25000 + randomInt(1000,5000);
			challengeUpdateTimer = $interval(function(){triggerChallengeUpdate($rootScope.challengeDetails.id)},intervalTime);
		}
	});

	$scope.$on('challengeDetails:updated', function(event,data) {



		data.flags = [];
		data.theads = [];

		$scope.exercises = [];
		var tmpExercises = [];
		for(var i in data.exerciseData){
			if(data.exerciseData.hasOwnProperty(i) && data.exerciseData[i].id)
				tmpExercises.push(data.exerciseData[i])
		}
		var size = 3;
		while (tmpExercises.length > 0){
			$scope.exercises.push(tmpExercises.splice(0, size));
		}

		for (var property in data.exerciseData) {
			if (data.exerciseData.hasOwnProperty(property)) {
				for(var f in data.exerciseData[property].flags){
					if (data.exerciseData[property].flags.hasOwnProperty(f) && !data.exerciseData[property].flags[f].flagList[0].optional ) {
						var tmpObj = {};
						tmpObj.id = data.exerciseData[property].flags[f].id
						tmpObj.name = data.exerciseData[property].flags[f].title
						tmpObj.ename = data.exerciseData[property].title;
						data.theads.push(tmpObj)
						data.flags.push(data.exerciseData[property].flags[f]);
					}
				}
			}
		}

		data.userRunFlags = 0;
		var userRemediated = 0;
		data.userRunExercises = 0;
		var tmpScore = 0;
		for(var i=0;i<data.runExercises.length;i++){
			for(var j=0;j<data.runExercises[i].results.length;j++){
				if(data.runExercises[i].id==$rootScope.exInstanceId){
					tmpScore += data.runExercises[i].results[j].score;
				}
				if(data.runExercises[i].user.user==$scope.user.user && data.runExercises[i].results.length>0){
					data.userRunExercises++
				}
				if(data.runExercises[i].user.user==$scope.user.user){
					data.userRunFlags++
				}
				if(data.runExercises[i].results[j].status == "0"){
					if(data.runExercises[i].user.user==$scope.user.user){
						userRemediated++;
					}
				}
			}
		}
		if(userRemediated==0 || data.userRunFlags == 0)
			data.userRemediation = 0;
		else
			data.userRemediation = (userRemediated/data.userRunFlags) * 100;

		if(data.exerciseData.length!=0 && data.userRunExercises !=0)
			data.userCompletion = (data.userRunExercises /  data.exerciseData.length) * 100;
		else
			data.userCompletion = 0;

		$rootScope.challengeDetails = data;
		$rootScope.challengeDetails.teams = [];

		for(var u in $rootScope.challengeDetails.users){
			if ($rootScope.challengeDetails.users.hasOwnProperty(u)){
				if(undefined!=$rootScope.challengeDetails.users[u].team && $rootScope.challengeDetails.teams.indexOf($rootScope.challengeDetails.users[u].team.name)<0){
					$rootScope.challengeDetails.teams.push($rootScope.challengeDetails.users[u].team.name);
				}
				$scope.challengeResults[$rootScope.challengeDetails.users[u].user] = {}
				$rootScope.challengeDetails.users[u].challengeRunFlags = 0;
				$rootScope.challengeDetails.users[u].challengeRunExercises = 0;
				$rootScope.challengeDetails.users[u].challengeScore = 0;
				$rootScope.challengeDetails.users[u].challengeRemediatedFlags = 0;
				for(var e in $rootScope.challengeDetails.runExercises){

					if($rootScope.challengeDetails.runExercises.hasOwnProperty(e) && $rootScope.challengeDetails.runExercises[e].user.user==$rootScope.challengeDetails.users[u].user){
						$rootScope.challengeDetails.users[u].challengeRunExercises++;
						for(var r in $rootScope.challengeDetails.runExercises[e].results){
							if($rootScope.challengeDetails.runExercises[e].results.hasOwnProperty(r)){
								$scope.challengeResults[$rootScope.challengeDetails.users[u].user][$rootScope.challengeDetails.runExercises[e].results[r].name] = $rootScope.challengeDetails.runExercises[e].results[r].status;
								if($rootScope.challengeDetails.runExercises[e].results[r].status=="0" && Number.isInteger($rootScope.challengeDetails.runExercises[e].results[r].score)){
									$rootScope.challengeDetails.users[u].challengeScore += $rootScope.challengeDetails.runExercises[e].results[r].score;
									$rootScope.challengeDetails.users[u].challengeRemediatedFlags++;
								}

								$rootScope.challengeDetails.users[u].challengeRunFlags++;
							} 
						}
					}
				}
			}
		}
		$scope.challengeUsers = $rootScope.challengeDetails.users;
		$scope.challengeDetailsTableMasterList = $rootScope.challengeDetails.users;
		if($scope.tableQuery=="")
			$scope.challengeDetailsTableFilteredList =  $rootScope.challengeDetails.users;
		else
			$scope.challengeDetailsTableFilteredList =  $filter("filter")($scope.challengeDetailsTableMasterList, $scope.tableQuery);


		if(challengeUpdateTimer==null){
			var intervalTime = 25000 + randomInt(1000,5000);
			challengeUpdateTimer = $interval(function(){triggerChallengeUpdate($rootScope.challengeDetails.id)},intervalTime);
		}
	});

	$scope.checkTriggerUpdate = function(){
		triggerChallengeUpdate($rootScope.challengeDetails.id);
		if(challengeUpdateTimer==null){
			var intervalTime = 25000 + randomInt(1000,5000);
			challengeUpdateTimer = $interval(function(){triggerChallengeUpdate($rootScope.challengeDetails.id)},intervalTime);
		}
	}
	$scope.refreshing = false;
	$scope.refreshChallengeResults = function(){
		triggerChallengeUpdate($rootScope.challengeDetails.id);
	}
	
	function triggerChallengeUpdate(id){
		//only if tournament panel is open
		if($rootScope.isChallenge && id == $rootScope.challengeDetails.id && $('#tournamentUL').is(":visible")){
			$scope.refreshing = true;
			server.getChallengeResults(id);
		}
		else{
			$interval.cancel(challengeUpdateTimer);
			challengeUpdateTimer = null;
		}
	}
	$scope.scoredFlags = [];
	$scope.solvedFlags = 0;
	$scope.exerciseCompleted = false;
	$scope.$on('runningExercisesScore:updated', function(event,data) {
		NProgress.done();
		$scope.refreshInProgress = false;
		for(var i in data){
			if(data.hasOwnProperty(i)){
				if(data[i].id == $rootScope.exInstanceId){
					$scope.exerciseHintsList = data[i].usedHints;
					$rootScope.exerciseScore = data[i].score.result;
					$rootScope.exerciseResults = data[i].results;
					if($rootScope.exerciseScore==-1)
						$rootScope.exerciseScore = 0;


					$scope.scoredFlags = [];
					for(var i=0; i<$rootScope.exerciseDetails.flags.length; i++){
						if($rootScope.exerciseDetails.flags[i].flagList[0].selfCheckAvailable){
							$scope.scoredFlags.push($rootScope.exerciseDetails.flags[i]);
						}
					}
					$scope.solvedFlags = 0;
					for(var j=0; j<$rootScope.exerciseResults.length;j++){
						if($rootScope.exerciseResults[j].status=="fixed" || $rootScope.exerciseResults[j].status == "0"){
							$scope.solvedFlags++;
						}
					}

					if($scope.scoredFlags.length > 0 && $scope.scoredFlags.length == $scope.solvedFlags){
						$scope.exerciseCompleted = true;
						setTimeout(function(){ $scope.showExerciseStatusModal();},2500);
					}

					return;
				}
			}
		}
		

	});
	
	$scope.getFlagCompleted = function(type){
		for(var j=0; j<$rootScope.exerciseResults.length;j++){
			if($rootScope.exerciseResults[j].type==type){
				if($rootScope.exerciseResults[j].status == "0")
					return $rootScope.exerciseResults[j].score;
				else
					return 0;
			}
		}
		
		return 0;
	}
	
	$scope.getUsedHintInfo = function(type){
		
		for(var i=0; i<$scope.exerciseHintsList.length;i++){
			if($scope.exerciseHintsList[i].type == type)
				return "(Hint -"+$scope.exerciseHintsList[i].scoreReducePercentage+"%)"
		}
		
	}
	$rootScope.exerciseResults = [];
	$rootScope.exerciseEndTime = "";
	$scope.tmpHints = [];
	
	$scope.$on('runningExercises:updated', function(event,data) {
		$scope.runningInstances = data;
		for(var i in data){
			if(data.hasOwnProperty(i)){
				if(data[i].id == $rootScope.exInstanceId){
					$rootScope.instanceDetails = data[i];
					$scope.exerciseStartRead = $rootScope.instanceDetails.exerciseStartOpened || false;
					$scope.exerciseLearnRead =  $rootScope.instanceDetails.exerciseLearnOpened || false;
					if($scope.exerciseStartRead){
						$scope.getInstanceStatus($rootScope.exInstanceId);
					}
					else{
						window.setTimeout(function(){ 
							if(!$scope.exerciseStartRead)
								$scope.navigateToStart();
						},15000);
					}
					server.refreshGuacToken($rootScope.exInstanceId);
					if(data[i].challengeId){
						$rootScope.isChallenge = true;
						server.getChallengeDetails(data[i].challengeId);
					}
					$rootScope.exerciseScore = data[i].score.result;
					if($rootScope.exerciseScore==-1)
						$rootScope.exerciseScore = 0;
					

					server.getExerciseDetails(data[i].exercise.uuid)
					$scope.exerciseName = $rootScope.instanceDetails.title;
					$rootScope.exerciseResults = data[i].results;
					var cSeconds = moment.utc($rootScope.instanceDetails.endTime).diff(moment.now(),'seconds');
					$rootScope.exerciseEndTime = $rootScope.instanceDetails.endTime;
					$scope.countdown = getCountdownString(cSeconds);
					jQuery(function ($) {
						var cDown = cSeconds,
						display = $('#cdown');
						function startTimer(duration, display) {
							var timer = duration, minutes, seconds;
							intervalT = $interval(function () {
								minutes = parseInt(timer / 60, 10)
								seconds = parseInt(timer % 60, 10);

								minutes = minutes < 10 ? "0" + minutes : minutes;
								seconds = seconds < 10 ? "0" + seconds : seconds;

								display.text(minutes + ":" + seconds);
								if(minutes<=0 && seconds<=0){
									$interval.cancel(intervalT);
									$('#exerciseTimeoutModal').modal({
										backdrop: 'static',
										keyboard: false
									});
									return;
								}
								if (--timer < 0) {
									timer = duration;
								}
							}, 1000);
						}
						startTimer(cDown, display);
					});
					$scope.solvedFlags = 0;
					var hasSolved = false;
					if($rootScope.exerciseResults.length>0)
						hasSolved = true;
					for(var j=0; j<$rootScope.exerciseResults.length;j++){
						if($rootScope.exerciseResults[j].hint){
							$scope.tmpHints.push($rootScope.exerciseResults[j].flagTitle);
						}
						
						if($rootScope.exerciseResults[j].status=="fixed" || $rootScope.exerciseResults[j].status == "0"){
							$scope.solvedFlags++;
						}else{
							hasSolved = false;
						}
					}
					$scope.exerciseHintsList = data[i].usedHints;

					if(($scope.scoredFlags.length > 0 && $scope.scoredFlags.length == $scope.solvedFlags) || hasSolved == true){
						setTimeout(function(){ $scope.showExerciseStatusModal();},2500);
					}
				
					break;
				}
			}
		}
	})
	$scope.showHelp = function(){
		//startNavigationWizard1();
	}

	function startNavigationWizard1(){
		var tripToShowNavigation = new Trip([
			{ sel : $(".startWizard"), content : "Click here to learn more about the vulnerability.", position : "s", expose: true},  
			{ sel : $(".learnWizard"), content : "Click here to setup and start the exercise.", position : "s", expose: true},
			{ sel : $(".flagWizard"), content : "These are the tasks you need to complete in this exercise. Click to view instructions for each task.", position : "s", expose: true, 
				onTripEnd: function(tripIndex) {
					for(var i in $rootScope.exerciseDetails.flags){
						if(!$rootScope.exerciseDetails.flags[i].flagList[0].optional){
							$scope.openPanelForFlagWizard($rootScope.exerciseDetails.flags[i]);
							window.setTimeout(function(){startNavigationWizard3();},1000);
							return;
						}
					}
				},
				onTripClose: function(i,o){
					setCookie("ex-tooltip","skip",10);
				},
				onTripStop: function(i,o){
					setCookie("ex-tooltip","skip",10);
				},
			}], {
			finishLabel: "Next",
			showNavigation : true,
			overlayHolder: '#topnavbar',
			canGoPrev: false,
			showCloseBox : false,
			delay : -1
		});
		tripToShowNavigation.start()
	}

	function handleCloseWizard(){
		document.getElementsByClassName('exerciseSetupDiv')[0].scrollIntoView()
		$scope.tmpPanelFlag = {};
		$scope.tmpPanelFlagOpen = false;

		$scope.blurred = false;
		$('#exerciseDropdown').removeClass('open')
		$('#tournamentDropdown').removeClass('open');
		document.location.hash = "#id="+$rootScope.exInstanceId;
		setupKeyboard();
	}

	function startNavigationWizard3(){
		var tripToShowNavigation = new Trip([
			{ sel : $(".instructionsWizard"), content : "Follow the step-by-step instructions to solve the exercise", position : "n"
			},
			{ sel : $(".selfcheckWizard"), content : "Click here to check your results.", position : "s"
			},
			{ sel : $(".hintWizard"), content : "If you're stuck, click here to get a hint", position : "s"
			}
			], {
			showNavigation : true,
			finishLabel: "Next",
			overlayHolder: '#topnavbar',
			showCloseBox : false,
			canGoPrev: false,
			delay : -1,
			onEnd: function(tripIndex,obj) {
				$scope.closeSidebar();
				handleCloseWizard();
				setCookie("ex-tooltip","skip",10);
				return;
			},
			onTripClose: function(i,o){
				$scope.closeSidebar();
				handleCloseWizard();
				setCookie("ex-tooltip","skip",10);
				return;
			},
			onTripStop: function(i,o){
				$scope.closeSidebar();
				handleCloseWizard();
				setCookie("ex-tooltip","skip",10);
				return;
			}
		});
		tripToShowNavigation.start()
	}
	

	$scope.stopExercise = function(){
		NProgress.start();
		server.stopInstance($rootScope.exInstanceId);
		$scope.status = false;
		$scope.stars = true;
		$scope.feedback = false;
		$scope.thanks = false;
	}
	$scope.backToSF = function(){
		try{
			window.opener.postMessage("backToSF",self.origin)
			setTimeout(function () {
				document.location = "/user/index.html#/running";
			},700);
		}
		catch(err){
			document.location = "/user/index.html#/running";
		}
	}

	$scope.backToCompleted = function(){
		try{
			window.opener.postMessage("backToSF",self.origin)
			setTimeout(function () {
				document.location = "/user/index.html#/history";
			},700);
		}
		catch(err){
			document.location = "/user/index.html#/history";
		}
	}


	$rootScope.kbItems = [];
	$rootScope.stackItems = [];

	$scope.$on('vulnKBLoaded:updated', function(event,data) {
		$scope.tmpKBTitle = data.vulnerability;
		document.querySelector('#kbMD').innerHTML = '';
		var cMd = data.md.text+"\n";
		if(data.kbMapping!=undefined){
			for(var i=0;i<Object.keys(data.kbMapping).length;i++){
				var tmpInner = data.kbMapping[Object.keys(data.kbMapping)[i]]
				cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
			}
		}
		var editor = new toastui.Editor.factory({
			el: document.querySelector('#kbMD'),
			viewer: true,
			usageStatistics:false,
			height: '500px',
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			initialValue: ''
		});
		editor.setMarkdown(cMd);
		autoplayMdVideo('#kbMD');
		$('#kbMD').find('a').each(function() {
			if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
				return;
			var a = new RegExp('/' + window.location.host + '/');
			if(!a.test(this.href)) {
				$(this).click(function(event) {
					event.preventDefault();
					event.stopPropagation();
					window.open(this.href, '_blank');
				});
			}
		});
		$scope.learnOpen = true;
		//tearDownKeyboard();
		$scope.exerciseLearnRead = true;
		$('.button-glow').removeClass('button-glow');
		server.setPanelRead(globalExerciseId,"learn");
		var kbFound = false;
		for(var j=0; j<$rootScope.kbItems.length; j++){
			if($rootScope.kbItems[j].uuid == data.uuid){
				$rootScope.kbItems[j] = data;
				kbFound = true;
				break;
			}
		}
		if(!kbFound){
			$rootScope.kbItems.push(data)
		}
	})
	$scope.appStatusRunning =  false;
	$scope.appStatusText = "Not tested.";
	$scope.appStatusTested = false;
	
	$scope.getStartPanelClass = function(){
		if(!$scope.exerciseStartRead || !$scope.appStatusTested)
			return 'headNotOpened';
		else if($scope.appStatusRunning == true)
			return 'headRemediated';
		else return 'headVulnerable';
	}
	$scope.getAppStatusClass = function(){
		if(!$scope.exerciseStartRead || !$scope.appStatusTested)
			return 'status-untested';
		else if($scope.appStatusRunning == true)
			return 'status-fixed';
		else return 'status-vulnerable';
	}
	
	
	
	
	$scope.getDivNavClass = function(){
		var s = "";
		//if($scope.startOpen)
		//	s += 'activePanel';
		if(!$scope.exerciseStartRead)
			s += ' button-glow';		
		return s;
	}
	
	$scope.getInstanceStatus = function(id){
		
		$scope.appStatusText = "Updating...";
		server.getInstanceStatus(id);
	}
	
	$scope.$on('instanceStatusReceived:updated', function(event,data) {
		
		if(data == undefined || data.status == undefined || data.status == 'stopped'){
			$scope.appStatusRunning =  false;
			$scope.appStatusTested = true;
			$scope.appStatusText = "Stopped";
			$rootScope.hackUnlocked = false;
			$rootScope.fixUnlocked = false;
		}
		else{
			$scope.appStatusRunning =  true;
			$scope.appStatusTested = true;
			$scope.appStatusText = "Running";
			$rootScope.hackUnlocked = true;
			var exp = sessionStorage.getItem('exploit-'+globalExerciseId);
			if(exp!=null){
				$rootScope.fixUnlocked = true;
			}
		}		
	});


	$scope.$on('stackKBLoaded:updated', function(event,data) {
		$scope.tmpKBStackTitle = "Start";
		document.querySelector('#kbStackMD').innerHTML = '';
		var editor = new toastui.Editor.factory({
			el: document.querySelector('#kbStackMD'),
			viewer: true,
			usageStatistics:false,
			height: '500px',
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			initialValue: data.md.text
		});
		autoplayMdVideo('#kbStackMD');
		$('#kbStackMD').find('a').each(function() {
			if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
				return;
			var a = new RegExp('/' + window.location.host + '/');
			if(!a.test(this.href)) {
				$(this).click(function(event) {
					event.preventDefault();
					event.stopPropagation();
					window.open(this.href, '_blank');
				});
			}
		});
		$scope.startOpen = true;
		//tearDownKeyboard();
		$scope.exerciseStartRead = true;
		server.setPanelRead(globalExerciseId,"start");
		setTimeout(function(){$location.path("id="+globalExerciseId+"&tab=start", false);},100);
		var stackFound = false;
		for(var k=0; k<$rootScope.stackItems.length; k++){
			if($rootScope.stackItems[k].uuid == data.uuid){
				$rootScope.stackItems[k] = data;
				stackFound = true;
				break;
			}
		}
		if(!stackFound){
			$rootScope.stackItems.push(data)
		}

	})
	
	$scope.refreshAppStatus = function(){
		$scope.getInstanceStatus($rootScope.exInstanceId);
	}

	$scope.highlightIfUser = function(username){
		if(username==$scope.user.user)
			return "highlightUser";
		return "";

	}
	$scope.highlightIfTableUser = function(username){
		if(username==$scope.user.user)
			return "highlightTableUser";
		return "";

	}

	$scope.hideKBModal = function(){
		$scope.learnOpen = false;
		$scope.startOpen = false;
		setupKeyboard();
	}
	


	$scope.isUser = function(username){
		if(username==$scope.user.user)
			return true
			return false;
	}

	$scope.openStackKB = function(exercise,title){
		$scope.tmpKBStackTitle = title;
		$scope.closeSidebar();
		$scope.closeMiniFlag();
		$scope.closeClipboardPanelNoKeyboardSetup();
		$scope.learnOpen = false;
		if(exercise.ignoreDefaultStack &&  undefined!=exercise.information.text){
			document.querySelector('#kbStackMD').innerHTML = '';
			var editor = new toastui.Editor.factory({
				el: document.querySelector('#kbStackMD'),
				viewer: true,
				usageStatistics:false,
				height: '500px',
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				initialValue: exercise.information.text
			});
			autoplayMdVideo('#kbStackMD');
			$('#kbStackMD').find('a').each(function() {
				if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
					return;
				var a = new RegExp('/' + window.location.host + '/');
				if(!a.test(this.href)) {
					$(this).click(function(event) {
						event.preventDefault();
						event.stopPropagation();
						window.open(this.href, '_blank');
					});
				}
			});
			//tearDownKeyboard();
			$scope.startOpen = true;
			$scope.exerciseStartRead = true;
			server.setPanelRead(globalExerciseId,"start");
			setTimeout(function(){$location.path("id="+globalExerciseId+"&tab=start", false);},150);
			return;
		}
		for(var j=0; j<$rootScope.stackItems.length; j++){
			if($rootScope.stackItems[j].uuid == exercise.stack.uuid && undefined != $rootScope.stackItems[j].md.text && $rootScope.stackItems[j].md.text != ""){

				if(undefined!=exercise.information.text){
					data = $rootScope.stackItems[j].md.text + "\n" + exercise.information.text;
				}
				else{
					data = $rootScope.stackItems[j].md.text
				}
				document.querySelector('#kbStackMD').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#kbStackMD'),
					viewer: true,
					usageStatistics:false,
					height: '500px',
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					initialValue: data
				});
				autoplayMdVideo('#kbStackMD');
				$('#kbStackMD').find('a').each(function() {
					if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
						return;
					var a = new RegExp('/' + window.location.host + '/');
					if(!a.test(this.href)) {
						$(this).click(function(event) {
							event.preventDefault();
							event.stopPropagation();
							window.open(this.href, '_blank');
						});
					}
				});
				//$('#kbStackModal').modal('show');
				$scope.startOpen = true;
				//tearDownKeyboard();
				window.setTimeout(function(){ $location.path("id="+globalExerciseId+"&tab=start", false);},200);
				return;
			}
		}
		server.loadStackKB(exercise.stack.uuid);
	}
	
	$scope.exerciseLearnRead = false;
	$scope.exerciseStartRead = false;
	
	$scope.closeKB = function(){
		$scope.learnOpen = false;
		$location.path("id="+globalExerciseId, false);
	}
	
	$scope.closeStackKB = function(){
		$scope.startOpen = false;
		$location.path("id="+globalExerciseId, false);
	}

	$scope.openKB = function(item,technology,title){
		$scope.tmpKBTitle = title;
		$scope.closeSidebar();
		$scope.closeMiniFlag();
		$scope.closeClipboardPanelNoKeyboardSetup();
		$scope.startOpen = false;
		if(item!=undefined && technology!=undefined){
			for(var j=0; j<$rootScope.kbItems.length; j++){
				if($rootScope.kbItems[j].uuid == item.uuid && undefined != $rootScope.kbItems[j].md.text && $rootScope.kbItems[j].md.text != ""){
					document.querySelector('#kbMD').innerHTML = '';
					var cMd = $rootScope.kbItems[j].md.text+"\n";
					if($rootScope.kbItems[j].kbMapping!=undefined){
						for(var i=0;i<Object.keys($rootScope.kbItems[j].kbMapping).length;i++){
							var tmpInner = $rootScope.kbItems[j].kbMapping[Object.keys($rootScope.kbItems[j].kbMapping)[i]]
							if(tmpInner.md.text != undefined)
								cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
						}
					}
					var editor = new toastui.Editor.factory({
						el: document.querySelector('#kbMD'),
						viewer: true,
						usageStatistics:false,
						height: '500px',
						plugins: [ codeSyntaxHightlight, colorSyntax ],
						initialValue: ''
					});
					editor.setMarkdown(cMd);
					autoplayMdVideo('#kbMD');
					$('#kbMD').find('a').each(function() {
						if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
							return;
						var a = new RegExp('/' + window.location.host + '/');
						if(!a.test(this.href)) {
							$(this).click(function(event) {
								event.preventDefault();
								event.stopPropagation();
								window.open(this.href, '_blank');
							});
						}
					});
					$scope.learnOpen = true;
					//tearDownKeyboard();
					$scope.exerciseLearnRead = true;
					$('.button-glow').removeClass('button-glow');
					server.setPanelRead(globalExerciseId,"learn");
					return;
				}
			}
			server.loadVulnKB(item.uuid,technology);
		}
	}



	function isKbPresent(kb){
		if(undefined==kb)
			return false;
		for(var j=0; j<$scope.exerciseKbs.length; j++){
			if($scope.exerciseKbs[j].uuid==kb.uuid)
				return true;
		}
		return false;
	}

	$scope.$on('exerciseDetails:updated', function(event,data) {
		$('.waitLoader').hide();
		$rootScope.exerciseDetails = data;
		$scope.exerciseKbs = []; 
		$scope.scoredFlags = [];
		
		var kbFound = false;
		for(var i=0; i<$rootScope.exerciseDetails.flags.length; i++){
			if(undefined!=$rootScope.exerciseDetails.flags[i].kb){
				if(!isKbPresent($rootScope.exerciseDetails.flags[i].kb))
					$scope.exerciseKbs.push($rootScope.exerciseDetails.flags[i].kb);
			}
			if($rootScope.exerciseDetails.flags[i].flagList[0].selfCheckAvailable){
				$scope.scoredFlags.push($rootScope.exerciseDetails.flags[i]);
			}
			if($rootScope.exerciseDetails.flags[i].flagList[0].type == "EXPLOITATION"){
				var exp = sessionStorage.getItem('exploit-'+globalExerciseId);
				if(exp!=null){
					$rootScope.exerciseDetails.flags[i].flagList[0].skipPanelForFlag = true;
					$rootScope.exerciseDetails.flags[i].flagList[0].panelOpened = true;
				}
			}
			for(var n=0;n<$scope.tmpHints.length;n++){
				if($rootScope.exerciseDetails.flags[i].title == $scope.tmpHints[n]){
					$rootScope.usedHints.push($rootScope.exerciseDetails.flags[i].flagList[0].id);
					$scope.tmpHints.remove(n,n);
				}
			}
			
		}	

		if($scope.scoredFlags.length > 0 && $scope.scoredFlags.length == $scope.solvedFlags){
			setTimeout(function(){ $scope.showExerciseStatusModal();},2500);
		}

	});

	$scope.getExerciseScoreStatus = function(){
		solved = $scope.getSolvedFlagsCount();
		return $scope.scoredFlags.length==solved;
	}

	$scope.getScoreFlagsCount = function(){
		return $scope.scoredFlags.length;
	}

	$scope.getSolvedFlagsCount = function(){
		var count = 0;
		for(var j=0; j<$rootScope.exerciseResults.length;j++){
			if($rootScope.exerciseResults[j].status=="fixed" || $rootScope.exerciseResults[j].status == "0"){
				count++;
			}
		}
		return count;
	}

	
	$scope.showExerciseStatusModal = function(){
		$scope.status = true;
		$scope.stars = false;
		$scope.feedback = false;
		$scope.thanks = false;
		$scope.hideKBModal();
		$scope.closeSidebar();
		$('#exerciseStatusModal').modal('show');
		$scope.doneOpen = true;
		tearDownKeyboard();
		
	}

	$scope.getResultStatusString = function(result){
		var status = result.status;
		if(undefined==status || status == "" || status == "3")
			return "Not tested";
		else if(status=="vulnerable" || status == "1")
			return "Vulnerable";
		else if(status=="fixed" || status == "0")
			return "Not Vulnerable";
		else if(status.indexOf("broken")>=0 || status == "2")
			return "Broken Functionality";
	}

	$scope.triggerFlagsMarkdown = function(fqId){
		window.setTimeout(function(){
			if(undefined != $rootScope.exerciseDetails.flags){
				for(var j=0;j<$rootScope.exerciseDetails.flags.length;j++){
					for(var k=0;k<$rootScope.exerciseDetails.flags[j].flagList.length; k++){	
						if($rootScope.exerciseDetails.flags[j].flagList[k].id==fqId){
							document.querySelector('#f'+$rootScope.exerciseDetails.flags[j].flagList[k].id+'-md').innerHTML = '';
							var editor = new toastui.Editor.factory({
								el: document.querySelector('#f'+$rootScope.exerciseDetails.flags[j].flagList[k].id+'-md'),
								viewer: true,
								usageStatistics:false,
								height: '500px',
								initialValue: $rootScope.exerciseDetails.flags[j].flagList[k].md.text
							});
							autoplayMdVideo('#f'+$rootScope.exerciseDetails.flags[j].flagList[k].id+'-md');
							$('#f'+$rootScope.exerciseDetails.flags[j].flagList[k].id+'-md').find('a').each(function() {
								if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
									return;
								var a = new RegExp('/' + window.location.host + '/');
								if(!a.test(this.href)) {
									$(this).click(function(event) {
										event.preventDefault();
										event.stopPropagation();
										window.open(this.href, '_blank');
									});
								}
							});
						}
					}
				}
			}
		},100);
	}
	$rootScope.getColorForScore = function(score){
		if(score<=20)
			return "rgba(118, 147, 193, 0.94)";
		if(score<=50)
			return "rgba(158, 74, 114, 0.87)";
		if(score<=75)
			return "rgba(118, 118, 193, 0.94)";
		if(score<=100)
			return "rgba(106, 106, 125, 0.87)";
		if(score<=125)
			return "rgba(56, 31, 8, 0.87)";
		return "rgba(189, 108, 34, 0.87)";
	}
	var loadMarkdownTs = function(id,text,ts){
		window.setTimeout(function(){
			var editor = new toastui.Editor.factory({
				el: document.querySelector('#'+id),
				height: '150px',
				usageStatistics:false,
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				initialValue: text,
				viewer:true
			}); 
			autoplayMdVideo('#'+id);
			$('#'+id).find('a').each(function() {
				if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
					return;
				var a = new RegExp('/' + window.location.host + '/');
				if(!a.test(this.href)) {
					$(this).click(function(event) {
						event.preventDefault();
						event.stopPropagation();
						window.open(this.href, '_blank');
					});
				}
			});
		},ts);
	}
	var loadMarkdown = function(id,text){
		window.setTimeout(function(){
			document.querySelector('#'+id).innerHTML = "";
			var editor = new toastui.Editor.factory({
				el: document.querySelector('#'+id),
				height: '150px',
				usageStatistics:false,
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				initialValue: text,
				viewer:true
			}); 
			autoplayMdVideo('#'+id);
			$('#'+id).find('a').each(function() {
				if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
					return;
				var a = new RegExp('/' + window.location.host + '/');
				if(!a.test(this.href)) {
					$(this).click(function(event) {
						event.preventDefault();
						event.stopPropagation();
						window.open(this.href, '_blank');
					});
				}
			});
		},150);
	}



	$scope.$on('hintReceived:updated', function(event,data) {
		if(undefined!=data){
			$scope.hintData = data;
			if(!$scope.miniFlagOpen){
				var sfMd = "#selfCheckMarkdown-md";
				var chMd = "#hintMarkdown-md";
			}
			else{
				var sfMd = "#selfCheckMarkdownMini-md";
				var chMd = "#hintMarkdownMini-md";
			}
			document.querySelector(sfMd).innerHTML = '';
			document.querySelector(chMd).innerHTML = '';
			var editor = new toastui.Editor.factory({
				el: document.querySelector(chMd),
				viewer: true,
				usageStatistics:false,
				height: '500px',
				initialValue: data.md.text
			});
			$(chMd).find('a').each(function() {
				if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
					return;
				var a = new RegExp('/' + window.location.host + '/');
				if(!a.test(this.href)) {
					$(this).click(function(event) {
						event.preventDefault();
						event.stopPropagation();
						window.open(this.href, '_blank');
					});
				}
			});
			server.getRunningExercisesScore();

		}
	});


	
	$scope.getFlagResultStatusClass = function(flag){
		if( undefined == flag || undefined == flag.selfCheck)
			return;
		for(var i in $rootScope.exerciseResults){
			if($rootScope.exerciseResults.hasOwnProperty(i) && flag.selfCheck.name == $rootScope.exerciseResults[i].name){
				var status = $rootScope.exerciseResults[i].status;
				if(undefined==status || status == "")
					return "status-untested";
				else if(status=="vulnerable" || status == "1")
					return "status-vulnerable";
				else if(status=="fixed" || status == "0")
					return "status-fixed";
				else if(status.indexOf("broken")>=0 || status == "2")
					return "status-vulnerable";
				return "status-untested";
			}
		}
		return "status-untested";
	}
	$scope.flagPanelExpanded = false;
	$scope.stackKBExpanded = false;
	$scope.kbExpanded = false;
	$scope.alignLeft = true;
	$scope.kbAlignLeft = false;
	
	$scope.leftAlign = function(){
		$scope.alignLeft = true;

	}
	$scope.rightAlign = function(){
		$scope.alignLeft = false;

	}
	$scope.leftKBAlign = function(){
		$scope.kbAlignLeft = true;

	}
	$scope.rightKBAlign = function(){
		$scope.kbAlignLeft = false;

	}
	
	
	$scope.getFlagPenlSizeAndOrientation = function(){
		var s = "";
		if($scope.flagPanelExpanded)
			s += "largeFlagPanel"
		if(!$scope.alignLeft){
			s += " alignRight "
		}
		return s;
	}
	
	$scope.getStackKBSizeAndOrientation = function(){
		var s = "";
		if($scope.stackKBExpanded)
			s += "largeStackKBPanel"
		if(!$scope.alignLeft){
			s += " alignRight "
		}
		return s;
	}
	$scope.getKBSizeAndOrientation = function(){
		var s = "";
		if($scope.kbExpanded)
			s += "largeKBPanel"
		if($scope.kbAlignLeft){
			s += " alignKBLeft "
		}
		return s;
	}
	
	$scope.reduceStackKB = function(){
		$scope.stackKBExpanded = false;
		$scope.kbExpanded = false;
		$scope.flagPanelExpanded = false;
	}
	
	$scope.expandStackKB = function(){
		$scope.stackKBExpanded = true;
		$scope.kbExpanded = true;
		$scope.flagPanelExpanded = true;
	}
	$scope.reduceKB = function(){
		$scope.kbExpanded = false;
		$scope.flagPanelExpanded = false;
		$scope.stackKBExpanded = false;
	}
	
	$scope.expandKB = function(){
		$scope.kbExpanded = true;
		$scope.flagPanelExpanded = true;
		$scope.stackKBExpanded = true;
	}
	
	$scope.reduceFlagPanel = function(){
		$scope.flagPanelExpanded = false;
		$scope.kbExpanded = false;
		$scope.stackKBExpanded = false;
	}
	
	$scope.expandFlagPanel = function(){
		$scope.flagPanelExpanded = true;
		$scope.kbExpanded = true;
		$scope.stackKBExpanded = true;
	}
	
	$scope.isSolved = function(flag){
		if( undefined == flag || undefined == flag.selfCheck )
			return;
		for(var i in $rootScope.exerciseResults){
			if(flag.selfCheck.name == $rootScope.exerciseResults[i].name){
				var status = $rootScope.exerciseResults[i].status;
				if(undefined==status || status == "")
					return false;
				else if(status=="fixed" || status == "0")
					return true;
				else return false;
			}
		}
	}
	$scope.getFlagResultStatus = function(flag){
		if( undefined == flag || undefined == flag.selfCheck )
			return;
		for(var i in $rootScope.exerciseResults){
			if(flag.selfCheck.name == $rootScope.exerciseResults[i].name){
				var status = $rootScope.exerciseResults[i].status;
				if(undefined==status || status == "" || status == "3")
					return "Not tested";
				else if(status=="vulnerable" || status == "1")
					return "Vulnerable";
				else if(status=="fixed" || status == "0")
					return "Not Vulnerable";
				else if(status.indexOf("broken")>=0 || status == "2")
					return "Broken Functionality";
				else return toTitleCase(status).replaceAll("-"," ");
			}
		}
		return "Not tested";
	}

	$scope.getPanelStatusClass = function(flag,last){
		if( undefined == flag || undefined == flag.title )
			return;
		
		if(flag.flagList[0].type == "EXPLOITATION" && !$rootScope.hackUnlocked){
			return "lockedPanel";
		}
		if(flag.flagList[0].type == "REMEDIATION" && !$rootScope.fixUnlocked){
			return "lockedPanel";
		}
		
		if( (flag.flagList[0].type == "REMEDIATION" && !$rootScope.fixUnlocked) || (flag.flagList[0].type == "EXPLOITATION" && !$rootScope.hackUnlocked)){
			return "headNotOpened";
		}
		if(flag.flagList[0].optional && !flag.flagList[0].panelOpened){
			return "headNotOpened";
		}
		else if(flag.flagList[0].optional && flag.flagList[0].panelOpened && !flag.flagList[0].skipPanelForFlag){
			return "headNotOpened";
		}
		else if(flag.flagList[0].optional && flag.flagList[0].panelOpened && flag.flagList[0].skipPanelForFlag){
			return "headRemediated";
		}
			
		for(var i in $rootScope.exerciseResults){
			if(flag.title == $rootScope.exerciseResults[i].flagTitle){
				var status = $rootScope.exerciseResults[i].status;
				if(status==undefined){
					return "headNotOpened";
				}
				else if(status=="vulnerable" || status =="1"){
					return "headVulnerable";
				}
				else if(status=="fixed" || status =="0"){
					return "headRemediated";

				}
				else if(status.indexOf("broken")>=0 || status == "2"){
					return "headVulnerable";

				}
				return "headNotOpened";
			}
		}
		return "headNotOpened";
	}


	$scope.getSelfCheckScore = function(f){
		if(undefined!=f && f.selfCheckAvailable && undefined!=f.score)
			return f.score;
		else
			return " ? ";
	}


	$scope.isAvailable = function(selfCheckStatus){
		if(undefined==selfCheckStatus)
			return false
			return true;
	}



	$scope.resultsLastRefreshed = undefined;
	$scope.$on('resultStatusReceived:updated', function(event,data) {

		$scope.flagIsVulnerable = false;
		$scope.resultsLastRefreshed = new Date();
		NProgress.done();

		if(undefined!=data && undefined!=data.selfcheck && data.selfcheck.length==0){
			$scope.asNotStarted = true;
			$scope.refreshInProgress = false;
			$scope.terminalMessage = "We could not get your results...";
		}
		else{
			$scope.terminalMessage = "";

			selfcheckData = data.selfcheck;
			scList = [];
			for(var i in $rootScope.exerciseDetails.flags){
				for(var j in $rootScope.exerciseDetails.flags[i].flagList){
					if($rootScope.exerciseDetails.flags[i].flagList[j].selfCheckAvailable){
						var sc = $rootScope.exerciseDetails.flags[i].flagList[j].selfCheck.name;
						if(undefined!=sc)	{
							scList.push(sc);
							for(var n=0;n<selfcheckData.length;n++){
								if(sc==selfcheckData[n].name){
									$rootScope.exerciseDetails.flags[i].flagList[j].selfCheckStatus = selfcheckData[n].status;
									$rootScope.exerciseDetails.flags[i].flagList[j].selfCheckMessage = $rootScope.exerciseDetails.flags[i].flagList[j].selfCheck.messageMappings[selfcheckData[n].status];
									$rootScope.exerciseDetails.flags[i].flagList[j].selfCheckMapping = $rootScope.exerciseDetails.flags[i].flagList[j].selfCheck.statusMapping[selfcheckData[n].status];
									
									break;
								}
							}
						}
					}
				}
			}
			var selfcheckData = data.selfcheck;
			for(var n=0;n<selfcheckData.length;n++){
				if(scList.indexOf(selfcheckData[n].name)<0){
					selfcheckData.remove(n,n);
					n = n-1;
				}
			}
			$scope.resultStatusData = selfcheckData;
			$scope.asNotStarted = false;
			for(var i in $rootScope.exerciseDetails.flags){
				for(var j in $rootScope.exerciseDetails.flags[i].flagList){
					if($rootScope.exerciseDetails.flags[i].flagList[j].selfCheckAvailable && $scope.currentResultWaiting == $rootScope.exerciseDetails.flags[i].flagList[j].selfCheck.name){
						var flagQuestion = $rootScope.exerciseDetails.flags[i].flagList[j];
						if(!flagQuestion.selfCheckAvailable || undefined==flagQuestion.selfCheckStatus){
							continue;
						}
						if(flagQuestion.selfCheckMessage){
							content =  flagQuestion.selfCheckMessage;
						}
						else if(flagQuestion.selfCheckMapping=="0"){
							content = "Not Vulnerable";
						}
						else{
							content = "Not Available";
						}
						
						var status = flagQuestion.selfCheckStatus;
						
						if(status=="broken-webserver"){
							$scope.appStatusRunning =  false;
							$scope.appStatusTested = true;
							$scope.appStatusText = "Stopped";
							$rootScope.hackUnlocked = false;
							$rootScope.fixUnlocked = false;
						}
						
						$scope.markdownClass = "markdownError";
						
						if(status=="vulnerable" || status == "1")
							$scope.markdownClass = "markdownRed";
						else if(status=="fixed" || status == "0")
							$scope.markdownClass = "markdownGreen";
						else if(status.indexOf("broken")>=0 || status == "2")
							$scope.markdownClass = "markdownRed";
					
						
						

						if(!$scope.miniFlagOpen){
							var sfMd = "#selfCheckMarkdown-md";
							var chMd = "#hintMarkdown-md";
						}
						else{
							var sfMd = "#selfCheckMarkdownMini-md";
							var chMd = "#hintMarkdownMini-md";
						}

						document.querySelector(chMd).innerHTML = '';
						document.querySelector(sfMd).innerHTML = '';
						var editor = new toastui.Editor.factory({
							el: document.querySelector(sfMd),
							viewer: true,
							usageStatistics:false,
							height: '500px',
							initialValue: content
						});
						$(sfMd).find('a').each(function() {
							if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
								return;
							var a = new RegExp('/' + window.location.host + '/');
							if(!a.test(this.href)) {
								$(this).click(function(event) {
									event.preventDefault();
									event.stopPropagation();
									window.open(this.href, '_blank');
								});
							}
						});
						window.setTimeout(function(){server.getRunningExercisesScore()},1350);
						return;
					}
				}
			}
			window.setTimeout(function(){server.getRunningExercisesScore()},1000);
		}
	});

	$scope.getExerciseDetails = function(uuid){
		server.getExerciseDetails(uuid);
	}
	function getCountdownString(wait){
		var w;
		try { w = parseInt(wait) } catch(err){ return ""; }
		if(w<10){
			return "00:0"+w;
		}
		if(w<60){
			return "00:"+w;
		}
		if(w<600){
			return "0"+Math.floor(w/60)+":"+(w % 60);
		}

	}

	$scope.emulatorPreload = false;

	function connectEmulator(ec2Ip,ec2Instance){
		
	}
	
	

	function waitForEmulator(ec2Ip,ec2Instance){

		
	}
	
	$scope.reloadWindow = function(){
		location.reload();
	}


	$scope.mobile = false;
	$scope.cIframeUrl = "";
	
	$scope.iframeLoaded = function(){
		
		if($scope.data.ec2Ip!=null && $scope.data.ec2Instance !=null){
			
		}
		else{
			var display = document.getElementById("display");
			display.innerHtml = "";

			var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
			
			width = Math.floor((width) * 2);
			height = Math.floor((height - 40) * 2);

			guac = new Guacamole.Client(new Guacamole.WebSocketTunnel("wss://"+$scope.guacFqdn+"/sf/websocket-tunnel?token="+$scope.guacToken+"&GUAC_DATA_SOURCE=mysql&GUAC_ID="+$scope.connectionId+"&GUAC_TYPE=c&GUAC_WIDTH="+width+"&GUAC_HEIGHT="+height+"&GUAC_DPI=192&GUAC_AUDIO=audio%2FL8&GUAC_AUDIO=audio%2FL16&GUAC_IMAGE=image%2Fjpeg&GUAC_IMAGE=image%2Fpng&GUAC_IMAGE=image%2Fwebp&", true));
			display.appendChild(guac.getDisplay().getElement());
			guac.onerror = function(event) {
				$('#conectionLostModal').modal('show');
				console.log(JSON.stringify(event));
			};
			guac.onstatechange = function(event) {
				if(event == 3){
					waitForResize();
					nativeClipboard();
					
				}
			};
		

			guac.connect();

			window.onunload = function() {
				if(null!=guac)
					guac.disconnect();
			}

			// Mouse
			gMouse = new Guacamole.Mouse(guac.getDisplay().getElement());

			gMouse.onmousedown = gMouse.onmouseup = gMouse.onmousemove = function(mouseState) {
				if(null!=guac)
					guac.sendMouseState(mouseState);
			};

			// Keyboard

			gKeyboard = new Guacamole.Keyboard(document);

			gKeyboard.onkeydown = function (keysym) {
				if(null!=guac)
					guac.sendKeyEvent(1, keysym);
			};

			gKeyboard.onkeyup = function (keysym) {
				if(null!=guac)
					guac.sendKeyEvent(0, keysym);
			};
			
		
			
		}
		
	}
	
	$scope.data = {};
	
	$scope.$on('tokenRefreshed:updated', function(event,data) {
		$scope.guacToken = data.token;
		$scope.guacUser = data.user;
		$scope.connectionId = data.connectionId;
		$scope.guacFqdn = data.fqdn;
		$scope.node = data.node;
		$scope.data = data;
		
		$scope.cIframeUrl = "https://"+$scope.guacFqdn+"/sf/aservlet?u="+$scope.guacUser+"&t="+$scope.guacToken+"&node="+encodeURIComponent($scope.node);
		
		
	});
	
	function nativeClipboard(){
		if(is_chrome || is_newedge){
			try{
				if (document.hasFocus()) {
					navigator.clipboard.readText().then(function(text){
						if(text!=null && text != "" && null!=guac){
							guac.setClipboard(text);
						}
					});
				}
			} catch(e){}
			document.addEventListener('copy', (event) => {
				try{
					navigator.clipboard.readText().then(function(text){
						if(text!=null && text != "" &&  null!=guac){
							guac.setClipboard(text);
						}
					});
				} catch(e){}
			});
			$(window).on("focus", function() {
					try{
						if (document.hasFocus()) {
							navigator.clipboard.readText().then(function(text){
								if(text!=null && text != "" &&  null!=guac){
									guac.setClipboard(text);
								}
							});
						}
					} catch(e){}
				
			});
		}
	}
	
	function waitForResize(){
		if( null!=guac && guac.getDisplay().getHeight()!=0)
			resizeExerciseCanvas();
		else
			setTimeout(function(){waitForResize()},150);
	}

	$scope.terminalMessage = "Ready...";
	$scope.currentResultWaiting = "";
	$scope.refreshInProgress = false;
	$scope.refreshAllResults = function(){
		NProgress.start();
		$scope.refreshInProgress = true;
		server.getResultStatus($rootScope.exInstanceId);
	}

	$scope.status = true;
	$scope.stars = false;
	$scope.feedback = false;
	$scope.thanks = false;
	$scope.currentVote = -1;
	$scope.voteExercise = function(){
		if($scope.currentVote>0){
			server.voteExercise($rootScope.exInstanceId, $scope.currentVote);
			if($scope.currentVote>=5){
				$scope.stars = false;
				$scope.status = false;
				$scope.feedback = false;
				$scope.thanks = true;
				$scope.closeFeedbackSteps(1200);
			}
			else{
				$scope.stars = false;
				$scope.status = false;
				$scope.thanks = false;
				$scope.feedback = true;
			}
		}
		else{
			$scope.closeFeedbackSteps(300);
			return;
		}
	}

	$scope.closeFeedbackSteps = function(duration){
		try{
			window.opener.postMessage("markCompleted",self.origin)
			setTimeout(function () {
				document.location = "/user/index.html#/history";
			},duration);
		}
		catch(err){
			document.location = "/user/index.html#/history";
		}
	}
	$scope.feedbackText = "";
	$scope.sendFeedback = function(){
		server.sendFeedback($rootScope.exInstanceId, $scope.feedbackText);
		$scope.stars = false;
		$scope.feedback = false;
		$scope.status = false;
		$scope.thanks = true;
		$scope.closeFeedbackSteps(1200);
	}	

	$scope.refreshResults = function(check,flag){
		if(undefined!=flag){
			$scope.openPanelForFlag(flag)
		}
		if(!$scope.miniFlagOpen){
			var sfMd = "#selfCheckMarkdown-md";
			var chMd = "#hintMarkdown-md";
		}
		else{
			var sfMd = "#selfCheckMarkdownMini-md";
			var chMd = "#hintMarkdownMini-md";
		}
		document.querySelector(sfMd).innerHTML = '';
		document.querySelector(chMd).innerHTML = '';
		$scope.currentResultWaiting = check;
		$scope.terminalMessage = "Checking your results...";
		NProgress.start();
		server.getResultStatus($rootScope.exInstanceId,check);
	};
	$scope.getPanelSize = function(tmpPanelFlag){
		if(undefined==tmpPanelFlag || undefined==tmpPanelFlag.flagList)
			return;
		if(!tmpPanelFlag.flagList[0].selfCheckAvailable && !tmpPanelFlag.flagList[0].hintAvailable)
			return 'col-md-12';
		else
			return 'col-md-7';
	}
	$scope.getHint = function(){
		if(!$scope.isFlipped && $scope.miniFlagOpen){
			$scope.isFlipped = true;
		}
		if(!$scope.miniFlagOpen){
			var sfMd = "#selfCheckMarkdown-md";
			var chMd = "#hintMarkdown-md";
		}
		else{
			var sfMd = "#selfCheckMarkdownMini-md";
			var chMd = "#hintMarkdownMini-md";
		}
		document.querySelector(sfMd).innerHTML = '';
		document.querySelector(chMd).innerHTML = '';
		$rootScope.usedHints.push($scope.hintFlagQuestionId);
		$scope.terminalMessage = "";
		server.getHintForQuestion($scope.hintFlagQuestionId, $rootScope.exInstanceId);
		$('#getHintModal').modal('hide');
	};
	
	
	$scope.getHintModal = function(id,flag,reduction,flip){

		if(undefined!=flag){
			$scope.openPanelForFlag(flag)
		}
		$scope.hintFlagQuestionId = id;
		$scope.hintFlagReduction = reduction;
		$scope.hintFlagReductionPoints = Math.floor(($scope.tmpPanelFlag.flagList[0].maxScore / 100) * reduction);
		var asked = false;
		
		for(var i=0; i<$rootScope.usedHints.length;i++){
			if($rootScope.usedHints[i]==id){
				asked = true;
				break;
			}
		}
		
		if(reduction!=0 && !asked)
			$('#getHintModal').modal('show');
		else
			$scope.getHint();
	};
}])

