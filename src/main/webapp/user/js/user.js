/*
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

const { Editor } = toastui;
var codeSyntaxHightlight = Editor.plugin.codeSyntaxHighlight;
var colorSyntax = Editor.plugin.colorSyntax;

Number.isInteger = Number.isInteger || function(value) {
	return typeof value === 'number' && 
	isFinite(value) && 
	Math.floor(value) === value;
};
function getSum(total, num) {
	return total + num;
}
function randomInt(min, max){
	return Math.floor(Math.random() * (+max - +min)) + +min; 
}
function receiveMessage(event){
	if (event.origin != self.origin)
		return;
	if(event.data == "backToSF"){
		event.source.postMessage("ok", event.origin);
		window.location = '/user/index.html#/running';
		angular.element(document.getElementById('assignedExercises')).scope().exerciseStopped();
		return;
	}
	if(event.data == "markCompleted"){
		event.source.postMessage("ok", event.origin);
		window.location = '/user/index.html#/history';
		angular.element(document.getElementById('assignedExercises')).scope().exerciseStopped();
	}
	if(event.data == "close"){
		self.close();
	}
	event.source.postMessage("ok",event.origin);
}
window.addEventListener("message", receiveMessage, false);

function cloneObj(obj){
	return JSON.parse(JSON.stringify(obj))
}
function autoplayMdVideo(selector){
	$(selector+' .mdvideo video').each(function(){
		this.autoplay = true;
		this.controls = true;
		this.load();    
	});
}
Array.prototype.remove = function(from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};
Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};
if (!Array.prototype.includes) {
	Object.defineProperty(Array.prototype, 'includes', {
		value: function(searchElement, fromIndex) {

			// 1. Let O be ? ToObject(this value).
			if (this == null) {
				throw new TypeError('"this" is null or not defined');
			}

			var o = Object(this);

			// 2. Let len be ? ToLength(? Get(O, "length")).
			var len = o.length >>> 0;

			// 3. If len is 0, return false.
			if (len === 0) {
				return false;
			}

			// 4. Let n be ? ToInteger(fromIndex).
			//    (If fromIndex is undefined, this step produces the value 0.)
			var n = fromIndex | 0;

			// 5. If n ≥ 0, then
			//  a. Let k be n.
			// 6. Else n < 0,
			//  a. Let k be len + n.
			//  b. If k < 0, let k be 0.
			var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

			function sameValueZero(x, y) {
				return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
			}

			// 7. Repeat, while k < len
			while (k < len) {
				// a. Let elementK be the result of ? Get(O, ! ToString(k)).
				// b. If SameValueZero(searchElement, elementK) is true, return true.
				// c. Increase k by 1. 
				if (sameValueZero(o[k], searchElement)) {
					return true;
				}
				k++;
			}

			// 8. Return false
			return false;
		}
	});
}
function deepCopy (arr) {
	var out = [];
	for (var i = 0, len = arr.length; i < len; i++) {
		var item = arr[i];
		var obj = {};
		for (var k in item) {
			obj[k] = item[k];
		}
		out.push(obj);
	}
	return out;
}
function replaceArrayContent(obj1, obj2){
	obj1.remove(0,(obj1.length-1));
	for(var i in obj2){
		obj1.push(obj2[i]);
	}
}
function replaceObjectContent(obj1, obj2){
	for (var key in obj1){
		if (obj1.hasOwnProperty(key)){
			delete obj1[key];
		}
	}
	for(var i in obj2){
		obj1[i] = obj2[i]
	}
}
jQuery.extend({
	deepclone: function(objThing) {

		if ( jQuery.isArray(objThing) ) {
			return jQuery.makeArray( jQuery.deepclone($(objThing)) );
		}
		return jQuery.extend(true, {}, objThing);
	},
});
function splitValue(value, index) {
	return (value.substring(0, index) + "," + value.substring(index)).split(',');
}
_st = function(fRef, mDelay) {
	if(typeof fRef == "function") {
		var argu = Array.prototype.slice.call(arguments,2);
		var f = (function(){ fRef.apply(null, argu); });
		return setTimeout(f, mDelay);
	}
	try{
		return window.setTimeout(fRef, mDelay);
	}
	catch(err){

	}
}
PNotify.prototype.options.stack.firstpos1 = 80; // or whatever pixel value you want.


$(document).ready(function(){

	$('#targetDiv').on('click','.snippetScroll', function(e){
		e.preventDefault();
		var offset = $('#'+$(this).attr('path')).offset();
		$('html, body').animate({
			scrollTop: offset.top - 50,
			scrollLeft: offset.left
		});
	})
});

var sf = angular.module('sfNg',['nya.bootstrap.select','jlareau.pnotify','ui.toggle','ngRoute','angular-table','chart.js','angular-notification-icons','angular-scroll-animate','angular-timeline']);


//register the interceptor as a service
var compareTo = function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {

			ngModel.$validators.compareTo = function(modelValue) {
				return modelValue == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};

};

sf.directive("compareTo", compareTo);

sf.directive('complexPassword', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			ctrl.$parsers.unshift(function(password) {
				var hasUpperCase = /[A-Z]/.test(password);
				var hasLowerCase = /[a-z]/.test(password);
				var hasNumbers = /\d/.test(password);
				var hasNonalphas = /\W/.test(password);
				var characterGroupCount = hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas;

				if ((password.length >= 8) && (characterGroupCount >= 3)) {
					ctrl.$setValidity('complexity', true);
					return password;
				}
				else {
					ctrl.$setValidity('complexity', false);
					return undefined;
				}

			});
		}
	}
});
sf.filter('capitalize', function() {
	return function(input) {
		return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
	}
});
sf.service('server',function($http,$timeout,$rootScope,notificationService,$interval){

	this.countries = [];

	var $this = this;




	this.cancelCrashedExercise = function(id){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'cancelCrashedExercise';
		obj.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('crashedExerciseCancelled:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.loadVulnKB = function(uuid,technology){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'getKBItem';
		obj.uuid = uuid;
		obj.technology = technology;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('vulnKBLoaded:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getPaths = function(){
		var obj = {};
		obj.action = 'getPaths';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: obj,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('paths:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getTeamStats = function(){
		var msg = {};
		msg.action = 'getTeamStats';
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('statsTeam:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getCountries = function(){

		var msg = {};
		msg.action = 'getCountries';

		var req = {
				method: 'POST',
				url: '/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceArrayContent($this.countries,response.data)
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}


	this.removeUser = function(){
		var msg = {};
		msg.action = 'removeUser';
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('removeUser:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}



	this.getUserStats = function(username){
		var msg = {};
		msg.action = 'getMyStats';
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('statsUser:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.user = {}

	this.addScoringComplaint = function(obj){

		obj.action = 'addScoringComplaint';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('scoringComplaintAdded:updated',response.data);
			notificationService.success('Thanks, your comment will be reviewed by our team shortly.');
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getUserTeamLeaderboard = function(){

		var msg = {};
		msg.action = 'getLeaderboard';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('leaderboard:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.getUserProfile = function(){

		var msg = {};
		msg.action = 'getUserInfo';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceObjectContent($this.user,response.data);
			$rootScope.$broadcast('userInfo:updated',response.data);

			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.updateUserProfile = function(updatedUser){
		var msg = {};
		msg.action = 'setUserInfo';
		msg.firstName = updatedUser.firstName;
		msg.lastName = updatedUser.lastName;
		msg.email = updatedUser.email;
		msg.country = updatedUser.country.short;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User profile updated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.updateUserPassword = function(oldPassword, newPassword){
		var msg = {};
		msg.action = 'setUserPassword';
		msg.oldPwd = oldPassword;
		msg.newPwd = newPassword;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User password updated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}

		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}


	this.getUserAchievements = function(){

		var msg = {};
		msg.action = 'getUserAchievements';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('achievements:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.applyForPathCertification = function(id){
		var msg = {};
		msg.action = 'applyForPathCertification';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(response.data.result == "success"){
				PNotify.removeAll();
				notificationService.success("You have successfully completed a Learning Path, check your new Certification in your dashboard!");
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}


	this.doUserLogout = function(){
		var msg = {};
		msg.action = 'doLogout';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$(document).attr('location', "/");
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	$rootScope.ctoken = "";
	this.getCToken = function(){
		var msg = {};
		msg.action = 'getUserCToken';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.ctoken = response.data.ctoken;
			$this.getInitialData();
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getCToken($this);



	this.exercisesData = [];

	this.getAvailableExercises = function(){

		var msg = {};
		msg.action = 'getExercises';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceArrayContent($this.exercisesData,response.data)
			$rootScope.$broadcast('exercises:updated',$this.exercisesData);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.cancelChallengeExercise = function(id){
		var msg = {};
		msg.action = 'cancelChallengeExercise';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result == "success"){
				$rootScope.$broadcast('cancelChallengeExercise:updated',response.data);
				notificationService.success("Tournament exercise cancelled, you can now play the exercise again.");
			}
			else{
				PNotify.removeAll();
				notificationService.info("Could not cancel the exercise, please try again or contact support.");
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.getChallenges = function(){

		var msg = {};
		msg.action = 'getChallenges';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			if(response.data.length==0)
				$rootScope.$broadcast('challenges:updated',null);
			else
				$rootScope.$broadcast('challenges:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}    
	this.getChallengeResults = function(id){

		var msg = {};
		msg.action = 'getChallengeResults';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			if(response.data.length==0)
				$rootScope.$broadcast('challengeResults:updated',null);
			else
				$rootScope.$broadcast('challengeResults:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.getChallengeDetails = function(id){

		var msg = {};
		msg.action = 'getChallengeDetails';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			if(response.data.length==0)
				$rootScope.$broadcast('challengeDetails:updated',null);
			else
				$rootScope.$broadcast('challengeDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.isExerciseIsInChallenge = function(uuid){

		var msg = {};
		msg.action = 'isExerciseInChallenge';
		msg.uuid = uuid;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('exerciseIsInChallenge:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}


	this.getExerciseDetails = function(uuid){

		var msg = {};
		msg.action = 'getExerciseLightDetails';
		msg.uuid = uuid;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('exerciseDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.downloadExerciseSolutions = function(id){

		var msg = {};
		msg.action = 'getSolutionFile';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('exerciseSolution:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	


	this.getRegionsForExercise = function(uuid){

		var msg = {};
		msg.action = 'getRegionsForExercise';
		msg.uuid = uuid;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('exerciseRegions:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getHintForQuestion = function(id,exId){

		var msg = {};
		msg.action = 'getHint';
		msg.id = id;
		msg.exId = exId;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hintReceived:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getResultStatus = function(id, check){

		if(undefined == check || null == check || "" === check)
			check = "all";
		var msg = {};
		msg.action = 'getResultStatus';
		msg.id = id;
		msg.check = check;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('resultStatusReceived:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}


	this.acceptCrashedInstanceResult= function(id){
		var msg = {};
		msg.action = 'acceptCrashedInstanceResult';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}

		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result == "success"){
				$rootScope.$broadcast('acceptCrashedInstanceResult:updated',response.data);
				notificationService.success("Result accepted.");
			}
			else{
				PNotify.removeAll();
				notificationService.info("Could not accept the result, please try again or contact support.");
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.stopInstance = function(id){
		var msg = {};
		msg.action = 'stopExerciseInstance';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}

		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result == "success"){
				$rootScope.$broadcast('exerciseStopped:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.info("Could not stop the exercise, please try again or contact support.");
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.pollReservation = function(id){
		var msg = {};
		msg.action = 'getReservationUpdate';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			PNotify.removeAll();
			$rootScope.$broadcast('instanceStarted:updated',response.data);
			if(response.data.waitSeconds>30){
				PNotify.removeAll();
				notificationService.notice("SecureFlag is very busy at this time. You're in queue, your environment will be ready in "+moment.utc(response.data.waitSeconds*1000).format('mm:ss'));
			}
			else if(response.data.waitSeconds>5){
				PNotify.removeAll();
				notificationService.notice("Your SecureFlag environment is almost ready, please hold on...");
			}
			$rootScope.$broadcast('instanceStarted:updated',response.data);			
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.startInstance = function(uuid, regions, challenge, crashedExerciseId){
		var msg = {};
		msg.action = 'launchExerciseInstance';
		msg.uuid = uuid;
		msg.region = regions;
		msg.challengeId = challenge;
		if(undefined!= crashedExerciseId && null!=crashedExerciseId)
			msg.crashedExerciseId = crashedExerciseId;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(response.data.errorMsg == "InstanceLimit"){
				PNotify.removeAll();
				notificationService.error("You reached your limit for exercises you can run at the same time. Browse to 'Running Exercises' and terminate completed exercises.");
				$('.waitLoader').hide();
				$("#startExerciseModal").modal('hide');
			}
			else if(response.data.errorMsg == "CreditsLimit"){
				PNotify.removeAll();
				notificationService.error("You don't have any credits left. Please contact support.");
				$('.waitLoader').hide();
				$("#startExerciseModal").modal('hide');
			}
			else if(response.data.errorMsg == "NoResourcesAvailable"){
				PNotify.removeAll();
				notificationService.error("The are no resources available to start the exercise in your region, please try again later or contact support.");
				$('.waitLoader').hide();
				$("#startExerciseModal").modal('hide');
			}
			else if(response.data.errorMsg == "GWUnavailable" || response.data.errorMsg == "RegionNotFound"){
				PNotify.removeAll();
				notificationService.error("The environment could not be started in your region, please try again or contact support.");
				$('.waitLoader').hide();
				$("#startExerciseModal").modal('hide');
			}
			else if(response.data.errorMsg == "ExerciseUnavailable"){
				PNotify.removeAll();
				notificationService.error("The exercise requested is unavailable for your organization, please try again or contact support.");
				$('.waitLoader').hide();
				$("#startExerciseModal").modal('hide');
			}
			else if(response.data.errorMsg == "AlreadyRun"){
				PNotify.removeAll();
				notificationService.error("The exercise requested is part of an active Challenge and it has already been run.");
				$('.waitLoader').hide();
				$("#startExerciseModal").modal('hide');
			}
			else if(response.data.errorMsg!=undefined){
				PNotify.removeAll();
				notificationService.error("The SecureFlag environment could not be started, please try again or contact support.");
				$('.waitLoader').hide();
				$("#startExerciseModal").modal('hide');
			}
			else{
				PNotify.removeAll();
				$rootScope.$broadcast('instanceStarted:updated',response.data);
				if(response.data.waitSeconds>30)
					notificationService.notice("SecureFlag is very busy at this time. You're in queue, your environment will be ready in "+moment.utc(response.data.waitSeconds*1000).format('mm:ss'));
				else
					notificationService.info("We're preparing your SecureFlag environment, it will be ready in a few seconds.");
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getUserReservations = function(){


		var msg = {};
		msg.action = 'getUserReservations';   

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(undefined!=response.data){
				$rootScope.$broadcast('userReservations:updated',response.data);
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getRunningExercises = function(id, regionRaw){
		var msg = {};
		msg.action = 'getRunningExercises';   

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(undefined!=response.data){
				$rootScope.$broadcast('runningExercises:updated',response.data);
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getUserHistory = function(){
		var msg = {};
		msg.action = 'getUserHistory';   

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(undefined!=response.data){
				$rootScope.$broadcast('userHistory:updated',response.data);
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getUserHistoryDetails = function(id){
		var msg = {};
		msg.action = 'getUserHistoryDetails';   
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(undefined!=response.data){
				$rootScope.$broadcast('userHistoryDetails:updated',response.data);
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getUnreadNotifications = function(){

		var msg = {};
		msg.action = 'getNotifications';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('unreadNotifications:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.markNotificationAsRead = function(idNotification){

		var msg = {};
		msg.action = 'markNotificationRead';
		msg.id = idNotification;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(undefined==response.data.error){
				$rootScope.$broadcast('unreadNotifications:updated',response.data);
			}
			else{
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.sendFeedback = function(exerciseInstance,message){
		var msg = {};
		msg.action = 'addFeedback';   
		msg.id = exerciseInstance;
		msg.feedback = message;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(undefined!=response.data && !response.data.errorMsg ){
				PNotify.removeAll();
				notificationService.success('Thanks for your feedback!');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Your feedback could not be sent, please try again. You can submit one feedback per exercise');
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getInitialData = function(){  
		this.getCountries();
		this.getUserProfile();
		this.getUserReservations();
		this.getRunningExercises();
		this.getUserAchievements();
		this.getAvailableExercises();
		this.getPaths();
		this.getChallenges();
		this.getUserStats();
		this.getUserTeamLeaderboard();
		this.getTeamStats();
		this.getUserHistory();
		this.getUnreadNotifications();
	}
	this.updateData = function(){
		if($rootScope.ctoken == ""){
			return;
		}
		console.log('updating data...')
		this.getUnreadNotifications();
		this.getRunningExercises();
	}
	var updateTime = + randomInt(50000,60000);
	$interval(this.updateData($this),updateTime)
});

sf.factory('xhrInterceptor', ['$q','$rootScope', function($q, $rootScope) {
	return {
		'request': function(config) {
			if(config.url == "/user/handler" && config.data.action !=undefined && config.data.action != "getUserCToken")
				config.data.ctoken = $rootScope.ctoken;
			return config;
		},
		'response': function(response) {
			if(undefined!=response && undefined!=response.data){
				try{
					if(response.data.indexOf('loginButton')>0){
						document.location = "/index.html";
					}
				}catch(err){}
			}
			if(undefined!=response && undefined!=response.data && undefined!=response.data.errorMsg && response.data.errorMsg=="ChangePassword"){
				document.location = "/changePassword.html";
				return;
			}
			if(undefined!=response && undefined!=response.data && undefined!=response.data.errorMsg && response.data.errorMsg=="VerifyEmail"){
				document.location = "/validateEmail.html";
				return;
			}
			return response;
		}
	};
}]);
sf.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
	var original = $location.path;
	$location.path = function (path, reload) {
		if (reload === false) {
			var lastRoute = $route.current;
			var un = $rootScope.$on('$locationChangeSuccess', function () {
				$route.current = lastRoute;
				un();
			});
		}
		return original.apply($location, [path]);
	};
}])


sf.config(['$httpProvider', function($httpProvider) {  
	$httpProvider.interceptors.push('xhrInterceptor');
}]);

sf.controller('navigation',['$rootScope','$scope','server','$timeout','$http','$location',function($rootScope,$scope,server,$timeout,$http,$location){

	$scope.user = server.user;

	$scope.logout = function(){
		server.doUserLogout();
	}

	$rootScope.historyBack = function(){
		window.history.back();
	}
	$scope.notifications = [];
	var ctokenReq = {
			method: 'POST',
			url: '/user/handler',
			data: { action: 'getUserCToken'}
	}
	var getExercisesReq = {
			method: 'POST',
			url: '/user/handler',
			data: { action: 'getExercises'}
	}
	var getPathsReq = {
			method: 'POST',
			url: '/user/handler',
			data: { action: 'getPaths'}
	}
	var getHistoryReq = {
			method: 'POST',
			url: '/user/handler',
			data: { action: 'getUserHistory'}
	}



	$scope.goTo = function(link){
		$location.path(link.replace("#",""), false);
	}
	$scope.$on('unreadNotifications:updated', function(event,data) {
		$scope.notifications = data; 
	})
	$scope.userCredits = -1;
	$scope.$on('userInfo:updated', function(event,data) {
		$scope.userCredits = data.credits; 
	})
	$scope.markAllNotificationsRead = function(){
		server.markNotificationAsRead(-1);
	}
	$scope.markNotificationRead = function(id){
		server.markNotificationAsRead(id);
	}

	$rootScope.$on('$locationChangeSuccess', function(event, newUrl, oldUrl){

		if(newUrl.indexOf('d2h-')>-1){
			return;
		}
		var target = newUrl.substr(newUrl.indexOf("#")).replace("#","").replace("/","");
		target = target.split("/")
		switch(target[0]){      
		case "dashboard":
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.history = false;
			$rootScope.visibility.achievements = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.welcome = false;
			$rootScope.visibility.home = false;
			$rootScope.visibility.runningExercises = false;
			$rootScope.visibility.leaderboard = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.dashboard = true;
			$(window).scrollTop(0);
			break;
		case "home":
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.history = false;
			$rootScope.visibility.achievements = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.dashboard = false;
			$rootScope.visibility.welcome = true;
			$rootScope.visibility.home = true;
			$rootScope.visibility.runningExercises = false;
			$rootScope.visibility.leaderboard = false;
			$rootScope.visibility.challenges = false;
			$(window).scrollTop(0);
			break;
		case "tournaments":
			if(target[1]=="details" && undefined!=target[2] && ""!=target[2]){
				var exId = target[2]
				if($rootScope.ctoken == ""){
					$http(ctokenReq).then(function successCallback(response) {
						$rootScope.ctoken = response.data.ctoken;
						$rootScope.selectedChallenge = exId;
						server.getChallengeDetails(exId);
					}, function errorCallback(response) {
						console.log('ajax error');
					});
				}
				else{
					server.getChallengeDetails(exId);
				}
				$rootScope.showChallengeDetails = true;
				$rootScope.showChallengesList = false;
			}
			else{
				$rootScope.showChallengeDetails = false;
				$rootScope.showChallengesList = true;
			}
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.history = false;
			$rootScope.visibility.achievements = false;
			$rootScope.visibility.dashboard = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.welcome = false;
			$rootScope.visibility.home = false;
			$rootScope.visibility.runningExercises = false;
			$rootScope.visibility.leaderboard = false;
			$rootScope.visibility.challenges = true;
			$(window).scrollTop(0);
			break;
		case "exercises":
			console.log('in nav exercises')
			if(target[1]=="details"){
				$rootScope.visibility.assignedExercises = false;
				$rootScope.visibility.exerciseDetails = true;
				$rootScope.visibility.runningExercises = false;    
				$rootScope.visibility.welcome = false;
				$rootScope.visibility.home = false;
				$rootScope.visibility.history = false;
				$rootScope.visibility.achievements = false;
				$rootScope.visibility.settings = false;
				$rootScope.visibility.dashboard = false;
				$rootScope.visibility.challenges = false;
				$rootScope.visibility.leaderboard = false;
				if(undefined != target[2]  && ""!=target[2] && undefined==$rootScope.exerciseDetails.id){
					getExDetails(target[2]);
				}
			}
			else{
				$rootScope.visibility.welcome = false;
				$rootScope.visibility.home = false;
				$rootScope.visibility.assignedExercises = true;
				$rootScope.visibility.history = false;
				$rootScope.visibility.achievements = false;
				$rootScope.visibility.settings = false;
				$rootScope.visibility.challenges = false;
				$rootScope.visibility.dashboard = false;
				$rootScope.visibility.exerciseDetails = false;
				$rootScope.visibility.runningExercises = false;    
				$rootScope.visibility.leaderboard = false;
				$rootScope.exerciseStarted = false;
				$rootScope.ready = false;
				$rootScope.alreadyLaunched = false;
				if(undefined==target[1]){
					$location.path("exercises/vulnerability", true);
					return;
				}
				else if("search"==target[1]){
					if($rootScope.availableExercises.length==0){
						$http(ctokenReq).then(function successCallback(response) {
							$rootScope.ctoken = response.data.ctoken;
							$http(getExercisesReq).then(function successCallback(response) {
								$rootScope.availableExercises = response.data;
								$rootScope.showExercisesForTags(decodeURI(target[2]));
							})
						})
					}
					else{
						$rootScope.showExercisesForTags(decodeURI(target[2]));
					}
					$(window).scrollTop(0);
					return;
				}
				else if("vulnerability"==target[1]){
					if(undefined!=target[2] && ""!=target[2]){
						if($rootScope.availableExercises.length==0){
							$http(ctokenReq).then(function successCallback(response) {
								$rootScope.ctoken = response.data.ctoken;
								$http(getExercisesReq).then(function successCallback(response) {
									$rootScope.availableExercises = response.data;
									$rootScope.trainingExBrowse = "vulnerability";
									$rootScope.showExercisesForTechnology(decodeURI(target[2]))
								}, function errorCallback(response) {
									console.log('ajax error');
								});
							});
						}
						else{
							$rootScope.trainingExBrowse = "vulnerability";
							$rootScope.showExercisesForTechnology(decodeURI(target[2]))
						}
					}
					else{
						if($rootScope.availableExercises.length==0){
							$http(ctokenReq).then(function successCallback(response) {
								$rootScope.ctoken = response.data.ctoken;
								$http(getExercisesReq).then(function successCallback(response) {
									$rootScope.browseVulnerabilities();
								}, function errorCallback(response) {
									console.log('ajax error');
								});
							});
						}
						else{
							$rootScope.browseVulnerabilities();
						}
					}
					$(window).scrollTop(0);
					return;
				}
				else if("learning"==target[1]){
					$rootScope.trainingExBrowse = "paths";
					if(undefined!=target[2] && ""!=target[2]){
						if($rootScope.availableExercises.length == 0){
							$http(ctokenReq).then(function successCallback(response) {
								$rootScope.ctoken = response.data.ctoken;
								$http(getExercisesReq).then(function successCallback(response) {
									$rootScope.availableExercises = response.data;
									if(undefined!=target[3]){
										$http(getPathsReq).then(function successCallback(response) {
											$rootScope.learningPaths = response.data;
											$http(getHistoryReq).then(function successCallback(response) {
												for(var i in $rootScope.learningPaths){
													if($rootScope.learningPaths.hasOwnProperty(i) && $rootScope.learningPaths[i].uuid == target[3]){
														$rootScope.getPathDetails($rootScope.learningPaths[i])
														break;
													}
												}
											});
										});
									}
									else{
										$http(getPathsReq).then(function successCallback(response) {
											$rootScope.showExercisesForTechnology(decodeURI(target[2]))
										});
									}
								}, function errorCallback(response) {
									console.log('ajax error');
								});
							});
						}
						else{
							if(undefined!=target[3]){
								for(var i in $rootScope.learningPaths){
									if($rootScope.learningPaths.hasOwnProperty(i) && $rootScope.learningPaths[i].uuid == target[3]){
										$rootScope.getPathDetails($rootScope.learningPaths[i])
										break;
									}
								}
							}
							else{
								$rootScope.showExercisesForTechnology(decodeURI(target[2]))
							}
						}

					}
					else{
						if($rootScope.availableExercises.length == 0){
							$http(ctokenReq).then(function successCallback(response) {
								$rootScope.ctoken = response.data.ctoken;
								$http(getExercisesReq).then(function successCallback(response) {
									$rootScope.availableExercises = response.data;
									$http(getPathsReq).then(function successCallback(response) {
										$rootScope.learningPaths = response.data;
										$rootScope.browseLearningPaths();
									});
								});
							});
						}
					}
					$(window).scrollTop(0);
					return;
				}
			}
			break;
		case "running":
			$rootScope.visibility.welcome = false;
			$rootScope.visibility.home = false;
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.history = false;
			$rootScope.visibility.achievements = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.leaderboard = false;
			$rootScope.visibility.dashboard = false;
			$rootScope.visibility.runningExercises = true;
			$(window).scrollTop(0);
			break;
		case "history":
			$rootScope.visibility.runningExercises = false;
			$rootScope.visibility.welcome = false;
			$rootScope.visibility.home = false;
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.history = true;
			$rootScope.visibility.achievements = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.leaderboard = false;
			$rootScope.visibility.dashboard = false;
			if(undefined!=target[1]){
				if($rootScope.ctoken == ""){
					$http(ctokenReq).then(function successCallback(response) {
						$rootScope.showResultsFor(target[2]);
						try{
							document.getElementById('exerciseResultsPane').scrollIntoView();
						}catch(e){}
					});
				}
				else{
					$rootScope.showResultsFor(target[2],0);
					try{
						document.getElementById('exerciseResultsPane').scrollIntoView();
					}catch(e){}
				}
			}
			else{
				try{
					$(window).scrollTop(0);
				}catch(e){}
			}
			break;
		case "achievements":
			$rootScope.visibility.leaderboard = false;
			$rootScope.visibility.runningExercises = false;
			$rootScope.visibility.welcome = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.home = false;
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.history = false;
			$rootScope.visibility.achievements = true;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.dashboard = false;
			$(window).scrollTop(0);
			break;
		case "settings":
			$rootScope.visibility.leaderboard = false;
			$rootScope.visibility.runningExercises = false;
			$rootScope.visibility.welcome = false;
			$rootScope.visibility.home = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.history = false;
			$rootScope.visibility.achievements = false;
			$rootScope.visibility.settings = true;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.dashboard = false;
			$(window).scrollTop(0);
			break;
		case "leaderboard":
			$rootScope.visibility.leaderboard = true;
			$rootScope.visibility.runningExercises = false;
			$rootScope.visibility.welcome = false;
			$rootScope.visibility.home = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.history = false;
			$rootScope.visibility.achievements = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.dashboard = false;
			$(window).scrollTop(0);
			break;
		default:{
			$rootScope.visibility.runningExercises = false;    
			$rootScope.visibility.assignedExercises = false;
			$rootScope.visibility.history = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.achievements = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exerciseDetails = false;
			$rootScope.visibility.welcome = false;
			$rootScope.visibility.home = false;
			$rootScope.visibility.dashboard = true;
			$(window).scrollTop(0);
			break;
		}
		}        
	});

	function getExDetails(uuid){

		if(undefined=!$rootScope.ctoken || $rootScope.ctoken!=""){
			var msg = {};
			msg.action = 'getUserCToken';

			var req = {
					method: 'POST',
					url: '/user/handler',
					data: msg,
			}
			$http(req).then(function successCallback(response) {
				if(undefined==response || undefined==response.data || undefined==response.data.ctoken){
					setTimeout(function(){getExDetails(uuid);},500);
					return;
				}
				$rootScope.ctoken = response.data.ctoken;

				server.getExerciseDetails(uuid);
				server.getRegionsForExercise(uuid);
				server.getInitialData();
			}, function errorCallback(response) {
				console.log('ajax error');
			});
		}
		else{
			server.getExerciseDetails(uuid);
			server.getRegionsForExercise(uuid);
			server.getInitialData();
		}

	}

	function getAllExercises(){

		var msg = {};
		msg.action = 'getUserCToken';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.ctoken = response.data.ctoken;

			server.getAvailableExercises();

		}, function errorCallback(response) {
			console.log('ajax error');
		});


	}

	$rootScope.visibility = {}
	$rootScope.visibility.welcome = true;
	$rootScope.visibility.home = true;
	$rootScope.visibility.assignedExercises = false;
	$rootScope.visibility.history = false;
	$rootScope.visibility.achievements = false;
	$rootScope.visibility.settings = false;
	$rootScope.visibility.exerciseDetails = false;
	$rootScope.visibility.runningExercises = false;    
	$rootScope.visibility.leaderboard = false;
}])

sf.controller('welcome',['$scope','server',function($scope,server){
	$scope.user = server.user;
}])
sf.controller('home',['$scope','server',function($scope,server){
	$scope.user = server.user;
}])
sf.controller('dashboard',['$scope','$rootScope','server',function($scope,$rootScope,server){
	$scope.user = server.user;
	$rootScope.pInfoReady = false;

	$scope.tableconfig = {
			itemsPerPage: 10,
			fillLastPage: false
	}


	$scope.$on('userStatsAreReady:updated', function(event,data) {

		var data = cloneObj(data);
		data.options.maintainAspectRatio = false;
		data.options.layout = {
				padding: {
					left: 0,
					right: 0,
					top: 0,
					bottom: 0
				}
		}

		$scope.userRemediation = data;

	});
	$scope.recentChallenges = [];

	$scope.$on('challengesAreAvailable:updated', function(event,data) {

		var tmpChallenges = [];
		for(i in data){
			if(data[i].status == "IN_PROGRESS"){
				tmpChallenges.push(data[i]);
			}
		}
		var l = tmpChallenges.length;
		if(tmpChallenges.length>2)
			tmpChallenges = tmpChallenges.slice(l-2);
		$scope.recentChallenges = tmpChallenges;
	});

	$scope.$on('runningExercises:updated', function(event,data) {

		$scope.latestRunning = [];
		var tmpRunning = [];
		for(i in data){
			if(data[i].status == "RUNNING"){
				tmpRunning.push(data[i]);
			}
		}
		var l = tmpRunning.length;
		if(tmpRunning.length>2)
			tmpRunning = tmpRunning.slice(l - 2)
			$scope.latestRunning = tmpRunning;


	});
	$scope.getCurrentScore = function(score){
		if(score==-1)
			return 0+" points";
		if(score==1)
			return "1 point";
		else return score + " points";
	}


	$scope.$on('statsAreReady:updated', function(event,data) {

		var tmpLatestStats = cloneObj(data);
		var l = tmpLatestStats.exercises.length;
		if(tmpLatestStats.exercises.length>10){
			tmpLatestStats.data[1] = tmpLatestStats.data[1].slice(l-10)
			tmpLatestStats.data[0] = tmpLatestStats.data[0].slice(l-10)
			tmpLatestStats.exercises = tmpLatestStats.exercises.slice(l-10)
			tmpLatestStats.labels = tmpLatestStats.labels.slice(l-10)
		}
		tmpLatestStats.options.maintainAspectRatio = false;
		tmpLatestStats.options.tooltips = {
				callbacks: {
					afterTitle: function(tooltipItems, data) {
						if(undefined==$scope.latestStats.exercises[tooltipItems[0].index])
							return "";
						return $scope.latestStats.exercises[tooltipItems[0].index];
					}
				},
				footerFontStyle: 'normal'
		};
		tmpLatestStats.options.layout = {
				padding: {
					left: 0,
					right: 0,
					top: 0,
					bottom: 0
				}
		}
		$scope.latestStats = tmpLatestStats;

	});
	$scope.latestExercises = [];
	$scope.$on('userHistoryIsReady:updated', function(event,data) {

		var tmpLatestExercises = cloneObj(data);
		var l = tmpLatestExercises.length;
		if(tmpLatestExercises.length>7)
			tmpLatestExercises = tmpLatestExercises.slice(l-7);
		$scope.latestExercises = tmpLatestExercises;

	});


	$scope.$on('achievements:updated', function(event,data) {

		$scope.userAchievements = data;
		if(undefined!=data.achievements){
			$scope.trophies = [];
			$scope.placements = [];
			$scope.certifications = [];

			for (var i in data.achievements){
				if(data.achievements.hasOwnProperty(i) && undefined!=data.achievements[i].achievement && undefined!=data.achievements[i].achievement.type){
					if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="TROPHY"){
						$scope.trophies.push(data.achievements[i]);
					}
					else if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="CHALLENGE"){
						$scope.placements.push(data.achievements[i]);
					}
					else if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="CERTIFICATION"){
						$scope.certifications.push(data.achievements[i]);
					}
				}

			}
		}
	});


}])
sf.controller('running',['$scope','server','$rootScope',function($scope,server,$rootScope){
	$rootScope.runningInstances = [];
	$rootScope.crashedInstances = [];
	$scope.lastUpdated = "";

	$scope.updateRunningExercises = function(){
		server.getRunningExercises();
	}

	$scope.cancelCrashed = function(id){
		server.cancelCrashedExercise(id);
	}
	$scope.acceptCrashed = function(id){
		$('.waitLoader').show();
		server.acceptCrashedInstanceResult(id);
	}

	$scope.$on('acceptCrashedInstanceResult:updated', function(event,data) {
		server.getRunningExercises();
		server.getUserAchievements();
		server.getUserStats();
		server.getUserTeamLeaderboard();
		server.getTeamStats();
		server.getUserHistory();
	});
	$scope.restartCrashed = function(id,uuid){
		server.getExerciseDetails(uuid);
		server.getRegionsForExercise(uuid);
		$rootScope.exerciseRestartCrashed = true;
		$rootScope.exerciseRestartCrashedId = id;
		$rootScope.ready = false;
		$rootScope.alreadyLaunched = false;
	}
	$scope.$on('crashedExerciseCancelled:updated', function(event,data) {
		server.getRunningExercises();
		server.getUserAchievements();
		server.getUserStats();
		server.getUserTeamLeaderboard();
		server.getTeamStats();
		server.getUserHistory();
	});

	$scope.$on('runningExercises:updated', function(event,data) {
		$rootScope.runningInstances = [];
		$rootScope.crashedInstances = [];
		$scope.lastUpdated = new Date();
		for(i in data){
			if(data[i].status == "RUNNING"){
				$rootScope.runningInstances.push(data[i]);
			}else if (data[i].status == "CRASHED") {
				$rootScope.crashedInstances.push(data[i]);
			}
		}
	})
	$rootScope.stopExercise = function(id){
		$('.waitLoader').show();
		server.stopInstance(id);
	}
	$rootScope.resumeExercise = function(exId,uuid){
		server.getExerciseDetails(uuid);
		$rootScope.exInstanceId = exId;
		$rootScope.exerciseRestartCrashed = false;
		$rootScope.exerciseRestartCrashedId = undefined;
		$rootScope.exerciseStarted = true;
		$rootScope.ready = true;
		$rootScope.alreadyLaunched = false;
		$rootScope.synId =  "exercise.html#/id="+$rootScope.exInstanceId;
	}
}])


sf.controller('leaderboard',['$scope','server','$rootScope',function($scope,server,$rootScope){
	$scope.leaderboardData = [];
	$scope.user = server.user;
	$scope.$on('leaderboard:updated', function(event,data) {
		$scope.leaderboardData = data.leaderboard;
		$scope.lastUpdated = new Date();
	});

	$scope.updateTeamStats = function(){
		server.getUserTeamLeaderboard();
		server.getTeamStats();
	}

	$scope.leaderboardtableconfig = {
			itemsPerPage: 30,
			fillLastPage: false
	}

	$scope.remediatedPerIssue = {}
	$scope.remediatedPerIssue.data = [];
	$scope.remediatedPerIssue.labels = [];
	$scope.remediatedPerCategory = {}
	$scope.remediatedPerCategory.data = [];
	$scope.remediatedPerCategory.labels = [];
	$scope.remediationRatePerIssue = [];
	$scope.remediationRatePerCategory = [];
	$scope.scorePerExercises = {};
	$scope.options = {}
	$scope.options.animation = false;
	$scope.options.layout = {
			padding: {
				left: 0,
				right: 0,
				top: 0,
				bottom: 0
			}
	}
	$scope.options.pieceLabel = {
			// render 'label', 'value', 'percentage' or custom function, default is 'percentage'
			render: 'percentage',
			// precision for percentage, default is 0
			precision: 0,
			// identifies whether or not labels of value 0 are displayed, default is false
			showZero: false,
			// font size, default is defaultFontSize
			fontSize: 12,
			// font color, can be color array for each data, default is defaultFontColor
			fontColor: '#FFF',
			// font style, default is defaultFontStyle
			fontStyle: 'normal',
			// font family, default is defaultFontFamily
			fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			// draw label in arc, default is false
			arc: false,
			// position to draw label, available value is 'default', 'border' and 'outside'
			// default is 'default'
			position: 'border',
			// draw label even it's overlap, default is false
			overlap: true
	}
	$scope.options.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};
	$scope.options.title = {
			text : "",
			display:true,
			position: 'bottom'

	}
	$scope.radarOptions = {}
	$scope.radarOptions.animation = false;
	$scope.radarOptions.title = {
			text : "",
			display:true,
			position: 'top'

	}
	$scope.radarOptions.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};

	$scope.$on('statsTeam:updated', function(event,data) {
		$scope.remediationRatePerIssue = [];
		for (var property in data.issuesRemediationRate) {
			if ("null"!=property && data.issuesRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.issuesRemediationRate[property]).length>0) {

				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.fontSize=11

				obj.options.title.text = property;
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality", "Partially Remediated"];
				var alwaysZero = true;				
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.issuesRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
					if(tmpValue!=0)
						alwaysZero = false;
				}
				if(!alwaysZero){
					$scope.remediationRatePerIssue.push(obj);	
				}
			}
		}
		$scope.remediatedPerIssue.data = [[]];
		$scope.remediatedPerIssue.labels = [];
		$scope.remediatedPerIssue.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerIssue.options.title.display = false;
		$scope.remediatedPerIssue.series = ["Remediated (%)"];

		for(var j in $scope.remediationRatePerIssue){
			if ($scope.remediationRatePerIssue.hasOwnProperty(j)){
				var tmpRem = $scope.remediationRatePerIssue[j].data[0]
				var tmpTot = $scope.remediationRatePerIssue[j].data.reduce(getSum);
				var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);
				var tmpName = $scope.remediationRatePerIssue[j].options.title.text
				$scope.remediatedPerIssue.labels.push(tmpName);
				$scope.remediatedPerIssue.data[0].push(tmpPercentage);
			}
		}

		$scope.remediationRatePerCategory = [];
		for (var property in data.categoriesRemediationRate) {
			if (data.categoriesRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.categoriesRemediationRate[property]).length>0) {
				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.text = property;
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.categoriesRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
				}
				$scope.remediationRatePerCategory.push(obj);
			}
		}
		$scope.remediatedPerCategory.data = [[],[],[]];
		$scope.remediatedPerCategory.labels = [];
		for(var j in $scope.remediationRatePerCategory){
			if ($scope.remediationRatePerCategory.hasOwnProperty(j)){
				var tmpRem = $scope.remediationRatePerCategory[j].data[0]
				var tmpTot = $scope.remediationRatePerCategory[j].data.reduce(getSum);
				var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);
				var tmpName = $scope.remediationRatePerCategory[j].options.title.text;
				$scope.remediatedPerCategory.labels.push(tmpName);
				$scope.remediatedPerCategory.data[0].push(tmpPercentage);
				$scope.remediatedPerCategory.data[1].push(Math.ceil(data.totalMinutesPerIssueCategory[tmpName]/60));
				$scope.remediatedPerCategory.data[2].push(data.avgMinutesPerIssueCategory[tmpName]);
			}
		}
		$scope.remediatedPerCategory.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerCategory.options.scale = {
				ticks: {
					beginAtZero: true,
					max: 100,
					min: 0
				}
		}
		$scope.remediatedPerCategory.options.title.display = false;
		$scope.remediatedPerCategory.series = ["Total Time Spent (hours)","Avg Time / Exercise (minutes)"];


		var tmpRemediatedPerCategoryPercentage = cloneObj($scope.remediatedPerCategory);
		var tmpRemediatedPerCategoryTime = cloneObj($scope.remediatedPerCategory);

		tmpRemediatedPerCategoryPercentage.data = tmpRemediatedPerCategoryPercentage.data.slice(0,1);
		tmpRemediatedPerCategoryPercentage.series = ["Remediated (%)"];

		tmpRemediatedPerCategoryTime.data = tmpRemediatedPerCategoryTime.data.slice(1);

		$scope.remediatedPerCategoryTime = tmpRemediatedPerCategoryTime;
		$scope.remediatedPerCategoryPercentage = tmpRemediatedPerCategoryPercentage;

	});
}]);


sf.controller('exercises',['$scope','server','$route','$rootScope','$interval','$location','$http','notificationService','$sce',function($scope,server,$route,$rootScope,$interval,$location,$http,notificationService,$sce){
	$scope.user = server.user;
	$scope.exercises = [];
	$rootScope.exerciseStarted = false;
	$rootScope.ready = false;
	$rootScope.alreadyLaunched = false;
	$scope.currentDemoToken = server.currentDemoToken;
	$scope.guacToken;
	var intervalT;
	$rootScope.exerciseIsInChallenge = false;
	$scope.resultStatusData = {}
	$rootScope.exInstanceId;
	$scope.asNotStarted = false;
	$scope.flagIsVulnerable = false;
	$rootScope.exerciseDetails = {};
	$scope.currentExerciseResults = server.currentExerciseResults;
	$scope.emptyExercises = true;
	$scope.availableRegions = ["Unavailable"];
	$scope.selectedRegion = "";
	$scope.assignedExercises = [];
	$rootScope.takenExercises = [];
	$rootScope.solvedExercises = [];
	$rootScope.timesPerExercise = {};
	var exerciseTechnologies = [];
	$scope.currentSearch = "";
	$scope.currentTechnology = "";
	$rootScope.availableExercises = [];
	$rootScope.kbItems = [];
	$rootScope.isEdge = navigator.userAgent.toLowerCase().indexOf('edge') != -1;
	$rootScope.isIE = navigator.userAgent.toLowerCase().indexOf('trident') != -1;
	$scope.userAchievements = {}
	$scope.$on('achievements:updated', function(event,data) {
		$scope.userAchievements = data;
	});

	var scoreClassMap = {	
			success: "success",
			average: "warning",
			failure: "danger",
			pending: "info",
			active: "active"

	};

	$rootScope.getBackgroundForExercise = function(id){
		if($rootScope.didTakeExercise(id)){
			if($rootScope.didSolveExercise(id)){
				return 'backgroundCompleted';
			}
			else{
				return 'backgroundCompletedFailed';
			}
		}
		else{
			return 'backgroundNotCompleted'
		}
	}

	$rootScope.getIconForExercise = function(id){
		if($rootScope.didTakeExercise(id)){
			if($rootScope.didSolveExercise(id)){
				return 'glyphicon-ok';
			}
			else{
				return 'glyphicon-remove';
			}
		}
		else{
			return 'glyphicon-play'
		}
	}

	$rootScope.getClassForCompletion = function(completion){		
		if(completion<=20)
			return scoreClassMap["failure"];
		if(completion<=50)
			return scoreClassMap["average"];
		if(completion<=75)
			return scoreClassMap["pending"];
		if(completion<=100)
			return scoreClassMap["success"];
		return scoreClassMap["average"];
	}

	$rootScope.getColorForScore = function(score){
		if(score<=20)
			return "rgba(118, 147, 193, 0.94)";
		if(score<=50)
			return "rgba(158, 74, 114, 0.87)";
		if(score<=75)
			return "rgba(118, 118, 193, 0.94)";
		if(score<=100)
			return "rgba(106, 106, 125, 0.87)";
		if(score<=125)
			return "rgba(56, 31, 8, 0.87)";
		return "rgba(189, 108, 34, 0.87)";
	}


	$scope.stopExercise = function(id){
		$('.waitLoader').show();
		server.stopInstance(id);
	}
	$rootScope.didTakeExercise = function(eid){
		return $rootScope.takenExercises.includes(eid); 
	}

	$rootScope.timesExercise = function(eid){
		return $rootScope.timesPerExercise[eid] || 0;
	}
	$rootScope.getTimesString = function(number){
		if(number == 0 || number > 2){
			return number+" times"
		}
		else if(number == 1){
			return "once"
		}
		else if(number == 2){
			return "twice"
		}
	}

	$rootScope.didSolveExercise = function(eid){
		return $rootScope.solvedExercises.includes(eid);
	}

	$scope.animateElementIn = function($el) {
		$el.removeClass('timeline-hidden');
		$el.addClass('bounce-in');
	};

	$scope.animateElementOut = function($el) {
		$el.addClass('timeline-hidden');
		$el.removeClass('bounce-in');
	};

	$scope.userAssignedExercises = [];

	Array.prototype.uniqueId = function() {
		var a = this.concat();
		for(var i=0; i<a.length; ++i) {
			for(var j=i+1; j<a.length; ++j) {
				if(a[i].exercise.id === a[j].exercise.id)
					a.splice(j--, 1);
			}
		}

		return a;
	};

	$scope.$on('assignedExercises:updated', function(event,data) {
		if(null == data){
			$scope.userAssignedExercises = [];
			return;   
		}
		for(var i=0;i<data.length;i++){
			if(!data[i].exercise){
				try{
					data.remove(i,i);
				}catch(err){
					continue;
				}
			}
			else{
				data[i].t = "u";
			}
		}

		$scope.userAssignedExercises = data;
		$scope.assignedExercises = userAssignedExercises;

	})
	$scope.getAssignedString = function(s){
		if(s=="u"){
			return "Assigned to you";
		}
		else if(s=="t")
			return "Assigned to your team";
	}


	$scope.getExerciseStatusString = function(status){
		switch(status){
		case "0":
			return "Available"
		case "1":
			return "Updated"
		case "2":
			return "Coming Soon"
		case "3":
			return "Not Available"
		case "4":
			return "Available"
		default:
			break;
		}

	}
	$scope.exercisesTech = [];
	$scope.showTechnologies = function(data){
		exerciseTechnologies = [];
		$scope.exercisesTech = [];
		for(var i in data){
			if(data.hasOwnProperty(i) && undefined!=data[i].technology){
				if(exerciseTechnologies.indexOf(data[i].technology)<0)
					exerciseTechnologies.push(data[i].technology)
			}
		}

		var size = 3;
		while (exerciseTechnologies.length > 0){
			$scope.exercisesTech.push(exerciseTechnologies.splice(0, size));
		}

		if($scope.exercisesTech.length<=0){
			$scope.emptyExercises = true;
		}
		else{
			$scope.emptyExercises = false;
		}
	};

	$scope.backToTechnologies = function(){
		$scope.currentTechnology = "";
		$scope.currentSearch = "";
		$rootScope.avExercisesList = false;
		$rootScope.avLearningList = false;
		$rootScope.avPathDetails = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avExercisesTech = true;


		$location.path("exercises/vulnerability", false);
		$scope.showTechnologies($rootScope.availableExercises);
		/*
		if($rootScope.trainingExBrowse == "vulnerability"){
			$location.path("exercises/vulnerability", false);
			$scope.showTechnologies($rootScope.availableExercises);
		}
		else{
			$location.path("exercises/learning", false);
			//$rootScope.showAllTrainingPaths();
			//$scope.showTechnologies($rootScope.learningPaths);

		}*/

	}
	$rootScope.trainingExBrowse = "";
	$rootScope.trainingExTech = "";

	$scope.getBrowseName = function(){
		if($rootScope.trainingExBrowse == "vulnerability")
			return "Vulnerability";
		else
			return "Learning Path";
	}
	$scope.getBrowseImage = function(){
		if($rootScope.trainingExBrowse == "vulnerability")
			return "/assets/img/vulnerability.svg";
		else 
			return "/assets/img/learning.svg";
	}


	$rootScope.browseVulnerabilities = function(){
		$scope.showTechnologies($rootScope.availableExercises);
		$rootScope.avExercisesList = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avLearningList = false;
		$rootScope.avPathDetails = false;
		$rootScope.avExercisesTech = true;
		$rootScope.trainingExBrowse = "vulnerability"
			$location.path("exercises/vulnerability", false);
	}

	$rootScope.browseLearningPaths = function(){
		//$scope.showTechnologies($rootScope.learningPaths);
		$rootScope.showAllTrainingPaths();
		$rootScope.avExercisesList = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avLearningList = true;
		$rootScope.avPathDetails = false;
		$rootScope.avExercisesTech = false;
		$rootScope.trainingExBrowse = "paths"
			$location.path("exercises/learning", false);
	}
	$rootScope.learningPathStatus = [{"id":"0","name":"Available"},{"id":"1","name":"Draft"},{"id":"2","name":"Inactive"},{"id":"3","name":"Deprecated"},{"id":"4","name":"Coming Soon"}]
	var difficultyComparator = function(o1,o2){
		if(undefined==o1 || undefined==o2 || undefined==o1.difficulty || undefined==o2.difficulty)
			return 0;
		const v1 = o1.difficulty.toLowerCase();
		const v2 = o2.difficulty.toLowerCase();
		if(v1==2)
			return 0;
		if(v1=="easy")
			return -1;
		if(v1=="hard")
			return 1;
		if(v1=="moderate" && v2 == "easy")
			return 1;
		if(v1=="moderate" && v2 == "hard")
			return -1;

	}

	$scope.getImageForDifficulty = function(difficulty){
		if(undefined==difficulty || null == difficulty)
			return "";
		difficulty = difficulty.toLowerCase();
		switch(difficulty){
		case "easy":
			return "/assets/img/easy.png";
		case "moderate":
			return "/assets/img/moderate.png";
		case "hard":
			return "/assets/img/hard.png";
		default:
			return "";
		}
	}
	$rootScope.currentPath = {};
	$scope.viewPathDetails = function(tech,pathId){
		$location.path("exercises/learning/"+tech+"/"+pathId,false)
	}
	$rootScope.getPathDetails = function(path){
		var takenExercises = 0;
		var solvedExercises = 0;
		for(var i=0;i<path.exercises.length;i++){
			if(undefined==path.exercises[i].id){
				for(var j=0;j<$rootScope.availableExercises.length;j++){
					if(undefined!=$rootScope.availableExercises[j] && $rootScope.availableExercises[j].uuid == path.exercises[i]){
						console.log(path.exercises[i]+" found")
						path.exercises[i] = $rootScope.availableExercises[j];
						break;
					}
				}
			}
			if($rootScope.didTakeExercise(path.exercises[i].uuid)){
				takenExercises++;
				if( $rootScope.didSolveExercise(path.exercises[i].uuid)){
					solvedExercises++;
				}
			}
		}
		$scope.currentPathCompletion = Math.round((solvedExercises / path.exercises.length)*100);
		$rootScope.currentPath = path;
		$rootScope.avExercisesTech = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avExercisesList = false;
		$rootScope.avLearningList = false;
		$rootScope.avPathDetails = true;
	}
	$rootScope.completedPaths = [];
	$scope.isPathCompleted = function(id){
		return isPathCertified(id);
	}

	function isPathCertified(pathId){
		if(undefined!=$scope.userAchievements && undefined!=$scope.userAchievements.achievements){
			for(var i in $scope.userAchievements.achievements){
				if($scope.userAchievements.achievements.hasOwnProperty(i) && undefined!=$scope.userAchievements.achievements[i].achievement && undefined!=$scope.userAchievements.achievements[i].achievement.type && $scope.userAchievements.achievements[i].achievement.type == "CERTIFICATION" && $scope.userAchievements.achievements[i].achievement.pathId == pathId){
					return true;
				}
			}
			return false;
		}
	}

	$scope.$on('userHistory:updated', function(event,data) {	
		setTimeout(function(){checkCompletedPaths();},800);
	})

	function checkCompletedPaths(){
		for(var i in $rootScope.learningPaths){
			if($rootScope.learningPaths[i].status != "0" || isPathCertified($rootScope.learningPaths[i].id)){
				continue;
			}
			var completed = true;
			for(var j in $rootScope.learningPaths[i]['exercises']){
				if($rootScope.learningPaths[i]['exercises'].hasOwnProperty(j)){
					if(typeof $rootScope.learningPaths[i]['exercises'][j] === 'string')
						var id =  $rootScope.learningPaths[i]['exercises'][j];
					else
						var id =  $rootScope.learningPaths[i]['exercises'][j]['uuid']

					if(!$rootScope.didTakeExercise(id) || !$rootScope.didSolveExercise(id)){
						completed = false;
						break;
					}
				}
			}
			if(completed){
				server.applyForPathCertification($rootScope.learningPaths[i].id)
			}	
		}
	}

	$rootScope.learningPaths = [];
	$scope.$on('paths:updated', function(event,data) {
		$rootScope.learningPaths = data;

		if($rootScope.takenExercises.length >0){
			checkCompletedPaths();
		}
		else{
			setTimeout(function () {
				checkCompletedPaths();
			},6000);
		}
		if($rootScope.trainingExBrowse == 'paths'){
			//$scope.showTechnologies($rootScope.learningPaths);
			$rootScope.showAllTrainingPaths();
		}

	});

	$rootScope.showAllTrainingPaths = function(){
		$scope.currentTechnology = "";
		$scope.exercises = [];
		$scope.paths = [];
		var tmpExercises = [];
		var tmpPaths = [];
		for(var i in $rootScope.learningPaths){
			if($rootScope.learningPaths.hasOwnProperty(i)){
				tmpPaths.push($rootScope.learningPaths[i])
			}
		}
		var size = 3;
		tmpPaths.sort(difficultyComparator);
		while (tmpPaths.length > 0){
			$scope.paths.push(tmpPaths.splice(0, size));
			$scope.emptyExercises = false;
		}
		if($scope.paths.length<=0){
			$scope.emptyExercises = true;
		}
		$rootScope.avExercisesList = false;
		$rootScope.avLearningList = true;
		$location.path("exercises/learning/", false);
	}

	$rootScope.showExercisesForTechnology = function(tech){
		$scope.currentTechnology = tech;
		$scope.exercises = [];
		$scope.fkExercises = {};
		$scope.paths = [];
		var tmpExercises = [];
		var tmpPaths = [];
		if($rootScope.trainingExBrowse == "vulnerability"){
			for(var i in $rootScope.availableExercises){
				if($rootScope.availableExercises.hasOwnProperty(i)){
					if($rootScope.availableExercises[i].technology==tech){
						tmpExercises.push($rootScope.availableExercises[i]);
						if($scope.fkExercises[$rootScope.availableExercises[i].framework]==undefined){
							$scope.fkExercises[$rootScope.availableExercises[i].framework] = [];
							$scope.fkExercises[$rootScope.availableExercises[i].framework].push($rootScope.availableExercises[i]);
						}
						else{
							$scope.fkExercises[$rootScope.availableExercises[i].framework].push($rootScope.availableExercises[i]);
						}

					}
				}
			}
			if(Object.keys($scope.fkExercises).length<=0){
				$scope.emptyExercises = true;
			}
			else{
				$scope.emptyExercises = false;
			}
			$rootScope.avLearningList = false;
			$rootScope.avExercisesList = true;
			$location.path("exercises/vulnerability/"+tech, false);
		}
		else if($rootScope.trainingExBrowse == "paths"){
			for(var i in $rootScope.learningPaths){
				if($rootScope.learningPaths.hasOwnProperty(i)){
					if($rootScope.learningPaths[i].technology==tech)
						tmpPaths.push($rootScope.learningPaths[i])
				}
			}
			var size = 2;
			tmpPaths.sort(difficultyComparator);
			while (tmpPaths.length > 0){
				$scope.paths.push(tmpPaths.splice(0, size));
				$scope.emptyExercises = false;
			}
			if($scope.paths.length<=0){
				$scope.emptyExercises = true;
			}
			$rootScope.avExercisesList = false;
			$rootScope.avLearningList = true;
			$location.path("exercises/learning/"+tech, false);
		}
		$rootScope.avExercisesTech = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avPathDetails = false;
	} 

	$scope.searchTags = function(){
		$location.path("exercises/search/"+$scope.currentSearch, false);
	}

	$rootScope.showExercisesForTags = function(search){
		if(search.length!=0 && search.length<3){
			return;
		}
		var tmpExercises = [];
		if(search.length==0){
			for(var n in $rootScope.availableExercises){
				if($rootScope.availableExercises.hasOwnProperty(n)){
					if($scope.currentTechnology!=""){
						if($rootScope.availableExercises[n].technology==$scope.currentTechnology)
							tmpExercises.push($rootScope.availableExercises[n]);
					}
					else{
						tmpExercises.push($rootScope.availableExercises[n]);
					}
				}
			}
		}
		else{
			for(var i in $rootScope.availableExercises){
				var added = false;
				if($rootScope.availableExercises.hasOwnProperty(i)){
					if($scope.currentTechnology!="" && ($rootScope.availableExercises[i].technology!=$scope.currentTechnology))
						continue;
					if(undefined!=$rootScope.availableExercises[i].tags){
						for(var j in $rootScope.availableExercises[i].tags){
							if(!added && $rootScope.availableExercises[i].tags.hasOwnProperty(j) && $rootScope.availableExercises[i].tags[j].toLowerCase().indexOf(search.toLowerCase())>=0){
								tmpExercises.push($rootScope.availableExercises[i]);
								added = true;
								break;
							}
						}
					} 
					if(!added && undefined!=$rootScope.availableExercises[i].title && $rootScope.availableExercises[i].title.toLowerCase().indexOf(search.toLowerCase())>=0){
						tmpExercises.push($rootScope.availableExercises[i]);
						added = true;
					}
					if(!added && undefined!=$rootScope.availableExercises[i].subtitle && $rootScope.availableExercises[i].subtitle.toLowerCase().indexOf(search.toLowerCase())>=0){
						tmpExercises.push($rootScope.availableExercises[i]);
						added = true;
					}
					if(!added && undefined!=$rootScope.availableExercises[i].description && $rootScope.availableExercises[i].description.toLowerCase().indexOf(search.toLowerCase())>=0){
						tmpExercises.push($rootScope.availableExercises[i]);
						added = true;
					}
					if(!added && undefined!=$rootScope.availableExercises[i].technology && $rootScope.availableExercises[i].technology.toLowerCase().indexOf(search.toLowerCase())>=0){
						tmpExercises.push($rootScope.availableExercises[i]);
						added = true;
					}
				}
			}
		}
		for(var j=0;j<tmpExercises.length;j++){
			if(!tmpExercises[j].id){
				try{
					tmpExercises.remove(j,j);
					j--;
				}catch(err){
					continue;
				}
			}
		}
		$scope.fkExercises = {};
		for (var l=0; l<tmpExercises.length; l++){
			
			if($scope.fkExercises[tmpExercises[l].technology]==undefined){
				$scope.fkExercises[tmpExercises[l].technology] = [];
				$scope.fkExercises[tmpExercises[l].technology].push(tmpExercises[l]);
			}
			else{
				$scope.fkExercises[tmpExercises[l].technology].push(tmpExercises[l]);
			}
			
			
		}
		
		if(Object.keys($scope.fkExercises).length<=0){
			$scope.emptyExercises = true;
		}
		else{
			$scope.emptyExercises = false;
		}
		$rootScope.avExercisesTech = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avPathDetails = false;
		$rootScope.avLearningList = false;
		$rootScope.avExercisesList = true;
		$scope.currentSearch = search;
		setTimeout(function(){document.getElementById('avExercisesListSearch').focus();},300);

	}



	$scope.$on('exercises:updated', function(event,data) {
		if(undefined!=data){
			$scope.exercises = [];
			$rootScope.availableExercises = data;
			if($rootScope.trainingExBrowse == 'vulnerability'){
				$scope.showTechnologies($rootScope.availableExercises);
			}
		}


	});

	function isKbPresent(kb){
		if(undefined==kb)
			return false;
		for(var j=0; j<$rootScope.exerciseKbs.length; j++){
			if($rootScope.exerciseKbs[j].uuid==kb.uuid)
				return true;
		}
		return false;
	}

	$scope.$on('vulnKBLoaded:updated', function(event,data) {
		var cMd = data.md.text+"\n";
		if(data.kbMapping!=undefined){
			for(var i=0;i<Object.keys(data.kbMapping).length;i++){
				var tmpInner = data.kbMapping[Object.keys(data.kbMapping)[i]]
				cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
			}
		}
		document.querySelector('#kbMD').innerHTML = '';
		var editor = new toastui.Editor.factory({
			el: document.querySelector('#kbMD'),
			viewer: true,
			height: '500px',
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			usageStatistics:false,
			initialValue: ''
		});
		editor.setMarkdown(cMd);
		$('#kbModal').modal('show');
		autoplayMdVideo('#kbMD')
		var kbFound = false;
		for(var j=0; j<$rootScope.kbItems.length; j++){
			if($rootScope.kbItems[j].uuid == data.uuid){
				$rootScope.kbItems[j] = data;
				kbFound = true;
				break;
			}
		}
		if(!kbFound){
			$rootScope.kbItems.push(data)
		}
		$('#kbMD').find('a').each(function() {
			if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
				return;
			var a = new RegExp('/' + window.location.host + '/');
			if(!a.test(this.href)) {
				$(this).click(function(event) {
					event.preventDefault();
					event.stopPropagation();
					window.open(this.href, '_blank');
				});
			}
		});
	})
	$rootScope.kbItems = [];
	$rootScope.openVulnKB = function(item,technology){
		if(undefined!=item && item.uuid!=undefined && item.uuid!=null & item.uuid!="" && technology!=undefined && technology!=null & technology!=""){
			for(var j=0; j<$rootScope.kbItems.length; j++){
				if($rootScope.kbItems[j].uuid == item.uuid && undefined != $rootScope.kbItems[j].md.text && $rootScope.kbItems[j].md.text != ""){
					document.querySelector('#kbMD').innerHTML = '';
					var cMd = $rootScope.kbItems[j].md.text+"\n";
					if($rootScope.kbItems[j].kbMapping!=undefined && $rootScope.kbItems[j].kbMapping[technology]!=undefined){
						var tmpInner = $rootScope.kbItems[j].kbMapping[technology];
						cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
					}
					else if(item.isAgnostic){
						server.loadVulnKB(item.uuid,technology); 
						return;
					}
					var editor = new toastui.Editor.factory({
						el: document.querySelector('#kbMD'),
						viewer: true,
						usageStatistics:false,
						height: '500px',
						plugins: [ codeSyntaxHightlight, colorSyntax ],
						initialValue: ''
					});
					editor.setMarkdown(cMd);
					$('#kbModal').modal('show');
					autoplayMdVideo('#kbMD')
					$('#kbMD').find('a').each(function() {
						if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
							return;
						var a = new RegExp('/' + window.location.host + '/');
						if(!a.test(this.href)) {
							$(this).click(function(event) {
								event.preventDefault();
								event.stopPropagation();
								window.open(this.href, '_blank');
							});
						}
					});
					return;
				}
			}
			server.loadVulnKB(item.uuid,technology);
		}
	}

	$scope.$on('exerciseDetails:updated', function(event,data) {
		data.res = [];
		for (var property in data.resources) {
			if (data.resources.hasOwnProperty(property)) {
				var tmpObj = {};
				tmpObj.name = property
				tmpObj.url = data.resources[property];
				data.res.push(tmpObj);
			}
		}
		$scope.selectedRegion = "";
		$rootScope.exerciseDetails = data;
		$rootScope.exerciseKbs = []; 

		for(var i=0; i<$rootScope.exerciseDetails.flags.length; i++){
			if(undefined!=$rootScope.exerciseDetails.flags[i].kb && !isKbPresent($rootScope.exerciseDetails.flags[i].kb))
				$rootScope.exerciseKbs.push($rootScope.exerciseDetails.flags[i].kb);
		}	
		if($rootScope.exerciseDetails.information != undefined){
			document.querySelector("#exerciseInfoMD").innerHTML = "";
			var editor = new toastui.Editor.factory({
				viewer: true,
				usageStatistics:false,
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				el: document.querySelector("#exerciseInfoMD"),
				initialValue: $rootScope.exerciseDetails.information.text
			});
			autoplayMdVideo('#exerciseInfoMD')
			$('#exerciseInfoMD').find('a').each(function() {
				if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
					return;
				var a = new RegExp('/' + window.location.host + '/');
				if(!a.test(this.href)) {
					$(this).click(function(event) {
						event.preventDefault();
						event.stopPropagation();
						window.open(this.href, '_blank');
					});
				}
			});
		}

		$location.path("exercises/details/"+$rootScope.exerciseDetails.uuid, false);
		$(window).scrollTop(0);
	});

	$scope.$on('hintReceived:updated', function(event,data) {
		if(undefined!=data){
			$scope.hintData = data;
			$("#hintReceivedModal").modal('show');
		}
	});

	$scope.downloadAPIReference = function(id){
		server.downloadExerciseReference(id);
	}


	$scope.getSelfChekResult = function(selfCheckName){
		if(undefined==selfCheckName || undefined==$scope.resultStatusData.length || $scope.resultStatusData.length==0){
			return "Not Available";
		}
		if(selfCheckName)
			return "Vulnerable";
		else
			return "Not Vulnerable";
	}
	$scope.getRemedationTableClassFromSelfCheckName = function(selfCheckName){
		if(undefined==selfCheckName || undefined==$scope.resultStatusData.length || $scope.resultStatusData.length==0){
			return "";
		}
		if(selfCheckName)
			return "status-vulnerable";
		else
			return "status-remediated";
	}
	$scope.isAvailable = function(selfCheckStatus){
		if(undefined==selfCheckStatus)
			return false
			return true;
	}


	$scope.$on('resultStatusReceived:updated', function(event,data) {
		$('.waitLoader').hide();
		$scope.flagIsVulnerable = false;
		if(undefined!=data && undefined!=data.selfcheck && data.selfcheck.length==0){
			$scope.asNotStarted = true;
		}
		else{
			selfcheckData = data.selfcheck;
			scList = [];
			for(var i in $rootScope.exerciseDetails.flags){
				for(var j in $rootScope.exerciseDetails.flags[i].flagList){
					var sc = $rootScope.exerciseDetails.flags[i].flagList[j].selfCheck.name;
					if(undefined!=sc)	{
						scList.push(sc);
						for(var n=0;n<selfcheckData.length;n++){
							if(sc==selfcheckData[n].name){
								$rootScope.exerciseDetails.flags[i].flagList[j].selfCheckStatus = selfcheckData[n].status;
								if(selfcheckData[n].status)
									$scope.flagIsVulnerable = true;
								break;
							}
						}
					}
				}
			}
			var selfcheckData = data.selfcheck;
			for(var n=0;n<selfcheckData.length;n++){
				if(scList.indexOf(selfcheckData[n].name)<0){
					selfcheckData.remove(n,n);
					n = n-1;
				}
				else{
					if(selfcheckData[n].status)
						$scope.flagIsVulnerable = true;
				}
			}
			$scope.resultStatusData = selfcheckData;
			$scope.asNotStarted = false;
		}

	});



	$scope.$on('exerciseRegions:updated', function(event,data) {

		if(data!=null){
			if(data.length>0)
				$scope.noRegionsAvailable = false;
			else
				$scope.noRegionsAvailable = true;
			$scope.availableRegions = [];
			$scope.regionsPingTimes = [];
			$scope.totalAvailableRegions = 0;
			for(var j in data){
				if(!data.hasOwnProperty(j))
					continue;
				$scope.totalAvailableRegions++;
				var p = new Ping();
				var fqdn = data[j].fqdn;
				p.ping("https://"+fqdn, data[j].name, function(err, ping,f,r) {
					console.log(f+" "+r+" "+ping)
					if(ping<3000){
						var obj = {};
						obj.name = r;
						obj.fqdn = f.replace("https://","");
						obj.ping = ping;
						$scope.regionsPingTimes.push(obj)
					}
					else{
						$scope.totalAvailableRegions--;
					}
				});
			}
		}
	});

	var technologyClassMap = {
			"NodeJS": "nodejsb",
			"Java": "javab",
			"Android": "androidb",
			"Ruby": "rubyb",
			"Python": "pythonb",
			"Go Lang":"golangb",
			"PHP":"phpb",
			".NET":"dotnetb",
			"Solidity (Ethereum)":"solidityb",
			"Agnostic":"agnosticb",
			"Kubernetes":"kubernetesb",
			"AWS":"awsb",
			"Docker":"dockerb",
			"Scala":"scalab",
			"Threat Hunting":"siemb",
			"Server Hardening":"serverhardeningb"
	};
	var pictForTechnology = {
			"NodeJS": "/assets/img/nodejs.png",
			"Java": "/assets/img/java.png",
			"Android": "/assets/img/android.png",
			"Ruby": "/assets/img/ruby.png",
			"Python": "/assets/img/python.png",
			"Go Lang":"/assets/img/golang.png",
			"PHP":"/assets/img/php.png",
			".NET":"/assets/img/dotnet.png",
			"Solidity (Ethereum)":"/assets/img/solidity.png",
			"Kubernetes":"/assets/img/k8s.png",
			"AWS":"/assets/img/aws.png",
			"Docker":"/assets/img/docker.png",
			"Scala":"/assets/img/scala.png",
			"Threat Hunting":"/assets/img/siem.png",
			"Server Hardening":"/assets/img/serverhardening.png"
	};

	var remediationClassMap = {
			false: "table-success",
			true: "table-danger"
	}



	$scope.getPictureForTechnology = function(technology){
		return pictForTechnology[technology]
	}
	$scope.getRemedationTableClass = function(status) {
		return remediationClassMap[status]
	};

	$rootScope.getExerciseClass = function(technology) {
		return technologyClassMap[technology];
	};

	$scope.getFormattedEnd = function(date){
		var out = moment(date).format("dddd, MMMM Do YYYY");
		return out;
	};

	$rootScope.getExerciseDetails = function(uuid){
		$rootScope.exerciseStarted = false;
		$rootScope.exerciseRestartCrashed = false;
		$rootScope.exerciseRestartCrashedId = undefined;
		$rootScope.ready = false;
		$rootScope.alreadyLaunched = false;
		server.getExerciseDetails(uuid);
		server.getRegionsForExercise(uuid);
	}
	$rootScope.startExerciseModal = function(exerciseId,uuid){
		if($rootScope.globalChallengeExercises.indexOf(uuid)==-1){
			$rootScope.exerciseIsInChallenge = false;
			$rootScope.selectedChallenge = "";
			$rootScope.startInstance(uuid);
		}
		else{
			$rootScope.exerciseIsInChallenge = true;
			$('.waitLoader').show();
			server.isExerciseIsInChallenge(uuid);
		}
	}
	var attemptAgainChallengeExerciseModal = function(id,uuid){
		$rootScope.challengeRetryExercise.uuid = uuid;
		$rootScope.challengeRetryExercise.id = id;
		$('#attemptAgainChallengeExerciseModal').modal('show');
	};
	$scope.retryChallengeExercise = function(){
		server.cancelChallengeExercise($rootScope.challengeRetryExercise.id);
		$('#attemptAgainChallengeExerciseModal').modal('hide');
	};

	$scope.$on('exerciseIsInChallenge:updated', function(event,data) {
		$('.waitLoader').hide();
		$rootScope.exerciseIsInChallenge = data.exerciseInChallenge;
		$rootScope.exerciseAlreadyRun = data.exerciseAlreadyRun;
		$rootScope.exerciseAlreadySolved = data.exerciseAlreadySolved;
		$rootScope.selectedChallenge = data.challengeId;
		if($rootScope.exerciseIsInChallenge && $rootScope.exerciseAlreadyRun && !$rootScope.exerciseAlreadySolved){
			attemptAgainChallengeExerciseModal(data.exerciseInstanceId,data.exerciseUuid)
		}
		else if(!$rootScope.exerciseIsInChallenge || ($rootScope.exerciseIsInChallenge && $rootScope.exerciseAlreadyRun && $rootScope.exerciseAlreadySolved)){
			$rootScope.exerciseIsInChallenge = false;
			$rootScope.selectedChallenge = "";
			$rootScope.startInstance(data.exerciseUuid);
		}
		else{
			$("#startExerciseModal").modal('show');
		}
	});
	
	
	
	$rootScope.startInstance = function(uuid){

		if($scope.regionsPingTimes.length==$scope.totalAvailableRegions){
			$('.waitLoader').show();
			server.startInstance(uuid, $scope.regionsPingTimes, $rootScope.selectedChallenge, $rootScope.exerciseRestartCrashedId);
		}
		else{
			$('.waitLoader').show();
			setTimeout(function () {
				$rootScope.startInstance(uuid);
			},500);
		}
	}

	var popupBlockerChecker = {
			check: function(popup_window){
				var _scope = this;
				if (popup_window) {
					if(/chrome/.test(navigator.userAgent.toLowerCase())){
						setTimeout(function () {
							_scope._is_popup_blocked(_scope, popup_window);
						},200);
					}else{
						popup_window.onload = function () {
							_scope._is_popup_blocked(_scope, popup_window);
						};
					}
				}else{
					_scope._displayError();
				}
			},
			_is_popup_blocked: function(scope, popup_window){
				if ((popup_window.innerHeight > 0)==false){ scope._displayError(); }
			},
			_displayError: function(){
				PNotify.removeAll();
				notificationService.error("A Popup Blocker is preventing opening a new tab for your SecureFlag environment. Please add this site to your exception list.");
			}
	};
	function getCountdownString(wait){
		var w;
		try { w = parseInt(wait) } catch(err){ return ""; }
		if(w<10){
			return "00:0"+w;
		}
		if(w<60){
			return "00:"+w;
		}
		if(w<600){
			return "0"+Math.floor(w/60)+":"+(w % 60);
		}

	}

	$scope.exerciseStopped = function(){
		server.getRunningExercises();
		setTimeout(function () {
			server.getUserHistory();
		},6500);
		$rootScope.exerciseStarted = false;
		$rootScope.ready = false;
		$rootScope.alreadyLaunched = false;
		$rootScope.visibility.welcome = false;
		$rootScope.visibility.home = false;
		$rootScope.visibility.assignedExercises = false;
		$rootScope.visibility.history = false;
		$rootScope.visibility.achievements = false;
		$rootScope.visibility.settings = false;
		$rootScope.visibility.exerciseDetails = false;
		$rootScope.visibility.leaderboard = false;
		$rootScope.visibility.runningExercises = true;
	}

	$scope.$on('exerciseStopped:updated', function(event,data) {
		$scope.exerciseStopped();
	});

	$scope.reservationPolled = false;
	$rootScope.synId = "";
	function handleInstanceStart(data){

		$rootScope.exerciseStarted = true;
		$rootScope.ready = false;
		$rootScope.alreadyLaunched = false;

		if(undefined!=data && data.fulfilled==true){

			$scope.countdown = getCountdownString(data.connection.countdown);
			var preload = data.connection.preload;
			$scope.guacUser = data.connection.user;
			$scope.guacFqdn = data.connection.fqdn;
			$scope.node = data.connection.node;
			$rootScope.exInstanceId = data.connection.exInstanceId;
			$rootScope.synId =  "exercise.html#/id="+$rootScope.exInstanceId;
			jQuery(function ($) {
				var cDown = data.connection.countdown,
				display = $('#cdown');
				function startTimer(duration, display, preload) {
					var timer = duration, minutes, seconds;
					$interval.cancel(intervalT);
					intervalT = $interval(function () {
						minutes = parseInt(timer / 60, 10)
						seconds = parseInt(timer % 60, 10);

						minutes = minutes < 10 ? "0" + minutes : minutes;
						seconds = seconds < 10 ? "0" + seconds : seconds;

						display.text(minutes + ":" + seconds);
						if(minutes==0 && seconds==0){
							$interval.cancel(intervalT);
							$rootScope.ready = true;
							$scope.reservationPolled = false;
							PNotify.removeAll();
							notificationService.notify({
								text: 'Your SecureFlag environment is ready.',
								type: 'success'
							});
							$scope.asNotStarted = false;
							server.getUserProfile();
							return;
						}
						if(preload && minutes==0 && seconds<=25){
							setupPreload();
						}
						if (--timer < 0) {
							timer = duration;
						}
					}, 1000);
				}
				startTimer(cDown, display, preload);
				server.getRunningExercises();
				server.getUserReservations();
			});
			$('.waitLoader').hide();
			$("#startExerciseModal").modal('hide');
		}
		else if(data.fulfilled==false && data.error==false){
			$scope.countdown = getCountdownString(data.waitSeconds);
			$rootScope.exerciseStarted = true;
			$rootScope.ready = false;
			$rootScope.alreadyLaunched = false;
			jQuery(function ($) {
				var cDown = data.waitSeconds,
				display = $('#cdown');
				idReservation = data.idReservation;
				function startTimer(duration, display,idReservation) {
					$interval.cancel(intervalT);
					var timer = duration, minutes, seconds;
					intervalT = $interval(function () {
						minutes = parseInt(timer / 60, 10)
						seconds = parseInt(timer % 60, 10);

						minutes = minutes < 10 ? "0" + minutes : minutes;
						seconds = seconds < 10 ? "0" + seconds : seconds;

						display.text(minutes + ":" + seconds);
						if(minutes==0 && seconds==0){
							$interval.cancel(intervalT);
							pollReservation(idReservation);	
							$scope.reservationPolled = true;
							return;
						}
						if (--timer < 0) {
							timer = duration;
						}
					}, 1000);
				}
				startTimer(cDown, display,idReservation);
			});
			$('.waitLoader').hide();
			$("#startExerciseModal").modal('hide');
		}
		else{
			PNotify.removeAll();
			notificationService.error('Something went wrong. The exercise could not be launched, please try again.');
		}
	}

	$scope.$on('userReservations:updated', function(event,data) {
		$rootScope.userReservations = data;
		for(var i=0;i<data.length;i++){
			if(data[i].error==false){
				pollReservation(data[i].idReservation);
			}
		}
	});

	function pollReservation(data){
		server.pollReservation(data)
	};

	$rootScope.timers = {};
	$rootScope.startTimer = function(duration, display,idReservation) {
		var timer = duration, minutes, seconds;
		if($rootScope.timers[idReservation]!=undefined)
			return;
		$rootScope.timers[idReservation] = $interval(function () {
			minutes = parseInt(timer / 60, 10)
			seconds = parseInt(timer % 60, 10);

			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;

			$('#'+display).text(minutes + ":" + seconds);
			if(minutes==0 && seconds==0){
				$interval.cancel($rootScope.timers[idReservation]);
				pollReservation(idReservation);	
				$scope.reservationPolled = true;
				return;
			}
			if (--timer < 0) {
				timer = duration;
			}
		}, 1000);
		return "00:00";
	}


	$scope.$on('instanceStarted:updated', function(event,data) {
		if(undefined!=data && data!=""){
			var present = false;
			for(var r in $rootScope.userReservations){
				if($rootScope.userReservations[r].idReservation == data.idReservation){
					present = true;
					break;
				}
			}
			if(!present)
				$rootScope.userReservations.push(data);
			handleInstanceStart(data);
		}
	});

	$scope.preloaded=false;
	function setupPreload(){
		
	}
	$scope.refreshResults = function(check){
		$('.waitLoader').show();
		server.getResultStatus($rootScope.exInstanceId,check);
	};
	$rootScope.launchEnvironment = function(){
		$('#preloadIframe').remove();
		$rootScope.alreadyLaunched = true;
		try{
			var popup = window.open("/user/exercise.html#id="+$rootScope.exInstanceId);  
			popupBlockerChecker.check(popup);
		}catch(err){
			var popup = window.open("/user/exercise.html#id="+$rootScope.exInstanceId);  
			popupBlockerChecker.check(popup);
		}	
	};
	$scope.getHint = function(){
		server.getHintForQuestion($scope.hintFlagQuestionId, $rootScope.exInstanceId);
		$('#getHintModal').modal('hide');
	};
	$scope.getHintModal = function(id){
		$scope.hintFlagQuestionId = id;
		$('#getHintModal').modal('show');
	};

	$scope.getResult = function(selfCheckName){
		return "Not Available";
	};

}])
sf.controller('history',['$scope','server','$rootScope','$filter','$location',function($scope,server,$rootScope,$filter,$location){
	$scope.user = server.user;
	$scope.showCodeDiff = false;
	$scope.showLogs = false;
	$scope.showResults = false;
	$scope.selectedResultRow = -1;
	$scope.feedbackMessage = "";
	$scope.emptyDiff = false;
	$scope.emptyLog = false;
	$scope.zipError = false;
	$rootScope.cancelledExercisesInHistory = false;



	$scope.userHistoryDetails = {}

	$scope.masterCompletedReviews = [];
	$scope.filteredCompletedList = []; 

	$scope.getUserHistory = function(){
		server.getUserHistory();
	}

	$scope.$on('exerciseSolution:updated', function(event,data) {
		document.querySelector('#solutionMD').innerHTML = '';
		var editor = new toastui.Editor.factory({
			el: document.querySelector('#solutionMD'),
			viewer: true,
			usageStatistics:false,
			height: '500px',
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			initialValue: data.text
		});
		autoplayMdVideo('#solutionMD')
		$('#solutionMD').find('a').each(function() {
			if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
				return;
			var a = new RegExp('/' + window.location.host + '/');
			if(!a.test(this.href)) {
				$(this).click(function(event) {
					event.preventDefault();
					event.stopPropagation();
					window.open(this.href, '_blank');
				});
			}
		});
		$('#solutionModal').modal('show');
	});
	$scope.triggerFlagsMarkdown = function(fq){
		window.setTimeout(function(){
			if(undefined != fq){
				document.querySelector('#f'+fq.id+'-md').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#f'+fq.id+'-md'),
					viewer: true,
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					usageStatistics:false,
					height: '500px',
					initialValue: fq.md.text
				});
				autoplayMdVideo('#f'+fq.id+'-md')
				$('#f'+fq.id+'-md').find('a').each(function() {
					if(this.href==undefined || this.href=="")
						return;
					if(this.href.indexOf("#exerciseKB")>=0){
						$(this).replaceWith($(this).text());
					}
				});
			}
		},100);
	}

	$scope.triggerHintsMarkdown = function(h){
		window.setTimeout(function(){
			if(undefined != h){
				document.querySelector('#h'+h.id+'-md').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#h'+h.id+'-md'),
					viewer: true,
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					usageStatistics:false,
					height: '500px',
					initialValue: h.md.text
				});
				autoplayMdVideo('#h'+h.id+'-md')
				$('#h'+h.id+'-md').find('a').each(function() {
					if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
						return;
					var a = new RegExp('/' + window.location.host + '/');
					if(!a.test(this.href)) {
						$(this).click(function(event) {
							event.preventDefault();
							event.stopPropagation();
							window.open(this.href, '_blank');
						});
					}
				});
			}
		},100);
	}


	$scope.updateFilteredList = function() {
		$scope.filteredCompletedList = $filter("filter")($scope.masterCompletedReviews, $scope.query);
	};

	$scope.tableconfig = {
			itemsPerPage: 8,
			fillLastPage: false
	}

	$scope.getScoringModeString = function(code){
		switch(parseInt(code)){
		case 0:
			return "Automated";
		case 1:
			return "Automated";
		case 2:
			return "Manual";
		default: 
			return "Manual";
		}
	}

	$scope.getExerciseStatusString = function(status){
		switch(status){
		case "STOPPING":
			return "Stopping";
			break;
		case "STOPPED":
			return "Pending Review";
			break;
		case "CANCELLED":
			return "Cancelled";
			break;
		case "REVIEWED":
			return "Completed";
			break;
		case "AUTOREVIEWED":
			return "Completed";
			break;
		case "REVIEWED_MODIFIED":
			return "Completed";
			break;
		default:
			return "Pending Review";
		break;
		}
	}

	$scope.$on('userHistory:updated', function(event,data) {
		for(var i=0;i<data.length;i++){
			if(data[i].status != "CANCELLED" && data[i].status != "CRASHED"){
				var tmpId = data[i].exercise.uuid;
				if(!$rootScope.takenExercises.includes(tmpId))
					$rootScope.takenExercises.push(tmpId);
				if(data[i].solved && !$rootScope.solvedExercises.includes(tmpId)){
					$rootScope.solvedExercises.push(tmpId);
				}
				if(undefined==$rootScope.timesPerExercise[tmpId]){
					$rootScope.timesPerExercise[tmpId] = 1;
				}
				else{
					$rootScope.timesPerExercise[tmpId] = $rootScope.timesPerExercise[tmpId] + 1;
				}
			}
		}
		$scope.masterCompletedReviews = data;
		$scope.filteredCompletedList = $filter("filter")($scope.masterCompletedReviews, "!CANCELLED");
		$rootScope.userHistory = data;

		$rootScope.$broadcast('userHistoryIsReady:updated',$scope.filteredCompletedList);

		$scope.lastUpdated = new Date();
		$rootScope.scorePerExercises = {}
		$rootScope.scorePerExercises.options = {}
		$rootScope.scorePerExercises.options.layout = {
				padding: {
					left: 0,
					right: 0,
					top: 0,
					bottom: 0
				}
		}
		$rootScope.scorePerExercises.options.legend = {
				display: true,
				position: 'bottom',
				labels: {
					fontColor: "#000",
					fontSize: 10
				}
		};

		$rootScope.scorePerExercises.datasetOverride = [ {showLine: true, pointStyle:'rectRounded',fill: false,
			pointRadius: 10,pointHoverRadius: 15},{ showLine: true,pointStyle:'rectRounded',fill: false,
				pointRadius: 10,pointHoverRadius: 15}] ;
		$rootScope.scorePerExercises.options.events= ["mousemove", "mouseout", "click", "touchstart", "touchmove", "touchend"];
		$rootScope.scorePerExercises.options.title = { display:true}

		$rootScope.scorePerExercises.options.tooltips = {
				callbacks: {
					// Use the footer callback to display the sum of the items showing in the tooltip
					afterTitle: function(tooltipItems, data) {
						if(undefined==$rootScope.scorePerExercises.exercises[tooltipItems[0].index])
							return "";
						return $rootScope.scorePerExercises.exercises[tooltipItems[0].index];
					}
				},
				footerFontStyle: 'normal'
		};
		$rootScope.scorePerExercises.options.pieceLabel = {
				render: 'value',
				precision: 0,
				showZero: true,
				fontSize: 14,
				fontColor: '#FFF',
				fontStyle: 'normal',
				fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
				arc: false,
				position: 'border',
				overlap: true
		}

		$rootScope.scorePerExercises.data = [[],[]];
		$rootScope.scorePerExercises.series = ["Score","Duration (minutes)"];
		$rootScope.scorePerExercises.labels = [];
		$rootScope.scorePerExercises.exercises = [];

		var tmpData = cloneObj(data);
		var l = tmpData.length;
		if(l>20){
			tmpData = tmpData.slice(l - 20);
		}

		for (var property in tmpData) {
			if(tmpData.hasOwnProperty(property) && tmpData[property].status == "CANCELLED")
				$rootScope.cancelledExercisesInHistory = true;
			if (tmpData.hasOwnProperty(property) && (tmpData[property].status == "REVIEWED" || tmpData[property].status == "AUTOREVIEWED" || tmpData[property].status == "REVIEWED_MODIFIED")) {
				$rootScope.scorePerExercises.labels.push(moment(tmpData[property].endTime).format('MMM DD'));	
				$rootScope.scorePerExercises.data[0].push(tmpData[property].score.result);
				if(undefined==tmpData[property].duration)
					tmpData[property].duration=0;
				$rootScope.scorePerExercises.data[1].push(tmpData[property].duration);
				$rootScope.scorePerExercises.exercises.push(tmpData[property].title);

			}
		}

		$rootScope.$broadcast('statsAreReady:updated',$rootScope.scorePerExercises);
	});

	var remediationClassMap = {
			"Not Vulnerable": "table-success",
			"Vulnerable": "table-danger",
			"Broken Functionality":"table-warning",
			"Not Available":"table-secondary",
			"Not Addressed":"table-info",
			"Partially Remediated":"table-warning"
	};
	$scope.getRemedationTableClass = function(status) {
		return remediationClassMap[status]
	};
	$scope.getStatusString = function(status){
		switch(status){
		case "1":
			return "Vulnerable"
		case "0":
			return "Not Vulnerable"
		case "2":
			return "Broken Functionality"
		case "4":
			return "Not Addressed"
		case "3":
			return "Not Available"
		case "5":
			return "Partially Remediated"
		default: 
			return "Not Available";
		}
	}
	var statusClassMap = {
			"0": "table-success",
			"1": "table-danger",
			"2": "table-warning",
			"4": "table-info",
			"NOT_AVAILABLE":"table-secondary"
	};
	$scope.getStatusClass = function(status) {
		s = statusClassMap[status];
		if(s == 'table-secondary' && ($scope.userHistoryDetails.status == 'AUTOREVIEWED' ||$scope.userHistoryDetails.status == 'REVIEWED_MODIFIED'))
			s = 'table-info';
		return s;
	};

	var scoreClassMap = {	
			success: "success",
			average: "warning",
			failure: "danger",
			pending: "info",
			active: "active"

	};

	$rootScope.getDurationInterval = function(start,end,dur){
		if(undefined != dur && null != dur && 0 != dur){
			return moment.utc(moment.duration(dur,"m").asMilliseconds()).format("H mm").replace(" ","h")+"'";
		}
		else{
			return moment.utc(moment(end).diff(moment(start))).format("H mm").replace(" ","h")+"'";
		}
	};
	$scope.getDatesInterval = function(start,end){
		var out = moment(start).format("MMM D YYYY, HH:mm");
		out += " - "+moment(end).format("HH:mm");
		return out;
	}

	$scope.getDateFormat = function(start){
		return moment(start).local().format("MMM D YYYY, HH:mm");
	}
	$rootScope.getDateInCurrentTimezone = function(date,format){
		if(date==null)
			return "N/A"
			return moment(date).local().format(format);
	}

	$rootScope.getResultsScore = function(score) {
		if(undefined == score || undefined==score.result)
			return "";
		if(score.result > -1)
			return score.result;
		else
			return "?";
	};


	$rootScope.getResultsScoreClass = function(result,total,status) {
		if(status=="CANCELLED")
			return scoreClassMap["active"];
		if(result==-1)
			return scoreClassMap["pending"];
		if(result>(total-(total/10)))
			return scoreClassMap["success"];
		else if(result<(total/3))
			return scoreClassMap["failure"];
		else  
			return scoreClassMap["average"];
	};


	$scope.toggleCodeDiff = function(){
		if($scope.showCodeDiff == true)
			$scope.showCodeDiff = false;
		else{
			$scope.showLogs = false;
			$scope.showCodeDiff = true;
		}
	}
	$scope.toggleInstanceLogs = function(){
		if($scope.showLogs == true)
			$scope.showLogs = false;
		else{
			$scope.showCodeDiff = false;
			$scope.showLogs = true;
		}
	}
	$scope.downloadSolutions = function(id){
		server.downloadExerciseSolutions(id);
	}

	$scope.leaveFeedbackModal = function(){
		$('#leaveFeedbackModal').modal('show');
	}

	$scope.doLeaveFeedback = function(){
		server.sendFeedback($scope.userHistoryDetails.id, $scope.feedbackMessage);
		$('#leaveFeedbackModal').modal('hide');
	}
	$scope.getScoreString = function(score){
		if(parseInt(score)>=0)
			return score;
		else{
			if($scope.userHistoryDetails.status == "AUTOREVIEWED" || $scope.userHistoryDetails.status == "REVIEWED_MODIFIED")
				return 0;
			return " ? ";
		}
	}

	$scope.getQuestionName = function(resName){
		if($scope.userHistoryDetails.exercise!=undefined){
			for(var i in $scope.userHistoryDetails.exercise.flags){
				if($scope.userHistoryDetails.exercise.flags.hasOwnProperty(i)){
					for(var j in $scope.userHistoryDetails.exercise.flags[i].flagList){
						if($scope.userHistoryDetails.exercise.flags[i].flagList.hasOwnProperty(j) && $scope.userHistoryDetails.exercise.flags[i].flagList[j].selfCheckAvailable && $scope.userHistoryDetails.exercise.flags[i].flagList[j].selfCheck.name==resName){
							return $scope.userHistoryDetails.exercise.flags[i].title;
						}
					}
				}
			}
		}
		return resName;
	}
	$scope.getQuestionMaxScore = function(resName){
		if($scope.userHistoryDetails.exercise!=undefined){

			for(var i in $scope.userHistoryDetails.exercise.flags){
				if($scope.userHistoryDetails.exercise.flags.hasOwnProperty(i)){
					for(var j in $scope.userHistoryDetails.exercise.flags[i].flagList){
						if($scope.userHistoryDetails.exercise.flags[i].flagList.hasOwnProperty(j) && $scope.userHistoryDetails.exercise.flags[i].flagList[j].selfCheckAvailable && $scope.userHistoryDetails.exercise.flags[i].flagList[j].selfCheck.name==resName){
							return $scope.userHistoryDetails.exercise.flags[i].flagList[j].maxScore;
						}
					}
				}
			}
		}
	}

	$scope.complaintExerciseId = -1;
	$scope.complaintExerciseFlag = "";

	$scope.openComplaintModal = function(e,f){
		$scope.complaintExerciseId = e;
		$scope.complaintExerciseFlag = f;
		$('#scoringComplaintModal').modal('show');
	}

	$scope.sendScoringComplaint = function(){
		var obj = {};
		obj.name = $scope.complaintExerciseFlag;
		obj.text = $scope.complaintModalTextInput;
		obj.id = $scope.complaintExerciseId;
		server.addScoringComplaint(obj);
		$('#scoringComplaintModal').modal('hide');
	}

	$scope.$on('scoringComplaintAdded:updated', function(event,data) {	
		server.getUserHistoryDetails($scope.userHistoryDetails.id);
		$scope.complaintModalTextInput = "";
	});
	$scope.$on('userHistoryDetails:updated', function(event,data) {	

		if(data.results.length == 0){
			data.results = [];
			for(var j in data.exercise.flags){
				if(data.exercise.flags.hasOwnProperty(j)){
					for(var i in data.exercise.flags[j].flagList){
						if(data.exercise.flags[j].flagList.hasOwnProperty(i) && data.exercise.flags[j].flagList[i].selfCheckAvailable){
							var tmpObj = {};
							tmpObj.name = data.exercise.flags[j].flagList[i].selfCheck.name;
							tmpObj.status = "NOT_AVAILABLE";
							tmpObj.verified = false; 
							data.results.push(tmpObj)
						}
					}
				}

			}
		}
		$scope.showCodeDiff = false;
		$scope.showLogs = false;
		$scope.userHistoryDetails = data;
		$scope.showResults = true;
		$scope.emptyDiff = false;
		$scope.emptyLog = false;
		$scope.zipError = false;

		var offset = $('#infoArea').offset();
		$('html, body').animate({
			scrollTop: offset.top + 200,
			scrollLeft: offset.left
		});
		var tba = "";

		JSZipUtils.getBinaryContent($scope.userHistoryDetails.id,$rootScope.ctoken, '/user/handler','getUserHistoryDetailsFile', function(err, data) {
			if(err) {
				$scope.zipError = true;
				return; // or handle err
			}

			JSZip.loadAsync(data).then(function(zip) {
				for(file in zip.files){
					if(file.indexOf('sourceDiff.txt')>-1){
						zip.file(file).async("string").then(function success(content) {
							var diffString = content;
							if(diffString==""){
								$scope.emptyDiff = true;
								$('#targetDiv').empty()
								return;
							}
							try{
								var diff2htmlUi = new Diff2HtmlUI({diff : diffString });
								diff2htmlUi.draw( '#targetDiv', {
									inputFormat : 'diff',
									showFiles : true,
									matching : 'lines'
								});
								diff2htmlUi.highlightCode('#targetDiv');
							} catch(err){
							}
						})                        
					}
				}
			});
		});    
	})
	$scope.showExerciseResult = function(eId, index){
		$location.path("history/details/"+eId, false);
		$scope.selectedResultRow = index;
	}
	$rootScope.showResultsFor = function(eId, index){
		server.getUserHistoryDetails(eId);
		$scope.selectedResultRow = index;
	}

	function htmlEncode(value){
		return $('<div/>').text(value).html();
	}    
}])
sf.controller('achievements',['$scope','server','$rootScope',function($scope,server,$rootScope){
	$scope.user = server.user;
	$scope.trophies = [];
	$scope.emptyTrophy = true;

	$scope.remediatedPerIssue = {}
	$scope.remediatedPerIssue.data = [];
	$scope.remediatedPerIssue.labels = [];
	$scope.remediatedPerCategory = {}
	$scope.remediatedPerCategory.data = [];
	$scope.remediatedPerCategory.labels = [];
	$scope.remediationRatePerIssue = [];
	$scope.remediationRatePerCategory = [];
	$scope.scorePerExercises = {};
	$scope.options = {}

	$scope.options.animation = false;
	$scope.options.pieceLabel = {
			// render 'label', 'value', 'percentage' or custom function, default is 'percentage'
			render: 'percentage',
			// precision for percentage, default is 0
			precision: 0,
			// identifies whether or not labels of value 0 are displayed, default is false
			showZero: false,
			// font size, default is defaultFontSize
			fontSize: 12,
			// font color, can be color array for each data, default is defaultFontColor
			fontColor: '#FFF',
			// font style, default is defaultFontStyle
			fontStyle: 'normal',
			// font family, default is defaultFontFamily
			fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			// draw label in arc, default is false
			arc: false,
			// position to draw label, available value is 'default', 'border' and 'outside'
			// default is 'default'
			position: 'border',
			// draw label even it's overlap, default is false
			overlap: true
	}

	$scope.options.layout = {
			padding: {
				left: 0,
				right: 0,
				top: 0,
				bottom: 0
			}
	}
	$scope.options.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};
	$scope.options.title = {
			text : "",
			display:true,
			position: 'bottom'

	}
	$scope.radarOptions = {}
	$scope.radarOptions.animation = false;
	$scope.radarOptions.title = {
			text : "",
			display:true,
			position: 'top'

	}
	$scope.radarOptions.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};

	$rootScope.$watch('scorePerExercises', function(newValue, oldValue) {
		$scope.scorePerExercises = $rootScope.scorePerExercises;
	});

	$scope.getUserStats = function(){
		server.getUserStats();
	}
	$scope.$on('statsUser:updated', function(event,data) {

		$scope.lastUpdated = new Date();

		$scope.remediationRatePerCategory = [];
		for (var property in data.categoriesRemediationRate) {
			if (data.categoriesRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.categoriesRemediationRate[property]).length>0) {
				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.text = property;
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.categoriesRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
				}
				$scope.remediationRatePerCategory.push(obj);
			}
		}
		$scope.remediatedPerCategory.data = [[]];
		$scope.remediatedPerCategory.labels = [];
		$scope.remediatedPerCategory.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerCategory.options.title.display = false;
		$scope.remediatedPerCategory.series = ["Total Time Spent (minutes)"];
		$scope.remediatedPerCategory.options.scales = {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
		}
		$scope.remediatedPerCategoryPercentage = cloneObj($scope.remediatedPerCategory);
		$scope.remediatedPerCategoryPercentage.series = ["Remediated (%)"];
		$scope.remediatedPerCategoryPercentage.options.scales = {
				display: true,
				ticks: {
					beginAtZero: true,
					min: 0,
					max: 100,
					stepSize: 10
				}
		}
		for(var j in $scope.remediationRatePerCategory){
			if ($scope.remediationRatePerCategory.hasOwnProperty(j)){
				try{
					var tmpName = $scope.remediationRatePerCategory[j].options.title.text;
					var tmpData = Math.ceil(data.totalMinutesPerIssueCategory[tmpName]);

					var tmpRem = $scope.remediationRatePerCategory[j].data[0]
					var tmpTot = $scope.remediationRatePerCategory[j].data.reduce(getSum);
					var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);

					if(!isNaN(tmpData) && tmpName!= "null"){
						$scope.remediatedPerCategory.labels.push(tmpName);
						$scope.remediatedPerCategory.data[0].push(tmpData);
						$scope.remediatedPerCategoryPercentage.labels.push(tmpName);
						$scope.remediatedPerCategoryPercentage.data[0].push(tmpPercentage);
					}
				}catch(err){}
			}
		}
		$rootScope.$broadcast('userStatsAreReady:updated',$scope.remediatedPerCategoryPercentage);


	});

	$scope.$on('achievements:updated', function(event,data) {
		$scope.userAchievements = data;
		if(undefined!=data.achievements){
			$scope.trophies = [];
			$scope.placements = [];
			$scope.certifications = [];

			$scope.nrCertifications = 0;
			$scope.nrPlacements = 0;
			$scope.nrTrophies = 0;

			$scope.emptyTrophy = true;
			$scope.emptyCertification = true;
			$scope.emptyPlacement = true;

			var tmpTrophy = [];
			var tmpPlacement = [];
			var tmpCertification = [];

			for (var i in data.achievements){
				if(data.achievements.hasOwnProperty(i) && undefined!=data.achievements[i].achievement && undefined!=data.achievements[i].achievement.type){
					if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="TROPHY"){
						tmpTrophy.push(data.achievements[i]);
						$scope.emptyTrophy = false;
					}
					else if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="CHALLENGE"){
						tmpPlacement.push(data.achievements[i]);
						$scope.emptyPlacement = false;	
					}
					else if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="CERTIFICATION"){
						tmpCertification.push(data.achievements[i]);
						$scope.emptyCertification = false;
					}
				}

			}
			$scope.nrCertifications = tmpCertification.length;
			$scope.nrPlacements = tmpPlacement.length;
			$scope.nrTrophies = tmpTrophy.length;


			if(undefined!=tmpTrophy && tmpTrophy.length>0){
				var size = 4;
				while (tmpTrophy.length > 0){
					$scope.trophies.push(tmpTrophy.splice(0, size));
				}
			}
			if(undefined!=tmpPlacement && tmpPlacement.length>0){
				var size = 5;
				while (tmpPlacement.length > 0){
					$scope.placements.push(tmpPlacement.splice(0, size));
				}
			}
			if(undefined!=tmpCertification && tmpCertification.length>0){
				var size = 4;
				while (tmpCertification.length > 0){
					$scope.certifications.push(tmpCertification.splice(0, size));
				}
			}
		}
	});


}]);
sf.controller('settings',['$scope','server','$timeout',function($scope,server,$timeout){
	$scope.user = server.user;
	$scope.countries = server.countries;
	$scope.oldPassword = "";
	$scope.newPasswordRepeat = "";
	$scope.newPassword = "";
	$scope.updateUserProfile = function(){
		server.updateUserProfile($scope.user);
	}
	$scope.removeUser = function(){
		server.removeUser();
		$('#userRemoveModal').modal('hide');
	}

	$scope.openRemoveModal = function(){
		$('#userRemoveModal').modal('show');

	}

	$scope.$on('removeUser:updated', function(event,data) {
		document.location = "/index.html";
	})

	$scope.updateUserPassword = function(){
		server.updateUserPassword($scope.userPasswordForm.oldPassword.$modelValue, $scope.userPasswordForm.newPassword.$modelValue);
		$scope.oldPassword = "";
		$scope.newPasswordRepeat = "";
		$scope.newPassword = "";
		$scope.userPasswordForm.$pristine = true;
	}
}]);
sf.controller('challenges',['$scope','server','$rootScope','$location','$filter','$interval',function($scope,server,$rootScope,$location,$filter,$interval){

	$scope.user = server.user;
	$rootScope.selectedChallenge = -1;
	$scope.filteredChallengesList = [];
	$scope.masterChallengesList = [];
	$rootScope.showChallengesList = true;
	$rootScope.showChallengeDetails = false;
	$scope.showCompleted = true;
	$scope.noActiveChallenges = true;

	$scope.challengeResults = {};
	$rootScope.challengeDetails = [];


	$scope.$watch("showCompleted", function() {
		$scope.updateFilteredList();
	});	

	$scope.getChallengeDetailsButton = function(id){
		$('.waitLoader').show();
		$rootScope.selectedChallenge = id;
		$location.path("tournaments/details/"+$rootScope.selectedChallenge, false);
	}
	$scope.getChallengeDetails = function(id){
		$rootScope.selectedChallenge = id;
		server.getChallengeDetails(id);
	}
	
	$scope.lastUpdated = "";
	$scope.updateActiveTournaments = function(){
		server.getChallenges();
	}
	
	$scope.getUsernameFromId = function(id){

		if(id==null)
			return "";
		for(var i=0; i<$rootScope.challengeDetails.users.length;i++){
			if($rootScope.challengeDetails.users[i].idUser==id)
				return $rootScope.challengeDetails.users[i].user;
		}
		return ""

	}

	$rootScope.getTournamentStatusClass = function(status){
		switch(status){
		case "IN_PROGRESS":
			return "b-success";
			break;
		case "NOT_STARTED":
			return "b-primary";
			break;
		case "FINISHED":
			return "b-danger";
			break;
		}
	}
	$rootScope.getChallengeStatusString = function(status){
		switch(status){
		case "IN_PROGRESS":
			return "In Progress";
			break;
		case "NOT_STARTED":
			return "Not Started";
			break;
		case "FINISHED":
			return "Completed";
			break;
		}
	}

	$scope.didTakeChallengeExercise = function(uuid){
		for(var e in $rootScope.challengeDetails.runExercises){
			if($rootScope.challengeDetails.runExercises.hasOwnProperty(e)){
				if($rootScope.challengeDetails.runExercises[e].user.user == $scope.user.user && $rootScope.challengeDetails.runExercises[e].exercise.uuid==uuid){
					if($rootScope.challengeDetails.runExercises[e].status != "CRASHED" && $rootScope.challengeDetails.runExercises[e].status != "CANCELLED"){
						return true;
					}
				}
			} 
		}
		return false;
	}

	$rootScope.challengeDetails = [];
	$scope.$on('challengeResults:updated', function(event,data) {

		$('.waitLoader').hide();

		data.exerciseData = $rootScope.challengeDetails.exerciseData.slice(0);

		data.flags = [];
		data.theads = [];

		$scope.exercises = [];
		var tmpExercises = [];
		for(var i in data.exerciseData){
			if(data.exerciseData.hasOwnProperty(i) && data.exerciseData[i].id)
				tmpExercises.push(data.exerciseData[i])
		}
		var size = 3;
		while (tmpExercises.length > 0){
			$scope.exercises.push(tmpExercises.splice(0, size));
		}

		for (var property in data.exerciseData) {
			if (data.exerciseData.hasOwnProperty(property)) {
				for(var f in data.exerciseData[property].flags){
					if (data.exerciseData[property].flags.hasOwnProperty(f) && !data.exerciseData[property].flags[f].flagList[0].optional ) {
						var tmpObj = {};
						tmpObj.id = data.exerciseData[property].flags[f].id
						tmpObj.name = data.exerciseData[property].flags[f].title
						data.theads.push(tmpObj)
						data.flags.push(data.exerciseData[property].flags[f]);
					}
				}
			}
		}

		var remediated = 0; 
		$rootScope.challengeDetails.runFlags = 0;
		data.userRunFlags = 0;
		var userRemediated = 0;
		data.userRunExercises = 0;
		data.challengeRunExercises=0;
		data.challengeRunningExercises=0;
		for(var i=0;i<data.runExercises.length;i++){
			if(data.runExercises[i].results.length>0){
				data.challengeRunExercises++;
			}
			if(data.runExercises[i].status=="RUNNING"){
				data.challengeRunningExercises++;
			}
			if(data.runExercises[i].user.user==$scope.user.user && data.runExercises[i].results.length>0){
				data.userRunExercises++
			}
			for(var j=0;j<data.runExercises[i].results.length;j++){
				data.runFlags++;
				if(data.runExercises[i].user.user==$scope.user.user){
					data.userRunFlags++
				}
				if(data.runExercises[i].results[j].status == "0"){
					remediated++;
					if(data.runExercises[i].user.user==$scope.user.user){
						userRemediated++;
					}
				}
			}
		}
		if(remediated==0 || data.runFlags == 0)
			data.remediation = 0;
		else
			data.remediation = (remediated/data.runFlags) * 100;

		if(userRemediated==0 || data.userRunFlags == 0)
			data.userRemediation = 0;
		else
			data.userRemediation = (userRemediated/data.userRunFlags) * 100;

		if(data.scopeExercises.length!=0 && data.userRunExercises !=0)
			data.userCompletion = (data.userRunExercises /  data.scopeExercises.length) * 100;
		else
			data.userCompletion = 0;


		data.lastRefreshed = new Date();
		$rootScope.challengeDetails = data;

		$rootScope.challengeDetails.teams = [];

		for(var u in $rootScope.challengeDetails.users){
			if ($rootScope.challengeDetails.users.hasOwnProperty(u)){

				$scope.challengeResults[$rootScope.challengeDetails.users[u].user] = {}
				$rootScope.challengeDetails.users[u].challengeRunFlags = 0;
				$rootScope.challengeDetails.users[u].challengeRunExercises = 0;
				$rootScope.challengeDetails.users[u].challengeScore = 0;
				$rootScope.challengeDetails.users[u].challengeRemediatedFlags = 0;
				for(var e in $rootScope.challengeDetails.runExercises){

					if($rootScope.challengeDetails.runExercises.hasOwnProperty(e) && $rootScope.challengeDetails.runExercises[e].user.user==$rootScope.challengeDetails.users[u].user){
						$rootScope.challengeDetails.users[u].challengeRunExercises++;
						for(var r in $rootScope.challengeDetails.runExercises[e].results){
							if($rootScope.challengeDetails.runExercises[e].results.hasOwnProperty(r)){
								$scope.challengeResults[$rootScope.challengeDetails.users[u].user][$rootScope.challengeDetails.runExercises[e].results[r].name] = $rootScope.challengeDetails.runExercises[e].results[r];
								if($rootScope.challengeDetails.runExercises[e].results[r].status=="0" && Number.isInteger($rootScope.challengeDetails.runExercises[e].results[r].score)){
									$rootScope.challengeDetails.users[u].challengeScore += $rootScope.challengeDetails.runExercises[e].results[r].score;
									$rootScope.challengeDetails.users[u].challengeRemediatedFlags++;
								}

								$rootScope.challengeDetails.users[u].challengeRunFlags++;
							} 
						}
					}
				}
			}
		}
		$rootScope.showChallengeDetails = true;
		$rootScope.showChallengesList = false;
		$scope.challengeDetailsTableMasterList = $rootScope.challengeDetails.users;
		if($scope.tableQuery=="")
			$scope.challengeDetailsTableFilteredList =  $rootScope.challengeDetails.users;
		else
			$scope.challengeDetailsTableFilteredList =  $filter("filter")($scope.challengeDetailsTableMasterList, $scope.tableQuery);

	});


	$rootScope.didSolveChallengeExercise = function(eid){
		for(var i=0; i<$rootScope.challengeDetails.runExercises.length;i++){
			if($rootScope.challengeDetails.runExercises[i].exercise.uuid == eid && $rootScope.challengeDetails.runExercises[i].user.user==$scope.user.user)
				return $rootScope.challengeDetails.runExercises[i].solved;
		}
		return false;
	}


	$scope.$on('challengeDetails:updated', function(event,data) {

		$('.waitLoader').hide();

		data.flags = [];
		data.theads = [];

		$scope.exercises = [];
		var tmpExercises = [];
		for(var i in data.exerciseData){
			if(data.exerciseData.hasOwnProperty(i) && data.exerciseData[i].id)
				tmpExercises.push(data.exerciseData[i])
		}
		var size = 3;
		while (tmpExercises.length > 0){
			$scope.exercises.push(tmpExercises.splice(0, size));
		}

		for (var property in data.exerciseData) {
			if (data.exerciseData.hasOwnProperty(property)) {
				for(var f in data.exerciseData[property].flags){
					if (data.exerciseData[property].flags.hasOwnProperty(f) && !data.exerciseData[property].flags[f].flagList[0].optional ) {
						var tmpObj = {};
						tmpObj.id = data.exerciseData[property].flags[f].id
						tmpObj.name = data.exerciseData[property].flags[f].title
						data.theads.push(tmpObj)
						data.flags.push(data.exerciseData[property].flags[f]);
					}
				}
			}
		}

		var remediated = 0; 
		$rootScope.challengeDetails.runFlags = 0;
		data.userRunFlags = 0;
		data.runFlags = 0;
		var userRemediated = 0;
		data.userRunExercises = 0;
		data.challengeRunExercises=0;
		data.challengeRunningExercises=0;
		for(var i=0;i<data.runExercises.length;i++){
			try{
				if(data.runExercises[i].results.length>0){
					data.challengeRunExercises++;
				}
				if(data.runExercises[i].status=="RUNNING"){
					data.challengeRunningExercises++;
				}
				if(data.runExercises[i].user.user==$scope.user.user && data.runExercises[i].results.length>0){
					data.userRunExercises++
				}
				for(var j=0;j<data.runExercises[i].results.length;j++){
					try{
						data.runFlags++;
						if(data.runExercises[i].user.user==$scope.user.user){
							data.userRunFlags++
						}
						if(data.runExercises[i].results[j].status == "0"){
							remediated++;
							if(data.runExercises[i].user.user==$scope.user.user){
								userRemediated++;
							}
						}
					}catch(e){}
				}
			}catch(e){}
		}
		if(remediated==0 || data.runFlags == 0)
			data.remediation = 0;
		else
			data.remediation = (remediated/data.runFlags) * 100;

		if(userRemediated==0 || data.userRunFlags == 0)
			data.userRemediation = 0;
		else
			data.userRemediation = (userRemediated/data.userRunFlags) * 100;

		if(data.scopeExercises.length!=0 && data.userRunExercises !=0)
			data.userCompletion = (data.userRunExercises /  data.scopeExercises.length) * 100;
		else
			data.userCompletion = 0;


		data.lastRefreshed = new Date();

		$rootScope.challengeDetails = data;

		$rootScope.challengeDetails.teams = [];

		for(var u in $rootScope.challengeDetails.users){
			if ($rootScope.challengeDetails.users.hasOwnProperty(u)){

				$scope.challengeResults[$rootScope.challengeDetails.users[u].user] = {}
				$rootScope.challengeDetails.users[u].challengeRunFlags = 0;
				$rootScope.challengeDetails.users[u].challengeRunExercises = 0;
				$rootScope.challengeDetails.users[u].challengeScore = 0;
				$rootScope.challengeDetails.users[u].challengeRemediatedFlags = 0;
				for(var e in $rootScope.challengeDetails.runExercises){

					if($rootScope.challengeDetails.runExercises.hasOwnProperty(e) && $rootScope.challengeDetails.runExercises[e].user.user==$rootScope.challengeDetails.users[u].user){
						$rootScope.challengeDetails.users[u].challengeRunExercises++;
						for(var r in $rootScope.challengeDetails.runExercises[e].results){
							if($rootScope.challengeDetails.runExercises[e].results.hasOwnProperty(r)){
								$scope.challengeResults[$rootScope.challengeDetails.users[u].user][$rootScope.challengeDetails.runExercises[e].results[r].name] = $rootScope.challengeDetails.runExercises[e].results[r];
								if($rootScope.challengeDetails.runExercises[e].results[r].status=="0" && Number.isInteger($rootScope.challengeDetails.runExercises[e].results[r].score)){
									$rootScope.challengeDetails.users[u].challengeScore += $rootScope.challengeDetails.runExercises[e].results[r].score;
									$rootScope.challengeDetails.users[u].challengeRemediatedFlags++;
								}

								$rootScope.challengeDetails.users[u].challengeRunFlags++;
							} 
						}
					}
				}
			}
		}
		$rootScope.showChallengeDetails = true;
		$rootScope.showChallengesList = false;
		$scope.challengeDetailsTableMasterList = $rootScope.challengeDetails.users;
		if($scope.tableQuery=="")
			$scope.challengeDetailsTableFilteredList =  $rootScope.challengeDetails.users;
		else
			$scope.challengeDetailsTableFilteredList =  $filter("filter")($scope.challengeDetailsTableMasterList, $scope.tableQuery);

	});

	$rootScope.challengeUserTableConfig = {
			itemsPerPage: 10,
			fillLastPage: false
	}

	$scope.challengeDetailsTableMasterList = [];
	$scope.challengeDetailsTableFilteredList = [];
	$scope.updateTableFilteredList = function() {
		$scope.challengeDetailsTableFilteredList = $filter("filter")($scope.challengeDetailsTableMasterList, $scope.tableQuery);
	};



	$scope.triggerChallengeUpdate = function(id){
		if($rootScope.showChallengeDetails && id == $rootScope.challengeDetails.id){
			server.getChallengeResults(id);
		}
	}

	$scope.highlightIfUser = function(username){
		if(username==$scope.user.user)
			return "highlightUser";
		return "";

	}
	$scope.highlightIfTableUser = function(username){
		if(username==$scope.user.user)
			return "highlightTableUser";
		return "";

	}


	$scope.isUser = function(username){
		if(username==$scope.user.user)
			return true
			return false;
	}

	$scope.getExerciseStatusForFlag = function(user,flag){
		var sf = getSelfCheckFromFlag(flag);
		if(sf=="")
			return "";
		var runExercises = $rootScope.challengeDetails.runExercises;
		for (var ex=0;ex<runExercises.length;ex++) {
			for(var res=0;res<runExercises[ex]["results"].length;res++){
				if (runExercises[ex]["results"][res].name==sf && runExercises[ex]["user"]["user"]==user) {
					return runExercises[ex].status;
				}
			}			
		}
	}

	$scope.getUserExerciseIdForUuid = function(uuid){
		var runExercises = $rootScope.challengeDetails.runExercises;
		for (var ex=0;ex<runExercises.length;ex++) {
			if (runExercises[ex]["exercise"]["uuid"]==uuid && runExercises[ex]["user"]["user"]==$scope.user.user) {
				return runExercises[ex].id;
			}
		}
	}
	$scope.getExerciseIdForFlag = function(user,flag){
		var sf = getSelfCheckFromFlag(flag);
		if(sf=="")
			return "";
		var runExercises = $rootScope.challengeDetails.runExercises;
		for (var ex=0;ex<runExercises.length;ex++) {
			for(var res=0;res<runExercises[ex]["results"].length;res++){
				if (runExercises[ex]["results"][res].name==sf && runExercises[ex]["user"]["user"]==user) {
					return runExercises[ex].id
				}
			}			
		}
	}
	function getSelfCheckFromFlag(flag){
		for(var f in $rootScope.challengeDetails.flags){
			if($rootScope.challengeDetails.flags.hasOwnProperty(f) && $rootScope.challengeDetails.flags[f].title==flag){
				for(var q in $rootScope.challengeDetails.flags[f].flagList){
					if($rootScope.challengeDetails.flags[f].flagList.hasOwnProperty(q) && $rootScope.challengeDetails.flags[f].flagList[q].selfCheckAvailable==true){
						return $rootScope.challengeDetails.flags[f].flagList[q].selfCheck.name;
					}
				}
			}
		}
		return "";
	}
	$scope.getClassPlacementInChallenge= function(usr,name){

		if($scope.challengeResults[usr]==undefined)
			return false;
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return false;
		if($scope.challengeResults[usr][sf]['firstForFlag']){
			return "goldPlacement";
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return "silverPlacement";
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return "bronzePlacement";
		}
		return "";
	}
	$scope.getPlacementInChallenge = function(usr,name){
		if($scope.challengeResults[usr]==undefined)
			return "";
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return "";

		if($scope.challengeResults[usr][sf]['firstForFlag']){
			return "1st";
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return "2nd";
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return "3rd";
		}
		return "";
	}

	$scope.isPlacedInChallenge = function(usr,name){

		if($scope.challengeResults[usr]==undefined)
			return "";
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return "";

		else if($scope.challengeResults[usr][sf]['firstForFlag']){
			return true;
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return true;
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return true;
		}
		return false;
	}

	$scope.$on('cancelChallengeExercise:updated', function(event,data) {
		$rootScope.getExerciseDetails($rootScope.challengeRetryExercise.uuid);
	});

	$rootScope.retryChallengeExerciseModal = function(id,uuid){
		$rootScope.challengeRetryExercise.uuid = uuid;
		$rootScope.challengeRetryExercise.id = id;
		$('#retryChallengeExerciseModal').modal('show');
	};
	$rootScope.challengeRetryExercise = {};
	$rootScope.challengeRetryExercise.id = "";
	$rootScope.challengeRetryExercise.uuid = "";
	$scope.retryChallengeExercise = function(){
		server.cancelChallengeExercise($rootScope.challengeRetryExercise.id);
		$('#retryChallengeExerciseModal').modal('hide');
	};

	$scope.getChallengeResultFor = function(usr,flag){
		var sf = getSelfCheckFromFlag(flag);

		if(sf=="")
			return "Not Started";
		var status = "-1";
		//loop1:
		try{
			var status = $scope.challengeResults[usr][sf].status
		}catch(e){
			status = "-1";
		}

		switch(status){
		case undefined:
			return "Not Started"
		case "1":
			return "Vulnerable"
		case "0":
			return "Not Vulnerable"
		case "2":
			return "Broken Functionality"
		case "3":
			return "Not Available"
		case "4":
			return "Not Addressed"
		case "5":
			return "Partially Remediated"
		default: return "Not Started"
		};
	}
	$scope.getClassForChallengeResult = function(user,flag){
		var status = $scope.getChallengeResultFor(user,flag);

		switch(status){
		case "-1":
			return "table-light"
		case "Vulnerable":
			return "table-danger"
		case "Not Vulnerable":
			return "table-success"
		case "Broken Functionality":
			return "table-warning"
		case "Not Addressed":
			return "table-info"
		case "Partially Remediated":
			return "table-warning"
		default: return "table-light"
		};



	}

	$scope.backToList = function(){
		$rootScope.showChallengeDetails = false;
		$rootScope.showChallengesList = true;
		$location.path("tournaments", false);

	}

	$scope.areThereCompleted = function(){
		var list = $filter("filter")($scope.masterChallengesList,{ status:"FINISHED"});
		if(list.length>0)
			return true;
		return false;
	}

	$scope.updateFilteredList = function() {
		if($scope.showCompleted)
			$scope.filteredChallengesList = $scope.masterChallengesList;
		else
			$scope.filteredChallengesList = $filter("filter")($scope.masterChallengesList,{ status:"!FINISHED"});

		if($scope.filteredChallengesList.length==0)
			$scope.noActiveChallenges = true;
		else
			$scope.noActiveChallenges = false;
	};
	$scope.challengestableconfig = {
			itemsPerPage: 20,
			fillLastPage: false
	}
	$rootScope.globalChallengeExercises = [];

	$scope.$on('challenges:updated', function(event,data) {

		if(null==data)
			data = [];
		$scope.lastUpdated = new Date();
		$rootScope.globalChallengeExercises = [];
		for(var j=0;j<data.length;j++){
			for (var i=0;i<data[j].scopeExercises.length;i++) {
				if($rootScope.globalChallengeExercises.indexOf(data[j].scopeExercises[i])==-1)
					$rootScope.globalChallengeExercises.push(data[j].scopeExercises[i]);
			}
		}
		if(null==data)
			data = []
		$scope.masterChallengesList = data;

		if($scope.showCompleted)
			$scope.filteredChallengesList = $scope.masterChallengesList;
		else
			$scope.filteredChallengesList = $filter("filter")($scope.masterChallengesList,{ status:"!FINISHED"});

		if($scope.filteredChallengesList.length==0)
			$scope.noActiveChallenges = true;
		else
			$scope.noActiveChallenges = false;

		$rootScope.$broadcast('challengesAreAvailable:updated',$scope.masterChallengesList);
	});

	$scope.getFormattedEnd = function(date){
		var out = moment(date).format("MMM Do YYYY");
		return out;
	};
}]);