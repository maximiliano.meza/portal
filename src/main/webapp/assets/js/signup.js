/*
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
function splitValue(value, index) {
	return (value.substring(0, index) + "," + value.substring(index)).split(',');
}
Array.prototype.remove = function(from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};

function deepCopy (arr) {
	var out = [];
	for (var i = 0, len = arr.length; i < len; i++) {
		var item = arr[i];
		var obj = {};
		for (var k in item) {
			obj[k] = item[k];
		}
		out.push(obj);
	}
	return out;
}

Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};


jQuery.extend({
	deepclone: function(objThing) {

		if ( jQuery.isArray(objThing) ) {
			return jQuery.makeArray( jQuery.deepclone($(objThing)) );
		}
		return jQuery.extend(true, {}, objThing);
	},
});
_st = function(fRef, mDelay) {
	if(typeof fRef == "function") {
		var argu = Array.prototype.slice.call(arguments,2);
		var f = (function(){ fRef.apply(null, argu); });
		return setTimeout(f, mDelay);
	}
	try{
		return window.setTimeout(fRef, mDelay);
	}
	catch(err){

	}
}




var sf = angular.module('sfNg-signup',['nya.bootstrap.select']);

//register the interceptor as a service
var compareTo = function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {

			ngModel.$validators.compareTo = function(modelValue) {
				return modelValue == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};
};

sf.directive("compareTo", compareTo);

sf.directive('complexPassword', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			ctrl.$parsers.unshift(function(password) {
				var hasUpperCase = /[A-Z]/.test(password);
				var hasLowerCase = /[a-z]/.test(password);
				var hasNumbers = /\d/.test(password);
				var hasNonalphas = /\W/.test(password);
				var characterGroupCount = hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas;

				if ((password.length >= 8) && (characterGroupCount >= 3)) {
					ctrl.$setValidity('complexity', true);
					return password;
				}
				else {
					ctrl.$setValidity('complexity', false);
					return undefined;
				}

			});
		}
	}
});

sf.service('server',function($http,$timeout,$rootScope){ 

	this.countries = [];

	function replaceArrayContent(obj1, obj2){
		obj1.remove(0,(obj1.length-1));
		for(var i in obj2){
			obj1.push(obj2[i]);
		}
	}
	function replaceObjectContent(obj1, obj2){
		for (var key in obj1){
			if (obj1.hasOwnProperty(key)){
				delete obj1[key];
			}
		}
		for(var i in obj2){
			obj1[i] = obj2[i]
		}
	}
	
	this.checkUsernameAvailable = function(username,code){

		var msg = {};
		msg.action = 'isUsernameAvailable';
		msg.username = username;
		msg.orgCode = code;
		var req = {
				method: 'POST',
				url: '/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('usernameAvailable:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	
	this.validateInviteCode = function(code){
		
		var msg = {};
		msg.action = 'isInviteCodeValid';
		msg.orgCode = code;
		var req = {
				method: 'POST',
				url: '/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('validateInviteCode:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getCountries = function($this){

		var msg = {};
		msg.action = 'getCountries';
		var req = {
				method: 'POST',
				url: '/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceArrayContent($this.countries,response.data)
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getCountries(this);

	this.doSignup = function(orgInviteCode, username,firstName,lastName,country,email,password){
		var msg = {};
		msg.action = 'doSignup';
		msg.orgCode = orgInviteCode;
		msg.username = username;
		msg.firstName = firstName;
		msg.lastName = lastName;
		msg.country = country;
		msg.email = email;
		msg.password = password;

		var req = {
				method: 'POST',
				url: '/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			if(response.data.result=="redirect"){
				$(document).attr('location', response.data.location);
			}
			else{
				alert("Signup failed, please try again or email support@secureflag.com")
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

});
sf.controller('signup',['$scope','server','$timeout',function($scope,server,$timeout){

	$scope.phase1 = true;
	$scope.phase2 = false;
	$scope.codeIsWrong = false;
	
	$scope.$on('validateInviteCode:updated', function(event,data) {
		if(data.result){
			$scope.codeIsWrong = false;
			$scope.phase1 = false;
			$scope.phase2 = true;
		}
		else{
			$scope.codeIsWrong = true;
		}
	});
	
	$scope.usernameAvailable = true;
	
	$scope.isUsernameAvailable = function(){
		if($scope.signup.username != "" )
			server.checkUsernameAvailable($scope.signup.username, $scope.signup.orgInvitationCode);
	}
	$scope.$on('usernameAvailable:updated', function(event,data) {
		$scope.usernameAvailable = data.result;
	});
	
	$scope.validateInviteCode = function(){
		server.validateInviteCode($scope.signup.orgInvitationCode);
	}

	$scope.signup = {};
	$scope.signup.orgInvitationCode = "";

	$scope.signup.username = "";	
	$scope.signup.firstName = "";
	$scope.signup.lastName = "";
	$scope.signup.selectedCountry = {};
	$scope.signup.email = "";
	$scope.signup.password = "";
	$scope.signup.repeatPassword = "";
	$scope.countries = server.countries;
	$scope.signup = function(){
		server.doSignup($scope.signup.orgInvitationCode,$scope.signup.username, $scope.signup.firstName,$scope.signup.lastName,$scope.signup.selectedCountry.short,$scope.signup.email,$scope.signup.password);
	}
}])
