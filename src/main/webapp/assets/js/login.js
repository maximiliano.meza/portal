/*
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

$(document).ready(function() {
	$('.waitLoader').hide();

	var getCToken = {}
	getCToken.action = "getUserCToken";
	$.ajax({
		type: "POST",
		url: '/user/handler',
		contentType: 'application/json',
		data: JSON.stringify(getCToken),
		success: function(obj) {
			try{
				if(obj.ctoken!=undefined && obj.ctoken.length > 3){
					console.log('user is logged in')
					document.location = "/user/index.html";
				}
				else{
					console.log('user is NOT logged in')
				}
			}catch(err){
				console.log('user is NOT logged in')
			}
		}
	});
	
	
	var featuresRequest = {};
	featuresRequest.action = "getPlatformFeatures";
	$.ajax({
		type: "POST",
		url: '/handler',
		contentType: 'application/json',
		data: JSON.stringify(featuresRequest),
		success: function(obj) {
			try{
				if(obj.emailModule==true){
					$('#resetPassword').show();
				}
			}catch(err){
				console.log('user is NOT logged in')
			}
		}
	});
	

	$('#loginButton').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		$('.waitLoader').show();
		var payload = {};
		payload.username = $('#username').val();
		payload.password = $('#password').val();
		payload.action = "doLogin";
		$.ajax({
			type: "POST",
			url: '/handler',
			contentType: 'application/json',
			data: JSON.stringify(payload),
			success: function(obj) {
				try{
					obj = JSON.parse(obj);
				}catch(err){}
				
				if(obj.result=="error" && obj.errMsg!=undefined && obj.errMsg=="VerifyEmail"){
					document.location = "/validateEmail.html";
				}
				else if(obj.result=="error" && obj.errMsg!=undefined && obj.errMsg=="ChangePassword"){
					document.location = "/changePassword.html";
				}
				else if(obj.result=="error"){
					$('#accountLockout').show();
					$('#password').val("");
					$('#loginButton').attr('disabled','disabled');
					$('#newMember').hide();
					$('.waitLoader').hide();
				}
				else if(obj.result=="redirect"){
					if(obj.location=="/index.html"){
						$('#incorrectLogin').show();
						$('#password').val("");
						$('.waitLoader').hide();
						setTimeout(function(){$("#incorrectLogin").hide()},5000);
					}
					else{
						$(document).attr('location', obj.location);
					}
				}


			}
		});
	});
	$(document).keyup(function(event){
		if(event.keyCode == 13){
			$("#loginButton").click();
		}
	});
	
	
});
