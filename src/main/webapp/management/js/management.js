/*
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

const { Editor } = toastui;
var codeSyntaxHightlight = Editor.plugin.codeSyntaxHighlight;
var colorSyntax = Editor.plugin.colorSyntax;

Number.isInteger = Number.isInteger || function(value) {
	return typeof value === 'number' && 
	isFinite(value) && 
	Math.floor(value) === value;
};
Array.prototype.remove = function(from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};
String.prototype.trim = function () {
	return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
};
String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};
String.prototype.hashCode = function() {
	var hash = 0, i, chr;
	if (this.length === 0) return hash;
	for (i = 0; i < this.length; i++) {
		chr   = this.charCodeAt(i);
		hash  = ((hash << 5) - hash) + chr;
	}
	return hash;
};
Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

function getSum(total, num) {
	return total + num;
}
function toTitleCase(str)
{
	return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
function htmlEncode(value){
	return $('<div/>').text(value).html();
}    
function deepCopy (arr) {
	var out = [];
	for (var i = 0, len = arr.length; i < len; i++) {
		var item = arr[i];
		var obj = {};
		for (var k in item) {
			obj[k] = item[k];
		}
		out.push(obj);
	}
	return out;
}
function cloneObj(obj){
	return JSON.parse(JSON.stringify(obj))
}
function replaceArrayContent(obj1, obj2){
	obj1.remove(0,(obj1.length-1));
	for(var i in obj2){
		obj1.push(obj2[i]);
	}
}
function replaceObjectContent(obj1, obj2){
	for (var key in obj1){
		if (obj1.hasOwnProperty(key)){
			delete obj1[key];
		}
	}
	for(var i in obj2){
		obj1[i] = obj2[i]
	}
}
jQuery.extend({
	deepclone: function(objThing) {

		if ( jQuery.isArray(objThing) ) {
			return jQuery.makeArray( jQuery.deepclone($(objThing)) );
		}
		return jQuery.extend(true, {}, objThing);
	},
});
function autoplayMdVideo(selector){
	$(selector+' .mdvideo video').each(function(){
		this.autoplay = true;
		this.controls = true;
		this.load();    
	});
}
function splitValue(value, index) {
	return (value.substring(0, index) + "," + value.substring(index)).split(',');
}
_st = function(fRef, mDelay) {
	if(typeof fRef == "function") {
		var argu = Array.prototype.slice.call(arguments,2);
		var f = (function(){ fRef.apply(null, argu); });
		return setTimeout(f, mDelay);
	}
	try{
		return window.setTimeout(fRef, mDelay);
	}
	catch(err){

	}
}
PNotify.prototype.options.stack.firstpos1 = 80; // or whatever pixel value you want.



$(document).ready(function(){

	$('#targetDiv').on('click','.snippetScroll', function(e){
		e.preventDefault();
		var offset = $('#'+$(this).attr('path')).offset();
		$('html, body').animate({
			scrollTop: offset.top - 50,
			scrollLeft: offset.left
		});
	})
	$('#completedTargetDiv').on('click','.snippetScroll', function(e){
		e.preventDefault();
		var offset = $('#'+$(this).attr('path')).offset();
		$('html, body').animate({
			scrollTop: offset.top - 50,
			scrollLeft: offset.left
		});
	})
});
var sf = angular.module('sfNg',['nya.bootstrap.select','jlareau.pnotify','ngRoute','ui.toggle','angular-table','chart.js','angular-notification-icons','angularSpinner','720kb.tooltips','ng-file-model','moment-picker','ngTagsInput','angular-timeline','ui.sortable']);


//register the interceptor as a service
var compareTo = function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {

			ngModel.$validators.compareTo = function(modelValue) {
				return modelValue == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};

};
sf.filter('capitalize', function() {
	return function(input) {
		return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
	}
});
sf.filter('inParenthesis', function() {
	return function(input) {
		return (!!input) ? "(" + input + ")" : '';
	}
});
sf.filter('titleCase', function() {
	return function(input) {
		input = input || '';
		return input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	};
})

sf.directive("compareTo", compareTo);

sf.directive('complexPassword', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			ctrl.$parsers.unshift(function(password) {
				var hasUpperCase = /[A-Z]/.test(password);
				var hasLowerCase = /[a-z]/.test(password);
				var hasNumbers = /\d/.test(password);
				var hasNonalphas = /\W/.test(password);
				var characterGroupCount = hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas;

				if ((password.length >= 8) && (characterGroupCount >= 3)) {
					ctrl.$setValidity('complexity', true);
					return password;
				}
				else {
					ctrl.$setValidity('complexity', false);
					return undefined;
				}

			});
		}
	}
});
sf.service('server',function($http,$timeout,$rootScope,notificationService,$interval){ 

	var $this = this; 
	this.countries = [];

	this.supportedAwsRegions = [];
	this.definedGateways = [];
	this.definedGatewaysRegions = [];

	$rootScope.hashCode = function(string) {
		var hash = 0, i, chr;
		if (string.length === 0) return hash;
		for (i = 0; i < string.length; i++) {
			chr   = string.charCodeAt(i);
			hash  = ((hash << 5) - hash) + chr;
		}
		return hash;
	};

	$rootScope.getRandom = function(){
		return Math.floor(100000 + Math.random() * 900000);
	}

	this.updateHubTechnologyKB = function(id){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'updateHubTechnologyKB';
		obj.uuid = id;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			if(undefined==response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Kb successfully updated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			$rootScope.$broadcast('hubTechnologyKBUpdated:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}
	this.updateHubVulnerabilityKB = function(id){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'updateHubVulnerabilityKB';
		obj.uuid = id;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			if(undefined==response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Kb successfully updated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			$rootScope.$broadcast('hubVulnerabilityKBUpdated:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}

	this.updateHubPath = function(uuid){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'installHubPath';
		obj.uuid = uuid;
		obj.orgs = [];
		obj.regions = [];
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			if(undefined==response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Learning Path in queue for update.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			$rootScope.$broadcast('hubPathUpdated:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}

	this.installHubPath = function(uuid, orgs, regions){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'installHubPath';
		obj.uuid = uuid;
		obj.orgs = orgs;
		obj.regions = regions;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			if(undefined==response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Learning Path in queue for download.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Installation failed, please try again.');
			}
			$rootScope.$broadcast('hubPathInstalled:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}



	this.updateHubExercise = function(uuid){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'installHubExercise';
		obj.uuid = uuid;
		obj.orgs = [];
		obj.regions = [];
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			if(undefined==response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Exercise in queue for update.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			$rootScope.$broadcast('hubExerciseUpdated:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}

	this.installHubExercises = function(uuid, orgs, regions){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'installHubExercise';
		obj.uuid = uuid;
		obj.orgs = orgs;
		obj.regions = regions;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			if(undefined==response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Exercise in queue for download.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Installation failed, please try again.');
			}
			$rootScope.$broadcast('hubExerciseInstalled:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}

	this.loadHubVulnKB = function(uuid,technology){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'getHubKBItem';
		obj.uuid = uuid;
		obj.technology = technology;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hubVulnKBLoaded:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}


	this.getHubVulnerabilityKBList = function(id){
		var obj = {};
		obj.action = 'getHubVulnerabilityKBList';
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hubVulnKBList:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}

	this.getHubTechnologyKBList = function(id){
		var obj = {};
		obj.action = 'getHubTechnologyKBList';
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hubTechKBList:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.loadVulnKBTechnology = function(uuid,technology){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'getKBItem';
		obj.uuid = uuid;
		obj.technology = technology;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('vulnKBLoaded:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}
	this.loadVulnKB = function(uuid){
		$('.waitLoader').show();
		var obj = {};
		obj.action = 'getKBItem';
		obj.uuid = uuid;
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('vulnKBLoaded:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}
	this.loadStackKB = function(uuid){
		$('.waitLoader').show();

		var obj = {};
		obj.action = 'getStackItem';
		obj.uuid = uuid;
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('stackKBLoaded:updated',response.data);
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});
	}
	this.updateScore = function(obj){
		obj.action = 'updateExerciseResult';
		$('.waitLoader').show();

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('scoringUpdated:updated',response.data);
			notificationService.success('Score updated.');
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}


	this.updateTechnology = function(tmpTech){
		var obj ={};
		obj.action = 'updateTechnology';
		obj.obj = tmpTech;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('updateTechnology:updated',response.data);
			if(undefined == response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Technology Stack KB item updated.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.deleteTechnology = function(tmpTech){
		var obj ={};
		obj.action = 'deleteTechnology';
		obj.uuid = tmpTech.uuid;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('deleteTechnology:updated',response.data);
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Technology Stack KB item deleted.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	
	this.addFramework = function(name){
		var obj ={};
		obj.action = 'addFramework';
		obj.name = name;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('addFramework:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.removeFramework = function(name){
		var obj ={};
		obj.action = 'removeFramework';
		obj.name = name;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('removeFramework:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.getFrameworks = function(){
		var obj ={};
		obj.action = 'getFrameworks';
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('getFrameworks:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	
	
	this.addTechnology = function(tmpTech){
		var obj ={};
		obj.action = 'addTechnology';
		obj.obj = tmpTech;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('addTechnology:updated',response.data);
			if(undefined == response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Technology Stack KB item added.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.updateVulnerability = function(tmpVuln){
		var obj ={};
		obj.action = 'updateVulnerability';
		obj.obj = tmpVuln;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('updateVulnerability:updated',response.data);
			if(undefined == response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Vulnerability KB item updated.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.deleteVulnerability = function(tmpVuln){
		var obj ={};
		obj.action = 'deleteVulnerability';
		obj.uuid = tmpVuln.uuid;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('deleteVulnerability:updated',response.data);
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Vulnerability KB item deleted.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.addVulnerability = function(tmpVuln){
		var obj ={};
		obj.action = 'addVulnerability';
		obj.obj = tmpVuln;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('addVulnerability:updated',response.data);
			if(undefined == response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Vulnerability KB item added.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}


	this.getAllStacks = function(){

		var obj ={};
		obj.action = 'getAllStacks';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('stackKbs:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.addScoringComment = function(obj){

		obj.action = 'addResultComment';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,	
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('scoringCommentAdded:updated',response.data);
			notificationService.success('Comment sent.');
			$('.waitLoader').hide();
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.getPaths = function(){
		var obj = {};
		obj.action = 'getPaths';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('paths:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.addPath = function(obj){
		obj.action = 'addPath';



		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(undefined == response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Learning Path added.');
				$rootScope.$broadcast('addPath:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.updatePath = function(obj){
		obj.action = 'updatePath';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(undefined == response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Learning Path modified.');
				$rootScope.$broadcast('updatePath:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.removePath = function(id){
		var obj = {};
		obj.action = 'removePath';
		obj.id = id;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Learning Path removed.');
				$rootScope.$broadcast('removePath:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}


	this.addChallenge = function(obj){
		obj.action = 'addChallenge';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Challenge added.');
				$rootScope.$broadcast('addChallenge:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.getUsersInOrg = function(organization){

		var obj = {};
		obj.action = 'getUsersInOrg';
		obj.orgId = organization.id;

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('usersInOrg:updated',response.data);

		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}


	this.addExercise = function(exercise){

		var obj = cloneObj(exercise);
		obj.action = 'addExercise';

		for(var i=0;i<obj.tags.length;i++){
			if (typeof obj.tags[i] === 'string' || obj.tags[i] instanceof String){
				continue;
			}
			else{
				obj.tags[i] = obj.tags[i].text;
			}
		}
		for(var j=0;j<obj.flags.length;j++){
			for(var k=0;k<obj.flags[j].flagList.length;k++){
				if(undefined!=obj.flags[j].flagList[k] && undefined!=obj.flags[j].flagList[k].selfCheck){
					for(var l in obj.flags[j].flagList[k].selfCheck.statusMapping){
						if (obj.flags[j].flagList[k].selfCheck.statusMapping.hasOwnProperty(l) && typeof obj.flags[j].flagList[k].selfCheck.statusMapping[l] === 'string' || obj.flags[j].flagList[k].selfCheck.statusMapping[l] instanceof String){
							continue;
						}
						else{
							if(undefined!=obj.flags[j].flagList[k].selfCheck.statusMapping[l].id)
								obj.flags[j].flagList[k].selfCheck.statusMapping[l] = obj.flags[j].flagList[k].selfCheck.statusMapping[l].id;
						}
					}
				}
			}
		}
		obj.status = $rootScope.getStatusCodeFromText(obj.status);
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(undefined == response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Exercise added.');
				$rootScope.$broadcast('exerciseAdded:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}

		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	};
	this.updateChallenge = function(obj){

		obj.action = 'updateChallenge';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Challenge updated.');
				$rootScope.$broadcast('challengeUpdated:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}

	this.updateAutomatedScoringForExercise = function(id,scoring){

		var obj = {};
		obj.action = 'updateExerciseDefaultScoring';
		obj.id = id;
		obj.defaultScoring = scoring;

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Exercise default scoring updated.');
				$rootScope.$broadcast('exerciseDefaultScoreUpdated:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').show();
			console.log('ajax error');
		});
	}

	this.updateExercise = function(exercise){
		var obj = cloneObj(exercise);
		obj.action = 'updateExercise';
		for(var i=0;i<obj.tags.length;i++){
			if (typeof obj.tags[i] === 'string' || obj.tags[i] instanceof String){
				continue;
			}
			else{
				obj.tags[i] = obj.tags[i].text;
			}
		}
		for(var j=0;j<obj.flags.length;j++){
			for(var k=0;k<obj.flags[j].flagList.length;k++){
				if(undefined!=obj.flags[j].flagList[k] && undefined!=obj.flags[j].flagList[k].selfCheck){
					for(var l in obj.flags[j].flagList[k].selfCheck.statusMapping){
						if (obj.flags[j].flagList[k].selfCheck.statusMapping.hasOwnProperty(l) && typeof obj.flags[j].flagList[k].selfCheck.statusMapping[l] === 'string' || obj.flags[j].flagList[k].selfCheck.statusMapping[l] instanceof String){
							continue;
						}
						else{
							if(undefined!=obj.flags[j].flagList[k].selfCheck.statusMapping[l].id)
								obj.flags[j].flagList[k].selfCheck.statusMapping[l] = obj.flags[j].flagList[k].selfCheck.statusMapping[l].id;
						}
					}
				}
			}
		}
		obj.status = $rootScope.getStatusCodeFromText(obj.status)	
		var req = {
			method: 'POST',
			url: '/management/sfadmin/handler',
			data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(undefined == response.data.result || response.data.result!="error"){
				PNotify.removeAll();
				notificationService.success('Exercise updated.');
				$rootScope.$broadcast('exerciseUpdated:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}

	this.updateOrganization = function(obj){

		obj.status = $rootScope.getOrgStatusCodeFromText(obj.status);
		obj.action = 'updateOrganization';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Organization updated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			$rootScope.$broadcast('organizationUpdated:updated',response.data);

		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}


	this.addOrganization = function(obj){

		obj.status = $rootScope.getOrgStatusCodeFromText(obj.status);
		obj.action = 'addOrganization';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				notificationService.success('Organization added. We recommend to logout and login again to manage the newly created organization.');
				PNotify.removeAll();
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			$rootScope.$broadcast('organizationAdded:updated',response.data);

		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}

	this.getInvitationCodes = function(id){

		var obj = {};
		obj.orgId = id;
		obj.action = 'getInvitationCodes';

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: obj,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('invitationCodes:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.generateInvitationCode = function(obj){

		obj.action = 'generateInvitation';
		
		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Invitation Code generated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Generation failed, please try again.');
			}
			$rootScope.$broadcast('invitationGenerated:updated',response.data);

		}, function errorCallback(response) {
			$('.waitLoader').show();
			console.log('ajax error');
		});

	}

	this.removeInvitationCode = function(obj){

		obj.action = 'removeInvitation';

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Invitation code removed.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Removal failed, please try again.');
			}
			$rootScope.$broadcast('invitationRemoved:updated',response.data);

		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}


	this.deleteUserAccount = function(usr){
		var msg = {};
		msg.action = 'removeUser';
		msg.username = usr;
		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('userDeleted:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.removeUser = function(){
		var msg = {};
		msg.action = 'removeUser';
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('removeUser:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.enableExerciseInRegion = function(taskId){
		msg = {};
		msg.taskDefId = taskId;
		msg.action = "enableExerciseInRegion";

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').show();
			$rootScope.$broadcast('exerciseInRegionEnabled:updated',response.data);
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Task definition enabled.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Update failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});	


	}
	this.disableExerciseInRegion = function(taskId){
		msg = {};
		msg.taskDefId = taskId;
		msg.action = "disableExerciseInRegion";
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('exerciseInRegionDisabled:updated',response.data);
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Task definition disabled.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Update failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});	


	}
	this.removeExerciseInRegion = function(exerciseId, taskId){
		msg = {};
		msg.exerciseId = exerciseId;
		msg.taskDefId = taskId;
		msg.action = "removeExerciseInRegion";
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('exerciseInRegionRemoved:updated',response.data);
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Task definition removed.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Update failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});	
	}

	this.addTaskDefinition = function(msg){
		msg.action = "addTaskDefinition";
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('taskDefinitionAdded:updated',response.data);
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Task definition added.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Update failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});	


	}

	this.deleteGateway = function(id){
		var msg = {};
		msg.id = id;
		msg.action = 'deleteGateway';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();

		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('gatewayDeleted:updated',response.data);
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Gateway deleted.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Gateways can be removed only if there is another Gateway defined for the same region, or if there are no exercises associated in that region.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});	
	}

	this.getAllOrganizations = function(){

		var msg = {};
		msg.action = 'getAllOrganizations';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('allOrganizations:updated',response.data);

		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.getOrganizations = function(){

		var msg = {};
		msg.action = 'getOrganizations';

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('organizations:updated',response.data);

		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.updateGateway = function(obj){

		obj.action = 'updateGateway';
		delete obj.region;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Gateway updated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			$rootScope.$broadcast('gatewayAdded:updated',response.data);

		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}
	this.addGateway = function(obj){

		obj.action = 'addGateway';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: obj,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Gateway added.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			$rootScope.$broadcast('gatewayAdded:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}
	this.getAWSRegions = function(){

		var msg = {};
		msg.action = 'getAWSRegions';

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceObjectContent($this.supportedAwsRegions,response.data);

			$rootScope.$broadcast('awsRegions:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.getOnlineGws = function(){

		var msg = {};
		msg.action = 'getOnlineGateways';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {

			$rootScope.$broadcast('onlineGws:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	
	this.getGateways = function(){

		var msg = {};
		msg.action = 'getGateways';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceObjectContent($this.definedGateways,response.data);
			var tmpGatewaysRegions = [];
			for(var i=0;i<response.data.length;i++){
				tmpGatewaysRegions.push(response.data[i].region);
			}
			replaceArrayContent($this.definedGatewaysRegions,tmpGatewaysRegions);
			$rootScope.$broadcast('gateways:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.getCountries = function(){

		var msg = {};
		msg.action = 'getCountries';

		var req = {
				method: 'POST',
				url: '/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceArrayContent($this.countries,response.data)
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.user = {}

	this.getUserProfileInfo = function(){
		var msg = {};
		msg.action = 'getUserInfo';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('userProfile:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	
	this.getChallengeResults = function(id){

		var msg = {};
		msg.action = 'getChallengeResults';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			if(response.data.length==0)
				$rootScope.$broadcast('challengeResults:updated',null);
			else
				$rootScope.$broadcast('challengeResults:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	
	this.getChallengeDetails = function(id){
		var msg = {};
		msg.action = 'getChallengeDetails';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			if(response.data.length==0)
				$rootScope.$broadcast('challengeDetails:updated',null);
			else
				$rootScope.$broadcast('challengeDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.getAllKbs  = function(){

		var msg = {};
		msg.action = 'getAllKbs';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('vulnerabilityKbs:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}    
	this.getChallenges = function(){

		var msg = {};
		msg.action = 'getChallenges';

		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('challenges:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}    

	this.getHubExercises = function(){

		var msg = {};
		msg.action = 'getHubExercises';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hubExercises:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.getHubExerciseDetails = function(uuid){

		var msg = {};
		msg.action = 'getHubExerciseDetails';
		msg.uuid = uuid;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hubExerciseDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}


	this.restartQueueItem = function(id){

		var msg = {};
		msg.action = 'restartInstallationQueueItem';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('queueItemRestarted:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}
	this.removeQueueItem = function(id){

		var msg = {};
		msg.action = 'removeInstallationQueueItem';
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('queueItemRemoved:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}
	this.getInstallationQueue = function(){

		var msg = {};
		msg.action = 'getInstallationQueue';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hubInstallationQueue:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.getHubPaths = function(){

		var msg = {};
		msg.action = 'getHubPaths';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('hubPaths:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.getUserProfile = function(){

		var msg = {};
		msg.action = 'getUserInfo';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			replaceObjectContent($this.user,response.data);
			$rootScope.$broadcast('userProfile:updated',response.data);
			$('.waitLoader').hide();
			if(response.data.r!= 3){
				$this.getGlobalStats([]);
				$this.getUsers();
			}
			$this.getTeams();
			$this.getUnreadNotifications();
			$this.getChallenges();

			
			if(response.data.r <= 3){
				$this.getPaths();
				$this.getPendingReviews();
				$this.getCompletedReviews();
				$this.getAvailableExercises();
				$this.getRunningExercises();
			}
			if(response.data.r <= 0){
				$this.getOrganizations();
				$this.getAWSRegions();
			}
			if(response.data.r<0){
				$this.getGateways();
				$this.getOnlineGws();
				$this.getHubExercises();
				$this.getHubPaths();
				$this.getInstallationQueue();
				$this.getAllOrganizations();
				$this.getHubVulnerabilityKBList();
				$this.getHubTechnologyKBList();
			}
			$this.getAllKbs();
			$this.getAllStacks();
			$this.getFrameworks();
		}, function errorCallback(response) {
			console.log('ajax error');
			$('.waitLoader').hide();
		});

	}
	this.updateUserProfile = function(updatedUser){
		var msg = {};
		msg.action = 'setUserInfo';
		msg.firstName = updatedUser.firstName;
		msg.lastName = updatedUser.lastName;
		msg.email = updatedUser.email;
		msg.country = updatedUser.country.short;

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User profile updated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}

	this.removeChallenge = function(id){
		var msg = {};
		msg.action = "removeChallenge";
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again. You can remove a challenge if no exercises were run. For further help, please contact support.');
			}
			else{
				PNotify.removeAll();
				notificationService.success('Challenge removed.');
				$rootScope.$broadcast('challengeRemoved:updated',response.data);
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}


	this.removeExercise = function(id){
		var msg = {};
		msg.action = "removeExercise";
		msg.exerciseId = id;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again. You can remove an exercise if it\'s not referenced by any user, team, run exercises. For further help, please contact support.');
			}
			else{
				PNotify.removeAll();
				notificationService.success('Exercise removed.');
				$rootScope.$broadcast('exerciseRemoved:updated',response.data);
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	};
	this.removeOrganization = function(id){
		var msg = {};
		msg.action = "removeOrganization";
		msg.orgId = id;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again. You can remove an organization if there are no users, teams, invitation codes, run/available exercises associated. For further help, please contact support.');
			}
			else{
				PNotify.removeAll();
				notificationService.success('Organization removed.');
				$rootScope.$broadcast('organizationRemoved:updated',response.data);
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	};

	this.addUser = function(user){
		var msg = {};
		msg.action = 'addUser';
		msg.username = user.username;
		msg.firstName = user.firstName;
		msg.lastName = user.lastName;
		msg.email = user.email;
		msg.country = user.country.short;
		msg.roleId = user.role.id;
		msg.concurrentExercisesLimit = user.concurrentExercisesLimit;
		msg.password = user.password;
		msg.forceChangePassword = user.forceChangePassword;
		msg.emailVerified = user.emailVerified;
		msg.orgId = user.organization.id;
		msg.credits = user.credits
		msg.orgs = user.organizations;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			else{
				PNotify.removeAll();
				notificationService.success('User added.');
				$rootScope.$broadcast('userAdded:updated',response.data);
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.updateUser = function(id,user){
		var msg = {};
		msg.action = 'updateUser';
		msg.id = id;
		msg.username = user.username;
		msg.firstName = user.firstName;
		msg.lastName = user.lastName;
		msg.email = user.email;
		msg.country = user.country.short;
		msg.roleId = user.role.id;
		msg.concurrentExercisesLimit = user.concurrentExercisesLimit;
		msg.orgId = user.organization.id;
		msg.credits = user.credits
		msg.orgs = user.organizations;
		msg.forceChangePassword = user.forceChangePassword;
		msg.emailVerified = user.emailVerified;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			else{
				PNotify.removeAll();
				notificationService.success('User updated.');
				$rootScope.$broadcast('userUpdate:updated',response.data);
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}


	this.addUsersToTeam = function(userList, id){
		var msg = {};
		msg.action = 'addUsersToTeam';
		msg.users = userList;
		msg.id = id;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
			else{
				PNotify.removeAll();
				notificationService.success('Users added to team.');
				$rootScope.$broadcast('usersAddedToTeam:updated',response.data);
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.getRegionsForExercise = function(uuid){

		var msg = {};
		msg.action = 'getRegionsForExercise';
		msg.uuid = uuid;

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('exerciseRegions:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getRunningExercises = function(){
		var msg = {};
		msg.action = 'getAllRunningExercises';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg
		}
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('runningExercises:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getExerciseDetails = function(uuid){

		var msg = {};
		msg.action = 'getExerciseDetails';
		msg.uuid = uuid;

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('exerciseDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.isTeamNameAvailable = function(name,orgId){

		var msg = {};
		msg.action = 'checkTeamNameAvailable';
		msg.name = name;
		msg.orgId = orgId;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('teamNameAvailable:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.isChallengeNameAvailable = function(name,org){

		var msg = {};
		msg.action = 'checkChallengeNameAvailable';
		msg.name = name;
		msg.orgId = org;
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('challengeNameAvailable:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.isPathNameAvailable = function(name,tech,diff){

		var msg = {};
		msg.action = 'checkPathNameAvailable';
		msg.name = name;
		msg.technology = tech;
		msg.difficulty = diff;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('pathNameAvailable:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.isOrgNameAvailable = function(name){

		var msg = {};
		msg.action = 'checkOrganizationNameAvailable';
		msg.name = name;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('orgNameAvailable:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.checkUsernameAvailable = function(username){

		var msg = {};
		msg.action = 'isUsernameAvailable';
		msg.username = username;
		var req = {
				method: 'POST',
				url: '/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('usernameAvailable:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.sendNotification = function(username,text){
		var msg = {};
		msg.action = 'sendNotification';
		msg.text = text;
		msg.username = username;

		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Notification sent.');
				$rootScope.$broadcast('notificationSent:updated',response.data);
			}
			else if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Action failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.removeTeamManager = function(teamId,username){
		var msg = {};
		msg.action = 'removeTeamManager';
		msg.teamId = teamId;
		msg.username = username;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User removed from team managers.');
				$rootScope.$broadcast('makeTeamManager:updated',response.data);
			}
			else if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.makeTeamManager = function(teamId,username){
		var msg = {};
		msg.action = 'addTeamManager';
		msg.teamId = teamId;
		msg.username = username;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User added as team manager.');
				$rootScope.$broadcast('makeTeamManager:updated',response.data);
			}
			else if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.removeFromTeam = function(teamId,username){
		var msg = {};
		msg.action = 'removeFromTeam';
		msg.teamId = teamId;
		msg.username = username;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User removed from team.');
				$rootScope.$broadcast('deletedFromTeam:updated',response.data);
			}
			else if(response.data.result=="error"){
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.deleteTeam = function(teamId){
		var msg = {};
		msg.action = 'deleteTeam';
		msg.teamId = teamId;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Team deleted.');
				$rootScope.$broadcast('deletedTeam:updated',response.data);
			}
			else if(response.data.result=="error"){
				if(response.data.errorMsg="TeamNotEmpty"){
					PNotify.removeAll();
					notificationService.notice('Team cannot be deleted, please remove all members first.');
				}
				else{
					PNotify.removeAll();
					notificationService.notice('Updated failed, please try again.');
				}
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.addTeam = function(teamName,orgName){
		var msg = {};
		msg.action = 'addTeam';
		msg.teamName = teamName;
		msg.orgName = orgName;

		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('Team created.');
				$rootScope.$broadcast('addTeam:updated',response.data);
			}
			else if(response.data.result=="error"){
				if(response.data.errorMsg=="TeamExists"){
					PNotify.removeAll();
					notificationService.notice('Specified team name already exists.');
				}
				else{
					PNotify.removeAll();
					notificationService.notice('Updated failed, please try again.');
				}
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.getAvailableExercises = function(){

		var msg = {};
		msg.action = 'getExercises';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('availableExercises:updated',response.data);

		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.enableExerciseForOrg = function(uuid,idOrg){
		var msg = {};
		msg.action = 'enableExerciseForOrg';
		msg.orgId = idOrg;
		msg.exercise = uuid;
		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(undefined==response.data.error){
				$rootScope.$broadcast('enableExerciseForOrg:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.disableExerciseForOrg = function(uuid,idOrg){
		var msg = {};
		msg.orgId = idOrg;
		msg.exercise = uuid;
		msg.action = 'disableExerciseForOrg';

		var req = {
				method: 'POST',
				url: '/management/sfadmin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(undefined==response.data.error){
				$rootScope.$broadcast('disableExerciseForOrg:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.getUnreadNotifications = function(){

		var msg = {};
		msg.action = 'getNotifications';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('unreadNotifications:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.markNotificationAsRead = function(idNotification){

		var msg = {};
		msg.action = 'markNotificationRead';
		msg.id = idNotification;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(undefined==response.data.error){
				$rootScope.$broadcast('unreadNotifications:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.updateUserPassword = function(oldPassword, newPassword){
		var msg = {};
		msg.action = 'setUserPassword';
		msg.oldPwd = oldPassword;
		msg.newPwd = newPassword;
		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User password updated.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}

		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});

	}

	this.resetEmailUserPassword = function(usr){
		var msg = {};
		msg.action = 'resetEmailUserPassword';
		msg.username = usr;
		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User password resetted.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.resetUserPassword = function(usr, pwd){
		var msg = {};
		msg.action = 'resetUserPassword';
		msg.username = usr;
		msg.password = pwd;
		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User password resetted.');
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.updateUserStatus = function(username,status){
		var msg = {};
		msg.action = 'updateUserStatus';
		msg.username = username;
		msg.status = status;
		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User status updated.');
				$rootScope.$broadcast('userStatus:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.unlockUserAccount = function(username){
		var msg = {};
		msg.action = 'unlockUserAccount';
		msg.username = username;
		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			if(response.data.result=="success"){
				PNotify.removeAll();
				notificationService.success('User account unlocked.');
				$rootScope.$broadcast('accountUnlocked:updated',response.data);
			}
			else{
				PNotify.removeAll();
				notificationService.notice('Updated failed, please try again.');
			}
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.doUserLogout = function(){
		var msg = {};
		msg.action = 'doLogout';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$(document).attr('location', "/");
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	
	this.getPlatformFeatures = function(){
		var obj = {};
		obj.action = 'getPlatformFeatures';
		var req = {
				method: 'POST',
				url: '/handler',
				data: obj,	
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('platformFeatures:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	$rootScope.ctoken = "";

	this.getCToken = function(){
		var msg = {};
		msg.action = 'getUserCToken';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.ctoken = response.data.ctoken;
			$this.getInitialData();
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getUsers = function(){
		var msg = {};
		msg.action = 'getUsers';
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('usersList:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getGlobalStats = function(filter){
		var msg = {};
		msg.action = 'getGlobalStats';
		msg.filter = filter;
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('stats:updated',response.data);
			$('#waitLoader').hide();
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getUserStats = function(username){
		var msg = {};
		msg.username = username;
		msg.action = 'getUserStats';
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('statsUser:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getUserExercises = function(username){
		var msg = {};
		msg.username = username;
		msg.action = 'getUserExercises';
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('userCompletedExercises:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}


	this.getTeamStats = function(id){
		var msg = {};
		msg.id = id;
		msg.action = 'getTeamStats';
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('statsTeam:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getUserAchievements = function(usr){
		var msg = {};
		msg.action = 'getUserAchievements';
		msg.username = usr;
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('userAchievements:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.renameTeam = function(teamId,teamName){
		var msg = {};
		msg.action = 'renameTeam';
		msg.teamId = teamId;
		msg.name = teamName;
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('teamRenamed:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}
	this.getUserDetails = function(usr){
		var msg = {};
		msg.action = 'getUserDetails';
		msg.username = usr;
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('userDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getCompletedReviews = function(){
		var msg = {};
		msg.action = 'getCompletedReviews';
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('completedReviews:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getTeams = function(){
		var msg = {};
		msg.action = 'getTeams';
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('teamsList:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getTeamDetails = function(id){
		var msg = {};
		msg.action = 'getTeamDetails';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('teamDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}
	this.getTeamMembers = function(id){
		var msg = {};
		msg.action = 'getTeamMembers';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/management/stats/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('teamMembers:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});
	}

	this.getCToken();

	this.getInitialData = function(){  
		this.getCountries();
		this.getUserProfile();
		this.getPlatformFeatures();
		initialData = true;
	}

	this.getPendingReviews = function(){
		var msg = {};
		msg.action = 'getPendingReviews';

		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('pendingReviews:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.getPendingReviewDetails = function(id){
		var msg = {};
		msg.action = 'getReviewDetails';
		msg.id = id
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('pendingReviewDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.getCompletedReviewDetails = function(id){
		var msg = {};
		msg.action = 'getReviewDetails';
		msg.id = id
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('completedReviewDetails:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}
	this.getUserFeedback = function(id){
		var msg = {};
		msg.action = 'getUserFeedback';
		msg.id = id
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.$broadcast('userFeedback:updated',response.data);
		}, function errorCallback(response) {
			console.log('ajax error');
		});

	}

	this.submitReview = function(o){
		var msg = {}
		msg.action = 'postReview';
		msg.obj = o;
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			PNotify.removeAll();
			notificationService.success('Review Submitted');
			$rootScope.$broadcast('reviewSubmitted:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});
	}

	this.getAvailableUsersForTeam = function(id){
		var msg ={};
		msg.id = id;
		msg.action = 'getAvailableUsersForTeam';
		var req = {
				method: 'POST',
				url: '/management/admin/handler',
				data: msg,
		}
		$('.waitLoader').show();
		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			$rootScope.$broadcast('availableUsersTeam:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});


	}

	this.markCancelled = function(id){
		var msg ={};
		msg.action = 'markAsCancelled';
		msg.id = id;
		var req = {
				method: 'POST',
				url: '/management/team/handler',
				data: msg,
		}
		$('.waitLoader').show();

		$http(req).then(function successCallback(response) {
			$('.waitLoader').hide();
			PNotify.removeAll();
			notificationService.success('Exercise marked as cancelled.');
			$rootScope.$broadcast('reviewCancelled:updated',response.data);
		}, function errorCallback(response) {
			$('.waitLoader').hide();
			console.log('ajax error');
		});


	}



})
sf.controller('navigation',['$rootScope','$scope','server','$timeout','$http','$location',function($rootScope,$scope,server,$timeout,$http,$location){
	$scope.user = server.user;


	updateData = function(){
		if($rootScope.ctoken == ""){
			return;
		}
		if($('.modal.in').length>0){
			console.log('modal open, skipping data udpdate...')
			return;
		}
		if($rootScope.visibility.review){
			console.log('review tab open, skipping data udpdate...')
			return;
		}

		console.log('updating data...')

		server.getTeams();
		server.getUnreadNotifications();
		server.getChallenges();

		if(server.user.r != 3){
			server.getUsers();
			server.getGlobalStats([]);
		}
		if(server.user.r <= 3){
			server.getPendingReviews();
			server.getCompletedReviews();
			server.getRunningExercises();
		}
	}
	setInterval(function(){updateData()},120000)



	$scope.logout = function(){
		server.doUserLogout();
	}
	$scope.notifications = [];

	$scope.goTo = function(link){
		$location.path(link.replace("#",""), false);
	}
	$scope.$on('unreadNotifications:updated', function(event,data) {
		$scope.notifications = data; 
	})
	$scope.markAllNotificationsRead = function(){
		server.markNotificationAsRead(-1);
	}
	$scope.markNotificationRead = function(id){
		server.markNotificationAsRead(id);
	}

	var ctokenReq = {
			method: 'POST',
			url: '/user/handler',
			data: { action: 'getUserCToken'}
	}
	var getExercisesReq = {
			method: 'POST',
			url: '/management/sfadmin/handler',
			data: { action: 'getHubExercises'}
	}
	var getPathsReq = {
			method: 'POST',
			url: '/management/sfadmin/handler',
			data: { action: 'getHubPaths'}
	}

	$rootScope.$on('$locationChangeSuccess', function(event, newUrl, oldUrl){
		if(newUrl.indexOf('d2h-')>-1){
			return;
		}
		var target = newUrl.substr(newUrl.indexOf("#")).replace("#","").replace("/","");
		target = target.split("/")
		switch(target[0]){       
		
		case "tournaments":
			if(target[1]=="details" && undefined!=target[2] && ""!=target[2]){
				var exId = target[2]
				if($rootScope.ctoken == ""){
					$http(ctokenReq).then(function successCallback(response) {
						$rootScope.ctoken = response.data.ctoken;
						server.getChallengeDetails(exId);

					}, function errorCallback(response) {
						console.log('ajax error');
					});
				}
				else{
					server.getChallengeDetails(exId);
				}
				$rootScope.showChallengeDetails = true;
				$rootScope.showChallengesList = false;
			}
			else{
				$rootScope.showChallengeDetails = false;
				$rootScope.showChallengesList = true;
			}
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.running = false;

			$rootScope.visibility.challenges = true;
			$(window).scrollTop(0);
			break;
		case "learning-paths":
			$rootScope.showChallengeDetails = false;
			$rootScope.showChallengesList = true;
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.paths = true;
			$(window).scrollTop(0);
			break;
		case "users":
			if(target[1]=="new"){
				return;
			}
			if(target[1]=="details" && undefined!=target[2] && ""!=target[2]){
				try{
					username = decodeURIComponent(target[2])
				}catch(err){
					username = target[2];
				}
				if($rootScope.ctoken == ""){
					$http(ctokenReq).then(function successCallback(response) {
						$rootScope.ctoken = response.data.ctoken;
						server.getUserDetails(username);
						server.getUserStats(username);
						server.getUserExercises(username);
						server.getUserAchievements(username);
						$rootScope.showUserList = false;
						$rootScope.showUserDetails = true;
						return;
					}, function errorCallback(response) {
						console.log('ajax error');
					});
				}
				server.getUserDetails(username);
				server.getUserStats(username);
				server.getUserExercises(username);
				server.getUserAchievements(username);
				$rootScope.showUserList = false;
				$rootScope.showUserDetails = true;
			}
			else{
				$rootScope.showUserList = true;
				$rootScope.showUserDetails = false;
			}
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.users = true;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.running = false;
			$(window).scrollTop(0);
			break;
		case "review":
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.review = true;
			$(window).scrollTop(0);
			break;
		case "running-exercises":
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.running = true;
			$(window).scrollTop(0);
			break;
		case "teams":
			if(target[1]=="details" && undefined!=target[2] && ""!=target[2]){
				id = target[2]
				if($rootScope.ctoken == ""){
					$http(ctokenReq).then(function successCallback(response) {
						$rootScope.ctoken = response.data.ctoken;
						$rootScope.selectedTeamId = id;
						server.getTeamMembers(id);
						server.getTeamStats(id);
						server.getTeamDetails(id);
						$rootScope.showTeamList = false;
						$rootScope.showTeamMembers = true;
						return;
					}, function errorCallback(response) {
						console.log('ajax error');
					});
				}
				else{
					$rootScope.selectedTeamId = id;
					server.getTeamMembers(id);
					server.getTeamStats(id);
					server.getTeamDetails(id);
					$rootScope.showTeamList = false;
					$rootScope.showTeamMembers = true;
				}
				$rootScope.showTeamList = false;
				$rootScope.showTeamMembers = true;
			}
			else{
				$rootScope.showTeamList = true;
				$rootScope.showTeamMembers = false;
			}
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.teams = true;
			$(window).scrollTop(0);
			break;
		case "exercises":
			if(target[1]=="details" && undefined!=target[2] && ""!=target[2]){
				var eId = target[2];
				if($rootScope.ctoken == ""){
					$http(ctokenReq).then(function successCallback(response) {
						server.getCompletedReviewDetails(eId);
						server.getUserFeedback(eId);
					}, function errorCallback(response) {
						console.log('ajax error');
					});
				}
				else{
					server.getCompletedReviewDetails(eId);
					server.getUserFeedback(eId);
				}
			}
			else{
				$(window).scrollTop(0);
			}
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.exercises = true;
			break;
		case "stats":
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.stats = true;
			$(window).scrollTop(0);
			break;
		case "settings":
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.settings = true;
			$(window).scrollTop(0);
			break;
		case "available-exercises":
			if(target[1]=="new"){
				return;
			}
			if(target[3]=="flags" || target[3] == "info"){
				return;
			}
			if(target[1]=="details"){
				$rootScope.visibility.users = false;
				$rootScope.visibility.stats = false;
				$rootScope.visibility.review = false;
				$rootScope.visibility.organizations = false;
				$rootScope.visibility.exercises = false;
				$rootScope.visibility.settings = false;
				$rootScope.visibility.challenges = false;
				$rootScope.visibility.paths = false;
				$rootScope.visibility.hub = false;
				$rootScope.visibility.teams = false;
				$rootScope.visibility.kb = false;
				$rootScope.visibility.gateways = false;
				$rootScope.visibility.running = false;
				$rootScope.visibility.availableExercises = true;
				if((undefined==$rootScope.exerciseDetails.uuid || $rootScope.exerciseDetails.uuid!=target[2])){
					getExDetails(target[2]);
				}
			}
			else{
				$rootScope.visibility.users = false;
				$rootScope.visibility.stats = false;
				$rootScope.visibility.review = false;
				$rootScope.visibility.organizations = false;
				$rootScope.visibility.paths = false;
				$rootScope.visibility.hub = false;
				$rootScope.visibility.exercises = false;
				$rootScope.visibility.settings = false;
				$rootScope.visibility.kb = false;
				$rootScope.visibility.teams = false;
				$rootScope.visibility.challenges = false;
				$rootScope.visibility.availableExercises = true;
				$rootScope.visibility.gateways = false;
				$rootScope.visibility.running = false;
				$rootScope.showExerciseList = true;
				$rootScope.showExerciseDetails = false;
			}
			$(window).scrollTop(0);
			break;
		case "organizations":
			$rootScope.visibility.kb = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.organizations = true;
			$(window).scrollTop(0);
			break;
		case "kb":
			$rootScope.visibility.kb = true;
			$rootScope.visibility.users = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.gateways = false;
			$(window).scrollTop(0);
			break;
		case "gateways":
			$rootScope.visibility.users = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.gateways = true;
			$(window).scrollTop(0);
			break;
		case "exercise-hub":
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.stats = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.hub = true;

			if(undefined==target[1]){
				$location.path("exercise-hub/vulnerability", true);
				return;
			}

			if(target[1]=="details"){
				$rootScope.hubExerciseList = false;
				$rootScope.hubExerciseDetails = true;	
				if(undefined!= target[2]){
					if($rootScope.ctoken == ""){
						$http(ctokenReq).then(function successCallback(response) {
							server.getHubExerciseDetails(target[2]);
						});
					}
					else{
						server.getHubExerciseDetails(target[2]);
					}
				}
			}
			else if("search"==target[1]){
				$rootScope.hubExerciseDetails = false;	
				$rootScope.hubExerciseList = true;

				if($rootScope.hubExercises.length==0){
					$http(ctokenReq).then(function successCallback(response) {
						$http(getExercisesReq).then(function successCallback(response) {
							$rootScope.hubExercises = response.data;
							$rootScope.showExercisesForTags(decodeURI(target[2]));
						})
					});
				}
				else{
					$rootScope.showExercisesForTags(decodeURI(target[2]));
				}

				$(window).scrollTop(0);
				return;
			}
			else if("vulnerability"==target[1]){
				$rootScope.hubExerciseDetails = false;	
				$rootScope.hubExerciseList = true;
				if(undefined!=target[2] && target[2].length>0){
					if($rootScope.hubExercises.length==0){
						$http(ctokenReq).then(function successCallback(response) {
							$http(getExercisesReq).then(function successCallback(response) {
								$rootScope.hubExercises = response.data;
								$rootScope.trainingExBrowse = "vulnerability";
								$rootScope.showExercisesForTechnology(decodeURI(target[2]))
							}, function errorCallback(response) {
								console.log('ajax error');
							});
						});
					}
					else{
						$rootScope.trainingExBrowse = "vulnerability";
						$rootScope.showExercisesForTechnology(decodeURI(target[2]))
					}
				}
				else{
					if($rootScope.hubExercises.length==0){
						$http(ctokenReq).then(function successCallback(response) {
							$http(getExercisesReq).then(function successCallback(response) {
								$rootScope.avExercisesList = false;
								$rootScope.avExercisesBrowse = false;
								$rootScope.avLearningList = false;
								$rootScope.avPathDetails = false;
								$rootScope.avExercisesTechList = true;
								$rootScope.trainingExBrowse = "vulnerability";;
								$rootScope.showTechnologies($rootScope.hubExercises);
							}, function errorCallback(response) {
								console.log('ajax error');
							});
						});
					}
					else{
						$rootScope.avExercisesList = false;
						$rootScope.avExercisesBrowse = false;
						$rootScope.avLearningList = false;
						$rootScope.avPathDetails = false;
						$rootScope.avExercisesTechList = true;
						$rootScope.trainingExBrowse = "vulnerability";;
						$rootScope.showTechnologies($rootScope.hubExercises);
					}
				}
				$(window).scrollTop(0);
				return;
			}
			else if("learning"==target[1]){
				$rootScope.hubExerciseDetails = false;	
				$rootScope.hubExerciseList = true;
				$rootScope.trainingExBrowse = "paths";
				if(undefined!=target[2] && target[2].length>0){
					if($rootScope.hubExercises.length == 0){
						$http(ctokenReq).then(function successCallback(response) {

							$http(getExercisesReq).then(function successCallback(response) {
								$rootScope.hubExercises = response.data;
								if(undefined!=target[3]){
									$http(getPathsReq).then(function successCallback(response) {
										$rootScope.hubLearningPaths = response.data;
										for(var i in $rootScope.hubLearningPaths){
											if($rootScope.hubLearningPaths.hasOwnProperty(i) && $rootScope.hubLearningPaths[i].uuid == target[3]){
												$rootScope.getPathDetails($rootScope.hubLearningPaths[i])
												break;
											}
										}
									});
								}
								else{
									$http(getPathsReq).then(function successCallback(response) {
										$rootScope.showExercisesForTechnology(decodeURI(target[2]))
									});
								}
							}, function errorCallback(response) {
								console.log('ajax error');
							});
						});

					}
					else{
						if(undefined!=target[3]){
							for(var i in $rootScope.hubLearningPaths){
								if($rootScope.hubLearningPaths.hasOwnProperty(i) && $rootScope.hubLearningPaths[i].uuid == target[3]){
									$rootScope.getPathDetails($rootScope.hubLearningPaths[i])
									break;
								}
							}
						}
						else{
							$rootScope.showExercisesForTechnology(decodeURI(target[2]))
						}
					}

				}
				else{
					if($rootScope.hubExercises.length == 0){
						$http(ctokenReq).then(function successCallback(response) {

							$http(getExercisesReq).then(function successCallback(response) {
								$rootScope.hubExercises = response.data;
								$http(getPathsReq).then(function successCallback(response) {
									$rootScope.hubLearningPaths = response.data;
									$rootScope.showAllTrainingPaths();
									$rootScope.avExercisesList = false;
									$rootScope.avExercisesBrowse = false;
									$rootScope.avLearningList = true;
									$rootScope.avPathDetails = false;
									$rootScope.avExercisesTechList = false;
									$rootScope.trainingExBrowse = "paths";
								});
							});
						});
					}
				}
				$(window).scrollTop(0);
				return;
			}

			break;
		default:{
			$rootScope.visibility.availableExercises = false;
			$rootScope.visibility.users = false;
			$rootScope.visibility.organizations = false;
			$rootScope.visibility.review = false;
			$rootScope.visibility.paths = false;
			$rootScope.visibility.hub = false;
			$rootScope.visibility.settings = false;
			$rootScope.visibility.exercises = false;
			$rootScope.visibility.kb = false;
			$rootScope.visibility.teams = false;
			$rootScope.visibility.challenges = false;
			$rootScope.visibility.gateways = false;
			$rootScope.visibility.running = false;
			$rootScope.visibility.stats = true;
			$(window).scrollTop(0);
			break;
		}

		}        

	});

	function getExDetails(uuid){

		var msg = {};
		msg.action = 'getUserCToken';

		var req = {
				method: 'POST',
				url: '/user/handler',
				data: msg,
		}
		$http(req).then(function successCallback(response) {
			$rootScope.ctoken = response.data.ctoken;
			server.getExerciseDetails(uuid);
			server.getRegionsForExercise(uuid);
		}, function errorCallback(response) {
			console.log('ajax error');
		});


	}

	$rootScope.visibility = {}
	$rootScope.visibility.availableExercises = false;
	$rootScope.visibility.users = false;
	$rootScope.visibility.exercises = false;
	$rootScope.visibility.organizations = false;
	$rootScope.visibility.review = false;
	$rootScope.visibility.settings = false;
	$rootScope.visibility.kb = false;
	$rootScope.visibility.teams = false;
	$rootScope.visibility.stats = true;

}])



sf.factory('xhrInterceptor', ['$q','$rootScope', function($q, $rootScope) {
	return {
		'request': function(config) {
			if((config.url == "/user/handler" || config.url == "/management/sfadmin/handler" ||config.url == "/management/stats/handler" ||config.url == "/management/admin/handler" ||config.url == "/management/team/handler") && config.data.action !=undefined && config.data.action != "getUserCToken")
				config.data.ctoken = $rootScope.ctoken;
			return config;
		},
		'response': function(response) {
			if(undefined!=response && undefined!=response.data){
				try{
					if(response.data.indexOf('loginButton')>0){
						document.location = "/index.html";
					}
				}catch(err){}
			}
			if(undefined!=response && undefined!=response.data && undefined!=response.data.errorMsg && response.data.errorMsg=="ChangePassword"){
				document.location = "/changePassword.html";
				return;
			}
			if(undefined!=response && undefined!=response.data && undefined!=response.data.errorMsg && response.data.errorMsg=="VerifyEmail"){
				document.location = "/validateEmail.html";
				return;
			}
			return response;
		}
	};
}]);
sf.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
	var original = $location.path;
	$location.path = function (path, reload) {
		if (reload === false) {
			var lastRoute = $route.current;
			var un = $rootScope.$on('$locationChangeSuccess', function () {
				$route.current = lastRoute;
				un();
			});
		}
		return original.apply($location, [path]);
	};
}])


sf.config(['$httpProvider', function($httpProvider) {  
	$httpProvider.interceptors.push('xhrInterceptor');
}]);

sf.controller('hub',['$scope','server','$rootScope','$location','$filter','notificationService',function($scope,server,$rootScope,$location,$filter,notificationService){

	$scope.selectedExercises = "";
	$scope.filteredAvailableExercisesList = [];
	$scope.user = server.user;
	$scope.masterAvailableExercisesList = [];
	$scope.availableRegions = [];
	$rootScope.hubExercises = [];
	$rootScope.hubLearningPaths = [];
	$rootScope.hubExerciseList = true;
	$rootScope.hubExerciseDetails = false;
	$scope.downloadQueueList = [];
	$rootScope.avExercisesTechList = true;
	$scope.definedGatewaysRegions = server.definedGatewaysRegions;
	$scope.hubExerciseInstallation = {};
	$scope.hubExerciseInstallation.deployToRegions = [];
	$scope.toggleDeployToRegionSelection = function(reg) {
		var idx = $scope.hubExerciseInstallation.deployToRegions.indexOf(reg);
		if (idx > -1) {
			$scope.hubExerciseInstallation.deployToRegions.splice(idx, 1);
		}
		else {
			$scope.hubExerciseInstallation.deployToRegions.push(reg);
		}
	};

	$scope.hubExerciseInstallation.deployForOrgs = [];
	$scope.toggleDeployForOrgsSelection = function(org) {
		var idx = $scope.hubExerciseInstallation.deployForOrgs.indexOf(org);
		if (idx > -1) {
			$scope.hubExerciseInstallation.deployForOrgs.splice(idx, 1);
		}
		else {
			$scope.hubExerciseInstallation.deployForOrgs.push(org);
		}
	};

	$scope.$on('organizations:updated', function(event,data) {
		$scope.definedOrgs = data;
	});
	$rootScope.downloadQueueUUIDList = [];
	$scope.$on('hubInstallationQueue:updated', function(event,data) {
		$scope.downloadQueueList = data;
		$rootScope.downloadQueueUUIDList = [];
		for(var i=0;i<$scope.downloadQueueList;i++){
			if(undefined != $scope.downloadQueueList[i].uuid)
				$rootScope.downloadQueueUUIDList.push($scope.downloadQueueList[i].uuid);
		}
		$scope.refreshing = false;
		if($scope.downloadQueueList.length>0)
			_st(reloadQueueAndAvailable,5000)
			else{
				server.getAvailableExercises();
				server.getPaths();
			}
	});
	function reloadQueueAndAvailable(){
		$scope.refreshing = true;
		server.getInstallationQueue();
	}


	$scope.openDownloadQueueModal = function(){
		$('#hubQueueModal').modal('show');
	};
	$scope.removeQueueItem = function(id){
		server.removeQueueItem(id);
	};
	$scope.restartQueueItem = function(id){
		server.restartQueueItem(id);
	};
	$scope.refreshing = false;
	$scope.$on('queueItemRemoved:updated', function(event,data) {
		server.getInstallationQueue();
	});
	$scope.$on('queueItemRestarted:updated', function(event,data) {
		server.getInstallationQueue();
	});

	$scope.updateDownloadQueue = function(){
		$scope.refreshing = true;
		server.getInstallationQueue();
	}

	$scope.getQueueStatus = function(status){

		switch(parseInt(status)){
		case 0:
			return "In Queue"
		case 1:
			return "Started"
		case 2:
			return "Image Pull Started"
		case 3:
			return "Image Pull Complete"
		case 4:
			return "Repo Created"
		case 5:
			return "Image Push Started"
		case 6:
			return "Image Push Complete"
		case 7:
			return "Image Removed"
		case 8:
			return "Task Definitions Created"
		case 9:
			return "Exercise Organizations setup"
		case 10:
			return "Complete"
		default:
			break;
		}

	}

	$scope.user = server.user;
	$scope.exercises = [];
	$scope.exerciseDetails = {};
	$scope.emptyExercises = true;
	$scope.assignedExercises = [];
	var exerciseTechnologies = [];
	$scope.currentSearch = "";
	$scope.currentTechnology = "";
	$rootScope.availableExercises = [];
	$rootScope.kbItems = [];

	var scoreClassMap = {	
			success: "success",
			average: "warning",
			failure: "danger",
			pending: "info",
			active: "active"

	};


	$rootScope.getIconForExercise = function(id){
		return 'glyphicon-minus'
	}

	$scope.updateHubExercise = function(){
		server.updateHubExercise($scope.exerciseDetails.uuid);
		$('#updateInstructions').modal('hide');
	}
	$scope.installHubExercises = function(){
		server.installHubExercises($scope.exerciseDetails.uuid,$scope.hubExerciseInstallation.deployForOrgs,$scope.hubExerciseInstallation.deployToRegions);
		$('#installationInstructions').modal('hide');
	}

	$scope.updateHubPath = function(){
		server.updateHubPath($scope.currentPath.uuid);
		$('#updatePathInstructions').modal('hide');
	}
	$scope.installHubPath = function(){
		server.installHubPath($scope.currentPath.uuid,$scope.hubExerciseInstallation.deployForOrgs,$scope.hubExerciseInstallation.deployToRegions);
		$('#installationPathInstructions').modal('hide');
	}

	$scope.$on('hubPathInstalled:updated', function(event,data) {
		if(undefined!=data.result && data.result=="error")
			return;
		server.getInstallationQueue();
		server.getPaths();
		server.getAvailableExercises();
		server.getAllStacks();
		server.getAllKbs();
		$location.path("exercise-hub/vulnerability", false);
	});
	$scope.$on('hubPathUpdated:updated', function(event,data) {
		if(undefined!=data.result && data.result=="error")
			return;
		server.getInstallationQueue();
		server.getPaths();
		server.getAvailableExercises();
		server.getAllStacks();
		server.getAllKbs();
		$location.path("exercise-hub/vulnerability", false);
	});

	$scope.$on('hubExerciseInstalled:updated', function(event,data) {
		if(undefined!=data.result && data.result=="error")
			return;
		server.getInstallationQueue();
		server.getAvailableExercises();
		server.getAllStacks();
		server.getAllKbs();
		$location.path("exercise-hub/vulnerability", false);
	});
	$scope.$on('hubExerciseUpdated:updated', function(event,data) {
		if(undefined!=data.result && data.result=="error")
			return;
		server.getAvailableExercises();
		server.getInstallationQueue();
		server.getAllStacks();
		server.getAllKbs();
		$location.path("available-exercises", false);
		$rootScope.showExerciseDetails = true;
		$rootScope.showExerciseList = false;
	});


	$scope.isPathUpdateAvailable = function(path){
		if(undefined==path || undefined==$rootScope.availablePathsList || $rootScope.availablePathsList.length == 0)
			return false;
		for(var i in $rootScope.availablePathsList){
			if($rootScope.availablePathsList.hasOwnProperty(i) && $rootScope.availablePathsList[i].uuid == path.uuid){
				try{
					if(undefined==$rootScope.availablePathsList[i].lastUpdate){
						return true;
					}
					if(moment.utc($rootScope.availablePathsList[i].lastUpdate.replace("[UTC]","")).format("X")<moment.utc(path.lastUpdate.replace("[UTC]","")).format("X")){
						return true;
					}
					return false;
				}catch(e){
					return false;
				}
			}
		}
	}






	$scope.isExerciseUpdateAvailable = function(exercise){
		if(undefined==exercise || undefined==$rootScope.availableExercises || $rootScope.availableExercises.length == 0)
			return false;
		for(var i in $rootScope.availableExercises){
			if($rootScope.availableExercises.hasOwnProperty(i) && $rootScope.availableExercises[i].uuid == exercise.uuid){
				try{
					if(undefined==$rootScope.availableExercises[i].lastUpdate || moment.utc($rootScope.availableExercises[i].lastUpdate.replace("[UTC]","")).format("X")<moment.utc(exercise.lastUpdate.replace("[UTC]","")).format("X")){
						return true;
					}
				}catch(e){
					try{
						if(undefined==$rootScope.availableExercises[i].lastUpdate || moment.utc($rootScope.availableExercises[i].lastUpdate).format("X")<moment.utc(exercise.lastUpdate).format("X")){
							return true;
						}
					}catch(e2){}
				}
				try{
					if(moment.utc($rootScope.availableExercises[i].image.updateDate.replace("[UTC]","")).format("X")<moment.utc(exercise.image.updateDate.replace("[UTC]","")).format("X")){
						return true;
					}
				}catch(e){
					try{
						if(moment.utc($rootScope.availableExercises[i].image.updateDate).format("X")<moment.utc(exercise.image.updateDate).format("X")){
							return true;
						}
					}catch(e2){}
				}
				return false;
			}
		}
		return false;
	}


	$scope.isInstalled = function(uuid){
		for(var i=0;i<$rootScope.availableExercises.length;i++){
			if($rootScope.availableExercises[i].uuid == uuid)
				return true;
		}
		return false;
	}

	$scope.isPathInstalled = function(uuid){
		for(var i=0;i<$rootScope.availablePathsList.length;i++){
			if($rootScope.availablePathsList[i].uuid == uuid)
				return true;
		}
		return false;
	}


	$rootScope.getColorForScore = function(score){
		if(score<=20)
			return "rgba(118, 147, 193, 0.94)";
		if(score<=50)
			return "rgba(158, 74, 114, 0.87)";
		if(score<=75)
			return "rgba(118, 118, 193, 0.94)";
		if(score<=100)
			return "rgba(106, 106, 125, 0.87)";
		if(score<=125)
			return "rgba(56, 31, 8, 0.87)";
		return "rgba(189, 108, 34, 0.87)";
	}

	$scope.openInstallationInstructionsModal = function(){
		$('#installationInstructions').modal('show');
	}
	$rootScope.openUpdateInstructionsModal = function(){
		$('#updateInstructions').modal('show');
	}

	$scope.openPathInstallationInstructionsModal = function(){
		$('#installationPathInstructions').modal('show');
	}
	$scope.openPathUpdateInstructionsModal = function(){
		$('#updatePathInstructions').modal('show');
	}

	$scope.animateElementIn = function($el) {
		$el.removeClass('timeline-hidden');
		$el.addClass('bounce-in');
	};

	$scope.animateElementOut = function($el) {
		$el.addClass('timeline-hidden');
		$el.removeClass('bounce-in');
	};

	$scope.userAssignedExercises = [];

	Array.prototype.uniqueId = function() {
		var a = this.concat();
		for(var i=0; i<a.length; ++i) {
			for(var j=i+1; j<a.length; ++j) {
				if(a[i].exercise.id === a[j].exercise.id)
					a.splice(j--, 1);
			}
		}

		return a;
	};



	$scope.getExerciseStatusString = function(status){
		switch(status){
		case "0":
			return "Available"
		case "1":
			return "Updated"
		case "2":
			return "Coming Soon"
		case "3":
			return "Not Available"
		case "4":
			return "Available"
		default:{
			var ret = status.replace("_"," ");
			return ret.charAt(0).toUpperCase() + ret.substr(1).toLowerCase();
		};
		}
	}
	$scope.exercisesTech = [];
	$rootScope.showTechnologies = function(data){
		exerciseTechnologies = [];
		$scope.exercisesTech = [];
		for(var i in data){
			if(data.hasOwnProperty(i) && undefined!=data[i].technology){
				if(exerciseTechnologies.indexOf(data[i].technology)<0)
					exerciseTechnologies.push(data[i].technology)
			}
		}

		var size = 3;
		while (exerciseTechnologies.length > 0){
			$scope.exercisesTech.push(exerciseTechnologies.splice(0, size));
		}

		if($scope.exercisesTech.length<=0){
			$scope.emptyExercises = true;
		}
		else{
			$scope.emptyExercises = false;
		}
	};

	$scope.backToTechnologies = function(){
		$scope.currentTechnology = "";
		$scope.currentSearch = "";
		$rootScope.avExercisesList = false;
		$rootScope.avLearningList = false;
		$rootScope.avPathDetails = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avExercisesTechList = true;
		$location.path("exercise-hub/vulnerability", false);
		$rootScope.showTechnologies($rootScope.hubExercises);
	}
	$rootScope.trainingExBrowse = "";
	$rootScope.trainingExTech = "";

	$scope.getBrowseName = function(){
		if($rootScope.trainingExBrowse == "vulnerability")
			return "Vulnerability";
		else
			return "Learning Path";
	}
	$scope.getBrowseImage = function(){
		if($rootScope.trainingExBrowse == "vulnerability")
			return "/assets/img/vulnerability.svg";
		else 
			return "/assets/img/learning.svg";
	}


	$rootScope.browseVulnerabilities = function(){
		$rootScope.showTechnologies($rootScope.hubExercises);
		$rootScope.avExercisesList = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avLearningList = false;
		$rootScope.avPathDetails = false;
		$rootScope.avExercisesTechList = true;
		$rootScope.trainingExBrowse = "vulnerability";
		$location.path("exercise-hub/vulnerability", false);
	}

	$rootScope.browseLearningPaths = function(){
		$rootScope.showAllTrainingPaths();
		$rootScope.avExercisesList = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avLearningList = true;
		$rootScope.avPathDetails = false;
		$rootScope.avExercisesTechList = false;
		$rootScope.trainingExBrowse = "paths";
		$location.path("exercise-hub/learning", false);
	}
	$rootScope.learningPathStatus = [{"id":"0","name":"Available"},{"id":"1","name":"Draft"},{"id":"2","name":"Inactive"},{"id":"3","name":"Deprecated"},{"id":"4","name":"Coming Soon"}]
	var difficultyComparator = function(o1,o2){
		if(undefined==o1 || undefined==o2 || undefined==o1.difficulty || undefined==o2.difficulty)
			return 0;
		const v1 = o1.difficulty.toLowerCase();
		const v2 = o2.difficulty.toLowerCase();
		if(v1==2)
			return 0;
		if(v1=="easy")
			return -1;
		if(v1=="hard")
			return 1;
		if(v1=="moderate" && v2 == "easy")
			return 1;
		if(v1=="moderate" && v2 == "hard")
			return -1;

	}

	$scope.getImageForDifficulty = function(difficulty){
		if(undefined==difficulty || null == difficulty)
			return "";
		difficulty = difficulty.toLowerCase();
		switch(difficulty){
		case "easy":
			return "/assets/img/easy.png";
		case "moderate":
			return "/assets/img/moderate.png";
		case "hard":
			return "/assets/img/hard.png";
		default:
			return "";
		}
	}
	$rootScope.currentPath = {};
	$rootScope.getPathDetails = function(path){

		for(var i=0;i<path.exercises.length;i++){
			if(undefined==path.exercises[i].id){
				for(var j=0;j<$rootScope.hubExercises.length;j++){
					if(undefined!=$rootScope.hubExercises[j] && $rootScope.hubExercises[j].uuid == path.exercises[i]){
						path.exercises[i] = $rootScope.hubExercises[j];
						break;
					}
				}
			}
		}
		$rootScope.currentPath = path;
		$rootScope.avExercisesTechList = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avExercisesList = false;
		$rootScope.avLearningList = false;
		$rootScope.avPathDetails = true;
	}

	$rootScope.hubLearningPaths = [];
	$scope.$on('hubPaths:updated', function(event,data) {
		$rootScope.hubLearningPaths = data;	
		if($rootScope.trainingExBrowse == 'paths'){
			$rootScope.showAllTrainingPaths();
		}

	});

	$rootScope.showAllTrainingPaths = function(){
		$scope.currentTechnology = "";
		$scope.exercises = [];
		$scope.paths = [];
		var tmpExercises = [];
		var tmpPaths = [];
		for(var i in $rootScope.hubLearningPaths){
			if($rootScope.hubLearningPaths.hasOwnProperty(i)){
				tmpPaths.push($rootScope.hubLearningPaths[i])
			}
		}
		var size = 3;
		tmpPaths.sort(difficultyComparator);
		while (tmpPaths.length > 0){
			$scope.paths.push(tmpPaths.splice(0, size));
			$scope.emptyExercises = false;
		}
		if($scope.paths.length<=0){
			$scope.emptyExercises = true;
		}
		$rootScope.avExercisesList = false;
		$rootScope.avLearningList = true;
	}
	$rootScope.backToTechnology = function(tech){
		if (history.length > 2) {
			// if history is not empty, go back:
			window.history.back();
		} else {
			// show other exercises in same tech:
			$rootScope.viewExercisesForTech(tech);
		}
	}

	$rootScope.viewExercisesForTech = function(tech){
		$location.path("exercise-hub/vulnerability/"+tech, false);
	}


	$rootScope.showExercisesForTechnology = function(tech){
		$scope.currentTechnology = tech;
		$scope.exercises = [];
		$scope.paths = [];

		var tmpExercises = [];
		var tmpPaths = [];

		$rootScope.avExercisesTechList = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avPathDetails = false;

		if($rootScope.trainingExBrowse == "vulnerability"){
			for(var i in $rootScope.hubExercises){
				if($rootScope.hubExercises.hasOwnProperty(i)){
					if($rootScope.hubExercises[i].technology==tech)
						tmpExercises.push($rootScope.hubExercises[i])
				}
			}
			var size = 3;
			while (tmpExercises.length > 0){
				$scope.exercises.push(tmpExercises.splice(0, size));
				$scope.emptyExercises = false;
			}
			if($scope.exercises.length<=0){
				$scope.emptyExercises = true;
			}
			$rootScope.avLearningList = false;
			$rootScope.avExercisesList = true;
		}
		else if($rootScope.trainingExBrowse == "paths"){
			for(var i in $rootScope.hubLearningPaths){
				if($rootScope.hubLearningPaths.hasOwnProperty(i)){
					if($rootScope.hubLearningPaths[i].technology==tech)
						tmpPaths.push($rootScope.hubLearningPaths[i])
				}
			}
			var size = 2;
			tmpPaths.sort(difficultyComparator);
			while (tmpPaths.length > 0){
				$scope.paths.push(tmpPaths.splice(0, size));
				$scope.emptyExercises = false;
			}
			if($scope.paths.length<=0){
				$scope.emptyExercises = true;
			}
			$rootScope.avExercisesList = false;
			$rootScope.avLearningList = true;
		}

	} 

	$scope.searchTags = function(){
		$location.path("exercise-hub/search/"+$scope.currentSearch, false);
	}

	$rootScope.showExercisesForTags = function(search){
		if(search.length!=0 && search.length<3){
			return;
		}
		var tmpExercises = [];
		$scope.exercises = [];
		if(search.length==0){
			for(var n in $rootScope.hubExercises){
				if($rootScope.hubExercises.hasOwnProperty(n)){
					if($scope.currentTechnology!=""){
						if($rootScope.hubExercises[n].technology==$scope.currentTechnology)
							tmpExercises.push($rootScope.hubExercises[n]);
					}
					else{
						tmpExercises.push($rootScope.hubExercises[n]);
					}
				}
			}
		}
		else{
			for(var i in $rootScope.hubExercises){
				var added = false;
				if($rootScope.hubExercises.hasOwnProperty(i)){
					if($scope.currentTechnology!="" && ($rootScope.hubExercises[i].technology!=$scope.currentTechnology))
						continue;
					if(undefined!=$rootScope.hubExercises[i].tags){
						for(var j in $rootScope.hubExercises[i].tags){
							if($rootScope.hubExercises[i].tags.hasOwnProperty(j) && $rootScope.hubExercises[i].tags[j].toLowerCase().indexOf(search.toLowerCase())>=0){
								tmpExercises.push($rootScope.hubExercises[i]);
								added = true;
							}
						}
					} 
					if(!added && undefined!=$rootScope.hubExercises[i].title && $rootScope.hubExercises[i].title.toLowerCase().indexOf(search.toLowerCase())>=0){
						tmpExercises.push($rootScope.hubExercises[i]);
						added = true;
					}
					if(!added && undefined!=$rootScope.hubExercises[i].subtitle && $rootScope.hubExercises[i].subtitle.toLowerCase().indexOf(search.toLowerCase())>=0){
						tmpExercises.push($rootScope.hubExercises[i]);
						added = true;
					}
					if(!added && undefined!=$rootScope.hubExercises[i].description && $rootScope.hubExercises[i].description.toLowerCase().indexOf(search.toLowerCase())>=0){
						tmpExercises.push($rootScope.hubExercises[i]);
						added = true;
					}
				}
			}
		}
		for(var j=0;j<tmpExercises.length;j++){
			if(!tmpExercises[j].id){
				try{
					tmpExercises.remove(j,j);
					j--;
				}catch(err){
					continue;
				}
			}
		}
		var size = 3;
		while (tmpExercises.length > 0){
			$scope.exercises.push(tmpExercises.splice(0, size));
			$scope.emptyExercises = false;
		}
		if($scope.exercises.length<=0){
			$scope.emptyExercises = true;
		}
		$rootScope.avExercisesTechList = false;
		$rootScope.avExercisesBrowse = false;
		$rootScope.avPathDetails = false;
		$rootScope.avLearningList = false;
		$rootScope.avExercisesList = true;
		$scope.currentSearch = search;
		setTimeout(function(){document.getElementById('avExercisesListSearch').focus();},300);

	}



	$scope.$on('hubExercises:updated', function(event,data) {
		if(undefined!=data){
			$rootScope.hubExercises = data;
			if($rootScope.trainingExBrowse == 'vulnerability'){
				$rootScope.showTechnologies($rootScope.hubExercises);
			}
			$scope.hubNotAvailable = false;
		}
		else{
			$scope.hubNotAvailable = true;
		}


	});

	function isKbPresent(kb){
		if(undefined==kb)
			return false;
		for(var j=0; j<$scope.exerciseKbs.length; j++){
			if($scope.exerciseKbs[j].uuid==kb.uuid)
				return true;
		}
		return false;
	}

	$scope.$on('hubVulnKBLoaded:updated', function(event,data) {
		document.querySelector('#hubKbMD').innerHTML = '';
		var cMd = data.md.text+"\n";
		var technology = $rootScope.hubExerciseDetails.technology;
		if(data.kbMapping!=undefined && data.kbMapping[technology]!=undefined){
			var tmpInner = data.kbMapping[technology];
			cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
		}
		var editor = new toastui.Editor.factory({
			el: document.querySelector('#hubKbMD'),
			viewer: true,
			height: '500px',
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			usageStatistics:false,
			initialValue: ''
		});
		editor.setMarkdown(cMd);
		$('#hubKbModal').modal('show');
		$('#hubKbMD').find('a').each(function() {
			if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
				return;
			var a = new RegExp('/' + window.location.host + '/');
			if(!a.test(this.href)) {
				$(this).click(function(event) {
					event.preventDefault();
					event.stopPropagation();
					window.open(this.href, '_blank');
				});
			}
		});
		autoplayMdVideo('#hubKbMD');
		var kbFound = false;
		for(var j=0; j<$rootScope.kbItems.length; j++){
			if($rootScope.kbItems[j].uuid == data.uuid){
				$rootScope.kbItems[j] = data;
				kbFound = true;
				break;
			}
		}
		if(!kbFound){
			$rootScope.kbItems.push(data)
		}

	})
	$rootScope.openHubVulnKB = function(item,technology){
		if(item!=undefined && item.uuid!=undefined && item.uuid!=null & item.uuid!="" && undefined!=technology){
			for(var j=0; j<$rootScope.kbItems.length; j++){
				if($rootScope.kbItems[j].uuid == item.uuid && undefined != $rootScope.kbItems[j].md.text && $rootScope.kbItems[j].md.text != ""){
					document.querySelector('#hubKbMD').innerHTML = '';
					var cMd = $rootScope.kbItems[j].md.text+"\n";
					if($rootScope.kbItems[j].kbMapping!=undefined && $rootScope.kbItems[j].kbMapping[technology]!=undefined){
						var tmpInner = $rootScope.kbItems[j].kbMapping[technology];
						cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
					}
					else if(item.isAgnostic){
						server.loadHubVulnKB(item.uuid,technology)
						return;
					}
					var editor = new toastui.Editor.factory({
						el: document.querySelector('#hubKbMD'),
						viewer: true,
						usageStatistics:false,
						height: '500px',
						plugins: [ codeSyntaxHightlight, colorSyntax ],
						initialValue: ''
					});
					editor.setMarkdown(cMd);
					$('#hubKbModal').modal('show');
					$('#hubKbMD').find('a').each(function() {
						if(this.href==undefined || this.href=="" || this.href.indexOf('#')==0)
							return;
						var a = new RegExp('/' + window.location.host + '/');
						if(!a.test(this.href)) {
							$(this).click(function(event) {
								event.preventDefault();
								event.stopPropagation();
								window.open(this.href, '_blank');
							});
						}
					});
					autoplayMdVideo('#hubKbMD');
					return;
				}
			}
			server.loadHubVulnKB(item.uuid,technology);
		}
	}
	$rootScope.hubExerciseDetails = {};
	$scope.$on('hubExerciseDetails:updated', function(event,data) {
		$scope.exerciseDetails = data;
		$scope.exerciseKbs = []; 
		$rootScope.hubExerciseDetails = data;
		for(var i=0; i<$scope.exerciseDetails.flags.length; i++){
			if(undefined!=$scope.exerciseDetails.flags[i].kb && !isKbPresent($scope.exerciseDetails.flags[i].kb))
				$scope.exerciseKbs.push($scope.exerciseDetails.flags[i].kb);
		}	
		$location.path("exercise-hub/details/"+$scope.exerciseDetails.uuid, false);
		$(window).scrollTop(0);
	});


	var technologyClassMap = {
			"NodeJS": "nodejsb",
			"Java": "javab",
			"Android": "androidb",
			"Ruby": "rubyb",
			"Python": "pythonb",
			"Go Lang":"golangb",
			"PHP":"phpb",
			".NET":"dotnetb",
			"Solidity (Ethereum)":"solidityb",
			"Agnostic":"agnosticb",
			"Kubernetes":"kubernetesb",
			"AWS":"awsb",
			"Docker":"dockerb",
			"Scala":"scalab",
			"Threat Hunting":"siemb",
			"Server Hardening":"serverhardeningb"
	};
	var pictForTechnology = {
			"NodeJS": "/assets/img/nodejs.png",
			"Java": "/assets/img/java.png",
			"Android": "/assets/img/android.png",
			"Ruby": "/assets/img/ruby.png",
			"Python": "/assets/img/python.png",
			"Go Lang":"/assets/img/golang.png",
			"PHP":"/assets/img/php.png",
			".NET":"/assets/img/dotnet.png",
			"Solidity (Ethereum)":"/assets/img/solidity.png",
			"Kubernetes":"/assets/img/k8s.png",
			"AWS":"/assets/img/aws.png",
			"Docker":"/assets/img/docker.png",
			"Scala":"/assets/img/scala.png",
			"Threat Hunting":"/assets/img/siem.png",
			"Server Hardening":"/assets/img/serverhardening.png"
	};

	var remediationClassMap = {
			false: "table-success",
			true: "table-danger"
	}



	$scope.getPictureForTechnology = function(technology){
		return pictForTechnology[technology]
	}
	$scope.getRemedationTableClass = function(status) {
		return remediationClassMap[status]
	};

	$rootScope.getExerciseClass = function(technology) {
		return technologyClassMap[technology];
	};

	$scope.getFormattedEnd = function(date){
		var out = moment(date).format("dddd, MMMM Do YYYY");
		return out;
	};

	$scope.getHubExerciseDetails = function(uuid){
		$location.path("exercise-hub/details/"+uuid, false);
	}

}]);
sf.controller('welcome',['$scope','server',function($scope,server){
	$scope.user = server.user;
}])
sf.controller('home',['$scope','server',function($scope,server){
	$scope.user = server.user;
}])
sf.controller('organizations',['$scope','server','$filter','$rootScope',function($scope,server,$filter,$rootScope){

	$scope.masterOrganizations = [];
	$scope.filteredOrganizationsList = []; 
	$scope.selectedOrganizationRow = -1;
	$scope.organizationstableconfig = {
			itemsPerPage: 10,
			fillLastPage: false
	}
	$scope.saveFlow = false;
	$scope.organizationStatusList = ["Active","Trial","Inactive"];
	
	$scope.getOrgStatusFromCode = function(code){
		if(!Number.isInteger(parseInt(code)))
			return;
		switch(parseInt(code)){
			case 0:
				return "Trial";
			case 1:
				return "Active";
			case 2:
				return "Inactive";
			default:
				return "";
			}
	}
	
	$rootScope.getOrgStatusCodeFromText = function(text){
		if(text!=undefined && text!="")
			text = text.toLowerCase();
		else
			return "";
		switch(text){
			case 'trial':
				return 0;
			case 'active':
				return 1;
			case 'inactive':
				return 2;
			}
	}

	$scope.newCodeMaxReedeems = '';
	$scope.invitationCodes = [];

	$scope.generateInvitationCode = function(challengeId){
		var obj = {};
		obj.orgId = $scope.selectedOrg.id;
		obj.maxUsers = $scope.newCodeMaxReedeems;
		if(undefined!=$scope.challengeId && null!=$scope.challengeId && ""!=$scope.challengeId)
			obj.challengeId = $scope.challengeId;
		server.generateInvitationCode(obj);
	}
	$scope.$on('invitationGenerated:updated', function(event,data) {
		$scope.newCodeMaxReedeems = ''
			server.getInvitationCodes($scope.selectedOrg.id);
	});
	$scope.removeInvitationCode = function(code){
		var obj = {};
		obj.orgId = $scope.selectedOrg.id;
		obj.orgCode = code;
		server.removeInvitationCode(obj);
	}

	$scope.$on('invitationRemoved:updated', function(event,data) {
		server.getInvitationCodes($scope.selectedOrg.id);
	});

	$scope.$on('invitationCodes:updated', function(event,data) {
		$scope.invitationCodes = data;
	});


	$scope.getReviewedDate = function(date){
		if(null!=date && undefined != date)
			return moment(date).local().format("MMM D, YYYY")
			else
				return "N/A";
	}
	$scope.updateFilteredList = function() {
		$scope.filteredOrganizationsList = $filter("filter")($scope.masterOrganizations, $scope.query);
	};

	$scope.$on('organizations:updated', function(event,data) {
		$scope.masterOrganizations = data;
		$scope.filteredOrganizationsList = $scope.masterOrganizations;
	});

	$scope.isOrgNameAvailable = function(){
		if($scope.newOrg.name != "" ){
			if($scope.saveFlow){
				if($scope.newOrg.name!=$scope.editingOrg.name){
					server.isOrgNameAvailable($scope.newOrg.name);
				}					
			}
			else{
				server.isOrgNameAvailable($scope.newOrg.name);
			}
		}
	}
	$scope.$on('orgNameAvailable:updated', function(event,data) {
		$scope.orgNameAvailable = data.result;
	});
	$scope.orgNameAvailable = true;
	$scope.newOrg = {};
	$scope.newOrg.name = "";
	$scope.newOrg.email = "";
	$scope.newOrg.status = 1;
	$scope.newOrg.defaultCredits = -1;
	$scope.newOrg.allowManualReview = true;
	
	$scope.newOrg.maxUsers = 100;
	$scope.organizationDetails = false;

	$scope.showOrganizationDetails = function(org){
		$scope.selectedOrg = org;
		server.getInvitationCodes($scope.selectedOrg.id);
		$scope.organizationDetails = true;
	}

	$scope.backToOrgList = function(){
		$scope.organizationDetails = false;
		$scope.selectedOrg = "";
	}

	$scope.addOrganizationModal= function(){
		$scope.saveFlow = false;
		$('#addOrgModal').modal('show');
	}

	$scope.updateOrganizationModal= function(item){
		$scope.saveFlow = true;
		$scope.editingOrg = item;
		$scope.orgId = item.id;
		$scope.newOrg.name = item.name;
		$scope.newOrg.email = item.email;
		$scope.newOrg.defaultCredits = item.defaultCredits;
		$scope.newOrg.status = $scope.getOrgStatusFromCode(item.status);
		$scope.newOrg.maxUsers = item.maxUsers;
		$scope.newOrg.allowManualReview = item.allowManualReview;
		$('#addOrgModal').modal('show');
	}


	var tmpOrgToBeRemoved = -1;

	$scope.removeOrganization = function(){
		$('#removeOrganizationModal').modal('hide');
		server.removeOrganization(tmpOrgToBeRemoved);
		tmpOrgToBeRemoved = -1;

	}
	$scope.removeOrganizationModal = function(id,name){
		$scope.tmpOrgToBeRemovedName = name;
		tmpOrgToBeRemoved = id;
		$('#removeOrganizationModal').modal('show');
	}

	$scope.addOrganization = function(){
		var obj = {};
		obj.name = $scope.newOrg.name;
		obj.email = $scope.newOrg.email;
		obj.maxUsers = $scope.newOrg.maxUsers;
		obj.allowManualReview = $scope.newOrg.allowManualReview;
		obj.credits = $scope.newOrg.defaultCredits;
		obj.status = $scope.newOrg.status;
		server.addOrganization(obj);
		$('#addOrgModal').modal('hide');

		$scope.newOrg.name = "";
		$scope.newOrg.email = "";
		$scope.newOrg.allowManualReview = true;
		$scope.newOrg.defaultCredits = -1;
		$scope.newOrg.status = 'Active';
		$scope.newOrg.maxUsers = 100;
	}

	$scope.updateOrganization = function(){
		var obj = {};
		obj.id = $scope.orgId;
		obj.name = $scope.newOrg.name;
		obj.email = $scope.newOrg.email;
		obj.maxUsers = $scope.newOrg.maxUsers;
		obj.allowManualReview = $scope.newOrg.allowManualReview;
		obj.credits = $scope.newOrg.defaultCredits;
		obj.status = $scope.newOrg.status;
		server.updateOrganization(obj);

		$('#addOrgModal').modal('hide');

		$scope.newOrg.name = "";
		$scope.newOrg.email = "";
		$scope.newOrg.allowManualReview = true;
		$scope.newOrg.credits = -1;
		$scope.newOrg.status = 1;
		$scope.newOrg.maxUsers = 100;
	}
	$scope.$on('organizationRemoved:updated', function(event,data) {
		if(data.result=="success"){
			server.getOrganizations();
			server.getUserProfileInfo();
		}
	});
	$scope.$on('organizationAdded:updated', function(event,data) {
		if(data.result=="success"){
			server.getOrganizations();
			server.getUserProfileInfo();
		}
	});
	$scope.$on('organizationUpdated:updated', function(event,data) {
		if(data.result=="success"){
			server.getOrganizations();
			server.getUserProfileInfo();
		}
	});
}])

sf.controller('paths',['$scope','server','$rootScope','$location','$filter','$interval',function($scope,server,$rootScope,$location,$filter,$interval){

	$scope.selectedPath = "";
	$scope.user = server.user;
	$scope.filteredPathsList = [];
	$scope.masterPathsList = [];
	$scope.pathNameAvailable = true;
	$scope.selectedExerciseList = [];
	$scope.selectedUsersList = [];

	$scope.tmpNewPath = {};
	$scope.tmpNewPath.name = "";
	$scope.tmpNewPath.details = "";
	$scope.tmpNewPath.technology = "";
	$scope.tmpNewPath.difficulty = "";
	$scope.tmpNewPath.tags = [];
	$scope.tmpNewPath.refresherPercentage = "";
	$scope.tmpNewPath.allowRenewal = true;
	$scope.tmpNewPath.monthsExpiration = "";

	$rootScope.certificationMonthDurations = ["6","12","18","24"];
	$rootScope.renewalRefresherPercentages = ["30","50","70"];
	$rootScope.learningPathStatus = [{"id":"0","name":"Available"},{"id":"1","name":"Draft"},{"id":"2","name":"Inactive"},{"id":"3","name":"Deprecated"},{"id":"4","name":"Coming Soon"}]


	$scope.isPathUpdateAvailable = function(path){
		if(undefined==path || undefined==$rootScope.hubLearningPaths  || $rootScope.hubLearningPaths .length == 0)
			return false;
		for(var i in $rootScope.hubLearningPaths){
			if($rootScope.hubLearningPaths.hasOwnProperty(i) && $rootScope.hubLearningPaths[i].uuid == path.uuid){
				try{
					if(undefined==path.lastUpdate){
						return true;
					}
					if(moment.utc(path.lastUpdate.replace("[UTC]","")).format("X")<moment.utc($rootScope.hubLearningPaths[i].lastUpdate.replace("[UTC]","")).format("X")){
						return true;
					}
					return false;
				}catch(e){
					return false;
				}
			}
		}
	}

	$scope.getRefresher = function(item){
		if(item.allowRenewal == true)
			return item.refresherPercentage + "%";
		else
			return "N/A";
	}

	$scope.getStatusName = function(id){
		for(var i in $rootScope.learningPathStatus){
			if(undefined!=$rootScope.learningPathStatus[i]["id"] && $rootScope.learningPathStatus[i]["id"] == id){
				return $rootScope.learningPathStatus[i]["name"];
			}
		}
	}

	$scope.getStatusObj = function(id){
		for(var i in $rootScope.learningPathStatus){
			if(undefined!=$rootScope.learningPathStatus[i]["id"] && $rootScope.learningPathStatus[i]["id"] == id){
				return $rootScope.learningPathStatus[i];
			}
		}
	}


	var tmpPathToRemove = -1;
	$scope.tmpPatheToBeRemovedName = "";
	$scope.removePathModal = function(id,name){
		tmpPathToRemove = id;
		$scope.tmpPathToBeRemovedName = name
		$('#removePathModal').modal('show');
	}
	$scope.removePath = function(){
		server.removePath(tmpPathToRemove);
		tmpPathToRemove = -1;
		$('#removePathModal').modal('hide');
	}

	$scope.usersInSelectedOrg = [];
	$scope.tmpNewChallenge = {};
	$scope.saveFlow = false;

	$scope.filteredAvailableExercisesList = [];
	$scope.masterAvailableExercisesList = [];
	$scope.exercisesForOrgs = [];

	$scope.getExerciseStatusString = function(status){
		switch(status){
		case "0":
			return "Available";
			break;
		case "2":
			return "Coming Soon";
			break;
		case "3":
			return "Inactive";
			break;
		default:
			return "N/A";
		}
	}

	$scope.updateAvailableExercisesFilteredList = function() {
		$scope.filteredAvailableExercisesList = $filter("filter")($scope.masterAvailableExercisesList, $scope.queryAvailableExercises);
	};
	$scope.availableExercisestableconfig = {
			itemsPerPage: 8,
			fillLastPage: false
	}
	$scope.$on('availableExercises:updated', function(event,data) {
		$scope.masterAvailableExercisesList = data.exercises;
		$scope.exercisesForOrgs = data.orgs;

		$scope.selectedExerciseList = [];

		if($scope.tmpNewPath.technology != "")
			$scope.filteredAvailableExercisesList = $filter("filter")($scope.masterAvailableExercisesList,{ technology:$scope.tmpNewPath.technology});
	});

	$scope.updatePathDefaultStack = function(){
		if($scope.tmpNewPath.technology != "Agnostic")
			$scope.filteredAvailableExercisesList = $filter("filter")($scope.masterAvailableExercisesList,{ technology:$scope.tmpNewPath.technology});
		else
			$scope.filteredAvailableExercisesList = $filter("filter")($scope.masterAvailableExercisesList);
		$scope.selectedExerciseList = [];
		$scope.isPathNameAvailable();
	}

	$scope.getExerciseTitleFromUUID = function(uuid){
		for(var i in $scope.masterAvailableExercisesList){
			if( $scope.masterAvailableExercisesList[i]["uuid"] == uuid){
				return $scope.masterAvailableExercisesList[i].title
			}
		}
		return "Exercise "+uuid;
	}

	$scope.orgCheckToggle=function(s){
		if(s.isChecked === true){
			s.isChecked === false;
		}else{
			s.isChecked === true;
		}
		for(var i in $scope.selectedOrgsList){
			if(undefined!= $scope.selectedOrgsList[i].id && $scope.selectedOrgsList[i].id == s.id){
				$scope.selectedOrgsList.remove(i,i);
				return;
			}
		}
		$scope.selectedOrgsList.push(s);
	}

	$scope.exerciseCheckToggle=function(s){
		if(s.isChecked === true){
			s.isChecked === false;
		}else{
			s.isChecked === true;
		}
		if($scope.selectedExerciseList.indexOf(s.uuid)<0){
			$scope.selectedExerciseList.push(s.uuid);
		}
		else{
			var idx = $scope.selectedExerciseList.indexOf(s.uuid);
			$scope.selectedExerciseList.remove(idx,idx);
		}
	}

	$scope.clear = function(){
		$scope.tmpNewPath = {};

		$scope.tmpNewPath = {};
		$scope.tmpNewPath.name = "";
		$scope.tmpNewPath.details = "";
		$scope.tmpNewPath.technology = "";
		$scope.tmpNewPath.difficulty = "";
		$scope.tmpNewPath.tags = [];
		$scope.tmpNewPath.refresherPercentage = "";
		$scope.tmpNewPath.allowRenewal = true;
		$scope.tmpNewPath.monthsExpiration = "";
		$scope.tmpNewPath.orgs = [];
		$scope.tmpNewPath.exercises = [];
		$scope.tmpNewPath.status = "";
		$scope.tmpNewPath.fromHub = false;


		for(var q in $scope.masterAvailableExercisesList){
			if($scope.masterAvailableExercisesList[q]['isChecked']!=undefined)
				$scope.masterAvailableExercisesList[q]['isChecked'] = false;
		}
		for(var p in $scope.masterOrgsList){
			if($scope.masterOrgsList[p]['isChecked']!=undefined)
				$scope.masterOrgsList[p]['isChecked'] = false;
		}
		$scope.filteredAvailableExercisesList = [];
		$scope.filteredOrgsList = $scope.masterOrgsList;

		$scope.selectedOrgsList = [];
		$scope.selectedExerciseList = [];
	}

	$scope.addNewPath = function(){
		$('#addNewPathModal').modal('hide');
		var obj = cloneObj($scope.tmpNewPath)
		obj.orgs = $scope.selectedOrgsList;
		obj.exercises = $scope.selectedExerciseList;
		for(var i=0;i<obj.orgs.length;i++){
			if (typeof obj.orgs[i] === 'string' || obj.orgs[i] instanceof String){
				continue;
			}
			else{
				obj.orgs[i] = obj.orgs[i].id;
			}
		}
		for(var i=0;i<obj.tags.length;i++){
			if (typeof obj.tags[i] === 'string' || obj.tags[i] instanceof String){
				continue;
			}
			else{
				obj.tags[i] = obj.tags[i].text;
			}
		}
		if (typeof obj.status !== 'string' && typeof obj.status !== 'number'){
			obj.status = obj.status.id;
		}
		server.addPath(obj);
	}


	$scope.$on('addPath:updated', function(event,data) {
		server.getPaths();
		$scope.clear();
		$scope.saveFlow = false;
	})
	$scope.$on('removePath:updated', function(event,data) {
		server.getPaths();
		$scope.saveFlow = false;
	})
	$scope.$on('updatePath:updated', function(event,data) {
		server.getPaths();
		$scope.clear();
		$scope.saveFlow = false;
	})

	$scope.updatePathModal = function(item){
		$scope.saveFlow = true;
		$scope.tmpNewPath = {};
		$scope.tmpNewPath.fromHub = item.fromHub;
		if(undefined == $scope.tmpNewPath.fromHub)
			$scope.tmpNewPath.fromHub = false;
		$scope.tmpNewPath.id = item.id;
		$scope.tmpNewPath.name = item.name;
		$scope.tmpNewPath.details = item.details;

		$scope.tmpNewPath.technology = item.technology;
		$scope.tmpNewPath.difficulty = item.difficulty;
		$scope.tmpNewPath.tags = item.tags;
		$scope.tmpNewPath.allowRenewal = item.allowRenewal;
		if($scope.tmpNewPath.allowRenewal == true)
			$scope.tmpNewPath.refresherPercentage = item.refresherPercentage.toString();
		else
			$scope.tmpNewPath.refresherPercentage = "";
		$scope.tmpNewPath.monthsExpiration = item.monthsExpiration.toString();
		$scope.tmpNewPath.status = $scope.getStatusObj(item.status);
		$scope.tmpNewPath.orgs = item.organizations;
		$scope.selectedOrgsList = $scope.tmpNewPath.orgs;
		$scope.filteredOrgsList = $scope.masterOrgsList;


		for(var i in $scope.filteredOrgsList){
			if($scope.filteredOrgsList[i]['id'] != undefined){
				for(var m in $scope.selectedOrgsList){
					if($scope.selectedOrgsList[m]['id'] != undefined && $scope.filteredOrgsList[i]['id'] == $scope.selectedOrgsList[m]['id']){
						$scope.filteredOrgsList[i].isChecked = true;
						break;
					}
				}
			}				
		}
		$scope.tmpNewPath.exercises = item.exercises;
		$scope.selectedExerciseList = $scope.tmpNewPath.exercises;

		if($scope.tmpNewPath.technology != "Agnostic")
			$scope.filteredAvailableExercisesList = $filter("filter")($scope.masterAvailableExercisesList,{ technology:$scope.tmpNewPath.technology});
		else
			$scope.filteredAvailableExercisesList = $filter("filter")($scope.masterAvailableExercisesList);


		for(var i in $scope.filteredAvailableExercisesList){
			if($scope.filteredAvailableExercisesList[i]['uuid'] != undefined){
				for(var m in $scope.selectedExerciseList){
					if($scope.selectedExerciseList[m] != undefined && $scope.filteredAvailableExercisesList[i]['uuid'] == $scope.selectedExerciseList[m]){
						$scope.filteredAvailableExercisesList[i].isChecked = true;
						break;
					}
				}
			}				
		}
		$('#addNewPathModal').modal('show');
	}

	$scope.updatePath = function(){

		$('#addNewPathModal').modal('hide');
		var obj = cloneObj($scope.tmpNewPath)
		obj.orgs = $scope.selectedOrgsList;
		obj.exercises = $scope.selectedExerciseList;
		for(var i=0;i<obj.orgs.length;i++){
			if (typeof obj.orgs[i] === 'string' || typeof obj.orgs[i] === 'number'){
				continue;
			}
			else{
				obj.orgs[i] = obj.orgs[i].id;
			}
		}
		for(var i=0;i<obj.tags.length;i++){
			if (typeof obj.tags[i] === 'string' || obj.tags[i] instanceof String){
				continue;
			}
			else{
				obj.tags[i] = obj.tags[i].text;
			}
		}
		if (typeof obj.status !== 'string' && typeof obj.status !== 'number'){
			obj.status = obj.status.id;
		}
		server.updatePath(obj);
	}

	$scope.isPathNameAvailable = function(name){
		if($scope.saveFlow && name == $scope.tmpNewPath.name){
			return true;
		}
		if($scope.tmpNewPath.name != "" && $scope.tmpNewPath.technology != "" && $scope.tmpNewPath.difficulty != "")
			server.isPathNameAvailable($scope.tmpNewPath.name, $scope.tmpNewPath.technology, $scope.tmpNewPath.difficulty);
	}
	$scope.$on('pathNameAvailable:updated', function(event,data) {
		$scope.pathNameAvailable = data.result;
	})
	$scope.masterOrgsList = [];
	$scope.filteredOrgsList = []; 
	$scope.$on('organizations:updated', function(event,data) {
		$scope.masterOrgsList = data;
		$scope.filteredOrgsList = $filter("filter")($scope.masterOrgsList, $scope.query);

		$scope.userObj = [];
		$scope.selectedOrgsList = [];



	});	
	$scope.orgstableconfig = {
			itemsPerPage: 5,
			fillLastPage: false
	}
	$scope.updateFilteredList = function() {
		$scope.filteredOrgsList = $filter("filter")($scope.masterOrgsList, $scope.query);
	};
	$scope.addNewPathModal = function(){
		if($scope.saveFlow){
			$scope.clear();
			$scope.saveFlow = false;
		}
		$('#addNewPathModal').modal('show');
	}

	$scope.updatePathsFilteredList = function() {
		$scope.filteredPathsList = $filter("filter")($scope.masterPathsList, $scope.queryPaths);
	};
	$scope.pathstableconfig = {
			itemsPerPage: 20,
			fillLastPage: false
	}
	$rootScope.availablePathsList = [];
	$scope.$on('paths:updated', function(event,data) {
		$scope.masterPathsList = data;
		$rootScope.availablePathsList  = data;
		$scope.filteredPathsList = $scope.masterPathsList;
	});

}]);

sf.controller('kb',['$scope','server','$filter','$rootScope',function($scope,server,$filter,$rootScope){
	$scope.saveFlow = false;
	$scope.filteredKBsList = []; 
	$scope.selectedKBRow = -1;
	$scope.kbstableconfig = {
			itemsPerPage: 30,
			fillLastPage: false
	}

	$scope.hubTechKBList = [];
	$scope.hubVulnKBList = [];
	

	$rootScope.exerciseFrameworksList = [];
	$scope.newFrameworkName = "";
	$scope.addFramework = function(){
		if($scope.newFrameworkName != "")
			server.addFramework($scope.newFrameworkName);
		$scope.newFrameworkName = "";
	}
	$scope.removeFramework = function(name){
		server.removeFramework(name);
	}
	$scope.$on('removeFramework:updated', function(event,data) {
		server.getFrameworks();
	});
	$scope.$on('addFramework:updated', function(event,data) {
		server.getFrameworks();
	});
	$scope.$on('getFrameworks:updated', function(event,data) {
		$rootScope.exerciseFrameworksList = [];
		for(var i = 0;i<data.length;i++){
			if($rootScope.exerciseFrameworksList.indexOf('name')==-1)
				$rootScope.exerciseFrameworksList.push(data[i].name);
		}	
	});
	


	$scope.isVulnerabilityKBUpdateAvailable = function(item){

		if(undefined==item || undefined==$scope.hubVulnKBList || $scope.hubVulnKBList.length == 0)
			return false;
		for(var i in $scope.hubVulnKBList){
			if($scope.hubVulnKBList.hasOwnProperty(i) && $scope.hubVulnKBList[i].uuid == item.uuid){
				try{
					if(moment.utc($scope.hubVulnKBList[i].lastUpdate.replace("[UTC]","")).format("X")>moment.utc(item.lastUpdate.replace("[UTC]","")).format("X")){
						return true;
					}
				}catch(e){
					try{
						if(moment.utc($scope.hubVulnKBList[i].lastUpdate).format("X")>moment.utc(item.lastUpdate).format("X")){
							return true;
						}
					}catch(e2){}

				}
				return false;
			}
		}
		return false;
	};
	$scope.openUpdateVulnerabilityKBModal = function(item){
		$scope.kbUpdateItem = item;
		$('#updateVulnerabilityKBModal').modal('show');
	};
	$scope.openUpdateTechnologyKBModal = function(item){
		$scope.kbUpdateItem = item;
		$('#updateTechnologyKBModal').modal('show');
	};

	$scope.updateHubTechnologyKB = function(item){
		server.updateHubTechnologyKB(item.uuid);
		$('#updateTechnologyKBModal').modal('hide');
	}
	$scope.updateHubVulnerabilityKB = function(item){
		server.updateHubVulnerabilityKB(item.uuid);
		$('#updateVulnerabilityKBModal').modal('hide');
	}
	$scope.isTechnologyKBUpdateAvailable = function(item){
		if(undefined==item || undefined==$scope.hubTechKBList || $scope.hubTechKBList.length == 0)
			return false;
		for(var i in $scope.hubTechKBList){
			if($scope.hubTechKBList.hasOwnProperty(i) && $scope.hubTechKBList[i].uuid == item.uuid){
				try{
					if(moment.utc($scope.hubTechKBList[i].lastUpdate.replace("[UTC]","")).format("X")>moment.utc(item.lastUpdate.replace("[UTC]","")).format("X")){
						return true;
					}
				}catch(e){
					try{
						if(moment.utc($scope.hubTechKBList[i].lastUpdate).format("X")>moment.utc(item.lastUpdate).format("X")){
							return true;
						}
					}catch(e2){}
				}
				return false;
			}
		}
		return false;
	};




	$scope.$on('hubTechnologyKBUpdated:updated', function(event,data) {
		server.getAllStacks();
	});

	$scope.$on('hubVulnerabilityKBUpdated:updated', function(event,data) {
		server.getAllKbs();
	});

	$scope.$on('hubVulnKBList:updated', function(event,data) {
		$scope.hubVulnKBList = data;
	});

	$scope.$on('hubTechKBList:updated', function(event,data) {
		$scope.hubTechKBList = data;
	})



	$scope.openVulnKB = function(item){
		$scope.vulnSaveFlow = false;

		for(var j=0; j<$rootScope.kbItems.length; j++){
			if($rootScope.kbItems[j].uuid == item.uuid && undefined != $rootScope.kbItems[j].md.text && $rootScope.kbItems[j].md.text != ""){
				var cMd = $rootScope.kbItems[j].md.text+"\n";
				if($rootScope.kbItems[j].kbMapping!=undefined){
					for(var i=0;i<Object.keys($rootScope.kbItems[j].kbMapping).length;i++){
						var tmpInner = $rootScope.kbItems[j].kbMapping[Object.keys($rootScope.kbItems[j].kbMapping)[i]]
						cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
					}
				}
				document.querySelector('#kbViewMD').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#kbViewMD'),
					viewer: true,
					usageStatistics:false,
					height: '500px',
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					initialValue: ''
				});
				editor.setMarkdown(cMd);
				$('#kbViewModal').modal('show');
				autoplayMdVideo('#kbViewMD');
				return;
			}
		}
		server.loadVulnKB(item.uuid);
	}
	$scope.$on('vulnKBLoaded:updated', function(event,data) {

		if($rootScope.visibility.kb){



			if(!$scope.vulnSaveFlow){
				var cMd = data.md.text+"\n";
				if(data.kbMapping!=undefined){
					for(var i=0;i<Object.keys(data.kbMapping).length;i++){
						var tmpInner = data.kbMapping[Object.keys(data.kbMapping)[i]]
						cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
					}
				}
				document.querySelector('#kbViewMD').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#kbViewMD'),
					viewer: true,
					usageStatistics:false,
					height: '500px',
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					initialValue: ''
				});
				editor.setMarkdown(cMd);
				$('#kbViewModal').modal('show');
				autoplayMdVideo('#kbViewMD')
			}
			else {
				document.querySelector('#vulnEditorMD').innerHTML = '';
				$scope.tmpVuln.md = {}
				$scope.tmpVuln.md.text = data.md.text;
				var vulnEditor = new toastui.Editor({
					el: document.querySelector('#vulnEditorMD'),
					usageStatistics:false,
					height: '340px',
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					initialEditType: 'markdown',
					hideModeSwitch: true,
					initialValue: $scope.tmpVuln.md.text,
					events: {
						change: function() {
							$scope.tmpVuln.md.text = vulnEditor.getMarkdown();
						},
					}
				});
			}

			var kbFound = false;
			for(var j=0; j<$rootScope.kbItems.length; j++){
				if($rootScope.kbItems[j].uuid == data.uuid){
					$rootScope.kbItems[j] = data;
					kbFound = true;
					break;
				}
			}
			if(!kbFound){
				$rootScope.kbItems.push(data)
			}



		}
	})


	$scope.tmpTech = {};
	$scope.tmpTech.technology = "";
	$scope.tmpTech.variant = "";
	$scope.tmpTech.imageUrl = "";
	$scope.tmpTech.md = {};
	$scope.tmpTech.md.text = "";
	$scope.tmpVuln = {}
	$scope.tmpVuln.vulnerability = "";
	$scope.tmpVuln.isAgnostic = false;
	$scope.tmpVuln.kbMapping = {};
	$scope.tmpVulnMappings = [];
	$scope.tmpVuln.category = "";
	$scope.tmpVuln.technology = "";
	$scope.tmpVuln.md = {};
	$scope.tmpVuln.md.text = "";

	$scope.vulnerabilityCategories = ["Cross-Site Scripting","NoSQL Injection","SQL Injection","XML Injection","Unsafe Deserialization","Cross-Site Request Forgery","Unrestricted File Download","Use of Dangerous Function","Sensitive Information Exposure","Inadequate Input Validation","Broken Cryptography","Unvalidated Redirects & Forwards","Code Injection","Broken Authentication","Broken Session Management","Broken Authorization","Unrestricted File Upload","Server-Side Template Injection","Insufficient Logging","","Initial Access","Execution","Persistence","Privilege Escalation","Defense Evasion","Credential Access","Discovery","Lateral Movement","Collection","Command and Control","Exfiltration","Impact"]

	$scope.updateTmpVulnByTech = function(){
		$rootScope.nonAgnosticKbItems = $filter("filter")($rootScope.kbItems, { technology: $scope.tmpVulnMapping.technology});
	}
	$scope.deleteTmpVulnMapping = function(item){
		for(var i=0; i<$scope.tmpVulnMappings.length;i++){
			if($scope.tmpVulnMappings[i].technology == item.technology){
				$scope.tmpVulnMappings.remove(i,i);
				return;
			}
		}
	};
	$scope.addTmpVulnMapping = function(){
		var a = { technology: $scope.tmpVulnMapping.technology, vulnerability: $scope.tmpVulnMapping.vulnerability}
		for(var i=0; i<$scope.tmpVulnMappings.length;i++){
			if($scope.tmpVulnMappings[i].technology == $scope.tmpVulnMapping.technology)
				return;
		}
		$scope.tmpVulnMappings.push(a);
	};
	$scope.addVulnerabilityModal = function(){
		$scope.vulnSaveFlow = false;
		$scope.tmpVuln = {}
		$scope.tmpVuln.vulnerability = "";
		$scope.tmpVuln.category = "";
		$scope.tmpVuln.technology = "";
		$scope.tmpVuln.isAgnostic = false;
		$scope.tmpVuln.kbMapping = {};
		$scope.tmpVulnMappings = [];
		$scope.tmpVuln.md = {};
		$scope.tmpVuln.md.text = "";
		document.querySelector('#vulnEditorMD').innerHTML = '';
		var vulnEditor = new toastui.Editor({
			el: document.querySelector('#vulnEditorMD'),
			usageStatistics:false,
			height: '340px',
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			initialEditType: 'markdown',
			hideModeSwitch: true,
			initialValue: $scope.tmpVuln.md.text,
			events: {
				change: function() {
					$scope.tmpVuln.md.text = vulnEditor.getMarkdown();
				},
			}
		});
		$('#addVulnerabilityModal').modal('show');

	}
	$scope.addVulnerability = function(){
		$scope.tmpVuln.kbMapping = {};
		if($scope.tmpVuln.isAgnostic){
			for(var i=0;i<$scope.tmpVulnMappings.length;i++){
				$scope.tmpVuln.kbMapping[$scope.tmpVulnMappings[i].technology] = $scope.tmpVulnMappings[i].vulnerability;
			}
		}
		server.addVulnerability($scope.tmpVuln);
	}
	$scope.editVulnerabilityModal = function(item){
		$scope.vulnSaveFlow = true;
		server.loadVulnKB(item.uuid);
		$scope.tmpVuln.id = item.id;
		$scope.tmpVuln.uuid = item.uuid;
		$scope.tmpVuln.technology = item.technology;
		$scope.tmpVuln.isAgnostic = item.isAgnostic || false;
		$scope.tmpVuln.vulnerability = item.vulnerability;
		$scope.tmpVuln.category = item.category
		$scope.tmpVuln.kbMapping = item.kbMapping || {};
		$scope.tmpVulnMappings = [];
		
		var keys = Object.keys($scope.tmpVuln.kbMapping);
		for(var j=0; j<keys.length;j++){
			var a = { technology: keys[j], vulnerability: $scope.tmpVuln.kbMapping[keys[j]]};
			$scope.tmpVulnMappings.push(a);
		}
		$('#addVulnerabilityModal').modal('show');
	}
	$scope.$on('updateVulnerability:updated', function(event,data) {
		$scope.tmpVuln = {}
		$scope.tmpVuln.vulnerability = "";
		$scope.tmpVuln.category = "";
		$scope.tmpVuln.technology = "";
		$scope.tmpVuln.isAgnostic = false;
		$scope.tmpVuln.md = {};
		$scope.tmpVuln.kbMapping = {};
		$scope.tmpVulnMappings = [];
		$scope.tmpVuln.md.text = "";
		$('#addVulnerabilityModal').modal('hide');
		server.getAllKbs();
	});
	$scope.$on('addVulnerability:updated', function(event,data) {
		$scope.tmpVuln = {}
		$scope.tmpVuln.vulnerability = "";
		$scope.tmpVuln.category = "";
		$scope.tmpVuln.technology = "";
		$scope.tmpVuln.isAgnostic = false;
		$scope.tmpVuln.md = {};
		$scope.tmpVuln.kbMapping = {};
		$scope.tmpVulnMappings = [];
		$scope.tmpVuln.md.text = "";
		$('#addVulnerabilityModal').modal('hide');
		server.getAllKbs();
	});
	$scope.$on('deleteVulnerability:updated', function(event,data) {
		$('#deleteVulnerabilityModal').modal('hide');
		server.getAllKbs();
	});
	$scope.$on('updateTechnology:updated', function(event,data) {
		$scope.tmpTech = {};
		$scope.tmpTech.technology = "";
		$scope.tmpTech.variant = "";
		$scope.tmpTech.imageUrl = "";
		$scope.tmpTech.md = {};
		$scope.tmpTech.md.text = "";
		$('#addStackModal').modal('hide');
		server.getAllStacks();
	});
	$scope.$on('addTechnology:updated', function(event,data) {
		$scope.tmpTech = {};
		$scope.tmpTech.technology = "";
		$scope.tmpTech.variant = "";
		$scope.tmpTech.imageUrl = "";
		$scope.tmpTech.md = {};
		$scope.tmpTech.md.text = "";
		$('#addStackModal').modal('hide');
		server.getAllStacks();
	});
	$scope.$on('deleteTechnology:updated', function(event,data) {
		$('#deleteTechnologyModal').modal('hide');
		server.getAllStacks();
	});

	$scope.updateVulnerability = function(){
		$scope.tmpVuln.kbMapping = {};
		if($scope.tmpVuln.isAgnostic){
			for(var i=0;i<$scope.tmpVulnMappings.length;i++){
				$scope.tmpVuln.kbMapping[$scope.tmpVulnMappings[i].technology] = $scope.tmpVulnMappings[i].vulnerability;
			}
		}
		server.updateVulnerability($scope.tmpVuln);
	}
	$scope.deleteVulnerabilityModal = function(item){
		$scope.tmpVulnDeleteItem = item;
		$('#deleteVulnerabilityModal').modal('show');
	}
	$scope.deleteVulnerability = function(){
		server.deleteVulnerability($scope.tmpVulnDeleteItem);
	}
	$scope.addTechnology = function(){
		server.addTechnology($scope.tmpTech);
	}
	$scope.deleteTechnology = function(){
		server.deleteTechnology($scope.tmpTechDeleteItem);
	}
	$scope.deleteTechnologyModal = function(item){
		$scope.tmpTechDeleteItem = item;
		$('#deleteTechnologyModal').modal('show');
	}

	$scope.updateTechnology = function(){
		server.updateTechnology($scope.tmpTech);
	}
	$scope.editTechnologyModal = function(item){
		$scope.stackSaveFlow = true;
		$scope.tmpTech.id = item.id;
		$scope.tmpTech.uuid = item.uuid;
		$scope.tmpTech.imageUrl = item.imageUrl;
		$scope.tmpTech.technology = item.technology;
		$scope.tmpTech.variant = item.variant;
		server.loadStackKB(item.uuid);
		$('#addStackModal').modal('show');
	}


	$scope.addTechnologyModal = function(){
		$scope.stackSaveFlow = false;
		document.querySelector('#stackEditorMD').innerHTML = '';
		var technologyEditor = new toastui.Editor({
			el: document.querySelector('#stackEditorMD'),
			usageStatistics:false,
			height: '340px',
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			initialEditType: 'markdown',
			hideModeSwitch: true,
			initialValue: "",
			events: {
				change: function() {
					$scope.tmpTech.md.text = technologyEditor.getMarkdown();
				},
			}
		});
		$('#addStackModal').modal('show');

	}

	$scope.openStackKB = function(item){
		$scope.stackSaveFlow = false;

		for(var j=0; j<$rootScope.stackItems.length; j++){
			if($rootScope.stackItems[j].uuid == item.uuid && undefined != $rootScope.stackItems[j].md.text && $rootScope.stackItems[j].md.text != ""){
				document.querySelector('#stackViewMD').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#stackViewMD'),
					viewer: true,
					usageStatistics:false,
					height: '500px',
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					initialValue: $rootScope.stackItems[j].md.text
				});
				$('#stackViewModal').modal('show');
				autoplayMdVideo('#stackViewMD')
				return;
			}
		}
		server.loadStackKB(item.uuid);
	}
	$scope.$on('stackKBLoaded:updated', function(event,data) {
		if($rootScope.visibility.kb){
			if(!$scope.stackSaveFlow){
				document.querySelector('#stackViewMD').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#stackViewMD'),
					viewer: true,
					usageStatistics:false,
					height: '500px',
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					initialValue: data.md.text
				});
				$('#stackViewModal').modal('show');
				autoplayMdVideo('#stackViewMD')

			}
			else {
				$scope.tmpTech.md = {};
				$scope.tmpTech.md.text = data.md.text;
				document.querySelector('#stackEditorMD').innerHTML = '';
				var technologyEditor = new toastui.Editor({
					el: document.querySelector('#stackEditorMD'),
					usageStatistics:false,
					height: '340px',
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					initialEditType: 'markdown',
					hideModeSwitch: true,
					initialValue: $scope.tmpTech.md.text,
					events: {
						change: function() {
							$scope.tmpTech.md.text = technologyEditor.getMarkdown();
						},
					}
				});
			}

			var kbFound = false;
			for(var j=0; j<$rootScope.stackItems.length; j++){
				if($rootScope.stackItems[j].uuid == data.uuid){
					$rootScope.stackItems[j] = data;
					kbFound = true;
					break;
				}
			}
			if(!kbFound){
				$rootScope.stackItems.push(data)
			}

		}
	})
	$scope.updateFilteredList = function() {
		$scope.filteredKBsList = $filter("filter")($rootScope.kbItems, $scope.query);
	};
	$scope.updateStackFilteredList = function() {
		$scope.filteredStacksList = $filter("filter")($rootScope.stackItems, $scope.queryStack);
	};
	$rootScope.stackItems = [];
	$scope.filteredStacksList = []; 
	$scope.selectedStackRow = -1;
	$scope.stackstableconfig = {
			itemsPerPage: 30,
			fillLastPage: false
	}
	$scope.$on('stackKbs:updated', function(event,data) {
		$rootScope.stackItems = data;
		$rootScope.technologyList = [];
		for(var i=0;i<$rootScope.stackItems.length;i++){
			if($rootScope.technologyList.indexOf($rootScope.stackItems[i].technology)<0)
				$rootScope.technologyList.push($rootScope.stackItems[i].technology)
		}
		$scope.updateStackFilteredList();
	});
	$scope.$on('vulnerabilityKbs:updated', function(event,data) {
		$rootScope.kbItems = data;
		$rootScope.nonAgnosticKbItems = $filter("filter")($rootScope.kbItems, { isAgnostic: false});
		$scope.updateFilteredList();
	});			
}]);
sf.controller('gateways',['$scope','server','$filter','$rootScope',function($scope,server,$filter,$rootScope){
	$scope.saveFlow = false;
	$scope.masterGateways = [];
	$scope.filteredGatewaysList = []; 
	$scope.selectedGatewayRow = -1;
	$scope.gatewaystableconfig = {
			itemsPerPage: 30,
			fillLastPage: false
	}
	$scope.newGateway = {};
	$scope.newGateway.region = "";
	$scope.newGateway.name = "";
	$scope.newGateway.fqdn = "";
	$scope.newGateway.enableImageSync = true;
	$scope.newGateway.enableScaleIn = true;
	$scope.newGateway.status = true;
	$scope.newGateway.placementStrategy = "";
	$scope.awsRegions = [];
	$scope.refreshing = false;

	$scope.placementStrategyCodes = ["0","1"]
	$scope.placementStrategies = [{ name:"BinPack",code:"0"},{name:"Spread",code:"1"}]
	$scope.getStringForPlacementStrategy = function(code){
		if(code==undefined || code == null)
			return "";
		for(var i in $scope.placementStrategies){
			if($scope.placementStrategies[i].code==code)
				return $scope.placementStrategies[i].name;
		}
	}
	$scope.refreshInstanceNodes = function(){
		$scope.refreshing = true;
		if(server.user.r<0){
			$scope.getOnlineGws();
		}
	}
	$rootScope.bytesToSize = function(bytes,auto) {
		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
		if (bytes == 0) return '0 Byte';
		if(sizes.indexOf(auto)>=0){
			var i = sizes.indexOf(auto)
		}
		else{
			var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		}

		return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};
	$scope.lastRefreshed = "";

	$scope.getReviewedDate = function(date){
		if(null!=date && undefined != date)
			return moment(date).format("MMM D, YYYY")
			else
				return "N/A";
	}
	$scope.updateFilteredList = function() {
		$scope.filteredGatewaysList = $filter("filter")($scope.masterGateways, $scope.query);
	};
	$scope.imageModuleAvailable = false;
	$scope.$on('platformFeatures:updated', function(event,data) {
		$scope.imageModuleAvailable = data.imageModule;
	});

	$scope.$on('instanceNodes:updated', function(event,data) {
		$scope.instanceNodes = data;
		$scope.refreshing = false;
		$scope.lastRefreshed = new Date();
	});

	$scope.$on('gateways:updated', function(event,data) {
		$scope.masterGateways = data;
		$scope.filteredGatewaysList = $scope.masterGateways;
	});
	$scope.$on('awsRegions:updated', function(event,data) {
		$scope.awsRegions = data;
	});	
	$scope.addGatewayModal= function(){
		$scope.saveFlow = false;
		$('#addGatewayModal').modal('show');
	}

	$scope.removeImage = function(image, region, instance){

		if(undefined==image || undefined == region || undefined == instance)
			return;

		var name = "";
		if(image.repoTags[0] != undefined){
			name = image.repoTags[0];
		}
		else if(image.repoDigests[0] != undefined){
			name = image.repoDigests[0];
		}

		server.removeImage(name,region,instance);
	}
	
	$scope.getOnlineGws = function(){
		server.getOnlineGws();
	}
	$scope.$on('onlineGws:updated', function(event,data) {
		$scope.onlineGws = data;
	});	

	$scope.isGatewayOnline = function(region){
		for(gw in $scope.onlineGws){
			if($scope.onlineGws[gw].region == region)
				return true;
		}
		return false;
	}

	getAWSSupportedRegionFromCode = function(reg){
		for(obj in $scope.awsRegions){
			if($scope.awsRegions.hasOwnProperty(obj)){
				if($scope.awsRegions[obj].code==reg){
					return $scope.awsRegions[obj];
				}
			}
		}
		return "";
	}
	$scope.editGateway = function(gw){
		$scope.saveFlow = true;
		$scope.newGateway.id = gw.id;
		$scope.newGateway.region = gw.region;
		$scope.newGateway.fqdn = gw.fqdn;
		$scope.newGateway.status = gw.active;
		$scope.newGateway.name = gw.name;
		$scope.newGateway.enableImageSync = gw.enableImageSync;
		$scope.newGateway.enableScaleIn = gw.enableScaleIn;
		$scope.newGateway.placementStrategy = gw.placementStrategy;

		$('#addGatewayModal').modal('show');
	}
	$scope.saveGateway = function(gw){
		$scope.newGateway.region = $scope.newGateway.region.code;
		server.updateGateway($scope.newGateway)
		$('#addGatewayModal').modal('hide');

	}

	$scope.deleteGateway = function(id){
		server.deleteGateway(id);
	} 
	$scope.$on('gatewayDeleted:updated', function(event,data) {
		server.getGateways();
		$scope.getOnlineGws();
	});

	$scope.addGateway = function(){
		var obj = {};
		obj.region = $scope.newGateway.region.code;
		obj.fqdn = $scope.newGateway.fqdn;
		obj.name =  $scope.newGateway.name;
		obj.status = $scope.newGateway.status;
		obj.enableImageSync = $scope.newGateway.enableImageSync;
		obj.enableScaleIn = $scope.newGateway.enableScaleIn;
		obj.placementStrategy = $scope.newGateway.placementStrategy;
		server.addGateway(obj);
		$('#addGatewayModal').modal('hide');
	}



	$scope.$on('gatewayAdded:updated', function(event,data) {
		if(data.result=="success"){
			server.getGateways();
			$scope.getOnlineGws();
			$scope.newGateway.region = "";
			$scope.newGateway.id = "";
			$scope.newGateway.fqdn = "";
			$scope.newGateway.status = true;
			$scope.newGateway.name = "";
			$scope.newGateway.enableImageSync = true;
			$scope.newGateway.enableScaleIn = true;
			$scope.newGateway.placementStrategy = "";
		}
	});
}])


sf.controller('completedExercises',['$scope','server','$rootScope','$filter','$location',function($scope,server,$rootScope,$filter,$location){
	$scope.masterCompletedReviews = [];
	$scope.hideCancelled = true;
	$scope.filteredCompletedList = []; 
	$scope.selectedCompletedRow = -1;
	$scope.showCompletedResult = false;
	$scope.showCompletedCodeDiff = false;
	$scope.showCompletedLogs = false;
	$scope.query = "";
	$scope.userFeedback = "";
	$scope.completedZipError = false;
	$scope.completedEmptyLog = false;
	$scope.completedEmptyDiff = false;
	$scope.completedtableconfig = {
			itemsPerPage: 10,
			fillLastPage: false
	}
	$scope.triggerHintsMarkdown = function(h){
		window.setTimeout(function(){
			if(undefined != h){
				document.querySelector('#h'+h.id+'-md').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#h'+h.id+'-md'),
					viewer: true,
					usageStatistics:false,
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					height: '500px',
					initialValue: h.md.text
				});
				autoplayMdVideo('#h'+h.id+'-md')
			}
		},100);
	}
	$scope.triggerFlagsCompletedMarkdown = function(fq){
		window.setTimeout(function(){
			if(undefined != fq){
				document.querySelector('#f'+fq.id+'-md').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#f'+fq.id+'-md'),
					viewer: true,
					usageStatistics	: false,
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					height: '500px',
					initialValue: fq.md.text
				});
				autoplayMdVideo('#f'+fq.id+'-md')
				$('#f'+fq.id+'-md').find('a').each(function() {
					if(this.href==undefined || this.href=="")
						return;
					if(this.href.indexOf("#exerciseKB")>=0){
						$(this).replaceWith($(this).text());
					}
				});
			}
		},100);
	}

	$scope.getIssueReportedStatus = function(item){

		if(!item.issuesReported)
			return "N/A";
		if(item.issuesReported && item.issuesReportedAddressed)
			return "Yes";
		if(item.issuesReported && !item.issuesReportedAddressed)
			return "No";

	}
	$scope.$on('userFeedback:updated', function(event,data) {
		$scope.userFeedback = data.id;
	});


	$scope.$watch("hideCancelled", function() {
		$scope.updateFilteredList();
	});

	$scope.getReviewedDate = function(date){
		if(null!=date && undefined != date)
			return moment(date).local().format("MMM D, HH:mm (Z)")
			else
				return "N/A";
	}

	$scope.updateFilteredList = function() {
		$scope.filteredCompletedList = $filter("filter")($scope.masterCompletedReviews, $scope.query);
		if($scope.hideCancelled){
			$scope.filteredCompletedList = $filter("filter")($scope.filteredCompletedList,{ status:"!CANCELLED"});
		}
	};

	$scope.$on('completedReviews:updated', function(event,data) {
		$scope.masterCompletedReviews = data;
		$scope.filteredCompletedList = $filter("filter")($scope.masterCompletedReviews, "!CANCELLED");
	});
	$scope.getDurationInterval = function(start,end){
		var out = moment.utc(moment(end).diff(moment(start))).format("H mm").replace(" ","h")+"'";
		return out;
	};
	$scope.getMinutesDuration = function(dur){
		if(undefined != dur && null != dur && 0 != dur){
			return moment.utc(moment.duration(dur,"m").asMilliseconds()).format("H mm").replace(" ","h")+"'";
		}
		else{
			return "N/A"
		}
	};
	$scope.getDatesInterval = function(start,end){
		var out = moment(start).local().format("YYYY/MM/DD, HH:mm");
		out += " - "+moment(end).local().format("HH:mm (Z)");
		return out;
	}
	var statusClassMap = {
			"REVIEWED": "table-success",
			"AUTOREVIEWED":"table-primary",
			"REVIEWED_MODIFIED":"table-info",
			"CANCELLED": "table-warning"
	}
	$scope.getStatusClass = function(status) {
		return statusClassMap[status]
	};
	$scope.getExerciseStatusString = function(status){
		switch(status){
		case "REVIEWED":
			return "Reviewed"
		case "AUTOREVIEWED":
			return "Auto"
		case "REVIEWED_MODIFIED":
			return "Auto (Modified)"
		case "CANCELLED":
			return "Cancelled"

		default: return "";
		}
	}
	$scope.getStatusString = function(status){
		switch(status){
		case "1":
			return "Vulnerable"
		case "0":
			return "Not Vulnerable"
		case "2":
			return "Broken Functionality"
		case "4":
			return "Not Addressed"
		case "5":
			return "Partially Remediated"
		case "3":
			return "N/A"

		default: return "";
		}
	}
	var remediationClassMap = {
			"0": "table-success",
			"1": "table-danger",
			"2": "table-warning",
			"4": "table-info",
			"5": "table-warning"
	};

	$scope.getRemediationClass = function(status){
		return remediationClassMap[status]
	};

	$scope.toggleCompletedCodeDiff = function(){
		if($scope.showCompletedCodeDiff == true)
			$scope.showCompletedCodeDiff = false;
		else{
			$scope.showCompletedLogs = false;
			$scope.showCompletedCodeDiff = true;
		}
	}
	$scope.toggleCompletedInstanceLogs = function(){
		if($scope.showCompletedLogs == true)
			$scope.showCompletedLogs = false;
		else{
			$scope.showCompletedCodeDiff = false;
			$scope.showCompletedLogs = true;
		}
	}

	$scope.complaintExerciseFlag = "";
	$scope.scoreModifyExercise;
	$scope.scoreModifyResult;

	$scope.updatedResult = {};

	$scope.openScoreEditModal = function(e,f){
		$scope.scoreModifyExercise = e;
		$scope.scoreModifyResult = f;
		$('#scoringEditModal').modal('show');
	}
	$scope.updateScore = function(){
		var obj = {};
		obj.comment = $scope.updatedResult.comment;
		obj.status  = $scope.updatedResult.status.id;
		obj.score = $scope.updatedResult.score;
		obj.id = $scope.scoreModifyExercise;
		obj.name = $scope.scoreModifyResult.name;
		server.updateScore(obj);
	}

	$scope.$on('scoringUpdated:updated', function(event,data) {	
		server.getCompletedReviewDetails($scope.scoreModifyExercise);
		$scope.complaintModalTextInput = "";
		$scope.updatedResult.comment = "";
		$scope.updatedResult.status = "";
		$scope.updatedResult.score = "";
	});

	$scope.openCommentModal = function(e,f){
		$scope.complaintExerciseId = e;
		$scope.complaintExerciseFlag = f;
		$('#scoringCommentModal').modal('show');
	}

	$scope.sendScoringComment = function(){
		var obj = {};
		obj.name = $scope.complaintExerciseFlag;
		obj.text = $scope.complaintModalTextInput;
		obj.id = $scope.complaintExerciseId;
		server.addScoringComment(obj);
		$('#scoringCommentModal').modal('hide');
	}

	$scope.$on('scoringCommentAdded:updated', function(event,data) {	
		server.getCompletedReviewDetails($scope.complaintExerciseId);
		$scope.complaintModalTextInput = "";
	});

	$scope.getQuestionName = function(resName){
		if(undefined==resName || resName=="")
			return;
		for(var i in $scope.completedItemDetails.exercise.flags){
			if($scope.completedItemDetails.exercise.flags.hasOwnProperty(i)){
				for(var j in $scope.completedItemDetails.exercise.flags[i].flagList){
					if($scope.completedItemDetails.exercise.flags[i].flagList.hasOwnProperty(j) && $scope.completedItemDetails.exercise.flags[i].flagList[j].selfCheckAvailable && $scope.completedItemDetails.exercise.flags[i].flagList[j].selfCheck.name==resName){
						return $scope.completedItemDetails.exercise.flags[i].title;
					}
				}
			}
		}
	}
	$scope.getQuestionMaxScore = function(resName){
		if(undefined==resName || resName=="")
			return;
		for(var i in $scope.completedItemDetails.exercise.flags){
			if($scope.completedItemDetails.exercise.flags.hasOwnProperty(i)){
				for(var j in $scope.completedItemDetails.exercise.flags[i].flagList){
					if($scope.completedItemDetails.exercise.flags[i].flagList.hasOwnProperty(j) && $scope.completedItemDetails.exercise.flags[i].flagList[j].selfCheckAvailable && $scope.completedItemDetails.exercise.flags[i].flagList[j].selfCheck.name==resName){
						return $scope.completedItemDetails.exercise.flags[i].flagList[j].maxScore;
					}
				}
			}
		}

	}

	$scope.markCancelled = function(){
		server.markCancelled($scope.completedItemDetails.id);
	}

	$scope.$on('completedReviewDetails:updated', function(event,data) {

		data.placements = [];
		var offset = $('#showCompletedResults').offset();
		$('html, body').animate({
			scrollTop: offset.top - 50,
			scrollLeft: offset.left
		});

		for(var i in data.results){
			if(data.results[i].firstForFlag){
				var tmpObj = {}
				tmpObj.name = data.results[i].flagTitle;
				tmpObj.placement = "1st place";
				data.placements.push(tmpObj)
			}
			if(data.results[i].secondForFlag){
				var tmpObj = {}
				tmpObj.name = data.results[i].flagTitle;
				tmpObj.placement = "2nd place";
				data.placements.push(tmpObj)
			}
			if(data.results[i].thirdForFlag){
				var tmpObj = {}
				tmpObj.name = data.results[i].flagTitle;
				tmpObj.placement = "3rd place";
				data.placements.push(tmpObj)
			}

			switch(data.results[i].status){
			case "VULNERABLE":
				data.results[i].status = "Vulnerable"
					break;
			case "NOT_VULNERABLE":
				data.results[i].status = "Not Vulnerable"
					break;
			case "BROKEN_FUNCTIONALITY":
				data.results[i].status = "Broken Functionality"
					break;
			case "NOT_AVAILABLE":
				data.results[i].status = "N/A"
					break;
			case "PARTIALLY_REMEDIATED":
				data.results[i].status = "Partially Remediated"
					break
			case "NOT_ADDRESSED":
				data.results[i].status = "Not Addressed"
					break;
			}


		}
		if(data.results.length == 0){
			data.results = [];
			for(var j in data.exercise.flags){
				data.results[j] = {};
				data.results[j].name = data.exercise.flags[j].title;
				data.results[j].status = "N/A";
				data.results[j].score = 0;
				data.results[j].verified = false; 
			}
		}

		$scope.completedItemDetails = data;
		$scope.showCompletedResult = false;
		$scope.showCompletedCodeDiff = false;

		$scope.showCompletedResult = true;
		$scope.completedZipError = false;
		$scope.completedEmptyLog = false;
		$scope.completedEmptyDiff = false;


		var tba = "";

		try{
			JSZipUtils.getBinaryContent($scope.completedItemDetails.id,$rootScope.ctoken,'/management/team/handler','getReviewFile', function(err, data) {
				if(err) {
					throw err; // or handle err
				}
				if(data.byteLength<=48){
					$scope.completedEmptyDiff = true;
					return;
				}
				JSZip.loadAsync(data).then(function(zip) {
					for(file in zip.files){
						if(file.indexOf('sourceDiff.txt')>-1){
							zip.file(file).async("string").then(function success(content) {
								var diffString = content;
								if(diffString==""){
									$scope.completedEmptyDiff = true;
									$('#completedTargetDiv').empty()
									return;
								}
								try{
									var diff2htmlUi = new Diff2HtmlUI({diff : diffString });
									diff2htmlUi.draw( '#completedTargetDiv', {
										inputFormat : 'diff',
										showFiles : true,
										matching : 'lines'
									});
									diff2htmlUi.highlightCode('#completedTargetDiv');
								} catch(err){
									$scope.completedEmptyDiff = true;
								}
							})                        
						}
				
					}
				});
			});    
		}
		catch(err2){
			$scope.completedEmptyDiff = true;
		}
	});

	$scope.showCompletedDetailsFor = function(eId, feedback, index){
		server.getCompletedReviewDetails(eId);
		if(feedback){
			server.getUserFeedback(eId);
		}
		else{
			$scope.userFeedback = "";
		}
		$scope.selectedCompletedRow = index;
		$location.path("exercises/details/"+eId, false);

	}
}])
sf.controller('users',['$scope','server','$location','$rootScope','$filter',function($scope,server,$location,$rootScope,$filter){

	$scope.saveFlow = false;

	$scope.masterUsersList = [];
	$scope.filteredUsersList = []; 

	$scope.remediatedPerIssue = {}
	$scope.remediatedPerIssue.data = [];
	$scope.remediatedPerIssue.labels = [];
	$scope.remediatedPerCategory = {}
	$scope.remediatedPerCategory.data = [];
	$scope.remediatedPerCategory.labels = [];
	$scope.remediationRatePerIssue = [];
	$scope.remediationRatePerCategory = [];
	$scope.scorePerExercises = {};

	$scope.user = server.user;

	$scope.adminUserRoleFilter = function(input) {
		return input.id >= $scope.user.r;
	};

	$scope.options = {}
	$scope.options.layout = {
			padding: {
				left: 0,
				right: 0,
				top: 0,
				bottom: 0
			}
	}
	$scope.options.animation = false;
	$scope.options.pieceLabel = {
			// render 'label', 'value', 'percentage' or custom function, default is 'percentage'
			render: 'percentage',
			// precision for percentage, default is 0
			precision: 0,
			// identifies whether or not labels of value 0 are displayed, default is false
			showZero: false,
			// font size, default is defaultFontSize
			fontSize: 12,
			// font color, can be color array for each data, default is defaultFontColor
			fontColor: '#FFF',
			// font style, default is defaultFontStyle
			fontStyle: 'normal',
			// font family, default is defaultFontFamily
			fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			// draw label in arc, default is false
			arc: false,
			// position to draw label, available value is 'default', 'border' and 'outside'
			// default is 'default'
			position: 'border',
			// draw label even it's overlap, default is false
			overlap: true
	}
	$scope.options.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};
	$scope.options.title = {
			text : "",
			display:true,
			position: 'bottom'

	}
	$scope.radarOptions = {}
	$scope.radarOptions.animation = false;
	$scope.radarOptions.title = {
			text : "",
			display:true,
			position: 'top'

	}
	$scope.radarOptions.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};

	$scope.formatOrganizations = function(objArray){
		var stringOut = "";
		for(var i=0;i<objArray.length;i++){
			stringOut += objArray[i].name;
			if(i<objArray.length-1){
				stringOut += ", ";
			}
		}
		return stringOut;
	}

	$scope.newNotificationText = "";
	$scope.sendNotificationModal = function(){
		$("#sendNotificationModal").modal('show');
	}
	$scope.sendNotification = function(){
		server.sendNotification($scope.selectedUser,$scope.newNotificationText);
		$scope.newNotificationText = "";
		$("#sendNotificationModal").modal('hide');
	}
	$scope.$on('userProfile:updated', function(event,data) {
		$scope.user = data;
	});

	$scope.$on('notificationSent:updated', function(event,data) {


	});

	$scope.newUser = {};
	$scope.newUser.username = "";
	$scope.newUser.firstName = "";
	$scope.newUser.lastName = "";
	$scope.newUser.email = "";
	$scope.newUser.credits = -1
	$scope.newUser.country = "";
	$scope.newUser.role = { id:7, name:"User"};
	$scope.newUser.concurrentExercisesLimit = 1;
	$scope.newUser.password = "";
	$scope.newUser.emailVerified = true;
	$scope.newUser.forceChangePassword = true;
	$scope.newUser.organization = "";

	$scope.availableUserRoles = [{ id:7, name:"User"},{ id:4, name:"Stats Analyst"},{ id:3, name:"Team Manager"},{ id:0, name: "Organization Admin"},{ id:-1, name: "SF Admin"}];



	$scope.countries = server.countries;

	$scope.getUserRoleString = function(id){
		for(var i in $scope.availableUserRoles){
			if($scope.availableUserRoles.hasOwnProperty(i)){
				if($scope.availableUserRoles[i].id==id)
					return $scope.availableUserRoles[i].name;
			}
		}
		return id;
	}

	$scope.usernameAvailable = true;

	$scope.isUsernameAvailable = function(){
		if($scope.newUser.username != "" && $scope.newUser.username!=$scope.userDetails.user)
			server.checkUsernameAvailable($scope.newUser.username);
	}


	$scope.$on('usernameAvailable:updated', function(event,data) {
		$scope.usernameAvailable = data.result;
	});

	$scope.$on('userUpdate:updated', function(event,data) {
		$scope.saveFlow = false;
		$scope.selectedUser = $scope.tmpUpdatedName;
		$scope.getUserDetails($scope.selectedUser);
		server.getUsers();
		$('#addUserModal').modal('hide');
		$scope.newUser = {};
		$scope.newUser.firstName = "";
		$scope.newUser.lastName = "";
		$scope.newUser.email = "";
		$scope.newUser.organizations = [];
		$scope.newUser.country = "";
		$scope.newUser.role = { id:7, name:"User"};
		$scope.newUser.concurrentExercisesLimit = 1;
		$scope.newUser.password = "";
		$scope.newUser.passwordRepeat = "";
		$scope.newUser.credits = 10;
		$scope.newUser.emailVerified = true;
		$scope.newUser.forceChangePassword = true;
		$scope.newUser.organization = "";
	});

	$scope.$on('userAdded:updated', function(event,data) {
		server.getUsers();
		$('#addUserModal').modal('hide');
		$scope.newUser = {};
		$scope.newUser.firstName = "";
		$scope.newUser.lastName = "";
		$scope.newUser.email = "";
		$scope.newUser.country = "";
		$scope.newUser.organizations = [];
		$scope.newUser.role = { id:7, name:"User"};
		$scope.newUser.concurrentExercisesLimit = 1;
		$scope.newUser.password = "";
		$scope.newUser.passwordRepeat = "";
		$scope.newUser.credits = 10;
		$scope.newUser.emailVerified = true;
		$scope.newUser.forceChangePassword = true;
		$scope.newUser.organization = "";
	});

	$scope.addUserModal = function(){
		$scope.saveFlow = false;
		$('#addUserModal').modal('show');
	}
	$scope.tmpUpdatedName = "";
	$scope.updateUserModal = function(){
		$scope.saveFlow = true;

		$scope.newUser.username = $scope.userDetails.user;
		$scope.newUser.firstName = $scope.userDetails.firstName;
		$scope.newUser.lastName = $scope.userDetails.lastName;
		$scope.newUser.email = $scope.userDetails.email;
		$scope.newUser.organizations = $scope.userDetails.organizations;
		$scope.newUser.forceChangePassword = $scope.userDetails.forceChangePassword;
		$scope.newUser.emailVerified = $scope.userDetails.emailVerified;

		$scope.orgObj = {};
		for(var i=0;i<$scope.newUser.organizations.length;i++){
			$scope.orgObj[$scope.newUser.organizations[i].id] = true;
		}

		$scope.newUser.country = $scope.userDetails.country;
		$scope.newUser.role = { id:$scope.userDetails.r, name:$scope.getUserRoleString($scope.userDetails.r) }
		$scope.newUser.concurrentExercisesLimit = $scope.userDetails.instanceLimit;
		$scope.newUser.organization = $scope.userDetails.defaultOrganization;
		$scope.newUser.credits = $scope.userDetails.credits


		$('#addUserModal').modal('show');
	}

	$scope.organizationstableconfig = {
			itemsPerPage: 10,
			fillLastPage: false
	}
	$scope.isUserManagingOrg = function(id){
		if(undefined==id || undefined == $scope.newUser.organizations)
			return false;
		for(var i=0;i<$scope.newUser.organizations.length;i++){
			if($scope.newUser.organizations[i].id == id){
				return true;
			}
		}
		return false;
	}
	$scope.managedCheckToggle=function(s){
		if(undefined==s || undefined == $scope.newUser.organizations)
			return;
		for(var i=0;i<$scope.newUser.organizations.length;i++){
			if($scope.newUser.organizations[i].id == s.id){
				$scope.newUser.organizations.remove(i,i);
				return;
			}
		}
		var obj = {};
		obj.id = s.id;
		obj.name = s.name;
		$scope.newUser.organizations.push(obj);		
	}

	$scope.$on('allOrganizations:updated', function(event,data) {
		$scope.allOrgs = data;
	});


	$scope.updateUser = function(){
		$scope.tmpUpdatedName = $scope.newUser.username;
		server.updateUser($scope.userDetails.idUser,$scope.newUser);
	}

	$scope.addUser = function(){
		server.addUser($scope.newUser);
	}

	$scope.clearAddUserModal = function(){
		$scope.newUser = {};
		$scope.newUser.firstName = "";
		$scope.newUser.lastName = "";
		$scope.newUser.email = "";
		$scope.newUser.organizations = [];
		$scope.newUser.country = "";
		$scope.newUser.role = { id:7, name:"User"};
		$scope.newUser.concurrentExercisesLimit = 1;
		$scope.newUser.password = "";
		$scope.newUser.passwordRepeat = "";
		$scope.newUser.credits = 10;
		$scope.newUser.emailVerified = true;
		$scope.newUser.forceChangePassword = true;
		$scope.newUser.organization = "";
	}

	$scope.$on('usersList:updated', function(event,data) {
		$scope.masterUsersList = data;
		$scope.filteredUsersList = $filter("filter")($scope.masterUsersList, $scope.query);
	});	
	$scope.usertableconfig = {
			itemsPerPage: 20,
			fillLastPage: false
	}
	$scope.passwordResetValue = "";
	$rootScope.showUserDetails = false;
	$rootScope.showUserList = true;
	$scope.selectedUser = "";

	$scope.updateFilteredList = function() {
		$scope.filteredUsersList = $filter("filter")($scope.masterUsersList, $scope.query);
	};

	$scope.removeFromTeam = function(){
		server.removeFromTeam($scope.userDetails.team.id, $scope.userDetails.user)
	}
	$scope.$on('deletedFromTeam:updated', function(event,data) {
		if(data.result=="success"){
			if($rootScope.visibility.users){
				$scope.getUserDetails($scope.selectedUser);
			}
		}
	})

	$scope.unlockAccount = function(){
		if($scope.selectedUser!="")
			server.unlockUserAccount($scope.selectedUser);
	}
	$scope.resetEmailPassword = function(){
		if($scope.selectedUser!=""){
			server.resetEmailUserPassword($scope.selectedUser);
		}
	}
	$scope.showPasswordResetModal = function(type){
		$scope.resetType = type;
		$('#passwordResetConfirmModal').modal('show');
	}


	$scope.resetPassword = function(){
		if($scope.selectedUser!="" && $scope.passwordResetValue!=""){
			server.resetUserPassword($scope.selectedUser,$scope.passwordResetValue);
			$scope.passwordResetValue="";
		}
	}
	$scope.$on('accountUnlocked:updated', function(event,data) {
		if($scope.selectedUser!="")
			$scope.getUserDetails($scope.selectedUser);
		server.getUsers();
	});
	$scope.back = function(){
		window.history.go(-1);
	}
	$scope.displayUserList = function(){
		$rootScope.showUserList = true;
		$rootScope.showUserDetails = false;
		$location.path("users", false);
	}
	$scope.userDetails = {};
	$scope.getUserDetails = function(username){
		server.getUserDetails(username);
		server.getUserStats(username);
		server.getUserExercises(username);
		server.getUserAchievements(username);
		$location.path("users/details/"+username, false);
	}
	$scope.$on('userCompletedExercises:updated', function(event,data) {
		$scope.scorePerExercises.options = cloneObj($scope.options);
		$scope.scorePerExercises.data = [[],[]];
		$scope.scorePerExercises.series = ["Score","Duration (minutes)"];
		$scope.scorePerExercises.labels = [];
		$scope.scorePerExercises.datasetOverride = [ {showLine: true, pointStyle:'rectRounded',fill: false,
			pointRadius: 10,pointHoverRadius: 15},{ showLine: true,pointStyle:'rectRounded',fill: false,
				pointRadius: 10,pointHoverRadius: 15}] ;
		$scope.scorePerExercises.options.events= ["mousemove", "mouseout", "click", "touchstart", "touchmove", "touchend"];
		$scope.scorePerExercises.options.title.display=false;
		$scope.scorePerExercises.options.tooltips = {
				callbacks: {
					// Use the footer callback to display the sum of the items showing in the tooltip
					afterTitle: function(tooltipItems, data) {
						if(undefined==$scope.scorePerExercises.exercises[tooltipItems[0].index])
							return "";
						return $scope.scorePerExercises.exercises[tooltipItems[0].index];
					}
				},
				footerFontStyle: 'normal'
		};
		$scope.scorePerExercises.options.pieceLabel = {
				// render 'label', 'value', 'percentage' or custom function, default is 'percentage'
				render: 'value',
				// precision for percentage, default is 0
				precision: 0,
				// identifies whether or not labels of value 0 are displayed, default is false
				showZero: true,
				// font size, default is defaultFontSize
				fontSize: 14,
				// font color, can be color array for each data, default is defaultFontColor
				fontColor: '#FFF',
				// font style, default is defaultFontStyle
				fontStyle: 'normal',
				// font family, default is defaultFontFamily
				fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
				// draw label in arc, default is false
				arc: false,
				// position to draw label, available value is 'default', 'border' and 'outside'
				// default is 'default'
				position: 'border',
				// draw label even it's overlap, default is false
				overlap: true
		}
		if(data.result!="error"){
			$scope.scorePerExercises.exercises = [];
			var tmpData = cloneObj(data);
			var l = tmpData.length;
			if(l>20){
				tmpData = tmpData.slice(l - 25);
			}
			for (var property in tmpData) {
				if (tmpData.hasOwnProperty(property)) {
					$scope.scorePerExercises.labels.push(moment(tmpData[property].endTime).format('MMM DD'))
					$scope.scorePerExercises.data[0].push(tmpData[property].score.result);
					$scope.scorePerExercises.data[1].push(tmpData[property].duration);
					$scope.scorePerExercises.exercises.push(tmpData[property].title);


				}
			}
		}
	})
	$scope.$on('statsUser:updated', function(event,data) {

		$scope.remediationRatePerCategory = [];
		for (var property in data.categoriesRemediationRate) {
			if (data.categoriesRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.categoriesRemediationRate[property]).length>0) {
				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.text = property;
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.categoriesRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
				}
				$scope.remediationRatePerCategory.push(obj);
			}
		}
		$scope.remediatedPerCategory.data = [[]];
		$scope.remediatedPerCategory.labels = [];
		$scope.remediatedPerCategory.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerCategory.options.title.display = false;
		$scope.remediatedPerCategory.series = ["Total Time spent (minutes)"];
		$scope.remediatedPerCategory.options.scales = {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
		}
		$scope.remediatedPerCategoryPercentage = cloneObj($scope.remediatedPerCategory);
		$scope.remediatedPerCategoryPercentage.options.scales = {
				display: true,
				ticks: {
					beginAtZero: true,
					min: 0,
					max: 100,
					stepSize: 10
				}
		}
		$scope.remediatedPerCategoryPercentage.series = ["Remediated (%)"];
		for(var j in $scope.remediationRatePerCategory){
			if ($scope.remediationRatePerCategory.hasOwnProperty(j)){
				try{
					var tmpName = $scope.remediationRatePerCategory[j].options.title.text;
					var tmpData = Math.ceil(data.totalMinutesPerIssueCategory[tmpName]);

					var tmpRem = $scope.remediationRatePerCategory[j].data[0]
					var tmpTot = $scope.remediationRatePerCategory[j].data.reduce(getSum);
					var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);

					if(!isNaN(tmpData) && tmpName!= "null"){
						$scope.remediatedPerCategory.labels.push(tmpName);
						$scope.remediatedPerCategory.data[0].push(tmpData);
						$scope.remediatedPerCategoryPercentage.labels.push(tmpName);
						$scope.remediatedPerCategoryPercentage.data[0].push(tmpPercentage);
					}
				}catch(err){}
			}
		}


	});

	$scope.getDateFormat = function(date){
		if(undefined==date || moment(date)._d=="Invalid Date")
			return "Not Available";
		else
			return moment(date).format("D MMMM YYYY");	
	}
	$scope.trophies = [];
	$scope.score = 0;
	$scope.$on('userAchievements:updated', function(event,data) {
		$scope.userAchievements = data;

		$scope.trophies = [];
		$scope.placements = [];
		$scope.certifications = [];

		$scope.emptyTrophy = true;
		$scope.emptyCertification = true;
		$scope.emptyPlacement = true;

		var tmpTrophy = [];
		var tmpPlacement = [];
		var tmpCertification = [];

		for (var i in data.achievements){
			if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="TROPHY"){
				tmpTrophy.push(data.achievements[i]);
				$scope.emptyTrophy = false;
			}
			else if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="CHALLENGE"){
				tmpPlacement.push(data.achievements[i]);
				$scope.emptyPlacement = false;	
			}
			else if(data.achievements.hasOwnProperty(i) && data.achievements[i].achievement.type=="CERTIFICATION"){
				tmpCertification.push(data.achievements[i]);
				$scope.emptyCertification = false;
			}


		}
		if(undefined!=tmpTrophy && tmpTrophy.length>0){
			var size = 5;
			while (tmpTrophy.length > 0){
				$scope.trophies.push(tmpTrophy.splice(0, size));
			}
		}
		if(undefined!=tmpPlacement && tmpPlacement.length>0){
			var size = 5;
			while (tmpPlacement.length > 0){
				$scope.placements.push(tmpPlacement.splice(0, size));
			}
		}
		if(undefined!=tmpCertification && tmpCertification.length>0){
			var size = 4;
			while (tmpCertification.length > 0){
				$scope.certifications.push(tmpCertification.splice(0, size));
			}
		}
	});

	$scope.deleteUserAccount = function(username){
		server.deleteUserAccount(username);
		$('#deleteUserAccountModal').modal('hide');
	}

	$scope.openRemoveModal = function(){
		$('#deleteUserAccountModal').modal('show');
	}

	$scope.$on('userDeleted:updated', function(event,data) {
		server.getUsers();
		$rootScope.showUserDetails = false;
		$rootScope.showUserList = true;
	})

	$scope.userAvailableStatus = ["ACTIVE","INACTIVE","INVITED","CONFIRM_EMAIL","LOCKED","REMOVED"];

	$scope.updateUserStatus = function(){
		server.updateUserStatus($scope.selectedUser,$scope.userDetails.status)
	};

	$scope.$on('userDetails:updated', function(event,data) {
		switch(data.r){
		case -1:
			data.role = "SF Admin";
			break;
		case 0:
			data.role = "Organization Admin";
			break;
		case 3:
			data.role = "Team Manager";
			break;
		case 4:
			data.role = "Stats Analyst";
			break;
		case 7:
			data.role = "User";
			break;
		default:
			data.role = "N/A";
		}
		$scope.selectedUser = data.user;
		$scope.userDetails = data;
		$rootScope.showUserList = false;
		$rootScope.showUserDetails = true;
	});
}])
sf.controller('availableExercises',['$scope','server','$rootScope','$location','$filter','notificationService',function($scope,server,$rootScope,$location,$filter,notificationService){

	$scope.selectedExercises = "";
	$scope.filteredAvailableExercisesList = [];
	$scope.user = server.user;
	$scope.masterAvailableExercisesList = [];
	$scope.exercisesForOrgs = [];
	$scope.availableRegions = [];
	$rootScope.showExerciseList = true;
	$rootScope.showExerciseDetails = false;
	$scope.supportedAwsRegions = server.supportedAwsRegions;
	$scope.definedGateways = server.definedGateways;
	$scope.definedGatewaysRegions = server.definedGatewaysRegions;

	$scope.newExerciseFlagList = true;
	$scope.saveFlow = false;

	var baseTags = ["category","curriculum","cve","cwe","framework","vulnerability","technology"];

	$scope.updateHExercise = function(uuid){
		$location.path("exercise-hub/details/"+uuid,false);
		setTimeout(function(){
			$rootScope.openUpdateInstructionsModal();
		},1000);
	};

	$scope.isExerciseUpdateAvailable = function(exercise){
		if(undefined==exercise || undefined==$rootScope.hubExercises || $rootScope.hubExercises.length == 0)
			return false;
		for(var i in $rootScope.hubExercises){
			if($rootScope.hubExercises.hasOwnProperty(i) && $rootScope.hubExercises[i].uuid == exercise.uuid){
				try{
					if(undefined==exercise.lastUpdate || moment.utc($rootScope.hubExercises[i].lastUpdate.replace("[UTC]","")).format("X")>moment.utc(exercise.lastUpdate.replace("[UTC]","")).format("X")){
						return true;
					}
				}catch(e){
					try{
						if(undefined==exercise.lastUpdate || moment.utc($rootScope.hubExercises[i].lastUpdate).format("X")>moment.utc(exercise.lastUpdate).format("X")){
							return true;
						}
					}catch(e2){}

				}
				try{
					if(moment.utc($rootScope.hubExercises[i].image.updateDate.replace("[UTC]","")).format("X")>moment.utc(exercise.image.updateDate.replace("[UTC]","")).format("X")){
						return true;
					}

				}catch(e){
					try{
						if(moment.utc($rootScope.hubExercises[i].image.updateDate).format("X")>moment.utc(exercise.image.updateDate).format("X")){
							return true;
						}
					}catch(e2){}

				}
				return false;
			}
		}
		return false;
	}

	$scope.loadTags = function(query) {
		var search_term = query.toUpperCase();
		$scope.matchingTags = [];
		angular.forEach(baseTags, function(item) {
			if (item.toUpperCase().indexOf(search_term) >= 0)
				$scope.matchingTags.push(item);

		});

		return $scope.matchingTags;
	};

	$scope.qualityList = ["0","1","2","3","4"]

	$scope.getNameFromQualityId = function(id){
		switch(id){
		case '0':
			return "Hub Default";
		case '1':
			return "Hub Vetted";
		case '2':
			return "SF Default";
		case '3':
			return "SF Vetted";
		case '4':
			return "SF Custom";
		}
	}

	function initEmptyExercise(){

		$scope.tmpNewExercise = {}
		$scope.tmpNewExercise.solution = { text : ""};
		$scope.tmpNewExercise.information = { text : ""};
		$scope.tmpNewExercise.ignoreDefaultStack = false;
		$scope.tmpNewExercise.difficulty = "";
		$scope.tmpNewExercise.status = "";
		$scope.tmpNewExercise.technology = "";
		$scope.tmpNewExercise.framework = "";
		$scope.tmpNewExercise.variant = "";
		$scope.tmpNewExercise.duration = "";
		$scope.tmpNewExercise.description = "";
		$scope.tmpNewExercise.flags = []
		$scope.tmpNewExercise.subtitle = "";
		$scope.tmpNewExercise.title = "";
		$scope.tmpNewExercise.author = ""; 
		$scope.tmpNewExercise.exerciseType = ""; 
		$scope.tmpNewExercise.tags = [];
		$scope.tmpNewExercise.quality = "";

		$scope.tmpNewExercise.trophyName = ""; 

		$scope.tmpFlag = {};
		$scope.tmpFlag.title = "";
		$scope.tmpFlag.selfCheck = { name: "", messageMappings: {}, statusMapping: {}};
		$scope.tmpFlag.selfCheckAvailable = false;
		$scope.tmpFlag.hint = { md: { text : ""}, type: "", scoreReducePercentage: ""};
		$scope.tmpFlag.hintAvailable  = false;
		$scope.tmpFlag.maxScore = 0;
		$scope.tmpFlag.optional = "";
		$scope.tmpFlag.type = "";
		$scope.tmpFlag.md = {};
		$scope.tmpFlag.md.text = "";
		$scope.tmpFlag.KBItem = "";


		$scope.tmpCheckerStatus = "";
		$scope.tmpCheckerMapping = "";
		$scope.tmpCheckerMessage = "";
		$scope.tmpSelfCheckMappings = [];

	}
	initEmptyExercise()
	$scope.tmpExerciseImage = {};

	

	var defaultMessages = {
			"Not Vulnerable":'<span style="color:#3c763d">**Congratulations!**</span> Your answer is correct. The vulnerability has been fixed!',
			'Not Addressed':'<span style="color:#e11d21">**Oops!** </span> The issue has not been addressed. Please follow the instructions.',
			'Vulnerable':'<span style="color:#e11d21">**Oops!** </span> Your answer is not correct. Please try again.',
			'Broken Functionality':'<span style="color:#e11d21">**Oops!** </span>  The functionality is not working anymore, fix it or restore the application\'s source code to its initial state.',
			'Partially Remediated':'<span style="color:#efab53">**Oops!** </span> You are getting closer! The issue has been partially remediated.'
	}

	$scope.updateDefaultMessage = function(){
		var res = defaultMessages[$scope.tmpCheckerMapping.name];
		if(undefined!=res){
			$scope.tmpCheckerMessage =  res;
			checkerMessageEditor.setMarkdown($scope.tmpCheckerMessage)
		}
	}
	var checkerMessageEditor;
	$scope.addTmpSelfCheckMapping = function(){

		for(var i in $scope.tmpSelfCheckMappings){
			if($scope.tmpSelfCheckMappings[i].status ==  $scope.tmpCheckerStatus){
				notificationService.notice("There is already a mapping with the same Self-Check status.")
				return;
			}
		}
		var obj = {};
		obj.status = $scope.tmpCheckerStatus;
		obj.mapping = $scope.tmpCheckerMapping
		obj.message = $scope.tmpCheckerMessage;
		$scope.tmpSelfCheckMappings.push(obj);
		$scope.tmpCheckerStatus = "";
		$scope.tmpCheckerMapping = "";
		$scope.tmpCheckerMessage = "";
		document.querySelector('#tmpCheckerMessage').innerHTML = "";
		checkerMessageEditor = new toastui.Editor({
			el: document.querySelector('#tmpCheckerMessage'),
			height: '150px',
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			usageStatistics:false,
			initialValue: "",
			initialEditType: 'markdown',
			hideModeSwitch: true,
			events: {
				change: function() {
					$scope.tmpCheckerMessage = checkerMessageEditor.getMarkdown();
				},
			}
		}); 

	};

	$scope.deleteTmpSelfCheckMapping = function(item){
		$scope.tmpSelfCheckMappings = $scope.tmpSelfCheckMappings.filter(function(mapping, index, arr){
			return !(mapping.status == item.status && mapping.id == item.id)
		});
	};

	$rootScope.loadMarkdown = function(id,text){
		window.setTimeout(function(){
			document.querySelector('#'+id).innerHTML = "";
			var editor = new toastui.Editor.factory({
				el: document.querySelector('#'+id),
				height: '150px',
				usageStatistics:false,
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				initialValue: text,
				viewer:true
			}); 
			autoplayMdVideo('#'+id);
			$('#'+id).find('a').each(function() {
				if(this.href==undefined || this.href=="")
					return;
				if(this.href.indexOf("#exerciseKB")>=0){
					$(this).replaceWith($(this).text());
				}
			});
		},150);
	}


	$scope.$on('exerciseDefaultScoreUpdated:updated', function(event,data) {
		server.getExerciseDetails($rootScope.exerciseDetails.uuid);
	});

	$scope.enableAutomatedScoringForExercise = function(id){
		server.updateAutomatedScoringForExercise(id,0)
	}

	$scope.disableAutomatedScoringExercise = function(id){
		server.updateAutomatedScoringForExercise(id,2)
	}

	$rootScope.exerciseStatusList = ["AVAILABLE","COMING SOON","INACTIVE"]
	$rootScope.difficultyLevelList = ["Easy","Moderate","Hard"];
	$rootScope.technologyList = [];
	$rootScope.variantList = [];
	$rootScope.exerciseTypes = ["TRAINING","CHALLENGE","BOTH"]
	$rootScope.flagTypes = ["REMEDIATION","EXPLOITATION","SECURE CODING","OTHER"]

	$scope.updateDefaultStackInfoMD = function(){

		if(undefined!=$scope.tmpNewExercise.technology && $scope.tmpNewExercise.technology != ""){
			for(var i in $rootScope.stackItems){
				if($rootScope.stackItems[i].technology == $scope.tmpNewExercise.technology){
					server.loadStackKB($rootScope.stackItems[i].uuid);
				}
			}
			$rootScope.variantList = [];
			$rootScope.variantList.push("Default")
			for(var i in $rootScope.stackItems){
				if($rootScope.stackItems[i].technology == $scope.tmpNewExercise.technology && $rootScope.stackItems[i].variant != undefined && $rootScope.variantList.indexOf($rootScope.stackItems[i].variant)==-1)
					$rootScope.variantList.push($rootScope.stackItems[i].variant)
			}
		}
	}

	$scope.updateStackInfoAndVariant = function(){
		if(undefined!=$scope.tmpNewExercise.technology && $scope.tmpNewExercise.technology != ""){
			for(var i in $rootScope.stackItems){
				if($rootScope.stackItems[i].technology == $scope.tmpNewExercise.technology){
					if(($scope.tmpNewExercise.variant == "Default" && undefined == $rootScope.stackItems[i].variant) || ($rootScope.stackItems[i].variant == $scope.tmpNewExercise.variant)){
						server.loadStackKB($rootScope.stackItems[i].uuid);
						return;
					}
				}
			}
		}
	}
	$scope.$on('stackKBLoaded:updated', function(event,data) {
		if($rootScope.visibility.availableExercises){
			document.querySelector('#EDStackMDViewer').innerHTML = "";
			var editor = new toastui.Editor.factory({
				el: document.querySelector('#EDStackMDViewer'),
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				height: '405px',
				usageStatistics:false,
				viewer: true,
				initialValue: data.md.text
			});
			$scope.tmpNewExercise.stack = data;
			autoplayMdVideo('#EDStackMDViewer')
			var kbFound = false;
			for(var j=0; j<$rootScope.stackItems.length; j++){
				if($rootScope.stackItems[j].uuid == data.uuid){
					$rootScope.stackItems[j] = data;
					kbFound = true;
					break;
				}
			}
			if(!kbFound){
				$rootScope.stackItems.push(data)
			}
		}
	});

	$scope.showAddExerciseModal = function(){
		$scope.saveFlow = false;
		$scope.ignoreDefaultStack = false;
		document.querySelector('#solutionMDEditor').innerHTML = "";
		var solutionMDEditor = new toastui.Editor({
			el: document.querySelector('#solutionMDEditor'),
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			height: '405px',
			usageStatistics:false,
			initialValue: $scope.tmpNewExercise.solution.text,
			initialEditType: 'markdown',
			hideModeSwitch: true,
			events: {
				change: function() {
					$scope.tmpNewExercise.solution.text = solutionMDEditor.getMarkdown();
				},
			}
		});
		document.querySelector('#EDInfoMDEditor').innerHTML = "";
		var infoMDEditor = new toastui.Editor({
			el: document.querySelector('#EDInfoMDEditor'),
			plugins: [ codeSyntaxHightlight, colorSyntax ],
			height: '300px',
			usageStatistics:false,
			initialEditType: 'markdown',
			hideModeSwitch: true,
			initialValue: "",
			events: {
				change: function() {
					$scope.tmpNewExercise.information.text = infoMDEditor.getMarkdown();
				},
			}
		});
		$('#addNewExerciseModal').modal('show');
	}

	$rootScope.getStatusCodeFromText = function(text){
		if(text!=undefined && text!="")
			text = text.toLowerCase();
		else
			return ""
			switch(text){
			case 'available':
				return 0;
			case 'coming soon':
				return 2;
			case 'inactive':
				return 3;
			}
	}	

	$scope.updateExercise = function(){

		if($scope.tmpNewExercise.solution.text == "") {
			PNotify.removeAll();
			notificationService.notice('Please provide exercise solution.');
			return;
		}
		if($scope.tmpNewExercise.information.text == "" && $scope.tmpNewExercise.ignoreDefaultStack == true){
			PNotify.removeAll();
			notificationService.notice('Please exercise info or choose default technology stack info.');
			return;
		}
		if($scope.tmpNewExercise.flags.length==0){
			PNotify.removeAll();
			notificationService.notice('Please define at list one exercise flag.');
			return;

		}
		if($scope.tmpNewExercise.difficulty == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s difficulty.');
			return;
		}
		if($scope.tmpNewExercise.status == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s status.');
			return;
		}
		if($scope.tmpNewExercise.technology == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s technology.');
			return;
		}
		if($scope.tmpNewExercise.duration == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s duration.');
			return;
		}
		if($scope.tmpNewExercise.author == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s author.');
			return;
		}
		if($scope.tmpNewExercise.exerciseType == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s type.');
			return;
		}
		if($scope.tmpNewExercise.description == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s description.');
			return;
		}
		if($scope.tmpNewExercise.subtitle == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s topics.');
			return;
		}
		if($scope.tmpNewExercise.title == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s title.');
			return;
		}
		if($scope.tmpNewExercise.trophyName == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide trophy\'s title.');
			return;
		}

		server.updateExercise($scope.tmpNewExercise);

	}


	$scope.addNewExercise = function(){

		if($scope.tmpNewExercise.solution.text == "") {
			PNotify.removeAll();
			notificationService.notice('Please provide exercise solution.');
			return;
		}
		if($scope.tmpNewExercise.information.text == "" && $scope.tmpNewExercise.ignoreDefaultStack == true){
			PNotify.removeAll();
			notificationService.notice('Please exercise info or choose default technology stack info.');
			return;
		}
		if($scope.tmpNewExercise.flags.length==0){
			PNotify.removeAll();
			notificationService.notice('Please define at list one exercise flag.');
			return;
		}
		if($scope.tmpNewExercise.difficulty == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s difficulty.');
			return;
		}
		if($scope.tmpNewExercise.status == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s status.');
			return;
		}
		if($scope.tmpNewExercise.technology == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s technology.');
			return;
		}
		if($scope.tmpNewExercise.duration == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s duration.');
			return;
		}
		if($scope.tmpNewExercise.author == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s author.');
			return;
		}
		if($scope.tmpNewExercise.exerciseType == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s type.');
			return;
		}
		if($scope.tmpNewExercise.description == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s description.');
			return;
		}
		if($scope.tmpNewExercise.subtitle == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s topics.');
			return;
		}
		if($scope.tmpNewExercise.title == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide exercise\'s title.');
			return;
		}
		if($scope.tmpNewExercise.trophyName == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide trophy\'s title.');
			return;
		}

		server.addExercise($scope.tmpNewExercise);
	}

	$scope.$on('exerciseRemoved:updated', function(event,data) {
		server.getAvailableExercises();
	});
	
	//TODO
	$scope.editExercise = function(){
		try{
			$scope.saveFlow = true;

			$scope.tmpNewExercise.id = $rootScope.exerciseDetails.id;
			$scope.tmpNewExercise.difficulty = $rootScope.exerciseDetails.difficulty;
			$scope.tmpNewExercise.status = $scope.getExerciseStatusString($rootScope.exerciseDetails.status).toUpperCase();
			$scope.tmpNewExercise.technology = $rootScope.exerciseDetails.technology;
			$scope.tmpNewExercise.duration = $rootScope.exerciseDetails.duration;
			$scope.tmpNewExercise.description =$rootScope.exerciseDetails.description;
			$scope.tmpNewExercise.subtitle = $rootScope.exerciseDetails.subtitle;
			$scope.tmpNewExercise.exerciseType = $rootScope.exerciseDetails.exerciseType;
			$scope.tmpNewExercise.author = $rootScope.exerciseDetails.author;
			$scope.tmpNewExercise.title = $rootScope.exerciseDetails.title;
			$scope.tmpNewExercise.trophyName = $rootScope.exerciseDetails.trophyName;  
			$scope.tmpNewExercise.information = $rootScope.exerciseDetails.information;
			$scope.tmpNewExercise.ignoreDefaultStack = $rootScope.exerciseDetails.ignoreDefaultStack;
			$scope.tmpNewExercise.stack = $rootScope.exerciseDetails.stack;
			$scope.tmpNewExercise.tags = $rootScope.exerciseDetails.tags;
			$scope.tmpNewExercise.quality = $rootScope.exerciseDetails.quality;
			$scope.tmpNewExercise.framework = $rootScope.exerciseDetails.framework;
			if($scope.tmpNewExercise.framework==undefined || $scope.tmpNewExercise.framework == "")
				$scope.tmpNewExercise.framework = "Default";
			document.querySelector('#solutionMDEditor').innerHTML = "";

			if(undefined!=$scope.tmpNewExercise.technology && $scope.tmpNewExercise.technology != ""){
				$rootScope.variantList = [];
				$rootScope.variantList.push("Default")
				for(var i in $rootScope.stackItems){
					if($rootScope.stackItems[i].technology == $scope.tmpNewExercise.technology && $rootScope.stackItems[i].variant != undefined && $rootScope.variantList.indexOf($rootScope.stackItems[i].variant)==-1)
						$rootScope.variantList.push($rootScope.stackItems[i].variant)
				}
			}

			$scope.tmpNewExercise.variant = $rootScope.exerciseDetails.stack.variant;
			if($scope.tmpNewExercise.variant==undefined || $scope.tmpNewExercise.variant == "")
				$scope.tmpNewExercise.variant = "Default";

			var sol = "";
			if(undefined != $scope.exerciseDetails.solution && undefined != $scope.exerciseDetails.solution.text){
				sol = $scope.exerciseDetails.solution.text;
				$scope.tmpNewExercise.solution = $scope.exerciseDetails.solution;
			}
			else{
				$scope.tmpNewExercise.solution = { text : ""};
			}

			document.querySelector('#EDStackMDViewer').innerHTML = "";
			var stackViewer = new toastui.Editor.factory({
				el: document.querySelector('#EDStackMDViewer'),
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				height: '405px',
				usageStatistics:false,
				viewer: true,
				initialValue: $scope.tmpNewExercise.stack.md.text
			});
			autoplayMdVideo('#EDStackMDViewer')

			var solutionMDEditor = new toastui.Editor({
				el: document.querySelector('#solutionMDEditor'),
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				height: '405px',
				initialEditType: 'markdown',
				hideModeSwitch: true,
				usageStatistics:false,
				initialValue: '',
				events: {
					change: function() {
						$scope.tmpNewExercise.solution.text = solutionMDEditor.getMarkdown();
					}
				}
			});

			var info = "";
			if(undefined != $scope.tmpNewExercise.information && undefined != $scope.tmpNewExercise.information.text)
				info = $scope.tmpNewExercise.information.text;
			else
				$scope.tmpNewExercise.information = { text : ""};

			document.querySelector('#EDInfoMDEditor').innerHTML = "";
			var infoMDEditor = new toastui.Editor({
				el: document.querySelector('#EDInfoMDEditor'),
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				height: '300px',
				usageStatistics:false,
				initialEditType: 'markdown',
				hideModeSwitch: true,
				initialValue: '',
				events: {
					change: function() {
						$scope.tmpNewExercise.information.text = infoMDEditor.getMarkdown();
					},
				}
			});

			$scope.tmpNewExercise.flags = $rootScope.exerciseDetails.flags;
			$scope.newExerciseFlagList = true;
			$('#addNewExerciseModal').modal('show');
			solutionMDEditor.setMarkdown(sol);
			infoMDEditor.setMarkdown(info);

		}catch(e){
			PNotify.removeAll();
			notificationService.notice('An error occured, please try again.');
		}
	}
	$scope.$on('exerciseUpdated:updated', function(event,data) {

		$rootScope.showExerciseDetails = false;
		$rootScope.showExerciseList = true;
		$location.path("available-exercises", false);

		server.getAvailableExercises();

		initEmptyExercise();

		$scope.newExerciseFlagList = true;
		$scope.saveFlow = false;

		$('#addNewExerciseModal').modal('hide');

	});

	$scope.clearExerciseModal = function(){

		initEmptyExercise();
		$scope.newExerciseFlagList = true;
		$scope.saveFlow = false;
	}


	$scope.$on('exerciseAdded:updated', function(event,data) {

		$scope.newExerciseFlagList = true;
		$scope.saveFlow = false;
		initEmptyExercise();
		$('#addNewExerciseModal').modal('hide');
		server.getAvailableExercises();
		if(undefined!=$scope.tmpExerciseImage && undefined!=$scope.tmpExerciseImage.url){
			$scope.tmpExerciseImage.idExercise = data.id;
			$('#importTaskDefinitionModal').modal('show');
		}

	});

	$scope.importTaskDefinition = function(){

		for(var i=0;i<$scope.tmpExerciseImage.deployTo.length;i++){

			$scope.newTaskDefinition.exerciseId = $scope.tmpExerciseImage.idExercise
			$scope.newTaskDefinition.region = $scope.tmpExerciseImage.deployTo[i];
			$scope.newTaskDefinition.softMemory = $scope.tmpExerciseImage.soft;
			$scope.newTaskDefinition.hardMemory = $scope.tmpExerciseImage.hard;
			$scope.newTaskDefinition.status = true;
			$scope.newTaskDefinition.imageUrl = $scope.tmpExerciseImage.url;
			$scope.newTaskDefinition.containerName = $scope.tmpExerciseImage.name;
			$scope.newTaskDefinition.taskDefinitionName = $scope.tmpExerciseImage.family;
			if($scope.tmpExerciseImage.family.indexOf(':')>-1)
				$scope.newTaskDefinition.taskDefinitionName = $scope.tmpExerciseImage.family.substr(0,$scope.tmpExerciseImage.family.indexOf(':'));
			let obj1 = cloneObj($scope.newTaskDefinition)
			server.addTaskDefinition(obj1);

		}
		$('#importTaskDefinitionModal').modal('hide');
	}

	$scope.tmpExerciseImage.deployTo = [];
	$scope.toggleDeployToSelection = function(reg) {
		var idx = $scope.tmpExerciseImage.deployTo.indexOf(reg);
		if (idx > -1) {
			$scope.tmpExerciseImage.deployTo.splice(idx, 1);
		}
		else {
			$scope.tmpExerciseImage.deployTo.push(reg);
		}
	};

	$scope.removeNewFlag = function(t,c){
		for(var i=0; i<$scope.tmpNewExercise.flags.length; i++){
			if($scope.tmpNewExercise.flags[i].title == t && $scope.tmpNewExercise.flags[i].category == c){
				$scope.tmpNewExercise.flags.remove(i,i);
				return;
			}
		}
	}

	$scope.addNewFlag = function(){

		if($scope.tmpFlag.hintAvailable){
			if($scope.tmpFlag.hint.scoreReducePercentage==null){
				PNotify.removeAll();
				notificationService.notice('Please provide score reduction for hint');
				return;
			}
			try{
				hintReduction = parseInt($scope.tmpFlag.hint.scoreReducePercentage);
			}catch(e){
				PNotify.removeAll();
				notificationService.notice('Please provide score reduction for hint');
				return;
			}
		}
		if($scope.tmpFlag.hintAvailable && $scope.tmpFlag.hint.md.text==""){
			PNotify.removeAll();
			notificationService.notice('Please provide text for hint');
			return;
		}
		if($scope.tmpFlag.md.text == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide text for instructions');
			return;
		}
		if($scope.tmpFlag.selfCheckAvailable && $scope.tmpFlag.selfCheck.name==""){
			PNotify.removeAll();
			notificationService.notice('Please provide self-check identifier');
			return;
		}
		if($scope.tmpFlag.selfCheckAvailable && $scope.tmpSelfCheckMappings.length<2){
			PNotify.removeAll();
			notificationService.notice('Please define at least one status mapping to "Vulnerable" and one to "Not Vulnerable"');
			return;
		}

		var obj2 = {};
		var obj = {};

		obj.title = $scope.tmpFlag.title;
		obj.kb = $scope.tmpFlag.KBItem;
		obj.category = $scope.tmpFlag.KBItem.category;
		obj.flagList = [];

		obj2.selfCheck = $scope.tmpFlag.selfCheck;
		if(undefined==obj2.selfCheck)
			obj2.selfCheck = {};
		obj2.selfCheck.messageMappings = {};
		obj2.selfCheck.statusMapping = {};

		for(var i=0;i<$scope.tmpSelfCheckMappings.length;i++){
			obj2.selfCheck.messageMappings[$scope.tmpSelfCheckMappings[i].status] = $scope.tmpSelfCheckMappings[i].message;
			obj2.selfCheck.statusMapping[$scope.tmpSelfCheckMappings[i].status] = $scope.tmpSelfCheckMappings[i].mapping;
		}
		obj2.selfCheckAvailable = $scope.tmpFlag.selfCheckAvailable;
		obj2.type = $scope.tmpFlag.type;
		obj2.hint = $scope.tmpFlag.hint;
		obj2.hint.type = obj2.type;
		obj2.hintAvailable = $scope.tmpFlag.hintAvailable;
		obj2.md = {};
		obj2.md.text = $scope.tmpFlag.md.text;
		obj2.optional = $scope.tmpFlag.optional;
		if(obj2.optional)
			obj2.maxScore = 0;
		else
			obj2.maxScore = $scope.tmpFlag.maxScore;

		obj.flagList.push(obj2);
		$scope.tmpNewExercise.flags.push(obj);

		$scope.newExerciseFlagList = true;

		$scope.tmpFlag = {};
		$scope.tmpFlag.title = "";
		$scope.tmpFlag.selfCheck = { name: "", messageMappings: {}, statusMapping: {}};
		$scope.tmpFlag.selfCheckAvailable = false;
		$scope.tmpFlag.hint = { md: { text : ""}, type: "", scoreReducePercentage: ""};
		$scope.tmpFlag.hintAvailable  = false;
		$scope.tmpFlag.maxScore = "";
		$scope.tmpFlag.optional = "";
		$scope.tmpFlag.type = "";
		$scope.tmpFlag.md = {};
		$scope.tmpFlag.md.text = "";
		$scope.tmpFlag.KBItem = "";
		$scope.tmpCheckerStatus = "";
		$scope.tmpCheckerMapping = "";
		$scope.tmpCheckerMessage = "";
		$scope.tmpSelfCheckMappings = [];

	}

	getObjectFromStatus = function(status){
		for(var i in $rootScope.vulnerabilityStatus){
			if ($rootScope.vulnerabilityStatus[i].id == status){
				return $rootScope.vulnerabilityStatus[i];
			}
		}
	}
	$scope.updateNewFlag = function(){

		if($scope.tmpFlag.hintAvailable){
			if($scope.tmpFlag.hint.scoreReducePercentage==null){
				PNotify.removeAll();
				notificationService.notice('Please provide score reduction for hint');
				return;
			}
			try{
				hintReduction = parseInt($scope.tmpFlag.hint.scoreReducePercentage);
			}catch(e){
				PNotify.removeAll();
				notificationService.notice('Please provide score reduction for hint');
				return;
			}
		}
		if($scope.tmpFlag.hintAvailable && $scope.tmpFlag.hint==""){
			PNotify.removeAll();
			notificationService.notice('Please provide text for hint');
			return;
		}
		if($scope.tmpFlag.md.text == ""){
			PNotify.removeAll();
			notificationService.notice('Please provide text for instructions');
			return;
		}
		if($scope.tmpFlag.selfCheckAvailable && $scope.tmpFlag.selfCheck.name==""){
			PNotify.removeAll();
			notificationService.notice('Please provide self-check identifier');
			return;
		}
		if($scope.tmpFlag.selfCheckAvailable && $scope.tmpSelfCheckMappings.length<2){
			PNotify.removeAll();
			notificationService.notice('Please define at least one status mapping to "Vulnerable" and one to "Not Vulnerable"');
			return;
		}

		var obj2 = {};
		var obj = {};

		obj.title = $scope.tmpFlag.title;
		obj.kb = $scope.tmpFlag.KBItem;
		obj.id = $scope.tmpFlag.id;
		obj.category = $scope.tmpFlag.KBItem.category
		obj.flagList = [];

		obj2.selfCheckAvailable = $scope.tmpFlag.selfCheckAvailable;

		if(obj2.selfCheckAvailable){
			obj2.selfCheck = $scope.tmpFlag.selfCheck;
			obj2.selfCheck.messageMappings = {}
			obj2.selfCheck.statusMapping = {}

			for(var i=0;i<$scope.tmpSelfCheckMappings.length;i++){
				obj2.selfCheck.messageMappings[$scope.tmpSelfCheckMappings[i].status] = $scope.tmpSelfCheckMappings[i].message;
				obj2.selfCheck.statusMapping[$scope.tmpSelfCheckMappings[i].status] = $scope.tmpSelfCheckMappings[i].mapping.id;
			}
		}
		obj2.type = $scope.tmpFlag.type;

		obj2.hintAvailable = $scope.tmpFlag.hintAvailable;
		if(obj2.hintAvailable){
			obj2.hint = $scope.tmpFlag.hint;
			obj2.hint.type = obj2.type;
		}
		obj2.md = {};
		obj2.md.text = $scope.tmpFlag.md.text;
		obj2.optional = $scope.tmpFlag.optional;
		if(obj2.optional)
			obj2.maxScore = 0;
		else
			obj2.maxScore = $scope.tmpFlag.maxScore;

		obj.flagList.push(obj2);

		for(var j in $scope.tmpNewExercise.flags){
			if($scope.tmpNewExercise.flags[j].id == $scope.tmpFlag.id){
				$scope.tmpNewExercise.flags[j] = obj;
			}
		}

		$scope.newExerciseFlagList = true;

		$scope.tmpFlag = {};
		$scope.tmpFlag.title = "";
		$scope.tmpFlag.selfCheck = { name: "", messageMappings: {}, statusMapping: {}};
		$scope.tmpFlag.selfCheckAvailable = false;
		$scope.tmpFlag.hint = { md: { text : ""}, type: "", scoreReducePercentage: ""};
		$scope.tmpFlag.hintAvailable  = false;
		$scope.tmpFlag.maxScore = "";
		$scope.tmpFlag.optional = "";
		$scope.tmpFlag.type = "";
		$scope.tmpFlag.md = {};
		$scope.tmpFlag.md.text = "";
		$scope.tmpFlag.KBItem = "";
		$scope.tmpCheckerStatus = "";
		$scope.tmpCheckerMapping = "";
		$scope.tmpCheckerMessage = "";
		$scope.tmpSelfCheckMappings = [];
	}
	$scope.flagSaveFlow = false;
	$scope.editNewFlagDialog = function(flag){
		if($scope.newExerciseFlagList){
			$scope.newExerciseFlagList = false;
			$scope.flagSaveFlow = true;
			$scope.tmpFlag = {};
			$scope.tmpFlag.id = flag.id
			$scope.tmpFlag.title = flag.title;
			$scope.tmpFlag.selfCheck = flag.flagList[0].selfCheck
			$scope.tmpFlag.selfCheckAvailable = flag.flagList[0].selfCheckAvailable
			$scope.tmpFlag.hint = flag.flagList[0].hint;
			$scope.tmpFlag.hintAvailable  = flag.flagList[0].hintAvailable;
			$scope.tmpFlag.maxScore = flag.flagList[0].maxScore;
			$scope.tmpFlag.optional = flag.flagList[0].optional;
			$scope.tmpFlag.type = flag.flagList[0].type;
			$scope.tmpFlag.md = {};
			$scope.tmpFlag.md.text = flag.flagList[0].md.text;
			$scope.tmpFlag.KBItem = flag.kb;
			$scope.tmpCheckerStatus = "";
			$scope.tmpCheckerMapping = "";
			$scope.tmpCheckerMessage = "";
			$scope.tmpSelfCheckMappings = [];
			if($scope.tmpFlag.selfCheckAvailable){
				for(var i in $scope.tmpFlag.selfCheck.statusMapping){
					if ($scope.tmpFlag.selfCheck.statusMapping.hasOwnProperty(i)){
						var obj = {};
						obj.status = i;
						obj.mapping = getObjectFromStatus($scope.tmpFlag.selfCheck.statusMapping[i]);
						obj.message = $scope.tmpFlag.selfCheck.messageMappings[i];
						$scope.tmpSelfCheckMappings.push(obj);
					}
				}

			}

			document.querySelector('#tmpFlagInstructionsMD').innerHTML = "";
			var instructionsEditor = new toastui.Editor({
				el: document.querySelector('#tmpFlagInstructionsMD'),
				height: '220px',
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				initialValue: ' ',
				initialEditType: 'markdown',
				hideModeSwitch: true,
				usageStatistics:false,
				events: {
					change: function() {
						$scope.tmpFlag.md.text = instructionsEditor.getMarkdown();
					},
				}
			}); 
			document.querySelector('#tmpFlagHint').innerHTML = "";
			if($scope.tmpFlag.hint == undefined || $scope.tmpFlag.hint.md == undefined || $scope.tmpFlag.hint.md.text == undefined){
				$scope.tmpFlag.hint = {};
				$scope.tmpFlag.hint.md = {}
				$scope.tmpFlag.hint.md.text = "";
			}
			var hintEditor = new toastui.Editor({
				el: document.querySelector('#tmpFlagHint'),
				height: '150px',
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				usageStatistics:false,
				initialValue: ' ',
				events: {
					change: function() {
						$scope.tmpFlag.hint.md.text = hintEditor.getMarkdown();
					},
				}
			}); 

			document.querySelector('#tmpCheckerMessage').innerHTML = "";
			checkerMessageEditor = new toastui.Editor({
				el: document.querySelector('#tmpCheckerMessage'),
				height: '150px',
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				usageStatistics:false,
				initialEditType: 'markdown',
				hideModeSwitch: true,
				initialValue: "",
				events: {
					change: function() {
						$scope.tmpCheckerMessage = checkerMessageEditor.getMarkdown();
					},
				}
			}); 
			if($scope.tmpFlag.hintAvailable)
				hintEditor.setMarkdown($scope.tmpFlag.hint.md.text)
			instructionsEditor.setMarkdown($scope.tmpFlag.md.text);
		}
		else{
			$scope.newExerciseFlagList = true;
		}
	}

	$scope.addNewFlagDialog = function(){
		if($scope.newExerciseFlagList){
			$scope.newExerciseFlagList = false;
			$scope.flagSaveFlow = false;
			document.querySelector('#tmpFlagInstructionsMD').innerHTML = "";
			$scope.tmpFlag.md = {};
			$scope.tmpFlag.md.text = "";
			$scope.tmpFlag.hint = {};
			$scope.tmpFlag.hint.md = {};
			$scope.tmpFlag.hint.md.text = "";
			
			var instructionsEditor = new toastui.Editor({
				el: document.querySelector('#tmpFlagInstructionsMD'),
				height: '220px',
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				initialValue: " ",
				initialEditType: 'markdown',
				hideModeSwitch: true,
				usageStatistics:false,
				events: {
					change: function() {
						$scope.tmpFlag.md.text = instructionsEditor.getMarkdown();
					},
				}
			});

			document.querySelector('#tmpFlagHint').innerHTML = "";
			var hintEditor = new toastui.Editor({
				el: document.querySelector('#tmpFlagHint'),
				height: '150px',
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				usageStatistics:false,
				initialValue: "",
				initialEditType: 'markdown',
				hideModeSwitch: true,
				events: {
					change: function() {
						$scope.tmpFlag.hint.md.text = hintEditor.getMarkdown();
					},
				}
			}); 
			document.querySelector('#tmpCheckerMessage').innerHTML = "";
			checkerMessageEditor = new toastui.Editor({
				el: document.querySelector('#tmpCheckerMessage'),
				height: '150px',
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				usageStatistics:false,
				initialValue: "",
				initialEditType: 'markdown',
				hideModeSwitch: true,
				events: {
					change: function() {
						$scope.tmpCheckerMessage = checkerMessageEditor.getMarkdown();
					},
				}
			}); 
		}
		else{
			$scope.newExerciseFlagList = true;
		}
	}


	$scope.getColorForScore = function(score){
		if(score<=20)
			return "rgba(118, 147, 193, 0.94)";
		if(score<=50)
			return "rgba(158, 74, 114, 0.87)";
		if(score<=75)
			return "rgba(118, 118, 193, 0.94)";
		if(score<=100)
			return "rgba(106, 106, 125, 0.87)";
		if(score<=125)
			return "rgba(56, 31, 8, 0.87)";
		return "rgba(189, 108, 34, 0.87)";
	}

	$scope.newTaskDefinition = {};

	$scope.newTaskDefinition.exerciseId = "";
	$scope.newTaskDefinition.region = "";
	$scope.newTaskDefinition.softMemory = "";
	$scope.newTaskDefinition.hardMemory = "";
	$scope.newTaskDefinition.status = true;
	$scope.newTaskDefinition.imageUrl = "";
	$scope.newTaskDefinition.containerName = "";
	$scope.newTaskDefinition.taskDefinitionName = "";

	$scope.$on('exerciseInRegionEnabled:updated', function(event,data) {
		server.getRegionsForExercise($rootScope.exerciseDetails.uuid);
	});
	$scope.$on('exerciseInRegionDisabled:updated', function(event,data) {
		server.getRegionsForExercise($rootScope.exerciseDetails.uuid);
	});
	$scope.$on('exerciseInRegionRemoved:updated', function(event,data) {
		server.getRegionsForExercise($rootScope.exerciseDetails.uuid);
	});

	$scope.$on('taskDefinitionAdded:updated', function(event,data) {
		$scope.newTaskDefinition.exerciseId = "";
		$scope.newTaskDefinition.region = "";
		$scope.newTaskDefinition.softMemory = "";
		$scope.newTaskDefinition.hardMemory = "";
		$scope.newTaskDefinition.status = true;
		$scope.newTaskDefinition.imageUrl = "";
		$scope.newTaskDefinition.containerName = "";
		$scope.newTaskDefinition.taskDefinitionName = "";
		server.getRegionsForExercise($rootScope.exerciseDetails.uuid);
	});
	$scope.addTaskDefinition = function(){
		$scope.newTaskDefinition.exerciseId = $rootScope.exerciseDetails.id;
		server.addTaskDefinition($scope.newTaskDefinition);
	}

	$scope.getExerciseDetails = function(uuid){
		server.getExerciseDetails(uuid);
		server.getRegionsForExercise(uuid);
	}

	$scope.manageTaskDefinitions = function(){
		$('#addRemoveRegionsModal').modal('show');
	}

	$scope.fillRevisionForm = function(data){
		$scope.newTaskDefinition.exerciseId = $rootScope.exerciseDetails.id;
		var reg = {};
		reg.name = data.region;
		reg.code = regionCodeFromName(reg.name);
		$scope.newTaskDefinition.region = reg.code;
		$scope.newTaskDefinition.softMemory = data.softMemoryLimit;
		$scope.newTaskDefinition.hardMemory = data.hardMemoryLimit;
		$scope.newTaskDefinition.status = true;
		$scope.newTaskDefinition.imageUrl = data.repositoryImageUrl;
		$scope.newTaskDefinition.containerName = data.containerName;
		$scope.newTaskDefinition.taskDefinitionName = data.name.substr(0,data.name.indexOf(':'));
	}



	$scope.clearTaskDefinitionForm = function(){
		$scope.newTaskDefinition.exerciseId = "";
		$scope.newTaskDefinition.region = "";
		$scope.newTaskDefinition.softMemory = "";
		$scope.newTaskDefinition.hardMemory = "";
		$scope.newTaskDefinition.status = true;
		$scope.newTaskDefinition.imageUrl = "";
		$scope.newTaskDefinition.containerName = "";
		$scope.newTaskDefinition.taskDefinitionName = "";
	}
	$scope.enableExerciseForOrg = function(uuid,idOrg){
		server.enableExerciseForOrg(uuid,idOrg);
	}
	$scope.disableExerciseForOrg = function(uuid,idOrg){
		server.disableExerciseForOrg(uuid,idOrg);
	}

	$scope.$on('disableExerciseForOrg:updated', function(event,data) {
		server.getAvailableExercises();
		PNotify.removeAll();
		notificationService.success("Exercise successfully disabled.")
	});
	$scope.$on('enableExerciseForOrg:updated', function(event,data) {
		server.getAvailableExercises();
		PNotify.removeAll();
		notificationService.success("Exercise successfully enabled.")
	});

	$rootScope.regionNameFromCode = function(regionCode){
		var region = "";
		switch(regionCode) {
		case "EU_WEST_1":
			region = "EU (Ireland)";
			break;
		case "US_EAST_1":
			region = "US East (N. Virginia)"
				break;
		case "AP_SOUTH_1":
			region = "Asia Pacific (Mumbai)"
				break;
		case "AP_EAST_1":
			region = "Asia Pacific (Hong Kong)"
				break;
		case "AP_SOUTHEAST_1":
			region = "Asia Pacific (Singapore)"
				break;
		case "US_EAST_2":
			region = "US East (Ohio)";
			break;
		case "US_WEST_2":
			region = "US West (Oregon)";
			break;
		case "US_WEST_1":
			region = "US West (N. California)";
			break;
		case "CA_CENTRAL_1":
			region = "Canada (Central)";
			break;
		case "EU_CENTRAL_1":
			region = "EU (Frankfurt)";
			break;
		case "EU_WEST_2":
			region = "EU (London)";
			break;
		case "EU_WEST_3":
			region = "EU (Paris)";
			break;
		case "EU_NORTH_1":
			region = "EU (Stockholm)";
			break;
		case "ME_SOUTH_1":
			region = "Middle East (Bahrain)";
			break;
		case "AP_NORTHEAST_2":
			region = "Asia Pacific (Seoul)";
			break;
		case "AP_NORTHEAST_1":
			region = "Asia Pacific (Tokyo)";
			break;
		case "AP_SOUTHEAST_2":
			region = "Asia Pacific (Sydney)";
			break;
		case "SA_EAST_1":
			region = "South America (São Paulo)";
			break;
		default:
			break;
		}
		return region;
	}

	function regionCodeFromName(regionName){
		var region = "";
		switch(regionName) {
		case "EU (Ireland)":
			region = "EU_WEST_1";
			break;
		case "US East (N. Virginia)":
			region = "US_EAST_1"
				break;
		case "Asia Pacific (Mumbai)":
			region = "AP_SOUTH_1"
				break;
		case "Asia Pacific (Singapore)":
			region = "AP_SOUTHEAST_1"
				break;
		case "Asia Pacific (Hong Kong)":
			region = "AP_EAST_1"
				break;
		case "US East (Ohio)":
			region = "US_EAST_2";
			break;
		case "US West (Oregon)":
			region = "US_WEST_2";
			break;
		case "US West (N. California)":
			region = "US_WEST_1";
			break;
		case "Canada (Central)":
			region = "CA_CENTRAL_1";
			break;
		case "EU (Frankfurt)":
			region = "EU_CENTRAL_1";
			break;
		case "EU (London)":
			region = "EU_WEST_2";
			break;
		case "EU (Paris)":
			region = "EU_WEST_3";
			break;
		case "EU (Stockholm)":
			region = "EU_NORTH_1";
			break;
		case "Middle East (Bahrain)":
			region = "ME_SOUTH_1";
			break;
		case "Asia Pacific (Seoul)":
			region = "AP_NORTHEAST_2";
			break;
		case "Asia Pacific (Tokyo)":
			region = "AP_NORTHEAST_1";
			break;
		case "Asia Pacific (Sydney)":
			region = "AP_SOUTHEAST_2";
			break;
		case "South America (São Paulo)":
			region = "SA_EAST_1";
			break;

		default:
			break;
		}
		return region;
	}

	$scope.$on('exerciseRegions:updated', function(event,data) {
		if(data!=null){
			$scope.availableRegions = [];
			for(var j in data){
				if(!Number.isInteger(parseInt(j)))
					continue;

				var region = "Unavailable";

				switch(data[j].region) {
				case "EU_WEST_1":
					region = "EU (Ireland)";
					break;
				case "US_EAST_1":
					region = "US East (N. Virginia)"
						break;
				case "AP_SOUTH_1":
					region = "Asia Pacific (Mumbai)"
						break;
				case "AP_EAST_1":
					region = "Asia Pacific (Hong Kong)"
						break;
				case "AP_SOUTHEAST_1":
					region = "Asia Pacific (Singapore)"
						break;
				case "US_EAST_2":
					region = "US East (Ohio)";
					break;
				case "US_WEST_2":
					region = "US West (Oregon)";
					break;
				case "US_WEST_1":
					region = "US West (N. California)";
					break;
				case "CA_CENTRAL_1":
					region = "Canada (Central)";
					break;
				case "EU_CENTRAL_1":
					region = "EU (Frankfurt)";
					break;
				case "EU_WEST_2":
					region = "EU (London)";
					break;
				case "EU_WEST_3":
					region = "EU (Paris)";
					break;
				case "EU_NORTH_1":
					region = "EU (Stockholm)";
					break;
				case "ME_SOUTH_1":
					region = "Middle East (Bahrain)";
					break;
				case "AP_NORTHEAST_2":
					region = "Asia Pacific (Seoul)";
					break;
				case "AP_NORTHEAST_1":
					region = "Asia Pacific (Tokyo)";
					break;
				case "AP_SOUTHEAST_2":
					region = "Asia Pacific (Sydney)";
					break;
				case "SA_EAST_1":
					region = "South America (São Paulo)";
					break;
				default:
					break;
				}
				obj = {}
				obj.containerName = data[j].taskDefinition.containerName;
				obj.taskId = data[j].taskDefinition.id;
				obj.hardMemoryLimit = data[j].taskDefinition.hardMemoryLimit;
				obj.softMemoryLimit = data[j].taskDefinition.softMemoryLimit;
				obj.repositoryImageUrl = data[j].taskDefinition.repositoryImageUrl;
				obj.region = region;
				obj.updateDate = data[j].taskDefinition.updateDate;
				obj.name = data[j].taskDefinition.taskDefinitionName;
				obj.active = data[j].active;
				$scope.availableRegions.push(obj);
			}
		}
	});

	function isKbPresent(kb){
		if(undefined==kb)
			return false;
		for(var j=0; j<$scope.exerciseKbs.length; j++){
			if($scope.exerciseKbs[j].uuid==kb.uuid)
				return true;
		}
		return false;
	}
	$scope.$on('vulnKBLoaded:updated', function(event,data) {

		if($rootScope.visibility.availableExercises){
			var technology = $rootScope.exerciseDetails.technology;
			var cMd = data.md.text+"\n";
			if(data.kbMapping!=undefined && data.kbMapping[technology]!=undefined){
				var tmpInner = data.kbMapping[technology];
				cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
			}
			document.querySelector('#kbMD').innerHTML = '';
			var editor = new toastui.Editor.factory({
				el: document.querySelector('#kbMD'),
				viewer: true,
				usageStatistics:false,
				height: '500px',
				plugins: [ codeSyntaxHightlight, colorSyntax ],
				initialValue: ''
			});
			editor.setMarkdown(cMd);
			$('#kbModal').modal('show');
			autoplayMdVideo('#kbMD')

			var kbFound = false;
			for(var j=0; j<$rootScope.kbItems.length; j++){
				if($rootScope.kbItems[j].uuid == data.uuid){
					$rootScope.kbItems[j] = data;
					kbFound = true;
					break;
				}
			}
			if(!kbFound){
				$rootScope.kbItems.push(data)
			}
		}
	})

	$scope.openVulnKB = function(item,technology){
		for(var j=0; j<$rootScope.kbItems.length; j++){
			if($rootScope.kbItems[j].uuid == item.uuid && undefined != $rootScope.kbItems[j].md.text && $rootScope.kbItems[j].md.text != ""){
				var cMd = $rootScope.kbItems[j].md.text+"\n";
				if($rootScope.kbItems[j].kbMapping!=undefined && $rootScope.kbItems[j].kbMapping[technology]!=undefined){
					var tmpInner = $rootScope.kbItems[j].kbMapping[technology];
					cMd += "\n***\n# "+tmpInner.technology+" #\n"+tmpInner.md.text+"\n";
				}
				else if(item.isAgnostic){
					server.loadVulnKBTechnology(item.uuid,technology)
					return;
				}
				document.querySelector('#kbMD').innerHTML = '';
				var editor = new toastui.Editor.factory({
					el: document.querySelector('#kbMD'),
					viewer: true,
					usageStatistics:false,
					height: '500px',
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					initialValue: ''
				});
				editor.setMarkdown(cMd);
				$('#kbModal').modal('show');
				autoplayMdVideo('#kbMD')
				return;
			}
		}
		server.loadVulnKBTechnology(item.uuid,technology);
	}

	$rootScope.exerciseDetails = [];
	$scope.solutionAvailable = true;
	
	
	
	$scope.$on('exerciseDetails:updated', function(event,data) {

		$rootScope.exerciseDetails = data;
		$scope.exerciseKbs = []; 
		var stackFound = false;
		if(undefined!=$rootScope.exerciseDetails.stack && undefined!=$rootScope.exerciseDetails.stack.md && undefined!=$rootScope.exerciseDetails.stack.md.text){
			for(var k=0; k<$rootScope.stackItems.length; k++){
				if($rootScope.stackItems[k].uuid == $rootScope.exerciseDetails.stack.uuid){
					$rootScope.stackItems[k] = $rootScope.exerciseDetails.stack;
					stackFound = true;
					break;
				}
			}
			if(!stackFound){
				$rootScope.stackItems.push($rootScope.exerciseDetails.stack)
			}
		}
		var kbFound = false;
		for(var i=0; i<$rootScope.exerciseDetails.flags.length; i++){
			if(undefined!=$rootScope.exerciseDetails.flags[i].kb){
				if(!isKbPresent($rootScope.exerciseDetails.flags[i].kb))
					$scope.exerciseKbs.push($rootScope.exerciseDetails.flags[i].kb);
				for(var j=0; j<$rootScope.kbItems.length; j++){
					if($rootScope.kbItems[j].uuid == $rootScope.exerciseDetails.flags[i].kb.uuid && undefined!=$rootScope.exerciseDetails.flags[i].kb.md && undefined!=$rootScope.exerciseDetails.flags[i].kb.md.text){
						$rootScope.kbItems[j] = $rootScope.exerciseDetails.flags[i].kb;
						kbFound = true;
						break;
					}
				}
			}
			
			if(!kbFound){
				$rootScope.kbItems.push(data)
			}

		}	
		try{
			if($rootScope.exerciseDetails.information != undefined){
				document.querySelector("#exerciseInfoMD").innerHTML = "";
				var editor = new toastui.Editor.factory({
					viewer: true,
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					usageStatistics:false,
					el: document.querySelector("#exerciseInfoMD"),
					initialValue: $rootScope.exerciseDetails.information.text
				});
				autoplayMdVideo('#exerciseInfoMD')
			}
		}catch(e){}
		try{
			if($rootScope.exerciseDetails.stack != undefined){
				document.querySelector("#exerciseStackMD").innerHTML = "";
				var editor = new toastui.Editor.factory({
					viewer: true,
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					usageStatistics:false,
					el: document.querySelector("#exerciseStackMD"),
					initialValue: $rootScope.exerciseDetails.stack.md.text
				});
				autoplayMdVideo('#exerciseStackMD')
			}
		}catch(e){}
		try{
			if($rootScope.exerciseDetails.solution != undefined){
				document.querySelector("#solutionMDViewer").innerHTML = "";
				var editor = new toastui.Editor.factory({
					viewer: true,
					plugins: [ codeSyntaxHightlight, colorSyntax ],
					usageStatistics:false,
					el: document.querySelector("#solutionMDViewer"),
					initialValue: $rootScope.exerciseDetails.solution.text
				});
				autoplayMdVideo('#solutionMDViewer')
				$scope.solutionAvailable = true;
			}
		}catch(e){
			$scope.solutionAvailable = false;

		}
		$rootScope.showExerciseDetails = true;
		$rootScope.showExerciseList = false;


		$scope.triggerFlagsMarkdown = function(fq){
			window.setTimeout(function(){
				try{
					if(undefined != fq){
						document.querySelector('#fe'+fq.id+'-md').innerHTML = '';
						var editor = new toastui.Editor.factory({
							el: document.querySelector('#fe'+fq.id+'-md'),
							viewer: true,
							usageStatistics:false,
							plugins: [ codeSyntaxHightlight, colorSyntax ],
							height: '500px',
							initialValue: fq.md.text
						});
						autoplayMdVideo('#fe'+fq.id+'-md')
					}
				}catch(e){}
			},100);
		}

		$location.path("available-exercises/details/"+$rootScope.exerciseDetails.uuid, false);
	});

	$scope.disableExerciseInRegion = function(taskId){
		server.disableExerciseInRegion(taskId);
	}
	$scope.enableExerciseInRegion = function(taskId){
		server.enableExerciseInRegion(taskId);
	}
	$scope.removeExerciseInRegion = function(exerciseId, taskId){
		server.removeExerciseInRegion(exerciseId, taskId);
	}
	var tmpExerciseToRemove = -1;
	$scope.removeExerciseModal = function(id,name){
		tmpExerciseToRemove = id;
		$scope.tmpExerciseToBeRemovedName = name
		$('#removeExerciseModal').modal('show');
	}
	$scope.removeExercise = function(){
		server.removeExercise(tmpExerciseToRemove);
		tmpExerciseToRemove = -1;
		$('#removeExerciseModal').modal('hide');
	}

	$scope.backToList = function(){
		$rootScope.showExerciseList = true;
		$rootScope.showExerciseDetails = false;
	}


	$scope.getExerciseStatusString = function(status){
		switch(status){
		case "0":
			return "Available";
			break;
		case "1":
			return "Download Queue";
			break;
		case "2":
			return "Coming Soon";
			break;
		case "3":
			return "Inactive";
			break;
		default:
			return "N/A";
		}
	}


	$scope.updateAvailableExercisesFilteredList = function() {
		$scope.filteredAvailableExercisesList = $filter("filter")($scope.masterAvailableExercisesList, $scope.queryAvailableExercises);
	};
	$scope.availableExercisestableconfig = {
			itemsPerPage: 20,
			fillLastPage: false
	}
	$scope.$on('availableExercises:updated', function(event,data) {
		$rootScope.availableExercises = data.exercises;
		$scope.masterAvailableExercisesList = data.exercises;
		$scope.exercisesForOrgs = data.orgs;
		$scope.filteredAvailableExercisesList = cloneObj($scope.masterAvailableExercisesList);
	});
/*
	$scope.$on('internetUpdated:updated', function(event,data) {
		$scope.getExerciseDetails($rootScope.exerciseDetails.uuid);
	});

	$scope.$on('clipboardUpdated:updated', function(event,data) {
		$scope.getExerciseDetails($rootScope.exerciseDetails.uuid);
	});

	$scope.enableClipboardForExercise = function(id){
		server.updateExerciseClipboardAccess(id,true);
	};
	$scope.disableClipboardForExercise = function(id){
		server.updateExerciseClipboardAccess(id,false)
	};


	$scope.enableInternetForExercise = function(id){
		server.updateExerciseInternetAccess(id,true);
	};
	$scope.disableInternetForExercise = function(id){
		server.updateExerciseInternetAccess(id,false)
	};
*/
	$scope.getExerciseEnabledForOrgText = function(orgId,uuid){
		if(undefined == orgId || undefined == uuid)
			return false;
		for(var i in $scope.exercisesForOrgs){
			if ($scope.exercisesForOrgs[i].hasOwnProperty("organization") && $scope.exercisesForOrgs[i].organization.id==orgId){
				for(var j in $scope.exercisesForOrgs[i].exercises){
					if($scope.exercisesForOrgs[i].exercises[j].uuid==uuid){
						return true;
					}
				}
				return false;
			}
		}
		return false;
	}

}]);

sf.controller('teams',['$scope','server','$rootScope','$location','$filter','notificationService',function($scope,server,$rootScope,$location,$filter,notificationService){
	$rootScope.showTeamMembers = false;
	$rootScope.showTeamList = true;
	$rootScope.selectedTeamId = "";
	$scope.selectedTeamManagers = [];
	$scope.filteredTeamsList = [];
	$scope.masterTeamsList = [];
	$scope.masterMembersList = [];
	$scope.filteredMembersList = [];

	$scope.userObj = [];
	$scope.selectedUsersList = [];
	$scope.userCheckToggle=function(s){
		if($scope.selectedUsersList.indexOf(s)<0){
			$scope.selectedUsersList.push(s);
		}
		else{
			var idx = $scope.selectedUsersList.indexOf(s);
			$scope.selectedUsersList.remove(idx,idx);
		}
	}

	function regionCodeFromName(regionName){
		var region = "";
		switch(regionName) {
		case "EU (Ireland)":
			region = "EU_WEST_1";
			break;
		case "US East (N. Virginia)":
			region = "US_EAST_1";
				break;
		case "Asia Pacific (Mumbai)":
			region = "AP_SOUTH_1";
				break;
		case "Asia Pacific (Singapore)":
			region = "AP_SOUTHEAST_1";
				break;
		case "Asia Pacific (Hong Kong)":
			region = "AP_EAST_1";
				break;
		case "US East (Ohio)":
			region = "US_EAST_2";
			break;
		case "US West (Oregon)":
			region = "US_WEST_2";
			break;
		case "US West (N. California)":
			region = "US_WEST_1";
			break;
		case "Canada (Central)":
			region = "CA_CENTRAL_1";
			break;
		case "EU (Frankfurt)":
			region = "EU_CENTRAL_1";
			break;
		case "EU (London)":
			region = "EU_WEST_2";
			break;
		case "EU (Paris)":
			region = "EU_WEST_3";
			break;
		case "EU (Stockholm)":
			region = "EU_NORTH_1";
			break;
		case "Middle East (Bahrain)":
			region = "ME_SOUTH_1";
			break;
		case "Asia Pacific (Seoul)":
			region = "AP_NORTHEAST_2";
			break;
		case "Asia Pacific (Tokyo)":
			region = "AP_NORTHEAST_1";
			break;
		case "Asia Pacific (Sydney)":
			region = "AP_SOUTHEAST_2";
			break;
		case "South America (São Paulo)":
			region = "SA_EAST_1";
			break;
		default:
			break;
		}
		return region;
	}

	//addusertoteam
	$scope.addToTeamModal = function(){
		$('#addUsersToTeamModal').modal('show');
		server.getAvailableUsersForTeam($rootScope.selectedTeamId);
	}

	$scope.$on('availableUsersTeam:updated', function(event,data) {
		$scope.availableUsersList = data;
	})

	$scope.renameTeam = function(teamId){
		server.renameTeam(teamId,$scope.renamedTeamName);
		$scope.renamedTeamName = "";
	}
	$scope.$on('teamRenamed:updated', function(event,data) {
		$('#renameTeamModal').modal('hide');
		PNotify.removeAll();
		notificationService.success('Team renamed.');
		server.getTeams();
	})
	$scope.teamToRename = {};
	$scope.renameTeamModal = function(id,name,org){
		$scope.teamToRename = {};
		$scope.teamToRename.id = id;
		$scope.teamToRename.name = name;
		$scope.teamToRename.org = org;
		$('#renameTeamModal').modal('show');
	}

	$scope.addUsersToTeam = function(){
		server.addUsersToTeam($scope.selectedUsersList, $rootScope.selectedTeamId);
		$scope.selectedUsersList = [];
	}
	$scope.$on('usersAddedToTeam:updated', function(event,data) {
		if(data.result!="error"){
			$scope.getTeamMembers($rootScope.selectedTeamId);
			server.getChallenges();
			if($scope.user.r != 3){
				server.getUsers();
				server.getGlobalStats([]);
			}
			if($scope.user.r <= 3){
				server.getPendingReviews();
				server.getCompletedReviews();
				server.getAvailableExercises();
				server.getRunningExercises();
			}	
			$('#addUsersToTeamModal').modal('hide');
		}

	})

	//add team
	$scope.user = server.user;
	$scope.newTeamName = "";
	$scope.newTeamOrganization = "";

	$scope.removeFromTeam = function(teamId, username){
		server.removeFromTeam(teamId, username);
		$('#removeFromTeamModal').modal('hide');
	}
	$scope.userToRemove = {};
	$scope.removeFromTeamModal = function(team, username){
		$scope.userToRemove = {}
		$scope.userToRemove.teamName = team.name;
		$scope.userToRemove.teamId = team.id;
		$scope.userToRemove.username = username;
		$('#removeFromTeamModal').modal('show');
	}
	$scope.$on('deletedFromTeam:updated', function(event,data) {
		if(data.result=="success"){
			if($rootScope.visibility.teams)
				$scope.getTeamMembers($rootScope.selectedTeamId)

				server.getChallenges();
			if($scope.user.r != 3){
				server.getUsers();
				server.getGlobalStats([]);
			}
			if($scope.user.r <= 3){
				server.getPendingReviews();
				server.getCompletedReviews();
				server.getAvailableExercises();
				server.getRunningExercises();
			}	
		}
	})

	$scope.removeTeamManagerModal = function(teamId, username){
		$scope.userToRemove= {};
		$scope.userToRemove.teamId = teamId;
		$scope.userToRemove.username = username;
		$('#deleteTeamManagerModal').modal('show');
	}
	$scope.removeTeamManager = function(teamId, username){
		$('#deleteTeamManagerModal').modal('hide');
		server.removeTeamManager(teamId, username)
	}
	$scope.$on('removeTeamManager:updated', function(event,data) {
		if(data.result=="success")
			server.getTeamDetails($rootScope.selectedTeamId);
	})

	$scope.makeTeamManager = function(teamId, username){
		server.makeTeamManager(teamId, username)
	}
	$scope.$on('makeTeamManager:updated', function(event,data) {
		if(data.result=="success")
			server.getTeamDetails($rootScope.selectedTeamId);
	})
	$scope.teamToDelete = {};
	$scope.deleteTeamModal = function(teamId,teamName){
		$scope.teamToDelete = {};
		$scope.teamToDelete.id = teamId;
		$scope.teamToDelete.name = teamName;
		$('#deleteTeamModal').modal('show');
	}
	$scope.deleteTeam = function(teamId){
		server.deleteTeam(teamId);
		$('#deleteTeamModal').modal('hide');
	}
	$scope.saveNewTeam = function(){
		if($scope.newTeamOrganization!="" && $scope.newTeamName!=""){
			server.addTeam($scope.newTeamName,$scope.newTeamOrganization);
			$('#newTeamModal').modal('hide');
			$scope.newTeamName = "";
			$scope.newTeamOrganization = "";
		}
	}
	$scope.clearAddTeamForm = function(){
		$scope.newTeamName = "";
		$scope.newTeamOrganization = "";
	}
	$scope.$on('addTeam:updated', function(event,data) {
		if(data.result=="success")
			server.getTeams();
	})
	$scope.$on('deletedTeam:updated', function(event,data) {
		if(data.result=="success")
			server.getTeams();
	})
	$scope.$on('teamNameAvailable:updated', function(event,data) {
		$scope.teamNameAvailable = data.result;
	});
	$scope.teamNameAvailable = true;

	$scope.isRenamedTeamNameAvailable = function(){
		if($scope.renamedTeamName != "" )
			server.isTeamNameAvailable($scope.renamedTeamName,$scope.teamToRename.org);
	}
	$scope.isTeamNameAvailable = function(){
		if($scope.newTeamName != "" && $scope.newTeamOrganization !="" )
			server.isTeamNameAvailable($scope.newTeamName,$scope.newTeamOrganization);
	}

	$scope.addNewTeamModal = function(){
		$('#newTeamModal').modal('show');
	}
	$scope.remediatedPerIssue = {}
	$scope.remediatedPerIssue.data = [];
	$scope.remediatedPerIssue.labels = [];
	$scope.remediatedPerCategory = {}
	$scope.remediatedPerCategory.data = [];
	$scope.remediatedPerCategory.labels = [];
	$scope.remediationRatePerIssue = [];
	$scope.remediationRatePerCategory = [];
	$scope.scorePerExercises = {};
	$scope.options = {}
	$scope.options.animation = false;
	$scope.options.layout = {
			padding: {
				left: 0,
				right: 0,
				top: 0,
				bottom: 0
			}
	}
	$scope.options.pieceLabel = {
			// render 'label', 'value', 'percentage' or custom function, default is 'percentage'
			render: 'percentage',
			// precision for percentage, default is 0
			precision: 0,
			// identifies whether or not labels of value 0 are displayed, default is false
			showZero: false,
			// font size, default is defaultFontSize
			fontSize: 12,
			// font color, can be color array for each data, default is defaultFontColor
			fontColor: '#FFF',
			// font style, default is defaultFontStyle
			fontStyle: 'normal',
			// font family, default is defaultFontFamily
			fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			// draw label in arc, default is false
			arc: false,
			// position to draw label, available value is 'default', 'border' and 'outside'
			// default is 'default'
			position: 'border',
			// draw label even it's overlap, default is false
			overlap: true
	}
	$scope.options.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};
	$scope.options.title = {
			text : "",
			display:true,
			position: 'bottom'

	}
	$scope.radarOptions = {}
	$scope.radarOptions.animation = false;
	$scope.radarOptions.title = {
			text : "",
			display:true,
			position: 'top'

	}
	$scope.radarOptions.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};

	$scope.updateTeamsFilteredList = function() {
		$scope.filteredTeamsList = $filter("filter")($scope.masterTeamsList, $scope.queryTeams);
	};


	$scope.updateMembersFilteredList = function() {
		$scope.filteredMembersList = $filter("filter")($scope.masterMembersList, $scope.queryMembers);
	};

	$scope.teamstableconfig = {
			itemsPerPage: 20,
			fillLastPage: false
	}
	$scope.$on('teamsList:updated', function(event,data) {
		$scope.masterTeamsList = data;
		$scope.filteredTeamsList = $scope.masterTeamsList;
	});
	$scope.isTeamManager = function(idUser){
		return $scope.selectedTeamManagers.indexOf(idUser)>=0;
	}
	$scope.displayTeamList = function(){
		window.history.go(-1);
	}
	$scope.teamMembersData = [];
	$scope.getTeamMembers = function(id){
		$location.path("teams/details/"+id, false);
		$rootScope.selectedTeamId = id;
		server.getTeamMembers(id);
		server.getTeamStats(id);
		server.getTeamDetails(id);
	};
	$scope.$on('teamDetails:updated', function(event,data) {
		$scope.selectedTeamManagers = [];
		for(var i=0; i<data.managers.length;i++){
			$scope.selectedTeamManagers.push(data.managers[i]['idUser']);
		}
	});
	$scope.$on('statsTeam:updated', function(event,data) {
		$scope.remediationRatePerIssue = [];
		for (var property in data.issuesRemediationRate) {
			if (data.issuesRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.issuesRemediationRate[property]).length>0) {

				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.text = property;
				obj.options.title.fontSize=11
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				var alwaysZero = true;
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.issuesRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
					if(tmpValue!=0)
						alwaysZero = false;
				}
				if(!alwaysZero){
					$scope.remediationRatePerIssue.push(obj);
				}
			}
		}
		$scope.remediatedPerIssue.data = [[]];
		$scope.remediatedPerIssue.labels = [];
		$scope.remediatedPerIssue.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerIssue.options.scale = {
				display: true,
				ticks: {
					beginAtZero: true,
					min: 0,
					max: 100,
					stepSize: 10
				}
		}
		$scope.remediatedPerIssue.options.title.display = false;
		$scope.remediatedPerIssue.series = ["Remediated (%)"];

		for(var j in $scope.remediationRatePerIssue){
			if ($scope.remediationRatePerIssue.hasOwnProperty(j)){
				var tmpRem = $scope.remediationRatePerIssue[j].data[0]
				var tmpTot = $scope.remediationRatePerIssue[j].data.reduce(getSum);
				var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);
				var tmpName = $scope.remediationRatePerIssue[j].options.title.text
				$scope.remediatedPerIssue.labels.push(tmpName);
				$scope.remediatedPerIssue.data[0].push(tmpPercentage);
			}
		}

		$scope.remediationRatePerCategory = [];
		for (var property in data.categoriesRemediationRate) {
			if (data.categoriesRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.categoriesRemediationRate[property]).length>0) {
				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.text = property;
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.categoriesRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
				}
				$scope.remediationRatePerCategory.push(obj);
			}
		}
		$scope.remediatedPerCategory.data = [[],[],[]];
		$scope.remediatedPerCategory.labels = [];
		for(var j in $scope.remediationRatePerCategory){
			if ($scope.remediationRatePerCategory.hasOwnProperty(j)){
				var tmpRem = $scope.remediationRatePerCategory[j].data[0]
				var tmpTot = $scope.remediationRatePerCategory[j].data.reduce(getSum);
				var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);
				var tmpName = $scope.remediationRatePerCategory[j].options.title.text;
				$scope.remediatedPerCategory.labels.push(tmpName);
				$scope.remediatedPerCategory.data[0].push(tmpPercentage);
				$scope.remediatedPerCategory.data[1].push(Math.ceil(data.totalMinutesPerIssueCategory[tmpName]/60));
				$scope.remediatedPerCategory.data[2].push(data.avgMinutesPerIssueCategory[tmpName]);
			}
		}
		$scope.remediatedPerCategory.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerCategory.options.scale = {
				ticks: {
					beginAtZero: true,
					max: 100,
					min: 0
				}
		}
		$scope.remediatedPerCategory.options.title.display = false;
		$scope.remediatedPerCategory.series = ["Remediated (%)","Total Time Spent (hours)","Avg Time / Exercise (minutes)"];

		var tmpRemediatedPerCategoryPercentage = cloneObj($scope.remediatedPerCategory);
		var tmpRemediatedPerCategoryTime = cloneObj($scope.remediatedPerCategory);

		tmpRemediatedPerCategoryPercentage.data = tmpRemediatedPerCategoryPercentage.data.slice(0,1);
		tmpRemediatedPerCategoryPercentage.series = tmpRemediatedPerCategoryPercentage.series.slice(0,1);

		tmpRemediatedPerCategoryTime.data = tmpRemediatedPerCategoryTime.data.slice(1);
		tmpRemediatedPerCategoryTime.series = tmpRemediatedPerCategoryTime.series.slice(1);

		$scope.remediatedPerCategoryTime = tmpRemediatedPerCategoryTime;
		$scope.remediatedPerCategoryPercentage = tmpRemediatedPerCategoryPercentage;


	});
	$scope.$on('teamMembers:updated', function(event,data) {
		$scope.masterMembersList = data;
		$scope.filteredMembersList = $scope.masterMembersList;
		$rootScope.showTeamList = false;
		$rootScope.showTeamMembers = true;
	});
}])
sf.controller('review',['$scope','server','$rootScope','$filter','$location',function($scope,server,$rootScope,$filter,$location){
	$scope.user = server.user;
	$scope.showCodeDiff = false;
	$scope.showLogs = false;
	$scope.showResults = false;
	$scope.selectedResultRow = -1;
	$rootScope.masterPendingReviews = []
	$scope.pendingtableconfig = {
			itemsPerPage: 10,
			fillLastPage: false
	}
	$scope.emptyDiff = false;
	$scope.emptyLog = false;
	$scope.zipError = false;
	$scope.filteredPendingList = []; 

	$scope.updateFilteredList = function() {
		$scope.filteredPendingList = $filter("filter")($rootScope.masterPendingReviews, $scope.query);
	};



	$scope.assessorScore = 0;
	$scope.partialScores = {};
	$scope.assessorComments = {};
	$scope.newIssuesIntroducedDetails = "";
	$scope.awardTrophy = false;
	$scope.newIssuesIntroduced = false;
	$scope.$watchCollection('partialScores', function(newVal, oldVal){
		$scope.assessorScore = 0;
		for(var i in $scope.partialScores){
			if(Number.isInteger(parseInt($scope.partialScores[i])))
				$scope.assessorScore += parseInt($scope.partialScores[i]);
		}
	}, true);

	$rootScope.vulnerabilityStatus = [{id:0, name:"Not Vulnerable"},{id:1, name:"Vulnerable"},{id:2, name:"Broken Functionality"},{id:4, name:"Not Addressed"},{id:3, name:"N/A"},{id:5,name:"Partially Remediated"}]

	$scope.pendingItemDetails = {};
	$scope.assessorStatus = {};

	$scope.markCancelled = function(){
		server.markCancelled($scope.pendingItemDetails.id);
	}

	$scope.submitReview = function(){
		var obj = {};
		obj.review = [];

		for (var property in $scope.partialScores) {
			if (property != "" && $scope.partialScores.hasOwnProperty(property)) {
				var tmpObj = {};
				tmpObj.name = property;
				tmpObj.status = $scope.assessorStatus[property].id;
				tmpObj.score = $scope.partialScores[property];
				tmpObj.verified = true;
				tmpObj.comment = $scope.assessorComments[property];
				obj.review.push(tmpObj);
			}
		}

		obj.id = $scope.pendingItemDetails.id;
		obj.totalScore = $scope.assessorScore;
		obj.awardTrophy = $scope.awardTrophy;
		obj.newIssuesIntroduced = $scope.newIssuesIntroduced;
		obj.newIssuesIntroducedText = $scope.newIssuesIntroducedDetails;

		server.submitReview(obj);
	}


	$scope.size = function(obj) {
		var size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	};

	$scope.$on('pendingReviews:updated', function(event,data) {
		$rootScope.masterPendingReviews = data;
		$scope.filteredPendingList = $filter("filter")($rootScope.masterPendingReviews, $scope.query);
		$scope.showResults = false;
	});
	$scope.$on('reviewSubmitted:updated', function(event,data) {
		server.getPendingReviews();
		server.getCompletedReviews();
		server.getUsers();
		$scope.showResults = false;
		$scope.showCodeDiff = false;
		$scope.showLogs = false;
		$scope.pendingItemDetails = {};
		$scope.emptyDiff = false;
		$scope.emptyLog = false;
		$scope.zipError = false;
		$(window).scrollTop(0);
	});
	$scope.$on('reviewCancelled:updated', function(event,data) {
		server.getPendingReviews();
		server.getCompletedReviews();
		$scope.showResults = false;
		$scope.showCodeDiff = false;
		$scope.showLogs = false;
		$scope.pendingItemDetails = {};
		$scope.emptyDiff = false;
		$scope.emptyLog = false;
		$scope.zipError = false;
		$(window).scrollTop(0);
	});



	var remediationClassMap = {
			"Remediated": "table-success",
			"Vulnerable": "table-danger",
			"Partially Remediated": "table-warning",
			"Broken Functionality":"table-warning"
	}	
	$scope.getRemedationTableClass = function(status) {
		return remediationClassMap[status]
	};
	var statusClassMap = {
			"0": "table-success",
			"1": "table-danger",
			"2": "table-warning",
			"4": "table-info",
			"3": "table-secondary",
			"N/A": "table-secondary",
			"5": "table-warning"
	}
	$scope.getStatusString = function(status){
		switch(status){
		case "1":
			return "Vulnerable"
		case "0":
			return "Not Vulnerable"
		case "2":
			return "Broken Functionality"
		case "3":
			return "N/A"
		case "4":
			return "Not Addressed"
		case "5":
			return "Partially Remediated"



		default: return status;
		}
	}
	$scope.getStatusClass = function(status) {
		return statusClassMap[status]
	};
	var flagTypeMap = {
			"EXPLOITATION": "table-info",
			"REMEDIATION": "table-warning"
	}
	$scope.getClassForFlagType = function(type){
		return flagTypeMap[type]
	};
	var scoreClassMap = {
			success: "success",
			average: "warning",
			failure: "danger",
			pending: "info"
	};
	$scope.getDurationInterval = function(start,end){
		var out = moment.utc(moment(end).diff(moment(start))).format("H mm").replace(" ","h")+"'";
		return out;

	};
	$scope.getMinutesDuration = function(dur){
		if(undefined != dur && null != dur && 0 != dur){
			return moment.utc(moment.duration(dur,"m").asMilliseconds()).format("H mm").replace(" ","h")+"'";
		}
		else{
			return "N/A"
		}
	};
	$scope.getDatesInterval = function(start,end){
		var out = moment(start).local().format("MMM D YYYY, HH:mm");
		out += " - "+moment(end).local().format("HH:mm (Z)");
		return out;
	}

	$scope.getResultsScoreClass = function(result,total) {
		if(result==-1)
			return scoreClassMap["pending"];
		if(result==-1)
			return scoreClassMap["pending"];
		if(result>(total-(total/10)))
			return scoreClassMap["success"];
		else if(result<(total/3))
			return scoreClassMap["failure"];
		else  
			return scoreClassMap["average"];
	};

	$scope.toggleCodeDiff = function(){
		if($scope.showCodeDiff == true)
			$scope.showCodeDiff = false;
		else{
			$scope.showLogs = false;
			$scope.showCodeDiff = true;
		}
	}
	$scope.toggleInstanceLogs = function(){
		if($scope.showLogs == true)
			$scope.showLogs = false;
		else{
			$scope.showCodeDiff = false;
			$scope.showLogs = true;
		}
	}

	$scope.getQuestionName = function(resName){
		for(var i in $scope.pendingItemDetails.exercise.flags){
			if($scope.pendingItemDetails.exercise.flags.hasOwnProperty(i)){
				for(var j in $scope.pendingItemDetails.exercise.flags[i].flagList){
					if($scope.pendingItemDetails.exercise.flags[i].flagList.hasOwnProperty(j) && $scope.pendingItemDetails.exercise.flags[i].flagList[j].selfCheckAvailable && $scope.pendingItemDetails.exercise.flags[i].flagList[j].selfCheck.name==resName){
						return $scope.pendingItemDetails.exercise.flags[i].title;
					}
				}
			}
		}
	}
	$scope.getQuestionMaxScore = function(resName){
		for(var i in $scope.pendingItemDetails.exercise.flags){
			if($scope.pendingItemDetails.exercise.flags.hasOwnProperty(i)){
				for(var j in $scope.pendingItemDetails.exercise.flags[i].flagList){
					if($scope.pendingItemDetails.exercise.flags[i].flagList.hasOwnProperty(j) && $scope.pendingItemDetails.exercise.flags[i].flagList[j].selfCheckAvailable && $scope.pendingItemDetails.exercise.flags[i].flagList[j].selfCheck.name==resName){
						return $scope.pendingItemDetails.exercise.flags[i].flagList[j].maxScore;
					}
				}
			}
		}

	}



	$scope.$on('pendingReviewDetails:updated', function(event,data) {

		var offset = $('#showResults').offset();
		$('html, body').animate({
			scrollTop: offset.top - 50,
			scrollLeft: offset.left
		});

		if(data.results.length == 0){

			for(var j in data.exercise.flags){
				if(data.exercise.flags.hasOwnProperty(j)){
					for(var i in data.exercise.flags[j].flagList){
						if(data.exercise.flags[j].flagList.hasOwnProperty(i) && data.exercise.flags[j].flagList[i].selfCheckAvailable){
							data.results[j] = {};
							data.results[j].name = data.exercise.flags[j].flagList[i].selfCheck.name;
							data.results[j].status = "NOT_AVAILABLE";
							data.results[j].verified = false; 
						}
					}
				}

			}
		}

		data.placements = [];



		for(var i in data.results){
			if(data.results[i].firstForFlag){
				var tmpObj = {}
				tmpObj.name = data.results[i].name;
				tmpObj.placement = "1st place";
				data.placements.push(tmpObj)
			}
			if(data.results[i].secondForFlag){
				var tmpObj = {}
				tmpObj.name = data.results[i].name;
				tmpObj.placement = "2nd place";
				data.placements.push(tmpObj)
			}
			if(data.results[i].thirdForFlag){
				var tmpObj = {}
				tmpObj.name = data.results[i].name;
				tmpObj.placement = "3rd place";
				data.placements.push(tmpObj)
			}

			$scope.partialScores[data.results[i].name] = data.results[i].score

			switch(data.results[i].status){
			case "VULNERABLE":
				data.results[i].status = "Vulnerable"
					break;
			case "NOT_VULNERABLE":
				data.results[i].status = "Not Vulnerable"
					break;
			case "BROKEN_FUNCTIONALITY":
				data.results[i].status = "Broken Functionality"
					break;
			case "NOT_AVAILABLE":
				data.results[i].status = "N/A"
					break;
			case "NOT_ADDRESSED":
				data.results[i].status = "Not Addressed"
					break;
			case "PARTIALLY_REMEDIATED":
				data.results[i].status = "Partially Remediated"
					break;
			}
		}

		$scope.showCodeDiff = false;
		$scope.showLogs = false;
		$scope.pendingItemDetails = data;
		$scope.emptyDiff = false;
		$scope.emptyLog = false;
		$scope.zipError = false;

		$scope.showResults = true;


		var tba = "";
		try{
			JSZipUtils.getBinaryContent($scope.pendingItemDetails.id,$rootScope.ctoken,'/management/team/handler','getReviewFile', function(err, data) {
				if(err) {
					throw err; // or handle err
				}
				try{

					if(data.byteLength<=48){
						$scope.emptyDiff = true;
						return;
					}

					JSZip.loadAsync(data).then(function(zip) {

						try{
							for(file in zip.files){
								if(file.indexOf('sourceDiff.txt')>-1){
									zip.file(file).async("string").then(function success(content) {
										var diffString = content;
										if(diffString==""){
											$scope.emptyDiff = true;
											$('#targetDiv').empty()
											return;
										}
										try{
											var diff2htmlUi = new Diff2HtmlUI({diff : diffString });
											diff2htmlUi.draw( '#targetDiv', {
												inputFormat : 'diff',
												showFiles : true,
												matching : 'lines'
											});
											diff2htmlUi.highlightCode('#targetDiv');
										} catch(err){
											$scope.emptyDiff = true;
										}
									})                        
								}
							}
						}catch(err1){
							$scope.emptyDiff = true;
						}
					});
				}catch(err){
					$scope.emptyDiff = true;
				}
			});
		}catch(e){
			$scope.emptyDiff = true;
		}
	})


	$scope.showDetailsFor = function(eId, index){
		$scope.assessorStatus = {};
		$scope.assessorScore = 0;
		$scope.partialScores = {};
		$scope.assessorComments = {};
		$scope.reviewForm.$setPristine();
		server.getPendingReviewDetails(eId);
		$scope.selectedResultRow = index;
		$location.path("review/details/"+eId, false);

	};
}]);
sf.controller('stats',['$scope','server',function($scope,server){

	$scope.user = server.user;

	$scope.orgFilter = [];
	$scope.statsHaveLoaded = false;
	$scope.refreshStatsFilter = function(){
		var filter = [];
		for(var i=0; i< $scope.orgFilter.length; i++){
			if($scope.orgFilter[i].checked)
				filter.push($scope.orgFilter[i].id);
		}
		server.getGlobalStats(filter);
	}

	$scope.$on('userProfile:updated', function(event,data) {
		$scope.orgFilter = [];
		for(var i=0;i<$scope.user.organizations.length;i++){
			var tmpObj = cloneObj($scope.user.organizations[i]);
			tmpObj.checked = true;
			$scope.orgFilter.push(tmpObj);
		}
	});


	$scope.charts = {};
	$scope.charts.dashboard = true;
	$scope.charts.remediationRateIssue = false;
	$scope.charts.remediationRateTeam = false;
	$scope.charts.remediationRateCategory  = false;
	$scope.charts.remediationRateRegion  = false;

	$scope.remediatedPerIssue = {}
	$scope.remediatedPerIssue.data = [];
	$scope.remediatedPerIssue.labels = [];

	$scope.remediatedPerCategory = {}
	$scope.remediatedPerCategory.data = [];
	$scope.remediatedPerCategory.labels = [];

	$scope.remediatedPerTeam = {}
	$scope.remediatedPerTeam.data = [];
	$scope.remediatedPerTeam.labels = [];


	$scope.remediatedPerRegion = {}
	$scope.remediatedPerRegion.data = [];
	$scope.remediatedPerRegion.labels = [];

	$scope.remediationRatePerIssue = [];
	$scope.remediationRatePerCategory = [];
	$scope.remediationRatePerRegion = [];
	$scope.remediationRatePerTeam = [];

	$scope.showStats = function(chart){
		if(undefined!=$scope.charts[chart]){
			$('.waitLoader').show();
			for(var i in $scope.charts){
				if($scope.charts.hasOwnProperty(i)){
					$scope.charts[i] = false;
				}
			}
			$scope.charts[chart] = true;
			$('.waitLoader').fadeOut(1000);
		}
	}

	$scope.options = {}
	$scope.options.animation = false;
	$scope.options.layout = {
			padding: {
				left: 0,
				right: 0,
				top: 0,
				bottom: 0
			}
	}
	$scope.options.pieceLabel = {
			// render 'label', 'value', 'percentage' or custom function, default is 'percentage'
			render: 'percentage',
			// precision for percentage, default is 0
			precision: 0,
			// identifies whether or not labels of value 0 are displayed, default is false
			showZero: false,
			// font size, default is defaultFontSize
			fontSize: 12,
			// font color, can be color array for each data, default is defaultFontColor
			fontColor: '#FFF',
			// font style, default is defaultFontStyle
			fontStyle: 'normal',
			// font family, default is defaultFontFamily
			fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			// draw label in arc, default is false
			arc: false,
			// position to draw label, available value is 'default', 'border' and 'outside'
			// default is 'default'
			position: 'border',
			// draw label even it's overlap, default is false
			overlap: true
	}
	$scope.options.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};
	$scope.options.title = {
			text : "",
			display:true,
			position: 'bottom'

	}
	$scope.radarOptions = {}
	$scope.radarOptions.title = {
			text : "",
			display:true,
			position: 'top'

	}
	$scope.radarOptions.legend = {
			display: true,
			position: 'bottom',
			labels: {
				fontColor: "#000",
				fontSize: 10
			}
	};

	$scope.$on('stats:updated', function(event,data) {



		$scope.statsObj = data;
		$scope.statsHaveLoaded = true;

		function regionCodeFromName(regionName){
			var region = "";
			switch(regionName) {
			case "EU_WEST_1":
				region = "EMEA";
				break;
			case "US_EAST_1":
				region = "North America"
					break;
			case "AP_SOUTH_1":
				region = "APAC"
					break;
			case "AP_SOUTHEAST_1":
				region = "APAC"
					break;
			case "AP_EAST_1":
				region = "APAC"
					break;
			case "US_EAST_2":
				region = "North America";
				break;
			case "US_WEST_2":
				region = "North America";
				break;
			case "US_WEST_1":
				region = "North America";
				break;
			case "CA_CENTRAL_1":
				region = "North America";
				break;
			case "EU_CENTRAL_1":
				region = "EMEA";
				break;
			case "EU_WEST_2":
				region = "EMEA";
				break;
			case "EU_WEST_3":
				region = "EMEA";
				break;
			case "EU_NORTH_1":
				region = "EU (Stockholm)";
				break;
			case "ME_SOUTH_1":
				region = "Middle East (Bahrain)";
				break;
			case "AP_NORTHEAST_2":
				region = "APAC";
				break;
			case "AP_NORTHEAST_1":
				region = "APAC";
				break;
			case "AP_SOUTHEAST_2":
				region = "APAC";
				break;
			case "SA_EAST_1":
				region = "South America";
				break;
			default:
				break;
			}
			return region;
		}

		$scope.remediationRatePerTeam = [];
		for (var property in data.teamRemediationRate) {
			if (data.teamRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.teamRemediationRate[property]).length>0) {
				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.text = property;
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.teamRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
				}
				$scope.remediationRatePerTeam.push(obj);
			}
		}
		$scope.remediatedPerTeam.data = [[],[],[]];
		$scope.remediatedPerTeam.labels = [];
		for(var j in $scope.remediationRatePerTeam){
			if ($scope.remediationRatePerTeam.hasOwnProperty(j)){
				var tmpRem = $scope.remediationRatePerTeam[j].data[0]
				var tmpTot = $scope.remediationRatePerTeam[j].data.reduce(getSum);
				var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);
				var tmpName = $scope.remediationRatePerTeam[j].options.title.text;
				$scope.remediatedPerTeam.labels.push(tmpName);
				$scope.remediatedPerTeam.data[0].push(tmpPercentage);
				$scope.remediatedPerTeam.data[1].push((Math.ceil(data.totalMinutesPerTeam[tmpName]/60)));
				$scope.remediatedPerTeam.data[2].push(data.avgMinutesPerTeam[tmpName]);

			}
		}
		$scope.remediatedPerTeam.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerTeam.options.title.display = false;
		$scope.remediatedPerTeam.series = ["Remediated (%)","Total Time Spent (hours)","Avg Time / Exercise (minutes)"];

		$scope.remediationRatePerIssue = [];
		for (var property in data.issuesRemediationRate) {
			if (data.issuesRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.issuesRemediationRate[property]).length>0) {
				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.text = property;
				obj.options.title.fontSize=11
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.issuesRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
				}
				$scope.remediationRatePerIssue.push(obj);
			}
		}
		$scope.remediatedPerIssue.data = [[]];
		$scope.remediatedPerIssue.labels = [];
		$scope.remediatedPerIssue.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerIssue.options.title.display = false;
		$scope.remediatedPerIssue.series = ["Remediated (%)"];
		$scope.remediatedPerIssue.options.scale = {
				display: true,
				ticks: {
					beginAtZero: true,
					min: 0,
					max: 100,
					stepSize: 10
				}
		}
		for(var j in $scope.remediationRatePerIssue){
			if ($scope.remediationRatePerIssue.hasOwnProperty(j)){
				var tmpRem = $scope.remediationRatePerIssue[j].data[0]
				var tmpTot = $scope.remediationRatePerIssue[j].data.reduce(getSum);
				var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);
				var tmpName = $scope.remediationRatePerIssue[j].options.title.text
				$scope.remediatedPerIssue.labels.push(tmpName);
				$scope.remediatedPerIssue.data[0].push(tmpPercentage);
			}
		}


		$scope.remediationRatePerCategory = [];
		for (var property in data.categoriesRemediationRate) {
			if (data.categoriesRemediationRate.hasOwnProperty(property) && 
					Object.keys(data.categoriesRemediationRate[property]).length>0) {
				var obj = {};
				obj.options = cloneObj($scope.options);
				obj.options.title.text = property;
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.categoriesRemediationRate[property][tmpStatus]
					if(undefined==tmpValue){
						tmpValue = 0;
					}
					obj.data.push(tmpValue);
				}
				$scope.remediationRatePerCategory.push(obj);
			}
		}
		$scope.remediatedPerCategory.data = [[],[],[]]
		$scope.remediatedPerCategory.labels = [];
		for(var j in $scope.remediationRatePerCategory){
			if ($scope.remediationRatePerCategory.hasOwnProperty(j)){
				var tmpRem = $scope.remediationRatePerCategory[j].data[0]
				var tmpTot = $scope.remediationRatePerCategory[j].data.reduce(getSum);
				var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);
				var tmpName = $scope.remediationRatePerCategory[j].options.title.text;
				$scope.remediatedPerCategory.labels.push(tmpName);
				$scope.remediatedPerCategory.data[0].push(tmpPercentage);
				$scope.remediatedPerCategory.data[1].push(Math.ceil(data.totalMinutesPerIssueCategory[tmpName]/60));
				$scope.remediatedPerCategory.data[2].push(data.avgMinutesPerIssueCategory[tmpName]);
			}
		}
		$scope.remediatedPerCategory.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerCategory.options.scale = {
				ticks: {
					beginAtZero: true,
					max: 100,
					min: 0
				}
		}
		$scope.remediatedPerCategory.options.title.display = false;
		$scope.remediatedPerCategory.series = ["Remediated (%)","Total Time Spent (hours)","Avg Time / Exercise (minutes)"];


		var tmpRemediatedPerCategoryPercentage = cloneObj($scope.remediatedPerCategory);
		var tmpRemediatedPerCategoryTime = cloneObj($scope.remediatedPerCategory);

		tmpRemediatedPerCategoryPercentage.data = tmpRemediatedPerCategoryPercentage.data.slice(0,1);
		tmpRemediatedPerCategoryPercentage.series = tmpRemediatedPerCategoryPercentage.series.slice(0,1);

		tmpRemediatedPerCategoryTime.data = tmpRemediatedPerCategoryTime.data.slice(1);
		tmpRemediatedPerCategoryTime.series = tmpRemediatedPerCategoryTime.series.slice(1);

		$scope.remediatedPerCategoryTime = tmpRemediatedPerCategoryTime;
		$scope.remediatedPerCategoryPercentage = tmpRemediatedPerCategoryPercentage;

		$scope.remediationRatePerRegion = [];
		for (var property in data.regionsRemediationRate) {
			if (data.regionsRemediationRate.hasOwnProperty(property) && Object.keys(data.regionsRemediationRate[property]).length>0) {



				var obj = {};
				obj.options = cloneObj($scope.options);
				region = regionCodeFromName(property);
				obj.options.title.text = region;
				obj.data = [];
				obj.labels = ["Not Vulnerable", "Vulnerable", "Not Addressed", "Broken Functionality","Partially Remediated"];
				for(var l=0;l<obj.labels.length;l++){
					var tmpStatus = obj.labels[l].toUpperCase().replace(" ","_");
					var tmpValue = data.regionsRemediationRate[property][tmpStatus]
					if(undefined!=tmpValue){
						obj.data.push(tmpValue)
					}
					else{
						obj.data.push(0)
					}
				}
				$scope.remediationRatePerRegion.push(obj);
			}
		}
		$scope.remediatedPerRegion.labels = [];
		$scope.remediatedPerRegion.data = [[],[],[]]

		for(var l in data.totalMinutesPerRegion){
			if(data.totalMinutesPerRegion.hasOwnProperty(l)){
				data.totalMinutesPerRegion[regionCodeFromName(l)] = data.totalMinutesPerRegion[l]
				delete data.totalMinutesPerRegion[l];
			}
		}

		for(var m in data.avgMinutesPerRegion){
			if(data.avgMinutesPerRegion.hasOwnProperty(m)){
				data.avgMinutesPerRegion[regionCodeFromName(m)] = data.avgMinutesPerRegion[m]
				delete data.avgMinutesPerRegion[m];
			}
		}
		var tmpRemRatePerRegion = [];
		for(var s1 in $scope.remediationRatePerRegion){
			var found = false;
			if($scope.remediationRatePerRegion.hasOwnProperty(s1)){
				for(var s2 in tmpRemRatePerRegion){
					if(tmpRemRatePerRegion.hasOwnProperty(s2)){
						if($scope.remediationRatePerRegion[s1].options.title.text==tmpRemRatePerRegion[s2].options.title.text){
							found = true;
							for(var s3 in tmpRemRatePerRegion[s2].data){
								if(tmpRemRatePerRegion[s2].data.hasOwnProperty(s3)){
									tmpRemRatePerRegion[s2].data[s3] += $scope.remediationRatePerRegion[s1].data[s3];
								}
							}
							break;
						}
					}
				}
				if(!found){
					tmpRemRatePerRegion.push($scope.remediationRatePerRegion[s1]);
				}
			}
		}

		for(var j in tmpRemRatePerRegion){	
			if ($scope.remediationRatePerRegion.hasOwnProperty(j)){

				var tmpRem = tmpRemRatePerRegion[j].data[0]
				var tmpTot = tmpRemRatePerRegion[j].data.reduce(getSum);
				var tmpPercentage =  Math.floor((tmpRem * 100) / tmpTot);
				var tmpName = tmpRemRatePerRegion[j].options.title.text;
				$scope.remediatedPerRegion.labels.push(tmpRemRatePerRegion[j].options.title.text);
				$scope.remediatedPerRegion.data[0].push(tmpPercentage);
				$scope.remediatedPerRegion.data[1].push(Math.ceil(data.totalMinutesPerRegion[regionCodeFromName(tmpName)]/60));
				$scope.remediatedPerRegion.data[2].push(data.avgMinutesPerRegion[regionCodeFromName(tmpName)]);
			}
		}
		$scope.remediatedPerRegion.options = cloneObj($scope.radarOptions);
		$scope.remediatedPerRegion.options.title.display = false;
		$scope.remediatedPerRegion.series = ["Remediated (%)","Total Time Spent (hours)","Avg Time / Exercise (minutes)"];

	});

}]);
sf.controller('settings',['$scope','server','$timeout',function($scope,server,$timeout){
	$scope.user = server.user;
	$scope.countries = server.countries;
	$scope.oldPassword = "";
	$scope.newPasswordRepeat = "";
	$scope.newPassword = "";
	$scope.updateUserProfile = function(){
		server.updateUserProfile($scope.user);
	}
	$scope.updateUserPassword = function(){
		server.updateUserPassword($scope.userPasswordForm.oldPassword.$modelValue, $scope.userPasswordForm.newPassword.$modelValue);
		$scope.oldPassword = "";
		$scope.newPasswordRepeat = "";
		$scope.newPassword = "";
	}
	$scope.removeUser = function(){
		server.removeUser();
		$('#userRemoveModal').modal('hide');
	}
	$scope.openRemoveModal = function(){
		$('#userRemoveModal').modal('show');	
	}
	$scope.$on('removeUser:updated', function(event,data) {
		document.location = "/index.html";
	})

}]);
sf.controller('runningExercises',['$scope','server','$rootScope','$location','$filter',function($scope,server,$rootScope,$location,$filter){

	$scope.masterRunning = [];
	$scope.filteredRunningList = []; 
	$scope.selectedRunningRow = -1;
	$scope.runningtableconfig = {
			itemsPerPage: 30,
			fillLastPage: false
	}
	$scope.updateFilteredList = function() {
		$scope.filteredRunningList = $filter("filter")($scope.masterRunning, $scope.query);
	};

	$scope.$on('runningExercises:updated', function(event,data) {
		$scope.masterRunning = data;
		$scope.filteredRunningList = $scope.masterRunning;
	});

	$rootScope.getRegionFromCode = function(code,b){
		if(code==undefined || code == "")
			return "";
		var region = "";

		if(b!=undefined)
			console.log(b)
			switch(code){
			case "EU_WEST_1":
				region = "EU (Ireland)";
				break;
			case "US_EAST_1":
				region = "US East (N. Virginia)"
					break;
			case "AP_EAST_1":
				region = "Asia Pacific (Hong Kong)"
					break;
			case "AP_SOUTH_1":
				region = "Asia Pacific (Mumbai)"
					break;
			case "AP_SOUTHEAST_1":
				region = "Asia Pacific (Singapore)"
					break;
			case "US_EAST_2":
				region = "US East (Ohio)";
				break;
			case "US_WEST_2":
				region = "US West (Oregon)";
				break;
			case "US_WEST_1":
				region = "US West (N. California)";
				break;
			case "CA_CENTRAL_1":
				region = "Canada (Central)";
				break;
			case "EU_CENTRAL_1":
				region = "EU (Frankfurt)";
				break;
			case "EU_WEST_2":
				region = "EU (London)";
				break;
			case "EU_WEST_3":
				region = "EU (Paris)";
				break;
			case "EU_NORTH_1":
				region = "EU (Stockholm)";
				break;
			case "ME_SOUTH_1":
				region = "Middle East (Bahrain)";
				break;
			case "AP_NORTHEAST_2":
				region = "Asia Pacific (Seoul)";
				break;
			case "AP_NORTHEAST_1":
				region = "Asia Pacific (Tokyo)";
				break;
			case "AP_SOUTHEAST_2":
				region = "Asia Pacific (Sydney)";
				break;
			case "SA_EAST_1":
				region = "South America (São Paulo)";
				break;
			default:
				break;
			}
		return region;

	}



}]);
sf.controller('challenges',['$scope','server','$rootScope','$location','$filter','$interval',function($scope,server,$rootScope,$location,$filter,$interval){

	$scope.user = server.user;
	$scope.availableUserRoles = [{ id:7, name:"User"},{ id:4, name:"Stats Analyst"},{ id:3, name:"Team Manager"},{ id:0, name: "Organization Admin"},{ id:-1, name: "SF Admin"}];
	$scope.countries = server.countries;
	$scope.selectedChallenge = "";
	$scope.filteredChallengesList = [];
	$scope.masterChallengesList = [];
	$rootScope.showChallengesList = true;
	$rootScope.showChallengeDetails = false;
	$scope.challengeNameAvailable = true;
	$scope.userObj = [];
	$scope.selectedExerciseList = [];
	$scope.selectedUsersList = [];

	var tmpChallengeToRemove = -1;
	$scope.tmpChallengeToBeRemovedName = "";
	$scope.removeChallengeModal = function(id,name){
		tmpChallengeToRemove = id;
		$scope.tmpChallengeToBeRemovedName = name
		$('#removeChallengeModal').modal('show');
	}
	$scope.removeChallenge = function(){
		server.removeChallenge(tmpChallengeToRemove);
		tmpChallengeToRemove = -1;
		$('#removeChallengeModal').modal('hide');
	}

	$scope.getChallengeUserScore = function(user){
		return "";
	}
	$scope.getChallengeUserRunExercises = function(user){
		return "";
	}

	
	$rootScope.getTournamentStatusClass = function(status){
		switch(status){
		case "IN_PROGRESS":
			return "b-success";
			break;
		case "NOT_STARTED":
			return "b-primary";
			break;
		case "FINISHED":
			return "b-danger";
			break;
		}
	}


	$scope.getChallengeDetails = function(exId){
		server.getChallengeDetails(exId);
	}
	$scope.scoringModes = [0,1,2];
	$scope.usersInSelectedOrg = [];
	$scope.tmpNewChallenge = {};
	$scope.saveFlow = false;

	$scope.filteredAvailableExercisesList = [];
	$scope.masterAvailableExercisesList = [];
	$scope.exercisesForOrgs = [];

	$scope.getExerciseStatusString = function(status){
		switch(status){
		case "0":
			return "Available";
			break;
		case "2":
			return "Coming Soon";
			break;
		case "3":
			return "Inactive";
			break;
		default:
			return "N/A";
		}
	}


	$scope.updateAvailableExercisesFilteredList = function() {
		$scope.filteredAvailableExercisesList = $filter("filter")($scope.masterAvailableExercisesList, $scope.queryAvailableExercises);
	};
	$scope.availableExercisestableconfig = {
			itemsPerPage: 8,
			fillLastPage: false
	}
	$scope.$on('availableExercises:updated', function(event,data) {
		$scope.masterAvailableExercisesList = [];
		$scope.exercisesForOrgs = data.orgs;

		$scope.selectedExerciseList = [];

		if($scope.tmpNewChallenge.organization && $scope.tmpNewChallenge.organization.id){
			for(var i in data.exercises){
				if(data.exercises[i].hasOwnProperty('uuid') && $scope.getExerciseEnabledForOrgText($scope.tmpNewChallenge.organization.id,data.exercises[i].uuid)){
					$scope.masterAvailableExercisesList.push(data.exercises[i]);
				}
			}
			for(var i in  $rootScope.challengeDetails.exercises){
				if( $rootScope.challengeDetails.exercises[i].hasOwnProperty('uuid')){
					for(var j in $scope.masterAvailableExercisesList){
						if( $scope.masterAvailableExercisesList[j].hasOwnProperty('uuid') && $scope.masterAvailableExercisesList[j].uuid == $rootScope.challengeDetails.exercises[i].uuid){
							$scope.selectedExerciseList.push($scope.masterAvailableExercisesList[j].id)
							$scope.masterAvailableExercisesList[j].isChecked = true;
						}
					}

				}
			}
			$scope.filteredAvailableExercisesList = cloneObj($scope.masterAvailableExercisesList);
		}
	});

	$scope.getExerciseTitleFromId = function(id){
		for(var i in $scope.masterAvailableExercisesList){
			if( $scope.masterAvailableExercisesList[i].id == id){
				return $scope.masterAvailableExercisesList[i].title
			}
		}
		return "Exercise "+id;
	}

	$rootScope.getScoringModeString = function(code){
		switch(parseInt(code)){
		case 0:
			return "Automated Scoring";
		case 2:
			return "Manual Scoring";
		default: 
			return "";
		}
	}

	$scope.getUserRoleString = function(id){
		for(var i in $scope.availableUserRoles){
			if($scope.availableUserRoles.hasOwnProperty(i)){
				if($scope.availableUserRoles[i].id==id)
					return $scope.availableUserRoles[i].name;
			}
		}
		return id;
	}



	$scope.userCheckToggle=function(s){
		if(s.isChecked === true){
			s.isChecked === false;
		}else{
			s.isChecked === true;
		}
		if($scope.selectedUsersList.indexOf(s.user)<0){
			$scope.selectedUsersList.push(s.user);
		}
		else{
			var idx = $scope.selectedUsersList.indexOf(s.user);
			$scope.selectedUsersList.remove(idx,idx);
		}
	}

	$scope.exerciseCheckToggle=function(s){
		if(s.isChecked === true){
			s.isChecked === false;
		}else{
			s.isChecked === true;
		}
		if($scope.selectedExerciseList.indexOf(s.id)<0){
			$scope.selectedExerciseList.push(s.id);
		}
		else{
			var idx = $scope.selectedExerciseList.indexOf(s.id);
			$scope.selectedExerciseList.remove(idx,idx);
		}
	}

	$scope.clear = function(){
		$scope.tmpNewChallenge = {};
		$scope.tmpNewChallenge.startDate = "";
		$scope.tmpNewChallenge.endDate = "";
		$scope.tmpNewChallenge.name = "";
		$scope.tmpNewChallenge.details = "";
		$scope.tmpNewChallenge.users = [];
		$scope.tmpNewChallenge.exercises = [];
		$scope.tmpNewChallenge.organization = "";
		$scope.tmpNewChallenge.scoringMode = "";
		$scope.masterUsersList = [];
		$scope.filteredUsersList = [];
		$scope.userObj = [];
		$scope.selectedUsersList = [];
		$scope.selectedExerciseList = [];
		$scope.filteredAvailableExercisesList = [];
		$scope.masterAvailableExercisesList = [];
		$scope.exercisesForOrgs = [];
	}

	$scope.addNewChallenge = function(){
		$('#addNewChallengeModal').modal('hide');
		var obj = cloneObj($scope.tmpNewChallenge)
		try{
			obj.endDate = moment.utc(obj.endDate).format('ddd, D MMM YYYY HH:mm:ss z');
			obj.startDate = moment.utc(obj.startDate).format('ddd, D MMM YYYY HH:mm:ss z');

		}catch(e){}
		obj.users = $scope.selectedUsersList;
		obj.exercises = $scope.selectedExerciseList;
		obj.idOrg = obj.organization.id
		delete obj.organization
		server.addChallenge(obj);
	}

	$scope.$on('addChallenge:updated', function(event,data) {
		server.getChallenges();
		$scope.clear();
		$scope.saveFlow = false;
	})
	$scope.$on('challengeRemoved:updated', function(event,data) {
		server.getChallenges();
		$scope.saveFlow = false;
	})
	$scope.$on('challengeUpdated:updated', function(event,data) {
		server.getChallenges();
		$scope.getChallengeDetails($rootScope.challengeDetails.id)
		$scope.clear();
		$scope.saveFlow = false;
	})

	$scope.updateChallengeModal = function(){
		$scope.saveFlow = true;

		$scope.tmpNewChallenge = {};
		$scope.tmpNewChallenge.organization = $rootScope.challengeDetails.organization;
		$scope.getUsersExercisesInOrg();
		$scope.tmpNewChallenge.id = $rootScope.challengeDetails.id;
		$scope.tmpNewChallenge.startDate = moment(moment($rootScope.challengeDetails.startDate).local().format());
		$scope.tmpNewChallenge.endDate = moment(moment($rootScope.challengeDetails.endDate).local().format());
		$scope.tmpNewChallenge.name = $rootScope.challengeDetails.name;
		$scope.tmpNewChallenge.details = $rootScope.challengeDetails.details;
		$scope.tmpNewChallenge.scoringMode = $rootScope.challengeDetails.scoring;
		$scope.tmpNewChallenge.firstPlace = $rootScope.challengeDetails.firstInFlag;
		$scope.tmpNewChallenge.secondPlace = $rootScope.challengeDetails.secondInFlag;
		$scope.tmpNewChallenge.thirdPlace = $rootScope.challengeDetails.thirdInFlag;
		$scope.tmpNewChallenge.users = [];
		$scope.tmpNewChallenge.exercises = [];

		$('#addNewChallengeModal').modal('show');
	}

	$scope.updateChallenge = function(){

		$('#addNewChallengeModal').modal('hide');
		var obj = cloneObj($scope.tmpNewChallenge)
		try{
			obj.endDate = moment.utc(obj.endDate).format('ddd, D MMM YYYY HH:mm:ss z');
			obj.startDate = moment.utc(obj.startDate).format('ddd, D MMM YYYY HH:mm:ss z');

		}catch(e){}
		obj.users = $scope.selectedUsersList;
		obj.exercises = $scope.selectedExerciseList;
		delete obj.organization		
		server.updateChallenge(obj);
	}

	$scope.isChallengeNameAvailable = function(name){
		if($scope.saveFlow && name == $rootScope.challengeDetails.name){
			return true;
		}
		if($scope.tmpNewChallenge.organization!="")
			server.isChallengeNameAvailable($scope.tmpNewChallenge.name, $scope.tmpNewChallenge.organization);
	}
	$scope.$on('challengeNameAvailable:updated', function(event,data) {
		$scope.challengeNameAvailable = data.result;
	})

	$scope.$on('challengeAdded:updated', function(event,data) {
		$scope.tmpNewChallenge = {};
		$scope.tmpNewChallenge.startDate = "";
		$scope.tmpNewChallenge.endDate = "";
		$scope.tmpNewChallenge.name = "";
		$scope.tmpNewChallenge.details = "";
		$scope.tmpNewChallenge.users = [];
		$scope.tmpNewChallenge.exercises = [];
		$scope.tmpNewChallenge.organization = "";
		$scope.tmpNewChallenge.scoringMode = "";
		$scope.masterUsersList = [];
		$scope.filteredUsersList = [];
	});

	$rootScope.getDateInCurrentTimezone = function(date,format){
		if(date==null)
			return "N/A"
			return moment(date).local().format(format);
	}

	$scope.getExerciseEnabledForOrgText = function(orgId,uuid){
		if(undefined == orgId || undefined == uuid)
			return false;
		for(var i in $scope.exercisesForOrgs){
			if ($scope.exercisesForOrgs[i].hasOwnProperty("organization") && $scope.exercisesForOrgs[i].organization.id==orgId){
				for(var j in $scope.exercisesForOrgs[i].exercises){
					if($scope.exercisesForOrgs[i].exercises[j].uuid==uuid){
						return true;
					}
				}
				return false;
			}
		}
		return false;
	}

	$scope.changeOrganization = function(){

		$scope.userObj = [];
		$scope.selectedUsersList = [];
		$scope.selectedExerciseList = [];
		$scope.getUsersExercisesInOrg();

	}

	$scope.getUsersExercisesInOrg = function(){

		if(!$scope.tmpNewChallenge.organization || !$scope.tmpNewChallenge.organization.id)
			return;
		server.getUsersInOrg($scope.tmpNewChallenge.organization)
		server.getAvailableExercises();

	}

	$scope.masterUsersList = [];
	$scope.filteredUsersList = []; 

	$scope.$on('usersInOrg:updated', function(event,data) {
		$scope.masterUsersList = data;
		$scope.filteredUsersList = $filter("filter")($scope.masterUsersList, $scope.query);

		$scope.userObj = [];
		$scope.selectedUsersList = [];

		for(var i in  $rootScope.challengeDetails.users){
			if( $rootScope.challengeDetails.users[i].hasOwnProperty('user')){
				for(var j in $scope.masterUsersList){
					if( $scope.masterUsersList[j].hasOwnProperty('user') && $scope.masterUsersList[j].user == $rootScope.challengeDetails.users[i].user){
						$scope.selectedUsersList.push($scope.masterUsersList[j].user)
						$scope.masterUsersList[j].isChecked = true;
					}
				}

			}
		}

	});	
	$scope.usertableconfig = {
			itemsPerPage: 9,
			fillLastPage: false
	}
	$scope.updateFilteredList = function() {
		$scope.filteredUsersList = $filter("filter")($scope.masterUsersList, $scope.query);
	};



	$scope.addNewChallengeModal = function(){
		if($scope.saveFlow){
			$scope.clear();
			$scope.saveFlow = false;
		}
		$('#addNewChallengeModal').modal('show');
	}

	$rootScope.challengeDetails = [];
	$scope.challengeResults = {};

	var challengeUpdateTimer = null;
	
	$scope.$on('challengeResults:updated', function(event,data) {
		
		data.exerciseData = $rootScope.challengeDetails.exerciseData.slice(0);
		
		data.flags = [];
		data.theads = [];
		
		for (var property in data.exerciseData) {
			if (data.exerciseData.hasOwnProperty(property)) {
				for(var f in data.exerciseData[property].flags){
					if (data.exerciseData[property].flags.hasOwnProperty(f) && !data.exerciseData[property].flags[f].flagList[0].optional ) {
						var tmpObj = {};
						tmpObj.id = data.exerciseData[property].flags[f].id
						tmpObj.name = data.exerciseData[property].flags[f].title
						data.theads.push(tmpObj)
						data.flags.push(data.exerciseData[property].flags[f]);
					}
				}
			}
		}

		var remediated = 0; 
		data.runFlags = 0;
		data.challengeRunningExercises=0;
		data.challengeRunExercises = 0;
		for(var i=0;i<data.runExercises.length;i++){
			if(data.runExercises[i].status=="RUNNING"){
				data.challengeRunningExercises++;
			}
			else{
				data.challengeRunExercises++;
			}
			for(var j=0;j<data.runExercises[i].results.length;j++){
				data.runFlags++;
				if(data.runExercises[i].results[j].status == "0"){
					remediated++;
				}
			}
		}
		if(remediated==0 || data.runFlags == 0)
			data.remediation = 0;
		else
			data.remediation = remediated/data.runFlags * 100;
		
		$rootScope.challengeDetails = data;
		$rootScope.challengeDetails.lastRefreshed = new Date();
		$rootScope.challengeDetails.teams = [];

		for(var u in $rootScope.challengeDetails.users){
			if ($rootScope.challengeDetails.users.hasOwnProperty(u)){
				if(undefined!=$rootScope.challengeDetails.users[u].team && $rootScope.challengeDetails.teams.indexOf($rootScope.challengeDetails.users[u].team.name)<0){
					$rootScope.challengeDetails.teams.push($rootScope.challengeDetails.users[u].team.name)
				}
				$scope.challengeResults[$rootScope.challengeDetails.users[u].user] = {}
				$rootScope.challengeDetails.users[u].challengeRunFlags = 0;
				$rootScope.challengeDetails.users[u].challengeRunExercises = 0;
				$rootScope.challengeDetails.users[u].challengeScore = 0;
				for(var e in $rootScope.challengeDetails.runExercises){
					if($rootScope.challengeDetails.runExercises.hasOwnProperty(e) && $rootScope.challengeDetails.runExercises[e].user.user==$rootScope.challengeDetails.users[u].user){
						$rootScope.challengeDetails.users[u].challengeRunExercises++;
						for(var r in $rootScope.challengeDetails.runExercises[e].results){
							if($rootScope.challengeDetails.runExercises[e].results.hasOwnProperty(r)){
								$scope.challengeResults[$rootScope.challengeDetails.users[u].user][$rootScope.challengeDetails.runExercises[e].results[r].name] = $rootScope.challengeDetails.runExercises[e].results[r];
								if($rootScope.challengeDetails.runExercises[e].results[r].status=="0" && Number.isInteger($rootScope.challengeDetails.runExercises[e].results[r].score)){
									$rootScope.challengeDetails.users[u].challengeScore += $rootScope.challengeDetails.runExercises[e].results[r].score;
								}
								$rootScope.challengeDetails.users[u].challengeRunFlags++;
							} 
						}
					}
				}
			}
		}
		$rootScope.showChallengeDetails = true;
		$rootScope.showChallengesList = false;
		if(challengeUpdateTimer==null ){
			challengeUpdateTimer = $interval(function(){$scope.triggerChallengeUpdate($rootScope.challengeDetails.id)},20000)
		}
		if($scope.tableQuery==""){
			$scope.challengeDetailsTableFilteredList =  $rootScope.challengeDetails.users;
		}
		else{
			$scope.challengeDetailsTableFilteredList =  $filter("filter")($rootScope.challengeDetails.users, $scope.tableQuery);
		}
		$scope.challengeDetailsTableMasterList = $rootScope.challengeDetails.users;

	});
	
	$scope.getUsernameFromId = function(id){
		
		if(id==null)
			return "";
		for(var i=0; i<$rootScope.challengeDetails.users.length;i++){
			if($rootScope.challengeDetails.users[i].idUser==id)
				return $rootScope.challengeDetails.users[i].user;
		}
		return ""
		
	}
	$rootScope.challengeUserTableConfig = {
			itemsPerPage: 15,
			fillLastPage: false
	}
	
	$scope.$on('challengeDetails:updated', function(event,data) {
		
		
		data.flags = [];
		data.theads = [];


		for (var property in data.exerciseData) {
			if (data.exerciseData.hasOwnProperty(property)) {
				for(var f in data.exerciseData[property].flags){
					if (data.exerciseData[property].flags.hasOwnProperty(f) && !data.exerciseData[property].flags[f].flagList[0].optional ) {
						var tmpObj = {};
						tmpObj.id = data.exerciseData[property].flags[f].id
						tmpObj.name = data.exerciseData[property].flags[f].title
						data.theads.push(tmpObj)
						data.flags.push(data.exerciseData[property].flags[f]);
					}
				}
			}
		}
		

		var remediated = 0; 
		data.runFlags = 0;
		data.challengeRunningExercises=0;
		data.challengeRunExercises = 0;
		for(var i=0;i<data.runExercises.length;i++){
			if(data.runExercises[i].status=="RUNNING"){
				data.challengeRunningExercises++;
			}
			else{
				data.challengeRunExercises++;
			}
			for(var j=0;j<data.runExercises[i].results.length;j++){
				data.runFlags++;
				if(data.runExercises[i].results[j].status == "0"){
					remediated++;
				}
			}
		}
		if(remediated==0 || data.runFlags == 0)
			data.remediation = 0;
		else
			data.remediation = remediated/data.runFlags * 100;
		
		$rootScope.challengeDetails = data;
		$rootScope.challengeDetails.lastRefreshed = new Date();
		$rootScope.challengeDetails.teams = [];

		for(var u in $rootScope.challengeDetails.users){
			if ($rootScope.challengeDetails.users.hasOwnProperty(u)){
				if(undefined!=$rootScope.challengeDetails.users[u].team && $rootScope.challengeDetails.teams.indexOf($rootScope.challengeDetails.users[u].team.name)<0){
					$rootScope.challengeDetails.teams.push($rootScope.challengeDetails.users[u].team.name)
				}
				$scope.challengeResults[$rootScope.challengeDetails.users[u].user] = {}
				$rootScope.challengeDetails.users[u].challengeRunFlags = 0;
				$rootScope.challengeDetails.users[u].challengeRunExercises = 0;
				$rootScope.challengeDetails.users[u].challengeScore = 0;
				for(var e in $rootScope.challengeDetails.runExercises){
					if($rootScope.challengeDetails.runExercises.hasOwnProperty(e) && $rootScope.challengeDetails.runExercises[e].user.user==$rootScope.challengeDetails.users[u].user){
						$rootScope.challengeDetails.users[u].challengeRunExercises++;
						for(var r in $rootScope.challengeDetails.runExercises[e].results){
							if($rootScope.challengeDetails.runExercises[e].results.hasOwnProperty(r)){
								$scope.challengeResults[$rootScope.challengeDetails.users[u].user][$rootScope.challengeDetails.runExercises[e].results[r].name] = $rootScope.challengeDetails.runExercises[e].results[r];
								if($rootScope.challengeDetails.runExercises[e].results[r].status=="0" && Number.isInteger($rootScope.challengeDetails.runExercises[e].results[r].score)){
									$rootScope.challengeDetails.users[u].challengeScore += $rootScope.challengeDetails.runExercises[e].results[r].score;
								}
								$rootScope.challengeDetails.users[u].challengeRunFlags++;
							} 
						}
					}
				}
			}
		}
		$rootScope.showChallengeDetails = true;
		$rootScope.showChallengesList = false;
		if(challengeUpdateTimer==null ){
			challengeUpdateTimer = $interval(function(){$scope.triggerChallengeUpdate($rootScope.challengeDetails.id)},20000)
		}
		if($scope.tableQuery==""){
			$scope.challengeDetailsTableFilteredList =  $rootScope.challengeDetails.users;
		}
		else{
			$scope.challengeDetailsTableFilteredList =  $filter("filter")($rootScope.challengeDetails.users, $scope.tableQuery);
		}
		$scope.challengeDetailsTableMasterList = $rootScope.challengeDetails.users;

		$location.path("tournaments/details/"+$rootScope.challengeDetails.id, false);

	});
	
	$scope.updateResultsTableFilteredList = function() {
		$scope.challengeDetailsTableFilteredList = $filter("filter")($scope.challengeDetailsTableMasterList, $scope.resultsTableQuery);
	};

	$scope.challengeDetailsTableMasterList = [];
	$scope.challengeDetailsTableFilteredList = [];
	$scope.updateTableFilteredList = function() {
		$scope.challengeDetailsTableFilteredList = $filter("filter")($scope.challengeDetailsTableMasterList, $scope.tableQuery);
	};
	$scope.triggerChallengeUpdate = function(id){
		if($rootScope.visibility.challenges && $rootScope.showChallengeDetails && id == $rootScope.challengeDetails.id){
			server.getChallengeResults(id);
		}
		else{
			$interval.cancel(challengeUpdateTimer);
			challengeUpdateTimer = null;
		}
	}

	$scope.getExerciseIdForFlag = function(usr,flag){
		var sf = getSelfCheckFromFlag(flag);
		if(sf=="")
			return "";
		var runExercises = $rootScope.challengeDetails.runExercises;
		for (var ex=0;ex<runExercises.length;ex++) {
			if(runExercises[ex].user.user != usr)
				continue;
			for(var res=0;res<runExercises[ex]["results"].length;res++){
				if (runExercises[ex]["results"][res].name==sf) {
					return runExercises[ex].id
				}
			}			
		}
	}
	function getSelfCheckFromFlag(flag){
		for(var f in $rootScope.challengeDetails.flags){
			if($rootScope.challengeDetails.flags.hasOwnProperty(f) && $rootScope.challengeDetails.flags[f].title==flag){
				for(var q in $rootScope.challengeDetails.flags[f].flagList){
					if($rootScope.challengeDetails.flags[f].flagList.hasOwnProperty(q) && $rootScope.challengeDetails.flags[f].flagList[q].selfCheckAvailable==true){
						return $rootScope.challengeDetails.flags[f].flagList[q].selfCheck.name;
					}
				}
			}
		}
		return "";
	}

	$scope.getClassPlacementInChallenge= function(usr,name){

		if($scope.challengeResults[usr]==undefined)
			return false;
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return false;
		if($scope.challengeResults[usr][sf]['firstForFlag']){
			return "goldPlacement";
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return "silverPlacement";
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return "bronzePlacement";
		}
		return "";
	}
	$scope.getPlacementInChallenge = function(usr,name){
		if($scope.challengeResults[usr]==undefined)
			return "";
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return "";

		if($scope.challengeResults[usr][sf]['firstForFlag']){
			return "1st";
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return "2nd";
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return "3rd";
		}
		return "";
	}

	$scope.isPlacedInChallenge = function(usr,name){

		if($scope.challengeResults[usr]==undefined)
			return "";
		var sf = getSelfCheckFromFlag(name);
		if($scope.challengeResults[usr][sf]==undefined)
			return "";

		else if($scope.challengeResults[usr][sf]['firstForFlag']){
			return true;
		}
		else if($scope.challengeResults[usr][sf]['secondForFlag']){
			return true;
		}
		else if($scope.challengeResults[usr][sf]['thirdForFlag']){
			return true;
		}
		return false;
	}

	$scope.getChallengeResultFor = function(usr,flag){
		var sf = getSelfCheckFromFlag(flag);

		if(sf=="")
			return "N/A";
		var status = "-1";
		//loop1:
		try{
			var status = $scope.challengeResults[usr][sf].status
		}catch(e){
			status = "-1";
		}

		switch(status){
		case undefined:
			return "Not Started"
		case "-1":
			return "Not Started"
		case "1":
			return "Vulnerable"
		case "0":
			return "Not Vulnerable"
		case "2":
			return "Broken Functionality"
		case "4":
			return "Not Addressed"
		case "5":
			return "Partially Remediated"
		default: return "N/A"
		};
	}
	$scope.getClassForChallengeResult = function(user,flag){
		var status = $scope.getChallengeResultFor(user,flag);

		switch(status){
		case "Vulnerable":
			return "table-danger"
		case "Not Vulnerable":
			return "table-success"
		case "Broken Functionality":
			return "table-warning"
		case "Not Addressed":
			return "table-info"
		case "Partially Remediated":
			return "table-warning"
		default: return "table-light"
		};



	}
	$scope.backToList = function(){
		$rootScope.showChallengeDetails = false;
		$rootScope.showChallengesList = true;
		$location.path("tournaments", false);
	}


	$scope.getChallengeStatusString = function(status){
		switch(status){
		case "IN_PROGRESS":
			return "In Progress";
			break;
		case "NOT_STARTED":
			return "Not Started";
			break;
		case "FINISHED":
			return "Finished";
			break;
		}
	}


	$scope.updateChallengesFilteredList = function() {
		$scope.filteredChallengesList = $filter("filter")($scope.masterChallengesList, $scope.queryChallenges);
	};
	$scope.challengestableconfig = {
			itemsPerPage: 20,
			fillLastPage: false
	}
	$scope.$on('challenges:updated', function(event,data) {
		$scope.masterChallengesList = data;
		$scope.filteredChallengesList = $scope.masterChallengesList;
	});

}]);
