# 
# This file is part of the SecureFlag Platform.
# Copyright (c) 2020 SecureFlag Limited.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
echo "Starting Portal Service..."
if [ -z "$ELB" ]; then
	echo 'Skipping ELB Configuration...'
	mv /tmp/nginx/notls /etc/nginx/sites-available/default
	/etc/init.d/nginx start
	sed -i "s/<\!--<secure>COOKIE_FLAG<\/secure>-->/<secure>false<\/secure>/g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/web.xml
else
	echo 'Performing ELB Configuration...'
	sed -i "s/FQDNTOREPLACE/${DOMAIN}/g" /tmp/nginx/elb
	mv /tmp/nginx/elb /etc/nginx/sites-available/default
	/etc/init.d/nginx start
	sed -i "s/<\!--<secure>COOKIE_FLAG<\/secure>-->/<secure>true<\/secure>/g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/web.xml
fi

sed -i "s/AWS_REGION/${AWS_REGION}/g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/classes/config.properties
sed -i "s/GUAC_ADMIN_PASSWORD/${GUAC_ADMIN_PASSWORD}/g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/classes/config.properties
sed -i "s/EXERCISE_POOL/${EXERCISE_CLUSTER}/g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/classes/config.properties
sed -i "s/GATEWAY_AGENT_PASSWORD/${GATEWAY_AGENT_PASSWORD}/g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/classes/config.properties
sed -i "s/HUB_ADDRESS/${HUB_ADDRESS}/g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/classes/config.properties
sed -i "s~HUB_ROLE~${HUB_ROLE}~g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/classes/config.properties
sed -i "s~SDK_ROLE~${SDK_ROLE}~g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/classes/config.properties
sed -i "s/MYSQL_PORTAL_PASSWORD/${MYSQL_PORTAL_PASSWORD}/g" /var/lib/tomcat8/webapps/ROOT/WEB-INF/classes/hibernate.cfg.xml

export CATALINA_OPTS=" -Xms512m -Xmx8192m "
chown tomcat8:497 /var/run/docker.sock

mkdir -p /usr/share/tomcat8/logs
/etc/init.d/tomcat8 start
rm /tmp/init.sh
rm -rf /tmp/nginx
tail -F /var/lib/tomcat8/logs/catalina.out