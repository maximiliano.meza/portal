# 
# This file is part of the SecureFlag Platform.
# Copyright (c) 2020 SecureFlag Limited.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

FROM ubuntu:16.04
ARG DEBIAN_FRONTEND=noninteractive
RUN	apt-get update && apt-get -y upgrade --no-install-recommends && apt-get -y install --no-install-recommends nginx openjdk-8-jdk tomcat8 unzip

COPY	portal.zip /tmp/portal.zip
RUN     unzip /tmp/portal.zip -d /tmp/ && rm -rf /var/lib/tomcat8/webapps \ 
		&& mv /tmp/fs/webapps/ /var/lib/tomcat8/ \
		&& mv /tmp/run/init.sh /tmp/init.sh && mv /tmp/run/nginx /tmp/nginx \
		&& mkdir /usr/share/tomcat8/.docker && mv /tmp/fs/config.json /usr/share/tomcat8/.docker/config.json && chown -R tomcat8:tomcat8 /usr/share/tomcat8 \
		&& mv /tmp/fs/docker-credential-ecr-login /bin/ \
		&& rm -rf /tmp/fs && rm -rf /tmp/run && rm /tmp/portal.zip \
		&& chown -R tomcat8:tomcat8 /var/lib/tomcat8/ && gpasswd -a www-data tomcat8 && chmod -R 0755 /var/lib/tomcat8/webapps && chmod +x /tmp/init.sh

VOLUME /var/run/docker.sock

EXPOSE 80
ENTRYPOINT /tmp/init.sh